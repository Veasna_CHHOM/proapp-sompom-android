package com.sompom.videomanager.helper;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.sompom.videomanager.model.MediaListItem;
import com.sompom.videomanager.widget.MediaRecyclerView;

/**
 * Created by He Rotha on 8/8/18.
 */
public class GridScrollCalculator extends LinearScrollCalculator {
    private GridLayoutManager mLayoutManager;
    private boolean mIsCalculateHeight;

    public GridScrollCalculator(MediaRecyclerView recyclerView,
                                boolean isCalculateHeight,
                                GridLayoutManager gridLayoutManager) {
        super(recyclerView, gridLayoutManager);
        mIsCalculateHeight = isCalculateHeight;
        mLayoutManager = gridLayoutManager;
    }

    @Override
    public synchronized void onScroll(boolean isStartPlay) {
        if (mIsCalculateHeight) {
            super.onScroll(isStartPlay);
        } else {
            calculateVideoPlaying();
        }
    }

    private void calculateVideoPlaying() {
        int first = mLayoutManager.findFirstVisibleItemPosition();
        int last = mLayoutManager.findLastVisibleItemPosition();
        boolean hasPlay = false;

        for (int i = first; i <= last; i++) {
            RecyclerView.ViewHolder holder = getRecyclerView().findViewHolderForAdapterPosition(i);
            if (holder instanceof MediaListItem) {
                hasPlay = true;
                checkPlaying(i, holder);
                break;
            }
        }

        if (!hasPlay) {
            pauseCurrent();
            getCurrentItem().setViewHolder(null);
            getCurrentItem().setPosition(-1);
        }

    }

    private void checkPlaying(int i, RecyclerView.ViewHolder holder) {
        if (i != getCurrentItem().getPosition()) {
            pauseCurrent();
            ((MediaListItem) holder).onMediaPlay(true);
            getCurrentItem().setViewHolder(holder);
            getCurrentItem().setPosition(i);
        }
    }

}
