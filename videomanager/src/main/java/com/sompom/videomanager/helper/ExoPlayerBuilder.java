package com.sompom.videomanager.helper;

import android.app.Application;
import android.content.Context;
import android.net.Uri;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.sompom.videomanager.model.CustomExoPlayerEventListener;

/**
 * Created by He Rotha on 9/29/17.
 */

public class ExoPlayerBuilder {
    private static final DefaultLoadControl sLoadControl = new DefaultLoadControl();
    private static final DefaultTrackSelector sTrackSelector = new DefaultTrackSelector();
    private static ExoData sExoData;

    private ExoPlayerBuilder() {
    }

    public static void initApp(Application application) {
        sExoData = new ExoData();
        sExoData.mRendererFactory = new DefaultRenderersFactory(application);
        sExoData.mSourceFactory = new CacheDataSourceFactory(application, 100 * 1024 * 1024, 5 * 1024 * 1024);
    }

    private static void initContext(Context context) {
        initApp((Application) context.getApplicationContext());
    }

    public static SimpleExoPlayer build(Context context, String url, OnPlayerListener listener) {
        if (sExoData == null) {
            initContext(context);
        }

        sExoData.mExoPlayer = ExoPlayerFactory.newSimpleInstance(sExoData.mRendererFactory, sTrackSelector, sLoadControl);
        final MediaSource videoSource = new ExtractorMediaSource.Factory(sExoData.mSourceFactory).createMediaSource(Uri.parse(url));
        sExoData.mExoPlayer.prepare(videoSource);
        sExoData.mExoPlayer.addListener(new CustomExoPlayerEventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playbackState == Player.STATE_ENDED) {
                    //restart video's position to 0 second if video is completed
                    sExoData.mExoPlayer.seekTo(0);
                    sExoData.mExoPlayer.setPlayWhenReady(false);
                } else if (playbackState == Player.STATE_READY && listener != null) {
                    listener.onPlayStatus(sExoData.mExoPlayer.getPlayWhenReady());
                }
            }

        });
        return sExoData.mExoPlayer;
    }

    public interface OnPlayerListener {
        /**
         * @param isPlaying: if isPlaying equal true means that video is playing, otherwise means pause
         */
        void onPlayStatus(boolean isPlaying);
    }

    private static class ExoData {
        private CacheDataSourceFactory mSourceFactory;
        private SimpleExoPlayer mExoPlayer;
        private DefaultRenderersFactory mRendererFactory;
    }
}
