package com.sompom.videomanager.helper;

import android.content.Context;

import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;

import java.io.File;

/**
 * Created by nuonveyo
 * on 1/9/19.
 */

public class VideoCache {
    private static SimpleCache sDownloadCache;

    public static SimpleCache getInstance(Context context, long maxCacheSize) {
        if (sDownloadCache == null) {
            LeastRecentlyUsedCacheEvictor evictor = new LeastRecentlyUsedCacheEvictor(maxCacheSize);
            File file = new File(context.getCacheDir(), "media");
            sDownloadCache = new SimpleCache(file, evictor);
        }
        return sDownloadCache;
    }
}
