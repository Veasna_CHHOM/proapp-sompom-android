package com.sompom.videomanager.helper;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.sompom.videomanager.model.MediaListItem;
import com.sompom.videomanager.widget.MediaRecyclerView;


/**
 * Created by He Rotha on 7/18/18.
 */
public class LinearScrollCalculator extends AbsScrollCalculator {
    private static final int PLAYING_PERCENTAGE = 60;
    private final LinearLayoutManager mLayoutManager;
    private OnButtonPlayClickListener mOnMuteButtonClick = new OnButtonPlayClickListener() {
        @Override
        public void onButtonPlayClick(boolean isPlay) {
            if (isPlay) {
                getRecyclerView().setAutoPlay(true);
                getRecyclerView().requestAudioFocus();
                getRecyclerView().resume();
            } else {
                getRecyclerView().setAutoPlay(false);
                getRecyclerView().abandonAudioFocus();
            }
        }

        @Override
        public void onButtonMuteClick(boolean isMute) {
//no need to handle video pause/resume too much if user click isMute
            //following user's experience, they will know how to avoid duel audio playing
            if (!isMute) {
                getRecyclerView().requestAudioFocus();
                getRecyclerView().abandonAudioFocus();
            }
        }
    };

    public LinearScrollCalculator(MediaRecyclerView recyclerView,
                                  LinearLayoutManager layoutManager) {
        super(recyclerView);
        mLayoutManager = layoutManager;
    }

    @Override
    public synchronized void onScroll(boolean isStartPlay) {
        int first = mLayoutManager.findFirstVisibleItemPosition();
        int last = mLayoutManager.findLastVisibleItemPosition();

        int maxPercent = -1;
        int maxIndex = -1;

        for (int i = first; i <= last; i++) {
            View tempView = getView(i);

            if (tempView == null) {
                continue;
            }
            int percent = getVisibilityPercents(tempView);
            if (percent > maxPercent && percent > PLAYING_PERCENTAGE) {
                maxPercent = percent;
                maxIndex = i;
            } else if (percent < PLAYING_PERCENTAGE &&
                    i == getCurrentItem().getPosition() &&
                    getCurrentItem().isPlaying()) {
                getCurrentItem().setPlaying(false);

                if (getCurrentItem().getViewHolder() instanceof MediaListItem) {
                    ((MediaListItem) getCurrentItem().getViewHolder()).onMediaPause();
                }
            }
        }

        startPlay(isStartPlay, maxPercent, maxIndex);
    }

    private View getView(int i) {
        View tempView = null;
        RecyclerView.ViewHolder viewHolder = getRecyclerView().findViewHolderForLayoutPosition(i);
        if (viewHolder instanceof MediaListItem) {
            tempView = ((MediaListItem) viewHolder).getPlayerView();
        }
        return tempView;
    }

    private void startPlay(boolean isStartPlay, int maxPercent, int maxIndex) {
        if (maxIndex == -1) {
            return;
        }

        if (getCurrentItem().getPosition() == maxIndex && getCurrentItem().isPlaying()) {
            return;
        }

        RecyclerView.ViewHolder holder = getRecyclerView().findViewHolderForAdapterPosition(maxIndex);
        if (getCurrentItem() != null &&
                getCurrentItem().getViewHolder() instanceof MediaListItem &&
                getCurrentItem().isPlaying()) {
            getCurrentItem().setPlaying(false);
            ((MediaListItem) getCurrentItem().getViewHolder()).onMediaPause();
        }

        getCurrentItem().setViewHolder(holder);
        getCurrentItem().setPercent(maxPercent);
        getCurrentItem().setPosition(maxIndex);
        getCurrentItem().setPlaying(true);

        if (holder instanceof MediaListItem) {
            ((MediaListItem) holder).onMediaPlay(isStartPlay);
            ((MediaListItem) holder).onVolumeClick(mOnMuteButtonClick);
        }
    }


}
