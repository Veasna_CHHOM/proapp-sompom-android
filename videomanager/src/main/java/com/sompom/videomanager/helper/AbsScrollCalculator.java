package com.sompom.videomanager.helper;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.sompom.videomanager.model.MediaListItem;
import com.sompom.videomanager.model.MyPlayingItem;
import com.sompom.videomanager.widget.MediaRecyclerView;

/**
 * Created by He Rotha on 8/8/18.
 */
public abstract class AbsScrollCalculator {
    private final MyPlayingItem mCurrentItem = new MyPlayingItem();
    private final Rect mCurrentViewRect = new Rect();
    private final MediaRecyclerView mRecyclerView;

    public AbsScrollCalculator(MediaRecyclerView recyclerView) {
        mRecyclerView = recyclerView;
    }

    public synchronized void onScroll(boolean isStartPlay) {

    }


    public MyPlayingItem getCurrentItem() {
        return mCurrentItem;
    }

    public MediaRecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    public int getVisibilityPercents(View view) {
        if (view == null) {
            return 0;
        }
        int percents = 100;

        view.getLocalVisibleRect(mCurrentViewRect);

        int height = view.getHeight();
        if (height == 0) {
            return 0;
        }
        if (viewIsPartiallyHiddenTop()) {
            // view is partially hidden behind the top edge
            percents = (height - mCurrentViewRect.top) * 100 / height;
        } else if (viewIsPartiallyHiddenBottom(height)) {
            percents = mCurrentViewRect.bottom * 100 / height;
        } else if (mCurrentViewRect.bottom < height) {
            percents = 0;
        }

        return percents;
    }

    private boolean viewIsPartiallyHiddenTop() {
        return mCurrentViewRect.top > 0;
    }

    private boolean viewIsPartiallyHiddenBottom(int height) {
        return mCurrentViewRect.bottom > 0 && mCurrentViewRect.bottom < height;
    }

    public void pauseCurrent() {
        try {
            if (mCurrentItem.getViewHolder() instanceof MediaListItem) {
                ((MediaListItem) mCurrentItem.getViewHolder()).onMediaPause();
            }
            mCurrentItem.setPlaying(false);
        } catch (Exception ignore) {
            //ignore the message
        }
    }

    public void setCurrentPlayPosition(int position) {
        RecyclerView.ViewHolder holder = getRecyclerView().findViewHolderForAdapterPosition(position);
        if (holder instanceof MediaListItem) {
            pauseCurrent();
            ((MediaListItem) holder).onMediaPlay(true);
            getCurrentItem().setPlaying(true);
            getCurrentItem().setViewHolder(holder);
            getCurrentItem().setPosition(position);
        }
    }
}
