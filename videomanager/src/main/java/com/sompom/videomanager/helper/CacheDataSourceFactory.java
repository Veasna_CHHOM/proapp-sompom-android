package com.sompom.videomanager.helper;

import android.content.Context;

import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.FileDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSink;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;
import com.sompom.videomanager.R;

/**
 * Created by nuonveyo
 * on 1/9/19.
 */

class CacheDataSourceFactory implements DataSource.Factory {
    private final Context mContext;
    private final DefaultDataSourceFactory mDefaultDataSourceFactory;
    private final long mMaxFileSize;
    private final long mMaxCacheSize;

    CacheDataSourceFactory(Context context, long maxCacheSize, long maxFileSize) {
        super();
        this.mContext = context;
        this.mMaxCacheSize = maxCacheSize;
        this.mMaxFileSize = maxFileSize;
        String userAgent = Util.getUserAgent(context, context.getString(R.string.app_name));
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        mDefaultDataSourceFactory = new DefaultDataSourceFactory(this.mContext,
                bandwidthMeter,
                new DefaultHttpDataSourceFactory(userAgent, bandwidthMeter));
    }

    @Override
    public DataSource createDataSource() {
        SimpleCache simpleCache = VideoCache.getInstance(mContext, mMaxCacheSize);
        return new CacheDataSource(simpleCache,
                mDefaultDataSourceFactory.createDataSource(),
                new FileDataSource(),
                new CacheDataSink(simpleCache, mMaxFileSize),
                CacheDataSource.FLAG_BLOCK_ON_CACHE | CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR,
                null);
    }
}
