package com.sompom.videomanager.model;

import android.support.annotation.Nullable;

import com.google.android.exoplayer2.ui.PlayerView;
import com.sompom.videomanager.helper.OnButtonPlayClickListener;

/**
 * Created by He Rotha on 7/24/18.
 */
public interface MediaListItem {

    //MediaRecyclerView fire listener
    void onMediaPlay(boolean isStartPlay);

    //MediaRecyclerView fire listener
    void onMediaPause();

    //MediaRecyclerView require
    default void onVolumeClick(OnButtonPlayClickListener listener) {
        //nothing to do
    }

    //MediaRecyclerView require
    @Nullable
    PlayerView getPlayerView();

}
