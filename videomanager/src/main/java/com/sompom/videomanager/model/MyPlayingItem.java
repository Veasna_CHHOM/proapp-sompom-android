package com.sompom.videomanager.model;

import android.support.v7.widget.RecyclerView;

/**
 * Created by He Rotha on 7/23/18.
 */
public class MyPlayingItem {
    private int mPosition = -1;
    private int mPercent;
    private RecyclerView.ViewHolder mViewHolder;
    private boolean mIsPlaying;

    public int getPosition() {
        return mPosition;
    }

    public void setPosition(int position) {
        mPosition = position;
    }

    public int getPercent() {
        return mPercent;
    }

    public void setPercent(int percent) {
        mPercent = percent;
    }

    public RecyclerView.ViewHolder getViewHolder() {
        return mViewHolder;
    }

    public void setViewHolder(RecyclerView.ViewHolder viewHolder) {
        mViewHolder = viewHolder;
    }

    public boolean isPlaying() {
        return mIsPlaying;
    }

    public void setPlaying(boolean playing) {
        mIsPlaying = playing;
    }
}
