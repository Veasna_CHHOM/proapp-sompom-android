package com.sompom.videomanager.widget;

import android.content.Context;
import android.media.AudioManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.sompom.videomanager.helper.AbsScrollCalculator;
import com.sompom.videomanager.helper.GridScrollCalculator;
import com.sompom.videomanager.helper.LinearScrollCalculator;

/**
 * Created by He Rotha on 7/23/18.
 */
public class MediaRecyclerView extends RecyclerView {

    private AbsScrollCalculator mVideoVisibilityCalculator;
    private final RecyclerView.OnScrollListener mOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int scrollState) {
            //there is nothing to do on this method
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            //auto play video is no need to handle to much condition,
            //due to user experience, they will know how to pause other media during play video
            mVideoVisibilityCalculator.onScroll(mIsAutoPlay);
        }
    };
    private boolean mIsResume;
    private boolean mIsEnablePauseResume = true;
    private boolean mIsAutoPlay = true;
    private AudioManager mAudioManager;
    private AudioManager.OnAudioFocusChangeListener mOnAudioFocusChangeListener = focusChange -> {
        if (focusChange > 0) {
            setAutoPlay(true);
            resume();
        } else {
            setAutoPlay(false);
            pause();
        }
    };

    public MediaRecyclerView(Context context) {
        super(context);
    }

    public MediaRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public MediaRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setLayoutManager(LayoutManager layout) {
        this.setLayoutManager(layout, true);
    }

    public void setLayoutManager(LayoutManager layout, boolean isCalculateHeight) {
        super.setLayoutManager(layout);
        if (layout instanceof GridLayoutManager) {
            mVideoVisibilityCalculator = new GridScrollCalculator(this, isCalculateHeight, (GridLayoutManager) layout);
        } else if (layout instanceof LinearLayoutManager) {
            mVideoVisibilityCalculator = new LinearScrollCalculator(this, (LinearLayoutManager) layout);
        }
        addOnScrollListener(mOnScrollListener);
    }

    public void setAutoResume(boolean enablePauseResume) {
        mIsEnablePauseResume = enablePauseResume;
    }


    public void onPause() {
        mIsResume = false;
        pause();
    }

    public void onResume() {
        mIsResume = true;
        resume();
    }

    public void pause() {
        if (mVideoVisibilityCalculator != null) {
            mVideoVisibilityCalculator.pauseCurrent();
        }
    }

    public void resume() {
        if (mVideoVisibilityCalculator != null && mIsEnablePauseResume && mIsResume) {
            mVideoVisibilityCalculator.onScroll(mIsAutoPlay);
        }
    }

    public void resumeAfterRecycle() {
        setRecyclerListener(viewHolder -> {
            setAutoResume(true);
            resume();
            setRecyclerListener(null);
        });
    }

    public void setAutoPlay(boolean isEnable) {
        mIsAutoPlay = isEnable;
    }

    public void setCurrentPlayPosition(int position) {
        if (mVideoVisibilityCalculator != null) {
            mVideoVisibilityCalculator.setCurrentPlayPosition(position);
        }
    }

    public void requestAudioFocus() {
        abandonAudioFocus();
        getAudioManager().requestAudioFocus(mOnAudioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
    }

    public void abandonAudioFocus() {
        getAudioManager().abandonAudioFocus(mOnAudioFocusChangeListener);
    }

    private AudioManager getAudioManager() {
        if (mAudioManager == null) {
            mAudioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
        }
        return mAudioManager;
    }
}
