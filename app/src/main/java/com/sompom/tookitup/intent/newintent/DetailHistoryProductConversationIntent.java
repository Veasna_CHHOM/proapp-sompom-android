package com.sompom.tookitup.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.newui.DetailHistoryProductConversationActivity;
import com.sompom.tookitup.utils.SharedPrefUtils;

/**
 * Created by nuonveyo on 7/2/18.
 */

public class DetailHistoryProductConversationIntent extends Intent {
    public DetailHistoryProductConversationIntent(Intent o) {
        super(o);
    }

    public DetailHistoryProductConversationIntent(Context packageContext, Product product) {
        super(packageContext, DetailHistoryProductConversationActivity.class);
        putExtra(SharedPrefUtils.DATA, product);
    }

    public Product getProduct() {
        return getParcelableExtra(SharedPrefUtils.DATA);
    }
}
