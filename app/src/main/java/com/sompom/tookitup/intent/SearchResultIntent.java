package com.sompom.tookitup.intent;

import android.content.Context;
import android.content.Intent;

import com.sompom.tookitup.newui.SearchResultActivity;

/**
 * Created by He Rotha on 8/21/18.
 */
public class SearchResultIntent extends Intent {
    public SearchResultIntent(Context packageContext) {
        super(packageContext, SearchResultActivity.class);
    }
}
