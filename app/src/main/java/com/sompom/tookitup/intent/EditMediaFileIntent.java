package com.sompom.tookitup.intent;

import android.content.Context;
import android.content.Intent;

import com.sompom.tookitup.model.LifeStream;
import com.sompom.tookitup.newui.EditMediaFileActivity;
import com.sompom.tookitup.utils.SharedPrefUtils;

/**
 * Created by nuonveyo on 5/7/18.
 */

public class EditMediaFileIntent extends Intent {

    public EditMediaFileIntent(Intent o) {
        super(o);
    }

    public EditMediaFileIntent(LifeStream lifeStream) {
        putExtra(SharedPrefUtils.DATA, lifeStream);
    }

    public EditMediaFileIntent(Context context, LifeStream lifeStream, int position, boolean isEdit) {
        super(context, EditMediaFileActivity.class);
        putExtra(SharedPrefUtils.DATA, lifeStream);
        putExtra(SharedPrefUtils.USER_ID, position);
        putExtra(SharedPrefUtils.STATUS, isEdit);
    }

    public int getPosition() {
        return getIntExtra(SharedPrefUtils.USER_ID, 0);
    }

    public boolean isEdit() {
        return getBooleanExtra(SharedPrefUtils.STATUS, false);
    }

    public LifeStream getLifeStream() {
        return getParcelableExtra(SharedPrefUtils.DATA);
    }

}
