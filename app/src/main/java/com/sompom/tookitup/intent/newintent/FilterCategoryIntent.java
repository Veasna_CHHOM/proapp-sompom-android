package com.sompom.tookitup.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.sompom.tookitup.model.result.Category;
import com.sompom.tookitup.newui.FilterCategoryActivity;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.ArrayList;

/**
 * Created by he.rotha on 4/21/16.
 */
public class FilterCategoryIntent extends Intent {
    public FilterCategoryIntent(Context packageContext, ArrayList<Category> categoryArrayList, boolean isEmpty) {
        super(packageContext, FilterCategoryActivity.class);
        this.putParcelableArrayListExtra(SharedPrefUtils.CATEGORY, categoryArrayList);
        this.putExtra(SharedPrefUtils.IS_ACTIVITY, isEmpty);
    }

    public FilterCategoryIntent(Intent o) {
        super(o);
    }

    public ArrayList<Category> getCategoryList() {
        return getParcelableArrayListExtra(SharedPrefUtils.CATEGORY);
    }

    public boolean isEmpty() {
        return getBooleanExtra(SharedPrefUtils.IS_ACTIVITY, true);
    }
}
