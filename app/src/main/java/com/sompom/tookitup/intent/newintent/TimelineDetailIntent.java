package com.sompom.tookitup.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.sompom.tookitup.model.WallStreetAdaptive;
import com.sompom.tookitup.newui.TimelineDetailActivity;
import com.sompom.tookitup.utils.SharedPrefUtils;

/**
 * Created by nuonveyo on 7/12/18.
 */

public class TimelineDetailIntent extends Intent {
    public TimelineDetailIntent(Intent o) {
        super(o);
    }

    public TimelineDetailIntent(Context packageContext, WallStreetAdaptive adaptive, int position) {
        super(packageContext, TimelineDetailActivity.class);
        putExtra(SharedPrefUtils.DATA, adaptive);
        putExtra(SharedPrefUtils.ID, position);
    }

    public TimelineDetailIntent(Context packageContext, String id) {
        super(packageContext, TimelineDetailActivity.class);
        putExtra(SharedPrefUtils.STATUS, id);
    }

    public int getPosition() {
        return getIntExtra(SharedPrefUtils.ID, 0);
    }

    public String getWallStreetId() {
        return getStringExtra(SharedPrefUtils.STATUS);
    }

    public WallStreetAdaptive getLifeStream() {
        return getParcelableExtra(SharedPrefUtils.DATA);
    }
}
