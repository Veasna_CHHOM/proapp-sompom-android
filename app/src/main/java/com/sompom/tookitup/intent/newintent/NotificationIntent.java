package com.sompom.tookitup.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.sompom.tookitup.newui.NotificationActivity;

/**
 * Created by Chhom Veasna on 6/27/2019.
 */
public class NotificationIntent extends Intent {

    public NotificationIntent(Context packageContext) {
        super(packageContext, NotificationActivity.class);
    }
}
