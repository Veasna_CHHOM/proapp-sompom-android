package com.sompom.tookitup.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.sompom.tookitup.newui.FilterDetailActivity;

/**
 * Created by nuonveyo on 7/5/18.
 */

public class SearchOptionIntent extends Intent {
    public SearchOptionIntent(Context packageContext) {
        super(packageContext, FilterDetailActivity.class);
    }


}
