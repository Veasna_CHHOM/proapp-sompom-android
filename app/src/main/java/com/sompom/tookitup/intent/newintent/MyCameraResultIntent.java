package com.sompom.tookitup.intent.newintent;

import android.content.Intent;

import com.desmond.squarecamera.intent.CameraResultIntent;
import com.desmond.squarecamera.model.MediaFile;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.emun.MediaType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by He Rotha on 6/12/18.
 */
public class MyCameraResultIntent extends CameraResultIntent {

    public MyCameraResultIntent(Intent o) {
        super(o);
    }

    public ArrayList<Media> getMedia() {
        ArrayList<Media> arrayList = new ArrayList<>();

        List<MediaFile> list = getMediaFiles();

        for (MediaFile mediaFile : list) {
            Media media = new Media();
            media.setTitle(null);
            media.setUrl(mediaFile.getPath());
            media.setWidth(mediaFile.getWidth());
            media.setHeight(mediaFile.getHeight());
            media.setIndex(mediaFile.getPosition());

            if (mediaFile.getType() == com.desmond.squarecamera.model.MediaType.VIDEO) {
                media.setType(MediaType.VIDEO);
            } else {
                media.setType(MediaType.IMAGE);
            }
            arrayList.add(media);
        }

        return arrayList;
    }
}
