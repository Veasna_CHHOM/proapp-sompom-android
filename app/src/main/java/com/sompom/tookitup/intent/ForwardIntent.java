package com.sompom.tookitup.intent;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;

import com.desmond.squarecamera.utils.Keys;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.newui.ForwardActivity;
import com.sompom.tookitup.utils.MediaUtil;

import java.util.ArrayList;


/**
 * Created by he.rotha on 2/9/16.
 */
public class ForwardIntent extends Intent {
    private Context mContext;
    private boolean mIsFromOutSideApp = false;

    public ForwardIntent(Context packageContext, String link) {
        super(packageContext, ForwardActivity.class);
        putExtra(Intent.EXTRA_TEXT, link);
    }

    public ForwardIntent(Context packageContext,
                         Media product) {
        super(packageContext, ForwardActivity.class);
        ArrayList<Media> data = new ArrayList<>();
        data.add(product);
        putExtra(Keys.DATA, data);
    }

    public ForwardIntent(Context packageContext,
                         Chat chat) {
        super(packageContext, ForwardActivity.class);
        if (chat.getMediaList() != null && chat.getMediaList().size() > 0) {
            ArrayList<Media> data = new ArrayList<>(chat.getMediaList());
            putExtra(Keys.DATA, data);
        } else {
            putExtra(Keys.PATH, chat);
        }
    }

    public ForwardIntent(Activity context, Intent o) {
        super(o);
        mContext = context;
    }

    public ArrayList<Media> getMedia() {
        ArrayList<Media> data = getParcelableArrayListExtra(Keys.DATA);
        if (data == null || data.isEmpty()) {
            String action = getAction();
            if (TextUtils.isEmpty(action)) {
                return null;
            } else {
                mIsFromOutSideApp = true;
                switch (action) {
                    case Intent.ACTION_SEND_MULTIPLE:
                        return handleMultiple();
                    case Intent.ACTION_SEND:
                    case Intent.EXTRA_STREAM:
                        return handleSingle();
                    default:
                        return null;
                }
            }
        } else {
            return data;
        }
    }

    public Chat getChat() {
        Chat chat = getParcelableExtra(Keys.PATH);
        if (chat == null) {
            mIsFromOutSideApp = true;
            Chat chat1 = new Chat();
            chat1.setContent(getStringExtra(Intent.EXTRA_TEXT));
            return chat1;
        } else {
            return chat;
        }
    }

    public boolean isFromOutSideApplication() {
        return mIsFromOutSideApp;
    }

    private ArrayList<Media> handleSingle() {
        Uri imageUri = getParcelableExtra(Intent.EXTRA_STREAM);
        ArrayList<Media> data = new ArrayList<>();
        Media productMedia = MediaUtil.getMediaFromOtherApp(mContext, imageUri);
        if (productMedia != null) {
            data.add(productMedia);
        }

        return data;
    }

    private ArrayList<Media> handleMultiple() {
        ArrayList<Uri> imageUris = getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        ArrayList<Media> data = new ArrayList<>();
        for (Uri uri : imageUris) {
            Media productMedia = MediaUtil.getMediaFromOtherApp(mContext, uri);
            if (productMedia != null) {
                data.add(productMedia);
            }
        }
        return data;
    }
}
