package com.sompom.tookitup.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.sompom.tookitup.newui.SuggestionActivity;

/**
 * Created by nuonveyo on 7/20/18.
 */

public class SuggestionIntent extends Intent {
    public SuggestionIntent(Context packageContext) {
        super(packageContext, SuggestionActivity.class);
    }
}
