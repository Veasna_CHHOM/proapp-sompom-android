package com.sompom.tookitup.intent;

import android.content.Context;
import android.content.Intent;

import com.sompom.tookitup.newui.AdvancedNotificationSettingActivity;

/**
 * Created by ChhomVeasna on 8/23/16.
 */
public class AdvancedNotificationSettingIntent extends Intent {
    public AdvancedNotificationSettingIntent(Context packageContext) {
        super(packageContext, AdvancedNotificationSettingActivity.class);
    }
}
