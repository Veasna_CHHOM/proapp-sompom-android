package com.sompom.tookitup.intent.newintent;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;

import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.newui.ProductFormActivity;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nuonveyo on 6/11/18.
 */

public class ProductFormIntent extends Intent {
    public ProductFormIntent(Intent intent) {
        super(intent);
    }

    public ProductFormIntent(Context context, List<Media> imagePaths) {
        super(context, ProductFormActivity.class);
        putParcelableArrayListExtra(SharedPrefUtils.DATA, (ArrayList<? extends Parcelable>) imagePaths);
    }

    public ProductFormIntent(Context context, Product product) {
        super(context, ProductFormActivity.class);
        putExtra(SharedPrefUtils.PRODUCT, product);
    }

    public List<Media> getImagePaths() {
        return getParcelableArrayListExtra(SharedPrefUtils.DATA);
    }

    public Product getProduct() {
        return getParcelableExtra(SharedPrefUtils.PRODUCT);
    }
}
