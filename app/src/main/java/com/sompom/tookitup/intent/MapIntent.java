package com.sompom.tookitup.intent;

import android.content.Context;
import android.content.Intent;

import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.newui.MapActivity;
import com.sompom.tookitup.utils.SharedPrefUtils;

/**
 * Created by he.rotha on 2/9/16.
 */
public class MapIntent extends Intent {
    public MapIntent(Context packageContext, Product product) {
        super(packageContext, MapActivity.class);
        putExtra(SharedPrefUtils.PRODUCT, product);
    }

    public MapIntent(Intent o) {
        super(o);
    }

    public Product getProduct() {
        return getParcelableExtra(SharedPrefUtils.PRODUCT);
    }
}
