package com.sompom.tookitup.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.sompom.tookitup.newui.SignUpActivity;

/**
 * Created by He Rotha on 6/4/18.
 */
public class LoginIntent extends Intent {
    public LoginIntent(Context packageContext) {
        super(packageContext, SignUpActivity.class);
    }
}
