package com.sompom.tookitup.intent;

import android.content.Context;
import android.content.Intent;

import com.sompom.tookitup.chat.call.NotificationCallVo;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.CallingActivity;
import com.sompom.tookitup.utils.SharedPrefUtils;

/**
 * Created by nuonveyo on 6/21/18.
 */

public class CallingIntent extends Intent {
    public CallingIntent(Context context, NotificationCallVo isChangeActivateStore) {
        super(context, CallingActivity.class);
        putExtra(SharedPrefUtils.STATUS, isChangeActivateStore);
        setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    }

    public CallingIntent(Context context, User callTo, Product product) {
        super(context, CallingActivity.class);
        putExtra(SharedPrefUtils.ID, callTo);
        putExtra(SharedPrefUtils.PRODUCT, product);
    }

    public CallingIntent(Intent data) {
        super(data);
    }

    public NotificationCallVo getNotification() {
        return getParcelableExtra(SharedPrefUtils.STATUS);
    }

    public Product getProduct() {
        return getParcelableExtra(SharedPrefUtils.PRODUCT);
    }

    public User getRecipient() {
        return getParcelableExtra(SharedPrefUtils.ID);
    }
}
