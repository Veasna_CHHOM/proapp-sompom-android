package com.sompom.tookitup.intent;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;

import com.sompom.tookitup.model.LifeStream;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.WallStreetAdaptive;
import com.sompom.tookitup.model.emun.CreateWallStreetScreenType;
import com.sompom.tookitup.newui.CreateTimelineActivity;
import com.sompom.tookitup.utils.MediaUtil;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.ArrayList;

/**
 * Created by nuonveyo on 5/4/18.
 */

public class CreateTimelineIntent extends Intent {
    private Context mContext;

    public CreateTimelineIntent(Activity activity, Intent o) {
        super(o);
        mContext = activity;
    }


    public CreateTimelineIntent(Context context, CreateWallStreetScreenType type) {
        super(context, CreateTimelineActivity.class);
        putExtra(SharedPrefUtils.ID, type.getId());
    }

    public CreateTimelineIntent(Context context, WallStreetAdaptive adaptive, CreateWallStreetScreenType type) {
        this(context, type);
        putExtra(SharedPrefUtils.DATA, adaptive);
    }

    public WallStreetAdaptive getLifeStream() {
        WallStreetAdaptive adaptive = getParcelableExtra(SharedPrefUtils.DATA);
        if (adaptive == null) {
            String action = getAction();
            if (TextUtils.isEmpty(action)) {
                return null;
            } else {
                LifeStream lifeStream = new LifeStream();
                lifeStream.setTitle(getStringExtra(Intent.EXTRA_TEXT));
                putExtra(SharedPrefUtils.ID, CreateWallStreetScreenType.SHARE_LINK.getId());
                switch (action) {
                    case Intent.ACTION_SEND_MULTIPLE:
                        return handleMultiple(lifeStream);
                    case Intent.ACTION_SEND:
                    case Intent.EXTRA_STREAM:
                        return handleSingle(lifeStream);
                    default:
                        return lifeStream;
                }
            }
        } else {
            return adaptive;
        }

    }

    public int getScreenTypeId() {
        return getIntExtra(SharedPrefUtils.ID, CreateWallStreetScreenType.CREATE_POST.getId());
    }

    private LifeStream handleSingle(LifeStream lifeStream) {
        Uri imageUri = getParcelableExtra(Intent.EXTRA_STREAM);
        ArrayList<Media> data = new ArrayList<>();
        Media productMedia = MediaUtil.getMediaFromOtherApp(mContext, imageUri);
        if (productMedia != null) {
            data.add(productMedia);
        }
        lifeStream.setMedia(data);
        return lifeStream;
    }

    private LifeStream handleMultiple(LifeStream lifeStream) {
        ArrayList<Uri> imageUris = getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        ArrayList<Media> data = new ArrayList<>();
        for (Uri uri : imageUris) {
            Media productMedia = MediaUtil.getMediaFromOtherApp(mContext, uri);
            if (productMedia != null) {
                data.add(productMedia);
            }
        }
        lifeStream.setMedia(data);
        return lifeStream;
    }
}
