package com.sompom.tookitup.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.sompom.tookitup.newui.SellerStoreActivity;
import com.sompom.tookitup.utils.SharedPrefUtils;

/**
 * Created by he.rotha on 3/15/16.
 */
public class SellerStoreIntent extends Intent {

    public SellerStoreIntent(Context packageContext, String userId) {
        super(packageContext, SellerStoreActivity.class);
        putExtra(SharedPrefUtils.PRODUCT, userId);
    }

    public SellerStoreIntent(Intent o) {
        super(o);
    }

    public String getUserId() {
        return getStringExtra(SharedPrefUtils.PRODUCT);
    }
}
