package com.sompom.tookitup.intent;

import android.content.Context;
import android.content.Intent;

import com.google.android.gms.maps.model.LatLng;
import com.sompom.tookitup.model.SearchAddressResult;
import com.sompom.tookitup.newui.SelectAddressActivity;

/**
 * Created by nuonveyo on 5/10/18.
 */

public class SelectAddressIntent extends Intent {
    private static final String DATA = "DATA";

    public SelectAddressIntent(Context context, LatLng latLng) {
        super(context, SelectAddressActivity.class);
        putExtra(DATA, latLng);
    }

    public SelectAddressIntent(Context context, SearchAddressResult searchAddressResult) {
        super(context, SelectAddressActivity.class);
        putExtra(DATA, searchAddressResult);
    }

    public SelectAddressIntent(LatLng latLng) {
        putExtra(DATA, latLng);
    }

    public SelectAddressIntent(Intent data) {
        super(data);
    }

    public SearchAddressResult getSearchAddressResult() {
        return getParcelableExtra(DATA);
    }

    public LatLng getLatLng() {
        return getParcelableExtra(DATA);
    }
}
