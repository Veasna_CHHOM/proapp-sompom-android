package com.sompom.tookitup.intent;

import android.content.Intent;

import com.sompom.tookitup.model.SearchAddressResult;
import com.sompom.tookitup.utils.SharedPrefUtils;

/**
 * Created by nuonveyo on 6/12/18.
 */

public class SelectAddressIntentResult extends Intent {
    public SelectAddressIntentResult(SearchAddressResult searchAddressResult) {
        putExtra(SharedPrefUtils.DATA, searchAddressResult);
    }

    public SelectAddressIntentResult(Intent data) {
        super(data);
    }

    public SearchAddressResult getResult() {
        return getParcelableExtra(SharedPrefUtils.DATA);
    }
}
