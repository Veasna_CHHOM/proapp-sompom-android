package com.sompom.tookitup.intent;

import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;

import com.sompom.tookitup.model.emun.DeeplinkType;

public class DeeplinkIntent extends Intent {
    private static final String ID = "id";
    private static final String TYPE = "type";
    private DeeplinkType mDeeplinkType = DeeplinkType.None;
    private String mId;

    public DeeplinkIntent(Intent o) {
        super(o);
    }

    public DeeplinkType getDeeplinkType() {
        init();
        return mDeeplinkType;
    }

    public String getId() {
        init();
        return mId;
    }

    private void init() {
        if (!TextUtils.isEmpty(mId)) {
            return;
        }
        Uri uri = getData();
        if (uri != null) {
            String type = uri.getQueryParameter(TYPE);
            mDeeplinkType = DeeplinkType.from(type);
            mId = uri.getQueryParameter(ID);
        }
    }


}
