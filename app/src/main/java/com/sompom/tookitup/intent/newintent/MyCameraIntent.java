package com.sompom.tookitup.intent.newintent;

import android.content.Context;

import com.desmond.squarecamera.helper.GalleryLoader;
import com.desmond.squarecamera.intent.CameraIntent;
import com.desmond.squarecamera.intent.CameraIntentBuilder;
import com.desmond.squarecamera.intent.CameraType;
import com.desmond.squarecamera.intent.GalleryType;
import com.desmond.squarecamera.model.MediaFile;
import com.desmond.squarecamera.widget.CameraTabLayout;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.emun.MediaType;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

/**
 * Created by He Rotha on 6/12/18.
 */
public class MyCameraIntent {

    private static final int CHAT_MAX_SELECTION = 6;
    private static final int TIMELINE_MAX_SELECTION = 25;
    private static final int ITEM_MAX_SELECTION = 10;

    private MyCameraIntent() {

    }

    public static CameraIntent newChatGalleryInstance(Context context) {
        CameraType cameraType = new CameraType()
                .addTab(CameraTabLayout.TabIndex.PHOTO, true);

        GalleryType galleryType = new GalleryType(GalleryLoader.LoaderType.PhotoOnly)
                .showCameraButton(cameraType)
                .numberSelection(CHAT_MAX_SELECTION);

        return new CameraIntentBuilder(context)
                .openType(galleryType).build();
    }

    public static CameraIntent newChatCameraInstance(Context context) {
        GalleryType galleryType = new GalleryType(GalleryLoader.LoaderType.PhotoOnly)
                .numberSelection(CHAT_MAX_SELECTION);

        CameraType cameraType = new CameraType()
                .showGallery(galleryType)
                .addTab(CameraTabLayout.TabIndex.PHOTO, true);

        return new CameraIntentBuilder(context)
                .openType(cameraType).build();
    }

    public static CameraIntent newCommentGalleryInstance(Context context) {
        CameraType cameraType = new CameraType()
                .addTab(CameraTabLayout.TabIndex.PHOTO, true);

        GalleryType galleryType = new GalleryType(GalleryLoader.LoaderType.PhotoOnly)
                .showCameraButton(cameraType)
                .numberSelection(1);

        return new CameraIntentBuilder(context)
                .openType(galleryType).build();
    }

    public static CameraIntent newCommentCameraInstance(Context context) {
        GalleryType galleryType = new GalleryType(GalleryLoader.LoaderType.PhotoOnly)
                .numberSelection(1);

        CameraType cameraType = new CameraType()
                .showGallery(galleryType)
                .addTab(CameraTabLayout.TabIndex.PHOTO, true);

        return new CameraIntentBuilder(context)
                .openType(cameraType).build();
    }

    public static CameraIntent newTimelineInstance(Context context, List<Media> media) {
        if (media != null && !media.isEmpty()) {
            CameraType cameraType = new CameraType()
                    .addTab(CameraTabLayout.TabIndex.LIVE, false)
                    .addTab(CameraTabLayout.TabIndex.PHOTO, true)
                    .addTab(CameraTabLayout.TabIndex.VIDEO, false);

            GalleryType galleryType = new GalleryType(GalleryLoader.LoaderType.PhotoVideo)
                    .selectFiles(convertTimelineMediaToMediaFile(media))
                    .showCameraButton(cameraType)
                    .numberSelection(TIMELINE_MAX_SELECTION);

            return new CameraIntentBuilder(context)
                    .openType(galleryType).build();
        } else {
            GalleryType galleryType = new GalleryType(GalleryLoader.LoaderType.PhotoVideo)
                    .numberSelection(TIMELINE_MAX_SELECTION);

            CameraType cameraType = new CameraType()
                    .showGallery(galleryType)
                    .addTab(CameraTabLayout.TabIndex.LIVE, false)
                    .addTab(CameraTabLayout.TabIndex.PHOTO, true)
                    .addTab(CameraTabLayout.TabIndex.VIDEO, false);


            return new CameraIntentBuilder(context)
                    .openType(cameraType).build();
        }
    }

    public static CameraIntent newLiveInstance(Context context) {
        GalleryType galleryType = new GalleryType(GalleryLoader.LoaderType.PhotoVideo)
                .numberSelection(TIMELINE_MAX_SELECTION);

        CameraType cameraType = new CameraType()
                .showGallery(galleryType)
                .addTab(CameraTabLayout.TabIndex.LIVE, true)
                .addTab(CameraTabLayout.TabIndex.PHOTO, false)
                .addTab(CameraTabLayout.TabIndex.VIDEO, false);

        return new CameraIntentBuilder(context)
                .openType(cameraType).build();

    }

    public static CameraIntent newProfileInstance(Context context) {
        GalleryType galleryType = new GalleryType(GalleryLoader.LoaderType.PhotoOnly)
                .numberSelection(1);

        CameraType cameraType = new CameraType()
                .addTab(CameraTabLayout.TabIndex.PHOTO, true)
                .showGallery(galleryType);

        return new CameraIntentBuilder(context)
                .openType(cameraType).build();

    }


    public static CameraIntent newProductInstance(Context context, List<Media> media) {
        if (media != null && !media.isEmpty()) {
            CameraType cameraType = new CameraType()
                    .showDescription(true)
                    .addTab(CameraTabLayout.TabIndex.PHOTO, true);

            GalleryType galleryType = new GalleryType(GalleryLoader.LoaderType.PhotoOnly)
                    .selectFiles(convertTimelineMediaToMediaFile(media))
                    .showCameraButton(cameraType)
                    .showDescription(true)
                    .numberSelection(ITEM_MAX_SELECTION);

            return new CameraIntentBuilder(context)
                    .openType(galleryType).build();
        } else {
            GalleryType galleryType = new GalleryType(GalleryLoader.LoaderType.PhotoOnly)
                    .showDescription(true)
                    .numberSelection(ITEM_MAX_SELECTION);

            CameraType cameraType = new CameraType()
                    .showGallery(galleryType)
                    .showDescription(true)
                    .addTab(CameraTabLayout.TabIndex.PHOTO, true);

            return new CameraIntentBuilder(context)
                    .openType(cameraType).build();
        }
    }

    private static List<MediaFile> convertTimelineMediaToMediaFile(@Nullable List<Media> productMedia) {
        List<MediaFile> list = new ArrayList<>();
        if (productMedia != null) {
            for (Media media : productMedia) {
                MediaFile mediaFile = new MediaFile();
                mediaFile.setPath(media.getUrl());
                mediaFile.setPosition(media.getIndex());
                if (media.getType() == MediaType.VIDEO) {
                    mediaFile.setType(com.desmond.squarecamera.model.MediaType.VIDEO);
                } else {
                    mediaFile.setType(com.desmond.squarecamera.model.MediaType.IMAGE);
                }
                mediaFile.setWidth(media.getWidth());
                mediaFile.setHeight(media.getHeight());
                list.add(mediaFile);
            }
        }
        return list;
    }

}
