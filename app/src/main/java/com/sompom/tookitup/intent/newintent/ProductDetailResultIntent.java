package com.sompom.tookitup.intent.newintent;

import android.content.Intent;

import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.utils.SharedPrefUtils;

/**
 * Created by nuonveyo on 6/6/18.
 */

public class ProductDetailResultIntent extends Intent {
    public ProductDetailResultIntent(Intent o) {
        super(o);
    }

    public ProductDetailResultIntent(Product product) {
        putExtra(SharedPrefUtils.PRODUCT, product);
    }

    public Product getProduct() {
        return getParcelableExtra(SharedPrefUtils.PRODUCT);
    }
}
