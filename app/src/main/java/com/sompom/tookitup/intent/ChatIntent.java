package com.sompom.tookitup.intent;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.ChatActivity;
import com.sompom.tookitup.utils.ConversationUtil;
import com.sompom.tookitup.utils.SharedPrefUtils;

/**
 * Created by nuonveyo on 5/4/18.
 */

public class ChatIntent extends Intent {
    private User mRecipient;
    private Product mProduct;
    private boolean mIsInitCall = false;
    private Conversation mConversation;

    public ChatIntent(Intent o) {
        super(o);
    }

    public ChatIntent(Context context, User user) {
        super(context, ChatActivity.class);
        putExtra(SharedPrefUtils.USER_ID, user);
    }

    public ChatIntent(Context context, Product product) {
        super(context, ChatActivity.class);
        putExtra(SharedPrefUtils.PRODUCT, product);
    }

    public ChatIntent(Context context, Chat chat) {
        super(context, ChatActivity.class);
        putExtra(SharedPrefUtils.PRODUCT, chat.getProduct());
        putExtra(SharedPrefUtils.USER_ID, chat.getSender());
    }

    public ChatIntent(Context context, Product product, User user) {
        super(context, ChatActivity.class);
        putExtra(SharedPrefUtils.PRODUCT, product);
        putExtra(SharedPrefUtils.USER_ID, user);
    }

    public ChatIntent(Context context, Conversation conversation) {
        super(context, ChatActivity.class);
        if (conversation.getProduct() != null && conversation.getProduct().getUser() == null) {
            if (TextUtils.equals(conversation.getSeller(), SharedPrefUtils.getUserId(context))) {
                conversation.getProduct().setUser(SharedPrefUtils.getUser(context));
            } else {
                conversation.getProduct().setUser(conversation.getRecipient(context));
            }
        }
        putExtra(SharedPrefUtils.DATA, conversation);
    }

    public void init(Context context) {
        mConversation = getParcelableExtra(SharedPrefUtils.DATA);
        if (mConversation != null) {
            mProduct = mConversation.getProduct();
            mRecipient = mConversation.getRecipient(context);
        } else {
            mProduct = getParcelableExtra(SharedPrefUtils.PRODUCT);
            mRecipient = getParcelableExtra(SharedPrefUtils.USER_ID);

            if (mRecipient == null && mProduct != null) {
                mRecipient = mProduct.getUser();
            }
        }

        mIsInitCall = true;
    }

    public Conversation getConversation() {
        return mConversation;
    }

    public User getRecipient() {
        if (!mIsInitCall) {
            throw new InitException();
        }
        return mRecipient;
    }

    public Product getProduct() {
        if (!mIsInitCall) {
            throw new InitException();
        }
        return mProduct;
    }

    public String getConversationId(Context context) {
        if (!mIsInitCall) {
            throw new InitException();
        }
        if (mProduct == null) {
            return ConversationUtil.getConversationId(mRecipient.getId(),
                    SharedPrefUtils.getUserId(context));
        } else {
            return ConversationUtil.getProductConversationId(mProduct.getId(),
                    SharedPrefUtils.getUserId(context),
                    mRecipient.getId());
        }
    }

    public static class InitException extends RuntimeException {
        public InitException() {
            super("need to call method init first");
        }
    }

}
