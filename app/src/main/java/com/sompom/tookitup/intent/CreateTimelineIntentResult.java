package com.sompom.tookitup.intent;

import android.content.Intent;

import com.sompom.tookitup.model.LifeStream;
import com.sompom.tookitup.utils.SharedPrefUtils;

/**
 * Created by nuonveyo
 * on 2/13/19.
 */

public class CreateTimelineIntentResult extends Intent {
    public CreateTimelineIntentResult(Intent o) {
        super(o);
    }

    public CreateTimelineIntentResult(LifeStream lifeStream) {
        putExtra(SharedPrefUtils.DATA, lifeStream);
    }

    public LifeStream getLifeStream() {
        return getParcelableExtra(SharedPrefUtils.DATA);
    }
}
