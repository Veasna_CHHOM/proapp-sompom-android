package com.sompom.tookitup.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.sompom.tookitup.newui.EditProfileActivity;

/**
 * Created by nuonveyo on 6/13/18.
 */

public class EditProfileIntent extends Intent {
    public EditProfileIntent(Context context) {
        super(context, EditProfileActivity.class);
    }
}
