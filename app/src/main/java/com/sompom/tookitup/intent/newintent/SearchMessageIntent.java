package com.sompom.tookitup.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.sompom.tookitup.newui.SearchMessageActivity;

/**
 * Created by nuonveyo on 7/2/18.
 */

public class SearchMessageIntent extends Intent {
    public SearchMessageIntent(Context context) {
        super(context, SearchMessageActivity.class);
    }
}
