package com.sompom.tookitup.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.newui.ProductDetailActivity;
import com.sompom.tookitup.utils.SharedPrefUtils;

/**
 * Created by nuonveyo on 6/6/18.
 */

public class ProductDetailIntent extends Intent {
    public ProductDetailIntent(Context packageContext,
                               Product product,
                               boolean isOpenComment,
                               boolean isFromNotification) {

        super(packageContext, ProductDetailActivity.class);
        putExtra(SharedPrefUtils.IS_ACTIVITY, isOpenComment);
        putExtra(SharedPrefUtils.PRODUCT, product);
        putExtra(SharedPrefUtils.IS_SHARE_FB, isFromNotification);
    }

    public ProductDetailIntent(Context packageContext,
                               String product,
                               boolean isOpenComment,
                               boolean isFromNotification) {

        super(packageContext, ProductDetailActivity.class);
        putExtra(SharedPrefUtils.IS_ACTIVITY, isOpenComment);
        putExtra(SharedPrefUtils.ID, product);
        putExtra(SharedPrefUtils.IS_SHARE_FB, isFromNotification);
    }

    public ProductDetailIntent(Intent o) {
        super(o);
    }

    public Product getProduct() {
        Product product = getParcelableExtra(SharedPrefUtils.PRODUCT);
        if (product == null) {
            product = new Product();
            product.setId(getStringExtra(SharedPrefUtils.ID));
        }
        return product;
    }

    public boolean isOpenComment() {
        return getBooleanExtra(SharedPrefUtils.IS_ACTIVITY, false);
    }

    public boolean isOpenFromNotification() {
        return getBooleanExtra(SharedPrefUtils.IS_SHARE_FB, false);
    }
}
