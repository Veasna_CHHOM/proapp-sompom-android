package com.sompom.tookitup.intent.newintent;

import android.content.Context;
import android.content.Intent;

import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.newui.HomeActivity;
import com.sompom.tookitup.utils.SharedPrefUtils;

/**
 * Created by He Rotha on 6/21/18.
 */
public class HomeIntent extends Intent {
    public HomeIntent(Context packageContext) {
        this(packageContext, Redirection.None);
    }

    public HomeIntent(Intent o) {
        super(o);
    }

    private HomeIntent(Context packageContext, Redirection redirection) {
        super(packageContext, HomeActivity.class);
        putExtra(SharedPrefUtils.ID, redirection);
    }

    public static HomeIntent openConversation(Context context, Chat chat) {
        HomeIntent intent = new HomeIntent(context, Redirection.Conversation);
        intent.putExtra(SharedPrefUtils.DATA, chat);
        return intent;
    }

    public Redirection getRedirection() {
        return (Redirection) getSerializableExtra(SharedPrefUtils.ID);
    }

    public Chat getChat() {
        return getParcelableExtra(SharedPrefUtils.DATA);
    }

    public enum Redirection {
        None, Conversation
    }
}
