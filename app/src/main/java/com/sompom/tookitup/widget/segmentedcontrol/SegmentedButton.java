package com.sompom.tookitup.widget.segmentedcontrol;

import android.annotation.TargetApi;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;

import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.LayoutSegmentedButtonBinding;

/**
 * Created by nuonveyo on 6/26/18.
 */

public class SegmentedButton extends LinearLayout implements ViewTreeObserver.OnGlobalLayoutListener {
    private LayoutSegmentedButtonBinding mBinding;
    @ColorInt
    private int mSelectedTextColor;
    @ColorInt
    private int mUnSelectedTextColor;
    @DrawableRes
    private int mSelectedBackground;
    @DrawableRes
    private int mUnSelectedBackground;
    private OnSegmentedButtonClick mSegmentedButtonClick;
    private boolean mIsDisableSetLayerType;

    public SegmentedButton(Context context) {
        super(context);
        init();
    }

    public SegmentedButton(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SegmentedButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SegmentedButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void setSelectedTextColor(int selectedTextColor) {
        mSelectedTextColor = selectedTextColor;
    }

    public void setUnSelectedTextColor(int unSelectedTextColor) {
        mUnSelectedTextColor = unSelectedTextColor;
    }

    public void setSelectedBackground(int selectedBackground) {
        mSelectedBackground = selectedBackground;
    }

    public void setUnSelectedBackground(int unSelectedBackground) {
        mUnSelectedBackground = unSelectedBackground;
    }

    public void setDisableSetLayerType(boolean disableSetLayerType) {
        mIsDisableSetLayerType = disableSetLayerType;
    }

    public void setIsSelected(boolean isSelected) {
        if (isSelected) {
            Typeface typefaceBold = ResourcesCompat.getFont(getContext(), R.font.sf_pro_bold);
            mBinding.textView.setTypeface(typefaceBold);
            mBinding.textView.setTextColor(mSelectedTextColor);
            setBackground(ContextCompat.getDrawable(getContext(), mSelectedBackground));
            if (!mIsDisableSetLayerType) {
                setLayerType(View.LAYER_TYPE_HARDWARE, null);
            }
        } else {
            Typeface typefaceBold = ResourcesCompat.getFont(getContext(), R.font.sf_pro_light);
            mBinding.textView.setTypeface(typefaceBold);
            mBinding.textView.setTextColor(mUnSelectedTextColor);
            setBackground(ContextCompat.getDrawable(getContext(), mUnSelectedBackground));
            if (!mIsDisableSetLayerType) {
                setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            }
        }
    }

    public void setTitle(@StringRes int title) {
        mBinding.textView.setText(title);
    }

    public void setOnSegmentedButtonClick(OnSegmentedButtonClick onSegmentedButtonClick) {
        mSegmentedButtonClick = onSegmentedButtonClick;
    }

    private void init() {
        getViewTreeObserver().addOnGlobalLayoutListener(this);

        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.layout_segmented_button,
                this,
                true);
        setOnClickListener(v -> {
            if (mSegmentedButtonClick != null) {
                mSegmentedButtonClick.onClick(v);
            }
        });
    }

    @Override
    public void onGlobalLayout() {
        int width = getWidth();
        if (width > 0) {
            getLayoutParams().width = (int) (width * 1.1);
            requestLayout();
        }
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }

    public interface OnSegmentedButtonClick {
        void onClick(View view);
    }
}
