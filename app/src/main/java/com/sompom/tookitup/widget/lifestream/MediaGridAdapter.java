package com.sompom.tookitup.widget.lifestream;

import android.view.View;

/**
 * Created by He Rotha on 8/25/17.
 */

public abstract class MediaGridAdapter {
    public abstract View getView(int position);

    public abstract int getItemCount();

    public abstract void onBind(View view, int position);

    public abstract void onRemainCount(View lastView, int remainCount);
}
