package com.sompom.tookitup.widget;

import android.content.Context;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import org.lucasr.twowayview.widget.SpannableGridLayoutManager;

/**
 * Created by he.rotha on 9/6/16.
 */
public class TwoWayLoadMoreLayoutManager extends SpannableGridLayoutManager {
    private static final String TAG = TwoWayLoadMoreLayoutManager.class.getName();
    private static final float MILLISECONDS_PER_INCH = 50f;
    private int mLoadMoreBeforeEnd;
    private boolean mIsLoadingMore = false;
    private boolean mShouldCheckPastVisibleItems = true;
    private OnLoadMoreCallback mOnLoadMoreListener;
    private LinearSmoothScroller mLinearSmoothScroller;

    public TwoWayLoadMoreLayoutManager(Context context) {
        super(context);
    }

    public TwoWayLoadMoreLayoutManager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TwoWayLoadMoreLayoutManager(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public int scrollVerticallyBy(final int dy, final RecyclerView.Recycler recycler, final RecyclerView.State state) {
        if (mOnLoadMoreListener != null) {
            int mVisibleItemCount = this.getChildCount();
            int mTotalItemCount = this.getItemCount();
            int mPastVisibleItems = this.getFirstVisiblePosition();
            if (!mIsLoadingMore) {
                if ((mVisibleItemCount + mPastVisibleItems) >= mTotalItemCount - mLoadMoreBeforeEnd) {
                    if (mShouldCheckPastVisibleItems) {
                        if (mPastVisibleItems != 0) {
                            mIsLoadingMore = true;
                            mOnLoadMoreListener.onLoadMoreFromBottom();
                        }
                    } else {
                        mIsLoadingMore = true;
                        mOnLoadMoreListener.onLoadMoreFromBottom();
                    }
                }
            }
        }

        return super.scrollVerticallyBy(dy, recycler, state);
    }

    public boolean isShouldCheckPastVisibleItems() {
        return mShouldCheckPastVisibleItems;
    }

    public void setShouldCheckPastVisibleItems(boolean shouldCheckPastVisibleItems) {
        mShouldCheckPastVisibleItems = shouldCheckPastVisibleItems;
    }

    public int getLoadMoreBeforeEnd() {
        return mLoadMoreBeforeEnd;
    }

    public void setLoadMoreBeforeEnd(final int loadMoreBeforeEnd) {
        mLoadMoreBeforeEnd = loadMoreBeforeEnd;
    }

    public void setOnLoadMoreListener(final OnLoadMoreCallback onLoadMoreListener) {
        mOnLoadMoreListener = onLoadMoreListener;
    }

    public void loadingFinished() {
        mIsLoadingMore = false;
    }

    @Override
    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
        super.smoothScrollToPosition(recyclerView, state, position);
//        if (mLinearSmoothScroller == null) {
//            mLinearSmoothScroller = new LinearSmoothScroller(recyclerView.getContext()) {
//                @Override
//                public PointF computeScrollVectorForPosition(int targetPosition) {
//                    return LoaderMoreLayoutManager.this
//                        .computeScrollVectorForPosition(targetPosition);
//                }
//
//                @Override
//                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
//                    return MILLISECONDS_PER_INCH / displayMetrics.densityDpi;
//                }
//            };
//        }
//        mLinearSmoothScroller.setTargetPosition(position);
//        startSmoothScroll(mLinearSmoothScroller);
    }

    public interface OnLoadMoreCallback {
        void onLoadMoreFromBottom();
    }

//    public int findFirstVisibleItemPosition() {
//        final View child = findOneVisibleChild(0, getChildCount(), false, true);
//        return child == null ? NO_POSITION : getPosition(child);
//    }

//    View findOneVisibleChild(int fromIndex, int toIndex, boolean completelyVisible,
//                             boolean acceptPartiallyVisible) {
//        ensureLayoutState();
//        final int start = mOrientationHelper.getStartAfterPadding();
//        final int end = mOrientationHelper.getEndAfterPadding();
//        final int next = toIndex > fromIndex ? 1 : -1;
//        View partiallyVisible = null;
//        for (int i = fromIndex; i != toIndex; i+=next) {
//            final View child = getChildAt(i);
//            final int childStart = mOrientationHelper.getDecoratedStart(child);
//            final int childEnd = mOrientationHelper.getDecoratedEnd(child);
//            if (childStart < end && childEnd > start) {
//                if (completelyVisible) {
//                    if (childStart >= start && childEnd <= end) {
//                        return child;
//                    } else if (acceptPartiallyVisible && partiallyVisible == null) {
//                        partiallyVisible = child;
//                    }
//                } else {
//                    return child;
//                }
//            }
//        }
//
//        return partiallyVisible;
//    }
//
//    void ensureLayoutState() {
//        if (mLayoutState == null) {
//            mLayoutState = createLayoutState();
//        }
//        if (mOrientationHelper == null) {
//            mOrientationHelper = OrientationHelper.createOrientationHelper(this, mOrientation);
//        }
//    }
}
