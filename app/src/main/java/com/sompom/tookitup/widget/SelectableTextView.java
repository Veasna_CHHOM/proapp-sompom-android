package com.sompom.tookitup.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.Gravity;

import com.sompom.tookitup.R;

/**
 * Created by ChhomVeasna on 11/28/16.
 */

public class SelectableTextView extends AppCompatTextView {
    private int mSelectedBackgroundRes;

    public SelectableTextView(Context context) {
        super(context);
    }

    public SelectableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initComponent(context, attrs);
    }

    public SelectableTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initComponent(context, attrs);
    }

    private void initComponent(Context ctx, AttributeSet attrs) {
        //Default selectable background resource
        mSelectedBackgroundRes = R.drawable.round_orange_stroke_background;
        setTextColor(ContextCompat.getColor(ctx, R.color.colorPrimaryDark));
        setTextSize(0, ctx.getResources().getDimensionPixelSize(R.dimen.text_small));
        setMinWidth(ctx.getResources().getDimensionPixelSize(R.dimen.selectable_min_width));
        setHeight(ctx.getResources().getDimensionPixelSize(R.dimen.selectable_min_height));
        setGravity(Gravity.CENTER);

        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.Sompom);
        boolean enableBackground = false;
        if (a.hasValue(R.styleable.Sompom_enableBackground)) {
            enableBackground = a.getBoolean(R.styleable.Sompom_enableBackground, false);
        }

        if (enableBackground) {
            setBackgroundResource(mSelectedBackgroundRes);
            setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
        } else {
            setBackgroundResource(R.drawable.transparent_background);
            setTextColor(ContextCompat.getColor(getContext(), R.color.darkGrey));
        }

        a.recycle();
    }

    public void setSelectedBackgroundRes(int selectedBackgroundRes) {
        mSelectedBackgroundRes = selectedBackgroundRes;
        setBackgroundResource(mSelectedBackgroundRes);
    }

    public void setSelected(boolean isSelected) {
        if (isSelected) {
            setBackgroundResource(mSelectedBackgroundRes);
            setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
        } else {
            setBackgroundResource(R.drawable.transparent_background);
            setTextColor(ContextCompat.getColor(getContext(), R.color.darkGrey));
        }
    }
}
