package com.sompom.tookitup.widget.actionbutton;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.LayoutActionButtonBinding;
import com.sompom.tookitup.model.emun.ActionButtonItem;
import com.sompom.tookitup.utils.AttributeConverter;

/**
 * Created by nuonveyo on 8/1/18.
 */

public class ActionButtonView extends LinearLayout {
    private LayoutActionButtonBinding mBinding;

    public ActionButtonView(Context context) {
        super(context);
        init();
    }

    public ActionButtonView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ActionButtonView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ActionButtonView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }


    private void init() {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.layout_action_button,
                this,
                true);
    }

    public void setData(ActionButtonItem item) {
        mBinding.titleTextView.setText(item.getTitle());
        mBinding.iconTextView.setText(item.getIcon());
    }

    public void setSelected(boolean isSelected) {
        if (isSelected) {
            Typeface typefaceBold = ResourcesCompat.getFont(getContext(), R.font.sf_pro_semibold);
            mBinding.titleTextView.setTypeface(typefaceBold);
            mBinding.titleTextView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
            mBinding.iconTextView.setTextColor(Color.WHITE);
            mBinding.iconTextView.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.oval_orange));
        } else {
            Typeface typeface = ResourcesCompat.getFont(getContext(), R.font.sf_pro_regular);
            mBinding.titleTextView.setTypeface(typeface);
            int color = AttributeConverter.convertAttrToColor(getContext(), R.attr.colorFirstText);
            mBinding.titleTextView.setTextColor(color);
            mBinding.iconTextView.setTextColor(color);
            mBinding.iconTextView.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.oval_grey));
        }
    }
}
