package com.sompom.tookitup.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

import com.sompom.tookitup.adapter.newadapter.StoreViewPagerAdapter;
import com.sompom.tookitup.newui.HomeActivity;
import com.sompom.tookitup.newui.fragment.AbsBaseFragment;
import com.sompom.tookitup.newui.fragment.MessageFragment;
import com.sompom.tookitup.newui.fragment.UserListFragment;
import com.sompom.tookitup.newui.fragment.WallStreetFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by He Rotha on 9/20/18.
 */
public class HomeViewPager extends ViewPager {
    private final List<AbsBaseFragment> mFragments = new ArrayList<>();

    public HomeViewPager(@NonNull Context context) {
        super(context);
    }

    public HomeViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public void setupAdapter(Context context,
                             FragmentManager manager,
                             com.sompom.tookitup.listener.OnClickListener callback,
                             boolean isLoadMessageData,
                             HomeTabLayout homeTabLayout) {
        mFragments.clear();

        //Wallstreet tap
        mFragments.add(WallStreetFragment.newInstance());

        //Hide near by map feature
//        NearbyMapFragment fragment = NearbyMapFragment.newInstance();
//        fragment.setOnClickListener(callback);
//        mFragments.add(fragment);
        //Add user list feature instead of nearly map
        //Map tap

        //Chat tap
        mFragments.add(MessageFragment.newInstance(isLoadMessageData));

        //User list tap
        UserListFragment fragment = UserListFragment.newInstance();
        mFragments.add(fragment);

        //Notification tap
//        NotificationFragment notificationFragment = NotificationFragment.newInstance();
//        notificationFragment.setOnUpdateBadgeValueListener(homeTabLayout);
//        mFragments.add(notificationFragment);

        StoreViewPagerAdapter adapter = new StoreViewPagerAdapter(manager, mFragments);
        setAdapter(adapter);
        setOffscreenPageLimit(adapter.getCount());
        if (isLoadMessageData) {
            setCurrentItem(2);
            if (context instanceof HomeActivity) {
                ((HomeActivity) context).shouldOpenConversation();
            }
        }
    }

    public AbsBaseFragment getCurrentFragment() {
        return mFragments.get(getCurrentItem());
    }

    public AbsBaseFragment getFragmentAt(int position) {
        return mFragments.get(position);
    }
}
