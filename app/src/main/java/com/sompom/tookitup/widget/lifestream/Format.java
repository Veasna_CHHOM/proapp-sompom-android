package com.sompom.tookitup.widget.lifestream;

/**
 * Created by He Rotha on 7/10/18.
 */
public enum Format {
    FreeStyle(1, 5),
    SquareAll(2, 5),
    SquareStart(3, 5),
    Three(4, 3);

    private int mValue;
    private int mMaxDisplay;

    Format(int value, int maxDisplay) {
        mValue = value;
        mMaxDisplay = maxDisplay;
    }

    public static Format fromValue(int value) {
        for (Format format : Format.values()) {
            if (format.mValue == value) {
                return format;
            }
        }
        return FreeStyle;
    }

    public int getValue() {
        return mValue;
    }

    public int getMaxDisplay() {
        return mMaxDisplay;
    }
}
