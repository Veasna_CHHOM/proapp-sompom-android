package com.sompom.tookitup.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.LayoutOverlapImageContainerBinding;
import com.sompom.tookitup.model.SupportingResource;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.viewmodel.binding.ImageViewBindingUtil;

import java.util.List;

/**
 * Created by nuonveyo on 7/13/18.
 */

public class OverlapImageContainer extends LinearLayout {
    public static final int DEFAULT_DISPLAY = 4;
    private int mLimit = DEFAULT_DISPLAY;
    private ViewGroup[] mViewGroups;
    private ImageView[] mImageViews;

    private LayoutOverlapImageContainerBinding mBinding;

    public OverlapImageContainer(Context context) {
        super(context);
        init(null);
    }

    public OverlapImageContainer(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public OverlapImageContainer(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public OverlapImageContainer(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    private void init(AttributeSet attributeSet) {
        setOrientation(HORIZONTAL);
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.layout_overlap_image_container,
                this,
                true);
        if (attributeSet != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attributeSet,
                    R.styleable.OverlapImageContainer,
                    0,
                    0);
            mLimit = typedArray.getInteger(R.styleable.OverlapImageContainer_limit, DEFAULT_DISPLAY);
            if (mLimit > DEFAULT_DISPLAY) {
                mLimit = DEFAULT_DISPLAY;
            }
            typedArray.recycle();
        }

        mViewGroups = new ViewGroup[]{
                mBinding.container1,
                mBinding.container2,
                mBinding.container3,
                mBinding.container4,
                mBinding.container5
        };
        mImageViews = new ImageView[]{
                mBinding.imageView1,
                mBinding.imageView2,
                mBinding.imageView3,
                mBinding.imageView4,
                mBinding.imageView5
        };
    }

    public void addItem(List<User> list) {
        mBinding.moreTextView.setVisibility(GONE);

        if (list != null && !list.isEmpty()) {
            setVisibility(VISIBLE);
            for (int i = 0; i < list.size(); i++) {
                if (i > mLimit) {
                    String more = "+" + (list.size() - mLimit);
                    mBinding.moreTextView.setText(more);
                    mBinding.moreTextView.setVisibility(VISIBLE);
                    mImageViews[mLimit].setVisibility(GONE);
                    break;
                } else {
                    ImageViewBindingUtil.setUserUrl(mImageViews[i], list.get(i));
                    mViewGroups[i].setVisibility(VISIBLE);
                }
            }
            if (list.size() < mImageViews.length) {
                for (int i = list.size(); i < mImageViews.length; i++) {
                    mViewGroups[i].setVisibility(GONE);
                }
            }
        } else {
            setVisibility(GONE);
        }
    }

    public void addBitMapItem(List<SupportingResource> list, int totalImage) {
        mBinding.moreTextView.setVisibility(GONE);

        if (list != null && !list.isEmpty()) {
            for (int i = 0; i < mImageViews.length; i++) {
                if (i >= list.size() && totalImage < mLimit) {
                    mViewGroups[i].setVisibility(GONE);
                } else {
                    if (i >= mLimit && totalImage > mLimit + 1) {

                        String more = "+" + (totalImage - mLimit);
                        mBinding.moreTextView.setText(more);
                        mBinding.moreTextView.setVisibility(VISIBLE);
                        mImageViews[i].setVisibility(GONE);
                    } else {

                        if (list.get(i).getBitmap() == null) {
                            mImageViews[i].setImageDrawable(list.get(i).getDrawable());
                        } else {
                            mImageViews[i].setImageBitmap(list.get(i).getBitmap());
                        }
                        mImageViews[i].setVisibility(VISIBLE);
                        mViewGroups[i].setVisibility(VISIBLE);
                    }
                }
            }
        }
    }

}
