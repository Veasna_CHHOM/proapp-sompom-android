package com.sompom.tookitup.widget;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by he.rotha on 3/7/16.
 */
public class LoadMoreGridLayoutManager extends GridLayoutManager {
    private int mLoadMoreBeforeEnd;
    private boolean mIsLoadingMore = false;
    private OnLoadMoreCallback mOnLoadMoreListener;

    public LoadMoreGridLayoutManager(Context context, int spanCount) {
        super(context, spanCount);
    }

    public int getLoadMoreBeforeEnd() {
        return mLoadMoreBeforeEnd;
    }

    public void setLoadMoreBeforeEnd(final int loadMoreBeforeEnd) {
        mLoadMoreBeforeEnd = loadMoreBeforeEnd;
    }

    public void setOnLoadMoreListener(final OnLoadMoreCallback onLoadMoreListener) {
        mOnLoadMoreListener = onLoadMoreListener;
    }

    public void loadingFinished() {
        mIsLoadingMore = false;
    }

    @Override
    public int scrollVerticallyBy(final int dy, final RecyclerView.Recycler recycler, final RecyclerView.State state) {
        if (mOnLoadMoreListener != null) {

            int mVisibleItemCount = this.getChildCount();
            int mTotalItemCount = this.getItemCount();
            int mPastVisibleItems = this.findFirstVisibleItemPosition();
            if (!mIsLoadingMore) {
                if ((mVisibleItemCount + mPastVisibleItems) >= mTotalItemCount - mLoadMoreBeforeEnd) {
//                    if (mPastVisibleItems != 0) {
                    mIsLoadingMore = true;
                    mOnLoadMoreListener.onLoadMoreFromBottom();
//                    }
                }
            }
        }
        return super.scrollVerticallyBy(dy, recycler, state);
    }

    public interface OnLoadMoreCallback {
        void onLoadMoreFromBottom();
    }
}
