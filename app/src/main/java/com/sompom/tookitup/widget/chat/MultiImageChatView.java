package com.sompom.tookitup.widget.chat;

import android.content.Context;
import android.support.v7.widget.GridLayout;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.OnItemClickListener;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.emun.ChatBg;
import com.sompom.tookitup.model.emun.MultiChatImageItemType;
import com.sompom.tookitup.utils.DrawChatImageRoundedUtil;
import com.sompom.tookitup.utils.GlideLoadUtil;
import com.sompom.tookitup.widget.RoundedImageView;

import java.util.List;

/**
 * Created by nuonveyo on 8/29/18.
 */

public class MultiImageChatView extends GridLayout {

    public MultiImageChatView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public MultiImageChatView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MultiImageChatView(Context context) {
        super(context);
    }

    public void generateLayout(List<Media> mediaList,
                               OnItemClickListener<Integer> onItemClickListener,
                               OnLongClickListener onLongClickListener,
                               ChatBg chatBg) {
        removeAllViews();

        MultiChatImageItemType colItem = MultiChatImageItemType.getMultiImageType(mediaList.size());
        int colCount = colItem.getCol();
        int rowCount = mediaList.size() / colCount;

        setColumnCount(colCount);
        setRowCount(rowCount + 1);

        if (mediaList.size() % colCount > 0) {
            rowCount += 1;
        }

        int imageSize = getImageSize(colItem);
        int marginRight = getContext().getResources().getDimensionPixelSize(R.dimen.space_tiny);
        int marginTop = getContext().getResources().getDimensionPixelSize(R.dimen.space_too_small);
        for (int i = 0, column = 0, row = 0; i < mediaList.size(); i++, column++) {
            if (column == colItem.getCol()) {
                column = 0;
                row++;
            }

            if (colItem == MultiChatImageItemType.OneCol) {
                RoundedImageView imageView = new RoundedImageView(getContext());
                imageView.setCircle(false);
                imageView.setCornerType(DrawChatImageRoundedUtil.getCornerType(chatBg));
                imageView.setScaleType(ImageView.ScaleType.CENTER);
                Media media = mediaList.get(i);


                int finalI = i;
                imageView.setOnClickListener(v -> onItemClickListener.onClick(finalI));
                imageView.setOnLongClickListener(view -> {
                    onLongClickListener.onLongClickAtPosition(media);
                    return false;
                });
                imageView.setTag(R.id.action_chat, media.getUrl());
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                imageView.setAdjustViewBounds(true);

                GridLayout.LayoutParams param = getLayoutParam(LayoutParams.WRAP_CONTENT, column, row);
                imageView.setLayoutParams(param);

                Orientation orientation = Orientation.getOrientation(media);
                if (orientation == Orientation.SQUARE) {
                    int size = getContext().getResources().getDimensionPixelSize(R.dimen.chat_media_width);
                    param.width = size;
                    param.height = size;
                } else if (orientation == Orientation.ORIENTATION) {
                    int maxWidth = getContext().getResources().getDimensionPixelSize(R.dimen.chat_media_width);
                    int maxHeight = maxWidth * media.getHeight() / media.getWidth();
                    param.width = maxWidth;
                    param.height = maxHeight;

                } else if (orientation == Orientation.PORTRAIT) {
                    int maxHeight = getContext().getResources().getDimensionPixelOffset(R.dimen.chat_media_height);
                    param.width = maxHeight * media.getWidth() / media.getHeight();
                    param.height = maxHeight;

                } else {
                    int size = getContext().getResources().getDimensionPixelSize(R.dimen.chat_media_width);
                    imageView.setMaxWidth(size);
                    imageView.setMaxHeight(size);
                }

                addView(imageView);

                GlideLoadUtil.setChatMultiImageView(imageView,
                        media.getUrl(),
                        600);

            } else {
                RoundedImageView imageView = new RoundedImageView(getContext());
                imageView.setCircle(false);
                imageView.setScaleCenterCrop(true);
                imageView.setCornerType(DrawChatImageRoundedUtil.getCornerType(chatBg,
                        rowCount,
                        colCount,
                        row,
                        column,
                        i,
                        mediaList.size()));

                GlideLoadUtil.setChatMultiImageView(imageView,
                        mediaList.get(i).getUrl(),
                        300);

                int finalI1 = i;
                imageView.setOnClickListener(v -> onItemClickListener.onClick(finalI1));
                imageView.setOnLongClickListener(view -> {
                    onLongClickListener.onLongClickAtPosition(mediaList.get(finalI1));
                    return false;
                });

                GridLayout.LayoutParams param = getLayoutParam(imageSize, column, row);
                param.rightMargin = marginRight;
                param.topMargin = marginTop;
                imageView.setLayoutParams(param);
                imageView.setTag(R.id.action_chat, mediaList.get(i).getUrl());
                addView(imageView);
            }
        }
    }

    private int getImageSize(MultiChatImageItemType colItem) {
        int imageSize = getContext().getResources().getDimensionPixelSize(R.dimen.chat_multi_image_size);
        if (colItem == MultiChatImageItemType.TwoCol) {
            imageSize = imageSize * MultiChatImageItemType.ThreeCol.getCol() / 2;
        }
        return imageSize;
    }

    private GridLayout.LayoutParams getLayoutParam(int size, int column, int row) {
        GridLayout.LayoutParams param = new GridLayout.LayoutParams();
        param.width = size;
        param.height = size;
        param.columnSpec = GridLayout.spec(column);
        param.rowSpec = GridLayout.spec(row);
        return param;
    }

    private enum Orientation {
        UNDEFINE,
        SQUARE,
        ORIENTATION,
        PORTRAIT;

        static Orientation getOrientation(Media media) {
            if (media.getWidth() > 0 && media.getHeight() > 0) {
                if (media.getWidth() == media.getHeight()) {
                    return SQUARE;
                } else if (media.getWidth() > media.getHeight()) {
                    return ORIENTATION;
                } else {
                    return PORTRAIT;
                }
            }
            return UNDEFINE;
        }
    }

    public interface OnLongClickListener {
        void onLongClickAtPosition(Media media);
    }
}
