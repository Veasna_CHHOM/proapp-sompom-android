package com.sompom.tookitup.widget.chat;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.chat.PlayAudioService;
import com.sompom.tookitup.databinding.LayoutChatMediaAudioBinding;
import com.sompom.tookitup.listener.PlayAudioServiceListener;
import com.sompom.tookitup.model.emun.ChatBg;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.AbsSoundControllerActivity;
import com.sompom.tookitup.utils.DateTimeUtils;
import com.sompom.tookitup.utils.ToastUtil;

/**
 * Created by nuonveyo on 9/19/18.
 */

public class ChatMediaAudioLayout extends LinearLayout {
    private LayoutChatMediaAudioBinding mBinding;
    private PlayAudioService mPlayAudioService;
    private Chat mChat;
    private IconType mIconType;
    private long mDuration;
    private Activity mActivity;
    private boolean mIsMe;
    private final PlayAudioServiceListener mPlayAudioServiceListener = new PlayAudioServiceListener() {

        @Override
        public void onPause(String currentId) {
            mIconType = IconType.PAUSE;
            setIcon();
        }

        @Override
        public void onPlaying(String currentId, long currentDuration) {
            if (isLoading()) {
                setShowLoading(false);
            }
            if (mIconType == IconType.PAUSE) {
                mIconType = IconType.PLAYING;
                setIcon();
            }
            long playingDuration = mDuration - currentDuration;
            mBinding.textViewDuration.setText(DateTimeUtils.getAudioTime(playingDuration));
            mBinding.progressBar.setProgress((int) currentDuration);
        }

        @Override
        public void onFinish() {
            removeAudioPlayListener();
            if (isLoading()) {
                setShowLoading(false);
            }
            mBinding.textViewDuration.setText(DateTimeUtils.getAudioTime(mDuration));
            mIconType = IconType.PAUSE;
            ValueAnimator valueAnimator = ValueAnimator.ofInt(mBinding.progressBar.getProgress(), 0);
            valueAnimator.setDuration(300);
            valueAnimator.addUpdateListener(valueAnimator1 -> mBinding.progressBar.setProgress((Integer) valueAnimator1.getAnimatedValue()));
            valueAnimator.start();
            setIcon();
        }

        @Override
        public void onResume(String currentId) {
            mIconType = IconType.PLAYING;
            setIcon();
        }

        @Override
        public void onError() {
            ToastUtil.showToast(mActivity,
                    mActivity.getString(R.string.chat_play_audio_error_message));
            setShowLoading(false);
        }

        @Override
        public void hideLoading(String currentId) {
            if (TextUtils.equals(currentId, mChat.getId())) {
                setShowLoading(false);
            }
        }
    };
    private com.sompom.tookitup.listener.OnClickListener mOnLongClickListener;

    public ChatMediaAudioLayout(Context context) {
        super(context);
        init();
    }

    public ChatMediaAudioLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ChatMediaAudioLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public ChatMediaAudioLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void setActivity(Activity activity) {
        mActivity = activity;
    }

    public void setChat(Chat chat, boolean isMe, com.sompom.tookitup.listener.OnClickListener listener) {
        mChat = chat;
        mIsMe = isMe;
        mDuration = chat.getMediaList().get(0).getDuration();
        mOnLongClickListener = listener;

        mIconType = IconType.PAUSE;
        mBinding.textViewDuration.setText(DateTimeUtils.getAudioTime(mDuration));
        mBinding.progressBar.setMax((int) mDuration);

        if (mIsMe) {
            setLoadingColor(R.color.colorAccentDark);
            updateColorResource(R.drawable.green_progress_drawable,
                    R.color.white,
                    R.drawable.corner_white_background,
                    R.drawable.oval_white_bg_green_stroke,
                    R.color.colorAccentDark);
        } else {
            setLoadingColor(R.color.chatAudio);
            updateColorResource(R.drawable.grey_progress_drawable,
                    R.color.white,
                    R.drawable.corner_grey_background,
                    R.drawable.oval_white_bg_grey_stroke,
                    R.color.chatAudio);
        }

        /*
          Remove and than set listener again when this item is recycled while scrolling,
          that be able to set progress and duration working properly.
         */
        if (mActivity instanceof AbsBaseActivity) {
            mPlayAudioService = getPlayAudioService();
            if (mPlayAudioService != null
                    && (mPlayAudioService.isPlaying() || mPlayAudioService.isPause())
                    && TextUtils.equals(mChat.getId(), mPlayAudioService.getCurrentAudioId())) {

                if (mPlayAudioService.isPause() && mPlayAudioService.getCurrentPauseDuration() > 0) {
                    long playingDuration = mDuration - mPlayAudioService.getCurrentPauseDuration();
                    mBinding.textViewDuration.setText(DateTimeUtils.getAudioTime(playingDuration));
                    mBinding.progressBar.setProgress((int) mPlayAudioService.getCurrentPauseDuration());
                }

                removeAudioPlayListener();
                mPlayAudioService = getPlayAudioService();
                if (mPlayAudioService != null) {
                    ((AbsSoundControllerActivity) mActivity).setPlayAudioServiceListener(mPlayAudioServiceListener);
                }
            }
        }
    }

    private void updateColorResource(@DrawableRes int drawable,
                                     @ColorRes int durationTextColor,
                                     @DrawableRes int durationBg,
                                     @DrawableRes int playBg,
                                     @ColorRes int indicatorBg) {
        mBinding.progressBar.setProgressDrawable(ContextCompat.getDrawable(mActivity, drawable));
        mBinding.textViewDuration.setTextColor(ContextCompat.getColor(mActivity, durationTextColor));
        mBinding.textViewDuration.setBackground(ContextCompat.getDrawable(mActivity, durationBg));
        mBinding.containerPlayButton.setBackground(ContextCompat.getDrawable(mActivity, playBg));
        mBinding.imageViewIcon.setImageDrawable(ContextCompat.getDrawable(mActivity, getIconPlay()));
        mBinding.backgroundOfLine.setBackground(ContextCompat.getDrawable(mActivity, indicatorBg));
    }

    public void setBackgroundVoice(ChatBg drawable) {
        if (drawable == null) {
            return;
        }
        mBinding.layoutRoot.setBackground(ContextCompat.getDrawable(getContext(), drawable.getDrawable(false)));
    }

    private void setLoadingColor(@ColorRes int colorRes) {
        mBinding.loading.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(mActivity, colorRes),
                PorterDuff.Mode.SRC_IN);
    }

    private int getIconPlay() {
        if (mIsMe) return mIconType.getIcon();
        else return mIconType.getIconRecipient();
    }

    private void init() {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.layout_chat_media_audio,
                this,
                true);
        mBinding.getRoot().setOnClickListener(v -> {
            if (mActivity instanceof AbsBaseActivity) {
                mPlayAudioService = getPlayAudioService();
                if (mPlayAudioService != null) {
                    // Click on current playing item
                    if (TextUtils.equals(mChat.getId(), mPlayAudioService.getCurrentAudioId())) {
                        if (mPlayAudioService.isPlaying()) {
                            mPlayAudioService.pause();
                        } else if (mPlayAudioService.isPause()) {
                            mPlayAudioService.resume();
                        } else {
                            mPlayAudioService.checkToHideLoading();
                            setShowLoading(true);
                            ((AbsSoundControllerActivity) mActivity).setPlayAudioServiceListener(mPlayAudioServiceListener);
                            PlayAudioService.startService(mActivity, mChat, true);
                        }
                    } else {
                        mPlayAudioService.checkToHideLoading();
                        setShowLoading(true);
                        if (mPlayAudioService.isPlaying() || mPlayAudioService.isPause()) {
                            mPlayAudioService.stop();
                        }
                        ((AbsSoundControllerActivity) mActivity).setPlayAudioServiceListener(mPlayAudioServiceListener);
                        PlayAudioService.startService(mActivity, mChat, true);
                    }
                }
            }
        });

        mBinding.getRoot().setOnLongClickListener(v -> {
            if (mOnLongClickListener != null) {
                if (mPlayAudioService != null && mPlayAudioService.isPlaying()) {
                    mPlayAudioService.pause();
                }
                mOnLongClickListener.onClick();
            }
            return true;
        });
    }

    private void setIcon() {
        mBinding.imageViewIcon.setImageDrawable(ContextCompat.getDrawable(mActivity, getIconPlay()));
    }

    private void removeAudioPlayListener() {
        if (mActivity instanceof AbsBaseActivity) {
            mPlayAudioService = null;
            ((AbsSoundControllerActivity) mActivity).setPlayAudioServiceListener(null);
        }
    }

    private PlayAudioService getPlayAudioService() {
        return ((AbsSoundControllerActivity) mActivity).getPlayAudioService();
    }

    private void setShowLoading(boolean isShow) {
        if (isShow) {
            mBinding.getRoot().setClickable(false);
            mBinding.imageViewIcon.setVisibility(GONE);
            mBinding.loading.setVisibility(VISIBLE);
        } else {
            mBinding.getRoot().setClickable(true);
            mBinding.loading.setVisibility(GONE);
            mBinding.imageViewIcon.setVisibility(VISIBLE);
        }
    }

    private boolean isLoading() {
        return mBinding.loading.getVisibility() == VISIBLE;
    }

    public enum IconType {
        PLAYING(R.drawable.ic_pause_green_dark, R.drawable.ic_pause),
        PAUSE(R.drawable.ic_right_arrow_small_green, R.drawable.ic_right_arrow_small_white);

        @DrawableRes
        private final int mIcon;
        @DrawableRes
        private final int mIconRecipient;

        IconType(int icon, int iconRecipient) {
            mIcon = icon;
            mIconRecipient = iconRecipient;
        }

        public int getIcon() {
            return mIcon;
        }

        public int getIconRecipient() {
            return mIconRecipient;
        }
    }
}
