package com.sompom.tookitup.widget;

import android.content.Context;
import android.util.AttributeSet;

import com.sompom.tookitup.model.emun.Range;

import io.apptik.widget.MultiSlider;

public class MyMultiSlider extends MultiSlider {
    public static final int MIN_SCROLL = 0;
    public static final int MAX_SCROLL = 100;

    private double mMax;
    private double mMin;
    private SlideChangeListener mSlideChangeListener;

    public MyMultiSlider(Context context) {
        super(context);
        init();
    }

    public MyMultiSlider(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyMultiSlider(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setMin(MIN_SCROLL);
        setMax(MAX_SCROLL);
        setOnThumbValueChangeListener((multiSlider, thumb, thumbIndex, value) -> {
            if (mSlideChangeListener == null) {
                return;
            }
            if (thumbIndex == 0) {
                mSlideChangeListener.onMinChange(value, findDoubleValue(value));
            } else {
                mSlideChangeListener.onMaxChange(value, findDoubleValue(value));
            }
        });
    }

    public synchronized void setMinMax(double min, double max) {
        mMax = max;
        mMin = min;
    }

    public void setValue(int min, int max) {
        getThumb(0).setValue(min);
        getThumb(1).setValue(max);
    }

    public void setSlideChangeListener(SlideChangeListener slideChangeListener) {
        mSlideChangeListener = slideChangeListener;
    }

    private double findDoubleValue(int scrollValue) {
        if (scrollValue == 0) {
            return 0;
        } else if (scrollValue < Range.RANGE_1.getPercent()) {
            return Range.RANGE_1.getValue();
        } else if (scrollValue < Range.RANGE_2.getPercent()) {
            return Range.RANGE_2.getValue();
        } else if (scrollValue < Range.RANGE_3.getPercent()) {
            return Range.RANGE_3.getValue();
        } else if (scrollValue < Range.RANGE_4.getPercent()) {
            return Range.RANGE_4.getValue();
        } else if (scrollValue < Range.RANGE_5.getPercent()) {
            return Range.RANGE_5.getValue();
        } else if (scrollValue < Range.RANGE_6.getPercent()) {
            return Range.RANGE_6.getValue();
        } else if (scrollValue < Range.RANGE_7.getPercent()) {
            return Range.RANGE_7.getValue();
        } else if (scrollValue < Range.RANGE_8.getPercent()) {
            return Range.RANGE_8.getValue();
        } else if (scrollValue < Range.RANGE_9.getPercent()) {
            return Range.RANGE_9.getValue();
        } else {
            if (mMax < Range.RANGE_10.getValue()) {
                return Range.RANGE_10.getValue();
            } else {
                return mMax;
            }
        }
    }

    private int findIntValue(double scrollValue) {
        double scrollPercent = scrollValue * 100 / (mMax - mMin);
        double divide = (MAX_SCROLL - MIN_SCROLL) * scrollPercent / 100;
        return (int) Math.round(divide);
    }


    public interface SlideChangeListener {
        void onMinChange(int scrollInt, double value);

        void onMaxChange(int scrollInt, double value);
    }
}
