package com.sompom.tookitup.widget;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.text.InputFilter;
import android.util.AttributeSet;

/**
 * Created by He Rotha on 11/30/18.
 */
public class ExcludeSpacialEditText extends AppCompatEditText {
    public ExcludeSpacialEditText(Context context) {
        super(context);
        init();
    }

    public ExcludeSpacialEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ExcludeSpacialEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        String specialChars = "/*!@#$%^&*()\"{}_[]|\\?/<>,.:-'';§£¥";
        InputFilter filter = (source, start, end, dest, dstart, dend) -> {
//            Timber.e("source: " + source + ", start:" + start + ", end: " + end + ", dest: " + dest + ", dstart: " + dstart + ", dend: " + dend);

            //check multi-space
            String stringSource = source.toString();
            String stringDest = dest.toString();
            if (stringSource.equals(" ")) {
                if (stringDest.length() == 0) {
                    return "";
                } else if (stringDest.length() > 1) {
                    if ((dstart > 0 &&
                            getText().charAt(dstart - 1) == ' ') ||
                            (getText().length() > dstart && getText().charAt(dstart) == ' ') ||
                            dstart == 0) {
                        return "";
                    }
                }
            }

            //check special characters
            for (int i = end - 1; i >= start; i--) {
                char currentChar = source.charAt(i);
                int type = Character.getType(currentChar);
                if (type == Character.SURROGATE ||
                        type == Character.OTHER_SYMBOL ||
                        type == Character.MATH_SYMBOL ||
                        specialChars.contains(String.valueOf(currentChar))) {
                    source = new StringBuilder(source.toString()).deleteCharAt(i).toString();
                }
            }

            return source;
        };
        setFilters(new InputFilter[]{filter});

    }
}
