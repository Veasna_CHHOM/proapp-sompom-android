package com.sompom.tookitup.widget.sortby;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.LayoutFilterDetailSortByBinding;

/**
 * Created by nuonveyo on 7/5/18.
 */

public class SortByButton extends LinearLayout {
    private LayoutFilterDetailSortByBinding mBiding;

    public SortByButton(Context context) {
        super(context);
        init();
    }

    public SortByButton(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SortByButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public SortByButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void setSelected(boolean isSelected) {
        if (isSelected) {
            mBiding.dropDownImageView.setVisibility(VISIBLE);
            mBiding.title.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
            Typeface typeface = ResourcesCompat.getFont(getContext(), R.font.sf_pro_semibold);
            mBiding.title.setTypeface(typeface);
            mBiding.title.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.round_orange_stroke_background));
        } else {
            mBiding.dropDownImageView.setVisibility(INVISIBLE);
            mBiding.title.setTextColor(ContextCompat.getColor(getContext(), R.color.darkGrey));
            Typeface typeface = ResourcesCompat.getFont(getContext(), R.font.sf_pro_regular);
            mBiding.title.setTypeface(typeface);
            mBiding.title.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.round_grey_stroke_background));
        }
    }

    public void setTitle(@StringRes int title) {
        mBiding.title.setText(title);
    }

    private void init() {
        mBiding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.layout_filter_detail_sort_by,
                this,
                true);
    }
}
