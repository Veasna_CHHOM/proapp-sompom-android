package com.sompom.tookitup.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.sompom.tookitup.R;
import com.sompom.tookitup.model.emun.RoundCornerType;
import com.sompom.tookitup.utils.BitmapConverter;
import com.sompom.tookitup.utils.DrawChatImageRoundedUtil;

/**
 * temporary disable rounding code, cos Glide already transform image to Round
 */
public class RoundedImageView extends AppCompatImageView {

    private final int mSmallRadius = getContext().getResources().getDimensionPixelSize(R.dimen.chat_small_radius);
    private int mMediumRadius = getContext().getResources().getDimensionPixelSize(R.dimen.chat_radius);

    private boolean mIsCircle = true;
    private boolean mIsScaleCenterCrop;
    private RoundCornerType mCornerType;

    public RoundedImageView(Context context) {
        super(context);
        init(null);
    }

    public RoundedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public RoundedImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public void setMediumRadius(int mediumRadius) {
        mMediumRadius = mediumRadius;
    }

    public void setCircle(boolean circle) {
        mIsCircle = circle;
    }

    public void setScaleCenterCrop(boolean scaleCenterCrop) {
        mIsScaleCenterCrop = scaleCenterCrop;
    }

    public void setCornerType(RoundCornerType cornerType) {
        mCornerType = cornerType;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        try {

            Drawable drawable = getDrawable();

            if (drawable == null) {
                return;
            }

            if (getWidth() == 0 || getHeight() == 0) {
                return;
            }

            Bitmap b;
            if (drawable instanceof VectorDrawable) {
                b = BitmapConverter.getBitmapFromVectorDrawable(getContext(), drawable);
            } else if (drawable instanceof GifDrawable) {
                b = BitmapConverter.getBitmapFromGifDrawable(getContext(), (GifDrawable) drawable);
            } else {
                b = ((BitmapDrawable) drawable).getBitmap();
            }

            if (mIsCircle) {
                b = DrawChatImageRoundedUtil.scaleCenterCrop(b, getWidth(), getHeight());
                Bitmap bitmap = b.copy(Bitmap.Config.ARGB_8888, true);
                int w = getWidth();
                Bitmap roundBitmap = DrawChatImageRoundedUtil.cropCircle(bitmap, w);
                canvas.drawBitmap(roundBitmap, 0, 0, null);

            } else {
                Bitmap bitmap;
                if (drawable instanceof VectorDrawable || mIsScaleCenterCrop) {
                    b = DrawChatImageRoundedUtil.scaleCenterCrop(b, getWidth(), getHeight());
                    bitmap = b.copy(Bitmap.Config.ARGB_8888, true);
                } else {
                    bitmap = b;
                }

                if (mCornerType == RoundCornerType.Single) {
                    Bitmap trans = DrawChatImageRoundedUtil.roundAll(bitmap, mMediumRadius);
                    canvas.drawBitmap(trans, 0, 0, null);

                } else if (mCornerType == RoundCornerType.MeTop) {
                    Bitmap trans1 = DrawChatImageRoundedUtil.roundTop(bitmap, mMediumRadius);
                    Bitmap trans2 = DrawChatImageRoundedUtil.roundBottomLeft(trans1, mMediumRadius);
                    Bitmap trans3 = DrawChatImageRoundedUtil.roundBottomRight(trans2, mSmallRadius);
                    canvas.drawBitmap(trans3, 0, 0, null);

                } else if (mCornerType == RoundCornerType.MeMid) {
                    Bitmap trans1 = DrawChatImageRoundedUtil.roundLeft(bitmap, mMediumRadius);
                    Bitmap trans2 = DrawChatImageRoundedUtil.roundRight(trans1, mSmallRadius);
                    canvas.drawBitmap(trans2, 0, 0, null);

                } else if (mCornerType == RoundCornerType.MeBottom) {
                    Bitmap trans1 = DrawChatImageRoundedUtil.roundTopLeft(bitmap, mMediumRadius);
                    Bitmap trans2 = DrawChatImageRoundedUtil.roundTopRight(trans1, mSmallRadius);
                    Bitmap trans3 = DrawChatImageRoundedUtil.roundBottom(trans2, mMediumRadius);
                    canvas.drawBitmap(trans3, 0, 0, null);

                } else if (mCornerType == RoundCornerType.YouTop) {
                    Bitmap trans1 = DrawChatImageRoundedUtil.roundTop(bitmap, mMediumRadius);
                    Bitmap trans2 = DrawChatImageRoundedUtil.roundBottomLeft(trans1, mSmallRadius);
                    Bitmap trans3 = DrawChatImageRoundedUtil.roundBottomRight(trans2, mMediumRadius);
                    canvas.drawBitmap(trans3, 0, 0, null);

                } else if (mCornerType == RoundCornerType.YouMid) {
                    Bitmap trans1 = DrawChatImageRoundedUtil.roundLeft(bitmap, mSmallRadius);
                    Bitmap trans2 = DrawChatImageRoundedUtil.roundRight(trans1, mMediumRadius);
                    canvas.drawBitmap(trans2, 0, 0, null);

                } else if (mCornerType == RoundCornerType.YouBottom) {
                    Bitmap trans1 = DrawChatImageRoundedUtil.roundTopLeft(bitmap, mSmallRadius);
                    Bitmap trans2 = DrawChatImageRoundedUtil.roundTopRight(trans1, mMediumRadius);
                    Bitmap trans3 = DrawChatImageRoundedUtil.roundBottom(trans2, mMediumRadius);
                    canvas.drawBitmap(trans3, 0, 0, null);

                } else if (mCornerType == RoundCornerType.TopLeft) {
                    Bitmap trans1 = DrawChatImageRoundedUtil.roundTopLeft(bitmap, mMediumRadius);
                    Bitmap trans2 = DrawChatImageRoundedUtil.roundTopRight(trans1, mSmallRadius);
                    Bitmap trans3 = DrawChatImageRoundedUtil.roundBottom(trans2, mSmallRadius);
                    canvas.drawBitmap(trans3, 0, 0, null);

                } else if (mCornerType == RoundCornerType.TopRight) {
                    Bitmap trans1 = DrawChatImageRoundedUtil.roundTopLeft(bitmap, mSmallRadius);
                    Bitmap trans2 = DrawChatImageRoundedUtil.roundTopRight(trans1, mMediumRadius);
                    Bitmap trans3 = DrawChatImageRoundedUtil.roundBottom(trans2, mSmallRadius);
                    canvas.drawBitmap(trans3, 0, 0, null);

                } else if (mCornerType == RoundCornerType.BottomLeft) {
                    Bitmap trans1 = DrawChatImageRoundedUtil.roundTop(bitmap, mSmallRadius);
                    Bitmap trans2 = DrawChatImageRoundedUtil.roundBottomLeft(trans1, mMediumRadius);
                    Bitmap trans3 = DrawChatImageRoundedUtil.roundBottomRight(trans2, mSmallRadius);
                    canvas.drawBitmap(trans3, 0, 0, null);

                } else if (mCornerType == RoundCornerType.BottomRight) {
                    Bitmap trans1 = DrawChatImageRoundedUtil.roundTop(bitmap, mSmallRadius);
                    Bitmap trans2 = DrawChatImageRoundedUtil.roundBottomLeft(trans1, mSmallRadius);
                    Bitmap trans3 = DrawChatImageRoundedUtil.roundBottomRight(trans2, mMediumRadius);
                    canvas.drawBitmap(trans3, 0, 0, null);

                } else if (mCornerType == RoundCornerType.Bottom) {
                    Bitmap trans1 = DrawChatImageRoundedUtil.roundTop(bitmap, mSmallRadius);
                    Bitmap trans2 = DrawChatImageRoundedUtil.roundBottom(trans1, mMediumRadius);
                    canvas.drawBitmap(trans2, 0, 0, null);

                } else {
                    Bitmap trans = DrawChatImageRoundedUtil.roundAll(bitmap, mSmallRadius);
                    canvas.drawBitmap(trans, 0, 0, null);

                }
            }

        } catch (ClassCastException ex) {
//            Timber.e("onDraw Image Error: %s", ex.toString());
            super.onDraw(canvas);
        }
    }

    private void init(AttributeSet attr) {
        if (attr != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attr, R.styleable.RoundedImageView);
            mIsCircle = typedArray.getBoolean(R.styleable.RoundedImageView_isCircle, true);
            typedArray.recycle();
        }
    }
}