package com.sompom.tookitup.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.view.ViewTreeObserver;

import com.sompom.tookitup.utils.RenderHashTagUtil;

/**
 * Created by nuonveyo on 11/16/18.
 */

public class WallStreetDescriptionTextView extends android.support.v7.widget.AppCompatTextView {
    private boolean mIsSeeMore;

    public WallStreetDescriptionTextView(Context context) {
        super(context);
    }

    public WallStreetDescriptionTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public WallStreetDescriptionTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setDescription(String description,
                               boolean isMaxLine,
                               com.sompom.tookitup.listener.OnClickListener onClickListener) {
        setText(RenderHashTagUtil.renderMentionUser(getContext(), description));
        mIsSeeMore = false;
        if (isMaxLine) {
            setMaxLines(4);
            int maxLine = getMaxLines();

            if (getTag() == null) {
                setTag(getText());
            }
            ViewTreeObserver vto = getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                @SuppressWarnings("deprecation")
                @Override
                public void onGlobalLayout() {
                    ViewTreeObserver obs = getViewTreeObserver();
                    obs.removeGlobalOnLayoutListener(this);

                    if (maxLine > 0 && getLineCount() >= maxLine) {
                        int lineEndIndex = getLayout().getLineEnd(maxLine - 1);
                        if (getText().length() > lineEndIndex) {
                            mIsSeeMore = true;
                            setText(RenderHashTagUtil.renderDescriptionWithSeeMore(getContext(), getText(), lineEndIndex));
                        }
                    }
                }
            });
        }
        setOnClickListener(v -> {
            if (getSelectionStart() == -1
                    && getSelectionEnd() == -1
                    && onClickListener != null) {
                onClickListener.onClick();
            }
        });
        setMovementMethod(LinkMovementMethod.getInstance());
    }

    public boolean isSeeMore() {
        return mIsSeeMore;
    }

}
