package com.sompom.tookitup.widget.lifestream;

import android.content.Context;
import android.content.res.TypedArray;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.PlayerView;
import com.sompom.tookitup.R;
import com.sompom.tookitup.helper.PlayControlDispatcher;
import com.sompom.tookitup.listener.OnMediaLayoutClickListener;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.emun.MediaType;
import com.sompom.tookitup.utils.GlideLoadUtil;
import com.sompom.tookitup.utils.VolumePlaying;
import com.sompom.videomanager.helper.ExoPlayerBuilder;

import timber.log.Timber;

/**
 * Created by He Rotha on 8/28/17.
 */

public class MediaLayout extends RelativeLayout implements PlayingHandler.OnPlayListener {
    private View mImageFloatingVideo;
    private View mImageViewFullScreen;
    private PlayerView mPlayerView;
    private ImageView mAudioMute;
    private ImageView mThumbnailImageView;
    private View mCloseView;

    private SimpleExoPlayer mPlayer;
    private Media mMedia;
    private boolean mIsShowControl = false;
    private boolean mIsMute;
    private boolean mIsSaveOnPause = false;
    private boolean mIsCenterCrop = true;
    private OnMediaLayoutClickListener mOnMediaLayoutClickListener;
    private OnVolumeClickListener mOnVolumeClickListener;
    private PlayingHandler mMyHandler = new PlayingHandler(this);
    private View.OnClickListener mMuteAudioClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            mIsMute = !mIsMute;
            VolumePlaying.updateMute(getContext(), mIsMute);
            if (getPlayer() != null) {
                if (mIsMute) {
                    getPlayer().setVolume(0f);
                } else {
                    getPlayer().setVolume(1f);
                }
            }
            setMute(mIsMute);
            if (mOnVolumeClickListener != null) {
                mOnVolumeClickListener.onVolumeClick(mIsMute);
            }
        }
    };

    public MediaLayout(Context context) {
        super(context);
        init(context, null);
    }

    public MediaLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public MediaLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attributeSet) {
        ViewDataBinding binding;
        TypedArray a = context.obtainStyledAttributes(attributeSet, R.styleable.MediaLayout);
        boolean enableBackground = false;
        if (a.hasValue(R.styleable.MediaLayout_includeControl)) {
            enableBackground = a.getBoolean(R.styleable.MediaLayout_includeControl, false);
        }
        a.recycle();
        if (enableBackground) {
            binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.layout_media_control, this, true);
        } else {
            binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.layout_media, this, true);
        }
        setOnClickListener(v -> {
            if (mOnMediaLayoutClickListener != null) {
                mOnMediaLayoutClickListener.onMediaLayoutClick(MediaLayout.this);
            }
        });
        View view = binding.getRoot();
        mPlayerView = view.findViewById(R.id.player_view);
        mImageFloatingVideo = mPlayerView.findViewById(R.id.imageFloatingVideo);
        mImageViewFullScreen = mPlayerView.findViewById(R.id.imageViewFullScreen);
        mAudioMute = view.findViewById(R.id.imageAudio);
        mThumbnailImageView = mPlayerView.findViewById(R.id.exo_thumbnail);
        mCloseView = view.findViewById(R.id.imageClose);
    }

    public void setCenterCropImage() {
        mIsCenterCrop = false;
    }

    public void setOnMediaLayoutClickListener(OnMediaLayoutClickListener onMediaLayoutClickListener) {
        mOnMediaLayoutClickListener = onMediaLayoutClickListener;
    }

    public void setVisibleMuteButton(boolean isVisible) {
        if (mAudioMute == null) {
            return;
        }
        if (isVisible && mMedia != null
                && (mMedia.getType() == MediaType.VIDEO
                || mMedia.getType() == MediaType.LIVE_VIDEO)) {
            mAudioMute.setVisibility(VISIBLE);
            mAudioMute.setOnClickListener(mMuteAudioClickListener);
        } else {
            mAudioMute.setVisibility(GONE);
            mAudioMute.setOnClickListener(null);
        }
    }

    public void setMute(boolean isMute) {
        mIsMute = isMute;
        if (mAudioMute == null) {
            return;
        }
        if (mMedia.getType() == MediaType.VIDEO
                || mMedia.getType() == MediaType.LIVE_VIDEO) {
            if (isMute) {
                mAudioMute.setImageResource(R.drawable.ic_volume_off_black_24dp);
            } else {
                mAudioMute.setImageResource(R.drawable.ic_volume_up_black_24dp);
            }
        }
    }

    public void release() {
        Timber.e("release");
        if (mPlayer != null) {
            mPlayer.stop();
            mPlayer.release();
            setKeepScreenOn(false);
        }
    }

    public void setMedia(Media media) {
        mMedia = media;
        if (TextUtils.isEmpty(mMedia.getUrl())) {
            return;
        }
        if (media.getType() == MediaType.VIDEO
                || mMedia.getType() == MediaType.LIVE_VIDEO) {
            if (mThumbnailImageView != null) {
                mThumbnailImageView.setVisibility(VISIBLE);
                if (mMedia.getUrl().startsWith("http")) {
                    GlideLoadUtil.load(mThumbnailImageView, mMedia.getThumbnail());
                } else {
                    GlideLoadUtil.load(mThumbnailImageView, mMedia.getUrl());
                }
            }
            if (mAudioMute != null) {
                mAudioMute.setVisibility(GONE);
            }
        } else if (media.getType() == MediaType.IMAGE) {
            if (mThumbnailImageView != null) {
                mThumbnailImageView.setVisibility(VISIBLE);
                GlideLoadUtil.load(mThumbnailImageView, media.getUrl(), mIsCenterCrop);
            }
            if (mAudioMute != null) {
                mAudioMute.setVisibility(GONE);
            }
        }
    }

    public void setSaveOnPause(boolean saveOnPause) {
        mIsSaveOnPause = saveOnPause;
    }

    public void setShowControl(boolean showControl) {
        mIsShowControl = showControl;
    }

    private SimpleExoPlayer setupPlayer(String url) {
        mPlayerView.setUseController(mIsShowControl);
        SimpleExoPlayer exoPlayer = ExoPlayerBuilder.build(getContext(), url, isPlaying -> {
            Timber.e("onPlayStatus " + isPlaying);
            if (mIsSaveOnPause) {
                mMedia.setPause(!isPlaying);
            }
            if (mMyHandler.isStartPlay()) {
                mThumbnailImageView.setVisibility(GONE);
            }
        });
        mPlayerView.setPlayer(exoPlayer);
        if (mPlayerView.getUseController()) {
            mPlayerView.setControlDispatcher(new PlayControlDispatcher() {
                @Override
                public void onButtonPlayClick(boolean isPlay) {
                    mOnVolumeClickListener.onButtonPlayClick(isPlay);
                    if (isPlay) {
                        mThumbnailImageView.setVisibility(GONE);
                    }
                    mMyHandler.setStartPlay(isPlay);

                }
            });
        }
        return exoPlayer;
    }

    public long getTime() {
        if (getPlayer() == null) {
            return 0;
        } else {
            return getPlayer().getCurrentPosition();
        }
    }

    private SimpleExoPlayer getPlayer() {
        return mPlayer;
    }

    public PlayerView getPlayerView() {
        return mPlayerView;
    }

    public void onMediaPlay(final boolean isStartPlay) {
        Timber.e(hashCode() + " onMediaPlay1 " + isStartPlay);
        mMyHandler.setStartPlay(isStartPlay);
        mMyHandler.startDelay();
    }

    public void onMediaPause() {
        Timber.e(hashCode() + " onMediaPause ");

        mMyHandler.destroy();
        if (mPlayer != null
                && (mMedia.getType() == MediaType.VIDEO
                || mMedia.getType() == MediaType.LIVE_VIDEO)) {
            mPlayer.stop();
            mMedia.setTime(mPlayer.getCurrentPosition());
            setKeepScreenOn(false);
        }
    }

    public void setVisibleCloseButton() {
        if (mCloseView != null) {
            mCloseView.setVisibility(VISIBLE);
        }
    }

    public void setOnCloseButtonClickedListener(OnClickListener onClickListener) {
        if (onClickListener != null && mCloseView != null) {
            mCloseView.setOnClickListener(onClickListener);
        }
    }

    public void setVisibleFloatingButton(boolean isShow) {
        if (mImageFloatingVideo == null) {
            return;
        }
        if (isShow) {
            mImageFloatingVideo.setVisibility(VISIBLE);
        } else {
            mImageFloatingVideo.setVisibility(GONE);
        }
    }

    public void setOnFloatingVideoClickedListener(OnClickListener onClickListener) {
        if (onClickListener != null && mImageFloatingVideo != null) {
            mImageFloatingVideo.setOnClickListener(onClickListener);
        }
    }

    public void setVisibleFullScreenButton(boolean isShow) {
        if (mImageViewFullScreen == null) {
            return;
        }
        if (isShow) {
            mImageViewFullScreen.setVisibility(VISIBLE);
        } else {
            mImageViewFullScreen.setVisibility(GONE);
        }
    }

    public void setOnFullScreenClickListener(OnClickListener onFullScreenClickListener) {
        if (onFullScreenClickListener != null && mImageViewFullScreen != null) {
            mImageViewFullScreen.setOnClickListener(onFullScreenClickListener);
        }
    }

    @Override
    public void onPlay(boolean isStartPlay) {
        Timber.e(hashCode() + " setPlayWhenReady " + isStartPlay);
        if (TextUtils.isEmpty(mMedia.getUrl())) {
            return;
        }
        if (mMedia.getType() == MediaType.VIDEO
                || mMedia.getType() == MediaType.LIVE_VIDEO) {
            mThumbnailImageView.setVisibility(VISIBLE);
            GlideLoadUtil.load(mThumbnailImageView, mMedia.getThumbnail());

            if (getPlayer() != null) {
                getPlayer().stop();
            }
            mPlayerView.setPlayer(null);
            mPlayer = setupPlayer(mMedia.getUrl());
            if (mAudioMute != null && mAudioMute.getVisibility() == View.VISIBLE) {
                if (mIsMute) {
                    mPlayer.setVolume(0f);
                } else {
                    mPlayer.setVolume(1f);
                }
            }
            mPlayer.setPlayWhenReady(isStartPlay);
            mPlayer.seekTo(mMedia.getTime());
            setKeepScreenOn(true);
        }
    }

    public void setOnVolumeClickListener(OnVolumeClickListener onVolumeClickListener) {
        mOnVolumeClickListener = onVolumeClickListener;
    }

    public interface OnVolumeClickListener {
        void onButtonPlayClick(boolean isPlay);

        void onVolumeClick(boolean isMute);
    }

}
