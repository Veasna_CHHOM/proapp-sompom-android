package com.sompom.tookitup.widget;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.LayoutPostContextMenuBinding;
import com.sompom.tookitup.model.PostContextMenuItem;

/**
 * Created by nuonveyo on 10/31/18.
 */

public class PostContextMenuLayout extends LinearLayout {
    private LayoutPostContextMenuBinding mBinding;

    public PostContextMenuLayout(Context context) {
        super(context);
        init();
    }

    public PostContextMenuLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PostContextMenuLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public PostContextMenuLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.layout_post_context_menu, this, true);
    }

    public void setData(PostContextMenuItem postContextMenuItem) {
        mBinding.iconTextView.setText(postContextMenuItem.getIcon());
        mBinding.titleTextView.setText(postContextMenuItem.getTitle());
    }
}
