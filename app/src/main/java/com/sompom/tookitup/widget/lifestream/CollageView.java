package com.sompom.tookitup.widget.lifestream;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sompom.tookitup.R;
import com.sompom.tookitup.utils.FormatNumber;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by He Rotha on 8/25/17.
 */

public class CollageView extends GridLayout {
    private static final String sLastItemParent = "LastItemParent";
    private final List<View> mLayouts = new ArrayList<>();
    private Orientation mOrientation = Orientation.Vertical;
    private Format mFormat;
    private int mLayoutRes;
    private int mNumberOfItem;
    private Setting mSetting;
    private TextView mBadgeView;

    public CollageView(Context context) {
        super(context);
    }

    public CollageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CollageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        mNumberOfItem = 0;
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CollageView);
            mLayoutRes = a.getResourceId(R.styleable.CollageView_layoutRes, -1);
            mNumberOfItem = a.getInt(R.styleable.CollageView_numberOfItem, 0);

            int orientation = a.getInt(R.styleable.CollageView_direction, 1);
            mOrientation = Orientation.fromValue(orientation);

            int format = a.getInt(R.styleable.CollageView_format, Format.Three.getValue());
            mFormat = Format.fromValue(format);

            a.recycle();
        }

        startGenerateView();
    }

    public void setOrientation(Orientation orientation) {
        mOrientation = orientation;
    }

    public void setFormat(Format format) {
        mFormat = format;
    }

    public void setNumberOfItem(int numberOfItem) {
        mNumberOfItem = numberOfItem;
    }

    @Override
    public void removeAllViews() {
        super.removeAllViews();
        mLayouts.clear();
    }

    public void startGenerateView() {
        if (mLayoutRes == 0 || mNumberOfItem == 0) {
            return;
        }
        if (mSetting != null) {
            if (mSetting.mOrientation == mOrientation &&
                    mSetting.mFormat == mFormat &&
                    mSetting.mNumberOfItem == mNumberOfItem) {
                return;
            } else {
                mSetting.mFormat = mFormat;
                mSetting.mNumberOfItem = mNumberOfItem;
                mSetting.mOrientation = mOrientation;
            }
        } else {
            mSetting = new Setting();
            mSetting.mFormat = mFormat;
            mSetting.mNumberOfItem = mNumberOfItem;
            mSetting.mOrientation = mOrientation;
        }

        removeAllViews();

        List<MediaGridProperty> properties = new ArrayList<>();
        if (mFormat == Format.Three) {
            final int marginSize = getResources().getDimensionPixelSize(R.dimen.space_tiny);
            handleThree(properties, marginSize);
        } else {
            if (mNumberOfItem == 1) {
                properties.add(new MediaGridProperty(0, 1, 0, 1));
            } else {
                final int marginSize = getResources().getDimensionPixelSize(R.dimen.space_tiny);
                if (mFormat == Format.FreeStyle) {
                    handleFreeStyle(properties, marginSize);
                } else {
                    handleSquare(properties, marginSize);
                }
            }
        }

        for (int i = 0; i < properties.size(); i++) {
            MediaGridProperty property = properties.get(i);
            View view;
            if (i >= mFormat.getMaxDisplay() - 1) {
                RelativeLayout relativeLayout = new RelativeLayout(getContext());
                relativeLayout.setTag(sLastItemParent);
                view = LayoutInflater.from(getContext()).inflate(mLayoutRes, relativeLayout, true);
            } else {
                view = LayoutInflater.from(getContext()).inflate(mLayoutRes, null, false);
            }

            Spec rowSpec = GridLayout.spec(property.getRow(), property.getRowSpan(), property.getRowWeight());
            Spec colSpec = GridLayout.spec(property.getCol(), property.getColSpan(), property.getColWeight());
            LayoutParams param = new LayoutParams(rowSpec, colSpec);
            param.width = 0;
            param.height = 0;
            param.setMargins(property.getMarginLeft(), property.getMarginTop(),
                    property.getMarginRight(), property.getMarginBottom());
            addView(view, param);
            mLayouts.add(view);
        }
    }

    private void handleThree(List<MediaGridProperty> properties, int marginSize) {
        if (mNumberOfItem == 1) {
            properties.add(new MediaGridProperty(0, 1, 0, 1));
        } else if (mNumberOfItem == 2) {
            properties.add(new MediaGridProperty(0, 1, 0, 1).withMarginRight(marginSize));
            properties.add(new MediaGridProperty(0, 1, 1, 1).withMarginLeft(marginSize));
        } else {
            properties.add(new MediaGridProperty(0, 1, 0, 1).withMarginRight(marginSize));
            properties.add(new MediaGridProperty(0, 1, 1, 1).withMarginRight(marginSize).withMarginLeft(marginSize));
            properties.add(new MediaGridProperty(0, 1, 2, 1).withMarginLeft(marginSize));
        }
    }

    private void handleSquare(List<MediaGridProperty> properties, int marginSize) {
        //some format is reuse the id of view type in com.sompom.tookitup.model.emun.ViewType class,
        //make sure recheck the id
        if (mNumberOfItem == 2) {
            properties.add(new MediaGridProperty(0, 1, 0, 1).withMarginRight(marginSize));
            properties.add(new MediaGridProperty(0, 1, 1, 1).withMarginLeft(marginSize));
        } else if (mNumberOfItem == 3) {
            if (mFormat == Format.SquareAll) {
                properties.add(new MediaGridProperty(0, 1, 1, 0, 2, 2).withMarginBottom(marginSize));
                properties.add(new MediaGridProperty(1, 1, 1, 0, 1, 1).withMarginTopRight(marginSize));
                properties.add(new MediaGridProperty(1, 1, 1, 1, 1, 1).withMarginTopLeft(marginSize));
            } else {
                properties.add(new MediaGridProperty(0, 2, 1, 0, 1, 2).withMarginRight(marginSize));
                properties.add(new MediaGridProperty(0, 1, 1, 1).withMarginLeftBottom(marginSize));
                properties.add(new MediaGridProperty(1, 1, 1, 1).withMarginTopLeft(marginSize));
            }
        } else if (mNumberOfItem == 4) {
            properties.add(new MediaGridProperty(0, 1, 0, 1).withMargin(0, 0, marginSize, marginSize));
            properties.add(new MediaGridProperty(0, 1, 1, 1).withMargin(0, marginSize, 0, marginSize));
            properties.add(new MediaGridProperty(1, 1, 0, 1).withMargin(marginSize, 0, marginSize, 0));
            properties.add(new MediaGridProperty(1, 1, 1, 1).withMargin(marginSize, marginSize, 0, 0));
        } else {
            properties.add(new MediaGridProperty(0, 3, 3, 0, 3, 3).withMargin(0, 0, marginSize, marginSize));
            properties.add(new MediaGridProperty(0, 3, 3, 3, 3, 3).withMargin(0, marginSize, 0, marginSize));
            properties.add(new MediaGridProperty(4, 2, 2, 0, 2, 2).withMargin(marginSize, 0, marginSize, 0));
            properties.add(new MediaGridProperty(4, 2, 2, 2, 2, 2).withMargin(marginSize, marginSize, marginSize, 0));
            properties.add(new MediaGridProperty(4, 2, 2, 4, 2, 2).withMargin(marginSize, marginSize, 0, 0));
        }
    }

    private void handleFreeStyle(List<MediaGridProperty> properties, int marginSize) {
        if (mNumberOfItem == 2) {
            if (mOrientation == Orientation.Vertical) {
                properties.add(new MediaGridProperty(0, 1, 0, 1).withMarginBottom(marginSize));
                properties.add(new MediaGridProperty(1, 1, 0, 1).withMarginTop(marginSize));
            } else {
                properties.add(new MediaGridProperty(0, 1, 0, 1).withMarginRight(marginSize));
                properties.add(new MediaGridProperty(0, 1, 1, 1).withMarginLeft(marginSize));
            }
        } else if (mNumberOfItem == 3) {
            if (mOrientation == Orientation.Vertical) {
                properties.add(new MediaGridProperty(0, 1, 2, 0, 2, 1).withMarginBottom(marginSize));
                properties.add(new MediaGridProperty(1, 1, 0, 1).withMarginTopRight(marginSize));
                properties.add(new MediaGridProperty(1, 1, 1, 1).withMarginTopLeft(marginSize));
            } else {
                properties.add(new MediaGridProperty(0, 2, 1, 0, 1, 2).withMarginRight(marginSize));
                properties.add(new MediaGridProperty(0, 1, 1, 1).withMarginLeftBottom(marginSize));
                properties.add(new MediaGridProperty(1, 1, 1, 1).withMarginTopLeft(marginSize));
            }
        } else if (mNumberOfItem == 4) {
            if (mOrientation == Orientation.Vertical) {
                properties.add(new MediaGridProperty(0, 1, 2, 0, 3, 1).withMarginBottom(marginSize));
                properties.add(new MediaGridProperty(1, 1, 0, 1).withMarginTopRight(marginSize));
                properties.add(new MediaGridProperty(1, 1, 1, 1).withMarginTopLeftRight(marginSize));
                properties.add(new MediaGridProperty(1, 1, 2, 1).withMarginTopLeft(marginSize));
            } else {
                properties.add(new MediaGridProperty(0, 3, 1, 0, 1, 2).withMarginRight(marginSize));
                properties.add(new MediaGridProperty(0, 1, 1, 1).withMarginLeftBottom(marginSize));
                properties.add(new MediaGridProperty(1, 1, 1, 1).withMarginTopLeftBottom(marginSize));
                properties.add(new MediaGridProperty(2, 1, 1, 1).withMarginTopLeft(marginSize));
            }
        } else {
            if (mOrientation == Orientation.Vertical) {
                properties.add(new MediaGridProperty(0, 3, 3, 0, 1, 1).withMargin(0, 0, marginSize, marginSize));
                properties.add(new MediaGridProperty(3, 3, 3, 0, 1, 1).withMargin(marginSize, 0, marginSize, 0));
                properties.add(new MediaGridProperty(0, 2, 2, 1, 1, 1).withMargin(0, marginSize, 0, marginSize));
                properties.add(new MediaGridProperty(2, 2, 2, 1, 1, 1).withMargin(marginSize, marginSize, 0, marginSize));
                properties.add(new MediaGridProperty(4, 2, 2, 1, 1, 1).withMargin(marginSize, marginSize, 0, 0));
            } else {
                properties.add(new MediaGridProperty(0, 1, 1, 0, 3, 3).withMargin(0, 0, marginSize, marginSize));
                properties.add(new MediaGridProperty(0, 1, 1, 3, 3, 3).withMargin(0, marginSize, 0, marginSize));
                properties.add(new MediaGridProperty(1, 1, 1, 0, 2, 2).withMargin(marginSize, 0, marginSize, 0));
                properties.add(new MediaGridProperty(1, 1, 1, 2, 2, 2).withMargin(marginSize, marginSize, marginSize, 0));
                properties.add(new MediaGridProperty(1, 1, 1, 4, 2, 2).withMargin(marginSize, marginSize, 0, 0));
            }
        }
    }

    @Override
    protected void onMeasure(int widthSpec, int heightSpec) {
        if (mFormat == Format.Three) {
            int spec;
            if (getTag(R.id.tagContainer) == null) {
                setTag(R.id.tagContainer, widthSpec);
                spec = widthSpec;
            } else {
                spec = (int) getTag(R.id.tagContainer);
            }
            int originalWidth = MeasureSpec.getSize(spec);
            int finalHeight = originalWidth / Format.Three.getMaxDisplay();
            if (mNumberOfItem < Format.Three.getMaxDisplay()) {
                int finalWidth = finalHeight * mNumberOfItem;
                super.onMeasure(MeasureSpec.makeMeasureSpec(finalWidth, MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(finalHeight, MeasureSpec.EXACTLY));
            } else {
                super.onMeasure(widthSpec, MeasureSpec.makeMeasureSpec(finalHeight, MeasureSpec.EXACTLY));
            }
            return;
        }

        if (mNumberOfItem == 1) {
            super.onMeasure(widthSpec, heightSpec);
        } else {
            if (mFormat != Format.FreeStyle) {
                if (mNumberOfItem == 2 || mNumberOfItem == 5) {
                    int originalWidth = MeasureSpec.getSize(widthSpec);
                    int finalHeight;
                    if (mNumberOfItem == 2) {
                        finalHeight = originalWidth / 2;
                    } else {
                        finalHeight = originalWidth / 2 + originalWidth / 3;
                    }
                    super.onMeasure(widthSpec, MeasureSpec.makeMeasureSpec(finalHeight, MeasureSpec.EXACTLY));
                } else {
                    super.onMeasure(widthSpec, widthSpec);
                }
            } else {
                super.onMeasure(widthSpec, widthSpec);
            }
        }
    }

    public void setBadge(int totalSize) {
        if (totalSize > mFormat.getMaxDisplay()) {
            totalSize = totalSize - mFormat.getMaxDisplay();
            View view = mLayouts.get(mLayouts.size() - 1);
            if (view.getTag() != null && view.getTag() instanceof String && view.getTag().equals(sLastItemParent)) {
                if (mBadgeView == null) {
                    mBadgeView = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.layout_media_more, null, false);
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                    ((RelativeLayout) view).addView(mBadgeView, params);
                }
                mBadgeView.setText(String.format("%s+", FormatNumber.format(totalSize)));
            }
        }
    }

    public void bindView(OnBindItemListener listener) {
        if (mNumberOfItem == 1) {
            int screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
            int screenHeight;
            Point point = listener.onRequiredWidthHeight();
            if (point != null) {
                if (point.x == point.y) {
                    screenHeight = screenWidth;
                } else {
                    screenHeight = point.y * screenWidth / point.x;
                }
            } else {
                screenHeight = screenWidth;
            }
            if (screenHeight > screenWidth) {
                screenHeight = screenWidth;
            }

            ViewGroup.LayoutParams param = getLayoutParams();
            if (param.width != screenWidth || param.height != screenHeight) {
                param.width = screenWidth;
                param.height = screenHeight;
                setLayoutParams(param);
            }
        }

        for (int i = 0; i < mLayouts.size() - 1; i++) {
            listener.onBind(mLayouts.get(i), i);
        }

        View view = mLayouts.get(mLayouts.size() - 1);
        if (view.getTag() != null && view.getTag() instanceof String && view.getTag().equals(sLastItemParent)) {
            RelativeLayout layout = (RelativeLayout) view;
            listener.onBind(layout.getChildAt(0), mLayouts.size() - 1);
        } else {
            listener.onBind(mLayouts.get(mLayouts.size() - 1), mLayouts.size() - 1);
        }
    }


    private class Setting {
        private int mNumberOfItem;
        private Orientation mOrientation;
        private Format mFormat;
    }
}
