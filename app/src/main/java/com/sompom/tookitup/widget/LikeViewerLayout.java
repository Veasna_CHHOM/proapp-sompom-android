package com.sompom.tookitup.widget;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.newadapter.LikeViewerAdapter;
import com.sompom.tookitup.databinding.LayoutLikeViewerBinding;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnLayoutLikeListener;
import com.sompom.tookitup.model.emun.FollowItemType;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.dialog.AbsBindingFragmentDialog;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.LikeViewerDataManager;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.newviewmodel.LayoutLikeViewModel;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by nuonveyo on 6/8/18.
 */

public class LikeViewerLayout extends AbsBindingFragmentDialog<LayoutLikeViewerBinding>
        implements OnLayoutLikeListener {
    public static final String TAG = LikeViewerLayout.class.getName();

    @Inject
    public ApiService mApiService;

    private LayoutLikeViewModel mViewModel;
    private LikeViewerAdapter mViewerAdapter;

    public static LikeViewerLayout newInstance(String productId) {
        Bundle args = new Bundle();
        args.putString(SharedPrefUtils.DATA, productId);
        LikeViewerLayout fragment = new LikeViewerLayout();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);

        String productId = getArguments().getString(SharedPrefUtils.DATA);
        getBinding().textviewClose.setOnClickListener(v -> dismiss());

        LikeViewerDataManager dataManager = new LikeViewerDataManager(getActivity(), mApiService);
        mViewModel = new LayoutLikeViewModel(productId, dataManager, this);
        setVariable(BR.viewModel, mViewModel);
    }

    @Override
    public void onComplete(List<User> result, boolean canLoadMore) {
        LoaderMoreLayoutManager layoutManager = new LoaderMoreLayoutManager(getActivity());
        if (canLoadMore) {
            layoutManager.setOnLoadMoreListener(() -> mViewModel.loadMore(new OnCallbackListListener<List<User>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    mViewerAdapter.setCanLoadMore(false);
                    layoutManager.setOnLoadMoreListener(null);
                    layoutManager.loadingFinished();
                }

                @Override
                public void onComplete(List<User> result, boolean canLoadMore) {
                    mViewerAdapter.addLoadMoreData(result);
                    mViewerAdapter.setCanLoadMore(canLoadMore);
                    layoutManager.loadingFinished();
                }
            }));
        }
        getBinding().recyclerview.setLayoutManager(layoutManager);

        mViewerAdapter = new LikeViewerAdapter(getActivity(),
                LikeViewerAdapter.ItemType.LIKE_ITEM,
                result,
                FollowItemType.FOLLOWING_AND_FOLLOWER,
                mViewModel);
        mViewerAdapter.setCanLoadMore(canLoadMore);
        getBinding().recyclerview.setAdapter(mViewerAdapter);
    }

    @Override
    public void onNotifyItem() {
        if (mViewerAdapter != null) {
            mViewerAdapter.notifyData();
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.layout_like_viewer;

    }
}
