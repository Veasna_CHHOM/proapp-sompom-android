package com.sompom.tookitup.widget;

import android.content.Context;
import android.graphics.PointF;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by imac on 9/18/15.
 */
public class TwoWaysRefreshableLayoutManager extends LinearLayoutManager {
    private boolean mIsLoadingMore = false;
    private OnLoadMoreCallback mOnLoadMoreListener;

    public TwoWaysRefreshableLayoutManager(final Context context) {
        super(context);
    }

    public TwoWaysRefreshableLayoutManager(final Context context, final int orientation, final boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }


    public TwoWaysRefreshableLayoutManager(final Context context, final AttributeSet attrs,
                                           final int defStyleAttr, final int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public int scrollVerticallyBy(final int dy, final RecyclerView.Recycler recycler, final RecyclerView.State state) {
        if (mOnLoadMoreListener == null) {
            return super.scrollVerticallyBy(dy, recycler, state);
        }
        if (!mIsLoadingMore && findFirstVisibleItemPosition() == 0) {
            mOnLoadMoreListener.onLoadMoreFromTop();
        }
        return super.scrollVerticallyBy(dy, recycler, state);
    }

    public void setOnLoadMoreListener(final OnLoadMoreCallback onLoadMoreListener) {
        mOnLoadMoreListener = onLoadMoreListener;
    }

    public void loadingFinished() {
        mIsLoadingMore = false;
    }

    @Override
    public void smoothScrollToPosition(final RecyclerView recyclerView, RecyclerView.State state, int position) {

        final LinearSmoothScroller linearSmoothScroller = new LinearSmoothScroller(recyclerView.getContext()) {

            @Override
            public PointF computeScrollVectorForPosition(int targetPosition) {
                return TwoWaysRefreshableLayoutManager.this.computeScrollVectorForPosition(targetPosition);
            }

            @Override
            protected int calculateTimeForScrolling(int dx) {
                return 50;
            }


        };

        linearSmoothScroller.setTargetPosition(position);
        startSmoothScroll(linearSmoothScroller);
    }

    public interface OnLoadMoreCallback {
        void onLoadMoreFromTop();

        default void onLoadMoreFromBottom() {
            //nothing to do on super class
        }
    }
}
