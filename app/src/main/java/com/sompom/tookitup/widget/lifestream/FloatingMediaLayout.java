package com.sompom.tookitup.widget.lifestream;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.LayoutMediaFloatingBinding;
import com.sompom.tookitup.helper.CustomExoPlayerEventListener;
import com.sompom.tookitup.listener.OnMediaListener;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.emun.MediaType;
import com.sompom.tookitup.utils.GlideLoadUtil;
import com.sompom.videomanager.helper.ExoPlayerBuilder;

import timber.log.Timber;

/**
 * Created by He Rotha on 8/28/17.
 */

public class FloatingMediaLayout extends RelativeLayout implements OnMediaListener {
    private LayoutMediaFloatingBinding mBinding;
    private SimpleExoPlayer mPlayer;
    private boolean mIsShowControl = false;
    private Media mMedia;
    private boolean mIsPause;
    private Handler mHandler;
    private Runnable mRunnable;
    private OnFloatingMediaPlayVideoListener mOnFloatingMediaPlayVideoListener;
    private final CustomExoPlayerEventListener mCustomExoPlayerEventListener = new CustomExoPlayerEventListener() {
        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            if (mMedia != null && mMedia.getType() == MediaType.VIDEO) {
                if (playbackState == Player.STATE_BUFFERING) {
                    mBinding.progressBar.setVisibility(VISIBLE);
                } else if (playbackState == Player.STATE_READY && playWhenReady) {
                    mBinding.imageView.setVisibility(GONE);
                    mBinding.progressBar.setVisibility(GONE);
                } else if (playbackState == Player.STATE_READY) {
                    mBinding.progressBar.setVisibility(GONE);
                } else if (playbackState == Player.STATE_ENDED) {
                    mBinding.imageView.setVisibility(VISIBLE);
                    mBinding.progressBar.setVisibility(GONE);
                    if (mOnFloatingMediaPlayVideoListener != null) {
                        mOnFloatingMediaPlayVideoListener.onVideoPlayed();
                    }
                }
            }
        }

        @Override
        public void onPlayerError(ExoPlaybackException error) {
            Timber.e("onPlayerError: %s", error.toString());
        }
    };
    private int mPosition;
    private ImageView mExoThumbnailImageView;

    public FloatingMediaLayout(Context context) {
        super(context);
        init(context);
    }

    public FloatingMediaLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public FloatingMediaLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.layout_media_floating, this, true);
        mExoThumbnailImageView = mBinding.imageView;
    }

    public int getPosition() {
        return mPosition;
    }

    public void setPosition(int position) {
        mPosition = position;
    }

    private void initPlayer(Context context) {
        if (getPlayer() != null) {
            getPlayer().stop();
        }
        mBinding.playerView.setPlayer(null);
        mPlayer = setupPlayer(context, mMedia.getUrl());
        mPlayer.addListener(mCustomExoPlayerEventListener);
    }

    public Media getMedia() {
        return mMedia;
    }

    public void setMedia(Media media) {
        mMedia = media;
        if (media.getType() == MediaType.VIDEO) {
            mBinding.playerView.setVisibility(VISIBLE);
            mBinding.imageView.setVisibility(GONE);
            GlideLoadUtil.load(mExoThumbnailImageView, mMedia.getThumbnail());
        } else if (media.getType() == MediaType.IMAGE) {
            mBinding.playerView.setVisibility(GONE);
            mBinding.imageView.setVisibility(VISIBLE);
            GlideLoadUtil.load(mBinding.imageView, media.getUrl());
        }
    }

    public void setShowControl(boolean showControl) {
        mIsShowControl = showControl;
    }

    private SimpleExoPlayer setupPlayer(final Context context, String url) {
        mBinding.playerView.setUseController(mIsShowControl);
        Timber.e("url: %s", url);
        SimpleExoPlayer exoPlayer = ExoPlayerBuilder.build(context, url, isPlaying -> mExoThumbnailImageView.setVisibility(GONE));
        mBinding.playerView.setPlayer(exoPlayer);
        return exoPlayer;
    }

    public long getTime() {
        if (getPlayer() == null) {
            return 0;
        } else {
            return getPlayer().getCurrentPosition();
        }
    }

    private SimpleExoPlayer getPlayer() {
        return mPlayer;
    }

    @Override
    public void onMediaPlay(boolean isWait) {
        if (isWait) {
            mIsPause = false;
            if (mHandler == null) {
                mHandler = new Handler();
                mRunnable = () -> {
                    if (!mIsPause) {
                        onMediaPlay();
                    }
                };
            }
            mHandler.postDelayed(mRunnable, 500);
        } else {
            onMediaPlay();
        }
    }

    @Override
    public void onMediaPlay() {
        initNewMedia();
        mExoThumbnailImageView.setVisibility(VISIBLE);
        GlideLoadUtil.load(mExoThumbnailImageView, mMedia.getThumbnail());
    }

    private void initNewMedia() {
        Timber.e("initNewMedia");

        initPlayer(getContext());
        mPlayer.setPlayWhenReady(true);
        mPlayer.seekTo(mMedia.getTime());
    }

    @Override
    public void onMediaPause() {
        mIsPause = true;
        if (mPlayer != null && mMedia.getType() == MediaType.VIDEO) {
            Timber.e("onMediaPause");
            mPlayer.setPlayWhenReady(false);
            mPlayer.stop();
            mMedia.setTime(mPlayer.getCurrentPosition());

            mPlayer.release();
        }

    }

    @Override
    public boolean isPlaying() {
        return getPlayer() != null && getPlayer().getPlayWhenReady();
    }

    public void setOnFloatingMediaPlayVideoListener(OnFloatingMediaPlayVideoListener onFloatingMediaPlayVideoListener) {
        mOnFloatingMediaPlayVideoListener = onFloatingMediaPlayVideoListener;
    }

    public interface OnFloatingMediaPlayVideoListener {
        void onVideoPlayed();
    }
}
