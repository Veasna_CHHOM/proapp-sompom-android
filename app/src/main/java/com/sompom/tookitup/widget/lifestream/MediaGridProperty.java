package com.sompom.tookitup.widget.lifestream;

/**
 * Created by He Rotha on 9/5/17.
 */
public class MediaGridProperty {
    private int mRow;
    private int mRowSpan;
    private float mRowWeight;
    private int mCol;
    private int mColSpan;
    private float mColWeight;
    private int mMarginTop;
    private int mMarginLeft;
    private int mMarginRight;
    private int mMarginBottom;

    public MediaGridProperty(int row, int rowSpan, float rowWeight,
                             int col, int colSpan, float colWeight) {
        mRow = row;
        mRowSpan = rowSpan;
        mRowWeight = rowWeight;
        mCol = col;
        mColSpan = colSpan;
        mColWeight = colWeight;
    }

    public MediaGridProperty(int row, int rowSpan,
                             int col, int colSpan) {
        mRow = row;
        mRowSpan = rowSpan;
        mCol = col;
        mColSpan = colSpan;
        mRowWeight = 1f;
        mColWeight = 1f;
    }

    public MediaGridProperty withMargin(int marginTop, int marginLeft, int marginRight, int marginBottom) {
        mMarginTop = marginTop;
        mMarginLeft = marginLeft;
        mMarginRight = marginRight;
        mMarginBottom = marginBottom;
        return this;
    }


    public int getRow() {
        return mRow;
    }

    public int getRowSpan() {
        return mRowSpan;
    }

    public float getRowWeight() {
        return mRowWeight;
    }

    public int getCol() {
        return mCol;
    }

    public int getColSpan() {
        return mColSpan;
    }

    public float getColWeight() {
        return mColWeight;
    }

    public int getMarginTop() {
        return mMarginTop;
    }

    public int getMarginLeft() {
        return mMarginLeft;
    }

    public int getMarginRight() {
        return mMarginRight;
    }

    public int getMarginBottom() {
        return mMarginBottom;
    }

    public MediaGridProperty withRow(int row) {
        mRow = row;
        return this;
    }

    public MediaGridProperty withRowSpan(int rowSpan) {
        mRowSpan = rowSpan;
        return this;
    }

    public MediaGridProperty withRowWeight(float rowWeight) {
        mRowWeight = rowWeight;
        return this;
    }

    public MediaGridProperty withCol(int col) {
        mCol = col;
        return this;
    }

    public MediaGridProperty withColSpan(int colSpan) {
        mColSpan = colSpan;
        return this;
    }

    public MediaGridProperty withColWeight(float colWeight) {
        mColWeight = colWeight;
        return this;
    }

    public MediaGridProperty withMarginTop(int marginTop) {
        mMarginTop = marginTop;
        return this;
    }

    public MediaGridProperty withMarginLeft(int marginLeft) {
        mMarginLeft = marginLeft;
        return this;
    }

    public MediaGridProperty withMarginRight(int marginRight) {
        mMarginRight = marginRight;
        return this;
    }

    public MediaGridProperty withMarginBottom(int marginBottom) {
        mMarginBottom = marginBottom;
        return this;
    }

    public MediaGridProperty withMarginTopRight(int margin) {
        mMarginTop = margin;
        mMarginRight = margin;
        return this;
    }

    public MediaGridProperty withMarginLeftBottom(int margin) {
        mMarginBottom = margin;
        mMarginLeft = margin;
        return this;
    }

    public MediaGridProperty withMarginTopLeft(int margin) {
        mMarginTop = margin;
        mMarginLeft = margin;
        return this;
    }

    public MediaGridProperty withMarginTopLeftRight(int margin) {
        mMarginTop = margin;
        mMarginLeft = margin;
        mMarginRight = margin;
        return this;
    }

    public MediaGridProperty withMarginTopLeftBottom(int margin) {
        mMarginTop = margin;
        mMarginLeft = margin;
        mMarginBottom = margin;
        return this;
    }


}
