package com.sompom.tookitup.widget.lifestream;

/**
 * Created by He Rotha on 7/10/18.
 */
public enum Orientation {
    Vertical(1), Horizontal(2);
    private int mValue;

    Orientation(int value) {
        mValue = value;
    }

    public static Orientation fromValue(int value) {
        for (Orientation format : Orientation.values()) {
            if (format.mValue == value) {
                return format;
            }
        }
        return Vertical;
    }
}
