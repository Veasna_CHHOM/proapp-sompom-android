package com.sompom.tookitup.widget.lifestream;

import com.sompom.tookitup.helper.MyHandler;

/**
 * Created by He Rotha on 2/7/19.
 */
public class PlayingHandler extends MyHandler implements MyHandler.OnDoActionListener {
    private boolean mIsStartPlay = false;
    private OnPlayListener mOnPlayListener;

    PlayingHandler(final OnPlayListener listener) {
        super(500);
        setOnDoActionListener(this);
        mOnPlayListener = listener;
    }

    public boolean isStartPlay() {
        return mIsStartPlay;
    }

    void setStartPlay(boolean startPlay) {
        mIsStartPlay = startPlay;
    }

    @Override
    public void onDoAction() {
        mOnPlayListener.onPlay(mIsStartPlay);
    }

    public interface OnPlayListener {
        void onPlay(boolean isStartToPlay);
    }
}
