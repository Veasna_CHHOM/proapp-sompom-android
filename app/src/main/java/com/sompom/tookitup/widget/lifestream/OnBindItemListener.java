package com.sompom.tookitup.widget.lifestream;

import android.graphics.Point;
import android.view.View;

/**
 * Created by He Rotha on 7/10/18.
 */
public interface OnBindItemListener {
    void onBind(View view, int position);

    Point onRequiredWidthHeight();

}
