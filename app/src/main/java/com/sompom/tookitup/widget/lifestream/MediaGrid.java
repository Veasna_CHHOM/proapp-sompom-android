package com.sompom.tookitup.widget.lifestream;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayout;
import android.util.AttributeSet;
import android.view.View;

import com.sompom.tookitup.R;
import com.sompom.tookitup.model.MediaGridProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by He Rotha on 8/25/17.
 */

public class MediaGrid extends GridLayout {
    private Orientation mOrientation = Orientation.Vertical;
    private int mMaximumDisplay = 4;
    private MediaGridAdapter mAdapter;
    private int mMarginSize;

    public MediaGrid(Context context) {
        super(context);
    }

    public MediaGrid(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public MediaGrid(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setMaximumDisplay(int maximumDisplay) {
        mMaximumDisplay = maximumDisplay;
    }

    public void setOrientation(Orientation orientation) {
        mOrientation = orientation;
    }

    public void setAdapter(MediaGridAdapter adapter) {
        mAdapter = adapter;
        init();
    }

    public void init() {
        if (mAdapter == null || mAdapter.getItemCount() <= 0) {
            return;
        }
        mMarginSize = getResources().getDimensionPixelSize(R.dimen.media_space);
        removeAllViews();

        int displayCount;
        if (mMaximumDisplay > mAdapter.getItemCount()) {
            displayCount = mAdapter.getItemCount();
        } else {
            displayCount = mMaximumDisplay;
        }

        List<MediaGridProperty> properties = new ArrayList<>();
        if (displayCount == 1) {
            properties.add(new MediaGridProperty(0, 1, 0, 1));
        } else if (displayCount == 2) {
            if (mOrientation == Orientation.Vertical) {
                properties.add(new MediaGridProperty(0, 1, 0, 1).withMarginBottom(mMarginSize));
                properties.add(new MediaGridProperty(1, 1, 0, 1).withMarginTop(mMarginSize));
            } else {
                properties.add(new MediaGridProperty(0, 1, 0, 1).withMarginBottom(mMarginSize));
                properties.add(new MediaGridProperty(1, 1, 0, 1).withMarginTop(mMarginSize));
            }
        } else if (displayCount == 3) {
            if (mOrientation == Orientation.Vertical) {
                properties.add(new MediaGridProperty(0, 1, 2, 0, 2, 1).withMarginBottom(mMarginSize));
                properties.add(new MediaGridProperty(1, 1, 0, 1).withMarginTopRight(mMarginSize));
                properties.add(new MediaGridProperty(1, 1, 1, 1).withMarginTopLeft(mMarginSize));
            } else {
                properties.add(new MediaGridProperty(0, 2, 1, 0, 1, 2).withMarginRight(mMarginSize));
                properties.add(new MediaGridProperty(0, 1, 1, 1).withMarginLeftBottom(mMarginSize));
                properties.add(new MediaGridProperty(1, 1, 1, 1).withMarginTopLeft(mMarginSize));
            }
        } else if (displayCount == 4) {
            if (mOrientation == Orientation.Vertical) {
                properties.add(new MediaGridProperty(0, 1, 2, 0, 3, 1).withMarginBottom(mMarginSize));
                properties.add(new MediaGridProperty(1, 1, 0, 1).withMarginTopRight(mMarginSize));
                properties.add(new MediaGridProperty(1, 1, 1, 1).withMarginTopLeftRight(mMarginSize));
                properties.add(new MediaGridProperty(1, 1, 2, 1).withMarginTopLeft(mMarginSize));
            } else {
                properties.add(new MediaGridProperty(0, 3, 1, 0, 1, 2).withMarginRight(mMarginSize));
                properties.add(new MediaGridProperty(0, 1, 1, 1).withMarginLeftBottom(mMarginSize));
                properties.add(new MediaGridProperty(1, 1, 1, 1).withMarginTopLeftBottom(mMarginSize));
                properties.add(new MediaGridProperty(2, 1, 1, 1).withMarginTopLeft(mMarginSize));
            }
        } else {
            if (mOrientation == Orientation.Vertical) {
                properties.add(new MediaGridProperty(0, 1, 3, 0, 4, 1).withMarginBottom(mMarginSize));
                properties.add(new MediaGridProperty(1, 1, 0, 1).withMarginTopRight(mMarginSize));
                properties.add(new MediaGridProperty(1, 1, 1, 1).withMarginTopLeftRight(mMarginSize));
                properties.add(new MediaGridProperty(1, 1, 2, 1).withMarginTopLeftRight(mMarginSize));
                properties.add(new MediaGridProperty(1, 1, 3, 1).withMarginTopLeft(mMarginSize));
            } else {
                properties.add(new MediaGridProperty(0, 4, 1, 0, 1, 3).withMarginRight(mMarginSize));
                properties.add(new MediaGridProperty(0, 1, 1, 1).withMarginLeftBottom(mMarginSize));
                properties.add(new MediaGridProperty(1, 1, 1, 1).withMarginTopLeftBottom(mMarginSize));
                properties.add(new MediaGridProperty(2, 1, 1, 1).withMarginTopLeftBottom(mMarginSize));
                properties.add(new MediaGridProperty(3, 1, 1, 1).withMarginTopLeft(mMarginSize));
            }
        }
        for (int i = 0; i < properties.size(); i++) {
            generateView(i, mAdapter.getView(i), properties.get(i));
        }
    }

    private void generateView(int position, View view, MediaGridProperty property) {
        Spec rowSpec = GridLayout.spec(property.getRow(), property.getRowSpan(), property.getRowWeight());
        Spec colSpec = GridLayout.spec(property.getCol(), property.getColSpan(), property.getColWeight());
        LayoutParams param = new LayoutParams(rowSpec, colSpec);
        param.width = 0;
        param.height = 0;
        param.setMargins(property.getMarginLeft(), property.getMarginTop(),
                property.getMarginRight(), property.getMarginBottom());
        addView(view, param);

        mAdapter.onBind(view, position);

        //to display remain item that not display on gird
        if (position >= mMaximumDisplay - 1) {
            int remainCount = getRemainItem();
            if (remainCount > 0) {
                mAdapter.onRemainCount(view, getRemainItem());
            }
        }
    }

    public int getRemainItem() {
        int count = mAdapter.getItemCount() - mMaximumDisplay;
        if (count < 0) {
            return 0;
        } else {
            return count;
        }
    }

    public enum Orientation {
        Vertical, Horizontal
    }

}
