package com.sompom.tookitup.widget.choosedistance;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.OnItemClickListener;
import com.sompom.tookitup.model.emun.ChooseDistanceItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nuonveyo on 7/5/18.
 */

public class ChooseDistanceContainer extends LinearLayout {
    @ColorInt
    private int mSelectedColor;
    @ColorInt
    private int mUnSelectedColor;
    private int mCheckPosition;
    private List<ChooseDistanceView> mChooseDistanceViews = new ArrayList<>();
    private OnItemClickListener<ChooseDistanceItem> mOnItemClickListener;

    public ChooseDistanceContainer(Context context) {
        super(context);
        init(null);
    }

    public ChooseDistanceContainer(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public ChooseDistanceContainer(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ChooseDistanceContainer(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    public void setOnItemClickListener(OnItemClickListener<ChooseDistanceItem> onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    private void init(AttributeSet attributeSet) {
        setOrientation(HORIZONTAL);
        if (attributeSet != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attributeSet,
                    R.styleable.ChooseDistanceContainer,
                    0,
                    0);
            mSelectedColor = typedArray.getColor(R.styleable.ChooseDistanceContainer_selectedColor,
                    ContextCompat.getColor(getContext(), R.color.colorPrimary));
            mUnSelectedColor = typedArray.getColor(R.styleable.ChooseDistanceContainer_unSelectedColor, Color.WHITE);

            typedArray.recycle();
        }
    }

    public void addView(ChooseDistanceItem item, int choosePosition) {
        mCheckPosition = choosePosition;
        ChooseDistanceView distanceView = new ChooseDistanceView(getContext());
        distanceView.setId(item.getId());
        distanceView.setData(item);
        distanceView.setSelectedColor(mSelectedColor);
        distanceView.setUnSelectedColor(mUnSelectedColor);
        distanceView.setSelected(item.getId() == mCheckPosition);
        distanceView.setViewOnItemClickListener(view -> {
            if (mCheckPosition != view.getId()) {
                for (ChooseDistanceView chooseDistanceView : mChooseDistanceViews) {
                    chooseDistanceView.setSelected(chooseDistanceView.getId() == view.getId());
                }
                mCheckPosition = view.getId();
                mOnItemClickListener.onClick(ChooseDistanceItem.getValueFromId(mCheckPosition));
            }
        });
        LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        mChooseDistanceViews.add(distanceView);
        addView(distanceView, params);
    }
}
