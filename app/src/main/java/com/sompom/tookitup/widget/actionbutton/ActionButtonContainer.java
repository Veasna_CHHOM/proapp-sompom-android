package com.sompom.tookitup.widget.actionbutton;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.LinearLayout;

import com.sompom.tookitup.listener.OnItemClickListener;
import com.sompom.tookitup.model.emun.ActionButtonItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nuonveyo on 8/1/18.
 */

public class ActionButtonContainer extends LinearLayout {
    private int mCheckPosition;
    private List<ActionButtonView> mButtonViewList = new ArrayList<>();
    private OnItemClickListener<ActionButtonItem> mOnItemClickListener;

    public ActionButtonContainer(Context context) {
        super(context);
    }

    public ActionButtonContainer(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ActionButtonContainer(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ActionButtonContainer(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setOnItemClickListener(OnItemClickListener<ActionButtonItem> onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    public void addView(ActionButtonItem item) {
        ActionButtonView buttonView = new ActionButtonView(getContext());
        buttonView.setId(item.getId());
        buttonView.setData(item);
        buttonView.setSelected(getChildCount() == mCheckPosition);
        buttonView.setOnClickListener(view -> {
            if (mCheckPosition != view.getId()) {
                for (ActionButtonView actionButtonView : mButtonViewList) {
                    actionButtonView.setSelected(actionButtonView.getId() == view.getId());
                }
                mCheckPosition = view.getId();
                mOnItemClickListener.onClick(ActionButtonItem.getValueFromId(mCheckPosition));
            }
        });
        buttonView.setGravity(Gravity.CENTER);
        LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1.0f);
        mButtonViewList.add(buttonView);
        addView(buttonView, params);
    }
}
