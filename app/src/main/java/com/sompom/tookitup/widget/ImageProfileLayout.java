package com.sompom.tookitup.widget;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.sompom.tookitup.R;
import com.sompom.tookitup.chat.listener.OnPresenceListener;
import com.sompom.tookitup.chat.service.SocketService;
import com.sompom.tookitup.databinding.CustomLayoutUserImageProfileBinding;
import com.sompom.tookitup.helper.GlideApp;
import com.sompom.tookitup.model.emun.Presence;
import com.sompom.tookitup.model.emun.RoundCornerType;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.viewmodel.binding.ImageViewBindingUtil;

import java.util.Date;

import timber.log.Timber;

/**
 * Created by nuonveyo on 8/6/18.
 */

public class ImageProfileLayout extends LinearLayout implements OnPresenceListener {
    private CustomLayoutUserImageProfileBinding mBinding;
    private boolean mIsShowStatus;
    private RenderType mRenderType = RenderType.USER;
    private int mImageDimension;
    private ImageSize mImageSize;
    private SocketService.SocketBinder mBinder;
    private User mUser;

    public ImageProfileLayout(Context context) {
        super(context);
        init(null);
    }

    public ImageProfileLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public ImageProfileLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    public void setImageDimension(int imageDimension) {
        mImageDimension = imageDimension;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ImageProfileLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    private void init(AttributeSet attr) {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.custom_layout_user_image_profile,
                this,
                true);

        if (attr != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attr,
                    R.styleable.ImageProfileLayout,
                    0,
                    0);
            mImageDimension = typedArray.getDimensionPixelSize(R.styleable.ImageProfileLayout_dimension, 0);
            mImageSize = ImageSize.fromValue(typedArray.getInt(R.styleable.ImageProfileLayout_size, ImageSize.LARGE.mValue));
            mRenderType = RenderType.fromValue(typedArray.getInt(R.styleable.ImageProfileLayout_render, RenderType.USER.mValue));
            mIsShowStatus = typedArray.getBoolean(R.styleable.ImageProfileLayout_status, true);

            validationLayout();
            typedArray.recycle();
        }
    }

    public void setUser(User user) {
        Timber.i("mIsShowStatus: " + mIsShowStatus);
        mUser = user;
        if (mRenderType == RenderType.USER) {
            ImageViewBindingUtil.setUserUrl(mBinding.imageViewProfile, user);
            if (mIsShowStatus) {
                setStatus(user);
            }
        } else {
            ImageViewBindingUtil.setUserUrl(mBinding.imageViewActive, user);
            String coverUrl = user.getUserCoverProfileThumbnail();
            if (TextUtils.isEmpty(coverUrl)) {
                coverUrl = user.getUserCoverProfile();
            }
            setChatMultiImageView(mBinding.imageViewProfile, coverUrl);
        }
    }

    public void loadSimpleUrl(String url) {
        ImageViewBindingUtil.loadImageUrl(mBinding.imageViewProfile, url, 150, 150, null, true);
        mBinding.imageViewActive.setVisibility(GONE);
    }

    public void setUser(User user, int dimension) {
        mImageDimension = dimension;
        mImageSize = ImageSize.LARGE;
        setUser(user);
        validationLayout();
    }

    private void setChatMultiImageView(ImageView imageView, String imageUrl) {
        Drawable drawable = ContextCompat.getDrawable(imageView.getContext(), R.drawable.ic_photo_placeholder_200dp);
        GlideApp.with(imageView)
                .load(imageUrl)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .placeholder(drawable)
                .override(0, 100)
                .into(imageView);
    }

    private int getDimension(boolean isLarge) {
        if (isLarge) {
            return getContext().getResources().getDimensionPixelSize(R.dimen.recently_message_user_profile_size);
        } else {
            return getContext().getResources().getDimensionPixelSize(R.dimen.user_profile_medium_size);
        }
    }

    private void setStatus(User user) {
        if (mBinder == null && getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).addOnServiceListener(binder -> {
                mBinder = (SocketService.SocketBinder) binder;
                mBinder.setUserStatus(user.getId(), user.isOnline(), user.getLastActivity());

                if (isAttachedToWindow()) {
                    mBinder.addPresenceListener(ImageProfileLayout.this);
                }
            });
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (mUser != null && mBinder != null && mIsShowStatus) {
            mBinder.addPresenceListener(ImageProfileLayout.this);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mBinder != null) {
            mBinder.removePresenceListener(ImageProfileLayout.this);
        }
    }

    public void setRenderType(RenderType renderType) {
        if (renderType == null) {
            return;
        }
        mRenderType = renderType;
        validationLayout();
    }

    private void validationLayout() {
        if (mImageDimension <= 0) {
            mImageDimension = getDimension(mImageSize == ImageSize.LARGE);
        }

        Timber.i("mImageDimension: " + mImageDimension + ", mRenderType: " + mRenderType);

        if (mRenderType == RenderType.USER) {
            mBinding.imageViewProfile.getLayoutParams().width = mImageDimension;
            mBinding.imageViewProfile.getLayoutParams().height = mImageDimension;

            ((MarginLayoutParams) mBinding.imageViewProfile.getLayoutParams()).bottomMargin = 0;
            ((MarginLayoutParams) mBinding.imageViewProfile.getLayoutParams()).rightMargin = 0;

            if (mIsShowStatus) {
                mBinding.imageViewActive.setVisibility(VISIBLE);
                int iconSize = mImageDimension * 35 / 100;
                mBinding.imageViewActive.getLayoutParams().width = iconSize;
                mBinding.imageViewActive.getLayoutParams().height = iconSize;
                mBinding.imageViewActive.setBackgroundResource(R.drawable.oval_white_bg);
            } else {
                mBinding.imageViewActive.setVisibility(GONE);
            }

        } else {
            mBinding.imageViewProfile.setCircle(false);
            mBinding.imageViewProfile.setCornerType(RoundCornerType.Single);
            int space = getResources().getDimensionPixelSize(R.dimen.space_medium);
            mBinding.imageViewProfile.setMediumRadius(space);

            mBinding.imageViewProfile.getLayoutParams().width = mImageDimension;
            mBinding.imageViewProfile.getLayoutParams().height = mImageDimension;

            ((MarginLayoutParams) mBinding.imageViewProfile.getLayoutParams()).bottomMargin = mImageDimension / 9;
            ((MarginLayoutParams) mBinding.imageViewProfile.getLayoutParams()).rightMargin = mImageDimension / 9;

            mBinding.imageViewActive.getLayoutParams().width = (int) (mImageDimension / 1.6);
            mBinding.imageViewActive.getLayoutParams().height = (int) (mImageDimension / 1.6);
        }
    }

    @Override
    public void onPresenceUpdate(String userId, Presence presence, Date lastOnlineTime) {
        Timber.i("onPresenceUpdate: " + presence);
        if (mUser != null && TextUtils.equals(userId, mUser.getId())) {
            ((Activity) getContext()).runOnUiThread(() -> {

                if (presence == Presence.Online) {
                    mBinding.imageViewActive.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.oval_green));
                    mBinding.imageViewActive.setVisibility(VISIBLE);
                } else if (presence == Presence.Offline) {
                    mBinding.imageViewActive.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.oval_dark_gray));
                    mBinding.imageViewActive.setVisibility(VISIBLE);
                } else {
                    mBinding.imageViewActive.setVisibility(GONE);
                }
            });
        }
    }

    @Override
    public String getUserId() {
        if (mUser == null) {
            return null;
        }
        return mUser.getId();
    }

    public enum RenderType {
        USER(1),
        SHOP(2);

        private final int mValue;

        RenderType(int value) {
            mValue = value;
        }

        private static RenderType fromValue(int value) {
            for (RenderType renderType : values()) {
                if (renderType.mValue == value) {
                    return renderType;
                }
            }
            return USER;
        }

    }

    public enum ImageSize {
        LARGE(1),
        MEDIUM(2);

        private final int mValue;

        ImageSize(int value) {
            mValue = value;
        }

        private static ImageSize fromValue(int value) {
            for (ImageSize renderType : values()) {
                if (renderType.mValue == value) {
                    return renderType;
                }
            }
            return LARGE;
        }

    }
}
