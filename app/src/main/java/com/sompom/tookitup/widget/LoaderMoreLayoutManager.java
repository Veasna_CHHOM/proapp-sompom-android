package com.sompom.tookitup.widget;

import android.content.Context;
import android.graphics.PointF;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by he.rotha on 3/7/16.
 */
public class LoaderMoreLayoutManager extends LinearLayoutManager {

    private boolean mIsLoadingMore = false;
    private OnLoadMoreCallback mOnLoadMoreListener;
    private LinearSmoothScroller mLinearSmoothScroller;

    public LoaderMoreLayoutManager(final Context context) {
        super(context);
    }

    public LoaderMoreLayoutManager(final Context context, final int orientation, final boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    public LoaderMoreLayoutManager(final Context context, final AttributeSet attrs,
                                   final int defStyleAttr, final int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public int scrollVerticallyBy(final int dy, final RecyclerView.Recycler recycler, final RecyclerView.State state) {
        if (mOnLoadMoreListener == null) {
            return super.scrollVerticallyBy(dy, recycler, state);
        }

        if (!mIsLoadingMore && findLastVisibleItemPosition() >= getItemCount() - 1) {
            mIsLoadingMore = true;
            mOnLoadMoreListener.onLoadMoreFromBottom();
        }
        return super.scrollVerticallyBy(dy, recycler, state);
    }

    public void setOnLoadMoreListener(final OnLoadMoreCallback onLoadMoreListener) {
        mOnLoadMoreListener = onLoadMoreListener;
    }

    public void loadingFinished() {
        mIsLoadingMore = false;
    }

    @Override
    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state,
                                       int position) {
        if (mLinearSmoothScroller == null) {
            mLinearSmoothScroller = new TopSnappedSmoothScroller(recyclerView.getContext());
        }
        mLinearSmoothScroller.setTargetPosition(position);
        startSmoothScroll(mLinearSmoothScroller);
    }

    public interface OnLoadMoreCallback {
        void onLoadMoreFromBottom();
    }

    private class TopSnappedSmoothScroller extends LinearSmoothScroller {
        public TopSnappedSmoothScroller(Context context) {
            super(context);

        }

        @Override
        public PointF computeScrollVectorForPosition(int targetPosition) {
            return LoaderMoreLayoutManager.this
                    .computeScrollVectorForPosition(targetPosition);
        }

        @Override
        protected int getVerticalSnapPreference() {
            return SNAP_TO_START;
        }
    }
}
