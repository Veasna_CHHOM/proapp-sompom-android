package com.sompom.tookitup.widget;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.newadapter.StoreViewPagerAdapter;
import com.sompom.tookitup.listener.OnHomeMenuClick;
import com.sompom.tookitup.listener.OnTabSelectListener;
import com.sompom.tookitup.model.emun.Status;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.fragment.MyWallStreetFragment;
import com.sompom.tookitup.newui.fragment.ProductStoreFragment;
import com.sompom.tookitup.viewmodel.newviewmodel.CustomProductTabViewModel;
import com.sompom.tookitup.widget.tablayout.CustomTabLayout;

import java.util.Arrays;

/**
 * Created by He Rotha on 3/20/19.
 */
public class SellerStoreTabLayout extends CustomTabLayout implements CustomTabLayout.OnTabSelectedListener {
    private MyWallStreetFragment mWallStreetFragment;
    private ProductStoreFragment mOnSellFragment;
    private CustomProductTabViewModel mWallStreetTabViewModel;
    private CustomProductTabViewModel mOnSellTabViewModel;
    private CustomProductTabViewModel mOldSaleTabViewModel;
    private StoreViewPagerAdapter mAdapter;

    public SellerStoreTabLayout(Context context) {
        super(context);
    }

    public SellerStoreTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SellerStoreTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setUpViewPager(FragmentManager fragmentManager, ViewPager viewPager, User user) {
        mWallStreetFragment = MyWallStreetFragment.newInstance(user.getId());
        mOnSellFragment = ProductStoreFragment.newInstance(user, Status.ON_SALE);
        ProductStoreFragment oldSaleFragment = ProductStoreFragment.newInstance(user, Status.SOLD);
        mAdapter = new StoreViewPagerAdapter(fragmentManager, Arrays.asList(mWallStreetFragment, mOnSellFragment, oldSaleFragment));
        viewPager.setAdapter(mAdapter);
        viewPager.setOffscreenPageLimit(mAdapter.getCount());
        super.setupWithViewPager(viewPager);

        ViewDataBinding wallStreetTab = getCustomTabLayout();
        mWallStreetTabViewModel = new CustomProductTabViewModel(getResources().getString(R.string.home_wall_street_title), 0);
        mWallStreetTabViewModel.setChangeTitleStyle(true);
        wallStreetTab.setVariable(BR.viewModel, mWallStreetTabViewModel);
        getTabAt(0).setCustomView(wallStreetTab.getRoot());

        ViewDataBinding onSellTab = getCustomTabLayout();
        mOnSellTabViewModel = new CustomProductTabViewModel(getResources().getString(R.string.seller_store_store_items_tab), user.getStoreItems());
        onSellTab.setVariable(BR.viewModel, mOnSellTabViewModel);
        getTabAt(1).setCustomView(onSellTab.getRoot());

        ViewDataBinding oldSaleTab = getCustomTabLayout();
        mOldSaleTabViewModel = new CustomProductTabViewModel(getContext().getString(R.string.seller_store_old_sells_tab), user.getOldSell());
        oldSaleTab.setVariable(BR.viewModel, mOldSaleTabViewModel);
        getTabAt(2).setCustomView(oldSaleTab.getRoot());

        addOnTabSelectedListener(this);
    }

    private ViewDataBinding getCustomTabLayout() {
        return DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.layout_custom_tab_seller_store, this, false);
    }

    @Override
    public void onTabSelected(CustomTabLayout.Tab tab) {
        Fragment fragment = mAdapter.getItem(tab.getPosition());
        checkTabMenuClick(fragment);
        if (tab.getPosition() == 0) {
            mWallStreetTabViewModel.setChangeTitleStyle(true);
        } else if (tab.getPosition() == 1) {
            mOnSellTabViewModel.setChangeTitleStyle(true);
        } else {
            mOldSaleTabViewModel.setChangeTitleStyle(true);
        }
        if (fragment instanceof OnTabSelectListener) {
            ((OnTabSelectListener) fragment).onTabSelected(true);
            fragment.onResume();
        }
    }

    @Override
    public void onTabUnselected(CustomTabLayout.Tab tab) {

        if (tab.getPosition() == 0) {
            mWallStreetTabViewModel.setChangeTitleStyle(false);
        } else if (tab.getPosition() == 1) {
            mOnSellTabViewModel.setChangeTitleStyle(false);
        } else {
            mOldSaleTabViewModel.setChangeTitleStyle(false);
        }
        Fragment fragment = mAdapter.getItem(tab.getPosition());
        if (fragment instanceof OnTabSelectListener) {
            ((OnTabSelectListener) fragment).onTabSelected(false);
            fragment.onPause();
        }
    }

    @Override
    public void onTabReselected(CustomTabLayout.Tab tab) {
        if (tab.getPosition() == 0 && (mWallStreetFragment != null && mWallStreetFragment.findFirstVisibleItemPosition() > 0)) {
            mWallStreetFragment.shouldScrollTop(false);
        }
    }

    private void checkTabMenuClick(Fragment fragment) {
        if (fragment instanceof OnHomeMenuClick) {
            ((OnHomeMenuClick) fragment).onTabMenuClick();
        }
    }

    public void addNewProduct() {
        if (mOnSellFragment.isAdded()) {
            mOnSellFragment.refreshData();
        }
        mOnSellTabViewModel.addCounter(1);
    }

    public void deleteProduct() {
        mOldSaleTabViewModel.addCounter(-1);
    }

    public boolean isShouldScrollToTop() {
        if (mWallStreetFragment != null && mWallStreetFragment.findFirstVisibleItemPosition() > 0) {
            mWallStreetFragment.shouldScrollTop(true);
            return true;
        }
        return false;
    }

}
