package com.sompom.tookitup.widget;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.LayoutLinkPreviewBinding;
import com.sompom.tookitup.helper.GlideApp;
import com.sompom.tookitup.model.result.LinkPreviewModel;
import com.sompom.tookitup.utils.GlideLoadUtil;

/**
 * Created by nuonveyo on 11/2/18.
 */

public class LinkPreviewLayout extends LinearLayout {
    private LayoutLinkPreviewBinding mBinding;
    private com.sompom.tookitup.listener.OnClickListener mOnCloseLinkPreviewListener;

    public LinkPreviewLayout(Context context) {
        super(context);
        init();
    }

    public LinkPreviewLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LinkPreviewLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public LinkPreviewLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void setOnCloseLinkPreviewListener(com.sompom.tookitup.listener.OnClickListener onCloseLinkPreviewListener) {
        mOnCloseLinkPreviewListener = onCloseLinkPreviewListener;
    }

    private void init() {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.layout_link_preview,
                this,
                true);
        mBinding.textViewClose.setOnClickListener(v -> {
            if (mOnCloseLinkPreviewListener != null) {
                hideLayout();
                mOnCloseLinkPreviewListener.onClick();
            }
        });
    }

    public void showLoading(boolean isShow) {
        mBinding.loading.setVisibility(isShow ? VISIBLE : GONE);
    }

    public void showLayout() {
        mBinding.containerLink.setVisibility(GONE);
        showLoading(true);
        setVisibility(VISIBLE);
    }

    public void hideLayout() {
        mBinding.containerLink.setVisibility(GONE);
        showLoading(false);
        setVisibility(GONE);
    }

    public void showGifLinkFromKeyboard(String link) {
        checkToShowLinkContent(false);
        setVisibility(VISIBLE);
        mBinding.textViewClose.setVisibility(GONE);
        mBinding.containerLink.setVisibility(VISIBLE);
        showLoading(true);
        checkToShowLinkGif(true);
        GlideApp
                .with(getContext())
                .asGif()
                .load(link)
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .listener(new RequestListener<GifDrawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<GifDrawable> target, boolean isFirstResource) {
                        showLoading(false);
                        mBinding.textViewClose.setVisibility(VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GifDrawable resource, Object model, Target<GifDrawable> target, DataSource dataSource, boolean isFirstResource) {
                        mBinding.textViewClose.setVisibility(VISIBLE);
                        showLoading(false);
                        return false;
                    }
                })
                .into(mBinding.imageViewGifLink);
    }

    public void setLinkPreviewModel(LinkPreviewModel linkPreviewModel, boolean isShowLink) {
        if (!TextUtils.isEmpty(linkPreviewModel.getLogo())) {
            GlideLoadUtil.load(mBinding.imageViewLogo, linkPreviewModel.getLogo(), false);
        } else {
            mBinding.imageViewLogo.setVisibility(GONE);
        }
        mBinding.textViewTitle.setText(linkPreviewModel.getTitle());
        mBinding.textViewDescription.setText(linkPreviewModel.getDescription());
        checkToShowLinkGif(false);
        checkToShowLinkContent(true);
        showLoading(false);
        mBinding.containerLink.setVisibility(VISIBLE);
    }

    public void checkToShowLinkContent(boolean isShow) {
        mBinding.cardViewTitle.setVisibility(isShow ? VISIBLE : GONE);
    }

    public void checkToShowLinkGif(boolean isShow) {
        mBinding.imageViewGifLink.setVisibility(isShow ? VISIBLE : GONE);
    }
}
