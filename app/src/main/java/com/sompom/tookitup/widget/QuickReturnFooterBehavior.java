package com.sompom.tookitup.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;

import com.sompom.tookitup.helper.AnimationHelper;

/**
 * CoordinatorLayout Behavior for a quick return footer
 * <p>
 * When a nested ScrollView is scrolled down, the quick return view will disappear.
 * When the ScrollView is scrolled back up, the quick return view will reappear.
 *
 * @author bherbst
 */
@SuppressWarnings("unused")
public class QuickReturnFooterBehavior extends CoordinatorLayout.Behavior<View> {
    private static final int ANIM_STATE_NONE = 0;
    private static final int ANIM_STATE_HIDING = 1;
    private static final int ANIM_STATE_SHOWING = 2;

    private int animState = ANIM_STATE_NONE;

    private int mDySinceDirectionChange;

    public QuickReturnFooterBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onStartNestedScroll(@NonNull CoordinatorLayout coordinatorLayout, @NonNull View child, @NonNull View directTargetChild, @NonNull View target, int nestedScrollAxes, int type) {
        return (nestedScrollAxes & ViewCompat.SCROLL_AXIS_VERTICAL) != 0;
    }

    @Override
    public void onNestedPreScroll(@NonNull CoordinatorLayout coordinatorLayout, @NonNull View child, @NonNull View target, int dx, int dy, @NonNull int[] consumed, int type) {
        if (dy > 0 && mDySinceDirectionChange < 0
                || dy < 0 && mDySinceDirectionChange > 0) {
            // We detected a direction change -- reset our cumulative delta Y
            mDySinceDirectionChange = 0;
        }

        mDySinceDirectionChange += dy;

        if (mDySinceDirectionChange > child.getHeight() && !isOrWillBeHidden(child)) {
            AnimationHelper.hideFabButton(child, new AnimatorListenerAdapter() {
                private boolean isCanceled = false;

                @Override
                public void onAnimationStart(Animator animation) {
                    animState = ANIM_STATE_HIDING;
                    isCanceled = false;
                    child.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    isCanceled = true;
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    animState = ANIM_STATE_NONE;
                    if (!isCanceled) {
                        child.setVisibility(View.INVISIBLE);
                    }
                }
            });
        } else if (mDySinceDirectionChange < 0 && !isOrWillBeShown(child)) {
            AnimationHelper.showFabButton(child, new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animator) {
                    animState = ANIM_STATE_SHOWING;
                    child.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    animState = ANIM_STATE_NONE;
                }
            });
        }
    }

    private boolean isOrWillBeHidden(View view) {
        if (view.getVisibility() == View.VISIBLE) {
            return animState == ANIM_STATE_HIDING;
        } else {
            return animState != ANIM_STATE_SHOWING;
        }
    }

    private boolean isOrWillBeShown(View view) {
        if (view.getVisibility() != View.VISIBLE) {
            return animState == ANIM_STATE_SHOWING;
        } else {
            return animState != ANIM_STATE_HIDING;
        }
    }
}