package com.sompom.tookitup.widget;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.CustomLayoutParticipantsImageProfileBinding;
import com.sompom.tookitup.model.result.User;

import java.util.List;

import timber.log.Timber;

public class RecipientProfileLayout extends LinearLayout {

    private static final int MULTIPLY_BY = 2;

    private CustomLayoutParticipantsImageProfileBinding mBinding;
    private int mChildSize;
    private int mProfileSize;

    public RecipientProfileLayout(Context context) {
        super(context);
        init();
    }

    public RecipientProfileLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RecipientProfileLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public RecipientProfileLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.custom_layout_participants_image_profile,
                this,
                true);
    }

    private void setChildWidth() {
        post(() -> {
            mChildSize = getWidth() / 2;
            //Minus the padding which apply for profile display.
            mProfileSize = mChildSize - (getResources().getDimensionPixelSize(R.dimen.group_profile_border) * MULTIPLY_BY);
            Timber.i("getWidth: " + getWidth() +
                    ", mChildSize: " + mChildSize +
                    " mProfileSize: " + mProfileSize);

            //For common profile
            setGroupChildSizeInternal(mBinding.profileImageView1,
                    mBinding.profileImageView2,
                    mBinding.profileImageView3,
                    mBinding.profileImageView4);

            //For the fifth profile
            setChildSizeInternal(mBinding.profileImageView5,
                    mChildSize,
                    (mChildSize - (getResources().getDimensionPixelSize(R.dimen.space_too_small) * MULTIPLY_BY)),
                    getResources().getDimensionPixelSize(R.dimen.space_too_small));

            //Single profile
            setChildSizeInternal(mBinding.profileImageView5,
                    getResources().getDimensionPixelSize(R.dimen.recently_message_user_profile_size),
                    (getResources().getDimensionPixelSize(R.dimen.recently_message_user_profile_size)
                            - (getResources().getDimensionPixelSize(R.dimen.group_profile_border) * MULTIPLY_BY)),
                    getResources().getDimensionPixelSize(R.dimen.group_profile_border));

            //More text
            setMoreTextSize();
        });
    }

    private void setMoreTextSize() {
        mBinding.textViewMore.getLayoutParams().width = mChildSize;
        mBinding.textViewMore.getLayoutParams().height = mChildSize;
    }

    private void setGroupChildSizeInternal(ImageProfileLayout... imageProfileLayouts) {
        for (ImageProfileLayout imageProfileLayout : imageProfileLayouts) {
            setChildSizeInternal(imageProfileLayout,
                    mChildSize,
                    mProfileSize,
                    getResources().getDimensionPixelSize(R.dimen.group_profile_border));
        }
    }

    private void setChildSizeInternal(ImageProfileLayout imageProfileLayout,
                                      int rootSize,
                                      int profileSize,
                                      int padding) {
        LinearLayout.LayoutParams params = new LayoutParams(rootSize, rootSize);
        imageProfileLayout.setImageDimension(rootSize);
//        imageProfileLayout.setPadding(padding, padding, padding, padding);
//        imageProfileLayout.getLayoutParams().width = rootSize;
//        imageProfileLayout.getLayoutParams().height = rootSize;

        imageProfileLayout.setLayoutParams(params);
    }

    public void bindData(List<User> participants) {
        //TODO: Need to remove
//        if (participants.size() == 4) {
//            List<User> test = new ArrayList<>();
//            for (int i = 0; i < 3; i++) {
//                test.add(participants.get(i));
//            }
//
//            participants = test;
//        } else if (participants.size() > 5) {
//            List<User> test = new ArrayList<>();
//            for (int i = 0; i < 2; i++) {
//                test.add(participants.get(i));
//            }
//
//            participants = test;
//        }

        if (participants.size() == 1) {
            mBinding.squareRoot.setVisibility(View.GONE);
            mBinding.textViewMore.setVisibility(View.GONE);
            mBinding.profileImageView5.setVisibility(View.GONE);
            mBinding.profileImageViewSingle.setVisibility(View.VISIBLE);

            mBinding.profileImageViewSingle.setUser(participants.get(0));
        } else if (participants.size() == 2) {
            mBinding.squareRoot.setVisibility(View.VISIBLE);
            mBinding.subSquareFirstRowRoot.setVisibility(View.VISIBLE);
            setChildVisibility(mBinding.subSquareFirstRowRoot, true);
            mBinding.subSquareSecondRowRoot.setVisibility(View.GONE);
            mBinding.textViewMore.setVisibility(View.GONE);
            mBinding.profileImageViewSingle.setVisibility(View.GONE);
            mBinding.profileImageView5.setVisibility(View.GONE);

            mBinding.profileImageView1.setUser(participants.get(0));
            mBinding.profileImageView2.setUser(participants.get(1));
        } else if (participants.size() == 3) {
            mBinding.squareRoot.setVisibility(View.VISIBLE);
            mBinding.subSquareFirstRowRoot.setVisibility(View.VISIBLE);
            mBinding.subSquareSecondRowRoot.setVisibility(View.VISIBLE);
            setChildVisibility(mBinding.subSquareFirstRowRoot, true);
            setChildVisibility(mBinding.subSquareSecondRowRoot, true);
            mBinding.profileImageView2.setVisibility(View.GONE);
            mBinding.textViewMore.setVisibility(View.GONE);
            mBinding.profileImageViewSingle.setVisibility(View.GONE);
            mBinding.profileImageView5.setVisibility(View.GONE);

            mBinding.profileImageView1.setUser(participants.get(0));
            mBinding.profileImageView3.setUser(participants.get(1));
            mBinding.profileImageView4.setUser(participants.get(2));
        } else if (participants.size() == 4) {
            mBinding.squareRoot.setVisibility(View.VISIBLE);
            mBinding.subSquareFirstRowRoot.setVisibility(View.VISIBLE);
            mBinding.subSquareSecondRowRoot.setVisibility(View.VISIBLE);
            setChildVisibility(mBinding.subSquareFirstRowRoot, true);
            setChildVisibility(mBinding.subSquareSecondRowRoot, true);
            mBinding.textViewMore.setVisibility(View.GONE);
            mBinding.profileImageViewSingle.setVisibility(View.GONE);
            mBinding.profileImageView5.setVisibility(View.GONE);

            mBinding.profileImageView1.setUser(participants.get(0));
            mBinding.profileImageView2.setUser(participants.get(1));
            mBinding.profileImageView3.setUser(participants.get(2));
            mBinding.profileImageView4.setUser(participants.get(3));
        } else if (participants.size() == 5) {
            mBinding.squareRoot.setVisibility(View.VISIBLE);
            mBinding.subSquareFirstRowRoot.setVisibility(View.VISIBLE);
            mBinding.subSquareSecondRowRoot.setVisibility(View.VISIBLE);
            setChildVisibility(mBinding.subSquareFirstRowRoot, true);
            setChildVisibility(mBinding.subSquareSecondRowRoot, true);
            mBinding.profileImageView5.setVisibility(View.VISIBLE);
            mBinding.textViewMore.setVisibility(View.GONE);
            mBinding.profileImageViewSingle.setVisibility(View.GONE);

            mBinding.profileImageView1.setUser(participants.get(0));
            mBinding.profileImageView2.setUser(participants.get(1));
            mBinding.profileImageView3.setUser(participants.get(2));
            mBinding.profileImageView4.setUser(participants.get(3));
            mBinding.profileImageView5.setUser(participants.get(4));
        } else {
            //More than 5 participants
            mBinding.squareRoot.setVisibility(View.VISIBLE);
            mBinding.subSquareFirstRowRoot.setVisibility(View.VISIBLE);
            mBinding.subSquareSecondRowRoot.setVisibility(View.VISIBLE);
            setChildVisibility(mBinding.subSquareFirstRowRoot, true);
            setChildVisibility(mBinding.subSquareSecondRowRoot, true);
            mBinding.profileImageView5.setVisibility(View.GONE);
            mBinding.profileImageViewSingle.setVisibility(View.GONE);
            mBinding.textViewMore.setVisibility(View.VISIBLE);

            mBinding.profileImageView1.setUser(participants.get(0));
            mBinding.profileImageView2.setUser(participants.get(1));
            mBinding.profileImageView3.setUser(participants.get(2));
            mBinding.profileImageView4.setUser(participants.get(3));
            mBinding.textViewMore.setText(String.format("+%s", String.valueOf(participants.size() - 4)));
        }
    }

    private void setChildVisibility(ViewGroup root, boolean enable) {
        for (int i = 0; i < root.getChildCount(); i++) {
            root.getChildAt(i).setVisibility(enable ? View.VISIBLE : View.GONE);
        }
    }
}

