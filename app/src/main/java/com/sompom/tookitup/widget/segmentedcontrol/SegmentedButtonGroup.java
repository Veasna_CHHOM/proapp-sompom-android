package com.sompom.tookitup.widget.segmentedcontrol;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.OnSegmentedClickListener;
import com.sompom.tookitup.model.emun.SegmentedControlItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nuonveyo on 6/26/18.
 */

public class SegmentedButtonGroup extends LinearLayout {
    private int mCheckPosition;
    @ColorInt
    private int mSelectedTextColor;
    @ColorInt
    private int mUnSelectedTextColor;
    private List<SegmentedButton> mSegmentedButtons = new ArrayList<>();
    private OnSegmentedClickListener mClickListener;

    public SegmentedButtonGroup(Context context) {
        super(context);
        init(null);
    }

    public SegmentedButtonGroup(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public SegmentedButtonGroup(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attributeSet) {
        setOrientation(HORIZONTAL);
        if (attributeSet != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attributeSet,
                    R.styleable.SegmentedButtonGroup,
                    0,
                    0);
            mCheckPosition = typedArray.getInt(R.styleable.SegmentedButtonGroup_checkedPosition, 0);
            mSelectedTextColor = typedArray.getColor(R.styleable.SegmentedButtonGroup_selectedBackgroundColor, 0);
            mUnSelectedTextColor = typedArray.getColor(R.styleable.SegmentedButtonGroup_unSelectedBackgroundColor, 0);

            typedArray.recycle();
        }
    }

    public void addChildView(int id, @StringRes int title, boolean isLast) {
        SegmentedButton segmentedButton = new SegmentedButton(getContext());
        segmentedButton.setId(id);
        segmentedButton.setSelectedTextColor(mSelectedTextColor);
        segmentedButton.setUnSelectedTextColor(mUnSelectedTextColor);
        segmentedButton.setTitle(title);
        if (getChildCount() == 0) {
            segmentedButton.setSelectedBackground(R.drawable.segment_first_selected_background);
            segmentedButton.setUnSelectedBackground(R.drawable.segment_first_unselected_background);
        } else if (isLast) {
            segmentedButton.setSelectedBackground(R.drawable.segment_last_selected_background);
            segmentedButton.setUnSelectedBackground(R.drawable.segment_last_unselected_background);
        } else {
            segmentedButton.setSelectedBackground(R.drawable.segment_middle_selected_background);
            segmentedButton.setUnSelectedBackground(R.drawable.segment_middle_unselected_background);
            segmentedButton.setDisableSetLayerType(true);
        }
        segmentedButton.setIsSelected(getChildCount() == mCheckPosition);
        segmentedButton.setOnSegmentedButtonClick(view -> {
            if (mCheckPosition != view.getId()) {
                for (SegmentedButton button : mSegmentedButtons) {
                    button.setIsSelected(view.getId() == button.getId());
                }
                mCheckPosition = view.getId();
                mClickListener.onClick(SegmentedControlItem.getSegmentedControlItem(mCheckPosition));
            }
        });
        mSegmentedButtons.add(segmentedButton);
        addView(segmentedButton);
    }

    public void setOnSegmentedItemClickListener(OnSegmentedClickListener listener) {
        mClickListener = listener;
    }

}
