package com.sompom.tookitup.widget;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;

import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.sompom.tookitup.model.AdsItem;
import com.sompom.tookitup.viewmodel.binding.ImageViewBindingUtil;

import timber.log.Timber;

/**
 * Created by He Rotha on 2/6/18.
 */

public class ChichaAdsView extends android.support.v7.widget.AppCompatImageView {

    public ChichaAdsView(@NonNull Context context) {
        super(context);
    }

    public ChichaAdsView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ChichaAdsView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    public void loadAds(AdsItem adsItem) {

        NativeCustomTemplateAd ads = adsItem.getAds();
        try {
            if (ads == null) {
                ImageViewBindingUtil.setAdsImage(this, adsItem);
                return;
            }
            Uri uri = ads.getImage("BannerFile").getUri();
            ImageViewBindingUtil.loadImageUrl(this, uri.toString(), 0, 0, null, false);
            ads.recordImpression();
        } catch (Exception e) {
            Timber.e("loadAds error: %s", e.getMessage());
        }
    }

    private String getPlaceAdsData(CharSequence data) {
        if (!TextUtils.isEmpty(data)) {
            return data.toString();
        }
        return "";
    }

}
