package com.sompom.tookitup.widget;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.LayoutCustomHomeTabBinding;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.listener.OnTabSelectListener;
import com.sompom.tookitup.model.result.UserBadge;
import com.sompom.tookitup.newui.fragment.AbsBaseFragment;
import com.sompom.tookitup.viewmodel.newviewmodel.LayoutCustomHomeTabViewModel;

import timber.log.Timber;

/**
 * Created by He Rotha on 9/20/18.
 */
public class HomeTabLayout extends TabLayout implements OnClickListener {
    private final LayoutCustomHomeTabViewModel[] mViewModels
            = new LayoutCustomHomeTabViewModel[HomeMenu.values().length];
    private OnTabLayoutChangeListener mTabLayoutChangeListener;
    private HomeViewPager mHomeViewPager;

    public HomeTabLayout(Context context) {
        super(context);
    }

    public HomeTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HomeTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setTabLayoutChangeListener(OnTabLayoutChangeListener tabLayoutChangeListener) {
        mTabLayoutChangeListener = tabLayoutChangeListener;
    }

    public void setupWithViewPager(@Nullable HomeViewPager viewPager) {
        super.setupWithViewPager(viewPager);

        if (viewPager == null) {
            return;
        }

        mHomeViewPager = viewPager;

        final LayoutInflater inflater = LayoutInflater.from(getContext());
        for (int i = 0; i < HomeMenu.values().length; i++) {
            final HomeMenu homeMenu = HomeMenu.values()[i];
            LayoutCustomHomeTabBinding binding =
                    DataBindingUtil.inflate(inflater, R.layout.layout_custom_home_tab, this, false);
            final LayoutCustomHomeTabViewModel viewModel = new LayoutCustomHomeTabViewModel(getContext(),
                    homeMenu.getTitle(),
                    homeMenu.getIcon());
            binding.setVariable(BR.viewModel, viewModel);

            mViewModels[i] = viewModel;
            Tab tab = getTabAt(i);
            if (tab != null) {
                tab.setCustomView(binding.getRoot());
            }
        }

        mViewModels[viewPager.getCurrentItem()].changeStyle(true);
        if (mTabLayoutChangeListener != null) {
            mTabLayoutChangeListener.onPageChanged(mHomeViewPager.getCurrentFragment(), mHomeViewPager.getCurrentItem());
        }
        addOnTabSelectedListener(new OnTabSelectedListener() {
            @Override
            public void onTabSelected(Tab tab) {
                mViewModels[tab.getPosition()].changeStyle(true);
                AbsBaseFragment fr = mHomeViewPager.getFragmentAt(tab.getPosition());
                if (fr instanceof OnTabSelectListener) {
                    ((OnTabSelectListener) fr).onTabSelected(true);
                    fr.onResume();
                }
                mTabLayoutChangeListener.onPageChanged(fr, tab.getPosition());
                //Clear the badge for Contact tab
                if (tab.getPosition() == 2) {
                    mViewModels[tab.getPosition()].setBadgeValue(0);
                }
            }

            @Override
            public void onTabUnselected(Tab tab) {
                AbsBaseFragment fr = mHomeViewPager.getFragmentAt(tab.getPosition());
                if (fr instanceof OnTabSelectListener) {
                    ((OnTabSelectListener) fr).onTabSelected(false);
                    fr.onPause();
                }
                mViewModels[tab.getPosition()].changeStyle(false);
            }

            @Override
            public void onTabReselected(Tab tab) {
                mTabLayoutChangeListener.onPageReselect(mHomeViewPager.getFragmentAt(tab.getPosition()), tab.getPosition());
            }
        });
    }

    public void setBadgeValue(UserBadge userBadge) {
        //Old code
//        if (mViewModels.length > 0) {
//            mViewModels[1].setBadgeValue(userBadge.getNearBy());
//            mViewModels[2].setBadgeValue(userBadge.getUnreadMessage());
//            mViewModels[3].setBadgeValue(userBadge.getUnreadNotification());
//        }

        if (mViewModels.length > 0) {
            mViewModels[1].setBadgeValue(userBadge.getUnreadMessage());
        }
    }

    public void addConversationBadge(int value) {
        Timber.e("addConversationBadge " + value);
        mViewModels[1].setBadgeValue(mViewModels[1].getSaveBadge() + value);
    }

    @Override
    public void onClick() {
        Timber.e("onUpdate BadgeValue: %s, %s",
                mHomeViewPager.getCurrentFragment(),
                mHomeViewPager.getCurrentItem());
        mViewModels[mHomeViewPager.getCurrentItem()].updateBadgeValue();
    }

    private enum HomeMenu {
        //Old codes
//        WALLSTREET(R.string.home_wall_street_title, R.string.code_home),
//        NEARBY(R.string.home_nearby_title, R.string.code_pin_location),
//        MESSAGE(R.string.home_message_title, R.string.code_comment),
//        NOTIFICATION(R.string.home_notification_title, R.string.code_notification);

        //Now we use only title and font icon is not using here.
        WALLSTREET(R.string.home_tab_newsfeed_title, R.string.code_home),
        MESSAGE(R.string.home_tab_message_title, R.string.code_comment),
        CONTACT(R.string.home_tab_contact_title, R.string.code_pin_location);

        private final int mTitle;
        private final int mIcon;

        HomeMenu(int title, int icon) {
            mTitle = title;
            mIcon = icon;
        }

        public int getTitle() {
            return mTitle;
        }

        public int getIcon() {
            return mIcon;
        }
    }

    public interface OnTabLayoutChangeListener {
        void onPageReselect(AbsBaseFragment fragment, int position);

        void onPageChanged(AbsBaseFragment fragment, int position);
    }

}
