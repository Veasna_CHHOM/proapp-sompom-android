package com.sompom.tookitup.observable;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.google.gson.Gson;
import com.sompom.tookitup.R;
import com.sompom.tookitup.model.result.ErrorResult;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.dialog.MessageDialog;
import com.sompom.tookitup.newui.dialog.SessionDialog;

import java.net.HttpURLConnection;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.functions.Function;
import retrofit2.Response;
import timber.log.Timber;


/**
 * Created by He Rotha on 10/2/17.
 */

public class ResponseHandleErrorFunc<T extends Response> implements Function<T, Observable<? extends T>> {
    private Context mContext;
    private boolean mIsSerializeErrorBody;

    public ResponseHandleErrorFunc(Context context, boolean isSerializeErrorBody) {
        mContext = context;
        mIsSerializeErrorBody = isSerializeErrorBody;
    }

    @Override
    public Observable<? extends T> apply(T data) {
        return Observable.create(emitter -> {

            //mean error code != 200, mean fail
            if (data.code() >= 200 && data.code() <= 299) {
                emitter.onNext(data);
                emitter.onComplete();
            } else {
                handleFail(data, emitter);
            }
        });
    }

    private void handleFail(T data, ObservableEmitter<T> emitter) {
        int errorCode = 0;
        String message = null;
        if (mIsSerializeErrorBody) {
            try {
                if (data.errorBody() == null) {
                    return;
                }
                ErrorResult result = new Gson().fromJson(data.errorBody().string(), ErrorResult.class);
                ErrorThrowable.AdditionalError additionalError = ErrorThrowable.AdditionalError.from(result.getMessage().getError());
                if (additionalError != ErrorThrowable.AdditionalError.NONE) {
                    errorCode = additionalError.getCode();
                    message = additionalError.getMessage();
                }
            } catch (Exception e) {
                Timber.e(e);
            }
        }

        if (errorCode == 0) {
            errorCode = data.code();
            message = mContext.getString(R.string.error_general);
        }
        if (errorCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
            showSessionDialog();
        }
        emitter.onError(new ErrorThrowable(errorCode, message));
    }

    private void showSessionDialog() {
        try {
            if (mContext instanceof AbsBaseActivity) {
                ((AbsBaseActivity) mContext).runOnUiThread(() -> {
                    Fragment fr = ((AbsBaseActivity) mContext).getSupportFragmentManager().findFragmentByTag(MessageDialog.TAG);
                    if (fr != null) {
                        return;
                    }
                    SessionDialog dialog = SessionDialog.newInstance();
                    dialog.show(((AbsBaseActivity) mContext).getSupportFragmentManager());
                });
            }
        } catch (Exception ex) {
            Timber.e(ex);
        }
    }
}
