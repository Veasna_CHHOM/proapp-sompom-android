package com.sompom.tookitup.observable;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.sompom.tookitup.R;
import com.sompom.tookitup.model.result.ErrorSupportModel;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.dialog.MessageDialog;
import com.sompom.tookitup.newui.dialog.SessionDialog;

import java.net.HttpURLConnection;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import timber.log.Timber;


/**
 * Created by He Rotha on 10/2/17.
 */

public class HandleErrorFunc<T extends ErrorSupportModel> implements Function<T, Observable<? extends T>> {
    private Context mContext;

    public HandleErrorFunc(Context context) {
        mContext = context;
    }

    @Override
    public Observable<? extends T> apply(T data) throws Exception {
        return Observable.create(emitter -> {
            int errorCode;
            String message;
            //mean error code != 200, mean fail
            if (data.getCode() >= 200 && data.getCode() <= 299) {
                emitter.onNext(data);
                emitter.onComplete();
            } else {
                errorCode = data.getCode();
                message = mContext.getString(R.string.error_general);
                emitter.onError(new ErrorThrowable(errorCode, message));

                if (errorCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    showSessionDialog();
                }
            }
        });
    }

    private void showSessionDialog() {
        try {
            if (mContext instanceof AbsBaseActivity) {
                ((AbsBaseActivity) mContext).runOnUiThread(() -> {
                    Fragment fr = ((AbsBaseActivity) mContext).getSupportFragmentManager().findFragmentByTag(MessageDialog.TAG);
                    if (fr != null) {
                        return;
                    }
                    SessionDialog dialog = SessionDialog.newInstance();
                    dialog.show(((AbsBaseActivity) mContext).getSupportFragmentManager());
                });
            }
        } catch (Exception ex) {
            Timber.e(ex);
        }
    }
}
