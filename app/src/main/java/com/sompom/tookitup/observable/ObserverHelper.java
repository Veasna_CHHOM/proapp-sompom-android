package com.sompom.tookitup.observable;

import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnCompleteListener;
import com.sompom.tookitup.model.result.ErrorSupportModel;

import java.net.ConnectException;
import java.net.UnknownHostException;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;


/**
 * Created by nuonveyo on 1/4/18.
 */
public class ObserverHelper<T extends ErrorSupportModel> {
    private final Context mContext;
    private final Observable<T> mTObservable;

    public ObserverHelper(Context context,
                          Observable<T> observable) {
        mContext = context;
        mTObservable = observable.concatMap(new HandleErrorFunc<>(mContext))
                .onErrorResumeNext(new HandleFailErrorFunc<>(mContext));
    }

    public Disposable execute(final OnCallbackListener<T> listener) {
        return mTObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(listener::onComplete, throwable -> {
                    Timber.e(throwable.toString());
                    Crashlytics.logException(throwable);

                    if (throwable instanceof ErrorThrowable) {
                        listener.onFail((ErrorThrowable) throwable);
                    } else if (throwable instanceof ConnectException ||
                            throwable instanceof UnknownHostException) {
                        final ErrorThrowable errorThrowable =
                                new ErrorThrowable(ErrorThrowable.NETWORK_ERROR,
                                        mContext.getString(R.string.error_internet_connection));
                        listener.onFail(errorThrowable);
                    } else {
                        final ErrorThrowable errorThrowable =
                                new ErrorThrowable(500,
                                        mContext.getString(R.string.error_general));
                        listener.onFail(errorThrowable);
                    }
                });
    }

    public Disposable execute(final OnCompleteListener<T> listener) {
        return mTObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(listener::onComplete, throwable -> {
                    Crashlytics.logException(throwable);
                    Timber.e(throwable);
                });
    }

    public Disposable execute() {
        return mTObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(t -> {

                }, throwable -> {
                    Crashlytics.logException(throwable);
                    Timber.e(throwable);
                });
    }
}

