package com.sompom.tookitup.observable;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.dialog.MessageDialog;
import com.sompom.tookitup.newui.dialog.SessionDialog;

import java.net.HttpURLConnection;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import retrofit2.HttpException;
import timber.log.Timber;


/**
 * Created by He Rotha on 10/2/17.
 */

public class HandleFailErrorFunc<T> implements Function<Throwable, ObservableSource<T>> {
    private Context mContext;

    public HandleFailErrorFunc(Context context) {
        mContext = context;
    }

    private void showSessionDialog() {
        try {
            if (mContext instanceof AbsBaseActivity) {
                ((AbsBaseActivity) mContext).runOnUiThread(() -> {
                    Fragment fr = ((AbsBaseActivity) mContext).getSupportFragmentManager().findFragmentByTag(MessageDialog.TAG);
                    if (fr != null) {
                        return;
                    }
                    SessionDialog dialog = SessionDialog.newInstance();
                    dialog.show(((AbsBaseActivity) mContext).getSupportFragmentManager());
                });
            }
        } catch (Exception ex) {
            Timber.e(ex);
        }
    }

    @Override
    public ObservableSource<T> apply(Throwable throwable) {
        if (throwable instanceof HttpException && ((HttpException) throwable).code() == HttpURLConnection.HTTP_UNAUTHORIZED) {
            showSessionDialog();
        }
        return Observable.error(throwable);
    }
}
