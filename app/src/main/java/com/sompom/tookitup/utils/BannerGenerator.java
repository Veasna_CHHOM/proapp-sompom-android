package com.sompom.tookitup.utils;

import android.content.Context;
import android.support.annotation.StringRes;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.sompom.tookitup.R;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by He Rotha on 10/11/17.
 */

public final class BannerGenerator {
    private BannerGenerator() {
    }

    public static List<AdView> get(Context context) {
        Timber.e("banner start load ");

        List<AdView> adViews = new ArrayList<>();
        adViews.add(generateAds(context, R.string.admob1));
        adViews.add(generateAds(context, R.string.admob2));
        adViews.add(generateAds(context, R.string.admob3));
        adViews.add(generateAds(context, R.string.admob4));
        adViews.add(generateAds(context, R.string.admob5));
        Timber.e("banner generate " + adViews.size());
        return adViews;
    }

    private static AdView generateAds(Context context, @StringRes int resource) {
        AdView adView = new AdView(context);
        adView.setAdSize(AdSize.SMART_BANNER);
        adView.setAdUnitId(context.getString(resource));
        final AdRequest adRequest = new AdRequest
                .Builder()
                .addTestDevice("0B0EC896DD97262FEDF7C18CEE835CC3")
                .build();
        adView.loadAd(adRequest);
        return adView;
    }
}
