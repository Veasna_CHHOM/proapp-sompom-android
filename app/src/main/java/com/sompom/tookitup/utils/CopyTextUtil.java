package com.sompom.tookitup.utils;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.widget.Toast;

import com.sompom.tookitup.R;

/**
 * Created by nuonveyo on 11/5/18.
 */

public final class CopyTextUtil {
    private CopyTextUtil() {
    }

    public static void copyTextToClipboard(Context context, String text) {
        if (context != null) {
            ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText(context.getResources().getString(R.string.app_name), text);
            if (clipboard != null) {
                clipboard.setPrimaryClip(clip);
                Toast.makeText(context, R.string.chat_text_copied, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
