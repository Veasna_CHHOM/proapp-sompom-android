package com.sompom.tookitup.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;

import com.desmond.squarecamera.utils.media.FileNameUtil;
import com.desmond.squarecamera.utils.media.ImageSaver;
import com.desmond.squarecamera.utils.media.ImageUtil;
import com.sompom.tookitup.model.Media;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by desmond on 24/10/14.
 */
public final class MediaUtil {

    private MediaUtil() {
    }

    public static long getAudioDuration(String path) {
        String mediaPath = Uri.parse(path).getPath();
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(mediaPath);
        String duration = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        mmr.release();

        if (TextUtils.isEmpty(duration)) {
            return 0;
        }
        return Long.parseLong(duration);
    }

    public static Media getMediaFromOtherApp(Context context, Uri uri) {
        String realPath = null;
        try {
            realPath = ImageUtil.getRealPathFromURI(context, uri);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (TextUtils.isEmpty(realPath)) {
            try {
                InputStream input = context.getContentResolver().openInputStream(uri);
                Bitmap bitmap = BitmapFactory.decodeStream(input);
                String file = FileNameUtil.getDirectory(context, true) +
                        File.separator +
                        FileNameUtil.convertNameFromUrl(uri.toString());
                realPath = ImageSaver.with(context)
                        .load(bitmap)
                        .into(file)
                        .resize(false)
                        .setBroadcast(false)
                        .execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (TextUtils.isEmpty(realPath)) {
            return null;
        }
        //future handling with minetype
        String extension = MimeTypeMap.getFileExtensionFromUrl(realPath);
        String type = null;
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }

        if (type != null) {
            if (!type.startsWith("image")) {
                return null;
            }
        }


        try {
            Media productMedia = new Media();
            productMedia.setUrl(realPath);
            ExifInterface exif = new ExifInterface(productMedia.getUrl());
            int width = exif.getAttributeInt(ExifInterface.TAG_IMAGE_WIDTH, 0);
            int height = exif.getAttributeInt(ExifInterface.TAG_IMAGE_LENGTH, 0);
            productMedia.setWidth(width);
            productMedia.setHeight(height);
            return productMedia;
        } catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }

}
