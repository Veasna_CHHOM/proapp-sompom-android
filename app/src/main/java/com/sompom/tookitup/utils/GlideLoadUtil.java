package com.sompom.tookitup.utils;

import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.sompom.tookitup.R;
import com.sompom.tookitup.helper.GlideApp;

import java.io.File;

import timber.log.Timber;

/**
 * Created by he.rotha on 5/18/16.
 */
public final class GlideLoadUtil {
    private GlideLoadUtil() {
    }

    public static void load(ImageView imageView, String url) {
        try {
            if (!TextUtils.isEmpty(url)) {
                if (!url.startsWith("http")) {
                    GlideApp.with(imageView)
                            .load(new File(url))
                            .centerCrop()
                            .override(0, 500)
                            .into(imageView);
                } else {
                    GlideApp.with(imageView)
                            .load(url)
                            .override(0, 500)
                            .centerCrop()
                            .into(imageView);
                }
            }
        } catch (IllegalArgumentException ex) {
            Timber.e(ex);
        }
    }

    public static void load(ImageView imageView, String url, boolean isCenterCrop) {
        try {

            if (!TextUtils.isEmpty(url)) {
                if (!url.startsWith("http")) {
                    if (isCenterCrop) {
                        GlideApp.with(imageView)
                                .load(new File(url))
                                .centerCrop()
                                .override(0, 500)

                                .into(imageView);
                    } else {
                        GlideApp.with(imageView)
                                .load(new File(url))
                                .override(0, 500)

                                .into(imageView);
                    }
                } else {
                    if (isCenterCrop) {
                        GlideApp.with(imageView)
                                .load(url)
                                .centerCrop()
                                .override(0, 500)

                                .into(imageView);
                    } else {
                        GlideApp.with(imageView)
                                .load(url)
                                .override(0, 500)

                                .into(imageView);
                    }
                }
            }
        } catch (IllegalArgumentException ex) {
            Timber.e(ex);
        }
    }

    public static void setChatMultiImageView(ImageView imageView, String imageUrl, int size) {
        try {
            Drawable drawable = ContextCompat.getDrawable(imageView.getContext(), R.drawable.ic_photo_placeholder_400dp);
            GlideApp.with(imageView)
                    .load(imageUrl)
                    .diskCacheStrategy(DiskCacheStrategy.DATA)
                    .placeholder(drawable)
                    .override(0, size)
                    .into(imageView);
        } catch (IllegalArgumentException ex) {
            Timber.e(ex);
        }
    }

}
