package com.sompom.tookitup.utils;

import com.onesignal.OneSignal;

import org.json.JSONObject;

import java.util.Collections;

import timber.log.Timber;

/**
 * Created by he.rotha on 5/2/16.
 */
public final class OneSignalUtils {
    private static final String TAG = OneSignalUtils.class.getSimpleName();
    private static final String USER_ID = "userId";

    private OneSignalUtils() {
    }

    public static void subscribeWithUserId(String userId) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(USER_ID, userId);
            Timber.e("Subscribe to OneSignal: " + jsonObject.toString());
            OneSignal.sendTags(jsonObject);
            Timber.e("User has subscribed to OneSignal successfully.");
        } catch (Exception e) {
            Timber.e(e.getMessage());
        }
    }

    public static void logout() {
        OneSignal.deleteTags(Collections.singletonList(USER_ID));
    }

}
