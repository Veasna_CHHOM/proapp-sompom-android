package com.sompom.tookitup.utils;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;

import com.sompom.tookitup.R;

/**
 * Created by ChhomVeasna on 8/15/16.
 */
public final class MailUtils {
    private MailUtils() {
    }

    public static void mailToSupporter(Context context) {
        Intent contactSupportIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:"));
        contactSupportIntent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.mail_subject));
        contactSupportIntent.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.mail_body));
        contactSupportIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{context.getString(R.string.contact_email)});
        context.startActivity(Intent.createChooser(contactSupportIntent,
                context.getString(R.string.send_mail)));
    }

    public static void contactSupport(Fragment fragment) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{fragment.getString(R.string.contact_email)});
        try {
            fragment.startActivity(Intent.createChooser(i,
                    fragment.getString(R.string.send_mail)));
        } catch (ActivityNotFoundException ex) {
            SnackBarUtil.showSnackBar(fragment.getView(), R.string.could_not_find_email_app);
        }
    }
}
