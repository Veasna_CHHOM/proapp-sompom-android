package com.sompom.tookitup.utils;

import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.sompom.tookitup.R;

import timber.log.Timber;

/**
 * Created by He Rotha on 9/27/17.
 */

public final class SnackBarUtil {
    private SnackBarUtil() {
    }

    public static void showSnackBar(View view, @StringRes int message) {
        try {
            if (view != null) {
                if (view.getContext() != null) {
                    Snackbar snackbar = Snackbar.make(view,
                            view.getContext().getString(message),
                            Snackbar.LENGTH_SHORT);
                    snackbar.getView().setBackgroundColor(ContextCompat.getColor(view.getContext(),
                            R.color.colorPrimary));
                    snackbar.show();
                }
            }
        } catch (Exception ignore) {
            Timber.e(ignore.toString());
        }
    }

    public static void showSnackBar(View view, String message) {
        Timber.e("showSnackBar: " + message);
        if (view == null) {
            return;
        }
        Snackbar snackbar = Snackbar.make(view,
                message,
                Snackbar.LENGTH_SHORT);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(view.getContext(),
                R.color.colorPrimary));
        snackbar.show();
    }
}
