package com.sompom.tookitup.utils;

import android.util.Patterns;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nuonveyo on 2/26/18.
 */

public class GenerateLinkPreviewUtil {
    private GenerateLinkPreviewUtil() {

    }

    public static String getPreviewLink(String text) {
        String replaceText = text.replace("\n", " ");
        String[] splitText = replaceText.split(" ");
        List<String> linksList = new ArrayList<>();

        for (String s : splitText) {
            if (Patterns.WEB_URL.matcher(s).matches()) {
                linksList.add(s);
            }
        }

        //Get last url to preview
        if (!linksList.isEmpty()) {
            String lastLink = linksList.get(linksList.size() - 1);
            String url;
            if (lastLink.contains("http://") || lastLink.contains("https://")) {
                url = lastLink;
            } else {
                url = "http://" + lastLink;
            }
            return url;
        }
        return null;
    }
}
