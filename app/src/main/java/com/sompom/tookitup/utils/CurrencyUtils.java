package com.sompom.tookitup.utils;

import com.sompom.tookitup.TookitupApplication;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

import timber.log.Timber;

/**
 * Created by he.rotha on 4/11/16.
 */
public final class CurrencyUtils {
    private CurrencyUtils() {
    }

    public static String getLocaleCurrency(String countryCode) {
        for (Locale locale : Locale.getAvailableLocales()) {
            try {
                if (locale.getCountry().equals(countryCode)) {
                    Currency currency = Currency.getInstance(locale);
                    return currency.getCurrencyCode();
                }
            } catch (Exception e) {
                Timber.e("getLocaleCurrency fail: %s", e.toString());
            }
        }
        return null;
    }

    public static String formatPrice(double value, String currencyCode) {
        String price;
        if (value < 10000) {
            price = formatMoney(value, currencyCode);
        } else {
            price = FormatNumber.format((long) value) + " " + getCurrencySymbol(currencyCode).getSymbol();
        }

        return price;
    }

    public static String formatPrice(double value) {
        String price;
        if (value < 10000) {
            NumberFormat formatter = new DecimalFormat("#,###.##");
            price = formatter.format(value);
        } else {
            price = FormatNumber.format((long) value);
        }

        return price;
    }

    public static String formatMoney(double value, String currencyCode) {
        NumberFormat formatter = new DecimalFormat("#,###.##");
        return formatter.format(value) + " " + getCurrencySymbol(currencyCode).getSymbol();
    }

    public static String formatFullCurrency(double value, String currencyCode) {
        Timber.e("currencycode is " + currencyCode);
        NumberFormat formatter = NumberFormat.getCurrencyInstance(TookitupApplication.sLocal);
        formatter.setCurrency(getCurrencySymbol(currencyCode));
        formatter.setMinimumFractionDigits(0);
        formatter.setMaximumFractionDigits(0);
        return formatter.format(value);
    }

    public static Currency getCurrencySymbol(String currencyCode) {
        return Currency.getInstance(currencyCode);
    }

    public static String formatDouble(double value) {
        NumberFormat formatter = new DecimalFormat("#.##");
        return formatter.format(value);
    }
}
