package com.sompom.tookitup.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

import com.sompom.tookitup.model.emun.ChatBg;
import com.sompom.tookitup.model.emun.RoundCornerType;

/**
 * Created by nuonveyo
 * on 1/2/19.
 */

public final class DrawChatImageRoundedUtil {

    private DrawChatImageRoundedUtil() {

    }

    private static Bitmap getOutputBitmap(Bitmap bitmap) {
        return Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(),
                bitmap.getConfig());
    }

    private static Paint getPaint() {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);
        return paint;
    }

    public static Bitmap scaleCenterCrop(Bitmap source, int newHeight, int newWidth) {
        int sourceWidth = source.getWidth();
        int sourceHeight = source.getHeight();

        // Compute the scaling factors to fit the new height and width, respectively.
        // To cover the final image, the final scaling will be the bigger
        // of these two.
        float xScale = (float) newWidth / sourceWidth;
        float yScale = (float) newHeight / sourceHeight;
        float scale = Math.max(xScale, yScale);

        // Now get the size of the source bitmap when scaled
        float scaledWidth = scale * sourceWidth;
        float scaledHeight = scale * sourceHeight;

        // Let's find out the upper left coordinates if the scaled bitmap
        // should be centered in the new size give by the parameters
        float left = (newWidth - scaledWidth) / 2;
        float top = (newHeight - scaledHeight) / 2;

        // The target rectangle for the new, scaled version of the source bitmap will now
        // be
        RectF targetRect = new RectF(left, top, left + scaledWidth, top + scaledHeight);

        // Finally, we create a new bitmap of the specified size and draw our new,
        // scaled bitmap onto it.
        Bitmap dest = Bitmap.createBitmap(newWidth, newHeight, source.getConfig());
        Canvas canvas = new Canvas(dest);
        canvas.drawBitmap(source, null, targetRect, null);

        return dest;
    }

    public static Bitmap cropCircle(Bitmap bmp, int radius) {
        Bitmap sbmp;
        if (bmp.getWidth() != radius || bmp.getHeight() != radius) {
            sbmp = Bitmap.createScaledBitmap(bmp, radius, radius, false);
        } else {
            sbmp = bmp;
        }

        Bitmap output = getOutputBitmap(sbmp);
        Canvas canvas = new Canvas(output);
        canvas.drawARGB(0, 0, 0, 0);
        final Paint paint = getPaint();

        final Rect rect = new Rect(0, 0, sbmp.getWidth(), sbmp.getHeight());

        canvas.drawCircle(sbmp.getWidth() / 2 - 0.1f, sbmp.getWidth() / 2 - 0.1f,
                sbmp.getWidth() / 2 - 0.1f, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(sbmp, rect, rect, paint);

        return output;
    }

    public static Bitmap roundAll(Bitmap bitmap, int radius) {
        Bitmap output = getOutputBitmap(bitmap);
        Canvas canvas = new Canvas(output);
        Paint paint = getPaint();
        canvas.drawARGB(0, 0, 0, 0);

        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        canvas.drawRoundRect(new RectF(rect), radius, radius, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static Bitmap roundTop(Bitmap bitmap, int radius) {

        Bitmap output = getOutputBitmap(bitmap);
        Canvas canvas = new Canvas(output);
        Paint paint = getPaint();
        canvas.drawARGB(0, 0, 0, 0);

        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        float right = bitmap.getWidth();
        float bottom = bitmap.getHeight();
        int diameter = radius * 2;

        canvas.drawRoundRect(new RectF(0, 0, right, diameter), radius, radius,
                paint);
        canvas.drawRect(new RectF(0, radius, right, bottom), paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static Bitmap roundBottom(Bitmap bitmap, int radius) {

        Bitmap output = getOutputBitmap(bitmap);
        Canvas canvas = new Canvas(output);
        Paint paint = getPaint();
        canvas.drawARGB(0, 0, 0, 0);

        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        float right = bitmap.getWidth();
        float bottom = bitmap.getHeight();
        int diameter = radius * 2;

        canvas.drawRoundRect(new RectF(0, bottom - diameter, right, bottom), radius, radius,
                paint);
        canvas.drawRect(new RectF(0, 0, right, bottom - radius), paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static Bitmap roundLeft(Bitmap bitmap, int radius) {
        Bitmap output = getOutputBitmap(bitmap);
        Canvas canvas = new Canvas(output);
        Paint paint = getPaint();
        canvas.drawARGB(0, 0, 0, 0);

        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        float right = bitmap.getWidth();
        float bottom = bitmap.getHeight();
        int diameter = radius * 2;

        canvas.drawRoundRect(new RectF(0, 0, diameter, bottom), radius, radius,
                paint);
        canvas.drawRect(new RectF(radius, 0, right, bottom), paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static Bitmap roundRight(Bitmap bitmap, int radius) {
        Bitmap output = getOutputBitmap(bitmap);
        Canvas canvas = new Canvas(output);
        Paint paint = getPaint();
        canvas.drawARGB(0, 0, 0, 0);

        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        float right = bitmap.getWidth();
        float bottom = bitmap.getHeight();
        int diameter = radius * 2;

        canvas.drawRoundRect(new RectF(right - diameter, 0, right, bottom), radius, radius, paint);
        canvas.drawRect(new RectF(0, 0, right - radius, bottom), paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static Bitmap roundTopLeft(Bitmap bitmap, int radius) {
//        Timber.e("before w%s, h%s =====", bitmap.getWidth(), bitmap.getHeight());

        Bitmap output = getOutputBitmap(bitmap);
        Canvas canvas = new Canvas(output);
        Paint paint = getPaint();
        canvas.drawARGB(0, 0, 0, 0);

        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        float right = bitmap.getWidth();
        float bottom = bitmap.getHeight();
        int diameter = radius * 2;

//        Timber.e("after w%s, h%s =====", right, bottom);

        canvas.drawRoundRect(new RectF(0, 0, diameter, diameter), radius, radius, paint);
        canvas.drawRect(new RectF(0, radius, radius, bottom), paint);
        canvas.drawRect(new RectF(radius, 0, right, bottom), paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static Bitmap roundTopRight(Bitmap bitmap, int radius) {
        Bitmap output = getOutputBitmap(bitmap);
        Canvas canvas = new Canvas(output);
        Paint paint = getPaint();
        canvas.drawARGB(0, 0, 0, 0);

        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        float right = bitmap.getWidth();
        float bottom = bitmap.getHeight();
        int diameter = radius * 2;

        canvas.drawRoundRect(new RectF(right - diameter, 0, right, diameter), radius,
                radius, paint);
        canvas.drawRect(new RectF(0, 0, right - radius, bottom), paint);
        canvas.drawRect(new RectF(right - radius, radius, right, bottom), paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static Bitmap roundBottomLeft(Bitmap bitmap, int radius) {
        Bitmap output = getOutputBitmap(bitmap);
        Canvas canvas = new Canvas(output);
        Paint paint = getPaint();
        canvas.drawARGB(0, 0, 0, 0);

        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        float right = bitmap.getWidth();
        float bottom = bitmap.getHeight();
        int diameter = radius * 2;

        canvas.drawRoundRect(new RectF(0, bottom - diameter, diameter, bottom), radius,
                radius, paint);
        canvas.drawRect(new RectF(0, 0, diameter, bottom - radius), paint);
        canvas.drawRect(new RectF(radius, 0, right, bottom), paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static Bitmap roundBottomRight(Bitmap bitmap, int radius) {
        Bitmap output = getOutputBitmap(bitmap);
        Canvas canvas = new Canvas(output);
        Paint paint = getPaint();
        canvas.drawARGB(0, 0, 0, 0);

        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        float right = bitmap.getWidth();
        float bottom = bitmap.getHeight();
        int diameter = radius * 2;

        canvas.drawRoundRect(new RectF(right - diameter, bottom - diameter, right, bottom), radius,
                radius, paint);
        canvas.drawRect(new RectF(0, 0, right - radius, bottom), paint);
        canvas.drawRect(new RectF(right - radius, 0, right, bottom - radius), paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static RoundCornerType getCornerType(ChatBg chatBg) {
        if (chatBg == ChatBg.ChatMeTop) {
            return RoundCornerType.MeTop;
        } else if (chatBg == ChatBg.ChatMeMid) {
            return RoundCornerType.MeMid;
        } else if (chatBg == ChatBg.ChatMeBottom) {
            return RoundCornerType.MeBottom;
        } else if (chatBg == ChatBg.ChatYouTop) {
            return RoundCornerType.YouTop;
        } else if (chatBg == ChatBg.ChatYouMid) {
            return RoundCornerType.YouMid;
        } else if (chatBg == ChatBg.ChatYouBottom) {
            return RoundCornerType.YouBottom;
        } else {
            return RoundCornerType.Single;
        }
    }

    public static RoundCornerType getCornerType(ChatBg currentType,
                                                int rowCount,
                                                int colCount,
                                                int curRow,
                                                int curCol,
                                                int curIndex,
                                                int totalIndex) {
        if (currentType == ChatBg.ChatMeTop) {
            //One Row
            if (rowCount == 1) {
                if (curCol == 0) {
                    return RoundCornerType.MeMid;
                } else if (isLastColInRow(colCount, curCol, curIndex, totalIndex)) {
                    return RoundCornerType.TopRight;
                } else {
                    return RoundCornerType.SmallSingle;
                }

            } else {
                //Multi Row
                if (curRow == 0) {
                    if (curCol == 0) {
                        return RoundCornerType.TopLeft;
                    } else if (isLastColInRow(colCount, curCol, curIndex, totalIndex)) {
                        return RoundCornerType.TopRight;
                    } else {
                        return RoundCornerType.SmallSingle;
                    }

                } else if (isLastRow(rowCount, curRow)) {
                    if (curCol == 0) {
                        return RoundCornerType.BottomLeft;
                    } else {
                        return RoundCornerType.SmallSingle;
                    }
                } else {
                    return RoundCornerType.SmallSingle;
                }
            }

        } else if (currentType == ChatBg.ChatMeMid) {
            //One Row
            if (rowCount == 1) {
                if (curCol == 0) {
                    return RoundCornerType.MeMid;
                } else {
                    return RoundCornerType.SmallSingle;
                }

            } else {
                //Multi Row
                if (curRow == 0) {
                    if (curCol == 0) {
                        return RoundCornerType.TopLeft;
                    } else {
                        return RoundCornerType.SmallSingle;
                    }

                } else if (isLastRow(rowCount, curRow)) {
                    if (curCol == 0) {
                        return RoundCornerType.BottomLeft;
                    } else {
                        return RoundCornerType.SmallSingle;
                    }
                } else {
                    return RoundCornerType.SmallSingle;
                }
            }
        } else if (currentType == ChatBg.ChatMeBottom) {
            //One Row
            if (rowCount == 1) {
                if (curCol == 0) {
                    return RoundCornerType.MeMid;
                } else if (isLastColInRow(colCount, curCol, curIndex, totalIndex)) {
                    return RoundCornerType.BottomRight;
                } else {
                    return RoundCornerType.SmallSingle;
                }

            } else {
                //Multi Row
                if (curRow == 0) {
                    if (curCol == 0) {
                        return RoundCornerType.TopLeft;
                    } else {
                        return RoundCornerType.SmallSingle;
                    }

                } else if (isLastRow(rowCount, curRow)) {
                    if (curCol == 0) {
                        return RoundCornerType.BottomLeft;
                    } else if (isLastColInRow(colCount, curCol, curIndex, totalIndex)) {
                        return RoundCornerType.BottomRight;
                    } else {
                        return RoundCornerType.SmallSingle;
                    }
                } else {
                    return RoundCornerType.SmallSingle;
                }
            }
        } else if (currentType == ChatBg.ChatYouTop) {
            //One Row
            if (rowCount == 1) {
                if (curCol == 0) {
                    return RoundCornerType.TopLeft;
                } else if (isLastColInRow(colCount, curCol, curIndex, totalIndex)) {
                    return RoundCornerType.YouMid;
                } else {
                    return RoundCornerType.SmallSingle;
                }

            } else {
                //Multi Row
                if (curRow == 0) {
                    if (curCol == 0) {
                        return RoundCornerType.TopLeft;
                    } else if (isLastColInRow(colCount, curCol, curIndex, totalIndex)) {
                        return RoundCornerType.TopRight;
                    } else {
                        return RoundCornerType.SmallSingle;
                    }

                } else if (isLastRow(rowCount, curRow)) {
                    if (isLastColInRow(colCount, curCol, curIndex, totalIndex)) {
                        return RoundCornerType.BottomRight;
                    } else {
                        return RoundCornerType.SmallSingle;
                    }
                } else {
                    return RoundCornerType.SmallSingle;
                }
            }
        } else if (currentType == ChatBg.ChatYouMid) {
            //One Row
            if (rowCount == 1) {
                if (isLastColInRow(colCount, curCol, curIndex, totalIndex)) {
                    return RoundCornerType.YouMid;
                } else {
                    return RoundCornerType.SmallSingle;
                }

            } else {
                //Multi Row
                if (curRow == 0) {
                    if (isLastColInRow(colCount, curCol, curIndex, totalIndex)) {
                        return RoundCornerType.TopRight;
                    } else {
                        return RoundCornerType.SmallSingle;
                    }

                } else if (isLastRow(rowCount, curRow)) {
                    if (isLastColInRow(colCount, curCol, curIndex, totalIndex)) {
                        return RoundCornerType.BottomRight;
                    } else {
                        return RoundCornerType.SmallSingle;
                    }
                } else {
                    return RoundCornerType.SmallSingle;
                }
            }
        } else if (currentType == ChatBg.ChatYouBottom) {
            //One Row
            if (rowCount == 1) {
                if (curCol == 0) {
                    return RoundCornerType.BottomLeft;
                } else if (isLastColInRow(colCount, curCol, curIndex, totalIndex)) {
                    return RoundCornerType.YouMid;
                } else {
                    return RoundCornerType.SmallSingle;
                }

            } else {
                //Multi Row
                if (curRow == 0) {
                    if (isLastColInRow(colCount, curCol, curIndex, totalIndex)) {
                        return RoundCornerType.TopRight;
                    } else {
                        return RoundCornerType.SmallSingle;
                    }

                } else if (isLastRow(rowCount, curRow)) {
                    if (curCol == 0) {
                        return RoundCornerType.BottomLeft;
                    } else if (isLastColInRow(colCount, curCol, curIndex, totalIndex)) {
                        return RoundCornerType.BottomRight;
                    } else {
                        return RoundCornerType.SmallSingle;
                    }
                } else {
                    return RoundCornerType.SmallSingle;
                }
            }
        } else {
            //One Row
            if (rowCount == 1) {
                if (curCol == 0) {
                    return RoundCornerType.MeMid;
                } else if (isLastColInRow(colCount, curCol, curIndex, totalIndex)) {
                    return RoundCornerType.YouMid;
                } else {
                    return RoundCornerType.SmallSingle;
                }

            } else {
                //Multi Row
                if (curRow == 0) {
                    if (curCol == 0) {
                        return RoundCornerType.TopLeft;
                    } else if (isLastColInRow(colCount, curCol, curIndex, totalIndex)) {
                        return RoundCornerType.TopRight;
                    } else {
                        return RoundCornerType.SmallSingle;
                    }

                } else if (isLastRow(rowCount, curRow)) {
                    if (curCol == 0) {
                        return RoundCornerType.BottomLeft;
                    } else if (isLastColInRow(colCount, curCol, curIndex, totalIndex)) {
                        return RoundCornerType.BottomRight;
                    } else {
                        return RoundCornerType.SmallSingle;
                    }
                } else {
                    return RoundCornerType.SmallSingle;
                }
            }
        }
    }

    private static boolean isLastColInRow(int colCount, int curCol, int index, int totalIndex) {
        return (curCol + 1) == colCount || isLastIndex(index, totalIndex);
    }

    private static boolean isLastIndex(int index, int totalIndex) {
        return index == (totalIndex - 1);
    }

    private static boolean isLastRow(int rowCount, int curRow) {
        return (curRow + 1) >= rowCount;
    }
}
