package com.sompom.tookitup.utils;

import android.os.Handler;
import android.support.v7.widget.RecyclerView;

import timber.log.Timber;

/**
 * Created by nuonveyo on 9/3/18.
 */

public class BetterSmoothScrollUtil {
    private static final int MAX_SCROLL = 5;

    public static void startScroll(RecyclerView recyclerView,
                                   final int targetItem,
                                   final int topItem) {
        final int distance = topItem - targetItem;

        Timber.e("TopItem: %s", topItem);
        Timber.e("distance: %s", distance);

        final int anchorItem;
        if (distance > MAX_SCROLL) {
            anchorItem = targetItem + MAX_SCROLL;
        } else if (distance < -MAX_SCROLL) {
            anchorItem = targetItem - MAX_SCROLL;
        } else {
            anchorItem = topItem;
        }
        Timber.e("distance: %s", distance);
        Timber.e("anchorItem: %s", anchorItem);

        if (anchorItem != topItem) {
            recyclerView.scrollToPosition(anchorItem);
        }
        new Handler().post(() -> recyclerView.smoothScrollToPosition(targetItem));
    }
}
