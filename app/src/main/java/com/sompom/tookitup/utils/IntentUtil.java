package com.sompom.tookitup.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.customtabs.CustomTabsIntent;

/**
 * Created by nuonveyo on 10/2/18.
 */

public final class IntentUtil {

    private static final String HTTP = "http://";
    private static final String HTTPS = "https://";

    private IntentUtil() {
    }

    public static void shareIntent(Activity activity, Bitmap bitmap) {
        if (activity == null) return;
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);

        String path = MediaStore.Images.Media.insertImage(activity.getContentResolver(), bitmap, "", null);
        Uri screenshotUri = Uri.parse(path);
        shareIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);

        shareIntent.setType("image/*");
        activity.startActivity(shareIntent);
    }

    static void deepLinkIntent(Context activity, String url) {
        String link;
        if (url.contains(HTTP) || url.contains(HTTPS)) {
            link = url;
        } else {
            link = HTTP + url;
        }
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(activity, Uri.parse(link));
    }

    public static void openAppSetting(Activity activity) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
        intent.setData(uri);
        activity.startActivity(intent);
    }
}
