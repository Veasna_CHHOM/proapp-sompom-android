package com.sompom.tookitup.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.StyleSpan;
import android.util.Patterns;
import android.view.View;

import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.listener.OnSpannableClickListener;
import com.sompom.tookitup.model.SearchAddressResult;
import com.sompom.tookitup.model.emun.PublishItem;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.widget.ImageProfileLayout;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import timber.log.Timber;

/**
 * Created by nuonveyo on 4/25/18.
 */

public final class SpannableUtil {
    private static final String DOT_FIRST = "DOT_FIRST";
    private static final String DOT_SECOND = "DOT_SECOND";
    private static final String USER = "USER";

    private SpannableUtil() {
    }

    public static Spannable setFacebookButtonText(Context context, String holdText, String boldText, String boldTextPhone) {
        Spannable spannable = new SpannableString(holdText);
        try {
            Typeface typefaceBold = ResourcesCompat.getFont(context, R.font.sf_pro_semibold);
            int color = AttributeConverter.convertAttrToColor(context, R.attr.colorFirstText);

            spannable.setSpan(new CustomTypefaceSpan("", typefaceBold),
                    holdText.indexOf(boldTextPhone),
                    holdText.indexOf(boldTextPhone) + boldTextPhone.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            spannable.setSpan(new ForegroundColorSpan(color),
                    holdText.indexOf(boldTextPhone),
                    holdText.indexOf(boldTextPhone) + boldTextPhone.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            spannable.setSpan(new CustomTypefaceSpan("", typefaceBold),
                    holdText.indexOf(boldText),
                    holdText.indexOf(boldText) + boldText.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            spannable.setSpan(new ForegroundColorSpan(color),
                    holdText.indexOf(boldText),
                    holdText.indexOf(boldText) + boldText.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        } catch (Exception e) {
            Timber.e("error: %s", e.toString());
        }
        return spannable;
    }

    public static Spannable setNotificationDescriptionText(Context context, String originalText, OnSpannableClickListener listener) {
        String decorateText = context.getString(R.string.edit_profile_disable_all_notification_description_bold);
        int color = ContextCompat.getColor(context, R.color.colorAccent);

        Spannable spannable = new SpannableString(originalText);
        try {
            spannable.setSpan(new ForegroundColorSpan(color),
                    originalText.indexOf(decorateText),
                    originalText.indexOf(decorateText) + decorateText.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            spannable.setSpan(new StyleSpan(Typeface.BOLD),
                    originalText.indexOf(decorateText),
                    originalText.indexOf(decorateText) + decorateText.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            ClickableSpan span = new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    widget.invalidate();
                    if (listener != null) {
                        listener.onClick();
                    }
                }
            };

            spannable.setSpan(span, originalText.indexOf(decorateText),
                    originalText.indexOf(decorateText) + decorateText.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } catch (Exception e) {
            Timber.e("e: %s", e.toString());
        }
        return spannable;
    }

    public static Spannable getNotificationName(Context context, String name, String description) {
        Spannable span = Spannable.Factory.getInstance().newSpannable(description);
        try {
            Typeface typefaceBold = ResourcesCompat.getFont(context, R.font.sf_pro_semibold);
            span.setSpan(new CustomTypefaceSpan("", typefaceBold),
                    description.indexOf(name),
                    description.indexOf(name) + name.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            Typeface typeface = ResourcesCompat.getFont(context, R.font.sf_pro_regular);
            span.setSpan(new CustomTypefaceSpan("", typeface),
                    description.indexOf(description),
                    description.indexOf(description) + description.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        } catch (Exception e) {
            Timber.e("Exception: %s", e.getMessage());
        }
        return span;
    }

    public static SpannableString getTimelineDate(Context context, String date, String address, PublishItem publishItem) {
        StringBuilder text = new StringBuilder();
        text.append(date).append(" ");
        if (!TextUtils.isEmpty(address)) {
            text
                    .append(" ")
                    .append(DOT_FIRST)
                    .append(" ")
                    .append(" ")
                    .append(address)
                    .append(" ")
                    .append(" ")
                    .append(DOT_SECOND)
                    .append(" ")
                    .append(" ")
                    .append(USER);
        } else {
            text
                    .append(" ")
                    .append(" ")
                    .append(DOT_FIRST)
                    .append(" ")
                    .append(" ")
                    .append(USER);
        }

        SpannableString spannableString = new SpannableString(text);

        spannableString.setSpan(
                getIcon(context, R.drawable.ic_small_cycle, 0.7f),
                text.indexOf(DOT_FIRST),
                text.indexOf(DOT_FIRST) + DOT_FIRST.length(),
                0);

        Pattern pattern = Pattern.compile(DOT_SECOND);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            spannableString.setSpan(
                    getIcon(context, R.drawable.ic_small_cycle, 0.7f),
                    text.indexOf(DOT_SECOND),
                    text.indexOf(DOT_SECOND) + DOT_SECOND.length(),
                    0);
        }

        spannableString.setSpan(getIcon(context, publishItem.getIcon(), 1.5f),
                text.indexOf(USER),
                text.indexOf(USER) + USER.length(),
                0);

        if (!TextUtils.isEmpty(address)) {
            Typeface typeface = ResourcesCompat.getFont(context, R.font.sf_pro_light);
            spannableString.setSpan(new CustomTypefaceSpan("", typeface),
                    text.indexOf(address),
                    text.indexOf(address) + address.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        }

        return spannableString;
    }

    public static SpannableString getProduct(Context context, String title, String address, PublishItem publishItem) {
        StringBuilder text = new StringBuilder();
        if (!TextUtils.isEmpty(title)) {
            text.append(title.toUpperCase()).append(" ");
        }
        if (!TextUtils.isEmpty(address)) {
            text
                    .append(" ")
                    .append(DOT_FIRST)
                    .append(" ")
                    .append(" ")
                    .append(address)
                    .append(" ")
                    .append(" ")
                    .append(DOT_SECOND)
                    .append(" ")
                    .append(" ")
                    .append(USER);
        } else {
            text
                    .append(" ")
                    .append(" ")
                    .append(DOT_FIRST)
                    .append(" ")
                    .append(" ")
                    .append(USER);
        }

        SpannableString spannableString = new SpannableString(text);
        if (!TextUtils.isEmpty(title)) {
            int orangeColor = ContextCompat.getColor(context, R.color.colorAccent);
            spannableString.setSpan(new ForegroundColorSpan(orangeColor),
                    0,
                    title.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            Typeface typeface = ResourcesCompat.getFont(context, R.font.sf_pro_bold);
            spannableString.setSpan(new CustomTypefaceSpan("", typeface),
                    0,
                    title.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        spannableString.setSpan(
                getIcon(context, R.drawable.ic_small_cycle, 0.7f),
                text.indexOf(DOT_FIRST),
                text.indexOf(DOT_FIRST) + DOT_FIRST.length(),
                0);

        Pattern pattern = Pattern.compile(DOT_SECOND);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            spannableString.setSpan(
                    getIcon(context, R.drawable.ic_small_cycle, 0.7f),
                    text.indexOf(DOT_SECOND),
                    text.indexOf(DOT_SECOND) + DOT_SECOND.length(),
                    0);
        }

        spannableString.setSpan(getIcon(context, publishItem.getIcon(), 1.5f),
                text.indexOf(USER),
                text.indexOf(USER) + USER.length(),
                0);

        if (!TextUtils.isEmpty(address)) {
            Typeface typeface = ResourcesCompat.getFont(context, R.font.sf_pro_light);
            spannableString.setSpan(new CustomTypefaceSpan("", typeface),
                    text.indexOf(address),
                    text.indexOf(address) + address.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        }

        return spannableString;
    }

    private static Bitmap getBitmap(Drawable vectorDrawable) {
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);
        return bitmap;
    }

    private static ImageSpan getIcon(Context context, @DrawableRes int drawableRest, float position) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableRest);
        Bitmap bitmap = getBitmap(drawable);
        return new ImageSpan(context, bitmap, ImageSpan.ALIGN_BASELINE) {
            public void draw(Canvas canvas, CharSequence text, int start,
                             int end, float x, int top, int y, int bottom,
                             Paint paint) {
                Drawable b = getDrawable();
                canvas.save();

                int transY = bottom - b.getBounds().bottom;
                transY -= paint.getFontMetricsInt().descent / position;

                canvas.translate(x, transY);
                b.draw(canvas);
                canvas.restore();
            }
        };
    }

    public static SpannableString getLikeCommentShareCounter(Context context, long likes, long shares, long comments) {
        StringBuilder text = new StringBuilder();
        boolean isAddFirstIcon = false;
        boolean isAddSecondIcon = false;
        if (likes > 0) {
            isAddFirstIcon = true;
            String likeResource;
            if (likes == 1) {
                likeResource = context.getString(R.string.product_detail_button_like);
            } else {
                likeResource = context.getString(R.string.home_button_likes);
            }
            text.append(FormatNumber.format(likes))
                    .append(" ")
                    .append(likeResource)
                    .append(" ");
        }

        if (shares > 0) {
            isAddSecondIcon = true;
            String shareResource;
            if (shares == 1) {
                shareResource = context.getString(R.string.product_detail_button_share);
            } else {
                shareResource = context.getString(R.string.home_button_shares);
            }
            if (isAddFirstIcon) {
                text.append(DOT_FIRST).append(" ");
            }
            text.append(FormatNumber.format(shares))
                    .append(" ")
                    .append(shareResource)
                    .append(" ");
        }

        if (comments > 0) {
            String commentResource;
            if (comments == 1) {
                commentResource = context.getString(R.string.home_button_comment);
            } else {
                commentResource = context.getString(R.string.product_detail_button_comments);
            }
            if (isAddFirstIcon || isAddSecondIcon) {
                text.append(DOT_SECOND).append(" ");
            }
            text.append(FormatNumber.format(comments))
                    .append(" ")
                    .append(commentResource);
        }

        SpannableString spannableString = new SpannableString(text);
        Pattern pattern = Pattern.compile(DOT_FIRST);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            spannableString.setSpan(
                    getIcon(context, R.drawable.ic_small_cycle, 0.7f),
                    text.indexOf(DOT_FIRST),
                    text.indexOf(DOT_FIRST) + DOT_FIRST.length(),
                    0);
        }

        Pattern pattern2 = Pattern.compile(DOT_SECOND);
        Matcher matcher2 = pattern2.matcher(text);
        if (matcher2.find()) {
            spannableString.setSpan(
                    getIcon(context, R.drawable.ic_small_cycle, 0.7f),
                    text.indexOf(DOT_SECOND),
                    text.indexOf(DOT_SECOND) + DOT_SECOND.length(),
                    0);
        }

        return spannableString;
    }

    public static SpannableString getUserNameSharedTimeline(Context context, User user, String shareText, ImageProfileLayout.RenderType renderType) {
        String userFullName;
        if (renderType == null || renderType == ImageProfileLayout.RenderType.USER) {
            userFullName = user.getFullName();
        } else {
            userFullName = user.getStoreName();
        }
        String text = userFullName + " " + shareText;
        SpannableString spannableString = new SpannableString(text);

        Typeface typefaceBold = ResourcesCompat.getFont(context, R.font.sf_pro_semibold);
        spannableString.setSpan(new CustomTypefaceSpan("", typefaceBold),
                text.indexOf(userFullName),
                text.indexOf(userFullName) + userFullName.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        int orangeColor = ContextCompat.getColor(context, R.color.colorPrimary);
        spannableString.setSpan(new ForegroundColorSpan(orangeColor),
                text.indexOf(shareText),
                text.indexOf(shareText) + shareText.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        Typeface typeface = ResourcesCompat.getFont(context, R.font.sf_pro_light);
        spannableString.setSpan(new CustomTypefaceSpan("", typeface),
                text.indexOf(shareText),
                text.indexOf(shareText) + shareText.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    public static SpannableString getUserNameSharedTimeline(Context context, User user, ImageProfileLayout.RenderType renderType) {
        String userFullName;
        if (renderType == null || renderType == ImageProfileLayout.RenderType.USER) {
            userFullName = user.getFullName();
        } else {
            userFullName = user.getStoreName();
        }

        SpannableString spannableString = new SpannableString(userFullName);

        Typeface typefaceBold = ResourcesCompat.getFont(context, R.font.sf_pro_semibold);
        spannableString.setSpan(new CustomTypefaceSpan("", typefaceBold),
                0,
                userFullName.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    public static Spannable getUserFullNameCheckIn(Context context, User user, SearchAddressResult searchAddressResult, View.OnClickListener listener) {
        String userFullName = user.getFullName();
        String at = context.getString(R.string.date_at);
        String place = searchAddressResult.getDisplayAddress();
        if (TextUtils.isEmpty(place)) {
            place = searchAddressResult.getCity();
        }
        if (!TextUtils.isEmpty(place)) {
            userFullName = userFullName + " "
                    + at + " "
                    + place;
        }
        Spannable span = Spannable.Factory.getInstance().newSpannable(userFullName);

        try {
            if (listener != null) {
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View textView) {
                        listener.onClick(textView);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };
                span.setSpan(clickableSpan,
                        userFullName.indexOf(place),
                        userFullName.indexOf(place) + place.length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }

            if (!TextUtils.isEmpty(place)) {
                Typeface typeface = ResourcesCompat.getFont(context, R.font.sf_pro_regular);
                span.setSpan(new CustomTypefaceSpan("", typeface),
                        userFullName.indexOf(at),
                        userFullName.indexOf(at) + at.length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                span.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.darkGrey)),
                        userFullName.indexOf(at),
                        userFullName.indexOf(at) + at.length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        } catch (Exception e) {
            Timber.e("Exception: %s", e.getMessage());
        }
        return span;
    }

    public static SpannableString getTextLink(Context context, String text, int color, OnClickListener onClickListener) {
        SpannableString buffer = new SpannableString(text);
        try {
            String replaceText = text.replace("\n", " ");
            String[] splitText = replaceText.split(" ");
            for (String url : splitText) {
                if (Patterns.WEB_URL.matcher(url).matches()) {
                    ClickableSpan clickableSpan = new ClickableSpan() {
                        @Override
                        public void onClick(@NonNull View textView) {
                            if (onClickListener != null) {
                                onClickListener.onClick();
                            }
                            IntentUtil.deepLinkIntent(context, url);
                        }
                    };

                    buffer.setSpan(clickableSpan,
                            text.indexOf(url),
                            text.indexOf(url) + url.length(),
                            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    buffer.setSpan(new ForegroundColorSpan(color),
                            text.indexOf(url),
                            text.indexOf(url) + url.length(),
                            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
        } catch (Exception e) {
            Timber.e("getTextLink error: %s", e.getMessage());
        }
        return buffer;
    }

    public static Spannable getUserStoreName(Context context, User user) {
        String userStoreName = user.getStoreName();
        if (TextUtils.isEmpty(userStoreName)) {
            userStoreName = user.getFullName();
        }
        String holdText = context.getString(R.string.user_store_dialog_available_title) + " " + userStoreName;
        SpannableString spannableString = new SpannableString(holdText);

        Typeface typeface = ResourcesCompat.getFont(context, R.font.sf_pro_semibold);
        spannableString.setSpan(new CustomTypefaceSpan("", typeface),
                holdText.indexOf(userStoreName),
                holdText.indexOf(userStoreName) + userStoreName.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        int color = AttributeConverter.convertAttrToColor(context, R.attr.colorFirstText);
        spannableString.setSpan(new ForegroundColorSpan(color),
                holdText.indexOf(userStoreName),
                holdText.indexOf(userStoreName) + userStoreName.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return spannableString;
    }

    public static Spannable getStoreName(Context context, String user) {
        final String suffix = "@";


        String fullText = suffix + user;
        SpannableString spannableString = new SpannableString(fullText);

        Typeface typeface = ResourcesCompat.getFont(context, R.font.sf_pro_semibold);
        spannableString.setSpan(new CustomTypefaceSpan("", typeface),
                fullText.indexOf(suffix),
                fullText.indexOf(suffix) + suffix.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        int color = AttributeConverter.convertAttrToColor(context, R.attr.colorFirstText);
        spannableString.setSpan(new ForegroundColorSpan(color),
                fullText.indexOf(suffix),
                fullText.indexOf(suffix) + suffix.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return spannableString;
    }
}
