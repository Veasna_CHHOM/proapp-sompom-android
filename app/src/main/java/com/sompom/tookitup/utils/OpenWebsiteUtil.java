package com.sompom.tookitup.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;

/**
 * Created by He Rotha on 9/27/17.
 */

public final class OpenWebsiteUtil {
    private OpenWebsiteUtil() {
    }

    public static void openWebsite(Context context, String url) {
        if (!TextUtils.isEmpty(url)) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            context.startActivity(browserIntent);
        }
    }
}
