package com.sompom.tookitup.utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.Person;
import android.support.v4.app.RemoteInput;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.graphics.drawable.IconCompat;
import android.text.TextUtils;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.FutureTarget;
import com.bumptech.glide.request.RequestOptions;
import com.sompom.tookitup.R;
import com.sompom.tookitup.chat.service.RemoteNotificationBroadcast;
import com.sompom.tookitup.database.MessageDb;
import com.sompom.tookitup.database.NotificationIdDb;
import com.sompom.tookitup.intent.ChatIntent;
import com.sompom.tookitup.intent.newintent.HomeIntent;
import com.sompom.tookitup.model.NotificationChannelSetting;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.viewmodel.binding.ImageViewBindingUtil;

import java.util.List;
import java.util.Objects;
import java.util.Random;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by He Rotha on 12/6/18.
 */
public final class ChatNotificationUtil {
    public static final String KEY_TEXT_REPLY = "key_text_reply";
    private static long[] sVibration;
    private static Uri sSound;

    private ChatNotificationUtil() {
    }

    public static void pushChatNotification(Context context, Chat chat) {
        Timber.e("pushChatNotification ");
        try {
            User myUser = SharedPrefUtils.getUser(context);
            final Observable<Bitmap> senderBitmap = downloadBitmap(context, chat.getSender());
            final Observable<Bitmap> myBitmap = downloadBitmap(context, myUser);
            final Observable<UserBitmap> userBitmap = Observable.zip(senderBitmap, myBitmap, (bitmap, bitmap2) -> {
                UserBitmap userBitmap12 = new UserBitmap();
                userBitmap12.mSenderBitmap = bitmap;
                userBitmap12.mMyBitmap = bitmap2;
                return userBitmap12;
            });
            userBitmap
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(userBitmap1 ->
                                    pushChatNotification(context, chat, userBitmap1.mSenderBitmap, myUser, userBitmap1.mMyBitmap),
                            throwable -> pushChatNotification(context, chat, null, myUser, null));
        } catch (Exception ex) {
            Timber.e("Exception: %s", ex.toString());
        }

    }

    private static void pushChatNotification(Context context,
                                             Chat chat,
                                             @Nullable Bitmap bitmap,
                                             User myUser,
                                             @Nullable Bitmap myBitmap) {

        final int notificationId = NotificationIdDb.getId(context, chat.getChannelId());

        Timber.e("push notificationId %s", notificationId);
        createNotificationChannel(context);
        NotificationCompat.Style style = createStyle(context, chat, bitmap, myUser, myBitmap);
        if (style == null) {
            Timber.e("cannot push, cos there are no unread message");
            return;
        }
        Intent intent = HomeIntent.openConversation(context, chat);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        Intent intent1 = new ChatIntent(context, chat);
        intent1.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        Intent intents[] = new Intent[]{intent, intent1};
        PendingIntent pendingIntent = PendingIntent.getActivities(context, notificationId, intents, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NotificationChannelSetting.Chat.getNotificationID())
                //no need to add title and message, cos MessageStyle manage it for us
//                .setContentTitle("Rotha")
//                .setContentText(messages.get(messages.size() - 1))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setStyle(style)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(pendingIntent)
                .setVibrate(getVibration())
                .setSound(getSound(context))
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setAutoCancel(true);

        if (bitmap != null) {
            builder.setLargeIcon(bitmap);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            NotificationCompat.Action action = createAction(context, notificationId, chat);
            builder.addAction(action);
        }

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(notificationId, builder.build());
    }

    /**
     * @return null, mean that no un-read message
     */
    private static NotificationCompat.Style createStyle(Context context,
                                                        Chat chat,
                                                        Bitmap bitmap,
                                                        User myUser,
                                                        Bitmap myBitmap) {
        Person.Builder senderBuilder = new Person.Builder()
                .setName(chat.getSender().getFullName())
                .setKey(chat.getSender().getId())
                .setBot(false)
                .setImportant(false);
        if (bitmap != null) {
            senderBuilder.setIcon(IconCompat.createWithBitmap(bitmap));
        }
        Person person = senderBuilder.build();

        Person.Builder myUserBuilder = new Person.Builder()
                .setName(myUser.getFullName())
                .setKey(myUser.getId())
                .setBot(false)
                .setImportant(false);
        if (myBitmap != null) {
            myUserBuilder.setIcon(IconCompat.createWithBitmap(myBitmap));
        }
        Person myPerson = myUserBuilder.build();

        NotificationCompat.MessagingStyle style = new NotificationCompat.MessagingStyle(myPerson);
        List<Chat> chatList = MessageDb.queryUnreadMessage(context, chat.getChannelId());
        if (chatList.isEmpty()) {
            return null;
        }
        for (Chat chatDt : chatList) {
            String message = chatDt.getActualContent(context);

            if (!TextUtils.isEmpty(message)) {
                style.addMessage(new NotificationCompat.MessagingStyle.Message(message, chatDt.getTime(), person));
                Timber.e("create style of " + person.getName() + " with message: " + message);
            }
        }
        Timber.e(" ----- ");

        return style;
    }

    @NonNull
    private static NotificationCompat.Action createAction(Context context, int notificationId, Chat chat) {
        RemoteInput remoteInput = new RemoteInput.Builder(KEY_TEXT_REPLY)
                .setLabel(context.getString(R.string.chat_notification_reply_button))
                .build();

        PendingIntent replyPendingIntent =
                PendingIntent.getBroadcast(context,
                        0,
                        RemoteNotificationBroadcast.getReplyMessageIntent(context, notificationId, chat),
                        PendingIntent.FLAG_UPDATE_CURRENT);

        return new NotificationCompat.Action.Builder(R.drawable.ic_play,
                context.getString(R.string.chat_notification_reply_button), replyPendingIntent)
                .addRemoteInput(remoteInput)
                .build();
    }

    private static void createNotificationChannel(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            final AudioAttributes att = new AudioAttributes.Builder().build();

            NotificationChannel channel =
                    new NotificationChannel(NotificationChannelSetting.Chat.getNotificationID(),
                            context.getString(NotificationChannelSetting.Chat.getNotificationName()),
                            NotificationManager.IMPORTANCE_HIGH);
            channel.setSound(getSound(context), att);
            channel.setVibrationPattern(getVibration());
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            Objects.requireNonNull(notificationManager).createNotificationChannel(channel);
        }
    }

    public static void cancelNotification(Context context, String notificationID) {
        final NotificationManagerCompat manager = NotificationManagerCompat.from(context);
        manager.cancel(NotificationIdDb.getId(context, notificationID));
    }

    private static long[] getVibration() {
        if (sVibration == null) {
            sVibration = new long[]{0, 500};
        }
        return sVibration;
    }

    private static Uri getSound(Context context) {
        if (sSound == null) {
            sSound = Uri.parse("android.resource://" + context.getPackageName() +
                    "/" + R.raw.get_coin_bonus);
        }

        return sSound;
    }

    public static void pushGeneralNotification(Context context, String title, String content) {
        Random rand = new Random();
        final int notificationId = rand.nextInt(99999) + 1;
        createNotificationChannel(context);

        Intent intent1 = new HomeIntent(context);
        intent1.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, notificationId, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NotificationChannelSetting.Home.getNotificationID())
                .setContentTitle(title)
                .setContentText(content)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(pendingIntent)
                .setVibrate(getVibration())
                .setSound(getSound(context))
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(notificationId, builder.build());
    }

    private static Observable<Bitmap> downloadBitmap(Context context, User user) {
        return Observable.create(e -> {
            try {
                if (TextUtils.isEmpty(user.getUserProfile())) {
                    //convert TextureDrawable to Bitmap
                    String name = user.getLastName();
                    Drawable drawable = ImageViewBindingUtil.getTextDrawable(context, name);
                    drawable = (DrawableCompat.wrap(drawable)).mutate();
                    Bitmap bitmap = Bitmap.createBitmap(300,
                            300, Bitmap.Config.ARGB_8888);
                    Canvas canvas = new Canvas(bitmap);
                    drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                    drawable.draw(canvas);
                    e.onNext(bitmap);
                    e.onComplete();
                } else {
                    FutureTarget<Bitmap> futureTarget = Glide
                            .with(context)
                            .asBitmap()
                            .load(user.getUserProfile())
                            .apply(new RequestOptions()
                                    .centerCrop()
                                    .dontAnimate()
                                    .circleCrop())
                            .submit(300, 300);

                    Bitmap bitmap = futureTarget.get();
                    e.onNext(bitmap);
                    e.onComplete();
                }
            } catch (Exception ex) {
                String name = user.getLastName();
                Drawable drawable = ImageViewBindingUtil.getTextDrawable(context, name);
                drawable = (DrawableCompat.wrap(drawable)).mutate();
                Bitmap bitmap = Bitmap.createBitmap(300,
                        300, Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                drawable.draw(canvas);
                e.onNext(bitmap);
                e.onComplete();
            }
        });
    }

    private static class UserBitmap {
        Bitmap mMyBitmap;
        Bitmap mSenderBitmap;
    }

}
