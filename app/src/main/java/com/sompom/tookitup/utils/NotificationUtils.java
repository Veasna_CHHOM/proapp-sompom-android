package com.sompom.tookitup.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.onesignal.service.NotificationIntentService;
import com.sompom.tookitup.intent.newintent.HomeIntent;
import com.sompom.tookitup.model.result.NotificationData;
import com.sompom.tookitup.model.result.OneSignalModel;
import com.sompom.tookitup.model.result.User;

import java.util.UUID;


/**
 * Created by he.rotha on 5/3/16.
 */
public final class NotificationUtils {
    private static final String TAG = NotificationUtils.class.getName();

    private NotificationUtils() {
    }


    public static void pushChatNotification(final Context context,
                                            final OneSignalModel data) {

        int notificationID = (int) UUID.fromString(data.getNotificationData().getConversation().getId())
                .getLeastSignificantBits();

        Intent intent = new Intent(context, NotificationIntentService.class);
        intent.putExtra(SharedPrefUtils.PRODUCT, data);

        final PendingIntent pIntent = PendingIntent.getService(context,
                notificationID,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        final NotificationManager mNotificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        User user = data.getNotificationData().getConversation().getRecipient(context);
        final String fullname = user.getFullName();

        final NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(getNotificationIcon())
                .setContentTitle(fullname)
                .setContentText(data.getNotificationData().getConversation().getLastMessage().getContent())
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContentIntent(pIntent)
                .setAutoCancel(true)
                .setVibrate(new long[]{0L})
                .setPriority(Notification.PRIORITY_HIGH)
                .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .setDefaults(Notification.DEFAULT_SOUND);
        NotificationCompat.InboxStyle style = new NotificationCompat.InboxStyle();
        style.setBuilder(builder);
        style.addLine(data.getNotificationData().getConversation().getLastMessage().getContent());
        style.setBigContentTitle(fullname);
        builder.setStyle(style);

        Notification chatNotificatin = style.build();
        mNotificationManager.notify(notificationID, chatNotificatin);
    }

    public static void pushNotification(Context context, OneSignalModel model) {
        if (model == null) {
            ToastUtil.showToast(context, "OneSignalModel == null");
            return;
        } else {
            if (model.getNotificationData() == null) {
                ToastUtil.showToast(context, "OneSignalModel.getData == null");
                return;
            }
            if (model.getType() == null) {
                ToastUtil.showToast(context, "OneSignalModel.getType == null");
                return;
            }
            if (model.getNotificationData().getBuyer() == null && model.getNotificationData()
                    .getUser() == null) {
                ToastUtil.showToast(context,
                        "OneSignalModel.getBuyer and OneSignalModel.getUser == null");
                return;
            }
        }

        String title = context.getString(R.string.app_name);
        String fullTextDescription = getFullTextDescription(context, model.getNotificationData(), model.getType());

        //Consider to take the message from notification directly
        if (fullTextDescription.isEmpty()) {
            fullTextDescription = model.getMessage();
        }

        if (model.getType() == OneSignalModel.Type.COMMENT) {
//            ProductDetailIntent intent = new ProductDetailIntent(context, model.getNotificationData().getProduct(), true, true);
//            pushNotification(context, title, fullTextDescription, intent);
        } else if (model.getType() == OneSignalModel.Type.COMMENT_LIFE_STREAM) {
//            LifeStreamIntent intent = new LifeStreamIntent(context, model.getNotificationData().getLifeStream().getId());
//            pushNotification(context, title, fullTextDescription, intent);
        } else if (model.getType() == OneSignalModel.Type.BUY) {
//            BuyerRequestDetailIntent intent = new BuyerRequestDetailIntent(context, model.getNotificationData().getProduct());
//            pushNotification(context, title, fullTextDescription, intent);
        } else if (model.getType() == OneSignalModel.Type.POST) {
//            ProductDetailIntent intent = new ProductDetailIntent(context, model.getNotificationData().getProduct(), false, true);
//            pushNotification(context, title, fullTextDescription, intent);
        } else {
            pushNotification(context, title, fullTextDescription);
        }
    }

    private static String getFullTextDescription(Context context, NotificationData notificationData, OneSignalModel.Type type) {
        if (notificationData != null) {
            String fullName = getFullNameFromNotification(context, notificationData, type);

            if (type == OneSignalModel.Type.POST) {
                return context.getString(R.string.post_new_product_notification, fullName);
            } else if (type == OneSignalModel.Type.BUY) {
                if (notificationData.getProduct() != null) {
                    return context.getString(R.string.product_request_notification,
                            fullName, notificationData.getProduct().getName());
                } else {
                    return "";
                }
            } else if (type == OneSignalModel.Type.COMMENT) {
                if (notificationData.getProduct() != null) {
                    return context.getString(R.string.comment_notification,
                            fullName, notificationData.getProduct().getName());
                } else {
                    return "";
                }
            } else if (type == OneSignalModel.Type.COMMENT_LIFE_STREAM) {
                if (notificationData.getLifeStream() != null) {
                    return context.getString(R.string.comment_notification,
                            fullName, notificationData.getLifeStream().getTitle());
                } else {
                    return "";
                }
            } else {
                //None define notification type
                return "";
            }
        } else {
            return "";
        }
    }

    private static String getFullNameFromNotification(Context context,
                                                      NotificationData notificationData,
                                                      OneSignalModel.Type type) {
        if (notificationData != null) {
            User user = null;

            if (type == OneSignalModel.Type.POST) {
                user = notificationData.getUser();
            } else if (type == OneSignalModel.Type.COMMENT) {
                user = notificationData.getBuyer();
            } else if (type == OneSignalModel.Type.BUY) {
                user = notificationData.getBuyer();
            } else if (type == OneSignalModel.Type.COMMENT_LIFE_STREAM) {
                user = notificationData.getUser();
            }
            //else {
            //None define notification type
            //}

            if (user != null) {
                return user.getFullName();
            } else {
                return "";
            }
        } else {
            return "";
        }
    }

    public static void pushNotification(Context context,
                                        String title,
                                        String subtitle) {
        int id = (int) System.currentTimeMillis();

        PendingIntent pIntent = PendingIntent.getActivity(context,
                id,
                new HomeIntent(context),
                PendingIntent.FLAG_UPDATE_CURRENT);


        pushNotification(context, title, subtitle, id, pIntent);
    }

    public static void pushNotification(Context context,
                                        String title,
                                        String subtitle,
                                        Intent intent) {
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        int id = (int) System.currentTimeMillis();
        PendingIntent pIntent = PendingIntent.getActivity(context,
                id,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        pushNotification(context, title, subtitle, id, pIntent);
//        int id = 1;
//
//        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
//        stackBuilder.addParentStack(ProductDetailActivity.class);
//        stackBuilder.addNextIntent(resultIntent);
//        PendingIntent resultPendingIntent =
//            stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
//        builder.setContentIntent(resultPendingIntent);
//        NotificationManager mNotificationManager =
//            (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        mNotificationManager.notify(id, builder.build());
//        pushNotification(context, title, subtitle, id, resultPendingIntent);

    }

    private static void pushNotification(Context context,
                                         String title,
                                         String subtitle,
                                         int notificationID,
                                         PendingIntent pIntent) {
        NotificationManager mNotificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

//        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
//            Notification.Builder builder = new Notification.Builder(context);
//            builder.setContentTitle(title)
//                .setContentText(subtitle)
//                .setSmallIcon(getNotificationIcon())
//                .setDefaults(Notification.DEFAULT_SOUND)
//                .setAutoCancel(true)
//                .setPriority(Notification.PRIORITY_HIGH)
//                .setContentIntent(pIntent);
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                builder.setColor(ContextCompat.getColor(context, R.color.colorPrimary));
//            }
//            Notification notification = new Notification.BigTextStyle(builder)
//                .bigText(subtitle).build();
//            mNotificationManager.notify(notificationID, notification);
//        } else {

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(getNotificationIcon())
                .setContentTitle(title)
                .setContentText(subtitle)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContentIntent(pIntent)
                .setAutoCancel(true)
                .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .setDefaults(Notification.DEFAULT_SOUND);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mBuilder.setPriority(Notification.PRIORITY_HIGH);
        }
        Notification notificaiton = new NotificationCompat.BigTextStyle(mBuilder)
                .bigText(subtitle).build();
        mNotificationManager.notify(notificationID, notificaiton);
//        }
    }

    private static int getNotificationIcon() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return R.drawable.ic_stat_tookitup;
        } else {
            return R.mipmap.ic_launcher;
        }
    }

    public enum NotificationType {
        ALL_NOTIFICATION("all_notification"),
        COMMENT_AFTER_ME_ON_PRODUCT("comment_after_me_on_product"),
        COMMENT_ON_PRODUCT_I_AM_SELLING("comment_on_product_i_am_selling"),
        SOMEONE_REQUEST_TO_PURCHASE_MY_PRODUCT("someone_request_to_purchase_my_product"),
        SELLER_I_FOLLOWED_POST_NEW_PRODUCT("seller_i_followed_post_new_product"),
        SELLER_I_FOLLOWED_UPDATE_PRODUCT("seller_i_followed_update_product");

        private String mNotification;

        NotificationType(String notification) {
            mNotification = notification;
        }

        public static NotificationType fromValue(String value) {
            for (NotificationType type : NotificationType.values()) {
                if (type.getNotification().equals(value)) {
                    return type;
                }
            }

            return NotificationType.ALL_NOTIFICATION;
        }

        public String getNotification() {
            return mNotification;
        }
    }
}