package com.sompom.tookitup.utils;

import android.content.Context;

/**
 * Created by He Rotha on 2/7/19.
 */
public final class VolumePlaying {
    private static Boolean sIsMuteVolume;

    private VolumePlaying() {
    }

    public static boolean isMute(Context context) {
        if (sIsMuteVolume == null) {
            sIsMuteVolume = SharedPrefUtils.isMuteVolume(context);
        }
        return sIsMuteVolume;
    }

    public static void updateMute(Context context, boolean isMute) {
        sIsMuteVolume = isMute;
        SharedPrefUtils.setMuteVolume(context, isMute);
    }
}
