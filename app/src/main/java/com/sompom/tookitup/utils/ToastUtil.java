package com.sompom.tookitup.utils;

import android.content.Context;
import android.support.annotation.StringRes;
import android.widget.Toast;

import com.sompom.tookitup.BuildConfig;

import timber.log.Timber;

/**
 * Created by He Rotha on 9/27/17.
 */

public final class ToastUtil {
    private ToastUtil() {
    }

    public static void showToast(Context context, String message) {
        if (BuildConfig.DEBUG) {
            if (context == null) {
                return;
            }
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            Timber.e(message);
        }
    }

    public static void showToast(Context context, @StringRes int message) {
        if (context == null) {
            return;
        }
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
