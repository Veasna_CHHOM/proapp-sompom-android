package com.sompom.tookitup.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import timber.log.Timber;

/**
 * Created by he.rotha on 4/10/17.
 */

public final class GeocoderUtil {
    private GeocoderUtil() {
    }

    public static String getCountry(Context context, double mLatitude, double mLongitude) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1);

            if (addresses != null && !addresses.isEmpty()) {
                return addresses.get(0).getCountryCode();
            }

        } catch (IOException ignored) {
            Timber.e("getCountryCode: " + ignored.toString());
        }
        return null;
    }

    public static void getCountry(final Context context,
                                  final double mLatitude,
                                  final double mLongitude,
                                  final Callback callback) {
        if (mLatitude <= 0 || mLongitude <= 0) {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            callback.onGetCountryCodeComplete(tm.getNetworkCountryIso());
        } else {
            new AsyncTask<Void, Void, Void>() {
                private String mCountry;

                @Override
                protected Void doInBackground(Void... params) {
                    Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                    List<Address> addresses;
                    try {
                        addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1);

                        if (addresses != null && !addresses.isEmpty()) {
                            mCountry = addresses.get(0).getCountryCode();
                        }

                    } catch (Exception ignored) {
                        Timber.e(ignored.toString());
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    callback.onGetCountryCodeComplete(mCountry);
                }
            }.execute();
        }
    }

    public interface Callback {
        void onGetCountryCodeComplete(String countryCode);
    }
}
