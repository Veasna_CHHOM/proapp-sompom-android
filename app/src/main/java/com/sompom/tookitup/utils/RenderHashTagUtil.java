package com.sompom.tookitup.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;

import com.sompom.tookitup.R;
import com.sompom.tookitup.helper.NavigateSellerStoreHelper;
import com.sompom.tookitup.model.result.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import timber.log.Timber;

/**
 * Created by nuonveyo on 11/14/18.
 */

public final class RenderHashTagUtil {
    private static final String PATTER_HAST_TAG = "[#]+[\\p{Alnum}\\p{M}*+_]+\\b";
    private static final String PATTER_USER_NAME = "+[@\\p{Alnum}\\p{M}*+_\\s]+[]]";
    private static final String PATTER_USER_ID = "[\\[]+[\\-A-Za-z0-9]+[:]";
    private static final String PATTER_USER_MENTION = "[@]+" + PATTER_USER_ID + PATTER_USER_NAME; // mention format @[user-id:user-name]

    private RenderHashTagUtil() {

    }

    public static Spannable renderMentionUser(Context context, String content) {
        SpannableStringBuilder builder = new SpannableStringBuilder(content);
        Pattern pattern = Pattern.compile(PATTER_USER_MENTION);
        Matcher matcher = pattern.matcher(content);

        while (matcher.find()) {
            final String substring = matcher.group();
            Matcher findUserName = Pattern.compile("[:]" + PATTER_USER_NAME).matcher(substring);
            Matcher findUserId = Pattern.compile(PATTER_USER_ID).matcher(substring);

            if (findUserName.find() && findUserId.find()) {
                final String userName = substring.substring(findUserName.start() + 1, findUserName.end() - 1);
                final String userId = substring.substring(findUserId.start() + 1, findUserId.end() - 1);

                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(userName);
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(@NonNull View textView) {
                        Timber.e("open userId: " + userName + " " + userId);
                        User user = new User();
                        user.setStoreName(userName);
                        user.setId(userId);
                        new NavigateSellerStoreHelper(context, user).openSellerStore();
                    }

                    @Override
                    public void updateDrawState(@NonNull TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                        int color = AttributeConverter.convertAttrToColor(context, R.attr.colorFirstText);
                        ds.setColor(color);
                    }
                };
                spannableStringBuilder.setSpan(clickableSpan,
                        0,
                        userName.length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                Typeface typefaceBold = ResourcesCompat.getFont(context, R.font.sf_pro_semibold);
                spannableStringBuilder.setSpan(new CustomTypefaceSpan("", typefaceBold),
                        0,
                        userName.length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                builder.replace(matcher.start(), matcher.end(), spannableStringBuilder);
                matcher.reset(builder.toString());

            }
        }
        return renderHashTag(context, builder);
    }

    private static Spannable renderHashTag(Context context, SpannableStringBuilder builder) {
        Pattern tagPatter = Pattern.compile(PATTER_HAST_TAG);
        Matcher tagMatcher = tagPatter.matcher(builder.toString());

        while (tagMatcher.find()) {
            String substring = tagMatcher.group();
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(@NonNull View textView) {
                    ToastUtil.showToast(context, substring);
                }

                @Override
                public void updateDrawState(@NonNull TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setUnderlineText(false);
                    int color = AttributeConverter.convertAttrToColor(context, R.attr.colorFirstText);
                    ds.setColor(color);
                }
            };
            builder.setSpan(clickableSpan,
                    tagMatcher.start(),
                    tagMatcher.end(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            Typeface typefaceBold = ResourcesCompat.getFont(context, R.font.sf_pro_semibold);
            builder.setSpan(new CustomTypefaceSpan("", typefaceBold),
                    tagMatcher.start(),
                    tagMatcher.end(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        }
        return builder;
    }

    public static Spannable renderDescriptionWithSeeMore(Context context, CharSequence charSequence, int lineEndIndex) {
        String seeMore = context.getString(R.string.home_see_more);
        String moreText = "... " + seeMore;

        CharSequence subSequence = charSequence.subSequence(0, lineEndIndex - moreText.length() - 1);
        SpannableStringBuilder builder = new SpannableStringBuilder(subSequence);
        builder.append(moreText);
        try {
            builder.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.darkGrey)),
                    builder.length() - seeMore.length(),
                    builder.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        } catch (Exception e) {
            Timber.e("renderDescriptionWithSeeMore: %s", e.getMessage());
        }
        return builder;
    }
}
