package com.sompom.tookitup.utils;

/**
 * Created by He Rotha on 10/24/17.
 */

@SuppressWarnings("DefaultFileTemplate")
public final class HTTPResponse {

    public static final int SUCCESS_CODE = 200;
    public static final int CREATE_SUCCESS_CODE = 201;
    public static final int BAD_REQUEST = 400;
    public static final int NOT_FOUND = 404;
    public static final int NOT_ACCEPTABLE = 406;
    public static final int CONFLICT = 409;
    public static final int NETWORK_ERROR = -1;
    public static final int INTERNAL_SERVER_ERROR = 500;
    public static final int FACEBOOK_AUTHENTICATION_ERROR = 911;

    private HTTPResponse() {
    }
}
