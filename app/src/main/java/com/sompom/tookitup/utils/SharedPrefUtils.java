package com.sompom.tookitup.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationManagerCompat;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.sinch.gson.reflect.TypeToken;
import com.sompom.tookitup.database.CurrencyDb;
import com.sompom.tookitup.database.RealmDb;
import com.sompom.tookitup.model.emun.AccountStatus;
import com.sompom.tookitup.model.emun.StoreStatus;
import com.sompom.tookitup.model.request.QueryProduct;
import com.sompom.tookitup.model.result.Category;
import com.sompom.tookitup.model.result.NotificationSettingModel;
import com.sompom.tookitup.model.result.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.realm.Realm;

/**
 * Created by he.rotha on 2/15/16.
 */
public final class SharedPrefUtils {
    public static final String CONVERSATION = "CONVERSATION";
    public static final String PRODUCT = "PRODUCT";
    public static final String USER_ID = "USER_ID";
    public static final String PATH = "PATH";
    public static final String IS_ACTIVITY = "IS_ACTIVITY";
    public static final String CATEGORY = "CATEGORY";
    public static final String STATUS = "STATUS";
    public static final String ACCOUNT_STATUS = "ACCOUNT_STATUS";
    public static final String IS_SHARE_FB = "IS_SHARE_FB";
    public static final String ID = "ID";
    public static final String DATA = "DATA";
    public static final String TITLE = "TITLE";

    private static final String FIRST_NAME = "FIRST_NAME";
    private static final String LAST_NAME = "LAST_NAME";
    private static final String EMAIL = "EMAIL";
    private static final String IMAGE_PATH = "IMAGE_PATH";
    private static final String IMAGE_COVER = "IMAGE_COVER";
    private static final String STORE_NAME = "STORE_NAME";
    private static final String QUERY_PRODUCT = "QUERY_PRODUCT";
    private static final String SHARE_PREF = "SHARE_PREF";
    private static final String PHONE = "PHONE";
    private static final String SOCIAL_ID = "SOCIAL_ID";
    private static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    private static final String LANGUAGE = "LANGUAGE";
    private static final String NOTIFICATION_SETTING = "NOTIFICATION_SETTING";
    private static final String IS_PUSH_NOTIFICATION = "IS_PUSH_NOTIFICATION";
    private static final String IS_MUTE = "IS_MUTE";
    private static final String LOGGED_IN_EMAIL = "LOGGED_IN_EMAIL";

    private SharedPrefUtils() {
    }


    public static boolean isLogin(Context context) {
        return !TextUtils.isEmpty(getUserId(context));
    }

    public static void addLoggedInEmail(Context context, String email) {
        String emails = getString(context, LOGGED_IN_EMAIL);
        if (TextUtils.isEmpty(emails)) {
            List<String> list = new ArrayList<>();
            list.add(email);
            setString(context, LOGGED_IN_EMAIL, new Gson().toJson(list));
        } else {
            List<String> list = new Gson().fromJson(emails, new TypeToken<List<String>>() {
            }.getType());
            Objects.requireNonNull(list).add(email);
            setString(context, LOGGED_IN_EMAIL, new Gson().toJson(list));
        }
    }

    public static boolean isEmailExist(Context context, String email) {
        List<String> list = new Gson().fromJson(getString(context, LOGGED_IN_EMAIL),
                new TypeToken<List<String>>() {
                }.getType());

        if (list != null && !list.isEmpty()) {
            for (String s : list) {
                if (s.matches(email)) {
                    return true;
                }
            }
        }

        return false;
    }

    private static SharedPreferences getPref(Context context) {
        return context.getSharedPreferences(
                SHARE_PREF,
                Context.MODE_PRIVATE);
    }

    public static void setString(Context context, String key, String value) {
        SharedPreferences pref = getPref(context);
        pref.edit().putString(key, value).apply();
    }

    public static String getString(Context context, String key) {
        return getPref(context).getString(key, "");
    }

    public static String getUserId(Context context) {
        if (context == null) {
            return null;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        return prefs.getString(USER_ID, null);
    }

    public static boolean checkIsMe(Context context, String id) {
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        final String myUserId = prefs.getString(USER_ID, "");
        return !TextUtils.isEmpty(myUserId) && myUserId.equals(id);
    }

    public static void setUserId(String userId, Context context) {
        if (context == null) {
            return;
        }
        OneSignalUtils.subscribeWithUserId(userId);

        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putString(USER_ID, userId).apply();
    }

    public static void setUserFirstName(String firstName, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putString(FIRST_NAME, firstName).apply();
    }

    public static String getUserFirstName(Context context) {
        if (context == null) {
            return "";
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        return prefs.getString(FIRST_NAME, null);
    }

    public static void setUserLastName(String lastName, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putString(LAST_NAME, lastName).apply();
    }

    public static String getAccessToken(Context context) {
        if (context == null) {
            return null;
        }
        String token;
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        token = prefs.getString(ACCESS_TOKEN, null);
        return token;
    }

    public static void setAccessToken(String accessToken, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putString(ACCESS_TOKEN, accessToken).apply();
    }

    public static String getUserLastName(Context context) {
        if (context == null) {
            return "";
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        return prefs.getString(LAST_NAME, null);
    }

    public static void setUserEmail(String email, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putString(EMAIL, email).apply();
    }

    public static String getUserEmail(Context context) {
        if (context == null) {
            return "";
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        return prefs.getString(EMAIL, null);
    }

    public static void setImageProfileUrl(String imageUrl, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putString(IMAGE_PATH, imageUrl).apply();
    }

    public static String getImageProfileUrl(Context context) {
        if (context == null) {
            return "";
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        return prefs.getString(IMAGE_PATH, null);
    }

    public static void setImageCover(String imageUrl, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putString(IMAGE_COVER, imageUrl).apply();
    }

    public static String getImageCover(Context context) {
        if (context == null) {
            return "";
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        return prefs.getString(IMAGE_COVER, null);
    }

    public static void setPhone(String value, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putString(PHONE, value).apply();
    }

    public static String getPhone(Context context) {
        if (context == null) {
            return "";
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        return prefs.getString(PHONE, "");
    }

    public static void setSocialId(String value, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putString(SOCIAL_ID, value).apply();
    }

    public static void setLanguage(String value, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putString(LANGUAGE, value).apply();
    }

    /**
     * it will return km, fr, en
     */
    public static String getLanguage(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        return prefs.getString(LANGUAGE, "");
    }


    /**
     * it will return kh, fr, en
     */
    public static String getProperLanguage(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        String language = prefs.getString(LANGUAGE, "en");
        if (language.equals("km")) {
            language = "kh";
        }
        return language;
    }

    public static void setStatus(StoreStatus value, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putInt(STATUS, value.getUserStatus()).apply();
    }

    public static StoreStatus getStatus(Context context) {
        if (context == null) {
            return StoreStatus.ACTIVE;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        int value = prefs.getInt(STATUS, StoreStatus.ACTIVE.getUserStatus());
        return StoreStatus.fromValue(value);
    }

    public static void setAccountStatus(AccountStatus value, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putInt(ACCOUNT_STATUS, value.getStatus()).apply();
    }

    public static AccountStatus getAccountStatus(Context context) {
        if (context == null) {
            return AccountStatus.OPEN;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        int value = prefs.getInt(ACCOUNT_STATUS, AccountStatus.OPEN.getStatus());
        return AccountStatus.fromValue(value);
    }

    public static void setNotificationSetting(Context context, NotificationSettingModel
            notificationSettingModel) {
        if (context != null) {
            SharedPreferences prefs = context.getSharedPreferences(
                    SHARE_PREF, Context.MODE_PRIVATE);
            String data = "";
            if (notificationSettingModel != null) {
                data = new Gson().toJson(notificationSettingModel);
            }
            prefs.edit().putString(NOTIFICATION_SETTING, data).apply();
        }
    }

    public static NotificationSettingModel getNotificationSetting(Context context) {
        if (context != null) {
            SharedPreferences prefs = context.getSharedPreferences(
                    SHARE_PREF, Context.MODE_PRIVATE);
            String data = prefs.getString(NOTIFICATION_SETTING, "");
            if (data.isEmpty()) {
                return new NotificationSettingModel();
            } else {
                return new Gson().fromJson(data, NotificationSettingModel.class);
            }
        } else {
            return null;
        }
    }

    public static void setPushNotification(Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putBoolean(IS_PUSH_NOTIFICATION, false).apply();
    }

    public static void setStoreName(String value, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putString(STORE_NAME, value).apply();
    }

    public static String getStoreName(Context context) {
        if (context == null) {
            return "";
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        return prefs.getString(STORE_NAME, "");
    }

    public static void clearValue(Context context) {
        if (context == null) {
            return;
        }
        OneSignalUtils.logout();
        Realm realm = RealmDb.getInstance(context);
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();

        setUserId(null, context);
        setUserFirstName(null, context);
        setUserLastName(null, context);
        setUserEmail(null, context);
        setImageProfileUrl(null, context);
        setPhone(null, context);
        setSocialId(null, context);
        setStatus(StoreStatus.ACTIVE, context);
        setAccountStatus(AccountStatus.OPEN, context);
        setAccessToken(null, context);
        setImageCover(null, context);
        setNotificationSetting(context, null);
        setPushNotification(context);
        setStoreName("", context);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.cancelAll();
    }

    public static void setUserValue(User user, Context context) {
        if (context == null) {
            return;
        }
        SharedPrefUtils.setUserEmail(user.getEmail(), context);
        SharedPrefUtils.setUserFirstName(user.getFirstName(), context);
        SharedPrefUtils.setUserLastName(user.getLastName(), context);
        SharedPrefUtils.setUserId(user.getId(), context);
        SharedPrefUtils.setImageProfileUrl(user.getUserProfile(), context);
        SharedPrefUtils.setPhone(user.getPhone(), context);
        SharedPrefUtils.setSocialId(user.getSocialId(), context);
        SharedPrefUtils.setStatus(user.getStatus(), context);
        SharedPrefUtils.setImageCover(user.getUserCoverProfile(), context);
        SharedPrefUtils.setStoreName(user.getStoreName(), context);
        SharedPrefUtils.setAccountStatus(user.getAccountStatus(), context);
        if (!TextUtils.isEmpty(user.getCurrency())) {
            CurrencyDb.updateSelectCurrency(context, user.getCurrency());
        }
        if (!TextUtils.isEmpty(user.getAccessToken())) {
            SharedPrefUtils.setAccessToken(user.getAccessToken(), context);
        }
    }

    public static User getUser(Context context) {
        User user = new User();
        user.setId(SharedPrefUtils.getUserId(context));
        user.setFirstName(SharedPrefUtils.getUserFirstName(context));
        user.setLastName(SharedPrefUtils.getUserLastName(context));
        user.setUserProfile(SharedPrefUtils.getImageProfileUrl(context));
        user.setUserCoverProfile(SharedPrefUtils.getImageCover(context));
        user.setPhone(SharedPrefUtils.getPhone(context));
        user.setEmail(SharedPrefUtils.getUserEmail(context));
        user.setStoreName(SharedPrefUtils.getStoreName(context));
        return user;
    }

    public static void setQueryProduct(QueryProduct queryProduct, Context context) {
        if (context == null) {
            return;
        }
        if (queryProduct.getCategories() != null) {
            for (Category category : queryProduct.getCategories()) {
                category.setCheck(true);
            }
        }
        Gson gson = new Gson();
        String json = gson.toJson(queryProduct);
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putString(QUERY_PRODUCT, json).apply();
    }

    public static QueryProduct getQueryProduct(Context context) {
        if (context == null) {
            return new QueryProduct(context);
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        String json = prefs.getString(QUERY_PRODUCT, null);
        if (!TextUtils.isEmpty(json)) {
            Gson gson = new Gson();
            return gson.fromJson(json, QueryProduct.class);
        } else {
            return new QueryProduct(context);
        }
    }

    public static void setMuteVolume(Context context, boolean isMute) {
        if (context == null) {
            return;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putBoolean(IS_MUTE, isMute).apply();
    }

    public static boolean isMuteVolume(Context context) {
        if (context == null) {
            return false;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        return prefs.getBoolean(IS_MUTE, false);
    }

    public static User getCurrentUser(Context context) {
        User user = new User();
        user.setId(SharedPrefUtils.getUserId(context));
        user.setFirstName(SharedPrefUtils.getUserFirstName(context));
        user.setLastName(SharedPrefUtils.getUserLastName(context));
        user.setUserProfile(SharedPrefUtils.getImageProfileUrl(context));
        return user;
    }
}
