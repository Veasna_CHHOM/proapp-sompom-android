package com.sompom.tookitup.utils;

import android.content.Context;
import android.text.TextUtils;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.sompom.tookitup.listener.OnCompleteListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.AdsItem;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.WallStreetAds;
import com.sompom.tookitup.model.emun.AdsUnit;
import com.sompom.tookitup.model.emun.MediaType;
import com.sompom.tookitup.model.result.ContentStat;
import com.sompom.tookitup.model.result.WallLoadMoreWrapper;
import com.sompom.tookitup.services.ApiService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by He Rotha on 2/6/18.
 */

public final class AdsLoaderUtil {
    private static final String FIELD_TITLE = "title";
    private static final String FIELD_SUBTITLE = "subTittle";
    private static final String FIELD_CONTENT_ID = "contentId";
    private static final String FIELD_CREATIVE_ICON = "creativeIcon";
    private static final String FIELD_CREATIVE_BANNER = "creativeBanner";
    private static final String FIELD_REDIRECTION = "redirection";

    private AdsLoaderUtil() {
    }

    public static Observable<WallLoadMoreWrapper<Adaptive>> loadStreetWall(Context context,
                                                                           ApiService apiService,
                                                                           WallLoadMoreWrapper<Adaptive> data) {
        return Observable.create((ObservableOnSubscribe<WallStreetAds>) e -> {
            List<WallStreetAds> findAds = findAdsItem(data);
            for (WallStreetAds findAd : findAds) {
                e.onNext(findAd);
            }
            e.onComplete();
        }).flatMap(new Function<WallStreetAds, ObservableSource<WallStreetAds>>() {
            @Override
            public ObservableSource<WallStreetAds> apply(WallStreetAds integerWallStreetAdsPair) {
                return Observable.create(e -> loadAds(context, integerWallStreetAdsPair, result -> {
                    e.onNext(integerWallStreetAdsPair);
                    e.onComplete();
                }));
            }
        }).flatMap((Function<WallStreetAds, ObservableSource<WallStreetAds>>) integerWallStreetAdsPair -> {
            //TODO: remove api below, cos it just testing
            return apiService.getUserById(SharedPrefUtils.getUserId(context))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .concatMap(userResponse -> {
                        //TODO: set contentSet to exist ads
                        ContentStat contentStat = new ContentStat();
                        contentStat.setTotalComments(1L);
                        contentStat.setTotalLikes(1L);
                        integerWallStreetAdsPair.setContentStat(contentStat);
                        return Observable.just(integerWallStreetAdsPair);
                    }).onErrorResumeNext(throwable -> {
                        Timber.e(throwable);
                        return Observable.just(integerWallStreetAdsPair);
                    });
        }).toList().toObservable()
                .concatMap((Function<List<WallStreetAds>, ObservableSource<WallLoadMoreWrapper<Adaptive>>>) objects -> {
                    for (int i = data.getData().size() - 1; i >= 0; i--) {
                        Adaptive datum = data.getData().get(i);
                        if (datum instanceof WallStreetAds && TextUtils.isEmpty(((WallStreetAds) datum).getTitle())) {
                            data.getData().remove(i);
                        }
                    }
                    return Observable.just(data);
                });

    }

    private static List<WallStreetAds> findAdsItem(WallLoadMoreWrapper<Adaptive> data) {
        List<WallStreetAds> adsItem = new ArrayList<>();
        for (int i = 0; i < data.getData().size(); i++) {
            Adaptive adaptive = data.getData().get(i);
            if (adaptive instanceof WallStreetAds) {
                adsItem.add((WallStreetAds) adaptive);
            }
        }
        return adsItem;
    }


    private static void loadAds(Context context,
                                WallStreetAds adaptive,
                                OnCompleteListener<Adaptive> listener) {
        PublisherAdRequest publisher = new PublisherAdRequest.Builder().build();
        VideoOptions videoOptions = new VideoOptions.Builder()
                .setStartMuted(true)
                .build();

        NativeAdOptions adOptions = new NativeAdOptions.Builder()
                .setVideoOptions(videoOptions)
                .setReturnUrlsForImageAssets(true)
                .build();

        AdLoader.Builder builder = new AdLoader.Builder(context, AdsUnit.STREET_WALL.getAdsUnitID());
        builder.withNativeAdOptions(adOptions);
        builder.forCustomTemplateAd(AdsUnit.STREET_WALL.getTemplateID(),
                ad -> {
                    CharSequence id = ad.getText(FIELD_CONTENT_ID);
                    if (!TextUtils.isEmpty(id)) {
                        adaptive.setId(id.toString());
                    }

                    CharSequence title = ad.getText(FIELD_TITLE);
                    if (!TextUtils.isEmpty(title)) {
                        adaptive.setTitle(title.toString());
                    }

                    CharSequence subTitle = ad.getText(FIELD_SUBTITLE);
                    if (!TextUtils.isEmpty(subTitle)) {
                        adaptive.setSubTitle(subTitle.toString());
                    }

                    NativeAd.Image icon = ad.getImage(FIELD_CREATIVE_ICON);
                    if (icon != null && icon.getUri() != null) {
                        adaptive.setCreativeIconUrl(icon.getUri().toString());
                    }

                    CharSequence redirect = ad.getText(FIELD_REDIRECTION);
                    if (!TextUtils.isEmpty(redirect)) {
                        adaptive.setRedicationUrl(redirect.toString());
                    }

                    NativeAd.Image banner = ad.getImage(FIELD_CREATIVE_BANNER);
                    if (banner != null && banner.getUri() != null) {
                        adaptive.setCreativeBannerUrl(banner.getUri().toString());
                    }

                    adaptive.setNativeAd(ad);
                    Media media = new Media();
                    media.setUrl(adaptive.getCreativeBannerUrl());
                    media.setType(MediaType.IMAGE);
                    adaptive.setMedia(Collections.singletonList(media));
                    listener.onComplete(adaptive);
                },
                (ad, s) -> Timber.e("onCustomClick: " + s));
        builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                Timber.e("onAdFailedToLoad: " + errorCode);
                listener.onComplete(null);
            }
        });

        builder.build().loadAd(publisher);
    }


    public static Observable<List<AdsItem>> load(Context context,
                                                 List<AdsItem> unitIdAndTemplateId) {
        ObservableOnSubscribe<List<AdsItem>> field = e -> {
            PublisherAdRequest publisher = new PublisherAdRequest.Builder().build();
            VideoOptions videoOptions = new VideoOptions.Builder()
                    .setStartMuted(true)
                    .build();

            NativeAdOptions adOptions = new NativeAdOptions.Builder()
                    .setVideoOptions(videoOptions)
                    .setReturnUrlsForImageAssets(true)
                    .build();

            final int[] count = {unitIdAndTemplateId.size()};
            if (unitIdAndTemplateId.isEmpty()) {
                e.onNext(unitIdAndTemplateId);
                e.onComplete();
            } else {
                for (AdsItem adsItem : unitIdAndTemplateId) {
                    AdsUnit adsUnit = adsItem.getItemAdsType().getAdsUnit();
                    if (adsUnit == null) {
                        count[0]--;
                        if (count[0] == 0) {
                            e.onNext(unitIdAndTemplateId);
                            e.onComplete();
                        }
                    } else {
                        AdLoader.Builder builder = new AdLoader.Builder(context, adsUnit.getAdsUnitID());
                        builder.withNativeAdOptions(adOptions);
                        builder.forCustomTemplateAd(adsUnit.getTemplateID(),
                                ad -> {
                                    Timber.e("load ads success: " + adsUnit);
                                    adsItem.setAds(ad);
                                    count[0]--;
                                    if (count[0] == 0) {
                                        e.onNext(unitIdAndTemplateId);
                                        e.onComplete();
                                    }
                                },
                                (ad, s) -> Timber.e("onCustomClick: " + s));
                        builder.withAdListener(new AdListener() {
                            @Override
                            public void onAdFailedToLoad(int errorCode) {
                                Timber.e("onAdFailedToLoad: " + errorCode);

                                count[0]--;
                                if (count[0] == 0) {
                                    e.onNext(unitIdAndTemplateId);
                                    e.onComplete();
                                }
                            }
                        });

                        builder.build().loadAd(publisher);
                    }
                }
            }
        };
        return Observable.create(field);
    }


}
