package com.sompom.tookitup.utils;

import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.sompom.tookitup.R;
import com.sompom.tookitup.TookitupApplication;
import com.sompom.tookitup.database.CurrencyDb;

import java.util.Locale;

/**
 * Created by imac on 5/5/17.
 */

public final class UpdateLanguage {
    private UpdateLanguage() {
    }

    public static void initLanguage(TookitupApplication context) {
        String ios3LocalLanguage = SharedPrefUtils.getLanguage(context);
        if (TextUtils.isEmpty(ios3LocalLanguage)) {
            Locale locale = CurrencyDb.getLocale(context);
            ios3LocalLanguage = locale.getISO3Language();
            String[] supportLanguages = context.getResources().getStringArray(R.array.support_lang_iso3);

            boolean isContain = false;
            for (String supportLanguage : supportLanguages) {
                if (supportLanguage.equals(ios3LocalLanguage)) {
                    isContain = true;
                    break;
                }
            }

            if (!isContain) {
                ios3LocalLanguage = supportLanguages[0];
            }

            SharedPrefUtils.setLanguage(ios3LocalLanguage, context);
        }
        UpdateLanguage.updateLanguage(context, ios3LocalLanguage);
    }

    public static void updateLanguage(TookitupApplication context, String localString) {
        Configuration conf = context.getResources().getConfiguration();
        conf.locale = new Locale(localString);
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(Activity.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);
        context.getResources().updateConfiguration(conf, metrics);
        Resources resources = new Resources(context.getAssets(), metrics, conf);
        resources.updateConfiguration(conf, metrics);
        context.getResources().updateConfiguration(conf, metrics);


        Configuration configuration = context.getBaseContext().getResources().getConfiguration();
        configuration.locale = conf.locale;
        context.getBaseContext().getResources()
                .updateConfiguration(configuration, context.getResources().getDisplayMetrics());

        Configuration systemConf = Resources.getSystem().getConfiguration();
        systemConf.locale = conf.locale;
        Resources.getSystem().updateConfiguration(systemConf, Resources.getSystem().getDisplayMetrics());

        Locale.setDefault(conf.locale);
//        update(context, localString);
    }
//    private static void update(TookitupApplication context, String localString) {
//        Configuration conf = context.getResources().getConfiguration();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            conf.setLocale(new Locale(localString));
//        } else {
//            conf.locale = new Locale(localString);
//        }
//
//        DisplayMetrics metrics = new DisplayMetrics();
//        WindowManager wm = (WindowManager) context.getSystemService(Activity.WINDOW_SERVICE);
//        wm.getDefaultDisplay().getMetrics(metrics);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            context.createConfigurationContext(conf);
//
//            Resources resources = Resources.getSystem();
//            resources.getConfiguration().setLocale(conf.getLocales().get(0));
//        } else {
//            context.getResources().updateConfiguration(conf, metrics);
//
//            Resources resources = new Resources(context.getAssets(), metrics, conf);
//            resources.updateConfiguration(conf, metrics);
//        }
//
////        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
////            Configuration configuration = context.getBaseContext().getResources().getConfiguration();
////            configuration.setLocale(conf.getLocales().get(0));
////            context.getBaseContext().createConfigurationContext(configuration);
////
////            Configuration systemConf = Resources.getSystem().getConfiguration();
////            systemConf.setLocale(conf.getLocales().get(0));
////
////            Resources.getSystem().getConfiguration().setLocale(conf.getLocales().get(0));
////        } else {
//            Configuration configuration = context.getBaseContext().getResources().getConfiguration();
//            configuration.locale = conf.locale;
//            context.getBaseContext()
// .getResources().updateConfiguration(configuration, context.getResources().getDisplayMetrics());
//
//            Configuration systemConf = Resources.getSystem().getConfiguration();
//            systemConf.locale = conf.locale;
//            Resources.getSystem().updateConfiguration(systemConf, Resources.getSystem().getDisplayMetrics());
////        }
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            Locale.setDefault(conf.getLocales().get(0));
//        } else {
//            Locale.setDefault(conf.locale);
//        }
//    }


}
