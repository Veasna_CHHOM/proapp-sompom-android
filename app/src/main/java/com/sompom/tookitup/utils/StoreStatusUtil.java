package com.sompom.tookitup.utils;

import android.content.Context;

import com.sompom.tookitup.R;
import com.sompom.tookitup.model.result.User;

/**
 * Created by He Rotha on 9/27/17.
 */

public final class StoreStatusUtil {
    private StoreStatusUtil() {
    }

    public static String getUserStatus(Context context, User user) {
        String value = "";
        if (user != null) {
            if (user.isIsRegisterWithPhone()) {
                value = context.getString(R.string.seller_store_authenticated_by_phone_only_title);
            }

            if (user.isIsRegisterWithFb()) {
                value = context.getString(R.string.seller_store_authenticated_by_facebook_only_title);
            }
            if (user.isIsRegisterWithFb() && user.isIsRegisterWithPhone()) {
                value = context.getString(R.string.seller_store_authenticated_by_facebook_and_mobile_phone_title);
            }
        }
        return value;
    }
}
