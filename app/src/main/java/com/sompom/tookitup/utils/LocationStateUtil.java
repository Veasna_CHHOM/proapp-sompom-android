package com.sompom.tookitup.utils;

import android.content.Context;
import android.location.LocationManager;

/**
 * Created by He Rotha on 9/27/17.
 */

public final class LocationStateUtil {
    private LocationStateUtil() {
    }

    public static boolean isLocationEnable(Context context) {
        LocationManager service = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return service.isProviderEnabled(LocationManager.GPS_PROVIDER)
                || service.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

    }
}
