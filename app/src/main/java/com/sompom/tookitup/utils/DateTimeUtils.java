package com.sompom.tookitup.utils;

import android.content.Context;
import android.text.format.DateUtils;

import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by ChhomVeasna on 8/17/16.
 */
public final class DateTimeUtils {
    public static int getHours(long milli) {
        Date currentDate = new Date(System.currentTimeMillis());
        Date previousDate = new Date(milli);
        long timeBetweenDate = currentDate.getTime() - previousDate.getTime();
        return (int) TimeUnit.MILLISECONDS.toHours(timeBetweenDate);
    }

    public static String getAudioTime(long milli) {
        return String.format(Locale.getDefault(), "%2d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(milli),
                TimeUnit.MILLISECONDS.toSeconds(milli) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milli))
        );
    }

    public static String parse(Context context, long time) {
        return DateUtils.getRelativeDateTimeString(

                context, // Suppose you are in an activity or other Context subclass

                time, // The time to display

                DateUtils.MINUTE_IN_MILLIS, // The resolution. This will display only
                // minutes (no "3 seconds ago")


                DateUtils.WEEK_IN_MILLIS, // The maximum resolution at which the time will switch
                // to default date instead of spans. This will not
                // display "3 weeks ago" but a full date instead

                0).toString(); // Eventual flags
    }
}
