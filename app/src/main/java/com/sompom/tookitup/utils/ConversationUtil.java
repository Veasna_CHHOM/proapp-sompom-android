package com.sompom.tookitup.utils;

/**
 * Created by He Rotha on 11/13/18.
 */
public final class ConversationUtil {
    private ConversationUtil() {
    }

    public static String getConversationId(String first,
                                           String second) {

        //format [userId_userId] order Id by alphabet
        int value = first.compareTo(second);
        String newText;
        if (value < 0) {
            newText = first + "_" + second;
        } else {
            newText = second + "_" + first;
        }
        return newText;

    }

    public static String getProductConversationId(String productId,
                                                  String first,
                                                  String second) {


        //format productID_[userId_userId] order Id by alphabet
        return productId + "_" + getConversationId(first, second);

    }

}
