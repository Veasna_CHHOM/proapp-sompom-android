package com.sompom.tookitup.utils;

import com.sompom.tookitup.model.BaseChatModel;
import com.sompom.tookitup.model.emun.ChatBg;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.model.result.User;

import java.util.List;

/**
 * Created by He Rotha on 7/16/18.
 */
public final class ChatUtility {
    private ChatUtility() {
    }

    public static GroupMessage getGroupMessage(List<BaseChatModel> data,
                                               int position,
                                               User user,
                                               boolean isMe) {
        GroupMessage group = getGroupMessage(data, position);
        ChatBg bg = getBackground(data, position, user, isMe);
        group.setChatBg(bg);
        return group;
    }

    private static ChatBg getBackground(List<BaseChatModel> data,
                                        int position,
                                        User user,
                                        boolean isMe) {


        State start = getBefore(data, position, user);
        State end = getAfter(data, position, user);
        if (!isMe) {
            if (start == State.U_START && end == State.U_END) {
                return ChatBg.ChatYouMid;
            } else {
                boolean notInGroupTop = start == State.ZERO || start == State.ME_START;
                boolean notInGroupBottom = end == State.END || end == State.ME_END;

                if (notInGroupTop && notInGroupBottom) {
                    return ChatBg.ChatYouSingle;
                } else if (notInGroupTop) {
                    return ChatBg.ChatYouTop;
                } else {
                    return ChatBg.ChatYouBottom;
                }
            }
        } else {
            if (start == State.ME_START && end == State.ME_END) {
                return ChatBg.ChatMeMid;
            } else {
                boolean notInGroupTop = start == State.ZERO || start == State.U_START;
                boolean notInGroupBottom = end == State.END || end == State.U_END;

                if (notInGroupTop && notInGroupBottom) {
                    return ChatBg.ChatMeSingle;
                } else if (notInGroupTop) {
                    return ChatBg.ChatMeTop;
                } else {
                    return ChatBg.ChatMeBottom;
                }
            }
        }
    }

    private static GroupMessage getGroupMessage(List<BaseChatModel> data,
                                                int position) {
        GroupMessage groupMessage = new GroupMessage();
        int newPosition = position - 1;
        if (newPosition >= 0 && data.get(newPosition) instanceof Chat) {
            Chat chat = (Chat) data.get(newPosition);
            long remainTime = Math.abs(chat.getTime() - data.get(position).getTime());
            groupMessage.setShowTime(remainTime > 3600 * 1000); //1 hour
        } else {
            groupMessage.setShowTime(true);
        }
        return groupMessage;
    }

    private static State getBefore(List<BaseChatModel> data,
                                   int position,
                                   User user) {
        int newPosition = position - 1;
        if (newPosition >= 0 && data.get(newPosition) instanceof Chat) {
            Chat chat = (Chat) data.get(newPosition);
            long remainTime = Math.abs(chat.getTime() - data.get(position).getTime());

            if (remainTime > 60000) {//1minute
                return State.ZERO;
            }
            if (user.getId().equals(chat.getSenderId())) {
                return State.ME_START;
            } else {
                return State.U_START;
            }
        } else {
            return State.ZERO;
        }

    }

    private static State getAfter(List<BaseChatModel> data,
                                  int position,
                                  User user) {
        int newPosition = position + 1;
        int size = data.size();
        if (newPosition < size && data.get(newPosition) instanceof Chat) {
            Chat chat = (Chat) data.get(newPosition);
            long remainTime = Math.abs(data.get(position).getTime() - chat.getTime());
            if (remainTime > 60000) { //1minute
                return State.END;
            }

            if (user.getId().equals(chat.getSenderId())) {
                return State.ME_END;
            } else {
                return State.U_END;
            }
        } else {
            return State.END;
        }

    }

    private enum State {
        ZERO, END, ME_START, U_START, ME_END, U_END
    }

    public static class GroupMessage {
        private boolean mIsShowTime;
        private ChatBg mChatBg;

        public boolean isShowTime() {
            return mIsShowTime;
        }

        void setShowTime(boolean showTime) {
            mIsShowTime = showTime;
        }

        public ChatBg getChatBg() {
            return mChatBg;
        }

        void setChatBg(ChatBg chatBg) {
            mChatBg = chatBg;
        }
    }
}
