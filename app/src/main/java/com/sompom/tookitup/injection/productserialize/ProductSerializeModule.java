package com.sompom.tookitup.injection.productserialize;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sompom.tookitup.helper.AdapterDeserializer;
import com.sompom.tookitup.injection.controller.ControllerScope;
import com.sompom.tookitup.injection.controller.NetworkModule;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.services.ApiService;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by He Rotha on 10/2/17.
 */
@Module(includes = NetworkModule.class)
public class ProductSerializeModule {

    @Provides
    @ProductSerializeQualifier
    @ControllerScope
    public ApiService getProductService(@ProductSerializeQualifier Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @Provides
    @ProductSerializeQualifier
    @ControllerScope
    public Retrofit.Builder getProductRetrofitBuilder(OkHttpClient okHttpClient,
                                                      @ProductSerializeQualifier Gson gson) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson));
    }

    @Provides
    @ProductSerializeQualifier
    @ControllerScope
    public Retrofit getProductRetrofit(@ControllerScope String url, @ProductSerializeQualifier Retrofit.Builder builder) {
        return builder.baseUrl(url).build();
    }

    @Provides
    @ProductSerializeQualifier
    @ControllerScope
    public Gson getDeserializerGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Adaptive.class, new AdapterDeserializer());
        return builder.create();
    }
}
