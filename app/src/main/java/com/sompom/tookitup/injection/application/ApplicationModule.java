package com.sompom.tookitup.injection.application;

import android.app.Application;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Rotha on 8/17/2017.
 */
@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @ApplicationScope
    Application applicationContext() {
        return mApplication;
    }

}
