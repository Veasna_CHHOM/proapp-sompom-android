package com.sompom.tookitup.injection.broadcast;

import com.sompom.tookitup.broadcast.onesignal.OneSignalReceiver;
import com.sompom.tookitup.broadcast.upload.LifeStreamUploadService;
import com.sompom.tookitup.broadcast.upload.ProductUploadService;
import com.sompom.tookitup.chat.service.SocketService;
import com.sompom.tookitup.injection.controller.ControllerScope;

import dagger.Subcomponent;

/**
 * Created by Rotha on 8/19/2017.
 */
@Subcomponent(modules = {BroadcastModule.class})
@ControllerScope
public interface BroadcastComponent {

    void inject(ProductUploadService receiver);

    void inject(LifeStreamUploadService lifeStreamUploadService);

    void inject(SocketService socketBinder);

    void inject(OneSignalReceiver oneSignalReceiver);
}
