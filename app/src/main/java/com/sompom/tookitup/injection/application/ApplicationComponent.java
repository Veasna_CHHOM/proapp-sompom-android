package com.sompom.tookitup.injection.application;

import com.sompom.tookitup.injection.broadcast.BroadcastComponent;
import com.sompom.tookitup.injection.broadcast.BroadcastModule;
import com.sompom.tookitup.injection.controller.ControllerComponent;
import com.sompom.tookitup.injection.controller.ControllerModule;
import com.sompom.tookitup.injection.token.TokenComponent;
import com.sompom.tookitup.injection.token.TokenModule;

import dagger.Component;

/**
 * Created by Rotha on 8/17/2017.
 */

@ApplicationScope
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    // Each subcomponent can depend on more than one module
    ControllerComponent newControllerComponent(ControllerModule module);

    //    BroadcastComponent newServiceComponent(BroadcastModule module);
    BroadcastComponent newBroadcastComponent(BroadcastModule serviceModule);

    TokenComponent newTokenComponent(TokenModule serviceModule);


}