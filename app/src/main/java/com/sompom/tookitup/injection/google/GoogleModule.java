package com.sompom.tookitup.injection.google;

import com.google.gson.Gson;
import com.sompom.tookitup.injection.controller.ControllerScope;
import com.sompom.tookitup.injection.controller.NetworkModule;
import com.sompom.tookitup.services.ApiGoogle;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by He Rotha on 10/2/17.
 */
@Module(includes = NetworkModule.class)
public class GoogleModule {

    @Provides
    @GoogleQualifier
    @ControllerScope
    public String getGoogleUrl() {
        return "https://maps.googleapis.com/";
    }

    @Provides
    @GoogleQualifier
    @ControllerScope
    public Retrofit getGoogleRetrofit(@GoogleQualifier Retrofit.Builder builder, @GoogleQualifier String url) {
        return builder.baseUrl(url).build();
    }

    @Provides
    @GoogleQualifier
    @ControllerScope
    public ApiGoogle getGoogleServices(@GoogleQualifier Retrofit retrofit) {
        return retrofit.create(ApiGoogle.class);
    }

    @Provides
    @GoogleQualifier
    @ControllerScope
    public Retrofit.Builder gettGoogleRetrofitBuilder(@GoogleQualifier OkHttpClient okHttpClient, Gson gson) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson));
    }

    @Provides
    @GoogleQualifier
    @ControllerScope
    public OkHttpClient gettGoogleOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor) {
        return new OkHttpClient.Builder()
                .readTimeout(1, TimeUnit.MINUTES)
                .addInterceptor(httpLoggingInterceptor)
                .connectTimeout(1, TimeUnit.MINUTES)
                .build();
    }


}
