package com.sompom.tookitup.injection.controller;


import com.sompom.tookitup.injection.google.GoogleModule;
import com.sompom.tookitup.newui.dialog.ChangePasswordDialog;
import com.sompom.tookitup.newui.dialog.ForgotPasswordDialog;
import com.sompom.tookitup.newui.dialog.ListFollowDialog;
import com.sompom.tookitup.newui.dialog.OtherUserProfileDialog;
import com.sompom.tookitup.newui.dialog.UserStoreBottomSheetDialog;
import com.sompom.tookitup.newui.fragment.AdvancedNotificationSettingFragment;
import com.sompom.tookitup.newui.fragment.AllMessageFragment;
import com.sompom.tookitup.newui.fragment.ChatFragment;
import com.sompom.tookitup.newui.fragment.CreateTimelineFragment;
import com.sompom.tookitup.newui.fragment.DetailHistoryProductConversationFragment;
import com.sompom.tookitup.newui.fragment.FilterDetailFragment;
import com.sompom.tookitup.newui.fragment.ForwardFragment;
import com.sompom.tookitup.newui.fragment.HomeFragment;
import com.sompom.tookitup.newui.fragment.MyWallStreetFragment;
import com.sompom.tookitup.newui.fragment.NearbyMapFragment;
import com.sompom.tookitup.newui.fragment.PopUpCommentFragment;
import com.sompom.tookitup.newui.fragment.ProductFormFragment;
import com.sompom.tookitup.newui.fragment.ReplyCommentFragment;
import com.sompom.tookitup.newui.fragment.SearchAddressFragment;
import com.sompom.tookitup.newui.fragment.SearchGeneralContainerResultFragment;
import com.sompom.tookitup.newui.fragment.SearchGeneralFragment;
import com.sompom.tookitup.newui.fragment.SearchGeneralTypeResultFragment;
import com.sompom.tookitup.newui.fragment.SearchResultFragment;
import com.sompom.tookitup.newui.fragment.SignUpFragment;
import com.sompom.tookitup.newui.fragment.SuggestionFragment;
import com.sompom.tookitup.newui.fragment.TabSuggestionFragment;
import com.sompom.tookitup.newui.fragment.TimelineDetailFragment;
import com.sompom.tookitup.newui.fragment.UserListFragment;
import com.sompom.tookitup.newui.fragment.WallStreetFragment;
import com.sompom.tookitup.widget.LikeViewerLayout;

import dagger.Subcomponent;

/**
 * Created by Rotha on 8/17/2017.
 */

@Subcomponent(modules = {ControllerModule.class, GoogleModule.class, PublicModule.class})
@ControllerScope
public interface ControllerComponent {

    void inject(AdvancedNotificationSettingFragment fragment);

    void inject(CreateTimelineFragment fragment);

    void inject(com.sompom.tookitup.newui.fragment.SellerStoreFragment fragment);

    void inject(com.sompom.tookitup.newui.fragment.ProductStoreFragment fragment);

    void inject(com.sompom.tookitup.newui.fragment.ProductDetailFragment fragment);

    void inject(com.sompom.tookitup.newui.SellerStoreActivity activity);

    void inject(com.sompom.tookitup.newui.ProductDetailActivity activity);

    void inject(SignUpFragment signUpFragment);

    void inject(ProductFormFragment fragment);

    void inject(SearchAddressFragment fragment);

    void inject(com.sompom.tookitup.newui.fragment.EditProfileFragment fragment);

    void inject(com.sompom.tookitup.newui.fragment.FilterCategoryFragment fragment);

    void inject(LikeViewerLayout likeViewerLayout);

    void inject(PopUpCommentFragment popUpCommentLayout);

    void inject(ChatFragment dialog);

    void inject(AllMessageFragment fragment);

    void inject(HomeFragment fragment);

    void inject(DetailHistoryProductConversationFragment fragment);

    void inject(SearchGeneralTypeResultFragment fragment);

    void inject(com.sompom.tookitup.newui.fragment.NotificationFragment fragment);

    void inject(ListFollowDialog fragment);

    void inject(WallStreetFragment wallStreetFragment);

    void inject(TimelineDetailFragment wallStreetFragment);

    void inject(NearbyMapFragment nearbyMapFragment);

    void inject(TabSuggestionFragment fragment);

    void inject(ReplyCommentFragment fragment);

    void inject(MyWallStreetFragment fragment);

    void inject(SearchGeneralFragment fragment);

    void inject(SuggestionFragment fragment);

    void inject(SearchResultFragment searchResultFragment);

    void inject(ForwardFragment forwardFragment);

    void inject(UserStoreBottomSheetDialog forwardFragment);

    void inject(FilterDetailFragment filterDetailFragment);

    void inject(SearchGeneralContainerResultFragment searchGeneralContainerResultFragment);

    void inject(ChangePasswordDialog dialog);

    void inject(ForgotPasswordDialog dialog);

    void inject(OtherUserProfileDialog dialog);

    void inject(UserListFragment dialog);
}
