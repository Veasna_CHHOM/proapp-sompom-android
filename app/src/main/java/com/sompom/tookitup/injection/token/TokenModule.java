package com.sompom.tookitup.injection.token;

import android.content.Context;

import com.sompom.tookitup.injection.controller.ControllerScope;
import com.sompom.tookitup.injection.controller.PublicModule;
import com.sompom.tookitup.injection.game.MoreGameModule;
import com.sompom.tookitup.injection.productserialize.ProductSerializeModule;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Rotha on 8/17/2017.
 */
@Module(includes = {ProductSerializeModule.class, MoreGameModule.class, PublicModule.class})
public class TokenModule {

    private Context mContext;

    public TokenModule(Context context) {
        mContext = context;
    }

    @Provides
    @ControllerScope
    Context context() {
        return mContext;
    }

}
