package com.sompom.tookitup.injection.controller;

import android.content.Context;

import com.google.gson.Gson;
import com.sompom.tookitup.helper.HeaderInterceptor;
import com.sompom.tookitup.helper.TokenAuthenticator;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.ServerUrl;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

/**
 * Created by He Rotha on 10/2/17.
 */
@Module
public class NetworkModule {

    @Provides
    @ControllerScope
    public String getUrl() {
        return ServerUrl.generateUrl();
    }

    @Provides
    @ControllerScope
    public Gson getGson() {
        return new Gson();
    }

    @Provides
    @ControllerScope
    public HttpLoggingInterceptor getHttpLoggingInterceptor() {
        HttpLoggingInterceptor logger = new HttpLoggingInterceptor(message -> Timber.i(message));
        logger.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logger;
    }

    @Provides
    @ControllerScope
    public HeaderInterceptor getHeaderInterceptor(Context context) {
        return new HeaderInterceptor(context);
    }

    @Provides
    @ControllerScope
    public TokenAuthenticator getAuthenticator(Context context) {
        return new TokenAuthenticator(context);
    }

    @Provides
    @ControllerScope
    public ApiService getServices(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @Provides
    @ControllerScope
    public Retrofit.Builder getRetrofitBuilder(OkHttpClient okHttpClient, Gson gson) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson));
    }

    @Provides
    @ControllerScope
    public Retrofit getServerRetrofit(Retrofit.Builder builder, String url) {
        return builder.baseUrl(url).build();
    }

    @Provides
    @ControllerScope
    public OkHttpClient getOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor,
                                        HeaderInterceptor headerInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor(headerInterceptor)
                .readTimeout(1, TimeUnit.MINUTES)
                .connectTimeout(1, TimeUnit.MINUTES)
                .build();
    }
}
