package com.sompom.tookitup.injection.controller;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Rotha on 8/17/2017.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ControllerScope {
}
