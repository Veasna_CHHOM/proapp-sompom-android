package com.sompom.tookitup.injection.mywallserialize.productserialize;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import javax.inject.Qualifier;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by He Rotha on 9/29/17.
 */

@Qualifier
@Documented
@Retention(RUNTIME)
public @interface MyWallSerializeQualifier {
}