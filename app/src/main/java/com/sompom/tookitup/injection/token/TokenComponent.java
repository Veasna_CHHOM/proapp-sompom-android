package com.sompom.tookitup.injection.token;

import com.sompom.tookitup.helper.TokenAuthenticator;
import com.sompom.tookitup.injection.controller.ControllerScope;

import dagger.Subcomponent;

/**
 * Created by Rotha on 8/19/2017.
 */
@Subcomponent(modules = {TokenModule.class})
@ControllerScope
public interface TokenComponent {
    void inject(TokenAuthenticator receiver);
}
