package com.sompom.tookitup.injection.controller;

import com.google.gson.Gson;
import com.sompom.tookitup.injection.game.MoreGameAPIQualifier;
import com.sompom.tookitup.services.ApiService;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by He Rotha on 10/2/17.
 */
@Module(includes = NetworkModule.class)
public class PublicModule {
    @Provides
    @PublicQualifier
    @ControllerScope
    public ApiService getTesteService(@PublicQualifier Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @Provides
    @PublicQualifier
    @ControllerScope
    public Retrofit.Builder getTestRetrofitBuilder(@MoreGameAPIQualifier OkHttpClient okHttpClient,
                                                   Gson gson) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson));
    }

    @Provides
    @PublicQualifier
    @ControllerScope
    public Retrofit getTestRetrofit(@PublicQualifier Retrofit.Builder builder, String url) {
        return builder.baseUrl(url).build();
    }
}
