package com.sompom.tookitup.chat.listener;

import com.sompom.tookitup.model.emun.Presence;

import java.util.Date;

/**
 * Created by He Rotha on 12/31/18.
 */
public interface OnPresenceListener {
    void onPresenceUpdate(String userId, Presence presence, Date lastOnlineTime);

    String getUserId();
}
