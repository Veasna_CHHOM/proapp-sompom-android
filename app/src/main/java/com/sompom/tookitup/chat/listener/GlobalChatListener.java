package com.sompom.tookitup.chat.listener;

import com.sompom.tookitup.chat.MessageState;
import com.sompom.tookitup.model.result.Chat;

/**
 * Created by He Rotha on 7/6/18.
 */
public interface GlobalChatListener {
    //currently, this feature used by Forward Screen only.
    //so, only my Chat & MessageState.Sent can retrieved
    void onMessageUpdated(Chat message, MessageState state);
}
