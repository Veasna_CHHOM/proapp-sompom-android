package com.sompom.tookitup.chat.listener;

import com.sompom.tookitup.model.emun.SegmentedControlItem;
import com.sompom.tookitup.model.result.Conversation;

/**
 * Created by He Rotha on 7/6/18.
 */
public interface ChannelListener {
    void onChannelCreateOrUpdate(Conversation channel);

    void onChannelRemoved(String channelId);

    SegmentedControlItem getCurrentSegmentedControlItem();

}
