package com.sompom.tookitup.chat.service;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.sompom.tookitup.broadcast.AbsService;
import com.sompom.tookitup.broadcast.onesignal.OneSignalReceiver;
import com.sompom.tookitup.chat.AbsChatBinder;
import com.sompom.tookitup.chat.MessageState;
import com.sompom.tookitup.chat.listener.ChannelListener;
import com.sompom.tookitup.chat.listener.ChatListener;
import com.sompom.tookitup.chat.listener.GlobalChatListener;
import com.sompom.tookitup.chat.listener.OnPresenceListener;
import com.sompom.tookitup.chat.listener.ServiceStateListener;
import com.sompom.tookitup.database.ConversationDb;
import com.sompom.tookitup.listener.OnBadgeUpdateListener;
import com.sompom.tookitup.listener.OnChatSocketListener;
import com.sompom.tookitup.model.emun.Presence;
import com.sompom.tookitup.model.emun.SegmentedControlItem;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.utils.ChatNotificationUtil;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

public class SocketService extends AbsService {

    private final SocketBinder mBinder = new SocketBinder();
    @Inject
    public ApiService mApiService;
    private ChatSocket mSocket;
    private TypingIndicator mTypingIndicator;

    public static Intent getReplyChatIntent(Context context,
                                            Chat chat) {
        Intent intent = new Intent(context, SocketService.class);
        intent.setAction(RemoteNotificationBroadcast.REPLY_ACTION);
        intent.putExtra(SharedPrefUtils.DATA, chat);
        return intent;
    }

    public static Intent pushNotification(Context context,
                                          Chat chat) {
        Intent intent = new Intent(context, SocketService.class);
        intent.setAction(OneSignalReceiver.ACTION_RECEIVE_NOTIFICATION);
        intent.putExtra(SharedPrefUtils.DATA, chat);
        return intent;
    }

    /**
     * @return true, mean that SocketService.class is Running, otherwise, it doesn't
     */
    public static boolean isRunning(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (manager == null) {
            return false;
        }
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (SocketService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        Timber.e("onStartCommand " + intent + " " + startId + " " + hashCode());
        if (intent != null) { //intent == null, mean app is destroyed
            if (TextUtils.equals(intent.getAction(), RemoteNotificationBroadcast.REPLY_ACTION)) {
                handleActionReply(intent);
            } else if (TextUtils.equals(intent.getAction(), OneSignalReceiver.ACTION_RECEIVE_NOTIFICATION)) {
                handleActionNotificatuon(intent);
            }
        }
        return START_STICKY;
    }

    private void handleActionNotificatuon(Intent intent) {
        final Chat chat = intent.getParcelableExtra(SharedPrefUtils.DATA);
        if (mSocket == null) {
            mBinder.startService(null);
        }
        if (mSocket != null) {
            mSocket.receiveChat(chat);
        }
    }

    private void handleActionReply(Intent intent) {
        final Chat chat = intent.getParcelableExtra(SharedPrefUtils.DATA);
        if (mSocket == null) {
            mBinder.startService(null);
        }
        if (mSocket != null) {
            mSocket.sendReply(chat);
        }
        ChatNotificationUtil.cancelNotification(getApplicationContext(), chat.getChannelId());
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class SocketBinder extends AbsChatBinder {
        final HashMap<String, Presence> mPresence = new HashMap<>();
        final HashMap<String, Date> mLastActivity = new HashMap<>();

        @Override
        public void startService(ServiceStateListener listener) {
            if (!SharedPrefUtils.isLogin(getApplicationContext())) {
                return;
            }
            if (mSocket != null) {
                if (mSocket.isconnected()) {
                    return;
                } else {
                    mSocket.removeAllCallbacks();
                    mSocket.disconnect();
                }
            }
            if (mApiService == null) {
                getControllerComponent().inject(SocketService.this);
            }
            mSocket = new ChatSocket(getApplicationContext(), listener, mApiService);
            mSocket.disableLogging();
            mSocket.connectAsync();
            mSocket.startListener(new SocketListener());
            mPresence.clear();
            mLastActivity.clear();
        }

        @Override
        public void stopService() {
            if (mSocket != null) {
                mSocket.disconnect();
            }
            mPresence.clear();
            mLastActivity.clear();
            getChannelListener().clear();
            getChatListener().clear();
            getOnPresenceListeners().clear();
            getGlobalChatListeners().clear();
        }

        @Override
        public void sendMessage(User user, Chat message) {
            if (mSocket != null) {
                boolean canSend = mSocket.sendTo(message, true);
                if (canSend) {
                    final Conversation con = ConversationDb.saveConversationFromChat(getApplicationContext(), message, true, user);
                    onChannelCreateOrUpdate(con);
                }
            }
        }

        @Override
        public void sendMessage(List<Chat> messages) {
            if (mSocket != null) {
                for (Chat message : messages) {
                    boolean canSend = mSocket.sendTo(message, true);
                    if (canSend) {
                        final Conversation con = ConversationDb.saveConversationFromChat(getApplicationContext(),
                                message,
                                true,
                                message.getRecipient());
                        onChannelCreateOrUpdate(con);
                    }
                }
            }
        }

        private void onChannelCreateOrUpdate(@Nullable Conversation conversation) {
            if (conversation == null) {
                return;
            }
            SegmentedControlItem item;
            if (conversation.getProduct() != null && conversation.getProduct().getUser() != null) {
                final User seller = conversation.getProduct().getUser();
                if (TextUtils.equals(seller.getId(), SharedPrefUtils.getUserId(SocketService.this))) {
                    item = SegmentedControlItem.Selling;
                } else {
                    item = SegmentedControlItem.Buying;
                }
            } else {
                item = SegmentedControlItem.Message;
            }

            for (ChannelListener channelListener : getChannelListener()) {
                if (channelListener.getCurrentSegmentedControlItem() == SegmentedControlItem.All ||
                        channelListener.getCurrentSegmentedControlItem() == item) {
                    channelListener.onChannelCreateOrUpdate(conversation);
                }
            }
        }

        @Override
        public void startTyping(String recipientUserId, String productId, String channelID) {
            if (mTypingIndicator == null) {
                mTypingIndicator = new TypingIndicator((sendTo,
                                                        isGroup,
                                                        productId1,
                                                        channelId,
                                                        isTyping) -> {
                    if (mSocket == null) {
                        return;
                    }

                    //One to one conversation
                    mSocket.startTyping(sendTo.get(0), productId1, channelId, isTyping);
                });
            }
            mTypingIndicator.startTyping(recipientUserId, productId, channelID);
        }

        @Override
        public void startTyping(List<String> recipientUserId, String channelID) {
            if (mTypingIndicator == null) {
                mTypingIndicator = new TypingIndicator((sendTo,
                                                        isGroup,
                                                        productId1,
                                                        channelId,
                                                        isTyping) -> {
                    if (mSocket == null) {
                        return;
                    }

                    //Group conversion
                    for (String recipient : sendTo) {
                        Timber.i("recipient: " + recipient);
                        mSocket.startTyping(recipient, productId1, channelId, isTyping);
                    }
                });
            }
            mTypingIndicator.startTyping(recipientUserId, channelID);
        }

        @Override
        public void stopTyping(String recipientUserId, String productId, String channelID) {
            if (mTypingIndicator != null) {
                mTypingIndicator.stopTyping();
            }
        }

        @Override
        public void removeMessage(Chat chat) {
            if (mSocket != null) {
                mSocket.deleteChat(chat);
            }
        }

        @Override
        public void onResume(String conversationId) {
            Timber.e("onResume " + conversationId);
            if (mSocket != null) {
                mSocket.updateSeenStatus(conversationId);
            }
        }

        @Override
        public void archivedConversation(String conversationId) {
            if (mSocket != null) {
                mSocket.archivedConversation(conversationId);
            }
        }

        @Override
        public void addPresenceListener(OnPresenceListener listener) {
            super.addPresenceListener(listener);

            final String id = listener.getUserId();
            Presence presence = mPresence.get(id);
            Date activity = mLastActivity.get(id);
            listener.onPresenceUpdate(id, presence, activity);
        }

        public void setUserStatus(String id,
                                  Boolean isOnline,
                                  Date lastDateActivity) {

            Presence currentPresence = mPresence.get(id);

            Presence newPresence;
            if (isOnline != null) {
                if (isOnline) {
                    newPresence = Presence.Online;
                } else {
                    newPresence = Presence.Offline;
                }
            } else {
                newPresence = currentPresence;
            }

            lastDateActivity = updateCurrentPresence(id, lastDateActivity, newPresence);

            if (currentPresence == null || currentPresence != newPresence) {
                for (OnPresenceListener onPresenceListener : getOnPresenceListeners()) {
                    if (TextUtils.equals(onPresenceListener.getUserId(), id)) {
                        onPresenceListener.onPresenceUpdate(id, newPresence, lastDateActivity);
                    }
                }
            }
        }

        private Date updateCurrentPresence(String id, Date lastDateActivity, Presence newPresence) {
            if (newPresence != null) {
                mPresence.put(id, newPresence);
            }
            if (lastDateActivity == null) {
                lastDateActivity = mLastActivity.get(id);
            } else {
                mLastActivity.put(id, lastDateActivity);
            }
            return lastDateActivity;
        }

        private class SocketListener implements OnChatSocketListener {
            @Override
            public void onMessageReceive(Chat chat) {
                for (ChatListener chatListener : getChatListener()) {
                    if (TextUtils.equals(chatListener.getChannelId(), chat.getChannelId())) {
                        chatListener.onReceiveMessage(chat);
                    }
                }

            }

            @Override
            public void onMessageTyping(Chat chat) {
                for (ChatListener chatListener : getChatListener()) {
                    if (TextUtils.equals(chatListener.getChannelId(), chat.getChannelId())) {
                        chatListener.onUserTyping(chat);
                    }
                }

            }

            @Override
            public void onConversationCreateOrUpdate(Conversation conversation) {
                SocketBinder.this.onChannelCreateOrUpdate(conversation);
            }

            @Override
            public void onConversationDelete(String conversationId) {
                for (ChannelListener channelListener : getChannelListener()) {
                    channelListener.onChannelRemoved(conversationId);
                }
            }

            @Override
            public void onPresenceUpdate(String userId, Presence presence, long lastActiveTime) {
                //currently, user presence doesn't handle on Chat Socket,
                //it is handled on API on class User#isOnline
            }

            @Override
            public void onMessageSent(Chat chat) {
                for (GlobalChatListener globalChatListener : mBinder.getGlobalChatListeners()) {
                    globalChatListener.onMessageUpdated(chat, MessageState.SENT);
                }
            }

            @Override
            public boolean isInBg(String channelId) {
                boolean isInBg = true;
                for (ChatListener chatListener : getChatListener()) {
                    isInBg = chatListener.isInBackground();
                    if (!isInBg && !TextUtils.equals(channelId, chatListener.getChannelId())) {
                        isInBg = true;
                    }
                }
                return isInBg;
            }

            @Override
            public List<String> getCurrentChannel() {
                final List<String> channels = new ArrayList<>();
                for (ChatListener chatListener : getChatListener()) {
                    channels.add(chatListener.getChannelId());
                }
                return channels;
            }

            @Override
            public void onBadgeUpdate(int value) {
                for (OnBadgeUpdateListener badgeListener : getBadgeListeners()) {
                    badgeListener.onConversationBadgeUpdate(value);
                }
            }
        }

    }


}
