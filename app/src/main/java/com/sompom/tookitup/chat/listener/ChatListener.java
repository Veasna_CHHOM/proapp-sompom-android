package com.sompom.tookitup.chat.listener;

import com.sompom.tookitup.model.result.Chat;

/**
 * Created by He Rotha on 7/6/18.
 */
public interface ChatListener {
    void onReceiveMessage(Chat message);

    void onUserTyping(Chat message);

    boolean isInBackground();

    String getChannelId();
}
