package com.sompom.tookitup.chat.call;

import com.sinch.android.rtc.ClientRegistration;
import com.sinch.android.rtc.SinchClient;
import com.sinch.android.rtc.SinchClientListener;
import com.sinch.android.rtc.SinchError;

import timber.log.Timber;

public class MySinchListner implements SinchClientListener {
    @Override
    public void onClientStarted(SinchClient sinchClient) {
        Timber.e("onClientStarted ");

    }

    @Override
    public void onClientStopped(SinchClient sinchClient) {
        Timber.e("onClientStopped ");
    }

    @Override
    public void onClientFailed(SinchClient sinchClient, SinchError sinchError) {
        Timber.e("onClientFailed " + sinchError.getMessage());
    }

    @Override
    public void onRegistrationCredentialsRequired(SinchClient sinchClient, ClientRegistration clientRegistration) {
        Timber.e("onRegistrationCredentialsRequired ");

    }

    @Override
    public void onLogMessage(int i, String s, String s1) {
    }
}
