package com.sompom.tookitup.chat;

import android.os.Binder;

import com.sompom.tookitup.chat.listener.ChannelListener;
import com.sompom.tookitup.chat.listener.ChatListener;
import com.sompom.tookitup.chat.listener.GlobalChatListener;
import com.sompom.tookitup.chat.listener.OnPresenceListener;
import com.sompom.tookitup.chat.listener.ServiceStateListener;
import com.sompom.tookitup.listener.OnBadgeUpdateListener;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.model.result.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by He Rotha on 7/6/18.
 */
public abstract class AbsChatBinder extends Binder {
    private final List<ChannelListener> mChannelListener = new ArrayList<>();
    private final List<ChatListener> mChatListener = new ArrayList<>();
    private final List<OnPresenceListener> mOnPresenceListeners = new ArrayList<>();
    private final List<GlobalChatListener> mGlobalChatListeners = new ArrayList<>();
    private final List<OnBadgeUpdateListener> mBadgeListeners = new ArrayList<>();

    public List<ChannelListener> getChannelListener() {
        return mChannelListener;
    }

    public List<ChatListener> getChatListener() {
        return mChatListener;
    }

    protected List<OnPresenceListener> getOnPresenceListeners() {
        return mOnPresenceListeners;
    }

    public List<GlobalChatListener> getGlobalChatListeners() {
        return mGlobalChatListeners;
    }

    public List<OnBadgeUpdateListener> getBadgeListeners() {
        return mBadgeListeners;
    }

    public abstract void startService(ServiceStateListener listener);

    public abstract void stopService();

    public abstract void sendMessage(User user, Chat message);

    public abstract void sendMessage(List<Chat> messages);

    public abstract void startTyping(String recipientUserId, String productId, String channelID);

    public abstract void startTyping(List<String> recipientUserId, String channelID);

    public abstract void stopTyping(String recipientUserId, String productId, String channelID);

    public abstract void removeMessage(Chat chat);

    public abstract void onResume(String conversationId);

    public abstract void archivedConversation(String conversationId);

    public void addChannelListener(ChannelListener listener) {
        if (!mChannelListener.contains(listener)) {
            mChannelListener.add(listener);
        }
    }

    public void removeChannelListener(ChannelListener listener) {
        mChannelListener.remove(listener);
    }

    public void addChatListener(ChatListener listener) {
        if (!mChatListener.contains(listener)) {
            mChatListener.add(listener);
        }
    }

    public void removeChatListener(ChatListener listener) {
        mChatListener.remove(listener);
    }

    public void removePresenceListener(OnPresenceListener listener) {
        mOnPresenceListeners.remove(listener);
    }

    public void addPresenceListener(OnPresenceListener listener) {
        if (!mOnPresenceListeners.contains(listener)) {
            mOnPresenceListeners.add(listener);
        }
    }

    public void addGlobalChatListener(GlobalChatListener listener) {
        if (!mGlobalChatListeners.contains(listener)) {
            mGlobalChatListeners.add(listener);
        }
    }

    public void removeGlobalChatListener(GlobalChatListener listener) {
        mGlobalChatListeners.remove(listener);
    }

    public void addBadgeListener(OnBadgeUpdateListener listener) {
        if (!mBadgeListeners.contains(listener)) {
            mBadgeListeners.add(listener);
        }
    }

    public void removeBadgeListener(OnBadgeUpdateListener listener) {
        mBadgeListeners.remove(listener);
    }


}
