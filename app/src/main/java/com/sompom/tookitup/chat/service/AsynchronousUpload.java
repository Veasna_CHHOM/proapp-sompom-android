package com.sompom.tookitup.chat.service;

import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;

import com.sompom.tookitup.database.MessageDb;
import com.sompom.tookitup.helper.upload.AbsChatUploader;
import com.sompom.tookitup.helper.upload.FileType;
import com.sompom.tookitup.helper.upload.UploadListener;
import com.sompom.tookitup.listener.OnCompleteListener;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.result.Chat;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by He Rotha on 11/15/18.
 */
class AsynchronousUpload {
    private static final int MAX_UPLOAD_COUNTER = 4;
    private final Context mContext;
    private final Chat mChat;

    private int mIntervalUpload = 1;
    private int mMediaSize;
    private int mStartFromIndex;

    AsynchronousUpload(Context context, Chat chat) {
        mContext = context;
        mChat = chat;

        int mediaSize = mChat.getMediaList().size();
        if (mediaSize > MAX_UPLOAD_COUNTER) {
            mMediaSize = MAX_UPLOAD_COUNTER;
            mIntervalUpload = getLoopCounter(mediaSize);
        } else {
            mMediaSize = mediaSize;
        }
    }

    void startUpload(OnCompleteListener<Chat> chatOnCompleteListener) {
        List<Pair<String, Integer>> uploadUrl = new ArrayList<>();
        for (int i = 0; i < mMediaSize; i++) {
            Media productMedia = mChat.getMediaList().get(mStartFromIndex);

            if (!productMedia.getUrl().startsWith("http")) {
                FileType fileType;
                if (mChat.getType() == Chat.Type.IMAGE) {
                    fileType = FileType.IMAGE;
                } else if (mChat.getType() == Chat.Type.AUDIO) {
                    fileType = FileType.Audio;
                } else {
                    break;
                }
                AbsChatUploader uploader = new AbsChatUploader(mContext, fileType, productMedia.getUrl());
                uploader.setListener(new UploadListener() {
                    @Override
                    public void onUploadSucceeded(String requestId, String imageUrl, String thumb) {
                        Timber.e("onUploadSucceeded: %s of %s", requestId, imageUrl);
                        onUploadSuccess(uploadUrl, requestId, imageUrl, chatOnCompleteListener);
                    }

                    @Override
                    public void onUploadFail(String id, Exception ex) {
                        Timber.e("onUploadFail: %s", id);
                    }

                    @Override
                    public void onUploadProgressing(String id, int percent) {
                        Timber.e("onUploadProgressing: %s of %d", id, percent);

                    }

                    @Override
                    public void onCancel(String id) {
                        Timber.e("onCancel: %s", id);
                    }
                });
                String request = uploader.doUpload();
                Pair<String, Integer> pair = new Pair<>(request, mStartFromIndex);
                uploadUrl.add(pair);
            } else {
                Timber.e("loop continue");
            }

            mStartFromIndex += 1;
        }
    }

    private void onUploadSuccess(List<Pair<String, Integer>> uploadUrl,
                                 String requestId,
                                 String imageUrl,
                                 OnCompleteListener<Chat> chatOnCompleteListener) {
        for (int i = uploadUrl.size() - 1; i >= 0; i--) {
            if (TextUtils.equals(uploadUrl.get(i).first, requestId)) {
                Integer index = uploadUrl.get(i).second;
                if (index != null) {
                    Media media = mChat.getMediaList().get(index);
                    media.setUrl(imageUrl);
                    uploadUrl.remove(i);
                    MessageDb.save(mContext, mChat);
                }
            }
        }

        Timber.e("uploadUrl size: %s", uploadUrl.size());
        if (uploadUrl.isEmpty()) {
            mIntervalUpload -= 1;
            Timber.e("mMediaSize: %s, mIntervalUpload: %s", mMediaSize, mIntervalUpload);
            if (mIntervalUpload >= 1) {
                int originalSize = mChat.getMediaList().size();
                int loopCounter = getLoopCounter(originalSize) - mIntervalUpload;
                mMediaSize = getMediaSize(originalSize, (mMediaSize * loopCounter));
                startUpload(chatOnCompleteListener);
            } else {
                chatOnCompleteListener.onComplete(mChat);
            }
        }
    }

    private int getLoopCounter(int mediaSize) {
        int number = mediaSize / MAX_UPLOAD_COUNTER;
        if (mediaSize % MAX_UPLOAD_COUNTER > 0) {
            number += 1;
        }
        Timber.e("number: %s", number);
        return number;
    }

    private int getMediaSize(int originalSize, int mediaSize) {
        int result = originalSize - mediaSize;
        Timber.e("getMediaSize result: %s", result);
        if (result > MAX_UPLOAD_COUNTER) {
            return MAX_UPLOAD_COUNTER;
        } else {
            return result;
        }
    }
}
