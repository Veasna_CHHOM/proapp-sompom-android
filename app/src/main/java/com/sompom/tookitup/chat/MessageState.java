package com.sompom.tookitup.chat;

import android.text.TextUtils;

import com.sompom.tookitup.R;

/**
 * Created by He Rotha on 7/6/18.
 */
public enum MessageState {
    SENDING("sending", R.drawable.ic_chat_wall_clock, R.string.status_sending),
    FAIL("fail", R.drawable.ic_chat_retry, R.string.status_failed),
    SENT("sent", R.drawable.ic_chat_single_tick, R.string.status_sent),
    //    DELIVERED("delivered", R.drawable.ic_chat_double_tick, R.string.status_delivered),
    SEEN("seen", R.drawable.ic_chat_double_tick, R.string.status_seen);

    private String mStatus;
    private int mIcon;
    private int mTextStatus;

    MessageState(String status, int icon, int textStatus) {
        mStatus = status;
        mIcon = icon;
        mTextStatus = textStatus;
    }

    public static MessageState fromValue(String value) {
        if (TextUtils.isEmpty(value)) {
            return SENT;
        }
        for (MessageState s : MessageState.values()) {
            if (s.getValue().equalsIgnoreCase(value)) {
                return s;
            }
        }
        return SENT;
    }

    public String getStatus() {
        return mStatus;
    }

    public int getIcon() {
        return mIcon;
    }

    public int getTextStatus() {
        return mTextStatus;
    }

    public String getValue() {
        return mStatus;
    }
}
