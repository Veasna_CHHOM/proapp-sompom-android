package com.sompom.tookitup.chat.service;

import android.app.RemoteInput;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.utils.ChatNotificationUtil;
import com.sompom.tookitup.utils.SharedPrefUtils;

import timber.log.Timber;

/**
 * Created by He Rotha on 12/6/18.
 */
public class RemoteNotificationBroadcast extends BroadcastReceiver {
    public static final String REPLY_ACTION = "REPLY_ACTION";

    public static Intent getReplyMessageIntent(Context context, int notificationId, Chat chat) {
        Intent intent = new Intent(context, RemoteNotificationBroadcast.class);
        intent.setAction(REPLY_ACTION);
        intent.putExtra(SharedPrefUtils.ID, notificationId);
        intent.putExtra(SharedPrefUtils.DATA, chat);
        return intent;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (REPLY_ACTION.equals(intent.getAction())) {
            handleActionReply(context, intent);
        }
    }

    private void handleActionReply(Context context, Intent intent) {
        Bundle bundle = getBundle(intent);
        if (bundle != null) {

            CharSequence message = bundle.getCharSequence(ChatNotificationUtil.KEY_TEXT_REPLY);
            if (!TextUtils.isEmpty(message)) {
                Chat chat = intent.getParcelableExtra(SharedPrefUtils.DATA);
                int notifyId = intent.getIntExtra(SharedPrefUtils.ID, 0);
                Timber.e("chat: %s", new Gson().toJson(chat));
                Timber.e("notifyId: %s", notifyId);

                Chat newChat = Chat.getNewChat(context, chat.getSender(), chat.getProduct());
                newChat.setChannelId(chat.getChannelId());
                newChat.setSendTo(chat.getSenderId());
                newChat.setContent(message.toString());
                context.startService(SocketService.getReplyChatIntent(context, newChat));
            }
        }
    }

    private Bundle getBundle(Intent intent) {
        Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);
        if (remoteInput != null) {
            return remoteInput;
        }
        return null;
    }
}
