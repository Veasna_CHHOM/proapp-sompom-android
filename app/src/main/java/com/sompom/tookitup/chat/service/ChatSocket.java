package com.sompom.tookitup.chat.service;


import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.GSonGMTDateAdapter;
import com.sompom.tookitup.chat.MessageState;
import com.sompom.tookitup.chat.listener.ServiceStateListener;
import com.sompom.tookitup.database.ConversationDb;
import com.sompom.tookitup.database.MessageDb;
import com.sompom.tookitup.database.MessageJunkDb;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnChatSocketListener;
import com.sompom.tookitup.listener.OnMessageDbListener;
import com.sompom.tookitup.listener.SocketListener;
import com.sompom.tookitup.model.ChatJunk;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.LoadMoreWrapper;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.utils.ChatNotificationUtil;
import com.sompom.tookitup.utils.SharedPrefUtils;

import org.json.JSONObject;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import io.github.sac.Ack;
import io.github.sac.ReconnectStrategy;
import io.github.sac.Socket;
import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by He Rotha on 10/26/18.
 */
public class ChatSocket extends Socket {
    private static final String URL = "ws://209.97.161.132:7581/socketcluster/";
    private final Context mContext;
    private final ApiService mApiService;
    private final LinkedList<Chat> mChatUploadLinkedList = new LinkedList<>();
    private OnChatSocketListener mOnChatSocketListener;
    private final OnMessageDbListener mOnMessageDbListener = new OnMessageDbListener() {
        @Override
        public void onConversationDelete(String conversationId) {
            if (mOnChatSocketListener != null) {
                mOnChatSocketListener.onConversationDelete(conversationId);
            }
        }

        @Override
        public void onConversationUpdate(Conversation conversation) {
            if (mOnChatSocketListener != null) {
                mOnChatSocketListener.onConversationCreateOrUpdate(conversation);
            }
        }
    };
    private String mMyUserId;
    private Gson mGson;

    ChatSocket(Context context, ServiceStateListener listener, ApiService apiService) {
        super(URL);
        mContext = context;
        mApiService = apiService;

        setReconnection(new ReconnectStrategy());
        setListener(new SocketListener() {

            @Override
            public void onConnected() {
                Timber.e("onConnected");
                try {
                    if (listener != null) {
                        listener.onServiceStart();
                    }

                    //clear all queue message, cos when network reconnected,
                    //unsent message will be resent, and start save queue again
                    mChatUploadLinkedList.clear();

                    //load new message from current open chat screen
                    loadNewMessageOfCurrentScreen(context);

                    //send remaining Sending Message Status
                    List<Chat> sending = MessageDb.queryByStatusAndUserId(mContext, MessageState.SENDING, mMyUserId);
                    Timber.e("sending remaining %s", sending.size());

                    for (Chat chat : sending) {
                        sendTo(chat, false);
                    }

                    //send remaining status of message
                    sendRemainingStatus(context);
                } catch (Exception ex) {
                    Timber.e("ex: %s", ex.toString());
                }
            }

            @Override
            public void onConnectError(Exception ex) {
                if (listener != null) {
                    listener.onServiceFail(ex);
                }
            }
        });
    }

    /**
     * @return true, mean chat is existed, otherwise, it doesn't
     */
    public static boolean isExistingChat(Context context, Chat chat, ApiService apiService) {
        Chat localChat = MessageDb.queryById(context, chat.getId());
        if (localChat != null && (localChat.getStatus() == chat.getStatus() || localChat.getStatus() == MessageState.SEEN)) {
            return true;
        }

        //download message data from server if db is empty
        if (MessageDb.getMessageCount(context, chat.getChannelId()) <= 0) {
            try {
                int numOfRequest = context.getResources().getInteger(R.integer.number_request_item);
                Observable<Response<LoadMoreWrapper<Chat>>> ob = apiService.getChatList(chat.getChannelId(), "0", numOfRequest);
                ResponseObserverHelper<Response<LoadMoreWrapper<Chat>>> call = new ResponseObserverHelper<>(context, ob);
                call.execute(result -> {
                    if (result.body() == null) {
                        return;
                    }
                    MessageDb.save(context, result.body().getData());
                });
            } catch (Exception ex) {
                Timber.e(ex.toString());
            }
        }
        return false;
    }

    private void sendRemainingStatus(Context context) {
        List<ChatJunk> junks = MessageJunkDb.queryAll(context);
        for (ChatJunk junk : junks) {
            if (!junk.isGroup()) {
                Timber.i("sendRemainingStatus one to one.");
                //One to one chat
                Chat newChat = new Chat();
                newChat.setId(junk.getMessageId());
                newChat.setSendingType(junk.getSendingType());
                newChat.setSendTo(junk.getSendTo());
                newChat.setChannelId(junk.getConversationId());
                newChat.setSenderId(mMyUserId);

                Channel channel = createChannel(newChat.getSendTo());
                channel.subscribe();

                Timber.e("send junk %s", new Gson().toJson(newChat));
                try {
                    final JSONObject jsonObject = new JSONObject(getGson().toJson(newChat));
                    getChannelByName(newChat.getSendTo()).publish(jsonObject);
                } catch (Exception e) {
                    Timber.e("error %s", e.toString());
                }
            } else {
                Timber.i("sendRemainingStatus to group.");
                //Group chat, so need to broadcast to all participants
                Conversation conversationById = ConversationDb.getConversationById(mContext, junk.getConversationId());
                if (conversationById != null) {
                    for (User participant : conversationById.getParticipants()) {

                        Chat newChat = new Chat();
                        newChat.setId(junk.getMessageId());
                        newChat.setSendingType(junk.getSendingType());
                        newChat.setSendTo(participant.getId());
                        newChat.setChannelId(junk.getConversationId());
                        newChat.setSenderId(mMyUserId);

                        Channel channel = createChannel(newChat.getSendTo());
                        channel.subscribe();

                        Timber.e("send junk %s", new Gson().toJson(newChat));
                        try {
                            final JSONObject jsonObject = new JSONObject(getGson().toJson(newChat));
                            getChannelByName(newChat.getSendTo()).publish(jsonObject);
                        } catch (Exception e) {
                            Timber.e("error %s", e.toString());
                        }
                    }
                }
            }
        }

        MessageJunkDb.delete(context);
    }

    private void loadNewMessageOfCurrentScreen(Context context) {
        List<String> channels = mOnChatSocketListener.getCurrentChannel();
        for (String channel : channels) {
            final Chat chat = MessageDb.getLastUnseenMessage(context, channel);
            if (chat == null) {
                continue;
            }
            //download message data from server if db is empty
            Observable<Response<List<Chat>>> ob = mApiService.getLatestChatList(chat.getChannelId(), chat.getId());
            ResponseObserverHelper<Response<List<Chat>>> call = new ResponseObserverHelper<>(mContext, ob);
            call.execute(new LoadNewMessageCallback(chat));
        }
    }

    void startListener(OnChatSocketListener onChatSocketListener) {
        mOnChatSocketListener = onChatSocketListener;
        mMyUserId = SharedPrefUtils.getUserId(mContext);
        Timber.e("listener my channel %s", mMyUserId);
        if (TextUtils.isEmpty(mMyUserId)) {
            return;
        }
        createChannel(mMyUserId).subscribe();
        onSubscribe(mMyUserId, (name, data) -> {
            Timber.e("name: " + name + ", data:" + data.toString());
            Chat chat = getGson().fromJson(data.toString(), Chat.class);

            Chat.SendingType sendingType = chat.getSendingType();
            if (sendingType == Chat.SendingType.TYPING) {
                mOnChatSocketListener.onMessageTyping(chat);
            } else if (sendingType == Chat.SendingType.READ_STATUS) {
                List<Chat> chats = MessageDb.queryUnreadMessageFromId(mContext,
                        chat.getId(),
                        chat.getChannelId(),
                        isGrouConverstion(chat.getChannelId()));
                if (chats == null || chats.isEmpty()) {
                    return;
                }
                for (Chat chat1 : chats) {
                    Timber.i("Clone sender and send to id from socket object.");
                    //Assign sender and sent to id from socket object
                    chat1.setSenderId(chat.getSenderId());
                    chat1.setSendTo(chat.getSendTo());
                    mOnChatSocketListener.onMessageReceive(chat1);
                }
            } else if (sendingType == Chat.SendingType.DELETE) {
                Chat newChat = MessageDb.queryById(mContext, chat.getId());
                MessageDb.delete(mContext, newChat, mOnMessageDbListener);
            } else if (sendingType == Chat.SendingType.NONE) {
                receiveChat(chat);
            }
        });
    }

    private boolean isGrouConverstion(String channelId) {
        Conversation conversationById = ConversationDb.getConversationById(mContext, channelId);
        return conversationById != null && !TextUtils.isEmpty(conversationById.getGroupId());
    }

    void receiveChat(Chat chat) {
        if (isExistingChat(mContext, chat, mApiService)) {
            return;
        }

        boolean isInBg = mOnChatSocketListener.isInBg(chat.getChannelId());

        //if app in background, just update badge, save, push notification...
        if (isInBg) {
            //update badge
            if (ConversationDb.isRead(mContext, chat)) {
                mOnChatSocketListener.onBadgeUpdate(1);
            }

            //save chat in db
            MessageDb.save(mContext, chat);

            //update conversation
            ConversationDb.saveConversationFromChat(mContext, chat, false, null);

            //push notification
            ChatNotificationUtil.pushChatNotification(mContext, chat);
        } else {
            ConversationDb.saveConversationFromChat(mContext, chat, true, null);
            if (!TextUtils.equals(chat.getSenderId(), mMyUserId)) {
                chat.setStatus(MessageState.SEEN);
            }

            Conversation conversationById = ConversationDb.getConversationById(mContext, chat.getChannelId());
            Timber.i("conversationById: " + new Gson().toJson(conversationById));
            boolean isGroupConversation = false;
            if (isGroupConversation(conversationById)) {
                //Broadcast seen status to all participants
                isGroupConversation = true;
                Timber.i("updateSeenStatusForGroup");
                for (User participant : conversationById.getParticipants()) {
                    sendReadStatus(chat, participant.getId());
                }
            } else {
                //Update message status for individual chat
                Timber.i("updateSeenStatus for individual.");
                sendReadStatus(chat, chat.getSenderId());
            }

            List<Chat> chats = MessageDb.queryUnreadMessageFromId(mContext,
                    chat.getId(),
                    chat.getChannelId(),
                    isGroupConversation);
            if (chats != null && !chats.isEmpty()) {
                for (Chat chat1 : chats) {
                    mOnChatSocketListener.onMessageReceive(chat1);
                }
            }
            MessageDb.save(mContext, chat);
            mOnChatSocketListener.onMessageReceive(chat);
        }

        mOnChatSocketListener.onConversationCreateOrUpdate(ConversationDb.getConversationById(mContext,
                chat.getChannelId()));
    }

    private void sendReadStatus(Chat chat, String sendTo) {
        Chat newChat = new Chat();
        newChat.setId(chat.getId());
        newChat.setSendingType(Chat.SendingType.READ_STATUS);
        newChat.setSendTo(sendTo);
        newChat.setChannelId(chat.getChannelId());
        newChat.setSenderId(mMyUserId);
        newChat.setGroup(chat.isGroup());

        Channel channel = createChannel(newChat.getSendTo());
        channel.subscribe();

        try {
            String json = getGson().toJson(newChat);
            Timber.e("update status %s", json);
            final JSONObject jsonObject = new JSONObject(json);
            getChannelByName(newChat.getSendTo()).publish(jsonObject);
        } catch (Exception e) {
            Timber.e("receiveChat %s", isconnected());
        }
    }

    private boolean isGroupConversation(Conversation conversation) {
        return conversation != null && !TextUtils.isEmpty(conversation.getGroupId());
    }

    void sendReply(Chat chat) {
        if (TextUtils.equals(chat.getSendTo(), mMyUserId)) {
            //user cannot send message to himself
            return;
        }
        MessageDb.save(mContext, chat);

        Chat newChat = new Chat(chat);
        newChat.setStatus(MessageState.SENT);

        Timber.e("sendReply %s", getGson().toJson(newChat));
        createChannel(newChat.getSendTo()).subscribe();
        try {
            getChannelByName(newChat.getSendTo()).publish(new JSONObject(getGson().toJson(newChat)), (name, error, data) -> {
                Timber.e("callback: " + name + ", error: " + error + ", data:" + data);
                Chat ackChat = getGson().fromJson(data.toString(), Chat.class);


                if (error == null) {
                    ackChat.setStatus(MessageState.SENT);
                } else {
                    ackChat.setStatus(MessageState.FAIL);
                }

                MessageDb.save(mContext, ackChat);
                ConversationDb.saveConversationFromChat(mContext, ackChat, false, null);

                if (mOnChatSocketListener != null) {
                    mOnChatSocketListener.onMessageReceive(ackChat);
                }

                updateSeenStatus(ackChat.getChannelId());
            });
        } catch (Exception e) {
            Timber.e("sendReply %s", isconnected());
        }
    }

    boolean sendTo(Chat chat, boolean isNeedToSaveDb) {
        Timber.i("Send chat => " + new Gson().toJson(chat));
        if (TextUtils.equals(chat.getSendTo(), mMyUserId)) {
            //user cannot send message to himself
            return false;
        }
        Chat newChat = new Chat(chat);
        if (isNeedToSaveDb) {
            MessageDb.save(mContext, newChat);
        }
        if (!isconnected()) {
            //even chat is disconnected, but message is saved
            return true;
        }
        if (newChat.getMediaList() != null
                && !newChat.getMediaList().isEmpty()
                && isContainerLocalMedia(newChat.getMediaList())) {
            mChatUploadLinkedList.add(newChat);
            if (mChatUploadLinkedList.size() <= 1) {
                AsynchronousUpload asynchronousUpload = new AsynchronousUpload(mContext, newChat);
                asynchronousUpload.startUpload(result -> sendTo(result, true));
            }
        } else {
            if (newChat.getMediaList() != null &&
                    !newChat.getMediaList().isEmpty() &&
                    !mChatUploadLinkedList.isEmpty()) {
                mChatUploadLinkedList.removeFirst();
            }

            newChat.setStatus(MessageState.SENT);
            newChat.setSendTo(newChat.getSendTo());

            Channel channel = createChannel(newChat.getSendTo());
            channel.subscribe();

            Timber.e(getGson().toJson(newChat));
            try {
                final JSONObject jsonObject = new JSONObject(getGson().toJson(newChat));
                getChannelByName(newChat.getSendTo()).publish(jsonObject, new SendMessageCallback());
            } catch (Exception e) {
                Timber.e("is connected %s", isconnected());
            }
            if (newChat.getMediaList() != null &&
                    !newChat.getMediaList().isEmpty() &&
                    !mChatUploadLinkedList.isEmpty()) {
                sendTo(mChatUploadLinkedList.getFirst(), false);
            }
        }
        return true;
    }

    private boolean isContainerLocalMedia(List<Media> mediaList) {
        for (Media productMedia : mediaList) {
            if (!productMedia.getUrl().startsWith("http")) {
                return true;
            }
        }
        return false;
    }

    void startTyping(String sendTo,
                     @Nullable String productId,
                     String channelId,
                     boolean isTyping) {
        if (TextUtils.isEmpty(sendTo) || TextUtils.equals(sendTo, mMyUserId)) {
            return;
        }

        Channel channel = createChannel(sendTo);
        channel.subscribe();

        Chat chat = new Chat();
        chat.setSenderId(mMyUserId);
        chat.setProductId(productId);
        chat.setChannelId(channelId);
        chat.setTyping(isTyping);
        chat.setSendingType(Chat.SendingType.TYPING);
        chat.setGroup(chat.isGroup());

        try {
            Timber.e("sendTo %s", sendTo);
            getChannelByName(sendTo).publish(new JSONObject(getGson().toJson(chat)));
        } catch (Exception e) {
            Timber.e("isConnect %s", isconnected());
        }
    }

    void updateSeenStatus(String conversationId) {
        //remove current display notification of that conversationId
        ChatNotificationUtil.cancelNotification(mContext, conversationId);
        Chat chat = MessageDb.getLatestRecipientMessage(mContext, conversationId);
        if (chat != null && chat.getStatus() != MessageState.SEEN) {
            Conversation conversationById = ConversationDb.getConversationById(mContext, conversationId);
            Timber.i("conversationById: " + new Gson().toJson(conversationById));
            if (isconnected()) {
                if (isGroupConversation(conversationById)) {
                    Timber.i("updateSeenStatusForGroup");
                    chat.setGroup(true);
                    for (User participant : conversationById.getParticipants()) {
                        sendReadStatus(chat, participant.getId());
                    }
                } else {
                    Timber.i("updateSeenStatusForOneToOneChat");
                    sendReadStatus(chat, chat.getSenderId());
                }

                mOnChatSocketListener.onBadgeUpdate(-1);
            } else {
                MessageJunkDb.save(mContext, chat, Chat.SendingType.READ_STATUS);
            }
        }

        //fire listener of each un-seen chats to seen
        if (chat != null) {
            List<Chat> list = MessageDb.updateSeenMessage(mContext, Objects.requireNonNull(chat), conversationId);
            for (Chat chat1 : list) {
                mOnChatSocketListener.onMessageReceive(chat1);
            }
        }

        //fire listener seen conversation
        Conversation conversation = ConversationDb.getConversationById(mContext, conversationId);
        if (conversation != null) {
            conversation.setRead(true);
            ConversationDb.save(mContext, conversation);
            mOnChatSocketListener.onConversationCreateOrUpdate(conversation);
        }
    }

    void archivedConversation(String conversationId) {
        //archived conversation in local
        ConversationDb.archivedConversation(mContext, conversationId);

        //fire listener delete conversation
        if (mOnChatSocketListener != null) {
            mOnChatSocketListener.onConversationDelete(conversationId);
        }
    }

    void deleteChat(Chat chat) {
        //send delete to socket
        Chat newChat = new Chat();
        newChat.setId(chat.getId());
        newChat.setSendingType(Chat.SendingType.DELETE);
        newChat.setSendTo(chat.getSendTo());
        newChat.setChannelId(chat.getChannelId());
        newChat.setSenderId(mMyUserId);
        newChat.setGroup(chat.isGroup());

        Channel channel = createChannel(newChat.getSendTo());
        channel.subscribe();

        try {
            final JSONObject jsonObject = new JSONObject(getGson().toJson(newChat));
            getChannelByName(newChat.getSendTo()).publish(jsonObject);
        } catch (Exception e) {
            Timber.e("deleteChat fail %s", e.toString());
        }

        //update local and fire listener of delete
        MessageDb.delete(mContext, chat, mOnMessageDbListener);
    }

    private Gson getGson() {
        if (mGson == null) {
            mGson = new GsonBuilder()
                    .registerTypeAdapter(Date.class, new GSonGMTDateAdapter())
                    .create();
        }
        return mGson;
    }

    private class LoadNewMessageCallback implements OnCallbackListener<Response<List<Chat>>> {
        private Chat mChat;

        private LoadNewMessageCallback(Chat chat) {
            this.mChat = chat;
        }

        @Override
        public void onFail(ErrorThrowable ex) {
            Timber.e("onFail");
            updateSeenStatus(mChat.getChannelId());
        }

        @Override
        public void onComplete(Response<List<Chat>> result) {
            List<Chat> chatList = result.body();
            if (chatList != null && !chatList.isEmpty()) {
                MessageDb.save(mContext, chatList);
                for (Chat chat : chatList) {
                    mOnChatSocketListener.onMessageReceive(chat);
                }
            }
            Timber.e("LoadNewMessageCallback ");

            updateSeenStatus(mChat.getChannelId());
        }
    }

    private class SendMessageCallback implements Ack {
        @Override
        public void call(String name, Object error, Object data) {
            Timber.e("back 1: " + data.toString());

            Chat ackChat = getGson().fromJson(data.toString(), Chat.class);
            if (error == null) {
                ackChat.setStatus(MessageState.SENT);

                if (mOnChatSocketListener != null) {
                    mOnChatSocketListener.onMessageSent(ackChat);
                }
            } else {
                ackChat.setStatus(MessageState.FAIL);
            }

            MessageDb.save(mContext, ackChat);
            Conversation con = ConversationDb.saveConversationFromChat(mContext, ackChat, true, null);

            if (mOnChatSocketListener != null) {
                mOnChatSocketListener.onMessageReceive(ackChat);
                mOnChatSocketListener.onConversationCreateOrUpdate(con);
            }

            //TODO: remove custom push notification if server is done
//            OneSignalWrapperData<Chat> oneSignalWrapperData = new OneSignalWrapperData<>();
//            oneSignalWrapperData.setData(ackChat);
//            oneSignalWrapperData.setType(OneSignalPushType.Conversation.getType());
//
//            OneSignalPushModel<Chat> model = new OneSignalPushModel<>(oneSignalWrapperData);
//            Observable<Response<Object>> ob = mApiOneSignal.pushChat(model);
//            BaseObserverHelper<Response<Object>> helper = new BaseObserverHelper<>(mContext, ob);
//            helper.execute();
        }
    }
}
