package com.sompom.tookitup.chat.listener;

/**
 * Created by He Rotha on 7/6/18.
 */
public interface ServiceStateListener {
    void onServiceStart();

    void onServiceFail(Exception ex);
}
