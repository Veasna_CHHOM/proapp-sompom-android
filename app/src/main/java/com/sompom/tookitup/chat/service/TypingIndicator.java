package com.sompom.tookitup.chat.service;

import android.os.Handler;

import java.util.Collections;
import java.util.List;

/**
 * Created by He Rotha on 11/15/18.
 */
class TypingIndicator {
    private final Handler mTypingHandler = new Handler();
    private final TypingRunnable mOnTypingTimeoutRunnable = new TypingRunnable();
    private final OnTypingListener mOnTypingListener;
    private Boolean mTyping = false;
    private boolean mIsGroupChatting;

    TypingIndicator(OnTypingListener onTypingListener) {
        mOnTypingListener = onTypingListener;
    }

    void startTyping(String sendTo,
                     String productId,
                     String channelId) {
        mIsGroupChatting = false;
        mOnTypingTimeoutRunnable.setRecipientId(Collections.singletonList(sendTo));
        mOnTypingTimeoutRunnable.setProductId(productId);
        mOnTypingTimeoutRunnable.setChannelId(channelId);

        if (!mTyping) {
            mTyping = true;
            mOnTypingListener.onTyping(mOnTypingTimeoutRunnable.mRecipientId,
                    false,
                    mOnTypingTimeoutRunnable.mProductId,
                    mOnTypingTimeoutRunnable.mChannelId,
                    true);

            mTypingHandler.removeCallbacks(mOnTypingTimeoutRunnable);
            mTypingHandler.postDelayed(mOnTypingTimeoutRunnable, 7000);
        }
    }

    void startTyping(List<String> sendTo,
                     String channelId) {
        mIsGroupChatting = true;
        mOnTypingTimeoutRunnable.setRecipientId(sendTo);
        mOnTypingTimeoutRunnable.setChannelId(channelId);

        if (!mTyping) {
            mTyping = true;
            mOnTypingListener.onTyping(mOnTypingTimeoutRunnable.mRecipientId,
                    true,
                    mOnTypingTimeoutRunnable.mProductId,
                    mOnTypingTimeoutRunnable.mChannelId,
                    true);

            mTypingHandler.removeCallbacks(mOnTypingTimeoutRunnable);
            mTypingHandler.postDelayed(mOnTypingTimeoutRunnable, 7000);
        }
    }

    void stopTyping() {
        mTyping = false;
        mTypingHandler.removeCallbacks(mOnTypingTimeoutRunnable);
    }

    public interface OnTypingListener {
        void onTyping(List<String> sendTo,
                      boolean isGroup,
                      String productId,
                      String channelId,
                      boolean isTyping);
    }

    private class TypingRunnable implements Runnable {

        private List<String> mRecipientId;
        private String mProductId;
        private String mChannelId;

        void setRecipientId(List<String> recipientId) {
            mRecipientId = recipientId;
        }

        void setProductId(String productId) {
            mProductId = productId;
        }

        void setChannelId(String channelId) {
            mChannelId = channelId;
        }

        @Override
        public void run() {
            if (!mTyping) {
                return;
            }
//            mOnTypingListener.onTyping(mRecipientId,
//                    mProductId,
//                    mChannelId,
//                    false);
            mTyping = false;
        }
    }
}
