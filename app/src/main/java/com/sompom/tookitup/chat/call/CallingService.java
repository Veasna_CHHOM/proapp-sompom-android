package com.sompom.tookitup.chat.call;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.sinch.android.rtc.NotificationResult;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.Sinch;
import com.sinch.android.rtc.SinchClient;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallListener;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

public class CallingService extends Service {
    private static final String APP_KEY = "de066fa9-d433-4f7c-a8a1-e20499ba0ada";
    private static final String APP_SECRET = "g1P6+thwTEu+d+WLunmu8w==";
    private static final String ENVIRONMENT = "clientapi.sinch.com";
    private static final String SERIALIZE_PRODUCT = "PRODUCT";
    private static final String SERIALIZE_USER = "USER";

    private SinchBinder mBinder = new SinchBinder();
    private SinchClient mSinchClient;
    private Call mCall;
    private List<CallingListener> mCallListener = new ArrayList<>();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }


    public interface CallingListener {
        void onCallEnd();

        void onIncomingCall(Call call);

        void onCallEstablished(Call call);

        void onCallProgressing(Call call);
    }

    public class SinchBinder extends Binder {


        public void start(String userId) {
            if (mSinchClient != null && mSinchClient.isStarted()) {
                return;
            }
            mSinchClient = Sinch.getSinchClientBuilder()
                    .context(getApplicationContext())
                    .userId(userId) //userId is current user login id
                    .applicationKey(APP_KEY)
                    .applicationSecret(APP_SECRET)
                    .environmentHost(ENVIRONMENT)
                    .build();

            mSinchClient.setSupportPushNotifications(true);
            mSinchClient.setSupportCalling(true);
            mSinchClient.setSupportActiveConnectionInBackground(true);
            mSinchClient.startListeningOnActiveConnection();
            mSinchClient.setSupportManagedPush(true);

            mSinchClient.addSinchClientListener(new MySinchListner());
            mSinchClient.getCallClient().addCallClientListener((callClient, callReceiver) -> {
                mCall = callReceiver;
                mCall.addCallListener(new SinchCallListener());

                for (CallingListener callingListener : mCallListener) {
                    Timber.e("onIncomming call " + callingListener);
                    callingListener.onIncomingCall(mCall);
                }
            });
            mSinchClient.start();

        }

        public void call(String recipientId, Product product) {
            Context context = getApplicationContext();
            User user = new User();
            user.setId(SharedPrefUtils.getUserId(context));
            user.setFirstName(SharedPrefUtils.getUserFirstName(context));
            user.setLastName(SharedPrefUtils.getUserLastName(context));
            user.setUserProfile(SharedPrefUtils.getImageProfileUrl(context));
            user.setStoreName(SharedPrefUtils.getStoreName(context));
            String userJson = new Gson().toJson(user);


            Map<String, String> headers = new HashMap<>();
            headers.put(SERIALIZE_USER, userJson);

            if (product != null) {
                Product cloneProduct = new Product();
                cloneProduct.setId(product.getId());
                cloneProduct.setName(product.getName());
                cloneProduct.setPrice(product.getPrice());
                cloneProduct.setCurrency(product.getCurrency());
                List<Media> list = Collections.singletonList(product.getMedia().get(0));
                cloneProduct.setProductMedia(list);
                headers.put(SERIALIZE_PRODUCT, new Gson().toJson(cloneProduct));
            }

            mCall = mSinchClient.getCallClient().callUser(recipientId, headers);
            mCall.addCallListener(new SinchCallListener());
        }

        public void setVolume(boolean isLoudSpeaker) {
            if (mSinchClient != null) {
                if (isLoudSpeaker) {
                    mSinchClient.getAudioController().enableSpeaker();
                } else {
                    mSinchClient.getAudioController().disableSpeaker();
                }
            }
        }

        public void answer() {
            if (mCall != null) {
                mCall.answer();
            }
        }

        /**
         * @return true mean it has error
         */
        public boolean hangup() {
            if (mCall != null) {
                Timber.e("hangup");
                mCall.hangup();
                return false;
            } else {
                Timber.e("hangup and null");
                return true;
            }
        }

        public void addCallListener(CallingListener callListener) {
            if (mCallListener.contains(callListener)) {
                return;
            }
            mCallListener.add(callListener);
        }

        public User getCurrentCallingUser(Map<String, String> header) {
            String data = header.get(SERIALIZE_USER);
            return new Gson().fromJson(data, User.class);
        }

        @Nullable
        public Product getCurrentProduct(Map<String, String> header) {
            String data = header.get(SERIALIZE_PRODUCT);
            return new Gson().fromJson(data, Product.class);
        }

        public void removeCallListener(CallingListener callListener) {
            mCallListener.remove(callListener);
        }

        public NotificationResult relayRemotePushNotificationPayload(final Map payload) {
            return mSinchClient.relayRemotePushNotificationPayload(payload);
        }

        public void stop() {
            if (mSinchClient != null) {
                mSinchClient.terminateGracefully();
                mSinchClient = null;
            }
        }

        public void logout() {
            if (mSinchClient == null) {
                return;
            }
            mSinchClient.stopListeningOnActiveConnection();
            mSinchClient.unregisterManagedPush();
            stop();
        }

    }

    private class SinchCallListener implements CallListener {
        @Override
        public void onCallEnded(Call endedCall) {
            mCall = null;
            for (CallingListener callingListener : mCallListener) {
                callingListener.onCallEnd();
            }
            vibrate();
        }

        @Override
        public void onCallEstablished(Call establishedCall) {
            for (CallingListener callingListener : mCallListener) {
                callingListener.onCallEstablished(establishedCall);
            }
            vibrate();
        }

        @Override
        public void onCallProgressing(Call progressingCall) {
            for (CallingListener callingListener : mCallListener) {
                callingListener.onCallProgressing(progressingCall);
            }

        }

        @Override
        public void onShouldSendPushNotification(Call call, List<PushPair> pushPairs) {
            Log.e("rotha", "onShouldSendPushNotification ");
        }

        private void vibrate() {
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                v.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE));
            } else {
                v.vibrate(100);
            }
        }
    }

}
