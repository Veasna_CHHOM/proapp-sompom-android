package com.sompom.tookitup.chat.call;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.sinch.android.rtc.NotificationResult;
import com.sinch.android.rtc.SinchHelpers;
import com.sompom.tookitup.intent.CallingIntent;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.HashMap;
import java.util.Map;

import timber.log.Timber;

import static android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND;
import static android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_VISIBLE;

public class FirebaseMsgService extends FirebaseMessagingService {
    //        ///To check if the app is in foreground ///
    public static boolean foregrounded() {
        ActivityManager.RunningAppProcessInfo appProcessInfo =
                new ActivityManager.RunningAppProcessInfo();
        ActivityManager.getMyMemoryState(appProcessInfo);
        return (appProcessInfo.importance == IMPORTANCE_FOREGROUND
                || appProcessInfo.importance == IMPORTANCE_VISIBLE);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.e("FirebaseMsgService", "onMessageReceived");

        if (SinchHelpers.isSinchPushPayload(remoteMessage.getData())) {
            // it's Sinch message - relay it to SinchClient
            Log.e("FirebaseMsgService", "it's Sinch message - relay it to SinchClient");
        } else {
            // it's NOT Sinch message - process yourself
            Log.e("FirebaseMsgService", "it's NOT Sinch message - process yourself");
        }


        Map<String, String> map = remoteMessage.getData();

        Timber.e(map.toString());
        if (SinchHelpers.isSinchPushPayload(map)) {
            ///Check if the application is in foreground if in foreground the CallingService already run ////
            if (foregrounded()) {
                return;
            }
//

            new ServiceConnection() {
                private Map payload;

                @Override
                public void onServiceConnected(ComponentName name, IBinder service) {
                    Timber.e("onServiceConnected");
                    if (payload != null) {
                        CallingService.SinchBinder sinchService = (CallingService.SinchBinder) service;
                        ((CallingService.SinchBinder) service).start(SharedPrefUtils.getUserId(getApplicationContext()));
                        NotificationResult result = sinchService.relayRemotePushNotificationPayload(payload);

                        if (result == null ||
                                result.getCallResult() == null ||
                                result.getCallResult().isCallCanceled()) {
                            return;
                        }
                        Map<String, String> data = result.getCallResult().getHeaders();
                        HashMap dataHashMap =
                                (data instanceof HashMap)
                                        ? (HashMap) data
                                        : new HashMap<>(data);

                        NotificationCallVo callVo = new NotificationCallVo();
                        callVo.setData(dataHashMap);
                        CallingIntent intent = new CallingIntent(getApplicationContext(), callVo);
                        startActivity(intent);
                    }
                    payload = null;
                }

                @Override
                public void onServiceDisconnected(ComponentName name) {
                    Timber.e("onServiceDisconnected");

                }

                public void relayMessageData(Map<String, String> data) {
                    payload = data;
                    getApplicationContext().bindService(new Intent(getApplicationContext(), CallingService.class), this, BIND_AUTO_CREATE);
                }
            }.relayMessageData(map);
        }


    }
//


}