package com.sompom.tookitup.broadcast.chat;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.sompom.tookitup.broadcast.AbsService;
import com.sompom.tookitup.listener.PlayAudioServiceListener;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.io.IOException;

import timber.log.Timber;

/**
 * Created by nuonveyo on 9/19/18.
 */

public class PlayAudioService extends AbsService implements MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener {

    private final PlayAudioBinder mBinder = new PlayAudioBinder();
    private final Handler mSeekBarUpdateHandler = new Handler();
    private MediaPlayer mMediaPlayer;
    private Chat mChat;
    private boolean mIsPause;
    private boolean mIsLoading;
    private boolean mIsPlaying;
    private final Runnable mUpdateSeekBar = new Runnable() {
        @Override
        public void run() {
            if (mMediaPlayer != null) {
                mSeekBarUpdateHandler.postDelayed(this, 10);
                mIsPlaying = true;
                mBinder.onPlaying(mMediaPlayer.getCurrentPosition());
            }
        }
    };

    public static void startService(Context context, Chat chat, boolean isLoading) {
        Intent intent = new Intent(context, PlayAudioService.class);
        intent.putExtra(SharedPrefUtils.STATUS, isLoading);
        intent.putExtra(SharedPrefUtils.DATA, chat);
        context.startService(intent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        stop();
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        mBinder.onError();
        mIsLoading = false;
        return true;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        start();
        mIsLoading = false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            mChat = intent.getParcelableExtra(SharedPrefUtils.DATA);
            mIsLoading = intent.getBooleanExtra(SharedPrefUtils.STATUS, false);
            inMediaPlayer();
        }
        return START_STICKY;
    }

    private void inMediaPlayer() {
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_SYSTEM);
        mMediaPlayer.setOnPreparedListener(this);
        mMediaPlayer.setOnCompletionListener(this);
        mMediaPlayer.setOnErrorListener(this);

        try {
            Timber.e("URI: %s", mChat.getMediaList().get(0).getUrl());
            mMediaPlayer.setDataSource(mChat.getMediaList().get(0).getUrl());
            mMediaPlayer.prepareAsync();
        } catch (IllegalStateException e) {
            Timber.e("play error: %s", e.getMessage());
            errorLoadAudioSource();
            stop();
        } catch (IOException e) {
            Timber.e("IOException: %s", e.getMessage());
            errorLoadAudioSource();
        }
    }

    private void errorLoadAudioSource() {
        mBinder.onError();
        mIsLoading = false;
        stop();
    }

    public boolean isPlaying() {
        return (mMediaPlayer != null && mMediaPlayer.isPlaying()) || mIsPlaying;
    }

    public boolean isPause() {
        return mIsPause;
    }

    public void checkToHideLoading() {
        if (mIsLoading) {
            mIsLoading = false;
            mBinder.hideLoading();
        }
    }

    public String getCurrentAudioId() {
        if (mChat == null) {
            return "";
        }
        return mChat.getId();
    }

    public void resume() {
        mIsPause = false;
        mBinder.onResume();
        mSeekBarUpdateHandler.postDelayed(mUpdateSeekBar, 0);
        mMediaPlayer.start();
    }

    public void pause() {
        mIsPlaying = false;
        mIsPause = true;
        mBinder.onPause();
        mSeekBarUpdateHandler.removeCallbacks(mUpdateSeekBar);
        mMediaPlayer.pause();
    }

    private void start() {
        if (mMediaPlayer != null) {
            mSeekBarUpdateHandler.postDelayed(mUpdateSeekBar, 0);
            mMediaPlayer.start();
        }
    }

    public void stop() {
        mBinder.onFinish();
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        mIsPause = false;
        mIsPlaying = false;
        mIsLoading = false;
        mChat = null;
        mSeekBarUpdateHandler.removeCallbacks(mUpdateSeekBar);
        stopSelf();
    }

    public long getCurrentPauseDuration() {
        if (mMediaPlayer != null) {
            return mMediaPlayer.getCurrentPosition();
        }
        return 0;
    }

    public class PlayAudioBinder extends Binder {
        private PlayAudioServiceListener mListener;

        public PlayAudioService getService() {
            return PlayAudioService.this;
        }

        public void setListener(PlayAudioServiceListener listener) {
            mListener = listener;
        }

        void onPlaying(long duration) {
            if (mListener != null) {
                mListener.onPlaying(mChat.getId(), duration);
            }
        }

        public void onError() {
            if (mListener != null) {
                mListener.onError();
            }
        }

        public void onPause() {
            if (mListener != null) {
                mListener.onPause(mChat.getId());
            }
        }

        void onFinish() {
            if (mListener != null) {
                mListener.onFinish();
            }
        }

        public void onResume() {
            if (mListener != null) {
                mListener.onResume(mChat.getId());
            }
        }

        public void hideLoading() {
            if (mListener != null) {
                mListener.hideLoading(mChat.getId());
            }
        }
    }
}
