/*
 * Copyright (c) 2018. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */

package com.sompom.tookitup.broadcast.upload;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.AbsService;
import com.sompom.tookitup.model.NotificationChannelSetting;
import com.sompom.tookitup.newui.HomeActivity;

import java.util.Random;


/**
 * Created by He Rotha on 2/20/18.
 */

public abstract class NotificationServices extends AbsService {
    private static final int NOTIFICATION_FOREGROUND = 1;
    private static final int NOTIFICATION_OPTION = 2;
    private NotificationManager mNotifyManager;

    @Override
    public void onCreate() {
        super.onCreate();
        mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public void startForegroundNotification(NotificationChannelSetting setting) {
        Intent notificationIntent = new Intent(this, HomeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);


        NotificationCompat.Builder notification = new NotificationCompat.Builder(this, getNotificationChannelID(setting))
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.product_form_notification_preparing))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setProgress(100, 100, true)
                .setContentIntent(pendingIntent)
                .setOnlyAlertOnce(true)
                .setOngoing(true);
        startForeground(NOTIFICATION_FOREGROUND, notification.build());

    }

    public void updateProgress(int percent, String title, NotificationChannelSetting setting) {
        Intent notificationIntent = new Intent(getApplicationContext(), HomeActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, 0);

        NotificationCompat.Builder notification = new NotificationCompat.Builder(getApplicationContext(), getNotificationChannelID(setting))
                .setContentTitle(getString(R.string.app_name))
                .setContentText(title)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setOnlyAlertOnce(true)
                .setContentIntent(pendingIntent)
                .setOngoing(true);

        if (percent >= 0 && percent <= 100) {
            notification.setProgress(100, percent, false);
        } else {
            notification.setProgress(100, 100, true);
        }
        mNotifyManager.notify(NOTIFICATION_FOREGROUND, notification.build());
    }

    public void pushSingleNotification(String title, PendingIntent pendingIntent, NotificationChannelSetting setting) {
        NotificationCompat.Builder notification = new NotificationCompat.Builder(getApplicationContext(), getNotificationChannelID(setting))
                .setContentTitle(getString(R.string.app_name))
                .setContentText(title)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setOnlyAlertOnce(true)
                .setContentIntent(pendingIntent)
                .setOngoing(false);
        Random rand = new Random();
        mNotifyManager.notify(rand.nextInt(99999) + 1, notification.build());
    }

    public void pushQueueNotification(String title, PendingIntent pendingIntent, NotificationChannelSetting setting) {
        NotificationCompat.Builder notification = new NotificationCompat.Builder(getApplicationContext(), getNotificationChannelID(setting))
                .setContentTitle(getString(R.string.app_name))
                .setContentText(title)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setOnlyAlertOnce(true)
                .setContentIntent(pendingIntent)
                .setOngoing(true);
        mNotifyManager.notify(NOTIFICATION_OPTION, notification.build());
    }

    public void cancelQueueNotification() {
        mNotifyManager.cancel(NOTIFICATION_OPTION);
    }

    private String getNotificationChannelID(NotificationChannelSetting setting) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            mNotifyManager.createNotificationChannel(setting.getNotificationChannel(this));
        }
        return setting.getNotificationID();
    }
}
