package com.sompom.tookitup.broadcast;

import android.app.Service;

import com.sompom.tookitup.TookitupApplication;
import com.sompom.tookitup.injection.broadcast.BroadcastComponent;
import com.sompom.tookitup.injection.broadcast.BroadcastModule;

/**
 * Created by He Rotha on 7/13/18.
 */
public abstract class AbsService extends Service {
    private BroadcastComponent mControllerComponent;

    public BroadcastComponent getControllerComponent() {
        if (mControllerComponent == null) {
            mControllerComponent = ((TookitupApplication) getApplicationContext())
                    .getApplicationComponent()
                    .newBroadcastComponent(new BroadcastModule(getApplicationContext()));
        }
        return mControllerComponent;
    }
}
