package com.sompom.tookitup.broadcast;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.annotation.Nullable;

import com.desmond.squarecamera.utils.Keys;
import com.sompom.tookitup.R;

/**
 * Created by He Rotha on 10/30/18.
 */
public class SoundEffectService extends IntentService {
    private MediaPlayer mMediaPlayer;

    public SoundEffectService() {
        super(SoundEffectService.class.getName());
    }

    public static void playSound(Context context,
                                 SoundFile soundFile) {
        AudioManager am = (AudioManager) context.getSystemService(AUDIO_SERVICE);
        if (am != null) {
            int currentVolume = am.getStreamVolume(AudioManager.STREAM_SYSTEM);
            if (currentVolume <= 0) {
                //no need to play sound effect, if phone volume is mute
                return;
            }
        }
        Intent intent = new Intent();
        intent.setClass(context, SoundEffectService.class);
        intent.putExtra(Keys.DATA, soundFile.mId);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent == null) {
            return;
        }
        int sound = intent.getIntExtra(Keys.DATA, 0);
        if (sound != 0) {
            if (mMediaPlayer != null) {
                mMediaPlayer.stop();
            }
            mMediaPlayer = MediaPlayer.create(this, sound);
            mMediaPlayer.setOnCompletionListener(MediaPlayer::stop);
            mMediaPlayer.start();
        }
    }


    public enum SoundFile {
        DROP(R.raw.drop),
        WOOSH(R.raw.woosh),
        MATCH(R.raw.match),
        SEND_MESSAGE(R.raw.send_message);

        private final int mId;

        SoundFile(int id) {
            mId = id;
        }
    }
}
