package com.sompom.tookitup.broadcast.upload;

import com.sompom.tookitup.helper.upload.FileType;
import com.sompom.tookitup.helper.upload.Orientation;

/**
 * Created by imac on 8/16/17.
 */

public class UploadMedia {
    //files path
    private String[] mFilePath;
    //origin path from mFilePath
    private String[] mOriginPath;
    //file thumbnail cos video need to has thumbnail
    private String[] mThumbnail;
    //file types
    private FileType[] mFileTypes;
    //id of object to upload
    private String mId;
    //id of notification
    private String mNotificationTitle;
    private Object mTag;
    //file types
    private Orientation[] mOrientations;

    /**
     * model for operating uploading
     *
     * @param id                : id of object
     * @param notificationTitle : title of Notification
     * @param filePath          : file to upload
     * @param fileTypes         : file type
     */
    public UploadMedia(String id,
                       String notificationTitle,
                       String[] filePath,
                       FileType[] fileTypes) {
        mId = id;
        mFilePath = filePath;
        mFileTypes = fileTypes;
        mNotificationTitle = notificationTitle;

        mThumbnail = new String[filePath.length];
        mOriginPath = new String[filePath.length];
        System.arraycopy(filePath, 0, mOriginPath, 0, mOriginPath.length);
    }

    /**
     * model for operating uploading
     *
     * @param id                : id of object
     * @param notificationTitle : title of Notification
     * @param filePath          : file to upload
     * @param fileTypes         : file type
     */
    public UploadMedia(String id,
                       String notificationTitle,
                       String[] filePath,
                       FileType[] fileTypes,
                       Orientation[] orientations) {
        mId = id;
        mFilePath = filePath;
        mFileTypes = fileTypes;
        mNotificationTitle = notificationTitle;
        mOrientations = orientations;

        mThumbnail = new String[filePath.length];
        mOriginPath = new String[filePath.length];
        System.arraycopy(filePath, 0, mOriginPath, 0, mOriginPath.length);
    }

    public String getNotificationTitle() {
        return mNotificationTitle;
    }

    public String getId() {
        return mId;
    }


    public String[] getFilePath() {
        return mFilePath;
    }

    public void setFilePath(String[] filePath) {
        mFilePath = filePath;
    }

    public String[] getThumbnail() {
        return mThumbnail;
    }

    public void setThumbnail(String[] thumbnail) {
        mThumbnail = thumbnail;
    }

    public FileType[] getFileTypes() {
        return mFileTypes;
    }

    public void setFileTypes(FileType[] fileTypes) {
        mFileTypes = fileTypes;
    }

    public Object getTag() {
        return mTag;
    }

    public void setTag(Object tag) {
        mTag = tag;
    }

    public Orientation[] getOrientations() {
        return mOrientations;
    }
}
