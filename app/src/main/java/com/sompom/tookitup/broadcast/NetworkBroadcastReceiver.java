package com.sompom.tookitup.broadcast;

/**
 * Created by he.rotha on 2/26/16.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.net.ConnectivityManager;

import com.sompom.tookitup.utils.LocationStateUtil;
import com.sompom.tookitup.utils.NetworkStateUtil;


public class NetworkBroadcastReceiver extends BroadcastReceiver {
    private NetworkListener mListener;
    private IntentFilter mIntentFilter;
    private boolean mIsBothBroadCast;

    public NetworkBroadcastReceiver() {
        mIsBothBroadCast = false;
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
    }

    public NetworkBroadcastReceiver(NetworkLocationListener listener) {
        mListener = listener;
        mIsBothBroadCast = true;
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(LocationManager.PROVIDERS_CHANGED_ACTION);
        mIntentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
    }

    public NetworkBroadcastReceiver(NetworkListener listener) {
        mListener = listener;
        mIsBothBroadCast = false;
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
    }

    public void setListener(NetworkListener listener) {
        mListener = listener;
    }

    public IntentFilter getIntentFilter() {
        return mIntentFilter;
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        if (mListener == null) {
            return;
        }
        if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            if (NetworkStateUtil.isNetworkAvailable(context)) {
                mListener.onNetworkState(NetworkState.Connected);
            } else {
                mListener.onNetworkState(NetworkState.Disconnected);
            }

            if (mIsBothBroadCast) {
                if (mListener instanceof NetworkLocationListener) {
                    if (LocationStateUtil.isLocationEnable(context)) {
                        ((NetworkLocationListener) mListener).onLocationEnable();
                    } else {
                        ((NetworkLocationListener) mListener).onLocationDisable();
                    }
                }
            }
        } else if (intent.getAction().equals(LocationManager.PROVIDERS_CHANGED_ACTION)) {
            if (mIsBothBroadCast) {
                if (mListener instanceof NetworkLocationListener) {
                    if (LocationStateUtil.isLocationEnable(context)) {
                        ((NetworkLocationListener) mListener).onLocationEnable();
                    } else {
                        ((NetworkLocationListener) mListener).onLocationDisable();
                    }
                }
            }
        }

    }

    public enum NetworkState {
        Connected, Disconnected, Nothing
    }

    public interface NetworkLocationListener extends NetworkListener {

        void onLocationEnable();

        void onLocationDisable();
    }

    public interface NetworkListener {

        void onNetworkState(NetworkState networkState);
    }
}