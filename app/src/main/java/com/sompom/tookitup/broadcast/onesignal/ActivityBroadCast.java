package com.sompom.tookitup.broadcast.onesignal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.sompom.tookitup.model.result.OneSignalModel;
import com.sompom.tookitup.utils.SharedPrefUtils;

/**
 * Created by he.rotha on 2/7/17.
 */

public class ActivityBroadCast extends BroadcastReceiver {
    public static final String CONVERSATION = "CONVERSATION";
    public static final String OTHER = "OTHER";
    private ChatNotificationListener mListener;
    private IntentFilter mIntentFilter;

    public ActivityBroadCast() {
    }

    public ActivityBroadCast(ChatNotificationListener listener) {
        this.mListener = listener;

        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(CONVERSATION);
        mIntentFilter.addAction(OTHER);
    }

    public IntentFilter getIntentFilter() {
        return mIntentFilter;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (mListener != null) {
            OneSignalModel notificationData = intent.getParcelableExtra(SharedPrefUtils.PRODUCT);
            if (notificationData != null) {
                if (intent.getAction().equals(CONVERSATION)) {
                    mListener.onReceiveChatNotification(notificationData);
                } else if (intent.getAction().equals(OTHER)) {
                    mListener.onReceiveNotification(notificationData);
                }
            }
        }
    }

    public interface ChatNotificationListener {
        void onReceiveChatNotification(OneSignalModel notificationData);

        void onReceiveNotification(OneSignalModel model);
    }

}
