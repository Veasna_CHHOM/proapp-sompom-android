package com.sompom.tookitup.broadcast.onesignal;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotificationReceivedResult;
import com.sompom.tookitup.TookitupApplication;
import com.sompom.tookitup.adapter.GSonGMTDateAdapter;
import com.sompom.tookitup.chat.service.ChatSocket;
import com.sompom.tookitup.chat.service.SocketService;
import com.sompom.tookitup.database.ConversationDb;
import com.sompom.tookitup.database.MessageDb;
import com.sompom.tookitup.helper.LocaleManager;
import com.sompom.tookitup.injection.broadcast.BroadcastComponent;
import com.sompom.tookitup.injection.broadcast.BroadcastModule;
import com.sompom.tookitup.model.emun.OneSignalPushType;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.model.result.Comment;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.utils.ChatNotificationUtil;
import com.sompom.tookitup.utils.SharedPrefUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by he.rotha on 5/2/16.
 */
public class OneSignalReceiver extends NotificationExtenderService {
    public static final String ACTION_RECEIVE_NOTIFICATION = "ACTION_RECEIVE_NOTIFICATION";
    @Inject
    public ApiService mApiService;
    private BroadcastComponent mControllerComponent;

    @Override
    protected boolean onNotificationProcessing(OSNotificationReceivedResult notification) {
        Context context = getApplicationContext();
        if (!SharedPrefUtils.isLogin(context)) {
            return true;
        }

        try {
            //json String receive from Parse
            JSONObject json = notification.payload.additionalData;
            OneSignalPushType type = OneSignalPushType.getType(getType(json));
            Timber.e("type: %s", type);
            JSONObject jsonData = json.getJSONObject("content");
            if (type == OneSignalPushType.Conversation) {
                handleChatNotificationManual(jsonData);
            } else if (type == OneSignalPushType.Comment) {
                //TODO: manage push notification with comment later
                Comment comment = getGson().fromJson(jsonData.toString(), Comment.class);
                Timber.e("comment: %s", new Gson().toJson(comment));
                ChatNotificationUtil.pushGeneralNotification(this,
                        comment.getUser().getFullName(),
                        comment.getContent());
            }
        } catch (Exception e) {
            Timber.e(e);
        }
        return true; //return TRUE for handle notification our self
    }

    private void handleChatNotificationManual(JSONObject jsonData) {
        Chat chat = getGson().fromJson(jsonData.toString(), Chat.class);
        Timber.e(jsonData.toString());

        //chat should not receive from myself
        if (TextUtils.equals(SharedPrefUtils.getUserId(this), chat.getSenderId())) {
            return;
        }

        if (mApiService == null) {
            getControllerComponent().inject(this);
        }
        //if this chat is already existed, we don't need to do anything
        if (ChatSocket.isExistingChat(this, chat, mApiService)) {
            return;
        }

        try {
            //due to new version of Android, GCM cannot start service in background,
            //so, we check current SocketService running first.
            //if it is running, we send this stuff to SocketService,
            //otherwise, we need to save chat in db and push notification our self
            boolean isRunning = SocketService.isRunning(this);
            Timber.e("SocketService running = %s", isRunning);
            if (isRunning) {
                startService(SocketService.pushNotification(getApplicationContext(), chat));
            } else {
                MessageDb.save(this, chat);
                ConversationDb.saveConversationFromChat(this, chat, false, null);
                ChatNotificationUtil.pushChatNotification(this, chat);
            }
        } catch (IllegalStateException ex) {
            Timber.e(ex);

            //due to new version of Android, GCM cannot start service in background,
            //even if server is running, but notification cannot start service sometime
            //so we need to handle notification our self again.
            MessageDb.save(this, chat);
            ConversationDb.saveConversationFromChat(this, chat, false, null);
            ChatNotificationUtil.pushChatNotification(this, chat);
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    private BroadcastComponent getControllerComponent() {
        if (mControllerComponent == null) {
            mControllerComponent = ((TookitupApplication) getApplicationContext())
                    .getApplicationComponent()
                    .newBroadcastComponent(new BroadcastModule(getApplicationContext()));
        }
        return mControllerComponent;
    }

    private String getType(JSONObject jsonObject) {
        try {
            return jsonObject.getString("type");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    private Gson getGson() {
        return new GsonBuilder()
                .registerTypeAdapter(Date.class, new GSonGMTDateAdapter())
                .create();
    }
}
