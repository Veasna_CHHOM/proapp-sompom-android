package com.sompom.tookitup.broadcast.upload;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.sompom.tookitup.R;
import com.sompom.tookitup.helper.upload.FileType;
import com.sompom.tookitup.helper.upload.Orientation;
import com.sompom.tookitup.intent.newintent.HomeIntent;
import com.sompom.tookitup.intent.newintent.ProductDetailIntent;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnCompleteListListener;
import com.sompom.tookitup.model.NotificationChannelSetting;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.ProductManager;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by imac on 8/16/17.
 */

public class ProductUploadService extends UploadService {
    private final UploadBinder mBinder = new UploadBinder();
    private final List<Product> mLifeStreams = new ArrayList<>();
    @Inject
    public ApiService mApiService;
    private ProductManager mProductManager;

    public static Intent getIntent(Context activity, Product product) {
        Intent intent = new Intent(activity, ProductUploadService.class);
        intent.putExtra(SharedPrefUtils.PRODUCT, product);
        return intent;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return START_STICKY;
        }
        if (mApiService == null) {
            getControllerComponent().inject(this);
        }
        mProductManager = new ProductManager(this, mApiService);

        Product product = intent.getParcelableExtra(SharedPrefUtils.PRODUCT);
        if (TextUtils.isEmpty(product.getUploadId())) {
            product.setUploadId(String.valueOf(System.currentTimeMillis()));
        }
        mLifeStreams.add(product);
        if (mLifeStreams.size() == 1) {
            cancelQueueNotification();
            startUploading(convertTimelineToUploadMedia(product), NotificationChannelSetting.UploadingProduct);
        } else {
            Intent homeIntent = new HomeIntent(getApplicationContext());

            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),
                    (int) System.currentTimeMillis(),
                    homeIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            pushQueueNotification(getString(R.string.product_form_notification_queue), pendingIntent, NotificationChannelSetting.UploadingProduct);
        }

        return START_STICKY;
    }

    private UploadMedia convertTimelineToUploadMedia(Product product) {
        if (product.getMedia() == null || product.getMedia().isEmpty()) {
            return new UploadMedia(product.getUploadId(), getString(R.string.app_name), new String[]{}, new FileType[]{});
        }
        String[] paths = new String[product.getMedia().size()];
        FileType[] fileTypes = new FileType[product.getMedia().size()];
        Orientation[] orientations = new Orientation[product.getMedia().size()];

        for (int i = 0; i < paths.length; i++) {
            paths[i] = product.getMedia().get(i).getUrl();
            fileTypes[i] = FileType.IMAGE;
            orientations[i] = Orientation.Portrait;
        }
        return new UploadMedia(product.getUploadId(), getString(R.string.app_name), paths, fileTypes, orientations);
    }

    @Override
    void onUploadSuccess(final UploadMedia uploadMedia) {
        for (final Product product : mLifeStreams) {
            if (product.getUploadId().equals(uploadMedia.getId())) {
                for (int i = 0; i < uploadMedia.getFilePath().length; i++) {
                    product.getMedia().get(i).setUrl(uploadMedia.getFilePath()[i]);
                    product.getMedia().get(i).setThumbnail(uploadMedia.getThumbnail()[i]);
                }
                Observable<Response<Product>> callback = mProductManager.postOrUpdate(product);
                try {
                    ResponseObserverHelper<Response<Product>> helper = new ResponseObserverHelper<>(getApplicationContext(), callback);
                    helper.execute(new OnCallbackListener<Response<Product>>() {
                        @Override
                        public void onFail(ErrorThrowable ex) {
                            generateFailAction(product);
                        }

                        @Override
                        public void onComplete(Response<Product> value) {
                            for (int i = mLifeStreams.size() - 1; i >= 0; i--) {
                                if (mLifeStreams.get(i).getUploadId().equals(uploadMedia.getId())) {
                                    mLifeStreams.remove(i);
                                    break;
                                }
                            }

                            if (value != null && value.body() != null) {
                                boolean isUpdate = !TextUtils.isEmpty(product.getId());
                                mBinder.onPostComplete(value.body(), isUpdate);
                                checkNextUpload();

                                Intent intent = new ProductDetailIntent(getApplicationContext(), value.body(), false, true);
                                PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),
                                        (int) System.currentTimeMillis(),
                                        intent,
                                        PendingIntent.FLAG_UPDATE_CURRENT);
                                String title = getMessage(isUpdate, product.getName());
                                pushSingleNotification(title, pendingIntent, NotificationChannelSetting.UploadingProduct);
                            } else {
                                generateFailAction(product);
                            }
                        }
                    });
                } catch (Exception e) {
                    Timber.e("onUpload Product fail: %s", e.toString());
                }
                break;
            }
        }
    }

    @Override
    void onUploadFail(UploadMedia uploadMedia) {
        Product product = null;
        for (int i = mLifeStreams.size() - 1; i >= 0; i--) {
            if (mLifeStreams.get(i).getUploadId().equals(uploadMedia.getId())) {
                product = mLifeStreams.get(i);
                mLifeStreams.remove(i);
                break;
            }
        }
        generateFailAction(product);
    }

    private String getMessage(boolean isUpdate, String productName) {
        if (isUpdate) {
            return getString(R.string.product_form_notification_update_success);
        } else {
            return getString(R.string.product_form_notification_upload_success, productName);
        }
    }

    private void generateFailAction(Product product) {
        if (product != null) {
            mLifeStreams.remove(product);

            Intent intent = ProductUploadService.getIntent(getApplicationContext(), product);
            PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(),
                    (int) System.currentTimeMillis(),
                    intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            pushSingleNotification(getString(R.string.product_form_notification_fail), pendingIntent, NotificationChannelSetting.UploadingProduct);
        }
        checkNextUpload();
    }

    private void checkNextUpload() {
        if (mLifeStreams.size() == 1) {
            cancelQueueNotification();
        }
        if (!mLifeStreams.isEmpty()) {
            startUploading(convertTimelineToUploadMedia(mLifeStreams.get(0)), NotificationChannelSetting.UploadingProduct);
        } else {
            stopForeground(true);
            stopSelf();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class UploadBinder extends Binder {
        private OnCompleteListListener<Product> mListener;

        public void setListener(OnCompleteListListener<Product> listener) {
            mListener = listener;
        }

        private void onPostComplete(Product product, boolean isUpdate) {
            if (mListener != null) {
                mListener.onComplete(product, isUpdate);
            }
        }
    }
}
