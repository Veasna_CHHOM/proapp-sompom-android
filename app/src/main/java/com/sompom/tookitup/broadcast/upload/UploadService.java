package com.sompom.tookitup.broadcast.upload;

import com.sompom.tookitup.R;
import com.sompom.tookitup.helper.upload.AbsLifeStreamUploader;
import com.sompom.tookitup.helper.upload.AbsMyUploader;
import com.sompom.tookitup.helper.upload.AbsProductUploader;
import com.sompom.tookitup.helper.upload.UploadListener;
import com.sompom.tookitup.model.NotificationChannelSetting;

import timber.log.Timber;

/**
 * Created by imac on 8/16/17.
 */

public abstract class UploadService extends NotificationServices {
    public void startUploading(UploadMedia media, NotificationChannelSetting setting) {
        startForegroundNotification(setting);
        if (media.getFilePath() != null && media.getFilePath().length > 0) {
            upload(media, 0, setting);
        } else {
            startPosting(media, setting);
        }
    }

    public void startPosting(UploadMedia uploadMedia, NotificationChannelSetting setting) {
        updateProgress(-1, getString(R.string.product_form_notification_posting), setting);
        onUploadSuccess(uploadMedia);
    }

    public void upload(final UploadMedia uploadMedia,
                       final int index,
                       final NotificationChannelSetting setting) {
        if (uploadMedia.getFilePath()[index].startsWith("http")) {
            checkNextUpload(uploadMedia, index, setting);
        } else {
            AbsMyUploader uploader;

            if (setting == NotificationChannelSetting.UploadingLifeStream) {
                uploader = new AbsLifeStreamUploader(this,
                        uploadMedia.getFilePath()[index],
                        uploadMedia.getFileTypes()[index],
                        uploadMedia.getOrientations()[index]);
            } else {
                uploader = new AbsProductUploader(this,
                        uploadMedia.getFilePath()[index],
                        uploadMedia.getFileTypes()[index],
                        uploadMedia.getOrientations()[index]);
            }

            uploader.setListener(new UploadListener() {
                private int mCurrentPercent = 0;

                @Override
                public void onUploadSucceeded(String requestId, String imageUrl, String thumb) {
                    uploadMedia.getFilePath()[index] = imageUrl;
                    uploadMedia.getThumbnail()[index] = thumb;
                    checkNextUpload(uploadMedia, index, setting);
                }

                @Override
                public void onUploadFail(String requestId, Exception ex) {
                    UploadService.this.onUploadFail(uploadMedia);
                }

                @Override
                public void onUploadProgressing(String id, int percent) {
                    if (mCurrentPercent != percent) {
                        if (percent % 5 == 0) {
                            mCurrentPercent = percent;
                            String title = getString(R.string.product_form_notification_uploading_in_progress, index + 1, uploadMedia.getFilePath().length);
                            updateProgress(percent, title, setting);
                        }
                    }
                }

                @Override
                public void onCancel(String id) {
                    Timber.e("onCancel Upload: %s", id);
                }
            });

            uploader.doUpload();
        }
    }

    private void checkNextUpload(UploadMedia uploadMedia, int index, NotificationChannelSetting setting) {
        if (index + 1 < uploadMedia.getFilePath().length) {
            upload(uploadMedia, index + 1, setting);
        } else {
            startPosting(uploadMedia, setting);
        }
    }

    abstract void onUploadSuccess(UploadMedia uploadMedia);

    abstract void onUploadFail(UploadMedia uploadMedia);

}
