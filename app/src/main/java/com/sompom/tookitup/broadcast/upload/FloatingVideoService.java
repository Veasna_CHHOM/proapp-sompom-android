package com.sompom.tookitup.broadcast.upload;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.media.AudioManager;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.WindowManager;

import com.sompom.tookitup.helper.CoordinateValueAnimator;
import com.sompom.tookitup.helper.FloatingVideoGesture;
import com.sompom.tookitup.model.LifeStream;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.widget.lifestream.FloatingMediaLayout;

import timber.log.Timber;

/**
 * Created by nuonveyo on 1/18/18.
 */

public class FloatingVideoService extends Service {
    private WindowManager mWindowManager;
    private FloatingMediaLayout mMediaLayout;
    private Point mScreen = new Point();
    private FloatingVideoBinder mFloatingVideoBinder = new FloatingVideoBinder();
    private AudioManager mAudioManager;
    private AudioManager.OnAudioFocusChangeListener mOnAudioListener = focusChange -> {
        Timber.e("OnAudioFocusChangeListener " + focusChange);
        if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
            removeVideo();
            stopSelf();
        }
    };

    public static void init(Context context) {
        Intent intent = new Intent(context, FloatingVideoService.class);
        context.startService(intent);
    }

    public static void startFloating(Context context, LifeStream lifeStream, int mediaPosition) {
        Intent intent = new Intent(context, FloatingVideoService.class);
        intent.putExtra(SharedPrefUtils.DATA, lifeStream);
        intent.putExtra(SharedPrefUtils.ID, mediaPosition);
        context.startService(intent);
    }

    public static void stop(Context context) {
        Intent intent = new Intent(context, FloatingVideoService.class);
        context.stopService(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LifeStream lifeStream = intent.getParcelableExtra(SharedPrefUtils.DATA);
        if (lifeStream != null) {
            int position = intent.getIntExtra(SharedPrefUtils.ID, 0);
            floatVideo(lifeStream, position);
        }
        Timber.e(hashCode() + " onStartCommand " + (mMediaLayout != null));
        return START_NOT_STICKY;
    }

    private void floatVideo(LifeStream lifeStream, int position) {
        removeVideo();
        if (mAudioManager == null) {
            mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        }
        if (mAudioManager != null) {
            mAudioManager.requestAudioFocus(mOnAudioListener,
                    AudioManager.STREAM_MUSIC,
                    AudioManager.AUDIOFOCUS_GAIN);
        }

        //Add the view to the window
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mMediaLayout = new FloatingMediaLayout(getApplicationContext());
        mMediaLayout.setOnFloatingMediaPlayVideoListener(() -> {
            removeVideo();
            stopSelf();
            mFloatingVideoBinder.onFloatingVideoPlayingFinish();
        });

        mMediaLayout.setShowControl(false);
        mMediaLayout.setMedia(lifeStream.getMedia().get(position));
        mMediaLayout.onMediaPlay();
        mWindowManager.getDefaultDisplay().getSize(mScreen);

        //Add the view to the window.
        int width = (int) (mScreen.x / 2.5);
        //TODO: need remove default height after server response media with and height
        int height = (int) (mScreen.y / 2.5);
        if (lifeStream.getMedia().get(position).getHeight() > 0 && lifeStream.getMedia().get(position).getWidth() > 0) {
            height = lifeStream.getMedia().get(position).getHeight() * width / lifeStream.getMedia().get(position).getWidth();
        }
        int layoutFlag;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            layoutFlag = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            layoutFlag = WindowManager.LayoutParams.TYPE_PHONE;
        }

        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                width,
                height,
                layoutFlag,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                        | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
                        | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                PixelFormat.TRANSLUCENT);

        //Specify the view mPosition
        params.gravity = Gravity.TOP | Gravity.START;
        params.x = 0;
        params.y = 0;

        mWindowManager.addView(mMediaLayout, params);

        final FloatingGesture listener = new FloatingGesture(lifeStream, position);
        final FloatingVideoGesture gesture = new FloatingVideoGesture(mWindowManager, mMediaLayout, listener);
        mMediaLayout.setOnTouchListener(gesture);
    }

    public void removeVideo() {
        if (mAudioManager != null) {
            mAudioManager.abandonAudioFocus(mOnAudioListener);
        }
        try {
            if (mMediaLayout != null) {
                mMediaLayout.onMediaPause();
                mWindowManager.removeView(mMediaLayout);
                mMediaLayout = null;
            }

        } catch (Exception ex) {
            Timber.e("removeVideo: " + ex.getMessage());
        }
    }

    public boolean isFloatingVideoPlaying() {
        Timber.e(hashCode() + " isFloatingVideoPlaying " + (mMediaLayout != null));
        return mMediaLayout != null;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mFloatingVideoBinder;
    }

    private Point findLinearOfTwoVector(float x1, float y1, float x2, float y2, float y3) {
        Point point = new Point();
        float b = (((y1 / x1) - (y2 / x2)) * (x1 * x2)) / ((-1 * x1) + x2);
        float a = (y1 - b) / x1;
        float x = (-b + y3) / a;
        point.x = (int) x;
        point.y = (int) y3;
        return point;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        removeVideo();
        stopSelf();
    }

    public interface OnVideoClickListener {
        void onVideoClick(int position, LifeStream lifeStream);

        void onFloatingVideoPlayingFinish();
    }

    public class FloatingVideoBinder extends Binder {
        private OnVideoClickListener mListener;

        public void setListener(OnVideoClickListener listener) {
            mListener = listener;
        }

        public boolean isFloating() {
            // Return this instance of FloatingVideoService so clients can call public methods
            return FloatingVideoService.this.isFloatingVideoPlaying();
        }

        public void removeVideo() {
            FloatingVideoService.this.removeVideo();
        }

        void onVideoClick(int position, LifeStream lifeStream) {
            if (mListener != null) {
                mListener.onVideoClick(position, lifeStream);
            }
        }

        void onFloatingVideoPlayingFinish() {
            if (mListener != null) {
                mListener.onFloatingVideoPlayingFinish();
            }
        }
    }

    private class FloatingGesture implements FloatingVideoGesture.OnGestureListener {
        private LifeStream mLifeStream;
        private int mPosition;

        FloatingGesture(LifeStream lifeStream, int position) {
            this.mLifeStream = lifeStream;
            this.mPosition = position;
        }

        @Override
        public void onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if (mMediaLayout == null) {
                return;
            }
            mMediaLayout.onMediaPause();
            mMediaLayout.setOnTouchListener(null);
            WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams) mMediaLayout.getLayoutParams();

            CoordinateValueAnimator animator = new CoordinateValueAnimator(CoordinateValueAnimator.Interpolator.LINEAR);
            animator.setAnimatorListener((x, y) -> {
                try {
                    if (x == animator.getEndX() && y == animator.getEndY()) {
                        removeVideo();
                        mFloatingVideoBinder.onFloatingVideoPlayingFinish();
                        stopSelf();
                    } else {
                        final WindowManager.LayoutParams param = (WindowManager.LayoutParams) mMediaLayout.getLayoutParams();
                        param.x = x;
                        param.y = y;
                        mWindowManager.updateViewLayout(mMediaLayout, param);
                        int alpha = x * 100 / animator.getEndX();
                        mMediaLayout.setAlpha(Math.abs(100 - alpha));
                    }
                } catch (Exception e) {
                    animator.cancel();
                }
            });

            if (e1.getY() < e2.getY()) {
                Point point = findLinearOfTwoVector(e1.getX(), e1.getY(), e2.getX(), e2.getY(), mScreen.y);
                animator.start(layoutParams.x, point.x, layoutParams.y, point.y);
            } else {
                Point point = findLinearOfTwoVector(e1.getX(), e1.getY(), e2.getX(), e2.getY(), 0);
                if (e1.getX() < e2.getX() && point.x < e2.getRawX()) {
                    point.x = point.x * 2;
                }
                animator.start(layoutParams.x, point.x, layoutParams.y, point.y);
            }
        }

        @Override
        public void onSingleTapUp(MotionEvent motionEvent) {
            if (mMediaLayout == null) {
                stopSelf();
                return;
            } else {
                mMediaLayout.onMediaPause();
            }
            mLifeStream.getMedia().get(mPosition).setTime(mMediaLayout.getTime());
            removeVideo();
            stopSelf();
            mFloatingVideoBinder.onVideoClick(mPosition, mLifeStream);
        }
    }
}
