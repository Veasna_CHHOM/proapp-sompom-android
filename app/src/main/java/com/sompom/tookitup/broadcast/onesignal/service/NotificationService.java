package com.sompom.tookitup.broadcast.onesignal.service;

import android.app.IntentService;
import android.content.Intent;

import com.sompom.tookitup.broadcast.onesignal.ActivityBroadCast;

/**
 * Created by he.rotha on 5/2/16.
 */
public class NotificationService extends IntentService {
    public static final String TAG = NotificationService.class.getName();

    public NotificationService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(ActivityBroadCast.OTHER);
        broadcastIntent.putExtras(intent.getExtras());
        sendBroadcast(broadcastIntent);
    }
}