package com.sompom.tookitup.broadcast.upload;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.sompom.tookitup.R;
import com.sompom.tookitup.helper.upload.FileType;
import com.sompom.tookitup.helper.upload.Orientation;
import com.sompom.tookitup.intent.newintent.HomeIntent;
import com.sompom.tookitup.intent.newintent.TimelineDetailIntent;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.model.LifeStream;
import com.sompom.tookitup.model.NotificationChannelSetting;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by imac on 8/16/17.
 */

public class LifeStreamUploadService extends UploadService {
    private final UploadBinder mBinder = new UploadBinder();
    private final List<LifeStream> mLifeStreams = new ArrayList<>();
    @Inject
    public ApiService mApiService;

    public static Intent getIntent(Context activity, LifeStream lifeStream) {
        Intent intent = new Intent(activity, LifeStreamUploadService.class);
        intent.putExtra(SharedPrefUtils.PRODUCT, lifeStream);
        return intent;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return START_STICKY;
        }
        if (mApiService == null) {
            getControllerComponent().inject(this);
        }
        LifeStream lifeStream = intent.getParcelableExtra(SharedPrefUtils.PRODUCT);
        if (TextUtils.isEmpty(lifeStream.getId())) {
            lifeStream.setId(String.valueOf(System.currentTimeMillis()));
        }
        mLifeStreams.add(lifeStream);
        if (mLifeStreams.size() == 1) {
            cancelQueueNotification();
            startUploading(convertTimelineToUploadMedia(lifeStream), NotificationChannelSetting.UploadingLifeStream);
        } else {
            Intent homeIntent = new HomeIntent(getApplicationContext());
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),
                    (int) System.currentTimeMillis(),
                    homeIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            pushQueueNotification(getString(R.string.product_form_notification_queue), pendingIntent, NotificationChannelSetting.UploadingLifeStream);
        }

        return START_STICKY;
    }

    private UploadMedia convertTimelineToUploadMedia(LifeStream lifeStream) {
        if (lifeStream.getMedia() == null || lifeStream.getMedia().isEmpty()) {
            return new UploadMedia(lifeStream.getId(), getString(R.string.app_name), new String[]{}, new FileType[]{});
        }
        String[] paths = new String[lifeStream.getMedia().size()];
        FileType[] fileTypes = new FileType[lifeStream.getMedia().size()];
        Orientation[] orientations = new Orientation[lifeStream.getMedia().size()];

        for (int i = 0; i < paths.length; i++) {
            paths[i] = lifeStream.getMedia().get(i).getUrl();
            fileTypes[i] = lifeStream.getMedia().get(i).getUploadType();

            if (lifeStream.getMedia().get(i).getHeight() >
                    lifeStream.getMedia().get(i).getWidth()) {
                orientations[i] = Orientation.Portrait;
            } else {
                orientations[i] = Orientation.Landscape;
            }
        }
        return new UploadMedia(lifeStream.getId(), getString(R.string.app_name), paths, fileTypes, orientations);
    }

    @Override
    void onUploadSuccess(final UploadMedia uploadMedia) {
        for (final LifeStream lifeStream : mLifeStreams) {
            if (lifeStream.getId().equals(uploadMedia.getId())) {
                for (int i = 0; i < uploadMedia.getFilePath().length; i++) {
                    lifeStream.getMedia().get(i).setUrl(uploadMedia.getFilePath()[i]);
                    lifeStream.getMedia().get(i).setThumbnail(uploadMedia.getThumbnail()[i]);
                }

                Observable<Response<LifeStream>> callback = mApiService.postWallStreet(new LifeStream(lifeStream));
                try {
                    ResponseObserverHelper<Response<LifeStream>> helper = new ResponseObserverHelper<>(getApplicationContext(), callback);
                    helper.execute(new OnCallbackListener<Response<LifeStream>>() {
                        @Override
                        public void onFail(ErrorThrowable ex) {
                            Timber.e("post fail: %s", ex.toString());
                            generateFailAction(lifeStream);
                        }

                        @Override
                        public void onComplete(Response<LifeStream> value) {
                            mBinder.onPostComplete(true);

                            for (int i = mLifeStreams.size() - 1; i >= 0; i--) {
                                if (TextUtils.equals(mLifeStreams.get(i).getId(), uploadMedia.getId())) {
                                    mLifeStreams.remove(i);
                                    break;
                                }
                            }

                            LifeStream lifeStream1 = value.body();
                            if (lifeStream1 != null) {
                                Intent intent = new TimelineDetailIntent(getApplicationContext(), lifeStream1.getId());
                                PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),
                                        (int) System.currentTimeMillis(),
                                        intent,
                                        PendingIntent.FLAG_UPDATE_CURRENT);
                                pushSingleNotification(getString(R.string.life_stream_posted), pendingIntent, NotificationChannelSetting.UploadingLifeStream);
                                stopForeground(true);
                                stopSelf();
                            } else {
                                generateFailAction(lifeStream);
                            }

                        }
                    });
                } catch (Exception e) {
                    Timber.e("error post to server: %s", e.toString());
                }
                break;
            }
        }
    }

    @Override
    void onUploadFail(UploadMedia uploadMedia) {
        Timber.e("onUploadFail");
        LifeStream lifeStream = null;
        for (int i = mLifeStreams.size() - 1; i >= 0; i--) {
            if (mLifeStreams.get(i).getId().equals(uploadMedia.getId())) {
                lifeStream = mLifeStreams.get(i);
                mLifeStreams.remove(i);
                break;
            }
        }
        generateFailAction(lifeStream);
    }

    private void generateFailAction(LifeStream lifeStream) {
        if (lifeStream != null) {
            mLifeStreams.remove(lifeStream);

            Intent intent = LifeStreamUploadService.getIntent(getApplicationContext(), lifeStream);
            PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(),
                    (int) System.currentTimeMillis(),
                    intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            pushSingleNotification(getString(R.string.cannot_post_life_stream_tap_retry), pendingIntent, NotificationChannelSetting.UploadingLifeStream);
        }
        checkNextUpload();
    }

    private void checkNextUpload() {
        if (mLifeStreams.size() == 1) {
            cancelQueueNotification();
        }
        if (!mLifeStreams.isEmpty()) {
            startUploading(convertTimelineToUploadMedia(mLifeStreams.get(0)), NotificationChannelSetting.UploadingLifeStream);
        } else {
            stopForeground(true);
            stopSelf();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public interface OnPostListener {
        void onPostComplete(boolean isSuccess);
    }

    public class UploadBinder extends Binder {
        private OnPostListener mListener;

        public void setListener(OnPostListener listener) {
            mListener = listener;
        }

        public UploadService getService() {
            // Return this instance of LocalService so clients can call public methods
            return LifeStreamUploadService.this;
        }

        public void onPostComplete(boolean isSuccess) {
            if (mListener != null) {
                mListener.onPostComplete(isSuccess);
            }
        }
    }
}
