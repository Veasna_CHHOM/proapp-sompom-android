package com.sompom.tookitup.helper;

import android.content.Context;
import android.view.OrientationEventListener;

/**
 * Created by He Rotha on 1/17/18.
 */

public class ScreenOrientationHelper extends OrientationEventListener {
    public static final int ORIENTATION_PORTRAIT = 0;
    public static final int ORIENTATION_LANDSCAPE_REVERSE = 1;
    public static final int ORIENTATION_PORTRAIT_REVERSE = 2;
    public static final int ORIENTATION_LANDSCAPE = 3;
    private int mLastOrientation;
    private Callback mCallback;

    public ScreenOrientationHelper(Context context) {
        super(context);
    }

    @Override
    public void onOrientationChanged(int orientation) {
        if (orientation < 0) {
            return; // Flip screen, Not take account
        }

        int curOrientation;

        if (orientation <= 70) {
            curOrientation = ORIENTATION_PORTRAIT;
        } else if (orientation <= 135) {
            curOrientation = ORIENTATION_LANDSCAPE_REVERSE;
        } else if (orientation <= 225) {
            curOrientation = ORIENTATION_PORTRAIT_REVERSE;
        } else if (orientation <= 290) {
            curOrientation = ORIENTATION_LANDSCAPE;
        } else {
            curOrientation = ORIENTATION_PORTRAIT;
        }
        if (curOrientation != mLastOrientation) {
            if (mCallback != null) {
                mCallback.onChanged(curOrientation);
            }
            mLastOrientation = curOrientation;
        }
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    public interface Callback {
        void onChanged(int orientation);
    }
}
