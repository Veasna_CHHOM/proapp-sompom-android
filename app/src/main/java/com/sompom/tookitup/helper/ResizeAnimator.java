package com.sompom.tookitup.helper;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.animation.LinearInterpolator;

/**
 * Created by imac on 5/2/17.
 */

public class ResizeAnimator {
    private int mStartDimen;
    private int mEndDimen;
    private View mViewToResize;
    private View mToggleView;
    private AnimatorListener mAnimatorListener;

    public ResizeAnimator(int startDimen, int endDimen, View viewToResize) {
        mStartDimen = startDimen;
        mEndDimen = endDimen;
        mViewToResize = viewToResize;
    }

    public void startAnimation() {
        ValueAnimator rootAnimator = ValueAnimator.ofInt(mStartDimen, mEndDimen);
        rootAnimator.setDuration(200);
        rootAnimator.setInterpolator(new LinearInterpolator());
        rootAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                mViewToResize.getLayoutParams().height = (Integer) animation.getAnimatedValue();
                mViewToResize.requestLayout();
                if (mToggleView != null) {
                    mToggleView.getLayoutParams().height = mEndDimen - (Integer) animation.getAnimatedValue();
                    mToggleView.requestLayout();
                }
            }
        });

        rootAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                if (mAnimatorListener != null) {
                    mAnimatorListener.onStart();
                }
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if (mAnimatorListener != null) {
                    mAnimatorListener.onEnd();
                }
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        rootAnimator.start();
    }

    public void setToggleView(View view) {
        if (view == mViewToResize) {
            return;
        }
        mToggleView = view;
    }

    public void setAnimatorListener(AnimatorListener animatorListener) {
        mAnimatorListener = animatorListener;
    }

    public interface AnimatorListener {
        void onStart();

        void onEnd();
    }
}
