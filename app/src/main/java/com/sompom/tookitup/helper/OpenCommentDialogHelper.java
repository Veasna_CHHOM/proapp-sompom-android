package com.sompom.tookitup.helper;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;

import com.sompom.tookitup.adapter.newadapter.TimelineAdapter;
import com.sompom.tookitup.listener.OnCompleteListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.WallStreetAdaptive;
import com.sompom.tookitup.model.result.Comment;
import com.sompom.tookitup.model.result.ContentStat;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.dialog.CommentDialog;

/**
 * Created by nuonveyo
 * on 2/18/19.
 */

public class OpenCommentDialogHelper {
    private final Activity mActivity;
    private boolean mIsShow;

    public OpenCommentDialogHelper(Activity activity) {
        mActivity = activity;
    }

    public void show(Adaptive adaptive,
                     RecyclerView.Adapter adapter) {
        show(adaptive, result -> {
            if (adapter instanceof TimelineAdapter) {
                ((TimelineAdapter) adapter).update(adaptive);
            }
        });
    }

    public void show(Adaptive adaptive,
                     OnCompleteListener<Adaptive> listener) {
        if (!mIsShow && mActivity instanceof AbsBaseActivity) {
            mIsShow = true;
            CommentDialog commentDialog = CommentDialog.newInstance(adaptive.getId());
            commentDialog.setOnDismissDialogListener(() -> {
                mIsShow = false;
                int addNum = commentDialog.getNumberOfAddComment();
                if (addNum > 0 &&
                        adaptive instanceof WallStreetAdaptive &&
                        ((WallStreetAdaptive) adaptive).getContentStat() != null) {
                    ContentStat stat = ((WallStreetAdaptive) adaptive).getContentStat();
                    long value = stat.getTotalComments() + addNum;
                    if (value >= 0) {
                        stat.setTotalComments(value);

                        listener.onComplete(adaptive);
                    }
                }
            });
            commentDialog.show(((AbsBaseActivity) mActivity).getSupportFragmentManager(), CommentDialog.TAG);
        }
    }

    public void show(WallStreetAdaptive adaptive,
                     Comment comment,
                     RecyclerView.Adapter adapter,
                     CommentDialog.OnCloseReplyCommentFragmentListener listener) {
        if (!mIsShow && mActivity instanceof AbsBaseActivity) {
            mIsShow = true;
            CommentDialog commentDialog = CommentDialog.newInstance(adaptive.getId(), comment);
            commentDialog.setOnNotifyCommentItemChangeListener(listener);
            commentDialog.setOnDismissDialogListener(() -> {
                mIsShow = false;
                int addNum = commentDialog.getNumberOfAddComment();
                if (addNum > 0 &&
                        adaptive.getContentStat() != null) {
                    ContentStat stat = adaptive.getContentStat();
                    long value = stat.getTotalComments() - addNum;
                    if (value >= 0) {
                        stat.setTotalComments(value);
                        if (adaptive instanceof Adaptive && adapter instanceof TimelineAdapter) {
                            ((TimelineAdapter) adapter).update((Adaptive) adaptive);
                        }
                    }
                }
            });
            commentDialog.show(((AbsBaseActivity) mActivity).getSupportFragmentManager(), CommentDialog.TAG);
        }
    }

}
