package com.sompom.tookitup.helper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.sompom.tookitup.R;
import com.sompom.tookitup.database.CurrencyDb;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.Locale;

public class LocaleManager {

    public static Context setLocale(Context c) {
        return updateResources(c, getAppLanguage(c));
    }

    public static Context setNewLocale(Context c, String language) {
        persistLanguage(c, language);
        return updateResources(c, language);
    }

    public static void applyChangeLanguage(Context context, String lang) {
        setNewLocale(context, lang);
        updateLanguage((Application) context.getApplicationContext(), lang);
    }

    @SuppressLint("ApplySharedPref")
    private static void persistLanguage(Context c, String language) {
        SharedPrefUtils.setLanguage(language, c);
    }

    private static Context updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        if (Build.VERSION.SDK_INT >= 17) {
            config.setLocale(locale);
            context = context.createConfigurationContext(config);
        } else {
            config.locale = locale;
            res.updateConfiguration(config, res.getDisplayMetrics());
        }
        return context;
    }

    public static Locale getLocale(Resources res) {
        Configuration config = res.getConfiguration();
        return Build.VERSION.SDK_INT >= 24 ? config.getLocales().get(0) : config.locale;
    }

    public static boolean isChangeLanguage(Context context, String oldLocal) {
        return !TextUtils.equals(oldLocal, SharedPrefUtils.getLanguage(context));
    }

    public static int selectLanguagePosition(Context context) {
        int languageSelectedIndex = 0;
        final String[] iso3SupportLanguage = context.getResources().getStringArray(R.array.support_lang_iso3);
        for (int i = 0; i < iso3SupportLanguage.length; i++) {
            if (iso3SupportLanguage[i].equals(SharedPrefUtils.getLanguage(context))) {
                languageSelectedIndex = i;
                break;
            }
        }
        return languageSelectedIndex;
    }

    public static void updateLanguage(Application context, String localString) {
        Configuration conf = context.getResources().getConfiguration();
        conf.locale = new Locale(localString);
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(Activity.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);
        context.getResources().updateConfiguration(conf, metrics);
        Resources resources = new Resources(context.getAssets(), metrics, conf);
        resources.updateConfiguration(conf, metrics);
        context.getResources().updateConfiguration(conf, metrics);


        Configuration configuration = context.getBaseContext().getResources().getConfiguration();
        configuration.locale = conf.locale;
        context.getBaseContext().getResources()
                .updateConfiguration(configuration, context.getResources().getDisplayMetrics());

        Configuration systemConf = Resources.getSystem().getConfiguration();
        systemConf.locale = conf.locale;
        Resources.getSystem().updateConfiguration(systemConf, Resources.getSystem().getDisplayMetrics());

        Locale.setDefault(conf.locale);
//        update(context, localString);
    }

    private static String getAppLanguage(Context context) {
        String ios3LocalLanguage = SharedPrefUtils.getLanguage(context);
        if (TextUtils.isEmpty(ios3LocalLanguage)) {
            Locale locale = CurrencyDb.getLocale(context);
            ios3LocalLanguage = locale.getISO3Language();
            String[] supportLanguages = context.getResources().getStringArray(R.array.support_lang_iso3);

            boolean isContain = false;
            for (String supportLanguage : supportLanguages) {
                if (supportLanguage.equals(ios3LocalLanguage)) {
                    isContain = true;
                    break;
                }
            }

            if (!isContain) {
                ios3LocalLanguage = supportLanguages[0];
            }
        }
        return ios3LocalLanguage;
    }
}