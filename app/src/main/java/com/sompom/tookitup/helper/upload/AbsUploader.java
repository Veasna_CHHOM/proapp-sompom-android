package com.sompom.tookitup.helper.upload;

import android.content.Context;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.sompom.tookitup.R;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Map;

import timber.log.Timber;

/**
 * Created by imac on 4/1/16.
 */
public abstract class AbsUploader {
    private Context mContext;
    private String mImagePath;
    private String mFileName;
    private String mPreset;
    private String mTag;

    /**
     * Constructor.
     *
     * @param context   :
     * @param imagePath : String the path of the image file in phone, where user want to us to upload.
     */
    public AbsUploader(final Context context,
                       final String imagePath,
                       final UploadType uploadType) {
        mContext = context;
        mImagePath = imagePath;
        if (uploadType == UploadType.Cover) {
            mPreset = mContext.getString(R.string.profile_cover_preset);
            mTag = mContext.getString(R.string.profile_cover_tag);
            mFileName = context.getString(R.string.upload_coverUserName_key) + "_" + SharedPrefUtils.getUserId(context) + "_" + System.currentTimeMillis();
        } else {
            mPreset = mContext.getString(R.string.profile_preset);
            mTag = mContext.getString(R.string.profile_tag);
            mFileName = context.getString(R.string.upload_profileUserName_key) + "_" + SharedPrefUtils.getUserId(context) + "_" + System.currentTimeMillis();
        }
        Timber.e(mPreset + ", " + mTag + ", " + mFileName + ", " + mImagePath);
    }

    public static void cancelUpload(String requestId) {
        MediaManager.get().cancelRequest(requestId);
    }

    public Context getContext() {
        return mContext;
    }

    /**
     * Call for Process upload image.
     */
    public String doUpload() {
        return MediaManager.get()
                .upload(new PayloadBuilder(FileType.IMAGE, mImagePath).build())
                .option("resource_type", "auto")
                .option("public_id", mFileName)
                .option("upload_preset", mPreset)
                .option("tags", mTag)
                .callback(new UploadCallback() {
                    @Override
                    public void onStart(String requestId) {
                        Timber.v("onServiceStart: " + requestId + "  " + MediaManager.get().hashCode());
                    }

                    @Override
                    public void onProgress(String requestId, long bytes, long totalBytes) {
                        Timber.v("onProgress: " + requestId + "  " + MediaManager.get().hashCode());
                    }

                    @Override
                    public void onSuccess(String requestId, Map resultData) {
                        try {
                            Timber.v("onSuccess: %s", resultData.toString());
                            ArrayList eager = (ArrayList) resultData.get("eager");
                            //when preset has transform, eager will be not null,
                            String thumb = null;
                            if (eager != null) {
                                thumb = (String) ((Map) eager.get(0)).get("url");
                            }
                            //otherwise, preset doesn't have transform, so we origin url
                            String url = (String) resultData.get("url");
                            onUploadSucceeded(url, thumb);
                        } catch (Exception ex) {
                            onUploadFail(requestId, ex.toString());
                        }
                    }

                    @Override
                    public void onError(String requestId, ErrorInfo error) {
                        Timber.e("onError: " + requestId + ", error:" + error.getDescription());
                        onUploadFail(requestId, error.getDescription());
                    }

                    @Override
                    public void onReschedule(String requestId, ErrorInfo error) {
                        MediaManager.get().cancelRequest(requestId);
                        onUploadFail(requestId, error.getDescription());
                        Timber.e("onReschedule: " + requestId + ", error:" + error);
                    }
                })
                .dispatch();
    }

    public abstract void onUploadSucceeded(String imageUrl, String thumb);

    /**
     * Call when upload is failed.
     *
     * @param ex : Exception that occur during uploading.
     */
    public abstract void onUploadFail(String id, String ex);

    public enum UploadType {
        Cover, Profile
    }
}
