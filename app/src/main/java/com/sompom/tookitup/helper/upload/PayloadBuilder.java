package com.sompom.tookitup.helper.upload;

import com.cloudinary.android.payload.FilePayload;
import com.cloudinary.android.payload.Payload;

/**
 * Created by He Rotha on 9/4/18.
 */
public class PayloadBuilder {
    private FileType mFileType;
    private String mPath;

    public PayloadBuilder(FileType fileType, String path) {
        mFileType = fileType;
        mPath = path;
    }

    public Payload build() {
        if (mFileType == FileType.IMAGE) {
            //check if file extension is GIF, it doesn't compress image
            if (!mPath.endsWith("gif") && !mPath.endsWith("GIF")) {
                return new CompressPayload(mPath);
            }
        }
        return new FilePayload(mPath);
    }
}
