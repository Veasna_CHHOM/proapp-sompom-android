package com.sompom.tookitup.helper;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.Task;
import com.sompom.baseactivity.PermissionCheckHelper;

import timber.log.Timber;


/**
 * Created by he.rotha on 6/10/16.
 */
public class RequestLocationManager implements PermissionCheckHelper.OnPermissionCallback {
    private static final int mRequestCode = 0X100;
    LocationSettingsRequest.Builder mBuilder;
    PendingResult<LocationSettingsResult> pendingResult;
    private AppCompatActivity mAppCompatActivity;
    private PermissionCheckHelper mLocationPermission;
    private LocationManager mLocationManager;
    private double mLongitude;
    private double mLatitude;
    private RequestLocationManager.OnRequestLocationCallback mListener;

    public RequestLocationManager(AppCompatActivity appCompatActivity) {
        mAppCompatActivity = appCompatActivity;
    }

    public void execute() {
        if (mLocationPermission == null) {
            mLocationPermission = new PermissionCheckHelper(mAppCompatActivity,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    mRequestCode);
            mLocationPermission.setCallback(this);
        }
        mLocationPermission.execute();
    }

    public AppCompatActivity getActivity() {
        return mAppCompatActivity;
    }

    @Override
    public void onPermissionGranted() {
        if (ActivityCompat.checkSelfPermission(mAppCompatActivity,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            mListener.onLocationPermissionDeny(false);
            return;
        }
        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mFusedLocationClient
                .getLastLocation()
                .addOnCompleteListener(getActivity(), task -> {
                    try {
                        Location location = task.getResult();
                        if (location != null) {
                            if (System.currentTimeMillis() - location.getTime() < 60 * 60 * 1000) {
                                mLatitude = location.getLatitude();
                                mLongitude = location.getLongitude();
                                mListener.onLocationSuccess(mLatitude, mLongitude);
                            } else {
                                showPopup();
                            }
                        } else {
                            showPopup();
                        }
                    } catch (Exception ex) {
                        showPopup();
                    }
                });
        Timber.e("requestOldLocation");
    }

    @Override
    public void onPermissionDenied(String[] grantedPermission,
                                   String[] deniedPermission,
                                   boolean isUserPressNeverAskAgain) {
        mListener.onLocationPermissionDeny(isUserPressNeverAskAgain);
    }

    private void requestNewLocation() {
        Timber.e("requestNewLocation");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) mAppCompatActivity.getSystemService(Context.LOCATION_SERVICE);
        }

        final LocationListener locationListener = new LocationListener() {

            public void onLocationChanged(Location location) {
                Timber.e("onLocationChanged");
                mLongitude = location.getLongitude();
                mLatitude = location.getLatitude();
                if (mAppCompatActivity != null) {
                    if (mLatitude == 0 && mLongitude == 0) {
                        onPermissionGranted();
                        return;
                    } else {
                        mListener.onLocationSuccess(mLatitude, mLongitude);
                    }

                    if (ActivityCompat.checkSelfPermission(mAppCompatActivity, Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED) {
                        mListener.onLocationPermissionDeny(false);
                        return;
                    }
                    mLocationManager.removeUpdates(this);
                }
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
                Timber.e("onStatusChanged");
            }

            public void onProviderEnabled(String provider) {
                Timber.e("onProviderEnabled");
            }

            public void onProviderDisabled(String provider) {
                Timber.e("onProviderDisabled");
            }
        };

        final long minTime = 1000;
        float minMeter = 0;

        final Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);

        String provider = mLocationManager.getBestProvider(criteria, false);
        Timber.e("provider: " + provider);
        mLocationManager.requestLocationUpdates(provider,
                minTime, minMeter, locationListener);

        if (TextUtils.isEmpty(provider)) {
            mListener.onLocationDisable();
        }
    }


    private void showPopup() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(5000);
        mBuilder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        Task<LocationSettingsResponse> result =
                LocationServices.getSettingsClient(getActivity()).checkLocationSettings(mBuilder.build());
        result.addOnCompleteListener(task -> {
            try {
                LocationSettingsResponse response = task.getResult(ApiException.class);
                requestNewLocation();
                // All location settings are satisfied. The client can initialize location
                // requests here.
            } catch (ApiException exception) {

                switch (exception.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the
                        // user a dialog.
                        try {
                            // Cast to a resolvable exception.
                            ResolvableApiException resolvable = (ResolvableApiException) exception;
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            resolvable.startResolutionForResult(getActivity(), mRequestCode);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        } catch (ClassCastException e) {
                            // Ignore, should be an impossible error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                    default:
                        mListener.onLocationDisable();
                        break;
                }
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case mRequestCode:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        // All required changes were successfully made
                        requestNewLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        // The user was asked to change settings, but chose not to
                        mListener.onLocationDisable();
                        break;
                    default:
                        break;
                }
                break;
        }
    }


    public void setOnRequestLocationCallback(RequestLocationManager.OnRequestLocationCallback listener) {
        mListener = listener;
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (mLocationPermission != null) {
            mLocationPermission.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public interface OnRequestLocationCallback {
        void onLocationSuccess(double latitude, double longtitude);

        void onLocationDisable();

        void onLocationPermissionDeny(boolean isUserPressNeverAskAgain);
    }
}
