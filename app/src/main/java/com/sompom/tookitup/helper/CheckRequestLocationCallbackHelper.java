package com.sompom.tookitup.helper;

import android.app.Activity;
import android.support.annotation.StringRes;

import com.sompom.tookitup.R;
import com.sompom.tookitup.model.Locations;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.dialog.MessageDialog;
import com.sompom.tookitup.utils.IntentUtil;

import timber.log.Timber;

/**
 * Created by nuonveyo
 * on 1/31/19.
 */

public class CheckRequestLocationCallbackHelper implements SingleRequestLocation.Callback {
    private final Activity mContext;
    private final Callback mCallback;

    public CheckRequestLocationCallbackHelper(Activity activity, Callback callback) {
        mContext = activity;
        mCallback = callback;
    }

    @Override
    public void onComplete(Locations location, boolean isNeverAskAgain) {
        Timber.e("onComplete: %s", isNeverAskAgain);
        if (isNeverAskAgain) {
            MessageDialog dialog = new MessageDialog();
            dialog.setTitle(getString(R.string.sign_up_access_permission_title));
            dialog.setMessage(getString(R.string.sign_up_access_location_description));
            dialog.setLeftText(getString(R.string.sign_up_access_app_setting_button), view -> IntentUtil.openAppSetting(mContext));
            dialog.setRightText(getString(R.string.edit_profile_no), null);
            dialog.show(((AbsBaseActivity) mContext).getSupportFragmentManager(), MessageDialog.TAG);
        }
        mCallback.onComplete(location);
    }

    private String getString(@StringRes int id) {
        return mContext.getString(id);
    }

    public interface Callback {
        void onComplete(Locations locations);
    }
}
