package com.sompom.tookitup.helper.upload;

import android.content.Context;
import android.text.TextUtils;

import com.cloudinary.android.MediaManager;
import com.sompom.tookitup.R;
import com.sompom.tookitup.model.emun.MediaType;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Map;

import timber.log.Timber;

/**
 * Created by imac on 4/1/16.
 */
public class AbsLifeStreamUploader extends AbsMyUploader {
    private Context mContext;
    private String mImagePath;
    private String mFileName;
    private String mPreset;
    private String mTag;
    private FileType mFileType;

    /**
     * Constructor.
     *
     * @param context   :
     * @param imagePath : String the path of the image file in phone, where user want to us to upload.
     */
    public AbsLifeStreamUploader(final Context context,
                                 final String imagePath,
                                 final FileType fileType,
                                 final Orientation orientation) {
        mContext = context;
        mImagePath = imagePath;
        mFileType = fileType;
        if (fileType == FileType.VIDEO) {
            if (orientation == Orientation.Portrait) {
                mPreset = mContext.getString(R.string.tookitup_video_port_preset);
            } else {
                mPreset = mContext.getString(R.string.tookitup_video_land_preset);
            }
        } else {
            mPreset = mContext.getString(R.string.tookitup_image_preset);
        }
        mTag = mContext.getString(R.string.life_stream_tag);
        mFileName = "lifestream_" + SharedPrefUtils.getUserId(context) + "_" + System.currentTimeMillis();
        Timber.e(mPreset + ", " + mTag + ", " + mFileName + ", " + mImagePath);
    }

    public static void cancelUpload(String requestId) {
        MediaManager.get().cancelRequest(requestId);
    }

    /**
     * Call for Process upload image.
     */
    @Override
    public String doUpload() {
        return MediaManager.get()
                .upload(new PayloadBuilder(mFileType, mImagePath).build())
                .option("resource_type", "auto")
                .option("public_id", mFileName)
                .option("upload_preset", mPreset)
                .option("tags", mTag)
                .callback(this)
                .dispatch();
    }

    @Override
    public void onSuccess(String requestId, Map resultData) {
        try {
            Timber.v("onSuccess: %s", resultData.toString());
            ArrayList eager = (ArrayList) resultData.get("eager");
            String url;
            //when preset has transform, eager will be not null,
            //otherwise, preset doesn't have transform, so we origin url
            if (eager != null) {
                url = (String) ((Map) eager.get(0)).get("url");
            } else {
                url = (String) resultData.get("url");
            }
            String thumb = null;
            String resourceType = (String) resultData.get("resource_type");
            Timber.v("resourceType: %s", resourceType);
            if (TextUtils.equals(resourceType, MediaType.VIDEO.getValue())) {
                String publicId = (String) resultData.get("public_id");
                thumb = MediaManager.get().url().resourceType("video").generate(publicId + ".jpg");
                Timber.v("thumb: %s", thumb);
            }
            mUploadListener.onUploadSucceeded(requestId, url, thumb);
        } catch (Exception ex) {
            mUploadListener.onUploadFail(requestId, ex);
        }
    }

}
