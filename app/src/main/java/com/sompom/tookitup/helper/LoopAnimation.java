package com.sompom.tookitup.helper;

import android.os.Handler;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by He Rotha on 9/11/17.
 */

public class LoopAnimation {
    private final Timer mSwipeTimer;
    private final Runnable mRunnable;
    private int mPosition = 0;
    private OnLoopAnimation mAnimation;

    public LoopAnimation(final int maxPosition) {
        final Handler handler = new Handler();
        mSwipeTimer = new Timer();
        mRunnable = () -> {
            if (mAnimation != null) {
                if (mPosition >= maxPosition) {
                    mPosition = 0;
                }
                mAnimation.onLoop(mPosition);
                mPosition++;
            }
        };
        mSwipeTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(mRunnable);
            }
        }, 100, 5000);
    }

    public void cancel() {
        mSwipeTimer.cancel();
    }

    public void setAnimation(OnLoopAnimation animation) {
        mAnimation = animation;
    }

    public interface OnLoopAnimation {
        void onLoop(int position);
    }
}
