package com.sompom.tookitup.helper;

import com.desmond.squarecamera.utils.Keys;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.sompom.tookitup.model.notification.NotificationAdaptive;
import com.sompom.tookitup.model.notification.NotificationComment;
import com.sompom.tookitup.model.notification.NotificationProduct;
import com.sompom.tookitup.model.notification.NotificationTimeline;
import com.sompom.tookitup.model.notification.NotificationUser;
import com.sompom.tookitup.model.result.LoadMoreWrapper;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by He Rotha on 8/30/17.
 */

public class NotificationAdapterDeserializer implements JsonDeserializer<LoadMoreWrapper<NotificationAdaptive>> {
    @Override
    public LoadMoreWrapper<NotificationAdaptive> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        LoadMoreWrapper<NotificationAdaptive> loadMoreWrapper = new LoadMoreWrapper<>();

        JsonObject jsonObject = (JsonObject) json;

        //deserialize Next
        JsonElement obj = jsonObject.get(LoadMoreWrapper.NEXT);
        if (obj != null && !obj.isJsonNull()) {
            loadMoreWrapper.setNextPage(obj.getAsString());
        }

        //deserialize List
        obj = jsonObject.get(LoadMoreWrapper.LIST);
        List<NotificationAdaptive> list = new ArrayList<>();
        for (JsonElement jsonElement : obj.getAsJsonArray()) {
            NotificationAdaptive adaptive = null;

            obj = jsonElement.getAsJsonObject();
            JsonElement ob = ((JsonObject) obj).get(Keys.OBJECT);
            JsonElement element = ob.getAsJsonArray().get(0);
            String type = element.getAsJsonObject().get(Keys.OBJECT_TYPE).getAsString();

            if (type.equalsIgnoreCase(Keys.OBJECT_POST)) {
                adaptive = context.deserialize(obj, NotificationTimeline.class);
            } else if (type.equalsIgnoreCase(Keys.OBJECT_USER)) {
                adaptive = context.deserialize(obj, NotificationUser.class);
            } else if (type.equalsIgnoreCase(Keys.OBJECT_PRODUCT)) {
                adaptive = context.deserialize(obj, NotificationProduct.class);
            } else if (type.equalsIgnoreCase(Keys.OBJECT_COMMENT)) {
                adaptive = context.deserialize(obj, NotificationComment.class);
            }

            if (adaptive != null) {
                list.add(adaptive);
            }
        }
        loadMoreWrapper.setData(list);
        return loadMoreWrapper;
    }

}
