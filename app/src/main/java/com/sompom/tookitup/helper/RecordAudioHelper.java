package com.sompom.tookitup.helper;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.media.MediaRecorder;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.desmond.squarecamera.utils.FormatTime;
import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.SoundEffectService;
import com.sompom.tookitup.broadcast.chat.PlayAudioService;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.AbsSoundControllerActivity;
import com.sompom.tookitup.utils.MediaUtil;
import com.sompom.tookitup.widget.chat.CustomVoiceRippleView;

import java.io.File;

import info.kimjihyok.ripplelibrary.Rate;
import info.kimjihyok.ripplelibrary.renderer.CircleRippleRenderer;
import info.kimjihyok.ripplelibrary.renderer.Renderer;
import timber.log.Timber;

/**
 * Created by nuonveyo on 9/18/18.
 */

public class RecordAudioHelper {
    private static final int ONE_SECOND = 1000;
    private static final double RIPPLE_RATIO = 1.45;

    private final Activity mContext;
    private final CustomVoiceRippleView mVoiceRipple;
    private final OnCallback mOnClickListener;
    private final TextView mTextView;
    private final View mSlideView;
    private final AudioGestureListener mAudioGestureListener;

    private Type mType;
    private File mRecordFile;
    private boolean mIsActionRecorded;

    public RecordAudioHelper(Activity context,
                             CustomVoiceRippleView voiceRippleView,
                             TextView textView,
                             View slideView,
                             OnCallback onClickListener) {
        mContext = context;
        mVoiceRipple = voiceRippleView;
        mTextView = textView;
        mSlideView = slideView;
        mOnClickListener = onClickListener;
        mVoiceRipple.setRecordingListener(new CustomVoiceRippleView.OnRecordingListener() {
            @Override
            public void onRecordingStopped(File fileRecorded) {
                if (MediaUtil.getAudioDuration(fileRecorded.getAbsolutePath()) < ONE_SECOND) {
                    Timber.e("delete file record: %s", fileRecorded.delete());
                    mOnClickListener.onStopRecord(null);
                } else {
                    mRecordFile = fileRecorded;
                    if (mOnClickListener != null) {
                        mOnClickListener.onStopRecord(mRecordFile);
                    }
                }
            }

            @Override
            public void onRecordingStarted() {
                mOnClickListener.onStartRecord();
            }
        });

        // set view related settings for ripple view
        mVoiceRipple.setRippleSampleRate(Rate.MEDIUM);
        mVoiceRipple.setRippleDecayRate(Rate.HIGH);
        mVoiceRipple.setBackgroundRippleRatio(RIPPLE_RATIO);
        mVoiceRipple.setIconSize(mContext.getResources().getInteger(R.integer.chat_icon_size));
        setIcon(Type.RECORD);

        // set recorder related settings for ripple view
        mVoiceRipple.setMediaRecorder(new MediaRecorder());
        mVoiceRipple.setAudioSource(MediaRecorder.AudioSource.MIC);
        mVoiceRipple.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mVoiceRipple.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

        Renderer currentRenderer = new CircleRippleRenderer(getDefaultRipplePaint(),
                getDefaultRippleBackgroundPaint(),
                getButtonPaint());
        mVoiceRipple.setRenderer(currentRenderer);

        mAudioGestureListener = new AudioGestureListener(mVoiceRipple.getContext()) {
            @Override
            public void onCancelRecord() {
                mVoiceRipple.reset(false);
                mVoiceRipple.deleteCurrentVoiceRecord();
                mOnClickListener.onStopRecord(null);
                super.onCancelRecord();
                SoundEffectService.playSound(mContext, SoundEffectService.SoundFile.WOOSH);
            }

            @Override
            public void onStopRecord() {
                if (mVoiceRipple.isRecording()) {
                    mVoiceRipple.reset(true);
                }
                super.onStopRecord();
            }

            @Override
            void onStartRecord() {
                onStartRecordClick();
            }

            @Override
            void onRecordingDuration(int second) {
                mTextView.setText(FormatTime.getFormattedDuration(second));
            }

            @Override
            void onClick() {
                if (mType == Type.RECORD) {
                    if (isRecordAudioPermissionGranted()) {
                        SoundEffectService.playSound(mContext, SoundEffectService.SoundFile.MATCH);
                        Toast.makeText(mContext, R.string.chat_voice_hint, Toast.LENGTH_LONG).show();
                    } else {
                        if (!mIsActionRecorded) {
                            mIsActionRecorded = true;
                            requestRecordAudioPermission();
                        }
                    }
                } else {
                    if (mOnClickListener != null) {
                        mOnClickListener.onClick();
                    }
                }
            }

            @Override
            void onTranslationXUpdate(float px) {
                mSlideView.setTranslationX(px);
            }

            @Override
            boolean isEnableTranslation() {
                return mType == Type.RECORD;
            }

            @Override
            void onTouch() {
                mIsActionRecorded = false;
            }
        };
        mVoiceRipple.setOnTouchListener(mAudioGestureListener);
    }

    private void onStartRecordClick() {
        if (mType == Type.RECORD && !mIsActionRecorded && mContext instanceof AbsBaseActivity) {
            mIsActionRecorded = true;
            if (isRecordAudioPermissionGranted()) {
                checkToStopAudioPlaying();
                mVoiceRipple.startRecording();
            } else {
                requestRecordAudioPermission();
            }
        }
    }

    private boolean isRecordAudioPermissionGranted() {
        return ContextCompat.checkSelfPermission(mContext, Manifest.permission.RECORD_AUDIO)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void requestRecordAudioPermission() {
        ((AbsBaseActivity) mContext).checkPermission(new CheckPermissionCallbackHelper(mContext,
                CheckPermissionCallbackHelper.Type.MICROPHONE) {
            @Override
            public void onPermissionGranted() {
                Timber.e("permission granted");
            }

        }, Manifest.permission.RECORD_AUDIO);
    }

    public void setMinScroll(int minScroll) {
        mAudioGestureListener.setMinScroll(minScroll);
    }

    private void checkToStopAudioPlaying() {
        PlayAudioService service = ((AbsSoundControllerActivity) mContext).getPlayAudioService();
        if (service != null && service.isPlaying()) service.stop();
    }

    public void destroy() {
        if (mRecordFile != null && mRecordFile.exists()) {
            Timber.e("delete record file: %s",
                    mRecordFile.delete());
        }

        try {
            if (mVoiceRipple != null) {
                mVoiceRipple.onStop();
                mVoiceRipple.onDestroy();
            }
        } catch (Exception e) {
            Timber.e("destroy error: %s", e.getMessage());
        }
    }

    public void setIcon(Type type) {
        mType = type;
        Drawable icon = getDrawable(type.getIcon());
        mVoiceRipple.setRecordDrawable(icon, icon);
    }

    public void checkToCancelRecord() {
        if (mVoiceRipple != null && mVoiceRipple.isRecording()) {
            mAudioGestureListener.onCancelRecord();
        }
    }

    private Paint getDefaultRipplePaint() {
        Paint ripplePaint = new Paint();
        ripplePaint.setStyle(Paint.Style.FILL);
        ripplePaint.setColor((ContextCompat.getColor(mContext, R.color.colorPrimary) & 0x00FFFFFF) | 0x40000000);
        ripplePaint.setAntiAlias(true);
        return ripplePaint;
    }

    private Paint getDefaultRippleBackgroundPaint() {
        Paint rippleBackgroundPaint = new Paint();
        rippleBackgroundPaint.setStyle(Paint.Style.FILL);
        rippleBackgroundPaint.setColor((ContextCompat.getColor(mContext, R.color.colorPrimary) & 0x00FFFFFF) | 0x40000000);
        rippleBackgroundPaint.setAntiAlias(true);
        return rippleBackgroundPaint;
    }

    private Paint getButtonPaint() {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        paint.setStyle(Paint.Style.FILL);
        return paint;
    }

    private Drawable getDrawable(@DrawableRes int drawable) {
        return ContextCompat.getDrawable(mContext, drawable);
    }

    public enum Type {
        RECORD(R.drawable.ic_microphone),
        SEND(R.drawable.ic_play);

        @DrawableRes
        private final int mIcon;

        Type(int icon) {
            mIcon = icon;
        }

        public int getIcon() {
            return mIcon;
        }
    }

    public interface OnCallback extends OnClickListener {
        void onStopRecord(File recordFile);

        void onStartRecord();
    }

}
