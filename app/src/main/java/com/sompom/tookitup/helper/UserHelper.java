package com.sompom.tookitup.helper;

import com.sompom.tookitup.model.result.User;

/**
 * Created by Chhom Veasna on 7/19/2019.
 */
public final class UserHelper {

    public static User validateUserPostBody(User user) {
        if (user != null) {
            /*
                Don't include position in body for user model on server store position field as reference
                and return back to client as String. So mobile manage to ready only this property.
             */
            user.setPosition(null);
        }

        return user;
    }
}
