package com.sompom.tookitup.helper;

import android.support.annotation.StringRes;

import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.model.emun.Status;
import com.sompom.tookitup.model.request.RequestBuyProductRequest;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.Result;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.dialog.MessageDialog;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ObserverHelper;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.utils.NetworkStateUtil;
import com.sompom.tookitup.utils.SharedPrefUtils;

import io.reactivex.Observable;

/**
 * Created by He Rotha on 10/6/17.
 */

public final class BaseBuyingProcessHelper {
    private AbsBaseActivity mAbsBaseActivity;
    private ApiService mApiService;
    private OnBuyingProcessListener mListener;
    private Product mProduct;

    public BaseBuyingProcessHelper(AbsBaseActivity absBaseActivity, ApiService apiService) {
        mAbsBaseActivity = absBaseActivity;
        mApiService = apiService;
    }

    public void setListener(OnBuyingProcessListener listener) {
        mListener = listener;
    }

    public void startBuyOnProduct(Product product) {
        mProduct = product;
        //check network connection
        if (!NetworkStateUtil.isNetworkAvailable(mAbsBaseActivity)) {
            mListener.onError(getString(R.string.error_no_internet_connection_message));
            return;
        }

        //check login
        mAbsBaseActivity.startLoginWizardActivity(new LoginActivityResultCallback() {
            @Override
            public void onActivityResultSuccess(boolean isAlreadyLogin) {
                //check product is belong to owner
                if (SharedPrefUtils.getUserId(mAbsBaseActivity).equals(mProduct.getUser().getId())) {
                    //owner of product can't buy product himself
                    mListener.onError(getString(R.string.error_buy_own_product_420));
                } else {
                    //check product is current buying or non-buying
                    if (!mProduct.isRequestBuy()) {
                        //check product status, which user can buy only if product On-Sale
                        if (mProduct.getStatus() == Status.ON_SALE) {
                            doBuyProduct();
                        } else {
                            mListener.onError(getString(R.string.you_cannot_buy_this_product));
                        }
                    } else {
                        doCancelProduct();
                    }
                }
            }

            @Override
            public void onActivityResultFail() {
                mListener.shouldEnableButton(true);
            }
        });
    }

    private void doBuyProduct() {
        final String title = getString(R.string.title_dialog_confirm_before_buying_product);
        final String subTitle = getString(R.string.description_confirm_before_buying_product);
        final String yes = getString(R.string.search_address_button_confirm);
        final String no = getString(R.string.title_button_refuse);

        final MessageDialog dialog = new MessageDialog();
        dialog.setTitle(title);
        dialog.setMessage(subTitle);
        dialog.setLeftText(yes, view -> {
            mListener.shouldEnableButton(false);
            callBuyService();
            dialog.dismiss();
        });
        dialog.setLeftText(no, null);

        dialog.show(mAbsBaseActivity.getSupportFragmentManager());
        mListener.shouldEnableButton(true);
    }

    private void doCancelProduct() {
        final String title = getString(R.string.purchasing_dialog_title);
        final String subTitle = getString(R.string.purchasing_dialog_detail);
        final String yes = getString(R.string.button_yes);
        final String no = getString(R.string.button_no);

        final MessageDialog dialog = new MessageDialog();
        dialog.setTitle(title);
        dialog.setMessage(subTitle);
        dialog.setRightText(yes, view -> {
            mListener.shouldEnableButton(false);
            callBuyService();
        });
        dialog.setLeftText(no, null);
        dialog.show(mAbsBaseActivity.getSupportFragmentManager());
        mListener.shouldEnableButton(true);
    }

    private void callBuyService() {
        RequestBuyProductRequest buying = new RequestBuyProductRequest(SharedPrefUtils.getUserId(mAbsBaseActivity),
                mProduct.getUser().getId(),
                mProduct.getId());
        Observable<Result<Product>> buyingRequestResponse = mApiService.requestBuyProduct(buying);
        ObserverHelper<Result<Product>> helper = new ObserverHelper<>(mAbsBaseActivity, buyingRequestResponse);
        helper.execute(new OnCallbackListener<Result<Product>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (!mProduct.isRequestBuy()) {
                    if (ex.getCode() == 404 || ex.getCode() == 409) {
                        mListener.onError(getString(R.string.error_buy_item_buy_delete_409));
                    } else if (ex.getCode() == 408) {
                        mListener.onError(getString(R.string.error_buy_item_sold_408));
                    } else {
                        mListener.onError(ex.toString());
                    }
                } else {
                    mListener.onError(getString(R.string.purchase_cancel_msg_error));
                }
            }

            @Override
            public void onComplete(Result<Product> requestResponse) {
                mProduct.setRequestBuy(requestResponse.getData().isRequestBuy());
                if (mProduct.isRequestBuy()) {
                    mListener.onBuyOrCancelSuccess(true, getString(R.string.buy_success_200));
                } else {
                    mListener.onBuyOrCancelSuccess(false, getString(R.string.purchase_cancel_msg));
                }
            }
        });
    }

    public String getString(@StringRes int stringRes) {
        return mAbsBaseActivity.getString(stringRes);
    }

    public interface OnBuyingProcessListener {
        void onBuyOrCancelSuccess(boolean isBuying, String message);

        void onError(String errorMessage);

        void shouldEnableButton(boolean isEnable);
    }
}
