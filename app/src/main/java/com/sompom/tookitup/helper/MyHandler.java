package com.sompom.tookitup.helper;


import android.os.Handler;

/**
 * Created by He Rotha on 1/10/19.
 */
public class MyHandler extends Handler {
    private long mDelayMillisecond;
    private OnDoActionListener mOnDoActionListener;
    private Runnable mRunnable = () -> {
        if (mOnDoActionListener == null) {
            return;
        }
        mOnDoActionListener.onDoAction();
    };

    public MyHandler(long delayMillisecond) {
        mDelayMillisecond = delayMillisecond;
    }

    public MyHandler(long delayMillisecond, OnDoActionListener listener) {
        mDelayMillisecond = delayMillisecond;
        mOnDoActionListener = listener;
    }

    public void setOnDoActionListener(OnDoActionListener onDoActionListener) {
        mOnDoActionListener = onDoActionListener;
    }

    public void startDelay() {
        removeCallbacks(mRunnable);
        postDelayed(mRunnable, mDelayMillisecond);
    }

    public void destroy() {
        removeCallbacks(mRunnable);
    }

    public interface OnDoActionListener {
        void onDoAction();
    }
}
