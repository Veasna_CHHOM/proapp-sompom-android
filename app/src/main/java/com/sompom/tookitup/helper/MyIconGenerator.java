package com.sompom.tookitup.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.sompom.tookitup.R;
import com.sompom.tookitup.model.SupportingResource;
import com.sompom.tookitup.widget.OverlapImageContainer;

import java.util.List;

import javax.annotation.Nullable;

/**
 * Created by He Rotha on 11/29/17.
 */

public class MyIconGenerator {
    private final Context mContext;
    private ViewGroup mContainer;
    private ViewGroup mContainer2;
    private ImageView mImageView;
    private OverlapImageContainer mImageViews;

    public MyIconGenerator(Context context) {
        this.mContext = context;
        this.mContainer = (ViewGroup) LayoutInflater.from(this.mContext).inflate(R.layout.layout_custer, (ViewGroup) null);
        mImageView = mContainer.findViewById(R.id.imageView);

        this.mContainer2 = (ViewGroup) LayoutInflater.from(this.mContext).inflate(R.layout.layout_custers, (ViewGroup) null);
        mImageViews = mContainer2.findViewById(R.id.imageView);
    }

    public Bitmap makeIcon(@Nullable SupportingResource url) {
        if (url != null) {
            if (url.getBitmap() != null) {
                mImageView.setImageBitmap(url.getBitmap());
            } else {
                mImageView.setImageDrawable(url.getDrawable());
            }
        }
        return this.makeIcon();
    }

    public Bitmap makeIcon() {
        int measureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        this.mContainer.measure(measureSpec, measureSpec);
        int measuredWidth = this.mContainer.getMeasuredWidth();
        int measuredHeight = this.mContainer.getMeasuredHeight();
        this.mContainer.layout(0, 0, measuredWidth, measuredHeight);

        Bitmap r = Bitmap.createBitmap(measuredWidth, measuredHeight, Bitmap.Config.ARGB_8888);
        r.eraseColor(0);
        Canvas canvas = new Canvas(r);

        this.mContainer.draw(canvas);
        return r;
    }

    public Bitmap makeIcons(@Nullable List<SupportingResource> bitmaps, int itemSize) {
        if (bitmaps != null && !bitmaps.isEmpty()) {
            mImageViews.addBitMapItem(bitmaps, itemSize);
        }

        return this.makeIcons();
    }

    public Bitmap makeIcons() {
        int measureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        this.mContainer2.measure(measureSpec, measureSpec);
        int measuredWidth = this.mContainer2.getMeasuredWidth();
        int measuredHeight = this.mContainer2.getMeasuredHeight();
        this.mContainer2.layout(0, 0, measuredWidth, measuredHeight);
        Bitmap r = Bitmap.createBitmap(measuredWidth, measuredHeight, Bitmap.Config.ARGB_8888);
        r.eraseColor(0);
        Canvas canvas = new Canvas(r);

        this.mContainer2.draw(canvas);
        return r;
    }

}
