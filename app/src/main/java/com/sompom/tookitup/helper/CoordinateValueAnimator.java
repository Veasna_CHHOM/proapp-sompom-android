package com.sompom.tookitup.helper;

import android.animation.ValueAnimator;
import android.view.animation.LinearInterpolator;

/**
 * Created by He Rotha on 1/18/18.
 */

public class CoordinateValueAnimator {
    private AnimatorListener mAnimatorListener;
    private BoundValueAnimator mValueAnimatorX;
    private BoundValueAnimator mValueAnimatorY;
    private int mCurrentX;
    private int mCurrentY;
    private int mEndX;
    private int mEndY;
    private Interpolator mInterpolator;

    public CoordinateValueAnimator(Interpolator interpolator) {
        mInterpolator = interpolator;
    }

    public void setAnimatorListener(AnimatorListener animatorListener) {
        mAnimatorListener = animatorListener;
    }

    public void cancel() {
        if (mValueAnimatorX != null) {
            mValueAnimatorX.cancel();
        }
        if (mValueAnimatorY != null) {
            mValueAnimatorY.cancel();
        }
    }

    public int getEndX() {
        return mEndX;
    }

    public int getEndY() {
        return mEndY;
    }

    public void start(int startX, int endX, int startY, int endY) {
        mCurrentX = startX;
        mCurrentY = startY;
        mEndX = endX;
        mEndY = endY;
        mValueAnimatorX = new BoundValueAnimator(startX, endX);
        mValueAnimatorY = new BoundValueAnimator(startY, endY);
        mValueAnimatorX.addUpdateListener(valueAnimator -> {
            mCurrentX = (int) valueAnimator.getAnimatedValue();
            mAnimatorListener.onAnimationUpdate(mCurrentX, mCurrentY);
        });
        mValueAnimatorY.addUpdateListener(valueAnimator -> {
            mCurrentY = (int) valueAnimator.getAnimatedValue();
            mAnimatorListener.onAnimationUpdate(mCurrentX, mCurrentY);
        });
        mValueAnimatorX.start();
        mValueAnimatorY.start();
    }


    public enum Interpolator {
        BOUND, LINEAR
    }

    public interface AnimatorListener {
        void onAnimationUpdate(int x, int y);
    }

    public class BoundValueAnimator extends ValueAnimator {
        public BoundValueAnimator(int startValue, int endValue) {
            setIntValues(startValue, endValue);
            init();
        }

        public BoundValueAnimator(float startValue, float endValue) {
            setFloatValues(startValue, endValue);
            init();
        }

        public void init() {
            if (mInterpolator == Interpolator.BOUND) {
                setDuration(1000);
                setInterpolator(new MyBounceInterpolator(.07, 8));
            } else {
                setDuration(100);
                setInterpolator(new LinearInterpolator());
            }
        }
    }
}
