package com.sompom.tookitup.helper;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.sompom.baseactivity.ResultCallback;
import com.sompom.tookitup.intent.newintent.EditProfileIntent;
import com.sompom.tookitup.listener.OnCompleteListener;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.dialog.OtherUserProfileDialog;
import com.sompom.tookitup.utils.SharedPrefUtils;

/**
 * Created by nuonveyo on 8/27/18.
 */

public class NavigateSellerStoreHelper {
    private Context mContext;
    private User mUser;

    public NavigateSellerStoreHelper(Context context, User user) {
        mContext = context;
        mUser = user;
    }

    public void openMyStore(boolean updateUser, OnCompleteListener<User> userOnCompleteListener) {
//        Old code
//        if (mContext instanceof AbsBaseActivity) {
//            ((AbsBaseActivity) mContext).startLoginWizardActivity(new LoginActivityResultCallback() {
//                @Override
//                public void onActivityResultSuccess(boolean isAlreadyLogin) {
//                    Timber.e("isAlreadyLogin: %s", isAlreadyLogin);
//                    Timber.e("mUser: %s", mUser);
//                    if (!isAlreadyLogin) {
//                        mUser = SharedPrefUtils.getUser(mContext);
//                    }
//                    if (mUser != null) {
//                        ((AbsBaseActivity) mContext).startActivityForResult(new SellerStoreIntent(mContext, mUser.getId()),
//                                new ResultCallback() {
//                                    @Override
//                                    public void onActivityResultSuccess(int resultCode, Intent data) {
//                                        mUser = SharedPrefUtils.getUser(mContext);
//                                        if (userOnCompleteListener != null) {
//                                            userOnCompleteListener.onComplete(mUser);
//                                        }
//                                    }
//                                });
//                    }
//                }
//            });
//        }

//        Manage to open edit my profile screen directly
        if (mContext instanceof AbsBaseActivity) {
            ((AbsBaseActivity) mContext).startActivityForResult(new EditProfileIntent(mContext),
                    new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            if (updateUser) {
                                mUser = SharedPrefUtils.getUser(mContext);
                                if (userOnCompleteListener != null) {
                                    userOnCompleteListener.onComplete(mUser);
                                }
                            }
                        }
                    });
        }
    }

    public void openSellerStore() {
        //Old code
//        mContext.startActivity(new SellerStoreIntent(mContext, mUser.getId()));

//        Open user profile screen instead.
        if (mUser == null) {
            return;
        }

        if (SharedPrefUtils.checkIsMe(mContext, mUser.getId())) {
            openMyStore(false, null);
        } else {
            if (mContext instanceof AppCompatActivity) {
                OtherUserProfileDialog dialog = OtherUserProfileDialog.newInstance(mUser.getId());
                dialog.show(((AppCompatActivity) mContext).getSupportFragmentManager(), null);
            }
        }
    }

    public static void openSellerStore(AppCompatActivity appCompatActivity, String userId) {
        OtherUserProfileDialog dialog = OtherUserProfileDialog.newInstance(userId);
        dialog.show(appCompatActivity.getSupportFragmentManager(), null);
    }
}
