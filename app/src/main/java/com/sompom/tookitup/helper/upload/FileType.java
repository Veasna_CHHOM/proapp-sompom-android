package com.sompom.tookitup.helper.upload;

/**
 * Created by He Rotha on 6/20/18.
 */
public enum FileType {
    IMAGE, VIDEO, LiveVideo, Audio
}
