package com.sompom.tookitup.helper;

import android.content.Context;
import android.support.v7.util.DiffUtil;
import android.text.TextUtils;

import com.sompom.tookitup.model.BaseChatModel;
import com.sompom.tookitup.model.TypingChatItem;
import com.sompom.tookitup.model.result.Chat;

import java.util.List;

public class ChatDiffCallback extends DiffUtil.Callback {
    private final List<BaseChatModel> mOldList;
    private final List<BaseChatModel> mNewList;
    private Context mContext;

    public ChatDiffCallback(Context context,
                            List<BaseChatModel> oldBaseChatModelList,
                            List<BaseChatModel> newBaseChatModelList) {
        this.mContext = context;
        this.mOldList = oldBaseChatModelList;
        this.mNewList = newBaseChatModelList;
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldPos, int newPos) {
        if (mOldList.get(oldPos) instanceof TypingChatItem && mNewList.get(newPos) instanceof TypingChatItem) {
            return true;
        } else {
            if (mOldList.get(oldPos) instanceof Chat && mNewList.get(newPos) instanceof Chat) {
                String oldId = ((Chat) mOldList.get(oldPos)).getId();
                String newId = ((Chat) mNewList.get(newPos)).getId();
                return TextUtils.equals(oldId, newId);
            }
        }
        return false;
    }

    @Override
    public boolean areContentsTheSame(int oldPos, int newPos) {
        if (mOldList.get(oldPos) instanceof TypingChatItem && mNewList.get(newPos) instanceof TypingChatItem) {
            return true;
        } else {
            if (mOldList.get(oldPos) instanceof Chat && mNewList.get(newPos) instanceof Chat) {
                Chat oldChat = ((Chat) mOldList.get(oldPos));
                Chat newChat = ((Chat) mNewList.get(newPos));
                return TextUtils.equals(oldChat.getActualContent(mContext), newChat.getActualContent(mContext)) &&
                        oldChat.getStatus() == newChat.getStatus() &&
                        oldChat.getDate().getTime() == newChat.getDate().getTime();
            }
        }
        return false;
    }


}
