package com.sompom.tookitup.helper.upload;

import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;

import com.cloudinary.Cloudinary;
import com.cloudinary.ProgressCallback;
import com.cloudinary.Transformation;
import com.cloudinary.utils.ObjectUtils;
import com.sompom.tookitup.R;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.io.File;
import java.util.Map;

import timber.log.Timber;

/**
 * Created by imac on 4/1/16.
 */
public abstract class AbsAmazonImageUploader {
    private static final String AB_NAME = "sompom-asia-jp";
    private static final String AM_COGNITO_POOL_ID = "ap-northeast-1:29a5d9e9-5d5f-40b4-9a5d-6449e237ff56";
    private static final String FOLDER_NAME = "tookitup/photos/";
    private Context mContext;
    private String mImagePath;
    private String mFullPathName;
    private String mFileName;
    private FileType mFileType;
    private boolean mIsNewThread = true;

    /**
     * Constructor.
     *
     * @param context     :
     * @param imagePath   : String the path of the image file in phone, where user want to us to upload.
     * @param profileName : String this recommended to use for upload profile picture, the profile
     *                    name should be name of the user, this will help to override the old profile
     *                    picture, avoid create too many profile picture of an user.
     */
    public AbsAmazonImageUploader(final Context context,
                                  final String imagePath,
                                  String profileName) {
        this(context, imagePath, profileName, FileType.IMAGE);
    }

    public AbsAmazonImageUploader(final Context context,
                                  final String imagePath,
                                  String profileName, FileType fileType) {
        this(context, imagePath, profileName, fileType, false, true);
    }

    public AbsAmazonImageUploader(final Context context,
                                  final String imagePath,
                                  String profileName,
                                  FileType fileType,
                                  boolean intermediateUpload,
                                  boolean isNewThread) {
        mFileType = fileType;
        mContext = context;
        mImagePath = imagePath;
        if (TextUtils.isEmpty(profileName)) {
            mFileName = "TookItUp_"
                    + SharedPrefUtils.getUserId(context) + "_" + System.currentTimeMillis();
        } else {
            mFileName = profileName + "_" +
                    SharedPrefUtils.getUserId(context) + "_" + System.currentTimeMillis();

        }
        mFullPathName = FOLDER_NAME + mFileName;
        Timber.e(mImagePath);
        setNewThread(isNewThread);
        if (intermediateUpload) {
            doUpload();
        }
    }

    public void setNewThread(boolean newThread) {
        mIsNewThread = newThread;
    }

    /**
     * Call for Process upload image.
     */
    public void doUpload() {
//        TookitupApplication.log(uploadProfileImg(new File(mImagePath), mFileName));
        //https://github.com/aws/aws-sdk-android
        //http://docs.aws.amazon.com/mobile/sdkforandroid/developerguide/s3transferutility.html
//        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
//                mContext,
//                AM_COGNITO_POOL_ID,
//                Regions.AP_NORTHEAST_1
//        );
//
//        final AmazonS3Client s3Client = new AmazonS3Client(credentialsProvider);
//        File fileToUpload = new File(mImagePath);
//
//        String contentType = getContentType(mImagePath);
//
//        ObjectMetadata metadata = new ObjectMetadata();
//        metadata.setCacheControl("max-age=31536000");
//        metadata.setContentType(contentType);
//
//        final TransferUtility transferUtility = new TransferUtility(s3Client, mContext);
//
//        final TransferObserver observer = transferUtility.upload(
//                AB_NAME,
//                mFullPathName,
//                fileToUpload,
//                metadata
//        );
//
//        observer.setTransferListener(new TransferListener() {
//            @Override
//            public void onStateChanged(int id, TransferState state) {
//                TookitupApplication.log(state.toString());
//                switch (state) {
//                    case COMPLETED:
//                        Log.d("Kethya", "process upload done.");
//                        try {
//                            new AsyncTask<Void, Void, String>() {
//                                @Override
//                                protected String doInBackground(Void... params) {
//                                    s3Client.setObjectAcl(AB_NAME, mFullPathName, CannedAccessControlList.PublicRead);
////                                    if (mFileName.startsWith("TookItUp_")) {
////                                        return mFileName;
////                                    } else {
//                                    URL s = s3Client.initUrl(AB_NAME, mFullPathName);
//                                    return s.toString();
////                                    }
//                                }
//
//                                @Override
//                                protected void onPostExecute(String aVoid) {
//                                    super.onPostExecute(aVoid);
//                                    TookitupApplication.log(aVoid);
//                                    onUploadSucceeded(aVoid);
//                                }
//                            }.execute();
//                        } catch (NetworkOnMainThreadException ex) {
//                            onUploadFail(ex);
//                        }
//                        break;
//                    case FAILED:
//                        TookitupApplication.log("process upload failed.");
//                        onUploadFail(new Exception("Uploading failed."));
//                        break;
//                    case CANCELED:
//                        TookitupApplication.log("process upload failed.");
//                        onUploadFail(new Exception("Uploading Canceled."));
//                        break;
//                    case PENDING_CANCEL:
//                    case PENDING_NETWORK_DISCONNECT:
//                        TookitupApplication.log("process error unknown");
//                        onUploadFail(new Exception("Uploading failed unknown error."));
//                        break;
//                    case WAITING_FOR_NETWORK:
//                        observer.cleanTransferListener();
//                        TookitupApplication.log("Network disconnected");
//                        onUploadFail(new Exception("Network disconnected"));
//                        break;
//                    default:
//                        break;
//                }
//            }
//
//            @Override
//            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
//                onUploadProgressing(id, bytesCurrent, bytesTotal);
//            }
//
//            @Override
//            public void onError(int id, Exception ex) {
//                onUploadFail(ex);
//            }
//
//
//        });
        final Cloudinary cloudinary = new Cloudinary(mContext.getString(R.string.cloudinary_url));

        final Map imageMap;
        if (mFileType == FileType.IMAGE) {
            imageMap = ObjectUtils.asMap("public_id", mFileName, "overwrite", true);
        } else {
            imageMap = ObjectUtils.asMap("public_id", mFileName, "overwrite", true, "resource_type", "video");
        }

        if (mIsNewThread) {
            new AsyncTask<Void, Void, Map>() {
                @Override
                protected Map doInBackground(Void... params) {
                    Map map = uploading(cloudinary, imageMap);
                    return map;
                }

                @Override
                protected void onPostExecute(Map map) {
                    super.onPostExecute(map);
                    initUrl(map, cloudinary);
                }
            }.execute();
        } else {
            Map map = uploading(cloudinary, imageMap);
            initUrl(map, cloudinary);
        }
    }

    private void initUrl(Map map, Cloudinary cloudinary) {
        if (map != null) {
            final String url;
            String thumbnail = null;
            if (mFileType == FileType.IMAGE) {
                url = cloudinary
                        .url()
                        .privateCdn(false)
                        .transformation(new Transformation().quality("auto").fetchFormat("webp"))
                        .generate(mFileName + ".jpg");
            } else {
                Transformation transformVideo = null;
                Transformation transformThumbnail = null;
                try {
                    MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
                    metaRetriever.setDataSource(mImagePath);
                    String heightS = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
                    String widthS = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
                    String duration = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);

                    Timber.e("duration of video: " + duration);
                    int height = Integer.parseInt(heightS);
                    int width = Integer.parseInt(widthS);

                    if (width > height) {
                        if (height > 700) {
                            transformVideo = new Transformation().height(700).crop("scale");
                            transformThumbnail = new Transformation().height(700).crop("scale")
                                    .startOffset(Integer.parseInt(duration) / 2 * 1000) //get half duration thumbnail in second
                                    .fetchFormat("jpg");
                        }
                    } else {
                        if (width > 700) {
                            transformVideo = new Transformation().width(700).crop("scale");
                            transformThumbnail = new Transformation().width(700).crop("scale")
                                    .startOffset(Integer.parseInt(duration) / 2 * 1000)//get half duration thumbnail in second
                                    .fetchFormat("jpg");
                        }
                    }

                } catch (Exception ex) {
                    Timber.e(ex.toString());
                }


                if (transformVideo != null) {
                    url = cloudinary
                            .url()
                            .privateCdn(false)
                            .resourceType("video")
                            .transformation(transformVideo)
                            .generate(mFileName + ".mp4");
                } else {
                    url = cloudinary
                            .url()
                            .privateCdn(false)
                            .resourceType("video")
                            .generate(mFileName + ".mp4");
                }

                if (transformThumbnail != null) {
                    thumbnail = cloudinary
                            .url()
                            .privateCdn(false)
                            .resourceType("video")
                            .transformation(transformThumbnail)
                            .generate(mFileName + ".jpg");
                } else {
                    thumbnail = cloudinary
                            .url()
                            .privateCdn(false)
                            .resourceType("video")
                            .generate(mFileName + ".jpg");
                }
            }
            onUploadSucceeded(url, thumbnail);
        } else {
            onUploadFail(new Exception());
        }
    }

    @Nullable
    private Map uploading(Cloudinary cloudinary, Map imageMap) {
        Map map = null;
        try {
            map = cloudinary.uploader().upload(new File(mImagePath), imageMap, new ProgressCallback() {
                @Override
                public void onProgress(long bytesUploaded, long totalBytes) {
                    long percent = bytesUploaded * 100 / totalBytes;
                    if (percent % 5 == 0) {
                        onUploadProgressing(0, percent, 100);
                    }
                }
            });
        } catch (Exception e) {
            Timber.e("doInBackground: " + e.toString());
        }
        return map;
    }

    public String getContentType(String imagePath) {
        String type = "image/jpeg";
        String extension = MimeTypeMap.getFileExtensionFromUrl(imagePath);
        if (!TextUtils.isEmpty(extension)) {
            String temp = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            if (!TextUtils.isEmpty(temp)) {
                type = temp;
            }
        }
        return type;
    }

    /**
     * Call when image is uploaded successfully.
     *
     * @param imageUrl  : Url that point to image on server.
     * @param thumbnail : Thumbnail image of video (work only if upload type is Video)
     */
    public abstract void onUploadSucceeded(String imageUrl, @Nullable String thumbnail);

    /**
     * Call when upload is failed.
     *
     * @param ex : Exception that occur during uploading.
     */
    public abstract void onUploadFail(Exception ex);

    /**
     * Call every second of image progressing while uploading the image.
     * Useful for checking percentage of upload progressing.
     *
     * @param id            : the id of the process.
     * @param bytesCurrent: int Size of the photos that is uploaded.
     * @param bytesTotal    : Total size of the photo that is going to be uploaded to server.
     */
    public void onUploadProgressing(int id, long bytesCurrent, long bytesTotal) {
        //STUB
    }

    public enum FileType {
        IMAGE, VIDEO
    }
}
