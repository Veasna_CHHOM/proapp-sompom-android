package com.sompom.tookitup.helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.desmond.squarecamera.model.AppTheme;
import com.desmond.squarecamera.utils.ThemeManager;
import com.facebook.accountkit.AccessToken;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.accountkit.ui.ThemeUIManager;
import com.sompom.tookitup.R;

/**
 * Created by he.rotha on 2/28/17.
 */

public class FacebookSMSValidation {

    private static final int APP_REQUEST_CODE = 12345;
    private OnSMSValidationResult mCallback;
    private Context mContext;

    public FacebookSMSValidation(Context context) {
        mContext = context;
    }

    public static void logOut() {
        AccountKit.logOut();
    }

    public void doValidation(OnSMSValidationResult callback) {
        mCallback = callback;
        final Intent intent = new Intent(mContext, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN);
        if (ThemeManager.getAppTheme(mContext) == AppTheme.Black) {
            configurationBuilder.setUIManager(new ThemeUIManager(R.style.AppLoginTheme_Black));
        } else {
            configurationBuilder.setUIManager(new ThemeUIManager(R.style.AppLoginTheme_White));
        }
//
        intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION, configurationBuilder.build());
        if (mContext instanceof Activity) {
            ((Activity) mContext).startActivityForResult(intent, APP_REQUEST_CODE);
        }
    }

    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (mCallback != null) {
            if (requestCode == APP_REQUEST_CODE) {
                AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
                if (!loginResult.wasCancelled()) {
                    if (loginResult.getError() != null) {
                        mCallback.onFail();
                    } else {
                        getAccount();
                    }
                } else {
                    mCallback.onCancel();
                }
            }
        }
    }

    private void getAccount() {
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(final Account account) {
                PhoneNumber phoneNumber = account.getPhoneNumber();
                String phoneNumberString = phoneNumber.toString();
                AccessToken token = AccountKit.getCurrentAccessToken();
                mCallback.onSuccess(phoneNumberString, token.getToken());
            }

            @Override
            public void onError(final AccountKitError error) {
                mCallback.onFail();
            }
        });
    }

    public interface OnSMSValidationResult {
        void onSuccess(String phoneNumber, String token);

        void onFail();

        void onCancel();
    }
}
