package com.sompom.tookitup.helper;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;

import timber.log.Timber;

/**
 * Created by He Rotha on 9/29/17.
 */

public abstract class CustomExoPlayerEventListener implements Player.EventListener {

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
        //this event is not necessary
        Timber.v("onTracksChanged");
    }

    @Override
    public void onLoadingChanged(boolean isLoading) {
        //this event is not necessary
        Timber.v("onLoadingChanged " + isLoading);

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
        //this event is not necessary
        Timber.v("onPlaybackParametersChanged");

    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
        Timber.v("onTimelineChanged");

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        Timber.v("onPlayerStateChanged");

    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {
        Timber.v("onRepeatModeChanged");

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {
        Timber.v("onShuffleModeEnabledChanged");

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        Timber.v("onPlayerError " + error.toString());

    }

    @Override
    public void onPositionDiscontinuity(int reason) {
        Timber.v("onPositionDiscontinuity");

    }

    @Override
    public void onSeekProcessed() {
        Timber.v("onSeekProcessed");

    }
}
