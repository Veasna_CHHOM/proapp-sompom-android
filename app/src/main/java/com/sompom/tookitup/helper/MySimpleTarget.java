package com.sompom.tookitup.helper;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.bumptech.glide.request.Request;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;

import timber.log.Timber;

/**
 * Created by He Rotha on 12/6/18.
 */
public class MySimpleTarget implements Target<Bitmap> {
    @Override
    public void onLoadStarted(@Nullable Drawable placeholder) {
        Timber.e("onLoadStarted");
    }

    @Override
    public void onLoadFailed(@Nullable Drawable errorDrawable) {
        Timber.e("onLoadFailed");

    }

    @Override
    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
        Timber.e("onResourceReady");

    }

    @Override
    public void onLoadCleared(@Nullable Drawable placeholder) {
        Timber.e("onLoadCleared");

    }

    @Override
    public void getSize(@NonNull SizeReadyCallback cb) {
        Timber.e("getSize");
        cb.onSizeReady(300, 300);

    }

    @Override
    public void removeCallback(@NonNull SizeReadyCallback cb) {
        Timber.e("removeCallback");

    }

    @Nullable
    @Override
    public Request getRequest() {
        return null;
    }

    @Override
    public void setRequest(@Nullable Request request) {
        Timber.e("setRequest");

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }
}
