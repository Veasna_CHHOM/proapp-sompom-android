package com.sompom.tookitup.helper.upload;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;

import timber.log.Timber;

/**
 * Created by He Rotha on 6/20/18.
 */
public abstract class AbsMyUploader implements UploadCallback {
    UploadListener mUploadListener;

    public void setListener(UploadListener listener) {
        mUploadListener = listener;
    }

    public abstract String doUpload();

    @Override
    public void onStart(String requestId) {
        Timber.v("onServiceStart: " + requestId + "  " + MediaManager.get().hashCode());
    }

    @Override
    public void onProgress(String requestId, long bytes, long totalBytes) {

        long percent = bytes * 100 / totalBytes;
        Timber.v("onProgress: " + requestId + "  " + percent);
        if (percent % 5 == 0) {
            mUploadListener.onUploadProgressing(requestId, (int) percent);
        }
    }

    @Override
    public void onError(String requestId, ErrorInfo error) {
        Timber.e("onError: " + requestId + ", error:" + error.getDescription());
        if (error.getCode() != ErrorInfo.REQUEST_CANCELLED) {
            mUploadListener.onUploadFail(requestId, new Exception(error.getDescription()));
        } else {
            mUploadListener.onCancel(requestId);
        }
    }

    @Override
    public void onReschedule(String requestId, ErrorInfo error) {
        MediaManager.get().cancelRequest(requestId);
        mUploadListener.onUploadFail(requestId, new Exception(error.getDescription()));
        Timber.e("onReschedule: " + requestId + ", error:" + error);
    }

}
