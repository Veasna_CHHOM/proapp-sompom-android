package com.sompom.tookitup.helper;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by He Rotha on 10/12/17.
 */

public abstract class MyTextWatcher implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        onTextChanged(s);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    public abstract void onTextChanged(CharSequence s);
}
