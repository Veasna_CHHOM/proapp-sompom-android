package com.sompom.tookitup.helper;

import android.support.v7.app.AppCompatActivity;

import com.sompom.tookitup.model.Locations;
import com.sompom.tookitup.utils.GeocoderUtil;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by imac on 6/15/17.
 */

public class SingleRequestLocation extends RequestLocationManager
        implements RequestLocationManager.OnRequestLocationCallback {

    private static Locations sLocations;
    private List<Callback> mCallback = new ArrayList<>();

    public SingleRequestLocation(AppCompatActivity appCompatActivity) {
        super(appCompatActivity);
        setOnRequestLocationCallback(this);
    }

    public static void reset() {
        if (sLocations != null) {
            sLocations.reset();
            sLocations = null;
        }
    }

    public static Locations getLocation() {
        return sLocations;
    }

    public void execute(Callback callback) {
        if (sLocations != null && !sLocations.isEmpty()) {
            Timber.e("country: " + sLocations.getCountry() +
                    ", latitude: " + sLocations.getLatitude() +
                    ", longtitude: " + sLocations.getLongtitude());
            callback.onComplete(sLocations, false);
        } else {
            forceExecute(callback);
        }
    }

    public void forceExecute(Callback callback) {
        mCallback.add(callback);
        execute();
    }

    @Override
    public void onLocationSuccess(final double latitude, final double longtitude) {
        getCountry(latitude, longtitude, false);
    }

    private void getCountry(final double latitude, final double longtitude, boolean isNeverAskAgain) {
        GeocoderUtil.getCountry(getActivity(), latitude, longtitude, countryCode -> {
            Locations location = new Locations();
            location.setLatitude(latitude);
            location.setLongtitude(longtitude);
            location.setCountry(countryCode);
            sLocations = location;
            Timber.e("country: " + sLocations.getCountry() +
                    ", latitude: " + latitude +
                    ", longtitude: " + longtitude);

            for (Callback callback : mCallback) {
                callback.onComplete(sLocations, isNeverAskAgain);
            }
            for (int i = mCallback.size() - 1; i >= 0; i--) {
                mCallback.remove(i);
            }
        });
    }

    @Override
    public void onLocationDisable() {
        getCountry(0, 0, false);
    }

    @Override
    public void onLocationPermissionDeny(boolean isNeverask) {
        getCountry(0, 0, isNeverask);
    }

    public interface Callback {
        void onComplete(Locations location, boolean isNeverAskAgain);
    }
}
