package com.sompom.tookitup.helper.upload;

import android.content.Context;

import java.io.FileNotFoundException;

import io.reactivex.Observable;

/**
 * Created by nuonveyo on 4/27/18.
 */

public class ProfileUploader extends AbsUploader {
    public static final int sUploadError = -100;
    private Callback mCallback;

    public ProfileUploader(Context context, String imagePath, final UploadType uploadType, Callback callback) {
        super(context, imagePath, uploadType);
        mCallback = callback;
    }

    public static Observable<UploadResult> uploadProfileImage(Context context, String path) {
        Observable<UploadResult> ob = Observable.create(e -> {
            ProfileUploader uploader = new ProfileUploader(context, path, UploadType.Profile, new Callback() {
                @Override
                public void onUploaded(UploadResult result) {
                    e.onNext(result);
                    e.onComplete();
                }

                @Override
                public void onUploadFail(Exception ex) {
                    e.onError(ex);
                    e.onComplete();
                }
            });
            uploader.doUpload();
        });
        ob = ob.onErrorResumeNext(throwable -> {
            if (throwable instanceof FileNotFoundException) {
                return Observable.just(new ProfileUploader.UploadResult());
            }
            return Observable.error(throwable);
        });
        return ob;
    }

    public static Observable<UploadResult> uploadCoverImage(Context context, String path) {
        Observable<UploadResult> ob = Observable.create(e -> {
            ProfileUploader uploader = new ProfileUploader(context, path, UploadType.Cover, new Callback() {
                @Override
                public void onUploaded(UploadResult uploadResult) {
                    e.onNext(uploadResult);
                    e.onComplete();
                }

                @Override
                public void onUploadFail(Exception ex) {
                    e.onError(ex);
                    e.onComplete();
                }
            });
            uploader.doUpload();
        });
        ob = ob.onErrorResumeNext(throwable -> {
            if (throwable instanceof FileNotFoundException) {
                return Observable.just(new ProfileUploader.UploadResult());
            }
            return Observable.error(throwable);
        });
        return ob;
    }

    @Override
    public void onUploadSucceeded(String imageUrl, String thumb) {
        if (mCallback != null) {
            mCallback.onUploaded(new UploadResult(imageUrl, thumb));
        }
    }

    @Override
    public void onUploadFail(String id, String ex) {
        if (mCallback != null) {
            mCallback.onUploadFail(new Exception(ex));
        }
    }

    public interface Callback {
        void onUploaded(UploadResult uploadResult);

        void onUploadFail(Exception ex);
    }

    public static class UploadResult {
        private String mUrl;
        private String mThumbnail;

        public UploadResult() {
        }

        public UploadResult(String url, String thumbnail) {
            mUrl = url;
            mThumbnail = thumbnail;
        }

        public String getUrl() {
            return mUrl;
        }


        public String getThumbnail() {
            return mThumbnail;
        }

    }
}
