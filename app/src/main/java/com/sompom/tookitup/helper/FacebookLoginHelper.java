package com.sompom.tookitup.helper;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.internal.ServerProtocol;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

/**
 * Created by he.rotha on 6/2/16.
 */
public class FacebookLoginHelper {
    private static final String TAG = FacebookLoginHelper.class.getName();
    private AppCompatActivity mAppCompatActivity;
    private CallbackManager mCallbackManager;
    private LoginCallBack mLoginCallBack;
    private PublicActionCallBack mPublicActionCallback;

    public FacebookLoginHelper(AppCompatActivity appCompatActivity, LoginCallBack loginCallBack) {
        mAppCompatActivity = appCompatActivity;
        mLoginCallBack = loginCallBack;
    }

    public FacebookLoginHelper(AppCompatActivity appCompatActivity, PublicActionCallBack publicActionCallback) {
        mAppCompatActivity = appCompatActivity;
        this.mPublicActionCallback = publicActionCallback;
    }

    public void execute() {
        LoginManager.getInstance().logOut();
        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        if (mLoginCallBack != null) {
                            final Authentication authentication = new Authentication();
                            authentication.setAccessToken(loginResult.getAccessToken().getToken());
                            authentication.setExpiredDate(loginResult.getAccessToken().getExpires().getTime());

                            GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                                    new GraphRequest.GraphJSONObjectCallback() {
                                        @Override
                                        public void onCompleted(JSONObject object, GraphResponse response) {
                                            final Gson gson = new GsonBuilder().create();
                                            final FacebookUserInfo facebookUserInfo
                                                    = gson.fromJson(object.toString(), FacebookUserInfo.class);
                                            authentication.setUserId(String.valueOf(facebookUserInfo.getId()));
                                            authentication.setLastName(facebookUserInfo.getLastName());
                                            authentication.setFirstName(facebookUserInfo.getFirstName());
                                            authentication.setEmail(facebookUserInfo.getEmail());
                                            Log.e("rotha", object.toString());
                                            try {
                                                JSONObject picture = response.getJSONObject()
                                                        .getJSONObject("picture").getJSONObject("data");
                                                String imageUrl = picture.getString("url");
                                                authentication.setPictureUrl(imageUrl);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                            if (facebookUserInfo.getBirthday() != null) {
                                                try {
                                                    final Date initDate = new SimpleDateFormat("MM/dd/yyyy")
                                                            .parse(facebookUserInfo.getBirthday());
                                                    authentication.setDateOfBirth(initDate.getTime());
                                                } catch (Exception ex) {
                                                    Log.e(TAG, ex.toString());
                                                }
                                            }

                                            mLoginCallBack.onSuccess(authentication);
                                        }
                                    });
                            Bundle parameters = new Bundle();
                            parameters.putString("fields", "id,first_name,last_name,email,picture.type(large)");
                            request.setParameters(parameters);
                            request.setVersion(ServerProtocol.getDefaultAPIVersion());
                            request.executeAsync();
                        } else {
                            mPublicActionCallback.onSuccess();
                        }
                    }

                    @Override
                    public void onCancel() {
                        if (mLoginCallBack != null) {
                            mLoginCallBack.onFail();
                        } else {
                            mPublicActionCallback.onFail();
                        }
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        if (mLoginCallBack != null) {
                            mLoginCallBack.onFail();
                        } else {
                            mPublicActionCallback.onFail();
                        }
                    }
                }

        );

        if (mLoginCallBack != null) {
            LoginManager.getInstance().
                    logInWithReadPermissions(mAppCompatActivity, Arrays.asList("public_profile", "email"));
        } else {
            LoginManager.getInstance().
                    logInWithPublishPermissions(mAppCompatActivity, Collections.singletonList("publish_actions"));
        }
    }

    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public interface LoginCallBack {
        void onSuccess(Authentication authentication);

        void onFail();
    }

    public interface PublicActionCallBack {
        void onSuccess();

        void onFail();
    }

    public class Authentication {
        private String mAccessToken;
        private String mUserId;
        private long mExpiredDate;
        private String mFirstName;
        private String mLastName;
        private String mPhone;
        private long mDateOfBirth;
        private String mEmail;
        private String mPosition;
        private String mCompany;
        private String mPictureUrl;

        public String getAccessToken() {
            return mAccessToken;
        }

        public void setAccessToken(String accessToken) {
            this.mAccessToken = accessToken;
        }

        public String getUserId() {
            return mUserId;
        }

        public void setUserId(String userId) {
            this.mUserId = userId;
        }

        public long getExpiredDate() {
            return mExpiredDate;
        }

        public void setExpiredDate(long expiredDate) {
            this.mExpiredDate = expiredDate;
        }

        public String getFirstName() {
            return mFirstName;
        }

        public void setFirstName(String firstName) {
            mFirstName = firstName;
        }

        public String getLastName() {
            return mLastName;
        }

        public void setLastName(String lastName) {
            mLastName = lastName;
        }

        public String getPhone() {
            return mPhone;
        }

        public void setPhone(String phone) {
            mPhone = phone;
        }

        public long getDateOfBirth() {
            return mDateOfBirth;
        }

        public void setDateOfBirth(long dateOfBirth) {
            mDateOfBirth = dateOfBirth;
        }

        public String getEmail() {
            return mEmail;
        }

        public void setEmail(String email) {
            mEmail = email;
        }

        public String getPosition() {
            return mPosition;
        }

        public void setPosition(String position) {
            mPosition = position;
        }

        public String getCompany() {
            return mCompany;
        }

        public void setCompany(String company) {
            mCompany = company;
        }

        public String getPictureUrl() {
            return mPictureUrl;
        }

        public void setPictureUrl(String pictureUrl) {
            mPictureUrl = pictureUrl;
        }
    }

    public class FacebookUserInfo implements Parcelable {
        //CHECKSTYLE:OFF
        public final Creator<FacebookUserInfo> CREATOR = new Creator<FacebookUserInfo>() {
            public FacebookUserInfo createFromParcel(Parcel in) {
                return new FacebookUserInfo(in);
            }

            public FacebookUserInfo[] newArray(int size) {
                return new FacebookUserInfo[size];
            }
        };

        private String locale;
        private long id;
        private String gender;
        private String first_name;
        private boolean verified;
        private String updated_time;
        private String name;
        private int timezone;
        private String link;
        private String last_name;
        private String email;
        private String birthday;
        //CHECKSTYLE:OFF

        public FacebookUserInfo() {

        }

        public FacebookUserInfo(Parcel in) {
            locale = in.readString();
            id = in.readLong();
            gender = in.readString();
            first_name = in.readString();
            verified = in.readInt() == 1;
            updated_time = in.readString();
            name = in.readString();
            timezone = in.readInt();
            link = in.readString();
            last_name = in.readString();
            email = in.readString();
            birthday = in.readString();
        }

        public String getLocale() {
            return locale;
        }

        public void setLocale(String locale) {
            this.locale = locale;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getFirstName() {
            return first_name;
        }

        public void setFirstName(String firstName) {
            first_name = firstName;
        }

        public boolean isVerified() {
            return verified;
        }

        public void setVerified(boolean verified) {
            this.verified = verified;
        }

        public String getUpdatedTime() {
            return updated_time;
        }

        public void setUpdatedTime(String updatedTime) {
            updated_time = updatedTime;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getTimezone() {
            return timezone;
        }

        public void setTimezone(int timezone) {
            this.timezone = timezone;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getLastName() {
            return last_name;
        }

        public void setLastName(String lastName) {
            last_name = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getBirthday() {
            return birthday;
        }

        public void setBirthday(String birthday) {
            this.birthday = birthday;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof FacebookUserInfo) {
                return ((FacebookUserInfo) obj).getId() == id;
            }
            return false;
        }

        @Override
        public int hashCode() {
            return ((Long) id).hashCode();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(locale);
            dest.writeLong(id);
            dest.writeString(gender);
            dest.writeString(first_name);
            dest.writeInt(verified ? 1 : 0);
            dest.writeString(updated_time);
            dest.writeString(name);
            dest.writeInt(timezone);
            dest.writeString(link);
            dest.writeString(last_name);
            dest.writeString(email);
            dest.writeString(birthday);
        }
    }
}
