package com.sompom.tookitup.helper;

/**
 * Created by imac on 7/31/17.
 */

public abstract class LoginActivityResultCallback {
    private int mRequestCode;

    public int getRequestCode() {
        return mRequestCode;
    }

    public void setRequestCode(int requestCode) {
        mRequestCode = requestCode;
    }

    public abstract void onActivityResultSuccess(boolean isAlreadyLogin);

    public void onActivityResultFail() {

    }
}
