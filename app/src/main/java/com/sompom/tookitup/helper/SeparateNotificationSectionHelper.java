package com.sompom.tookitup.helper;

import android.text.format.DateUtils;

import com.sompom.tookitup.model.emun.NotificationItem;
import com.sompom.tookitup.model.notification.NotificationAdaptive;
import com.sompom.tookitup.model.notification.NotificationHeader;
import com.sompom.tookitup.utils.DateTimeUtils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by nuonveyo on 3/20/18.
 */

public class SeparateNotificationSectionHelper {
    private CompositeDisposable mCompositeDisposable;

    public SeparateNotificationSectionHelper() {
        mCompositeDisposable = new CompositeDisposable();
    }

    public void addMoreNotificationData(int hours,
                                        List<NotificationAdaptive> baseNotificationModels,
                                        Callback callback) {
        Observable<List<NotificationAdaptive>> observable = Observable.just(baseNotificationModels)
                .concatMap(baseNotificationModels1 -> {
                    List<NotificationAdaptive> baseNotificationModelList = new ArrayList<>();
                    for (NotificationAdaptive baseNotificationModel : baseNotificationModels1) {
                        if (DateTimeUtils.getHours(baseNotificationModel.getPublished().getTime()) > hours &&
                                DateUtils.isToday(baseNotificationModel.getPublished().getTime())) {
                            NotificationHeader notificationHeaderEarlier = new NotificationHeader();
                            notificationHeaderEarlier.setNotificationItem(NotificationItem.Earlier);
                            baseNotificationModelList.add(notificationHeaderEarlier);
                        }
                        baseNotificationModelList.add(baseNotificationModel);
                    }
                    return Observable.just(baseNotificationModelList);
                });

        Disposable disposable = observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::onSeparateCompleted, throwable -> {
                    Timber.e("error: %s", throwable.getMessage());
                    callback.onSeparateCompleted(baseNotificationModels);
                });
        addDisposable(disposable);
    }

    public void addDisposable(Disposable disposable) {
        mCompositeDisposable.add(disposable);
    }

    public interface Callback {
        void onSeparateCompleted(List<NotificationAdaptive> baseNotificationModels);
    }
}
