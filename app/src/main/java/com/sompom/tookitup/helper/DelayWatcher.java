package com.sompom.tookitup.helper;

import android.text.Editable;
import android.text.TextWatcher;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import timber.log.Timber;

/**
 * Created by He Rotha on 2/23/18.
 */

public abstract class DelayWatcher implements TextWatcher {
    private PublishSubject<String> mPublishSubject;
    private boolean mIsDelay = true;

    public DelayWatcher(boolean isDelay) {
        mIsDelay = isDelay;
        if (isDelay) {
            mPublishSubject = PublishSubject.create();
            mPublishSubject.debounce(1000, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::onTextChanged,
                            throwable -> Timber.e("Error: %s", throwable.getMessage()));
        }
    }

    public abstract void onTextChanged(String text);

    public void onTyping(String text) {

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (mIsDelay) {
            onTyping(charSequence.toString());
            mPublishSubject.onNext(charSequence.toString());
        } else {
            onTextChanged(charSequence.toString());
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {
    }

}
