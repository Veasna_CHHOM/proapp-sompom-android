package com.sompom.tookitup.helper;

import android.view.animation.Animation;

/**
 * Created by He Rotha on 9/11/17.
 */

public class AlphaAnimation extends android.view.animation.AlphaAnimation {
    private OnAnimationListener mOnAnimationListener;

    public AlphaAnimation(float fromAlpha, float toAlpha) {
        super(fromAlpha, toAlpha);
        setDuration(500);
    }

    public void setOnAnimationListener(OnAnimationListener onAnimationListener) {
        mOnAnimationListener = onAnimationListener;
        setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (mOnAnimationListener != null) {
                    mOnAnimationListener.onAnimationComplete();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public interface OnAnimationListener {
        void onAnimationComplete();
    }
}
