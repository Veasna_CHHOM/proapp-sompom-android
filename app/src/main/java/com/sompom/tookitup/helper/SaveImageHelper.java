package com.sompom.tookitup.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.desmond.squarecamera.utils.media.FileNameUtil;
import com.desmond.squarecamera.utils.media.ImageSaver;
import com.sompom.tookitup.R;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.utils.ToastUtil;

import java.util.List;

/**
 * Created by nuonveyo on 9/17/18.
 */

public class SaveImageHelper {
    private Context mContext;
    private boolean mIsNotError;

    public SaveImageHelper(Context context) {
        mContext = context;
    }

    public void startSave(List<Media> medias) {
        for (int i = 0; i < medias.size(); i++) {
            int finalI = i;
            download(medias.get(finalI), new OnCallback() {
                @Override
                public void onFail() {
                    if (finalI == medias.size() - 1 && !mIsNotError) {
                        ToastUtil.showToast(mContext, mContext.getString(R.string.chat_image_save_fail));
                    }
                }

                @Override
                public void onSuccess(@NonNull Bitmap resource) {
                    mIsNotError = true;
                    new Handler().post(() -> {
                        ImageSaver.with(mContext)
                                .load(resource)
                                .into(FileNameUtil.getMessengerFile(medias.get(finalI).getUrl(), mContext).toString())
                                .resize(false)
                                .execute();
                    });
                    if (finalI == medias.size() - 1) {
                        ToastUtil.showToast(mContext, mContext.getString(R.string.chat_image_save_success));
                    }
                }
            });
        }
    }

    public void startSave(Media media) {
        download(media, new OnCallback() {
            @Override
            public void onFail() {
                ToastUtil.showToast(mContext, mContext.getString(R.string.chat_image_save_fail));
            }

            @Override
            public void onSuccess(@NonNull Bitmap resource) {
                new Handler().post(() -> ImageSaver.with(mContext)
                        .load(resource)
                        .into(FileNameUtil.getMessengerFile(media.getUrl(), mContext).toString())
                        .resize(false)
                        .execute());
                ToastUtil.showToast(mContext, mContext.getString(R.string.chat_image_save_success));
            }
        });
    }

    public void download(Media media, OnCallback onCallback) {
        GlideApp.with(mContext)
                .asBitmap()
                .load(media.getUrl())
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e,
                                                Object model,
                                                Target<Bitmap> target
                            , boolean isFirstResource) {
                        if (onCallback != null) onCallback.onFail();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource,
                                                   Object model,
                                                   Target<Bitmap> target,
                                                   DataSource dataSource,
                                                   boolean isFirstResource) {
                        return false;
                    }
                })
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource,
                                                @Nullable Transition<? super Bitmap> transition) {
                        if (onCallback != null) onCallback.onSuccess(resource);
                    }
                });
    }

    public interface OnCallback {
        void onFail();

        void onSuccess(@NonNull Bitmap resource);
    }
}
