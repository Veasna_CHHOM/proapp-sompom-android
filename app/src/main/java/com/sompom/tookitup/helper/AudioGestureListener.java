package com.sompom.tookitup.helper;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by He Rotha on 10/4/18.
 */
public abstract class AudioGestureListener implements View.OnTouchListener {
    private static final int MAX_VIDEO_DURATION = 60;
    private int mCordX;
    private float mMarginX;
    private GestureDetector mGestureDetector;
    private float mCurrentTranslationX = -1;
    private boolean mIsRecording = false;
    private int mCurrVideoRecTimer = 0;
    private int mMinScroll = 0;
    private Handler mTimerHandler = new Handler();
    private Handler mQuickLongClickHandler = new Handler();
    private View mView;
    private Runnable mQuickLongClickRunnable;
    private Runnable mDelayTimeRunnable;


    AudioGestureListener(Context context) {
        mDelayTimeRunnable = () -> {
            if (mCurrVideoRecTimer > MAX_VIDEO_DURATION) {
                onStopRecord();
            } else {
                onRecordingDuration(mCurrVideoRecTimer);
                mTimerHandler.postDelayed(mDelayTimeRunnable, 1000);
            }
            mCurrVideoRecTimer++;
        };
        mQuickLongClickRunnable = () -> {
            //vibrate phone when start recording
            startVibration();

            mCurrVideoRecTimer = 0;
            onStartRecord();
            mIsRecording = true;
            mTimerHandler.post(mDelayTimeRunnable);
        };
        mGestureDetector = new GestureDetector(context, new GestureDetector.OnGestureListener() {
            @Override
            public boolean onDown(MotionEvent motionEvent) {
                return false;
            }

            @Override
            public void onShowPress(MotionEvent motionEvent) {

            }

            @Override
            public boolean onSingleTapUp(MotionEvent motionEvent) {
                onClick();
                return false;
            }

            @Override
            public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
                return false;
            }

            @Override
            public void onLongPress(MotionEvent motionEvent) {

            }

            @Override
            public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
                return false;
            }
        });
    }

    private void startVibration() {
        Vibrator v = (Vibrator) mView.getContext().getSystemService(Context.VIBRATOR_SERVICE);
        if (v != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                v.vibrate(VibrationEffect.createOneShot(10, VibrationEffect.DEFAULT_AMPLITUDE));
            } else {
                v.vibrate(10);
            }
        }
    }

    void setMinScroll(int minScroll) {
        mMinScroll = minScroll;
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        if (isEnableTranslation()) {
            mView = view;
            if (mCurrentTranslationX == -1) {
                mCurrentTranslationX = view.getTranslationX();
            }
            int currentX = (int) event.getRawX();
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    onTouch();
                    mMarginX = view.getTranslationX();
                    mCordX = currentX;
                    mIsRecording = false;

                    mQuickLongClickHandler.post(mQuickLongClickRunnable);
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (mIsRecording) {
                        int diffMoveX = currentX - mCordX;
                        float destinationX = mMarginX + diffMoveX;
                        if (destinationX < 0) {
                            if (Math.abs(destinationX) > mMinScroll) {
                                onCancelRecord();
                                return true;
                            } else {
                                view.setTranslationX(destinationX);
                                onTranslationXUpdate(destinationX);
                            }
                        }
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    mQuickLongClickHandler.removeCallbacks(mQuickLongClickRunnable);
                    if (mIsRecording) {
                        onStopRecord();
                    }
                    break;
                default:
                    break;
            }
        }
        mGestureDetector.onTouchEvent(event);
        view.performClick();
        return false;
    }

    public void onCancelRecord() {
        startVibration();
        reset();
    }

    private void reset() {
        mIsRecording = false;
        mCurrVideoRecTimer = 0;
        mQuickLongClickHandler.removeCallbacks(mQuickLongClickRunnable);
        mTimerHandler.removeCallbacks(mDelayTimeRunnable);
        animation(mView);
    }

    public void onStopRecord() {
        reset();
        onRecordingDuration(0);
    }

    abstract void onStartRecord();

    abstract void onRecordingDuration(int second);

    abstract void onClick();

    abstract void onTranslationXUpdate(float px);

    abstract boolean isEnableTranslation();

    abstract void onTouch();

    private void animation(final View view) {
        view.setOnTouchListener(null);
        BoundValueAnimator mValueAnimatorX = new BoundValueAnimator(view.getTranslationX(), mCurrentTranslationX);
        mValueAnimatorX.addUpdateListener(animation -> {
            view.setTranslationX((Float) animation.getAnimatedValue());
            onTranslationXUpdate((Float) animation.getAnimatedValue());
        });
        mValueAnimatorX.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                view.setOnTouchListener(AudioGestureListener.this);
            }
        });
        mValueAnimatorX.start();
    }

    public class BoundValueAnimator extends ValueAnimator {

        private BoundValueAnimator(float startValue, float endValue) {
            setFloatValues(startValue, endValue);
            init();
        }

        public void init() {
            setDuration(1000);
            setInterpolator(new MyBounceInterpolator(.2, 8));
        }

    }
}
