package com.sompom.tookitup.helper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import timber.log.Timber;

/**
 * Created by He Rotha on 1/18/18.
 */

public class FloatingVideoGesture implements View.OnTouchListener {
    private static final int SWIPE_MIN_DISTANCE = 100;
    private static final int SWIPE_THRESHOLD_VELOCITY = 150;
    private WindowManager mWindowManager;
    private View mView;
    private OnGestureListener mOnGestureListener;
    private Context mContext;
    private CoordinateValueAnimator mCoordinateValueAnimator;
    private Point mWindow = new Point();
    private GestureDetector mGestureDetector;
    private int mCordX;
    private int mCordY;
    private int mMarginX;
    private int mMarginY;
    private boolean mIsEnableActionUp = false;


    public FloatingVideoGesture(WindowManager windowManager,
                                View view,
                                OnGestureListener listener) {
        mWindowManager = windowManager;
        mView = view;
        mOnGestureListener = listener;
        mContext = mView.getContext();
        mCoordinateValueAnimator = new CoordinateValueAnimator(CoordinateValueAnimator.Interpolator.BOUND);
        mCoordinateValueAnimator.setAnimatorListener((x, y) -> {
            try {
                final WindowManager.LayoutParams param = (WindowManager.LayoutParams) mView.getLayoutParams();
                param.x = x;
                param.y = y;
                mWindowManager.updateViewLayout(mView, param);
            } catch (Exception ex) {
                mCoordinateValueAnimator.cancel();
            }
        });

        windowManager.getDefaultDisplay().getSize(mWindow);

        mGestureDetector = new GestureDetector(mContext, new GestureDetector.OnGestureListener() {
            @Override
            public boolean onDown(MotionEvent motionEvent) {
//                Timber.e("onDown");
                return false;
            }

            @Override
            public void onShowPress(MotionEvent motionEvent) {
//                Timber.e("onShowPress");
            }

            @Override
            public boolean onSingleTapUp(MotionEvent motionEvent) {
//                Timber.e("onSingleTapUp");
                mOnGestureListener.onSingleTapUp(motionEvent);
                return false;
            }

            @Override
            public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
//                Timber.e("onScroll");

                return false;
            }

            @Override
            public void onLongPress(MotionEvent motionEvent) {
//                Timber.e("onLongPress");

            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

                boolean isFlingStrong = false;
                if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    Timber.e("onFling right to left");
                    isFlingStrong = true;
                } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    isFlingStrong = true;
                }

                if (e1.getY() - e2.getY() > SWIPE_MIN_DISTANCE && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
                    Timber.e("onFling Bottom to top");
                    isFlingStrong = true;
                } else if (e2.getY() - e1.getY() > SWIPE_MIN_DISTANCE && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
                    Timber.e("onFling  Top to bottom");
                    isFlingStrong = true;
                }
                if (!isFlingStrong) {
                    return false;
                }
                Timber.e(velocityX + "=" + mWindow.x + "   " + velocityY + "=" + mWindow.y);
                float diffX = Math.abs(velocityX);
                float diffY = Math.abs(velocityY);
                if (diffX > mWindow.x / 1.5 || diffY > mWindow.y / 1.5) {
                    mCoordinateValueAnimator.cancel();
                    mIsEnableActionUp = false;
                    mOnGestureListener.onFling(e1, e2, velocityX, velocityY);
                }

                return false;
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View view, MotionEvent event) {
        WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams) mView.getLayoutParams();
        mGestureDetector.onTouchEvent(event);
        int currentX = (int) event.getRawX();
        int currentY = (int) event.getRawY();
        int destinationX;
        int destinationY;

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mIsEnableActionUp = true;
                mMarginX = layoutParams.x;
                mMarginY = layoutParams.y;

                mCordX = currentX;
                mCordY = currentY;
                mCoordinateValueAnimator.cancel();
                break;
            case MotionEvent.ACTION_MOVE:
                int diffMoveX = currentX - mCordX;
                int diffMoveY = currentY - mCordY;

                destinationX = mMarginX + diffMoveX;
                destinationY = mMarginY + diffMoveY;

                layoutParams.x = destinationX;
                layoutParams.y = destinationY;
                mWindowManager.updateViewLayout(mView, layoutParams);
                break;
            case MotionEvent.ACTION_UP:
                if (mIsEnableActionUp) {
                    int startX = layoutParams.x + (mView.getWidth() / 2);
                    int startY = layoutParams.y + (mView.getHeight() / 2);

                    int halfScreenX = mWindow.x / 2;
                    int halfScreenY = mWindow.y / 2;
                    if (startX <= halfScreenX && startY <= halfScreenY) { //top_left
                        mCoordinateValueAnimator.start(layoutParams.x, 0, layoutParams.y, 0);
                    } else if (startX > halfScreenX && startY <= halfScreenY) { //top_right
                        mCoordinateValueAnimator.start(layoutParams.x, mWindow.x - mView.getWidth(), layoutParams.y, 0);
                    } else if (startX <= halfScreenX && startY > halfScreenY) { //bottom_left
                        mCoordinateValueAnimator.start(layoutParams.x,
                                0,
                                layoutParams.y,
                                mWindow.y - mView.getHeight() - getStatusBarHeight());
                    } else { //bottom_right
                        mCoordinateValueAnimator.start(layoutParams.x,
                                mWindow.x - mView.getWidth(),
                                layoutParams.y,
                                mWindow.y - mView.getHeight() - getStatusBarHeight());
                    }
                }
                break;
            default:
                break;
        }
        return true;
    }

    private int getStatusBarHeight() {
        return (int) Math.ceil(25 * mContext.getResources().getDisplayMetrics().density);
    }


    public interface OnGestureListener {
        void onFling(MotionEvent startMotion, MotionEvent endMotion, float velocityX, float velocityY);

        void onSingleTapUp(MotionEvent motionEvent);
    }
}
