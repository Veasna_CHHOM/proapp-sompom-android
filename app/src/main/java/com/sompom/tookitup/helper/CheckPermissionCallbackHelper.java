package com.sompom.tookitup.helper;

import android.app.Activity;
import android.support.annotation.StringRes;

import com.sompom.baseactivity.PermissionCheckHelper;
import com.sompom.tookitup.R;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.dialog.MessageDialog;
import com.sompom.tookitup.utils.IntentUtil;

/**
 * Created by nuonveyo
 * on 1/31/19.
 */

public abstract class CheckPermissionCallbackHelper implements PermissionCheckHelper.OnPermissionCallback {
    private final Activity mContext;
    private final Type mType;

    protected CheckPermissionCallbackHelper(Activity context, Type type) {
        mContext = context;
        mType = type;
    }

    @Override
    public void onPermissionDenied(String[] grantedPermission, String[] deniedPermission, boolean isUserPressNeverAskAgain) {
        if (isUserPressNeverAskAgain) {
            MessageDialog dialog = new MessageDialog();
            dialog.setTitle(getString(R.string.sign_up_access_permission_title));
            dialog.setMessage(getDescription());
            dialog.setLeftText(getString(R.string.sign_up_access_app_setting_button), view -> IntentUtil.openAppSetting(mContext));
            dialog.setRightText(getString(R.string.edit_profile_no), null);
            dialog.setOnDialogDismiss(this::onCancelClick);
            dialog.show(((AbsBaseActivity) mContext).getSupportFragmentManager(), MessageDialog.TAG);
        }
    }

    public void onCancelClick() {

    }

    private String getString(@StringRes int id) {
        return mContext.getString(id);
    }

    private String getDescription() {
        if (mType == Type.CAMERA) {
            return getString(R.string.sign_up_access_camera_description);
        } else if (mType == Type.MICROPHONE) {
            return getString(R.string.sign_up_access_microphone_description);
        } else {
            return getString(R.string.sign_up_access_storage_description);
        }
    }

    public enum Type {
        CAMERA,
        MICROPHONE,
        STORAGE
    }
}