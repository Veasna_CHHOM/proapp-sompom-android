package com.sompom.tookitup.helper;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.AdsItem;
import com.sompom.tookitup.model.LifeStream;
import com.sompom.tookitup.model.ShareAds;
import com.sompom.tookitup.model.SharedProduct;
import com.sompom.tookitup.model.SharedTimeline;
import com.sompom.tookitup.model.WallStreetAds;
import com.sompom.tookitup.model.emun.ItemType;
import com.sompom.tookitup.model.result.Product;

import java.lang.reflect.Type;

/**
 * Created by He Rotha on 8/30/17.
 */

public class AdapterDeserializer implements JsonDeserializer<Adaptive> {
    @Override
    public Adaptive deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
        JsonObject jObject = (JsonObject) json;
        JsonElement typeObj = jObject.get(AdsItem.FIELD_ITEM_TYPE);

        if (typeObj != null) {

            String typeVal = typeObj.getAsString();
            if (typeVal.equalsIgnoreCase(ItemType.TIMELINE.getValue())
                    || typeVal.equalsIgnoreCase(ItemType.LIVE_VIDEO.getValue())) {
                return context.deserialize(json, LifeStream.class);
            } else if (typeVal.equalsIgnoreCase(ItemType.ADS_ITEM.getValue())) {
                return context.deserialize(json, WallStreetAds.class);
            } else if (typeVal.equalsIgnoreCase(ItemType.ADS_FULL_MOBILE.getValue())
                    || typeVal.equalsIgnoreCase(ItemType.ADS_FOUR_SQUARE.getValue())
                    || typeVal.equalsIgnoreCase(ItemType.ADS_FULL_H.getValue())
                    || typeVal.equalsIgnoreCase(ItemType.ADS_TWO_VERTICAL.getValue())
                    || typeVal.equalsIgnoreCase(ItemType.ADS_TWO_HORIZONTAL.getValue())
                    || typeVal.equalsIgnoreCase(ItemType.ADS_THREE_HORIZONTAL.getValue())
                    || typeVal.equalsIgnoreCase(ItemType.ADS_BANNER.getValue())) {
                return context.deserialize(json, AdsItem.class);
            } else if (typeVal.equalsIgnoreCase(ItemType.SHARED_TIMELINE.getValue())
                    || typeVal.equalsIgnoreCase(ItemType.SHARED_LIVE_VIDEO.getValue())) {
                return context.deserialize(json, SharedTimeline.class);
            } else if (typeVal.equalsIgnoreCase(ItemType.SHARED_ADS_ITEM.getValue())) {
                return context.deserialize(json, ShareAds.class);
            } else if (typeVal.equalsIgnoreCase(ItemType.SHARED_ITEM.getValue())) {
                return context.deserialize(json, SharedProduct.class);
            }
        }
        return context.deserialize(json, Product.class);
    }
}
