package com.sompom.tookitup.helper;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;


/**
 * Created by He Rotha on 9/27/17.
 */

public class AppPauseResumeLifecycle implements Application.ActivityLifecycleCallbacks {
    private int mNumRunningActivities = 0;

    @Override
    public void onActivityStarted(Activity activity) {
        mNumRunningActivities++;
        if (mNumRunningActivities == 1) {
//            if (activity instanceof AbsNotificationActivity) {
//                ((AbsNotificationActivity) activity).onAppResume();
//            }
        }

    }

    @Override
    public void onActivityStopped(Activity activity) {
        mNumRunningActivities--;
        if (mNumRunningActivities == 0) {
//            if (activity instanceof AbsNotificationActivity) {
//                ((AbsNotificationActivity) activity).onAppPause();
//            }
        }
    }


    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    @Override
    public void onActivityResumed(Activity activity) {
    }

    @Override
    public void onActivityPaused(Activity activity) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
    }
}
