package com.sompom.tookitup.helper;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;

/**
 * Created by He Rotha on 11/2/18.
 */
public abstract class ScrimColorAppbarListener implements AppBarLayout.OnOffsetChangedListener {
    private final ValueAnimator mExpendColor;
    private final ValueAnimator mCollapseColor;
    private State mState = State.Expend;
    private CollapsingToolbarLayout mCollapsingToolbarLayout;

    public ScrimColorAppbarListener(CollapsingToolbarLayout collapsingToolbarLayout) {
        mCollapsingToolbarLayout = collapsingToolbarLayout;

        int start = Color.BLACK;
        int end = Color.WHITE;

        mCollapseColor = ValueAnimator.ofObject(new ArgbEvaluator(), end, start);
        mCollapseColor.setDuration(mCollapsingToolbarLayout.getScrimAnimationDuration()); // milliseconds
        mCollapseColor.addUpdateListener(animator -> {
            onColorChange((Integer) animator.getAnimatedValue());
        });

        mExpendColor = ValueAnimator.ofObject(new ArgbEvaluator(), start, end);
        mExpendColor.setDuration(mCollapsingToolbarLayout.getScrimAnimationDuration()); // milliseconds
        mExpendColor.addUpdateListener(animator -> {
            onColorChange((Integer) animator.getAnimatedValue());
        });
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        int scrimSize = mCollapsingToolbarLayout.getScrimVisibleHeightTrigger();
        boolean isExpend = mCollapsingToolbarLayout.getHeight() - scrimSize >= Math.abs(verticalOffset);
        if (isExpend && mState == State.Collapse) {
            mState = State.Expend;
            onStateChange(mState);
            mCollapseColor.cancel();
            if (!mExpendColor.isRunning()) {
                mExpendColor.start();
            }
        } else if (!isExpend && mState == State.Expend) {
            mState = State.Collapse;
            onStateChange(mState);
            mExpendColor.cancel();
            if (!mCollapseColor.isRunning()) {
                mCollapseColor.start();
            }
        }
    }

    public void onStateChange(State state) {
        //do nothing
    }

    public void onColorChange(int color) {
        //do nothing

    }

    public enum State {
        Collapse, Expend
    }

}
