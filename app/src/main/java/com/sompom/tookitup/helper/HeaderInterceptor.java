package com.sompom.tookitup.helper;

import android.content.Context;
import android.text.TextUtils;

import com.sompom.tookitup.R;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import timber.log.Timber;

/**
 * Created by He Rotha on 9/29/17.
 */

public class HeaderInterceptor implements Interceptor {
    private static final String AUTHENTICATION = "Authorization";
    private static final String BEARER = "Bearer ";
    private Context mContext;

    public HeaderInterceptor(Context context) {
        this.mContext = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder builder = chain
                .request()
                .newBuilder()
                .addHeader("Device", mContext.getString(R.string.device))
                .addHeader("Accept-Language", SharedPrefUtils.getLanguage(mContext));

        final String token = SharedPrefUtils.getAccessToken(mContext);
        Timber.e("this is token: " + token);
        if (!TextUtils.isEmpty(token)) {
            builder.addHeader(AUTHENTICATION, BEARER + token);
        }

        Request request = builder.build();
        return chain.proceed(request);
    }
}
