package com.sompom.tookitup.helper;

import android.text.TextUtils;

import com.sompom.tookitup.utils.GenerateLinkPreviewUtil;

import timber.log.Timber;

/**
 * Created by He Rotha on 2/23/18.
 */

public abstract class UrlDetectionWatcher extends DelayWatcher {

    public UrlDetectionWatcher() {
        super(true);
    }

    @Override
    public void onTextChanged(String text) {
        Timber.e("shouldDetectUrl: %s", shouldDetectUrl());
        if (shouldDetectUrl()) {
            Timber.e("onTextChanged: %s", text);
            String link = GenerateLinkPreviewUtil.getPreviewLink(text);
            if (!TextUtils.isEmpty(link)) {
                onUrlInput(link);
            }
        }
    }

    public boolean shouldDetectUrl() {
        return true;
    }

    public abstract void onUrlInput(String url);
}
