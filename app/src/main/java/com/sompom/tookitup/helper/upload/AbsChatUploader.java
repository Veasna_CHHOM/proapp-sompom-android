package com.sompom.tookitup.helper.upload;

import android.content.Context;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.policy.UploadPolicy;
import com.sompom.tookitup.R;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Map;

import timber.log.Timber;

/**
 * Created by imac on 4/1/16.
 */
public class AbsChatUploader extends AbsMyUploader {
    private final String mImagePath;
    private final String mFileName;
    private final String mPreset;
    private final String mTag;
    private final FileType mFileType;

    /**
     * Constructor.
     *
     * @param context   :
     * @param imagePath : String the path of the image file in phone, where user want to us to upload.
     */
    public AbsChatUploader(final Context context,
                           FileType fileType,
                           final String imagePath) {
        mImagePath = imagePath;
        mPreset = context.getString(R.string.tookitup_image_preset);
        mTag = context.getString(R.string.chat_tag);
        mFileName = "chat_" + SharedPrefUtils.getUserId(context) + "_" + System.currentTimeMillis();
        mFileType = fileType;
        Timber.e(mPreset + ", " + mTag + ", " + mFileName + ", " + mImagePath);
    }

    public static void cancelUpload(String requestId) {
        MediaManager.get().cancelRequest(requestId);
    }

    /**
     * Call for Process upload image.
     */
    public String doUpload() {
        UploadPolicy uploadPolicy = new UploadPolicy.Builder().maxRetries(0).build();
        return MediaManager.get()
                .upload(new PayloadBuilder(mFileType, mImagePath).build())
                .option("resource_type", "auto")
                .option("public_id", mFileName)
                .option("upload_preset", mPreset)
                .option("tags", mTag)
                .policy(uploadPolicy)
                .callback(this)
                .dispatch();
    }

    @Override
    public void onSuccess(String requestId, Map resultData) {
        try {
            Timber.v("onSuccess: %s", resultData.toString());
            ArrayList eager = (ArrayList) resultData.get("eager");
            //when preset has transform, eager will be not null,
            String thumb = null;
            if (eager != null) {
                thumb = (String) ((Map) eager.get(0)).get("url");
            }
            //otherwise, preset doesn't have transform, so we origin url
            String url = (String) resultData.get("url");
            mUploadListener.onUploadSucceeded(requestId, url, thumb);
        } catch (Exception ex) {
            mUploadListener.onUploadFail(requestId, ex);
        }
    }

}
