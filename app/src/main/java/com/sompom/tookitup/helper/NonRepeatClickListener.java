package com.sompom.tookitup.helper;

import android.view.View;

/**
 * Created by imac on 5/16/17.
 */

public abstract class NonRepeatClickListener implements View.OnClickListener {
    @Override
    public void onClick(final View v) {
        v.setOnClickListener(null);
        v.postDelayed(new Runnable() {
            @Override
            public void run() {
                v.setOnClickListener(NonRepeatClickListener.this);
            }
        }, 500);
        onNonRepeatClick(v);
    }

    public abstract void onNonRepeatClick(View v);
}
