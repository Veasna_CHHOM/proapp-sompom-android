package com.sompom.tookitup.helper;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.sompom.tookitup.chat.MessageState;

import java.util.ArrayList;

/**
 * Created by He Rotha on 1/23/19.
 */
public class StateCache extends ArrayList<StateCache.StateCacheModel> {

    @Nullable
    public MessageState getById(String id) {
        for (StateCacheModel model : this) {
            if (TextUtils.equals(id, model.mId)) {
                return model.mMessageState;
            }
        }
        return null;
    }

    public void addById(String id, String messageId) {
        for (StateCacheModel model : this) {
            if (TextUtils.equals(id, model.mId)) {
                return;
            }
        }
        add(new StateCacheModel(id, messageId, MessageState.SENDING));
    }

    @Nullable
    public String updateStateByMessageId(String messageId, MessageState state) {
        for (StateCacheModel model : this) {
            if (TextUtils.equals(messageId, model.mMessageId)) {
                model.mMessageState = state;
                return model.mId;
            }
        }
        return null;
    }


    static class StateCacheModel {
        private final String mId;
        private final String mMessageId;
        private MessageState mMessageState;

        StateCacheModel(String id, String messageId, MessageState messageState) {
            mId = id;
            mMessageId = messageId;
            mMessageState = messageState;
        }
    }

}
