package com.sompom.tookitup.helper.upload;

/**
 * Created by He Rotha on 6/20/18.
 */
public interface UploadListener {
    void onUploadSucceeded(String requestId, String imageUrl, String thumb);

    void onUploadFail(String id, Exception ex);

    void onUploadProgressing(String id, int percent);

    void onCancel(String id);
}
