package com.sompom.tookitup.helper;

import android.content.Context;
import android.graphics.Canvas;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.sompom.tookitup.R;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.viewmodel.binding.ImageViewBindingUtil;

/**
 * Created by He Rotha on 12/27/18.
 */
public class ChatStatusItemDecoration extends RecyclerView.ItemDecoration {
    private final int mSize;
    private final int mOriginSize;
    private final DecorateItem mDecorateItem;
    private final int mPaddingItem;
    private final ImageView mImageView;

    public ChatStatusItemDecoration(Context context,
                                    DecorateItem decorateItem,
                                    User user) {
        mDecorateItem = decorateItem;
        mPaddingItem = context.getResources().getDimensionPixelSize(R.dimen.space_small);
        mOriginSize = context.getResources().getDimensionPixelSize(R.dimen.chat_height);
        mSize = (int) (mOriginSize * 0.7);
        mImageView = new ImageView(context);
        mImageView.layout(0, 0, mSize, mSize);

        ImageViewBindingUtil.setUserUrl(mImageView, user);
    }

    @Override
    public void onDrawOver(@NonNull Canvas c,
                           @NonNull RecyclerView parent,
                           @NonNull RecyclerView.State state) {
        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);
            if (child == null) {
                return;
            }

            int realPos = parent.getChildAdapterPosition(child);
            if (realPos == RecyclerView.NO_POSITION) {
                return;
            }
            boolean isSeen = mDecorateItem.isSeen(realPos);
            if (isSeen) {
                final int top = child.getBottom() - mSize;
                final int left = parent.getWidth() - mSize / 2 - (mPaddingItem * 2 + mOriginSize) / 2;

                c.save();
                c.translate(left, top);
                mImageView.draw(c);
                c.restore();
            }
        }
    }

    public interface DecorateItem {
        boolean isSeen(int position);
    }
}
