package com.sompom.tookitup.helper;

import com.sompom.tookitup.adapter.newadapter.TimelineAdapter;
import com.sompom.tookitup.listener.OnTimelineHeaderItemClickListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.newui.AbsBaseActivity;

/**
 * Created by He Rotha on 2/1/19.
 */
public class WallStreetIntentData implements IntentData {
    private final AbsBaseActivity mContext;
    private final OnTimelineHeaderItemClickListener mClickListener;
    private int mMediaPosition;

    public WallStreetIntentData(AbsBaseActivity context,
                                OnTimelineHeaderItemClickListener clickListener) {
        mContext = context;
        mClickListener = clickListener;
    }

    public void setMediaPosition(int mediaPosition) {
        mMediaPosition = mediaPosition;
    }

    @Override
    public int getMediaClickPosition() {
        return mMediaPosition;
    }

    @Override
    public AbsBaseActivity getActivity() {
        return mContext;
    }

    @Override
    public void onDataResultCallBack(Adaptive adaptive, boolean isRemove) {
        final boolean isTimelineAdapter = mClickListener.getRecyclerView().getAdapter() instanceof TimelineAdapter;
        if (!isTimelineAdapter) {
            return;
        }
        TimelineAdapter adapter = (TimelineAdapter) mClickListener.getRecyclerView().getAdapter();
        if (adaptive != null) {
            if (isRemove) {
                adapter.remove(adaptive);
            } else {
                adapter.update(adaptive);
            }
        }

        if (!isFloatingVideoPlaying()) {
            mClickListener.getRecyclerView().resumeAfterRecycle();
        }

    }

    private boolean isFloatingVideoPlaying() {
        return getActivity().getFloatingVideo() != null && getActivity().getFloatingVideo().isFloating();
    }
}
