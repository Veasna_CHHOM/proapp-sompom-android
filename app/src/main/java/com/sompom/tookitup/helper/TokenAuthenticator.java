package com.sompom.tookitup.helper;

import android.content.Context;
import android.text.TextUtils;

import com.sompom.tookitup.TookitupApplication;
import com.sompom.tookitup.injection.controller.PublicQualifier;
import com.sompom.tookitup.injection.token.TokenComponent;
import com.sompom.tookitup.injection.token.TokenModule;
import com.sompom.tookitup.model.request.RefreshTokenRequest;
import com.sompom.tookitup.model.result.Result;
import com.sompom.tookitup.model.result.Token;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;
import retrofit2.Call;
import timber.log.Timber;

/**
 * Created by He Rotha on 9/29/17.
 */

public class TokenAuthenticator implements Authenticator {
    private static final String AUTHENTICATION = "Authorization";
    private static final String BEARER = "Bearer ";
    @Inject
    @PublicQualifier
    public ApiService mApiService;

    private Context mContext;

    public TokenAuthenticator(Context context) {
        mContext = context;
    }

    @Override
    public Request authenticate(Route route, final Response response) throws IOException {
        TokenComponent broadcastModule = ((TookitupApplication) mContext.getApplicationContext())
                .getApplicationComponent()
                .newTokenComponent(new TokenModule(mContext));
        broadcastModule.inject(this);

        Timber.e("authenticate");
        String userId = SharedPrefUtils.getUserId(mContext);

        if (!TextUtils.isEmpty(userId)) {
            Timber.e("request new access token");
            RefreshTokenRequest refreshTokenRequest = new RefreshTokenRequest(userId);
            Call<Result<Token>> callback = mApiService.getRefreshToken(refreshTokenRequest);
            retrofit2.Response<Result<Token>> res = callback.execute();
            if (res.body() == null) {
                Timber.e("error generate token, server return body null");
                return null;
            }
            if (res.body().getData() == null) {
                Timber.e("error generate token, server return data null");
                return null;
            }
            Timber.e("access token recieve: " + res.body().getData().getAccessToken());
            SharedPrefUtils.setAccessToken(res.body().getData().getAccessToken(), mContext);
            if (responseCount(response) == 1) {
                return response.request().newBuilder()
                        .header(AUTHENTICATION, BEARER + SharedPrefUtils.getAccessToken(mContext)).build();
            } else {
                Timber.e("The amount of entering authenticate method for this request is more than 1 time.");
            }
        } else {
            Timber.e("cannot request new access token due user Id is null");
        }

        return null;
    }

    private int responseCount(Response response) {
        int result = 1;
        while ((response = response.priorResponse()) != null) {
            result++;
        }
        return result;
    }

}
