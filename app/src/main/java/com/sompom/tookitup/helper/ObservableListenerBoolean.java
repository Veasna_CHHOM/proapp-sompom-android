package com.sompom.tookitup.helper;

import android.databinding.ObservableBoolean;

/**
 * Created by He Rotha on 12/14/17.
 */

public class ObservableListenerBoolean extends ObservableBoolean {
    private OnValueSetListener mOnValueSetListener;
    private boolean mIsShowKeyboard = true;

    public ObservableListenerBoolean(boolean value) {
        super(value);
    }

    public ObservableListenerBoolean() {
    }

    public void setShowKeyboard(boolean showKeyboard) {
        mIsShowKeyboard = showKeyboard;
    }

    @Override
    public void set(boolean value) {
        super.set(value);
        if (mIsShowKeyboard && mOnValueSetListener != null) {
            mOnValueSetListener.onValueSet(value);
        }
    }

    public void setOnValueSetListener(OnValueSetListener onValueSetListener) {
        mOnValueSetListener = onValueSetListener;
    }

    public interface OnValueSetListener {
        void onValueSet(boolean isSet);
    }
}
