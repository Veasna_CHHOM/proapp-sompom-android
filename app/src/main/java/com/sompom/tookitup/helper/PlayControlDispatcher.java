package com.sompom.tookitup.helper;

import com.google.android.exoplayer2.ControlDispatcher;
import com.google.android.exoplayer2.Player;

/**
 * Created by He Rotha on 2/6/19.
 */
public abstract class PlayControlDispatcher implements ControlDispatcher {

    @Override
    public boolean dispatchSetPlayWhenReady(Player player, boolean playWhenReady) {
        player.setPlayWhenReady(playWhenReady);
        onButtonPlayClick(playWhenReady);
        return true;
    }

    @Override
    public boolean dispatchSeekTo(Player player, int windowIndex, long positionMs) {
        player.seekTo(windowIndex, positionMs);
        return true;
    }

    @Override
    public boolean dispatchSetRepeatMode(Player player, @Player.RepeatMode int repeatMode) {
        player.setRepeatMode(repeatMode);
        return true;
    }

    @Override
    public boolean dispatchSetShuffleModeEnabled(Player player, boolean shuffleModeEnabled) {
        player.setShuffleModeEnabled(shuffleModeEnabled);
        return true;
    }

    @Override
    public boolean dispatchStop(Player player, boolean reset) {
        player.stop(reset);
        return true;
    }

    public abstract void onButtonPlayClick(boolean isPlay);

}
