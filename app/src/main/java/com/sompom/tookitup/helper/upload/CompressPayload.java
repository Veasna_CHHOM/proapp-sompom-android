package com.sompom.tookitup.helper.upload;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;

import com.cloudinary.android.payload.ByteArrayPayload;
import com.desmond.squarecamera.utils.media.ImageUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

/**
 * Created by He Rotha on 9/4/18.
 */
public class CompressPayload extends ByteArrayPayload {

    public CompressPayload(String filePath) {
        data = convertFileToByteArray(filePath);
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    private byte[] convertFileToByteArray(String filePath) {
        byte[] bytesArray = null;
        File file = new File(filePath);
        try {
            ExifInterface exif = new ExifInterface(file.getAbsolutePath());

            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            int rotationInDegrees = exifToDegrees(rotation);
            Matrix matrix = new Matrix();
            if (rotation != 0f) {
                matrix.preRotate(rotationInDegrees);
            }

            Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            bitmap = ImageUtil.rotateImageWithoutCompress(bitmap, rotationInDegrees);
            bitmap = ImageUtil.resizeBitmap(bitmap);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            bytesArray = stream.toByteArray();
            bitmap.recycle();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return bytesArray;
    }
}
