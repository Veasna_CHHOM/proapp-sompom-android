package com.sompom.tookitup.helper.upload;

import android.content.Context;

import com.cloudinary.android.MediaManager;
import com.sompom.tookitup.R;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Map;

import timber.log.Timber;

/**
 * Created by imac on 4/1/16.
 */
public class AbsProductUploader extends AbsMyUploader {
    private Context mContext;
    private String mImagePath;
    private String mFileName;
    private String mPreset;
    private String mTag;

    /**
     * Constructor.
     *
     * @param context   :
     * @param imagePath : String the path of the image file in phone, where user want to us to upload.
     */
    public AbsProductUploader(final Context context,
                              final String imagePath,
                              final FileType fileType,
                              final Orientation orientation) {
        mContext = context;
        mImagePath = imagePath;
        mPreset = mContext.getString(R.string.product_preset);
        mTag = mContext.getString(R.string.product_tag);
        mFileName = "product_" + SharedPrefUtils.getUserId(context) + "_" + System.currentTimeMillis();
        Timber.e(mPreset + ", " + mTag + ", " + mFileName + ", " + mImagePath);
    }

    public static void cancelUpload(String requestId) {
        MediaManager.get().cancelRequest(requestId);
    }

    /**
     * Call for Process upload image.
     */
    public String doUpload() {
        return MediaManager.get()
                .upload(new PayloadBuilder(FileType.IMAGE, mImagePath).build())
                .option("resource_type", "auto")
                .option("public_id", mFileName)
                .option("upload_preset", mPreset)
                .option("tags", mTag)
                .callback(this)
                .dispatch();
    }

    @Override
    public void onSuccess(String requestId, Map resultData) {
        try {
            Timber.v("onSuccess: %s", resultData.toString());
            ArrayList eager = (ArrayList) resultData.get("eager");
            //when preset has transform, eager will be not null,
            String thumb = null;
            if (eager != null) {
                thumb = (String) ((Map) eager.get(0)).get("url");
            }
            //otherwise, preset doesn't have transform, so we origin url
            String url = (String) resultData.get("url");
            mUploadListener.onUploadSucceeded(requestId, url, thumb);
        } catch (Exception ex) {
            mUploadListener.onUploadFail(requestId, ex);
        }
    }

}
