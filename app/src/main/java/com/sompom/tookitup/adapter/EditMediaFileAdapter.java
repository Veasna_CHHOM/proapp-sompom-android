package com.sompom.tookitup.adapter;

import android.databinding.DataBindingUtil;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.ListItemEditMediaFileBinding;
import com.sompom.tookitup.listener.OnRemoveMediaFileItemListener;
import com.sompom.tookitup.model.LifeStream;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.emun.MediaType;
import com.sompom.tookitup.utils.VolumePlaying;
import com.sompom.tookitup.viewholder.BindingViewHolder;
import com.sompom.tookitup.viewholder.TimelineViewHolder;
import com.sompom.tookitup.viewmodel.ListItemEditMediaFileViewModel;
import com.sompom.tookitup.widget.lifestream.MediaLayout;

/**
 * Created by He Rotha on 9/6/17.
 */

public class EditMediaFileAdapter extends RecyclerView.Adapter<BindingViewHolder> {
    private final LifeStream mLifeStream;
    private final OnRemoveMediaFileItemListener mOnCallback;
    private final boolean mIsEdit;

    public EditMediaFileAdapter(LifeStream lifeStream, boolean isEdit, OnRemoveMediaFileItemListener onCallback) {
        mLifeStream = lifeStream;
        mIsEdit = isEdit;
        mOnCallback = onCallback;
    }

    @NonNull
    @Override
    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListItemEditMediaFileBinding binding;
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.list_item_edit_media_file, parent, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            binding.mediaLayouts.setTransitionName(parent.getResources().getString(R.string.media_transition_anim) + viewType);
        }
        return new TimelineViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
        ListItemEditMediaFileBinding binding = (ListItemEditMediaFileBinding) holder.getBinding();
        Media media = mLifeStream.getMedia().get(position);
        MediaLayout mediaLayout = binding.mediaLayouts;
        mediaLayout.setLayoutParams(getLayoutParam(mediaLayout, media));
        mediaLayout.setCenterCropImage();
        mediaLayout.setMedia(media);
        if (mIsEdit) {
            mediaLayout.setMute(VolumePlaying.isMute(mediaLayout.getContext()));
            mediaLayout.setVisibleMuteButton(true);
        } else {
            mediaLayout.setVisibleCloseButton();
        }
        mediaLayout.setOnCloseButtonClickedListener(v -> {
            if (mOnCallback != null) {
                mOnCallback.onRemoveItem(holder.getAdapterPosition());
            }
        });
        if (media.getType() == MediaType.VIDEO) {
            mediaLayout.setShowControl(true);
            mediaLayout.setSaveOnPause(true);
            if (holder instanceof TimelineViewHolder) {
                ((TimelineViewHolder) holder).setMediaLayout(mediaLayout);
            }
        }

        ListItemEditMediaFileViewModel lifeStreamViewModel = new ListItemEditMediaFileViewModel(media.getTitle(),
                result -> mLifeStream.getMedia().get(position).setTitle(result));
        holder.setVariable(BR.viewModel, lifeStreamViewModel);
    }

    @Override
    public int getItemCount() {
        return mLifeStream.getMedia().size();
    }

    @Override
    public void onViewRecycled(@NonNull BindingViewHolder holder) {
        super.onViewRecycled(holder);
        if (holder instanceof TimelineViewHolder) {
            ((TimelineViewHolder) holder).stopOrRelease();
        }
    }

    private ViewGroup.LayoutParams getLayoutParam(MediaLayout mediaLayout, Media media) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((AppCompatActivity) mediaLayout.getContext()).getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int screenWidth = metrics.widthPixels;
        ViewGroup.LayoutParams param = mediaLayout.getLayoutParams();
        int size;
        if (media.getWidth() == 0 || media.getHeight() == 0) {
            size = screenWidth;
        } else {
            size = media.getHeight() * screenWidth / media.getWidth();
        }
        param.width = screenWidth;
        param.height = size;
        return param;
    }
}
