package com.sompom.tookitup.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.sompom.tookitup.R;

/**
 * Created by He Rotha on 11/26/18.
 */
public class SingleChoiceAdapter extends ArrayAdapter<String> {

    private int mSelectPosition;
    private String[] mTitles;
    private DialogInterface.OnClickListener mSingleChoiceListener;
    private Context mContext;

    public SingleChoiceAdapter(Context context,
                               String[] title,
                               int selectPosition,
                               DialogInterface.OnClickListener listener) {
        super(context, R.layout.list_item_single_choice, title);
        mSelectPosition = selectPosition;
        mTitles = title;
        mSingleChoiceListener = listener;
        mContext = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.list_item_single_choice, parent, false);

        }
        AppCompatRadioButton radioButton = convertView.findViewById(R.id.radioButton);
        radioButton.setText(mTitles[position]);
        radioButton.setOnCheckedChangeListener(null);

        if (position == mSelectPosition) {
            radioButton.setChecked(true);
        } else {
            radioButton.setChecked(false);
        }

        radioButton.setOnCheckedChangeListener((compoundButton, isCheck) -> {
            if (isCheck) {
                mSelectPosition = position;
                mSingleChoiceListener.onClick(null, mSelectPosition);
            }
            notifyDataSetChanged();
        });
        return convertView;
    }

}
