package com.sompom.tookitup.adapter.newadapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdView;
import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.RefreshableAdapter;
import com.sompom.tookitup.databinding.LayoutAdmobBinding;
import com.sompom.tookitup.databinding.ListItemProductMediaBinding;
import com.sompom.tookitup.databinding.ListItemTimelineBinding;
import com.sompom.tookitup.databinding.ListItemTimelineMediaBinding;
import com.sompom.tookitup.databinding.ListItemTimelineSharedLiveVideoBinding;
import com.sompom.tookitup.listener.OnTimelineItemButtonClickDelegateListener;
import com.sompom.tookitup.listener.OnTimelineItemButtonClickListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.LifeStream;
import com.sompom.tookitup.model.ShareAds;
import com.sompom.tookitup.model.SharedProduct;
import com.sompom.tookitup.model.SharedTimeline;
import com.sompom.tookitup.model.emun.ViewType;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.WallStreetDataManager;
import com.sompom.tookitup.viewholder.BindingViewHolder;
import com.sompom.tookitup.viewholder.TimelineViewHolder;
import com.sompom.tookitup.viewmodel.BannerViewModel;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemMainTimelineViewModel;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemTimelineLiveViewModel;
import com.sompom.tookitup.widget.lifestream.CollageView;

import java.util.List;

import timber.log.Timber;

/**
 * Created by He Rotha on 7/10/18.
 */
public class TimelineAdapter extends RefreshableAdapter<Adaptive, BindingViewHolder> {
    private final OnTimelineItemButtonClickDelegateListener mOnItemClickListener;

    private final WallStreetDataManager mWallStreetDataManager;
    private final ApiService mApiService;
    private final AbsBaseActivity mContext;
    private List<AdView> mAdViews;

    public TimelineAdapter(AbsBaseActivity context,
                           ApiService apiService,
                           List<Adaptive> datas,
                           OnTimelineItemButtonClickListener listener) {
        super(datas);
        mContext = context;
        mApiService = apiService;
        mOnItemClickListener = new OnTimelineItemButtonClickDelegateListener(listener) {
            @Override
            public void onFollowButtonClick(String id, boolean isFollowing) {
                super.onFollowButtonClick(id, isFollowing);
                for (int i = 0; i < mDatas.size(); i++) {
                    Adaptive data = mDatas.get(i);
                    if (data instanceof LifeStream) {
                        check(i, ((LifeStream) data).getUser(), id, isFollowing);
                    } else if (data instanceof SharedTimeline) {
                        check(i, ((SharedTimeline) data).getUser(), id, isFollowing);
                        check(i, ((SharedTimeline) data).getLifeStream().getUser(), id, isFollowing);
                    } else if (data instanceof Product) {
                        check(i, ((Product) data).getUser(), id, isFollowing);
                    } else if (data instanceof SharedProduct) {
                        check(i, ((SharedProduct) data).getUser(), id, isFollowing);
                        check(i, ((SharedProduct) data).getProduct().getUser(), id, isFollowing);
                    } else if (data instanceof ShareAds) {
                        check(i, ((ShareAds) data).getUser(), id, isFollowing);
                    }
                }
            }

            private void check(int position, User user, String id, boolean isFollow) {
                if (TextUtils.equals(user.getId(), id) && user.isFollow() != isFollow) {
                    user.setFollow(isFollow);
                    notifyItemChanged(position);
                }
            }
        };
        mWallStreetDataManager = new WallStreetDataManager(mContext, mApiService, null, null);

    }


    @Override
    public void onViewRecycled(@NonNull BindingViewHolder holder) {
        super.onViewRecycled(holder);
        //release auto play video
        if (holder instanceof TimelineViewHolder) {
            ((TimelineViewHolder) holder).stopOrRelease();
        }
    }

    public void setAdViews(List<AdView> adViews) {
        mAdViews = adViews;
    }

    public void remove(Adaptive adaptive) {
        for (int i = 0; i < mDatas.size(); i++) {
            if (TextUtils.equals(mDatas.get(i).getId(), adaptive.getId())) {
                mDatas.remove(i);
                notifyItemRemoved(i);
                break;
            }
        }
    }

    public void update(Adaptive adaptive) {
        Timber.e("start update %s", adaptive.getId());
        for (int i = 0; i < mDatas.size(); i++) {
            if (TextUtils.equals(mDatas.get(i).getId(), adaptive.getId())) {
                updateItem(adaptive, i);
                break;
            }
        }

        Timber.e("stop update ");

    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
//      WallStreet Style 1
        if (viewType >= ViewType.Product_FreeStyle_1.getViewType() && viewType <= ViewType.Product_SquareAll_5.getViewType()) {
//        WallStreet Style 2
//        if (viewType >= ViewType.ProductThreeOneItem.getViewType() && viewType <= ViewType.ProductThreeThreeItem.getViewType()) {
            final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            final ViewDataBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.list_item_product_media, parent, false);
            TimelineViewHolder holder = new TimelineViewHolder(dataBinding);
            ListItemProductMediaBinding binding = (ListItemProductMediaBinding) holder.getBinding();
            CollageView collageView = binding.mediaLayouts;
            ViewType v = ViewType.fromValue(viewType);
            collageView.setOrientation(v.getOrientation());
            collageView.setNumberOfItem(v.getNumberOfItem());
            collageView.setFormat(v.getFormat());
            collageView.startGenerateView();
            return holder;
        } else if (viewType == ViewType.LiveVideoTimeline.getTimelineViewType()) {
            final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            final ViewDataBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.list_item_timeline_shared_live_video, parent, false);
            return new TimelineViewHolder(dataBinding);
        } else if (viewType == ViewType.Ads.getTimelineViewType()) {
            return new BindingViewHolder.Builder(parent, R.layout.layout_admob).build();
        } else if (viewType == ViewType.Timeline.getTimelineViewType()) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_timeline).build();
        } else {
            final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            final ViewDataBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.list_item_timeline_media, parent, false);
            TimelineViewHolder holder = new TimelineViewHolder(dataBinding);
            ListItemTimelineMediaBinding binding = (ListItemTimelineMediaBinding) holder.getBinding();
            CollageView collageView = binding.mediaLayouts;
            ViewType v = ViewType.fromValue(viewType);
            collageView.setOrientation(v.getOrientation());
            collageView.setNumberOfItem(v.getNumberOfItem());
            collageView.setFormat(v.getFormat());
            collageView.startGenerateView();
            return holder;
        }
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        if (holder.getBinding() instanceof ListItemProductMediaBinding
                || holder.getBinding() instanceof ListItemTimelineBinding
                || holder.getBinding() instanceof ListItemTimelineMediaBinding) {
            ListItemMainTimelineViewModel viewModel = new ListItemMainTimelineViewModel(mContext, holder,
                    mWallStreetDataManager,
                    mDatas.get(position),
                    holder.getAdapterPosition(),
                    mOnItemClickListener);
            holder.setVariable(BR.viewModel, viewModel);
        } else if (holder.getBinding() instanceof ListItemTimelineSharedLiveVideoBinding) {
            ListItemTimelineLiveViewModel viewModel = new ListItemTimelineLiveViewModel(mContext, holder,
                    mDatas.get(position),
                    mWallStreetDataManager,
                    holder.getAdapterPosition(),
                    mOnItemClickListener);
            holder.setVariable(BR.viewModel, viewModel);
        } else if (holder.getBinding() instanceof LayoutAdmobBinding) {
            final BannerViewModel bannerViewModel = new BannerViewModel(position, mAdViews);
            ((LayoutAdmobBinding) holder.getBinding()).setViewModel(bannerViewModel);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position >= 0 && position < mDatas.size()) {
            return mDatas.get(position).getTimelineViewType().getTimelineViewType();
        }
        return super.getItemViewType(position);
    }

    public ApiService getApiService() {
        return mApiService;
    }

    AbsBaseActivity getAbsBaseActivity() {
        return mContext;
    }

    public void updateItem(Adaptive adaptive, int position) {
        mDatas.set(position, adaptive);
        notifyItemChanged(position);

        if (adaptive instanceof LifeStream) {
            User user = ((LifeStream) adaptive).getUser();
            mOnItemClickListener.onFollowButtonClick(user.getId(), user.isFollow());
        } else if (adaptive instanceof SharedTimeline) {
            User user = ((SharedTimeline) adaptive).getUser();
            mOnItemClickListener.onFollowButtonClick(user.getId(), user.isFollow());

            user = ((SharedTimeline) adaptive).getLifeStream().getUser();
            mOnItemClickListener.onFollowButtonClick(user.getId(), user.isFollow());
        } else if (adaptive instanceof Product) {
            User user = ((Product) adaptive).getUser();
            mOnItemClickListener.onFollowButtonClick(user.getId(), user.isFollow());
        } else if (adaptive instanceof SharedProduct) {
            User user = ((SharedProduct) adaptive).getUser();
            mOnItemClickListener.onFollowButtonClick(user.getId(), user.isFollow());

            user = ((SharedProduct) adaptive).getProduct().getUser();
            mOnItemClickListener.onFollowButtonClick(user.getId(), user.isFollow());
        } else if (adaptive instanceof ShareAds) {
            User user = ((ShareAds) adaptive).getUser();
            mOnItemClickListener.onFollowButtonClick(user.getId(), user.isFollow());
        }
        Timber.e("update " + position);
    }

    public void removeData(int position) {
        mDatas.remove(position);
        notifyItemRemoved(position);
    }
}
