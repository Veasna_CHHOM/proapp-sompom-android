package com.sompom.tookitup.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.sompom.tookitup.newui.fragment.MoreGameFragment;

import java.util.List;

/**
 * Created by he.rotha on 4/29/16.
 */
public class MoreGameAdapter extends FragmentStatePagerAdapter {
    private List<MoreGameFragment> mGameFragments;

    public MoreGameAdapter(FragmentManager fm, List<MoreGameFragment> fr) {
        super(fm);
        mGameFragments = fr;
    }

    @Override
    public Fragment getItem(int position) {
        return mGameFragments.get(position);
    }

    @Override
    public int getCount() {
        return mGameFragments.size();
    }
}
