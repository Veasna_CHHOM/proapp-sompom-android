package com.sompom.tookitup.adapter.newadapter;

import android.view.ViewGroup;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.ListItemCommentBinding;
import com.sompom.tookitup.databinding.ListItemCommentGifBinding;
import com.sompom.tookitup.databinding.ListItemCommentImageBinding;
import com.sompom.tookitup.listener.OnCommentItemClickListener;
import com.sompom.tookitup.listener.OnLoadPreviousCommentClickListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.LoadPreviousComment;
import com.sompom.tookitup.model.emun.MediaType;
import com.sompom.tookitup.model.result.Comment;
import com.sompom.tookitup.viewholder.BindingViewHolder;
import com.sompom.tookitup.viewmodel.newviewmodel.CommentViewModel;

import java.util.List;

/**
 * Created by nuonveyo on 7/23/18.
 */

public class ReplyCommentAdapter extends CommentAdapter {
    private static final int COMMENT_ITEM = 0X0014;
    private static final int GIF_ITEM = 0X0015;
    private static final int IMAGE_ITEM = 0X0016;

    public ReplyCommentAdapter(List<Adaptive> commentList,
                               OnCommentItemClickListener onCommentItemClickListener,
                               OnLoadPreviousCommentClickListener onLoadPreviousItemClickListener) {
        super(commentList, onCommentItemClickListener, onLoadPreviousItemClickListener);
        setReplyComment();
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        if (viewType == GIF_ITEM) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_comment_gif).build();
        } else if (viewType == IMAGE_ITEM) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_comment_image).build();
        } else if (viewType == COMMENT_ITEM) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_comment).build();
        } else {
            return super.onCreateView(parent, viewType);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position >= 0 && position < mDatas.size()) {
            if (position == 0 || mDatas.get(position) instanceof LoadPreviousComment) {
                return super.getItemViewType(position);
            } else {
                if (((Comment) mDatas.get(position)).getMedia() == null || ((Comment) mDatas.get(position)).getMedia().isEmpty()) {
                    return COMMENT_ITEM;
                } else {
                    if (((Comment) mDatas.get(position)).getMedia().get(0).getType() == MediaType.TENOR_GIF) {
                        return GIF_ITEM;
                    } else if (((Comment) mDatas.get(position)).getMedia().get(0).getType() == MediaType.IMAGE) {
                        return IMAGE_ITEM;
                    }
                }
            }
        }
        return super.getItemViewType(position);
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        if (holder.getBinding() instanceof ListItemCommentGifBinding
                || holder.getBinding() instanceof ListItemCommentImageBinding
                || holder.getBinding() instanceof ListItemCommentBinding) {
            CommentViewModel viewModel = new CommentViewModel(holder.getContext(),
                    position,
                    (Comment) mDatas.get(position),
                    mOnCommentItemClickListener,
                    true);
            holder.setVariable(BR.viewModel, viewModel);
        } else {
            super.onBindData(holder, position);
        }
    }

    public void addNewComment(List<Comment> comments) {
        int previousItemCount = getItemCount();
        mDatas.addAll(comments);
        notifyItemRangeInserted(previousItemCount + 1, comments.size());
    }
}
