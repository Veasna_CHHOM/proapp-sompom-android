package com.sompom.tookitup.adapter.newadapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by he.rotha on 3/4/16.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private List<Fragment> mFragmentProductDetailItems = new ArrayList<>();
    private List<String> mTitleList = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentProductDetailItems.add(fragment);
        mTitleList.add(title);
    }

    public Fragment getFr(int position) {
        return mFragmentProductDetailItems.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentProductDetailItems.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentProductDetailItems.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mTitleList.get(position);
    }
}
