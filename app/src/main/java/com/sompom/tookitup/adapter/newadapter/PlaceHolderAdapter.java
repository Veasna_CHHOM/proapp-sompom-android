package com.sompom.tookitup.adapter.newadapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.sompom.tookitup.R;
import com.sompom.tookitup.model.emun.PlaceholderItem;
import com.sompom.tookitup.viewholder.BindingViewHolder;

/**
 * Created by He Rotha on 11/6/17.
 */

public class PlaceHolderAdapter extends RecyclerView.Adapter<BindingViewHolder> {
    private static final int HEADER_ITEM = 0x001;
    private int mPlaceHolderId;

    public PlaceHolderAdapter(int placeHolderId) {
        mPlaceHolderId = placeHolderId;
    }

    @NonNull
    @Override
    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == HEADER_ITEM) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_header_placeholder).build();
        } else {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_home_placeholder).build();
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    @Override
    public int getItemViewType(int position) {
        if (mPlaceHolderId == PlaceholderItem.Home.getId() && position == 0) {
            return HEADER_ITEM;
        }
        return super.getItemViewType(position);
    }
}
