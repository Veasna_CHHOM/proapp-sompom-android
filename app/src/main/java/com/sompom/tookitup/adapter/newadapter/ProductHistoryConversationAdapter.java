package com.sompom.tookitup.adapter.newadapter;

import android.content.Context;
import android.view.ViewGroup;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.RefreshableAdapter;
import com.sompom.tookitup.listener.OnCompleteListener;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.viewholder.BindingViewHolder;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemProductHistoryConversation;

import java.util.List;

/**
 * Created by nuonveyo on 6/8/18.
 */

public class ProductHistoryConversationAdapter extends RefreshableAdapter<Conversation, BindingViewHolder> {
    private final Context mContext;
    private final OnCompleteListener<Conversation> mOnItemClickListener;

    public ProductHistoryConversationAdapter(Context context,
                                             List<Conversation> datas,
                                             OnCompleteListener<Conversation> listener) {
        super(datas);
        mContext = context;
        mOnItemClickListener = listener;
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_product_hitory_conversation).build();
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        final ListItemProductHistoryConversation viewModel = new ListItemProductHistoryConversation(mContext,
                mDatas.get(position),
                mOnItemClickListener);
        holder.setVariable(BR.viewModel, viewModel);
    }

}
