package com.sompom.tookitup.adapter.newadapter;

import android.view.ViewGroup;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.RefreshableAdapter;
import com.sompom.tookitup.listener.OnCompleteListener;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.viewholder.BindingViewHolder;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemUserStoreDialogViewModel;

import java.util.List;

/**
 * Created by nuonveyo on 10/25/18.
 */

public class UserStoreDialogAdapter extends RefreshableAdapter<Product, BindingViewHolder> {
    private OnCompleteListener<Product> mOnItemClickListener;

    public UserStoreDialogAdapter(List<Product> list, OnCompleteListener<Product> listener) {
        super(list);
        mOnItemClickListener = listener;
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_user_store_dialog).build();
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        ListItemUserStoreDialogViewModel viewModel = new ListItemUserStoreDialogViewModel(mDatas.get(position), mOnItemClickListener);
        holder.setVariable(BR.viewModel, viewModel);
    }
}
