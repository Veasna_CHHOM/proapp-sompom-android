package com.sompom.tookitup.adapter.newadapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.LayoutChatImageFullScreenBinding;
import com.sompom.tookitup.listener.ChatImageClickCallback;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.viewmodel.newviewmodel.LayoutChatImageFullScreenViewModel;

import java.util.List;

/**
 * Created by nuonveyo on 11/28/17.
 */

public class ChatImageFullScreenPagerAdapter extends PagerAdapter {
    private Context mContext;
    private List<Media> mProductMediaList;
    private OnClickListener mOnClickListener;

    public ChatImageFullScreenPagerAdapter(Context context, List<Media> productMediaList) {
        mContext = context;
        mProductMediaList = productMediaList;
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        LayoutChatImageFullScreenBinding binding = DataBindingUtil.inflate(inflater, R.layout.layout_chat_image_full_screen, container, false);
        binding.setVariable(BR.viewModel, new LayoutChatImageFullScreenViewModel(mProductMediaList.get(position).getUrl()));
        binding.imageView.setOnTouchListener(new ChatImageClickCallback(mContext, mOnClickListener));
        container.addView(binding.getRoot());
        return binding.getRoot();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public int getCount() {
        return mProductMediaList.size();
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
