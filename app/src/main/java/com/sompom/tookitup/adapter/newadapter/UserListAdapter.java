package com.sompom.tookitup.adapter.newadapter;

import android.view.ViewGroup;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.RefreshableAdapter;
import com.sompom.tookitup.listener.UserListAdaptive;
import com.sompom.tookitup.model.UserGroup;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.viewholder.BindingViewHolder;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemUserGroupTitleViewModel;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemUserViewModel;

import java.util.List;

public class UserListAdapter<D extends UserListAdaptive> extends RefreshableAdapter<D, BindingViewHolder> {

    private static final int VIEW_TYPE_HEADER = 1;
    private static final int VIEW_TYPE_PARTICIPANT = 2;

    public UserListAdapter(List<D> datas) {
        super(datas);
        setCanLoadMore(false);
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_HEADER) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_user_group_title).build();
        } else {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_user).build();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mDatas.get(position) instanceof UserGroup) {
            return VIEW_TYPE_HEADER;
        } else if (mDatas.get(position) instanceof User) {
            return VIEW_TYPE_PARTICIPANT;
        } else {
            return super.getItemViewType(position);
        }
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_HEADER) {
            UserGroup group = (UserGroup) mDatas.get(position);
            ListItemUserGroupTitleViewModel viewModel = new ListItemUserGroupTitleViewModel(group.getName());
            holder.getBinding().setVariable(BR.viewModel, viewModel);
        } else {
            User user = (User) mDatas.get(position);
            ListItemUserViewModel viewModel = new ListItemUserViewModel(holder.getContext(), user);
            holder.getBinding().setVariable(BR.viewModel, viewModel);
        }
    }
}
