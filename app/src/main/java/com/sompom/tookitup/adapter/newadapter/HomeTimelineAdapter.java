package com.sompom.tookitup.adapter.newadapter;

import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.OnTimelineActiveUserItemClick;
import com.sompom.tookitup.listener.OnTimelineItemButtonClickListener;
import com.sompom.tookitup.model.ActiveUser;
import com.sompom.tookitup.model.CreateTimelineItem;
import com.sompom.tookitup.model.MoreGameItem;
import com.sompom.tookitup.model.emun.ViewType;
import com.sompom.tookitup.model.result.MoreGame;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.viewholder.BindingViewHolder;
import com.sompom.tookitup.viewmodel.ListItemPostViewModel;
import com.sompom.tookitup.viewmodel.ViewPagerViewModel;

import java.util.List;

/**
 * Created by He Rotha on 7/10/18.
 */
public class HomeTimelineAdapter extends TimelineAdapter {
    private FragmentManager mFragmentManager;
    private ActiveUser mActiveUser;
    private OnTimelineActiveUserItemClick mOnTimelineActiveUserItemClick;
    private List<MoreGame> mMoreGameList;
    private CreateTimelineItem mCreateTimelineItem;

    public HomeTimelineAdapter(AbsBaseActivity context,
                               ApiService apiService,
                               OnTimelineActiveUserItemClick itemClick,
                               OnTimelineItemButtonClickListener onItemClickListener) {
        super(context, apiService, null, onItemClickListener);
        mOnTimelineActiveUserItemClick = itemClick;

//        Disable Post
//        mCreateTimelineItem = new CreateTimelineItem();
//        mDatas.add(0, mCreateTimelineItem);

        //TODO: re-enable after live feature done
//        mDatas.add(1, mActiveUser);
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        if (viewType == ViewType.ActiveUser.getTimelineViewType()) {
            BindingViewHolder holder = new BindingViewHolder.Builder(parent, R.layout.list_item_timeline_activite_user).build();
            RecyclerView recyclerView = holder.itemView.findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(parent.getContext(), LinearLayoutManager.HORIZONTAL, false));
            recyclerView.setAdapter(new TimelineActiveUserAdapter(mActiveUser, mOnTimelineActiveUserItemClick));
            return holder;
        } else if (viewType == ViewType.ViewPagerAd.getTimelineViewType()) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_viewpager).build();
        } else if (viewType == ViewType.NewPost.getTimelineViewType()) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_new_post).build();
        }
        return super.onCreateView(parent, viewType);
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        if (mDatas.get(position) instanceof MoreGameItem) {
            ViewPagerViewModel model = new ViewPagerViewModel(mMoreGameList, mFragmentManager);
            holder.setVariable(BR.viewModel, model);
        } else if (mDatas.get(position) instanceof CreateTimelineItem) {
            holder.setVariable(BR.viewModel, new ListItemPostViewModel(getAbsBaseActivity(), mCreateTimelineItem));
        } else {
            super.onBindData(holder, position);
        }
    }

    public void setActiveUser(ActiveUser activeUser) {
        mActiveUser = new ActiveUser();
        mActiveUser.clear();
        mActiveUser.addAll(activeUser);
        mDatas.set(1, mActiveUser);
        notifyItemChanged(1);
    }

    public void setMoreGame(FragmentManager manager, MoreGameItem moreGame) {
        mFragmentManager = manager;
        if (mMoreGameList == null) {
            mMoreGameList = moreGame;
            /*
               Since we hide the post for now, so the
               more game index should be in the first one, 0 index.
               If we enable back the post, then more game should come after, 1 index.
             */
            mDatas.add(0, new MoreGameItem());
            notifyItemInserted(1);
        } else {
            mMoreGameList = moreGame;
            notifyItemChanged(1);
        }
    }

    public void clearTimeline() {
        int index = -1;
        for (int i = 0; i < mDatas.size(); i++) {
            if (!(mDatas.get(i) instanceof MoreGameItem ||
                    mDatas.get(i) instanceof ActiveUser || mDatas.get(i) instanceof CreateTimelineItem)) {
                index = i;
                break;
            }
        }
        if (index < 0) {
            return;
        }
        mDatas.subList(index, mDatas.size()).clear();
        notifyDataSetChanged();
    }
}
