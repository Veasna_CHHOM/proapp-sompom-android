package com.sompom.tookitup.adapter.newadapter;

import android.text.TextUtils;
import android.view.ViewGroup;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.RefreshableAdapter;
import com.sompom.tookitup.chat.MessageState;
import com.sompom.tookitup.helper.StateCache;
import com.sompom.tookitup.listener.ConversationDataAdaptive;
import com.sompom.tookitup.model.ForwardTitle;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.viewholder.BindingViewHolder;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemForwardViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by He Rotha on 9/13/18.
 */
public class ForwardAdapter extends RefreshableAdapter<ConversationDataAdaptive, BindingViewHolder> {
    private static final int VIEW_TYPE_PEOPLE = 1;
    private static final int VIEW_TYPE_TITLE = 2;
    private final StateCache mStateList = new StateCache();
    private boolean mIsPeopleDataAdded = false;
    private OnForwardItemClickListener mOnForwardItemClickListener;
    private final OnForwardItemClickListener mMyCacheListener = adaptive -> {
        String messageId = mOnForwardItemClickListener.onSendClick(adaptive);
        mStateList.addById(adaptive.getId(), messageId);
        return messageId;
    };

    public ForwardAdapter() {
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_PEOPLE) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_forward_people).build();
        } else {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_forward_title).build();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position > 0 && position < mDatas.size()) {
            if (mDatas.get(position) instanceof User || mDatas.get(position) instanceof Conversation) {
                return VIEW_TYPE_PEOPLE;
            } else {
                return VIEW_TYPE_TITLE;
            }
        }
        return super.getItemViewType(position);
    }

    public void setOnForwardItemClickListener(OnForwardItemClickListener onForwardItemClickListener) {
        mOnForwardItemClickListener = onForwardItemClickListener;
    }

    public void update(String messageId, MessageState state) {
        final String id = mStateList.updateStateByMessageId(messageId, state);
        if (TextUtils.isEmpty(id)) {
            return;
        }
        for (int i = 0; i < mDatas.size(); i++) {
            if (TextUtils.equals(id, mDatas.get(i).getId())) {
                notifyItemChanged(i);
                break;
            }
        }
    }

    public void addSuggestion(List<ConversationDataAdaptive> activeUser) {
        List<ConversationDataAdaptive> data = new ArrayList<>();
        data.add(new ForwardTitle(R.string.forward_suggest_header));
        data.addAll(activeUser);
        addLoadMoreData(data);
    }

    public void addPeople(List<ConversationDataAdaptive> activeUser) {
        List<ConversationDataAdaptive> data = new ArrayList<>();
        if (!mIsPeopleDataAdded) {
            data.add(new ForwardTitle(R.string.forward_people_header));
        }
        data.addAll(activeUser);
        addLoadMoreData(data);
        mIsPeopleDataAdded = true;
    }

    public void update(List<ConversationDataAdaptive> activeUser) {
        mIsPeopleDataAdded = false;
        mDatas.clear();
        if (activeUser != null && !activeUser.isEmpty()) {
            mDatas.add(new ForwardTitle(R.string.forward_people_header));
            mDatas.addAll(activeUser);
        }
        notifyDataSetChanged();
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        ListItemForwardViewModel viewModel = null;
        final MessageState state = mStateList.getById(mDatas.get(position).getId());
        if (mDatas.get(position) instanceof User) {
            viewModel = new ListItemForwardViewModel(holder.getContext(), mDatas.get(position), state, mMyCacheListener);
        } else if (mDatas.get(position) instanceof Conversation) {
            viewModel = new ListItemForwardViewModel(holder.getContext(), ((Conversation) mDatas.get(position)).getRecipient(holder.getContext()), state, mMyCacheListener);
        } else if (mDatas.get(position) instanceof ForwardTitle) {
            viewModel = new ListItemForwardViewModel((ForwardTitle) mDatas.get(position));
        }
        if (viewModel != null) {
            holder.getBinding().setVariable(BR.viewModel, viewModel);
        }
    }

    public interface OnForwardItemClickListener {
        String onSendClick(ConversationDataAdaptive adaptive);
    }

}
