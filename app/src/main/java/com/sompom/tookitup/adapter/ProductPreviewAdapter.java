package com.sompom.tookitup.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.viewholder.BindingViewHolder;
import com.sompom.tookitup.viewmodel.newviewmodel.ProductPreviewViewModel;

import java.util.Collections;
import java.util.List;

/**
 * Created by he.rotha on 2/26/16.
 */
public class ProductPreviewAdapter extends RecyclerView.Adapter<BindingViewHolder> {
    private List<Media> mProductUrl;
    private OnProductItemClickListener mListener;

    public ProductPreviewAdapter(List<Media> datas) {
        mProductUrl = datas;
        Collections.sort(mProductUrl);
    }

    public void setProductItemClickListener(OnProductItemClickListener listener) {
        this.mListener = listener;
    }

    @NonNull
    @Override
    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_image_from).build();
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
        ProductPreviewViewModel data = new ProductPreviewViewModel(mProductUrl.get(position), mListener);
        holder.setVariable(BR.viewModel, data);
    }

    @Override
    public int getItemCount() {
        return mProductUrl.size();
    }

    public interface OnProductItemClickListener {
        void onItemEditClick();
    }
}
