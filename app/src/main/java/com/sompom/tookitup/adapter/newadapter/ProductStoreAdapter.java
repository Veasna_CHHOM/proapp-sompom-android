package com.sompom.tookitup.adapter.newadapter;

import android.view.ViewGroup;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.RefreshableAdapter;
import com.sompom.tookitup.listener.OnLikeProductListener;
import com.sompom.tookitup.listener.OnProductItemClickListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.emun.Status;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.viewholder.BindingViewHolder;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemProductStoreViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nuonveyo on 6/5/18.
 */

public class ProductStoreAdapter extends RefreshableAdapter<Adaptive, BindingViewHolder> {
    private final boolean mIsEditable;
    private final boolean mIsMe;
    private final boolean mIsShowImageProfile;
    private final int mProductStatusId;
    private final OnProductItemClickListener mOnProductItemClickListener;
    private final String mUserId;
    private OnLikeProductListener mOnLikeProductListener;
    private AbsBaseActivity mActivity;

    public ProductStoreAdapter(AbsBaseActivity activity,
                               List<Adaptive> productList,
                               String userId,
                               boolean isMe,
                               boolean isEditable,
                               boolean isShowImageProfile,
                               int productStatusId,
                               OnProductItemClickListener onProductItemClickListener) {
        super(productList);
        mActivity = activity;
        mUserId = userId;
        mIsMe = isMe;
        mIsEditable = isEditable;
        mIsShowImageProfile = isShowImageProfile;
        mProductStatusId = productStatusId;
        mOnProductItemClickListener = onProductItemClickListener;
    }

    public static ProductStoreAdapter newRelatedInstance(AbsBaseActivity activity,
                                                         List<Product> productList,
                                                         OnProductItemClickListener onProductItemClickListener) {
        return new ProductStoreAdapter(activity,
                new ArrayList<>(productList),
                "",
                false,
                false,
                true,
                Status.ON_SALE.getStatusProduct(), onProductItemClickListener);
    }

    public void setOnLikeProductListener(OnLikeProductListener onLikeProductListener) {
        mOnLikeProductListener = onLikeProductListener;
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {

        return new BindingViewHolder.Builder(parent, R.layout.list_item_product_store).build();


    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        ListItemProductStoreViewModel viewModel = new ListItemProductStoreViewModel(mActivity,
                (Product) mDatas.get(position),
                mUserId,
                mIsMe,
                mIsEditable,
                mIsShowImageProfile,
                mProductStatusId,
                mOnProductItemClickListener);
        viewModel.setOnItemClickListener(product -> {
            if (mOnLikeProductListener != null) {
                mOnLikeProductListener.onLikeClick(product, holder.getAdapterPosition());
            }
        });
        holder.setVariable(BR.viewModel, viewModel);

    }

    public void remove(Product product) {
        int index = mDatas.indexOf(product);
        mDatas.remove(index);
        notifyItemRemoved(index);
    }

    public void addFirstIndex(Product product) {
        mDatas.add(0, product);
        notifyItemInserted(0);
    }

    public void updateItem(int position, Product product) {
        mDatas.set(position, product);
        notifyItemChanged(position);
    }

    public int getIndex(Product product) {
        return mDatas.indexOf(product);
    }
}
