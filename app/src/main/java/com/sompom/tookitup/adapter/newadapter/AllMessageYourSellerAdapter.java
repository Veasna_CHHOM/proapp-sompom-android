package com.sompom.tookitup.adapter.newadapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.OnMessageItemClick;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.viewholder.BindingViewHolder;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemAllMessageYourSeller;

import java.util.List;

/**
 * Created by nuonveyo on 6/26/18.
 */

public class AllMessageYourSellerAdapter extends RecyclerView.Adapter<BindingViewHolder> {
    private List<User> mLists;
    private OnMessageItemClick mOnMessageItemClick;
    private boolean mIsShowActiveIcon = true;

    public AllMessageYourSellerAdapter(List<User> lists, OnMessageItemClick messageItemClick) {
        mLists = lists;
        mOnMessageItemClick = messageItemClick;
    }

    public void setShowActiveIcon(boolean showActiveIcon) {
        mIsShowActiveIcon = showActiveIcon;
    }

    @NonNull
    @Override
    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_all_messag_your_seller).build();
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
        ListItemAllMessageYourSeller viewModel = new ListItemAllMessageYourSeller(mLists.get(position), mOnMessageItemClick);
        holder.setVariable(BR.viewModel, viewModel);
    }

    @Override
    public int getItemCount() {
        return mLists.size();
    }
}
