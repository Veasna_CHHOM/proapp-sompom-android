package com.sompom.tookitup.adapter.newadapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.OnTimelineActiveUserItemClick;
import com.sompom.tookitup.model.ActiveUser;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.viewholder.BindingViewHolder;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemActiveUserViewModel;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemAllMessageYourSeller;

/**
 * Created by nuonveyo on 6/26/18.
 */

public class TimelineActiveUserAdapter extends RecyclerView.Adapter<BindingViewHolder> {
    private static final int LIVE_ITEM = 1;
    private static final int NORMAL_ITEM = 2;
    private ActiveUser mLists;
    private OnTimelineActiveUserItemClick mOnTimelineActiveUserItemClick;


    public TimelineActiveUserAdapter(ActiveUser lists, OnTimelineActiveUserItemClick listener) {
        mLists = lists;
        mOnTimelineActiveUserItemClick = listener;
    }

    @NonNull
    @Override
    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == LIVE_ITEM) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_active_user_live).build();
        } else {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_all_messag_your_seller).build();
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
        if (position == 0) {
            ListItemActiveUserViewModel viewModel = new ListItemActiveUserViewModel(mOnTimelineActiveUserItemClick);
            holder.setVariable(BR.viewModel, viewModel);
        } else {
            User user = mLists.get(position - 1);
            ListItemAllMessageYourSeller viewModel = new ListItemAllMessageYourSeller(user,
                    mOnTimelineActiveUserItemClick);
            holder.setVariable(BR.viewModel, viewModel);
        }
    }

    @Override
    public int getItemCount() {
        return mLists.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return LIVE_ITEM;
        } else {
            return NORMAL_ITEM;
        }
    }
}
