package com.sompom.tookitup.adapter.newadapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.sompom.tookitup.newui.fragment.AbsBaseFragment;

import java.util.List;

/**
 * Created by he.rotha on 3/4/16.
 */
public class StoreViewPagerAdapter extends FragmentStatePagerAdapter {
    private List<AbsBaseFragment> mFragmentProductDetailItems;

    public StoreViewPagerAdapter(FragmentManager fm, List<AbsBaseFragment> fr) {
        super(fm);
        mFragmentProductDetailItems = fr;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentProductDetailItems.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentProductDetailItems.size();
    }

}
