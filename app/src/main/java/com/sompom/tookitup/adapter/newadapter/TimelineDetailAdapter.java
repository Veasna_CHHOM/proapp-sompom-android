package com.sompom.tookitup.adapter.newadapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.ListItemTimelineDetailBinding;
import com.sompom.tookitup.databinding.ListItemTimelineDetailMediaBinding;
import com.sompom.tookitup.databinding.ListItemTimelineDetailOneMediaBinding;
import com.sompom.tookitup.databinding.ListItemTimelineDetailShareBinding;
import com.sompom.tookitup.listener.OnCommentItemClickListener;
import com.sompom.tookitup.listener.OnDetailShareItemCallback;
import com.sompom.tookitup.listener.OnFloatingVideoClickedListener;
import com.sompom.tookitup.listener.OnLoadPreviousCommentClickListener;
import com.sompom.tookitup.listener.OnTimelineItemButtonClickListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.LifeStream;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.SharedProduct;
import com.sompom.tookitup.model.SharedTimeline;
import com.sompom.tookitup.model.WallStreetAdaptive;
import com.sompom.tookitup.model.emun.MediaType;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.WallStreetDataManager;
import com.sompom.tookitup.viewholder.BindingViewHolder;
import com.sompom.tookitup.viewholder.TimelineViewHolder;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemTimelineDetailShareViewModel;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemTimelineDetailViewModel;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemTimelineMediaDetailViewModel;
import com.sompom.tookitup.widget.lifestream.MediaLayout;

import java.util.List;

import timber.log.Timber;

/**
 * Created by nuonveyo on 7/12/18.
 */

public class TimelineDetailAdapter extends CommentAdapter {
    private static final int MEDIA_ITEM = 86;
    private static final int NORMAL_ITEM = 87;
    private static final int SHARE_ITEM = 88;
    private static final int ONE_MEDIA_ITEM = 89;

    private final OnTimelineItemButtonClickListener mOnTimelineItemButtonClickListener;
    private final OnFloatingVideoClickedListener mOnFloatingVideoClickedListener;
    private final AbsBaseActivity mContext;
    private final ApiService mApiService;
    private final OnDetailShareItemCallback mOnLikeButtonClickListener;


    public TimelineDetailAdapter(AbsBaseActivity context,
                                 ApiService apiService,
                                 List<Adaptive> list,
                                 OnTimelineItemButtonClickListener listener,
                                 OnFloatingVideoClickedListener onFloatingVideoClickedListener,
                                 OnCommentItemClickListener onCommentItemClickListener,
                                 OnDetailShareItemCallback onClickListener,
                                 OnLoadPreviousCommentClickListener onLoadPreviousItemClickListener) {
        super(list, onCommentItemClickListener, onLoadPreviousItemClickListener);
        mContext = context;
        mApiService = apiService;
        mOnTimelineItemButtonClickListener = listener;
        mOnFloatingVideoClickedListener = onFloatingVideoClickedListener;
        mOnLikeButtonClickListener = onClickListener;
    }

    @NonNull
    @Override
    public BindingViewHolder onCreateView(@NonNull ViewGroup parent, int viewType) {
        if (viewType == NORMAL_ITEM) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_timeline_detail).build();
        } else if (viewType == MEDIA_ITEM || viewType == ONE_MEDIA_ITEM) {
            @LayoutRes int layout = R.layout.list_item_timeline_detail_media;
            if (viewType == ONE_MEDIA_ITEM) {
                layout = R.layout.list_item_timeline_detail_one_media;
            }

            final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            final ViewDataBinding dataBinding = DataBindingUtil.inflate(inflater, layout, parent, false);
            return new TimelineViewHolder(dataBinding);
        } else if (viewType == SHARE_ITEM) {
            final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            final ViewDataBinding dataBinding = DataBindingUtil.inflate(inflater, R.layout.list_item_timeline_detail_share, parent, false);
            return new TimelineViewHolder(dataBinding);
        }
        return super.onCreateView(parent, viewType);
    }


    public void update(Adaptive adaptive) {
        Timber.e("start update %s", adaptive.getId());
        for (int i = 0; i < mDatas.size(); i++) {
            if (TextUtils.equals(mDatas.get(i).getId(), adaptive.getId())) {
                updateItem(i, adaptive);
                break;
            }
        }

        Timber.e("stop update ");

    }

    @Override
    public void onBindData(@NonNull BindingViewHolder holder, int position) {
        if (holder.getBinding() instanceof ListItemTimelineDetailBinding
                || holder.getBinding() instanceof ListItemTimelineDetailOneMediaBinding) {

            LifeStream lifeStream = (LifeStream) mDatas.get(position);
            ListItemTimelineDetailViewModel viewModel = new ListItemTimelineDetailViewModel(mContext,
                    lifeStream,
                    getDataManager(),
                    position,
                    mOnTimelineItemButtonClickListener,
                    mOnLikeButtonClickListener);

            if (holder.getBinding() instanceof ListItemTimelineDetailOneMediaBinding
                    && (lifeStream.getMedia() != null
                    && !lifeStream.getMedia().isEmpty())) {
                Media media = lifeStream.getMedia().get(0);
                ListItemTimelineDetailOneMediaBinding binding = (ListItemTimelineDetailOneMediaBinding) holder.getBinding();
                binding.mediaLayouts.setVisibility(View.VISIBLE);
                setMediaLayout(holder, binding.mediaLayouts, media);
            }

            holder.setVariable(BR.viewModel, viewModel);

        } else if (holder.getBinding() instanceof ListItemTimelineDetailShareBinding) {
            ListItemTimelineDetailShareViewModel viewModel = new ListItemTimelineDetailShareViewModel(mContext,
                    holder,
                    getDataManager(),
                    (WallStreetAdaptive) mDatas.get(position),
                    mOnTimelineItemButtonClickListener,
                    mOnLikeButtonClickListener);
            holder.setVariable(BR.viewModel, viewModel);

        } else if (holder.getBinding() instanceof ListItemTimelineDetailMediaBinding) {
            Media media = (Media) mDatas.get(position);
            ListItemTimelineMediaDetailViewModel viewModel = new ListItemTimelineMediaDetailViewModel(mContext,
                    media,
                    getDataManager(),
                    position,
                    mOnTimelineItemButtonClickListener);

            ListItemTimelineDetailMediaBinding binding = (ListItemTimelineDetailMediaBinding) holder.getBinding();
            setMediaLayout(holder, binding.mediaLayouts, media);
            holder.setVariable(BR.viewModel, viewModel);

        } else {
            super.onBindData(holder, position);
        }
    }

    private int getMediaPosition(int position) {
        if (mDatas.get(0) instanceof LifeStream) {
            int newPos = position - 1;
            if (newPos < 0) {
                return 0;
            } else {
                return newPos;
            }
        } else {
            return position;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position >= 0 && position < mDatas.size()) {
            if (mDatas.get(position) instanceof LifeStream) {
                return getNormalItemType(position);
            } else if (mDatas.get(position) instanceof Media) {
                return MEDIA_ITEM;
            } else if (mDatas.get(position) instanceof SharedTimeline
                    || mDatas.get(position) instanceof SharedProduct) {
                return SHARE_ITEM;
            } else {
                return super.getItemViewType(position);
            }
        }
        return super.getItemViewType(position);
    }

    private int getNormalItemType(int position) {
        LifeStream lifeStream = (LifeStream) mDatas.get(position);
        if (lifeStream.getMedia() != null && lifeStream.getMedia().size() == 1
                || (!TextUtils.isEmpty(lifeStream.getDescription()) && (lifeStream.getMedia() == null || lifeStream.getMedia().isEmpty()))) {
            return ONE_MEDIA_ITEM;
        } else {
            return NORMAL_ITEM;
        }
    }

    private ViewGroup.LayoutParams getLayoutParam(MediaLayout mediaLayout, Media media) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((AppCompatActivity) mediaLayout.getContext()).getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int screenWidth = metrics.widthPixels;
        ViewGroup.LayoutParams param = mediaLayout.getLayoutParams();
        int height;
        if (media.getWidth() == 0 || media.getHeight() == 0) {
            height = screenWidth;
        } else {
            height = media.getHeight() * screenWidth / media.getWidth();
        }
        param.width = screenWidth;
        param.height = height;
        return param;
    }

    private void setMediaLayout(BindingViewHolder holder, MediaLayout mediaLayout, Media media) {
        mediaLayout.setLayoutParams(getLayoutParam(mediaLayout, media));
        mediaLayout.setMedia(media);
        if (media.getType() == MediaType.VIDEO) {
            mediaLayout.setMute(false);
            mediaLayout.setVisibleMuteButton(false);
            mediaLayout.setShowControl(true);
            mediaLayout.setSaveOnPause(true);
            mediaLayout.setVisibleFloatingButton(true);
            mediaLayout.setVisibleFullScreenButton(true);
            mediaLayout.setOnFloatingVideoClickedListener(v -> mOnFloatingVideoClickedListener.onFloatingVideo(getMediaPosition(holder.getAdapterPosition()), mediaLayout.getTime()));
            mediaLayout.setOnFullScreenClickListener(v -> mOnFloatingVideoClickedListener.onFullScreenClicked(getMediaPosition(holder.getAdapterPosition()), mediaLayout.getTime()));
            if (holder instanceof TimelineViewHolder) {
                ((TimelineViewHolder) holder).setMediaLayout(mediaLayout);
            }
        }
    }

    private WallStreetDataManager getDataManager() {
        return new WallStreetDataManager(mContext,
                mApiService,
                null,
                null);

    }

    public void updateItem(int position, Adaptive adaptive) {
        mDatas.set(position, adaptive);
        notifyItemChanged(position);
    }
}
