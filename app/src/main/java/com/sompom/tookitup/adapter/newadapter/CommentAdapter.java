package com.sompom.tookitup.adapter.newadapter;

import android.text.TextUtils;
import android.view.ViewGroup;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.RefreshableAdapter;
import com.sompom.tookitup.databinding.ListItemLoadPrevouisCommentBinding;
import com.sompom.tookitup.databinding.ListItemMainCommentBinding;
import com.sompom.tookitup.databinding.ListItemMainCommentGifBinding;
import com.sompom.tookitup.databinding.ListItemMainCommentImageBinding;
import com.sompom.tookitup.listener.OnCommentItemClickListener;
import com.sompom.tookitup.listener.OnLoadPreviousCommentClickListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.LoadPreviousComment;
import com.sompom.tookitup.model.emun.MediaType;
import com.sompom.tookitup.model.result.Comment;
import com.sompom.tookitup.viewholder.BindingViewHolder;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemLoadPreviousCommentViewModel;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemMainCommentViewModel;

import java.util.List;

/**
 * Created by he.rotha on 3/4/16.
 */
public class CommentAdapter extends RefreshableAdapter<Adaptive, BindingViewHolder> {
    private static final int LOAD_PREVIOUS_ITEM = 0X009;
    private static final int TEXT_ITEM = 0X0010;
    private static final int GIF_ITEM = 0X0011;
    private static final int IMAGE_ITEM = 0X0013;

    final OnCommentItemClickListener mOnCommentItemClickListener;

    private final OnLoadPreviousCommentClickListener mOnLoadPreviousItemClickListener;
    private boolean mIsReplyComment;

    public CommentAdapter(List<Adaptive> comments,
                          OnCommentItemClickListener onCommentItemClickListener,
                          OnLoadPreviousCommentClickListener onLoadPreviousItemClickListener) {
        super(comments);
        mOnLoadPreviousItemClickListener = onLoadPreviousItemClickListener;
        mOnCommentItemClickListener = onCommentItemClickListener;
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        if (viewType == GIF_ITEM) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_main_comment_gif).build();
        } else if (viewType == IMAGE_ITEM) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_main_comment_image).build();
        } else if (viewType == LOAD_PREVIOUS_ITEM) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_load_prevouis_comment).build();
        } else {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_main_comment).build();
        }
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        if (holder.getBinding() instanceof ListItemLoadPrevouisCommentBinding) {
            ListItemLoadPreviousCommentViewModel viewModel = new ListItemLoadPreviousCommentViewModel(mIsReplyComment,
                    position,
                    mOnLoadPreviousItemClickListener,
                    (LoadPreviousComment) mDatas.get(position));
            holder.setVariable(BR.viewModel, viewModel);

        } else if (holder.getBinding() instanceof ListItemMainCommentGifBinding
                || holder.getBinding() instanceof ListItemMainCommentImageBinding
                || holder.getBinding() instanceof ListItemMainCommentBinding) {
            ListItemMainCommentViewModel commentViewModel = new ListItemMainCommentViewModel(holder.getContext(),
                    position,
                    (Comment) mDatas.get(position),
                    mOnCommentItemClickListener);
            holder.setVariable(BR.viewModel, commentViewModel);
            if (mIsReplyComment) {
                commentViewModel.mIsShowReplayLayout.set(false);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position >= 0 && position < mDatas.size()) {
            if (mDatas.get(position) instanceof LoadPreviousComment) {
                return LOAD_PREVIOUS_ITEM;
            } else if (((Comment) mDatas.get(position)).getMedia() == null || ((Comment) mDatas.get(position)).getMedia().isEmpty()) {
                return TEXT_ITEM;
            } else {
                if (((Comment) mDatas.get(position)).getMedia().get(0).getType() == MediaType.TENOR_GIF) {
                    return GIF_ITEM;
                } else if (((Comment) mDatas.get(position)).getMedia().get(0).getType() == MediaType.IMAGE) {
                    return IMAGE_ITEM;
                }
            }
        }
        return super.getItemViewType(position);
    }

    public void addLatestComment(Comment comment) {
        this.mDatas.add(comment);
        if (!canLoadMore()) {
            notifyItemInserted(mDatas.size() - 1);
        } else {
            notifyItemInserted(mDatas.size());
        }
    }

    public void notifyItemChanged(int position, Comment comment) {
        mDatas.set(position, comment);
        notifyItemChanged(position);
    }

    void setReplyComment() {
        mIsReplyComment = true;
    }

    public void removeInstanceComment() {
        for (int i = mDatas.size() - 1; i >= 0; i--) {
            if (mDatas.get(i) instanceof Comment) {
                if (TextUtils.isEmpty(((Comment) mDatas.get(i)).getId())) {
                    mDatas.remove(i);
//                notifyItemRemoved(i + 1);
                }
            }
        }
        notifyDataSetChanged();
    }

    public void checkToRemoveLoadPreviousItem(boolean isLoadMore,
                                              LoadPreviousComment previousComment,
                                              int position) {
        if (!isLoadMore) {
            notifyItemRemove(position);
        } else {
            notifyItemChanged(position, previousComment);
        }
    }

    public void checkToRemoveAndAddNewComment(boolean isLoadMore,
                                              LoadPreviousComment previousComment,
                                              int position,
                                              List<Comment> comments) {
        if (!isLoadMore) {
            notifyItemRemove(position);
            addLoadPreviousComment(position, comments);
        } else {
            notifyItemChanged(position, previousComment);
            addLoadPreviousComment(position + 1, comments);
        }
    }

    public void notifyItemRemove(int position) {
        mDatas.remove(position);
        notifyItemRemoved(position);
    }

    public void addLoadPreviousComment(int addToIndex, List<Comment> commentList) {
        mDatas.addAll(addToIndex, commentList);
        notifyItemRangeInserted(addToIndex, commentList.size());
    }
}
