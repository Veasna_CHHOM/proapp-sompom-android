package com.sompom.tookitup.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.ViewGroup;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.OnCategoryClickListener;
import com.sompom.tookitup.model.result.Category;
import com.sompom.tookitup.viewholder.BindingViewHolder;
import com.sompom.tookitup.viewmodel.FilterCategoryViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by he.rotha on 4/21/16.
 */
public class FilterCategoryAdapter extends RecyclerView.Adapter<BindingViewHolder> {

    private List<Category> mCategories;
    private List<Category> mCategoriesAll;
    private boolean mIsDisableMultiCheck;
    private OnCategoryClickListener mOnCategoryClickListener;

    public FilterCategoryAdapter(List<Category> categories) {
        mCategories = categories;
        mCategoriesAll = new ArrayList<>(categories);
    }

    public void setDisableMultiCheck(boolean disableMultiCheck) {
        mIsDisableMultiCheck = disableMultiCheck;
    }

    public void setFilter(CharSequence value) {
        if (!TextUtils.isEmpty(value.toString().trim())) {
            mCategories.clear();
            for (Category category : mCategoriesAll) {
                if (category.getName().toLowerCase().contains(value.toString().toLowerCase())) {
                    mCategories.add(category);
                }
            }
        } else {
            mCategories.clear();
            mCategories.addAll(mCategoriesAll);
        }
        notifyDataSetChanged();
    }

    public void setOnCategoryClickListener(OnCategoryClickListener onCategoryClickListener) {
        mOnCategoryClickListener = onCategoryClickListener;
    }

    @NonNull
    @Override
    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_category_filter).build();
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
        FilterCategoryViewModel model =
                new FilterCategoryViewModel(mCategories.get(position), mCategoriesAll, mIsDisableMultiCheck);
        model.setOnCategoryClickListener(mOnCategoryClickListener);
        holder.setVariable(BR.viewModel, model);
    }

    public ArrayList<Category> getSelectedCategory() {
        ArrayList<Category> categories = new ArrayList<>();
        for (Category category : mCategoriesAll) {
            if (category.isCheck()) {
                categories.add(category);
            }
        }
        return categories;
    }

    public void notifyDataSetChange(List<Category> categories) {
        if (categories != null) {
            mCategories.clear();
            mCategories.addAll(categories);
            notifyDataSetChanged();
        }
    }


    @Override
    public int getItemCount() {
        return mCategories.size();
    }
}
