package com.sompom.tookitup.adapter;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.chat.MessageState;
import com.sompom.tookitup.database.ConversationDb;
import com.sompom.tookitup.databinding.ListItemChatMeLinkBinding;
import com.sompom.tookitup.databinding.ListItemChatMeMediaAudioBinding;
import com.sompom.tookitup.databinding.ListItemChatMeMediaBinding;
import com.sompom.tookitup.databinding.ListItemChatMeRemoveMessageBinding;
import com.sompom.tookitup.databinding.ListItemChatRecipientLinkBinding;
import com.sompom.tookitup.databinding.ListItemChatRecipientMediaAudioBinding;
import com.sompom.tookitup.databinding.ListItemChatRecipientMediaBinding;
import com.sompom.tookitup.databinding.ListItemChatRecipientRemoveMessageBinding;
import com.sompom.tookitup.databinding.ListItemChatTypingBinding;
import com.sompom.tookitup.helper.ChatDiffCallback;
import com.sompom.tookitup.helper.ChatStatusItemDecoration;
import com.sompom.tookitup.listener.OnChatItemListener;
import com.sompom.tookitup.model.BaseChatModel;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.SelectedChat;
import com.sompom.tookitup.model.TypingChatItem;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.utils.ChatUtility;
import com.sompom.tookitup.viewholder.BindingViewHolder;
import com.sompom.tookitup.viewmodel.ChatMediaViewModel;
import com.sompom.tookitup.viewmodel.ChatViewModel;
import com.sompom.tookitup.viewmodel.ListItemChatLInkPreviewViewModel;
import com.sompom.tookitup.viewmodel.ListItemChatRemoveMessageViewModel;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemChatTypingViewModel;

import java.util.Collections;
import java.util.List;

import timber.log.Timber;

/**
 * Created by he.rotha on 3/4/16.
 */
public class ChatAdapter extends RefreshableTopAdapter<BaseChatModel, BindingViewHolder>
        implements ChatStatusItemDecoration.DecorateItem {
    private static final int RECIPIENT_VIEW_MEDIA_TYPE = 0X019;
    private static final int ME_VIEW_MEDIA_TYPE = 0X010;
    private static final int ME_VIEW_TYPE = 0X011;
    private static final int RECIPIENT_VIEW_TYPE = 0X012;
    private static final int TYPING_VIEW_TYPE = 0X013;
    private static final int ME_VIEW_MEDIA_AUDIO_TYPE = 0X021;
    private static final int RECIPIENT_VIEW_MEDIA_AUDIO_TYPE = 0X022;
    private static final int ME_VIEW_GIF_TYPE = 0X023;
    private static final int RECIPIENT_VIEW_GIF_TYPE = 0X024;
    private static final int ME_LINK_VIEW_TYPE = 0X025;
    private static final int RECIPIENT_LINK_VIEW_TYPE = 0X026;
    private static final int ME_REMOVE_MESSAGE = 0X027;
    private static final int RECIPIENT_REMOVE_MESSAGE = 0X028;

    private final User mMyUser;
    private final SelectedChat mSelectedChat;
    private final OnChatItemListener mOnImageClickListener;
    private final Activity mActivity;
    private final TypingChatItem mTypingChatItem = new TypingChatItem();
    private Conversation mConversation;

    public ChatAdapter(Activity activity,
                       Conversation conversation,
                       User myUser,
                       List<BaseChatModel> chats,
                       OnChatItemListener onItemClickListener) {
        super(chats);
        mConversation = conversation;
        mActivity = activity;
        mSelectedChat = new SelectedChat();
        mOnImageClickListener = onItemClickListener;
        mMyUser = myUser;
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        int resourceId;
        if (viewType == TYPING_VIEW_TYPE) {
            resourceId = R.layout.list_item_chat_typing;
        } else if (viewType == ME_REMOVE_MESSAGE) {
            resourceId = R.layout.list_item_chat_me_remove_message;
        } else if (viewType == ME_VIEW_TYPE) {
            resourceId = R.layout.list_item_chat_me;
        } else if (viewType == ME_LINK_VIEW_TYPE) {
            resourceId = R.layout.list_item_chat_me_link;
        } else if (viewType == ME_VIEW_MEDIA_TYPE) {
            resourceId = R.layout.list_item_chat_me_media;
        } else if (viewType == RECIPIENT_VIEW_MEDIA_TYPE) {
            resourceId = R.layout.list_item_chat_recipient_media;
        } else if (viewType == ME_VIEW_MEDIA_AUDIO_TYPE) {
            resourceId = R.layout.list_item_chat_me_media_audio;
        } else if (viewType == RECIPIENT_REMOVE_MESSAGE) {
            resourceId = R.layout.list_item_chat_recipient_remove_message;
        } else if (viewType == RECIPIENT_VIEW_MEDIA_AUDIO_TYPE) {
            resourceId = R.layout.list_item_chat_recipient_media_audio;
        } else if (viewType == ME_VIEW_GIF_TYPE) {
            resourceId = R.layout.list_item_chat_me_gif;
        } else if (viewType == RECIPIENT_VIEW_GIF_TYPE) {
            resourceId = R.layout.list_item_chat_recipient_gif;
        } else if (viewType == RECIPIENT_LINK_VIEW_TYPE) {
            resourceId = R.layout.list_item_chat_recipient_link;
        } else {
            resourceId = R.layout.list_item_chat_recipient;
        }
        return new BindingViewHolder.Builder(parent, resourceId).build();
    }

    public void setData(List<BaseChatModel> chats) {
        if (mData.isEmpty()) {
            mData = chats;
            notifyDataSetChanged();
        } else {
            final ChatDiffCallback diffCallback = new ChatDiffCallback(mActivity, mData, chats);
            final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
            mData.clear();
            mData.addAll(chats);
            diffResult.dispatchUpdatesTo(this);
        }
    }

    public void displayTyping(User user, boolean isEnable) {
        int newIndex = mData.indexOf(mTypingChatItem);
        boolean isTypingItemAdded = newIndex < 0;
        if (isEnable) {
            if (isTypingItemAdded) {
                mTypingChatItem.setSender(user);
                mData.add(mTypingChatItem);
                safeNotifyItemInsertChange(mData.size() - 1);
            }
        } else {
            if (!isTypingItemAdded) {
                mData.remove(newIndex);
                safeNotifyItemRemove(newIndex);
            }
        }
    }

    @Override
    public int getItemViewType(final int position) {
        if (mData.isEmpty() || (position == 0 && canLoadMore())) {
            return super.getItemViewType(position);
        } else {
            int itemPosition;
            if (canLoadMore()) {
                itemPosition = position - 1;
            } else {
                itemPosition = position;
            }

            if (itemPosition < 0) {
                itemPosition = 0;
            }
            if (mData.get(itemPosition) instanceof Chat) {
                return getItemType(itemPosition);
            } else {
                return TYPING_VIEW_TYPE;
            }
        }
    }

    private int getItemType(int itemPosition) {
        if (isMe(itemPosition)) {
            return getItemTypeChatMe(itemPosition);
        } else {
            return getItemTypeChatRecipient(itemPosition);
        }
    }

    private int getItemTypeChatMe(int itemPosition) {
        if (((Chat) mData.get(itemPosition)).isDeleted()) {
            return ME_REMOVE_MESSAGE;
        } else if (((Chat) mData.get(itemPosition)).getType() == Chat.Type.IMAGE) {
            return ME_VIEW_MEDIA_TYPE;
        } else if (((Chat) mData.get(itemPosition)).getType() == Chat.Type.AUDIO) {
            return ME_VIEW_MEDIA_AUDIO_TYPE;
        } else if (((Chat) mData.get(itemPosition)).getType() == Chat.Type.GIF) {
            return ME_VIEW_GIF_TYPE;
        } else {
            if (Patterns.WEB_URL.matcher(((Chat) mData.get(itemPosition)).getContent()).find()) {
                return ME_LINK_VIEW_TYPE;
            } else {
                return ME_VIEW_TYPE;
            }
        }
    }

    private int getItemTypeChatRecipient(int itemPosition) {
        if (((Chat) mData.get(itemPosition)).isDeleted()) {
            return RECIPIENT_REMOVE_MESSAGE;
        } else if (((Chat) mData.get(itemPosition)).getType() == Chat.Type.IMAGE) {
            return RECIPIENT_VIEW_MEDIA_TYPE;
        } else if (((Chat) mData.get(itemPosition)).getType() == Chat.Type.AUDIO) {
            return RECIPIENT_VIEW_MEDIA_AUDIO_TYPE;
        } else if (((Chat) mData.get(itemPosition)).getType() == Chat.Type.GIF) {
            return RECIPIENT_VIEW_GIF_TYPE;
        } else {
            Chat chat = (Chat) mData.get(itemPosition);
            if (!TextUtils.isEmpty(chat.getContent()) && Patterns.WEB_URL.matcher(chat.getContent()).find()) {
                return RECIPIENT_LINK_VIEW_TYPE;
            } else {
                return RECIPIENT_VIEW_TYPE;
            }
        }
    }

    private boolean isMe(int position) {
        if (position < 0) {
            position = 0;
        }
        try {
            String userId = ((Chat) mData.get(position)).getSenderId();
            return userId.equals(mMyUser.getId());
        } catch (Exception ex) {
            return false;
        }
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        ChatUtility.GroupMessage drawable = ChatUtility.getGroupMessage(mData, position, mMyUser, isMe(position));

        if (holder.getBinding() instanceof ListItemChatTypingBinding) {
            ListItemChatTypingViewModel viewModel = new ListItemChatTypingViewModel(((TypingChatItem) mData.get(position)).getSender());
            holder.setVariable(BR.viewModel, viewModel);
        } else if (holder.getBinding() instanceof ListItemChatMeRemoveMessageBinding
                || holder.getBinding() instanceof ListItemChatRecipientRemoveMessageBinding) {
            Chat message = (Chat) mData.get(position);
            ListItemChatRemoveMessageViewModel viewModel = new ListItemChatRemoveMessageViewModel(mActivity,
                    mConversation,
                    message,
                    drawable,
                    position,
                    mSelectedChat,
                    new MyOnChatItemListener(holder));
            holder.setVariable(BR.viewModel, viewModel);
        } else if (holder.getBinding() instanceof ListItemChatMeMediaBinding
                || holder.getBinding() instanceof ListItemChatRecipientMediaBinding
                || holder.getBinding() instanceof ListItemChatMeMediaAudioBinding
                || holder.getBinding() instanceof ListItemChatRecipientMediaAudioBinding) {
            Chat message = (Chat) mData.get(position);
            ChatMediaViewModel chatViewModel = new ChatMediaViewModel(mActivity,
                    mConversation,
                    message,
                    drawable,
                    position,
                    mSelectedChat,
                    new MyOnChatItemListener(holder));
            holder.setVariable(BR.viewModel, chatViewModel);
        } else if (holder.getBinding() instanceof ListItemChatMeLinkBinding
                || holder.getBinding() instanceof ListItemChatRecipientLinkBinding) {
            Chat message = (Chat) mData.get(position);
            ListItemChatLInkPreviewViewModel chatViewModel = new ListItemChatLInkPreviewViewModel(mActivity,
                    mConversation,
                    holder.getBinding().getRoot(),
                    position,
                    message,
                    mSelectedChat,
                    drawable,
                    new MyOnChatItemListener(holder));
            holder.setVariable(BR.viewModel, chatViewModel);
        } else {
            Chat message = (Chat) mData.get(position);
            ChatViewModel chatViewModel = new ChatViewModel(mActivity,
                    mConversation,
                    holder.getBinding().getRoot(),
                    position,
                    message,
                    mSelectedChat,
                    drawable,
                    new MyOnChatItemListener(holder));
            holder.setVariable(BR.viewModel, chatViewModel);
        }
    }

    /**
     * @return true: mean add new message, otherwise, update
     */
    public boolean addLatestChat(Chat chat) {
        Timber.i("addLatestChat: " + new Gson().toJson(chat));
        Timber.e("addLatestChat " + chat.getActualContent(mActivity) + "  " + chat.getStatus());
        if (isEmpty()) {
            setShowEmpty(false);
            safeNotifyItemRemove(0);
            setCanLoadMore(false);
        }

        int index = mData.indexOf(chat);
        Timber.i("index: " + index);
        if (index < 0) {
            int addIndex = mData.size() - 1;

            for (int i = mData.size() - 1; i >= 0; i--) {
                if (mData.get(i) instanceof Chat && (chat.getStatus() == MessageState.SENDING || mData.get(i).getTime() <= chat.getTime())) {
                    addIndex = i + 1;
                    break;
                }
            }

            if (addIndex < 0) {
                addIndex = 0;
            }

            if (chat.getStatus() == MessageState.SEEN) {
                Timber.i("Add new seen message.");
                updateSeenStatus(chat, index);
            }

            mData.add(addIndex, chat);
            notifyDataSetChanged();
            return true;
        } else {
            if (chat.getStatus() == MessageState.SEEN) {
                Timber.i("Update seen message.");
                updateSeenStatus(chat, index);
            } else {
                /*
                    Check to add sent message only once time for group message.
                */
                if (mData.get(index) instanceof Chat) {
                    if (((Chat) mData.get(index)).getStatus() == MessageState.SENT) {
                        //The message was already added as sent status
                        return false;
                    }
                }
            }

            mData.set(index, chat);
            notifyDataSetChanged();

            return false;
        }
    }

    /*
       Manage to update seen status for both individual and group chat when the seen message was
       broadcast by chat socket.
     */
    private void updateSeenStatus(Chat seenMessage, int existIndex) {
//        String checkingId = seenMessage.isGroup() ? seenMessage.getSenderId() : seenMessage.getSendTo();
        String checkingId = seenMessage.getSenderId();
        Timber.i("Sender id: " + checkingId);
        if (TextUtils.isEmpty(checkingId)) {
            return;
        }

        for (BaseChatModel datum : mData) {
            if (datum instanceof Chat) {
                Chat chat = (Chat) datum;
                if (chat.getSeenParticipants() != null) {
                    for (int i = chat.getSeenParticipants().size() - 1; i >= 0; i--) {
                        //Remove user from previous seen message
                        if (chat.getSeenParticipants().get(i).getId().matches(checkingId)) {
                            chat.getSeenParticipants().get(i).setSeenMessage(false);
                            break;
                        }
                    }
                }
            }
        }

        //Add sender to the seen list of last message.
        if (seenMessage.getSeenParticipants() == null) {
            User participantById = getParticipantById(checkingId);
            if (participantById != null) {
                seenMessage.setSeenParticipants(Collections.singletonList(participantById.cloneForSeenStatus()));
                updateStatusOfSeenConversation(participantById.getId(), seenMessage.getId());
            }
        } else {
            boolean alreadySeen = false;
            for (User seenParticipant : seenMessage.getSeenParticipants()) {
                if (seenParticipant.getId().matches(checkingId)) {
                    alreadySeen = true;
                    break;
                }
            }
            if (!alreadySeen) {
                User participantById = getParticipantById(checkingId);
                if (participantById != null) {
                    seenMessage.getSeenParticipants().add(participantById.cloneForSeenStatus());
                    updateStatusOfSeenConversation(participantById.getId(), seenMessage.getId());
                }
            }
        }

        /*
        To maintain the original sender id of message for there will many changes of sender id
        in group chat.
         */
        if (existIndex >= 0) {
            if (mData.get(existIndex) instanceof Chat) {
                seenMessage.setSenderId(((Chat) mData.get(existIndex)).getSenderId());
            }
        }
    }

    private void updateStatusOfSeenConversation(String userId, String lastSeenMessageId) {
        JsonObject statusSeenConversation = mConversation.getStatusSeenConversation();
        if (statusSeenConversation.has(userId)) {
            statusSeenConversation.remove(userId);
        }
        statusSeenConversation.addProperty(userId, lastSeenMessageId);
        ConversationDb.save(mActivity, mConversation);
        Timber.i("Update updateStatusOfSeenConversation list of conversation.");
    }

    private User getParticipantById(String id) {
        if (!TextUtils.isEmpty(id) &&
                mConversation != null &&
                mConversation.getParticipants() != null) {
            //Ignore sender avatar
            if (id.matches(mMyUser.getId())) {
                return null;
            }
            Timber.i("mConversation.getParticipants(): " + new Gson().toJson(mConversation.getParticipants()));
            for (User participant : mConversation.getParticipants()) {
                if (participant.getId().matches(id)) {
                    return participant;
                }
            }
        }

        Timber.i("Participant not found.");
        return null;
    }

    public void addByLoadMore(List<BaseChatModel> moreChats, boolean isNextPage) {
        setCanLoadMore(isNextPage);
        mData.addAll(0, moreChats);
        if (!canLoadMore()) {
            notifyItemRemoved(0);
        }
        notifyItemRangeInserted(0, moreChats.size());
        if (canLoadMore()) {
            notifyItemChanged(moreChats.size() + 1);
        } else {
            notifyItemChanged(moreChats.size());
        }
    }

    public void deleteItem(Chat chat) {
        int position = mData.indexOf(chat);
        if (position >= 0 && position < mData.size()) {
            BaseChatModel data = mData.get(position);
            if (data instanceof Chat) {
                ((Chat) data).setDelete(true);
            }
            notifyItemChanged(position);
        }
    }

    public Chat getLatestChat() {
        for (BaseChatModel datum : mData) {
            if (datum instanceof Chat) {
                return (Chat) datum;
            }
        }
        return null;
    }

    @Override
    public boolean isSeen(int position) {
        if (canLoadMore()) {
            position = position - 1;
            if (position < 0) {
                position = 0;
            }
        }
        if (position >= mData.size()) {
            return false;
        }
        if (mData.get(position) instanceof Chat) {
            final boolean isSeen = ((Chat) mData.get(position)).getStatus() == MessageState.SEEN;

            if (position + 1 < mData.size() && mData.get(position + 1) instanceof Chat) {
                Chat chatIndex = (Chat) mData.get(position + 1);
                if (chatIndex.getStatus() == MessageState.SEEN || !TextUtils.equals(chatIndex.getSenderId(), mMyUser.getId())) {
                    return false;
                }
            }
            return isSeen;
        }
        return false;
    }

    private class MyOnChatItemListener implements OnChatItemListener {
        private final BindingViewHolder mHolder;

        MyOnChatItemListener(BindingViewHolder holder) {
            mHolder = holder;
        }

        @Override
        public void onImageClick(List<Media> list, int productMediaPosition) {
            mOnImageClickListener.onImageClick(list, productMediaPosition);
        }

        @Override
        public void onChatItemLongPressClick(Chat chat, @Nullable Media media, int chatItemPosition) {
            mOnImageClickListener.onChatItemLongPressClick(chat, media, mHolder.getAdapterPosition());
        }

        @Override
        public void onForwardClick(Chat chat) {
            mOnImageClickListener.onForwardClick(chat);
        }

        @Override
        public void onMessageRetryClick(Chat chat) {
            mOnImageClickListener.onMessageRetryClick(chat);
        }

        @Override
        public User getUser(String id) {
            return mOnImageClickListener.getUser(id);
        }
    }
}
