package com.sompom.tookitup.adapter.newadapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.OnMessageItemClick;
import com.sompom.tookitup.model.ActiveUser;
import com.sompom.tookitup.viewholder.BindingViewHolder;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemChatSellerViewModel;

import java.util.List;

/**
 * Created by nuonveyo on 8/2/18.
 */

public class SearchGeneralAdapter extends RecyclerView.Adapter<BindingViewHolder> {
    private static final int PEOPLE_NEARBY = -1;
    private static final int SUGGESTED_PEOPLE = -2;
    private static final int LAST_SEARCH = -3;
    private List<ActiveUser> mActiveUsers;
    private OnMessageItemClick mOnMessageItemClick;

    public SearchGeneralAdapter(List<ActiveUser> activeUsers, OnMessageItemClick messageItemClick) {
        mActiveUsers = activeUsers;
        mOnMessageItemClick = messageItemClick;
    }

    @NonNull
    @Override
    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BindingViewHolder holder = new BindingViewHolder.Builder(parent, R.layout.list_item_search_general).build();
        RecyclerView recyclerView = holder.itemView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(parent.getContext(), LinearLayoutManager.HORIZONTAL, false));
        AllMessageYourSellerAdapter adapter;
        if (viewType == PEOPLE_NEARBY) {
            adapter = new AllMessageYourSellerAdapter(getUserByType(ActiveUser.SellerItemType.PEOPLE_NEARBY), mOnMessageItemClick);
        } else if (viewType == SUGGESTED_PEOPLE) {
            adapter = new AllMessageYourSellerAdapter(getUserByType(ActiveUser.SellerItemType.SUGGESTED_PEOPLE), mOnMessageItemClick);
        } else {
            adapter = new AllMessageYourSellerAdapter(getUserByType(ActiveUser.SellerItemType.LAST_SEARCH), mOnMessageItemClick);
        }
        adapter.setShowActiveIcon(false);
        recyclerView.setAdapter(adapter);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
        ListItemChatSellerViewModel viewModel = new ListItemChatSellerViewModel(mActiveUsers.get(position));
        holder.setVariable(BR.viewModel, viewModel);
    }

    @Override
    public int getItemCount() {
        return mActiveUsers.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (mActiveUsers.get(position).getSellerItemType() == ActiveUser.SellerItemType.PEOPLE_NEARBY) {
            return PEOPLE_NEARBY;
        } else if (mActiveUsers.get(position).getSellerItemType() == ActiveUser.SellerItemType.SUGGESTED_PEOPLE) {
            return SUGGESTED_PEOPLE;
        } else {
            return LAST_SEARCH;
        }
    }

    public void addMoreItem(ActiveUser activeUser) {
        mActiveUsers.add(activeUser);
        notifyItemInserted(mActiveUsers.size());
    }

    private ActiveUser getUserByType(ActiveUser.SellerItemType itemType) {
        for (ActiveUser activeUser : mActiveUsers) {
            if (activeUser.getSellerItemType() == itemType) {
                return activeUser;
            }
        }
        return mActiveUsers.get(0);
    }
}
