package com.sompom.tookitup.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sompom.tookitup.R;

import java.util.List;

/**
 * Created by imac on 9/25/15.
 */
public abstract class RefreshableTopAdapter<D, T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {

    private static final int AT_TOP = 0X000;
    private static final int AT_BOTTOM = 0X001;
    private static final int VIEW_ITEM = 0X002;
    private static final int VIEW_PROGRESS = 0X003;
    private static final int EMPTY_ITEM = 0X004;

    List<D> mData;
    private boolean mCanLoadMore = true;
    private boolean mIsShowEmpty = true;
    private String mEmptyMessage;

    RefreshableTopAdapter(List<D> data) {
        mData = data;
    }

    @Override
    public int getItemCount() {
        if (isEmpty() && mIsShowEmpty) {
            return 1;
        }
        if (mCanLoadMore) {
            return mData.size() + 1;
        } else {
            return mData.size();
        }
    }

    boolean canLoadMore() {
        return mCanLoadMore;
    }

    public void setCanLoadMore(final boolean canLoadMore) {
        mCanLoadMore = canLoadMore;
    }

    public void setEmptyMessage(String emptyMessage) {
        mEmptyMessage = emptyMessage;
    }

    public void setShowEmpty(boolean showEmpty) {
        mIsShowEmpty = showEmpty;
    }

    public void updateData(List<D> updateLIst, int where) {
        if (where == AT_BOTTOM) {
            mData.addAll(updateLIst);
        } else if (where == AT_TOP) {
            mData.addAll(0, updateLIst);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final T holder, final int position) {
        if (getItemViewType(position) != VIEW_PROGRESS) {
            if (getItemViewType(position) == EMPTY_ITEM) {
                EmptyMessageViewHolder emptyMessageViewHolder = (EmptyMessageViewHolder) holder;
                if (TextUtils.isEmpty(mEmptyMessage)) {
                    mEmptyMessage = emptyMessageViewHolder.getContext().getString(R.string.chat_empty_message);
                }
                emptyMessageViewHolder.setMessage(mEmptyMessage);
            } else if (mCanLoadMore) {
                onBindData(holder, position - 1);
            } else {
                onBindData(holder, position);
            }
        }
    }

    @NonNull
    @Override
    public T onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        if (viewType == VIEW_PROGRESS) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loading, parent, false);
            ProgressViewHolder progressViewHolder = new ProgressViewHolder(v);
            return (T) progressViewHolder;
        } else if (viewType == EMPTY_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_empty_chat, parent, false);
            EmptyMessageViewHolder progressViewHolder = new EmptyMessageViewHolder(v);
            return (T) progressViewHolder;
        } else {
            return onCreateView(parent, viewType);
        }
    }

    public void safeNotifyItemInsertChange(int index) {
        if (index > 0) {
            if (canLoadMore()) {
                notifyItemInserted(index + 1);
            } else {
                notifyItemInserted(index);
            }
        }
    }

    public void safeNotifyItemRemove(int index) {
        if (index > 0) {
            if (canLoadMore()) {
                notifyItemRemoved(index + 1);
            } else {
                notifyItemRemoved(index);
            }
        }
    }

    protected abstract T onCreateView(final ViewGroup parent, final int viewType);


    protected abstract void onBindData(final T holder, final int position);

    @Override
    public int getItemViewType(int position) {
        if (isEmpty()) {
            return EMPTY_ITEM;
        }
        if (position == 0 && mCanLoadMore) {
            return VIEW_PROGRESS;
        } else {
            return VIEW_ITEM;
        }
    }

    public boolean isEmpty() {
        return mData.isEmpty();
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        ProgressViewHolder(View v) {
            super(v);
        }
    }

    public static class EmptyMessageViewHolder extends RecyclerView.ViewHolder {
        private final Context mContext;
        private final TextView mTextView;

        EmptyMessageViewHolder(@NonNull View itemView) {
            super(itemView);
            mContext = itemView.getContext();
            mTextView = itemView.findViewById(R.id.messageTextView);
        }

        public Context getContext() {
            return mContext;
        }

        public void setMessage(String message) {
            mTextView.setText(message);
        }
    }
}
