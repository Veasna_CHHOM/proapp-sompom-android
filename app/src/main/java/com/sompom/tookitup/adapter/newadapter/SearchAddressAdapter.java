package com.sompom.tookitup.adapter.newadapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.OnItemClickListener;
import com.sompom.tookitup.model.SearchAddressResult;
import com.sompom.tookitup.viewholder.BindingViewHolder;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemSearchAddressViewModel;

import java.util.List;

/**
 * Created by nuonveyo on 6/11/18.
 */

public class SearchAddressAdapter extends RecyclerView.Adapter<BindingViewHolder> {
    private List<SearchAddressResult> mSearchAddressResultList;
    private OnItemClickListener<SearchAddressResult> mListener;

    public SearchAddressAdapter(List<SearchAddressResult> searchAddressResultList,
                                OnItemClickListener<SearchAddressResult> listener) {
        mSearchAddressResultList = searchAddressResultList;
        mListener = listener;
    }

    public void setData(List<SearchAddressResult> resultList) {
        mSearchAddressResultList.clear();
        mSearchAddressResultList.addAll(resultList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_search_address).build();
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
        ListItemSearchAddressViewModel viewModel = new ListItemSearchAddressViewModel(mSearchAddressResultList.get(position),
                mListener);
        holder.setVariable(BR.viewModel, viewModel);
    }

    @Override
    public int getItemCount() {
        return mSearchAddressResultList.size();
    }
}
