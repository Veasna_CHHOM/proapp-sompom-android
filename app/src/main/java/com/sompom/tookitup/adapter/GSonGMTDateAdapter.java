package com.sompom.tookitup.adapter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import timber.log.Timber;

/**
 * Created by nuonveyo on 12/12/18.
 */

public class GSonGMTDateAdapter implements JsonSerializer<Date>, JsonDeserializer<Date> {
    private DateFormat mDateFormat;

    public GSonGMTDateAdapter() {
        mDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        mDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
    }

    @Override
    public synchronized Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        try {
            return mDateFormat.parse(json.getAsString());
        } catch (ParseException e) {
            Timber.e("deserialize: %s", e.toString());
            throw new JsonParseException(e);
        }
    }

    @Override
    public synchronized JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
        return new JsonPrimitive(mDateFormat.format(src));
    }
}
