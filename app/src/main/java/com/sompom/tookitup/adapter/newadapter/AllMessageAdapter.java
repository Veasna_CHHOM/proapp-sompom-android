package com.sompom.tookitup.adapter.newadapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.ViewGroup;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.RefreshableAdapter;
import com.sompom.tookitup.listener.ConversationDataAdaptive;
import com.sompom.tookitup.listener.OnMessageItemClick;
import com.sompom.tookitup.model.ActiveUser;
import com.sompom.tookitup.model.ActiveUserWrapper;
import com.sompom.tookitup.model.emun.SegmentedControlItem;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.viewholder.BindingViewHolder;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemAllMessageViewModel;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemChatSellerViewModel;

import java.util.List;

/**
 * Created by nuonveyo on 6/26/18.
 */

public class AllMessageAdapter extends RefreshableAdapter<ConversationDataAdaptive, BindingViewHolder> {
    private static final int VIEW_TYPE_CONVERSATION = -1;
    private static final int VIEW_TYPE_YOUR_SELLER = -2;
    private static final int VIEW_TYPE_ACTIVE_SELLER = -3;
    private final OnMessageItemClick mOnMessageItemClick;
    private final SegmentedControlItem mSegmentedControlItem;
    private ActiveUser mActiveSeller;
    private Context mContext;

    public AllMessageAdapter(Context context,
                             List<ConversationDataAdaptive> data,
                             OnMessageItemClick listener,
                             SegmentedControlItem segmentedControlItem) {
        super(data);
        mContext = context;
        mOnMessageItemClick = listener;
        mSegmentedControlItem = segmentedControlItem;
    }

    public void addChatSeller(ActiveUserWrapper activeUser, int index) {
        if (!activeUser.getActiveSeller().isEmpty()) {
            if (mActiveSeller == null) {
                mActiveSeller = activeUser.getActiveSeller();
                if (mDatas.isEmpty()) {
                    mActiveSeller.setShowMore(false);
                }
                mDatas.add(index, mActiveSeller);
                notifyItemInserted(index);
            } else {
                mActiveSeller = activeUser.getActiveSeller();
                if (mDatas.isEmpty()) {
                    mActiveSeller.setShowMore(false);
                }
                mDatas.set(index, mActiveSeller);
                notifyItemChanged(index);
            }
        }
    }

    public void updateOrCreateConversation(Conversation conversation) {
        int index;

        if (mSegmentedControlItem == SegmentedControlItem.Selling) {
            index = getUpdateSellingConversationIndex(conversation);
        } else {
            index = mDatas.indexOf(conversation);
        }

        index = mDatas.indexOf(conversation);

        if (index >= 0) {
            if (mDatas.get(index) instanceof Conversation) {
                Conversation c = (Conversation) mDatas.get(index);
                if (TextUtils.equals(c.getContent(mContext), conversation.getContent(mContext)) &&
                        c.getDate() != null && conversation.getDate() != null &
                        c.getDate().equals(conversation.getDate())) {
                    mDatas.set(index, conversation);
                    notifyItemChanged(index);
                    return;
                }
            }
            mDatas.remove(index);
            mDatas.add(0, conversation);
            notifyItemMoved(index, 0);
            notifyItemChanged(0);

            checkToRemoveMoreConversationTitle();
        } else {
            mDatas.add(0, conversation);
            notifyItemInserted(0);
        }
    }

    private void checkToRemoveMoreConversationTitle() {
        if (mSegmentedControlItem == SegmentedControlItem.All) {
            int lastIndex = mDatas.size() - 1;
            if (mDatas.get(lastIndex) instanceof ActiveUser) {
                ((ActiveUser) mDatas.get(lastIndex)).setShowMore(false);
                notifyItemChanged(lastIndex);
            }
        }
    }

    private int getUpdateSellingConversationIndex(Conversation conversation) {
        Product newProduct = conversation.getProduct();
        if (newProduct == null) {
            newProduct = conversation.getLastMessage().getProduct();
        }

        if (newProduct != null && !mDatas.isEmpty()) {
            String newProductId = newProduct.getId();

            for (int i = 0; i < mDatas.size(); i++) {
                Conversation conversation1 = (Conversation) mDatas.get(i);
                Product product = conversation1.getProduct();
                if (product == null) {
                    product = conversation1.getLastMessage().getProduct();
                }

                if (TextUtils.equals(product.getId(), newProductId)) {
                    return i;
                }
            }
        }
        return -1;
    }

    public void removeConversation(String conversationId) {
        Conversation conversation = new Conversation();
        conversation.setId(conversationId);

        int index = mDatas.indexOf(conversation);
        if (index >= 0) {
            mDatas.remove(index);
            notifyItemRemoved(index);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position >= 0 && position < mDatas.size()) {
            if (mDatas.get(position) instanceof Conversation) {
                return VIEW_TYPE_CONVERSATION;
            } else if (mDatas.get(position) instanceof ActiveUser) {
                if (((ActiveUser) mDatas.get(position)).getSellerItemType() == ActiveUser.SellerItemType.YOUR_SELLER) {
                    return VIEW_TYPE_YOUR_SELLER;
                } else {
                    return VIEW_TYPE_ACTIVE_SELLER;
                }
            }
        }
        return super.getItemViewType(position);
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_CONVERSATION) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_all_messag).build();
        } else {
            BindingViewHolder holder = new BindingViewHolder.Builder(parent, R.layout.list_item_chat_seller).build();
            RecyclerView recyclerView = holder.itemView.findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(parent.getContext(), LinearLayoutManager.HORIZONTAL, false));

            if (viewType == VIEW_TYPE_YOUR_SELLER) {
                recyclerView.setAdapter(new AllMessageYourSellerAdapter(new ActiveUser(), mOnMessageItemClick));
            } else {
                recyclerView.setAdapter(new AllMessageYourSellerAdapter(mActiveSeller, mOnMessageItemClick));
            }
            return holder;
        }
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        final Context context = holder.getBinding().getRoot().getContext();

        if (mDatas.get(position) instanceof Conversation) {
            final Conversation message = (Conversation) mDatas.get(position);
            ListItemAllMessageViewModel viewModel = new ListItemAllMessageViewModel(context,
                    message,
                    mOnMessageItemClick,
                    mSegmentedControlItem);
            holder.setVariable(BR.viewModel, viewModel);

        } else if (mDatas.get(position) instanceof ActiveUser) {
            final ActiveUser message = (ActiveUser) mDatas.get(position);

            ListItemChatSellerViewModel viewModel = new ListItemChatSellerViewModel(message);
            holder.setVariable(BR.viewModel, viewModel);
        }
    }


}
