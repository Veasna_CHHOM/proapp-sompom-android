package com.sompom.tookitup.adapter.newadapter;

import android.view.ViewGroup;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.RefreshableAdapter;
import com.sompom.tookitup.listener.OnUserClickListener;
import com.sompom.tookitup.model.emun.SuggestionTab;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.viewholder.BindingViewHolder;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemSuggestionViewModel;

import java.util.List;

/**
 * Created by nuonveyo on 7/20/18.
 */

public class SuggestionAdapter extends RefreshableAdapter<User, BindingViewHolder> {
    private SuggestionTab mSuggestionTab;
    private OnUserClickListener mOnUserClickListener;

    public SuggestionAdapter(List<User> userList, SuggestionTab tab, OnUserClickListener onUserClickListener) {
        super(userList);
        mSuggestionTab = tab;
        mOnUserClickListener = onUserClickListener;
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_suggestion).build();
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        ListItemSuggestionViewModel viewModel = new ListItemSuggestionViewModel(mDatas.get(position),
                mSuggestionTab, mOnUserClickListener);
        holder.setVariable(BR.viewModel, viewModel);
    }

}
