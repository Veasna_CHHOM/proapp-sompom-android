package com.sompom.tookitup.adapter.newadapter;

import android.content.Context;
import android.view.ViewGroup;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.RefreshableAdapter;
import com.sompom.tookitup.intent.ChatIntent;
import com.sompom.tookitup.listener.OnLikeItemListener;
import com.sompom.tookitup.listener.OnMessageItemClick;
import com.sompom.tookitup.model.Search;
import com.sompom.tookitup.model.emun.FollowItemType;
import com.sompom.tookitup.model.emun.SegmentedControlItem;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewholder.BindingViewHolder;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemAllMessageViewModel;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemLikeViewModel;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemSearchMessageResultProductViewModel;

import java.util.List;

import timber.log.Timber;

/**
 * Created by nuonveyo on 6/8/18.
 */

public class SearchGeneralTypeResultAdapter extends RefreshableAdapter<Search, BindingViewHolder> {
    private static final int PRODUCT = -1;
    private static final int USER = -2;
    private static final int CONVERSATION = -3;

    private final OnLikeItemListener mListener;
    private final Context mContext;
    private String mMyUserId;

    public SearchGeneralTypeResultAdapter(Context context,
                                          List<Search> datas,
                                          OnLikeItemListener likeItemListener) {
        super(datas);
        mContext = context;
        mMyUserId = SharedPrefUtils.getUserId(context);
        mListener = likeItemListener;
    }

    public void setData(List<Search> searches) {
        mDatas.clear();
        mDatas.addAll(searches);
        notifyDataSetChanged();
    }

    @Override
    public BindingViewHolder onCreateView(ViewGroup parent, int viewType) {
        if (viewType == PRODUCT) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_search_result_message_product).build();
        } else if (viewType == CONVERSATION) {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_all_messag).build();
        } else {
            return new BindingViewHolder.Builder(parent, R.layout.list_item_like_viewer).build();
        }
    }

    @Override
    public void onBindData(BindingViewHolder holder, int position) {
        if (mDatas.get(position) instanceof Conversation) {
            final Conversation message = (Conversation) mDatas.get(position);
            ListItemAllMessageViewModel viewModel = new ListItemAllMessageViewModel(mContext, message, new OnMessageItemClick() {
                @Override
                public void onConversationUserClick(User user) {
                    Timber.e("onConversationUserClick");
                }

                @Override
                public void onConversationClick(Conversation conversation) {
                    mContext.startActivity(new ChatIntent(mContext, conversation));
                }
            }, SegmentedControlItem.All);
            holder.setVariable(BR.viewModel, viewModel);
        } else if (mDatas.get(position) instanceof User) {
            final ListItemLikeViewModel viewModel = new ListItemLikeViewModel(mContext,
                    mMyUserId,
                    (User) mDatas.get(position),
                    FollowItemType.FOLLOWER,
                    mListener);
            holder.setVariable(BR.viewModel, viewModel);
        } else if (mDatas.get(position) instanceof Product) {
            final ListItemSearchMessageResultProductViewModel viewModel =
                    new ListItemSearchMessageResultProductViewModel((Product) mDatas.get(position));
            holder.setVariable(BR.viewModel, viewModel);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mDatas.get(position) instanceof Product) {
            return PRODUCT;
        } else if (mDatas.get(position) instanceof User) {
            return USER;
        } else if (mDatas.get(position) instanceof Conversation) {
            return CONVERSATION;
        }
        return super.getItemViewType(position);
    }

    public void notifyData() {
        mMyUserId = SharedPrefUtils.getUserId(mContext);
        notifyDataSetChanged();
    }
}
