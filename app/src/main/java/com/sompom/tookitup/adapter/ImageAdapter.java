package com.sompom.tookitup.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sompom.tookitup.newui.fragment.ImageFragment;

import java.util.List;

/**
 * Created by he.rotha on 3/4/16.
 */
public class ImageAdapter extends FragmentPagerAdapter {
    private final List<ImageFragment> mFragmentProductDetailItems;

    public ImageAdapter(FragmentManager fm, List<ImageFragment> fr) {
        super(fm);
        mFragmentProductDetailItems = fr;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentProductDetailItems.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentProductDetailItems.size();
    }
}
