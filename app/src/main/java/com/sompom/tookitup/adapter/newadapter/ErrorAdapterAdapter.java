package com.sompom.tookitup.adapter.newadapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.model.emun.ErrorLoadingType;
import com.sompom.tookitup.viewholder.BindingViewHolder;
import com.sompom.tookitup.viewmodel.ListItemErrorLoadingViewModel;

/**
 * Created by He Rotha on 11/6/17.
 */

public class ErrorAdapterAdapter extends RecyclerView.Adapter<BindingViewHolder> {
    private ErrorLoadingType mErrorLoadingType;
    private String mErrorMessage;
    private OnClickListener mOnClickListener;

    public ErrorAdapterAdapter(ErrorLoadingType errorLoadingType,
                               String errorMessage,
                               OnClickListener onClickListener) {
        mErrorLoadingType = errorLoadingType;
        mErrorMessage = errorMessage;
        mOnClickListener = onClickListener;
    }

    @Override
    @NonNull
    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.layout_base_error).build();
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
        holder.setVariable(BR.viewModel, new ListItemErrorLoadingViewModel(holder.getContext(),
                mErrorLoadingType,
                mErrorMessage,
                mOnClickListener));
    }

    @Override
    public int getItemCount() {
        return 1;
    }
}
