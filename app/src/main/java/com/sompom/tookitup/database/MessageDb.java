package com.sompom.tookitup.database;

import android.content.Context;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.sompom.tookitup.chat.MessageState;
import com.sompom.tookitup.listener.OnMessageDbListener;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import timber.log.Timber;

/**
 * Created by He Rotha on 10/30/18.
 */
public final class MessageDb {
    private static final int MINIMUM_UNREAD = 5;
    private static final String FIELD_SENDER_ID = "mSenderId";
    private static final String FIELD_STATUS = "mStatus";
    private static final String FIELD_DATE = "mDate";
    private static final String FIELD_ID = "mId";
    private static final String FIELD_CHANNEL_ID = "mChannelId";

    private MessageDb() {

    }

    public static void save(Context context, Chat chat) {
        Timber.i("Save chat: " + new Gson().toJson(chat));
        Realm realm = RealmDb.getInstance(context);
        realm.beginTransaction();
        realm.insertOrUpdate(chat);
        realm.commitTransaction();
        realm.close();
    }

    public static void save(Context context, List<Chat> chats) {
        Realm realm = RealmDb.getInstance(context);
        realm.beginTransaction();
        realm.insertOrUpdate(chats);
        realm.commitTransaction();
        realm.close();
    }

    public static void delete(Context context, Chat chat, OnMessageDbListener listener) {
        if (chat == null) {
            return;
        }
        Realm realm = RealmDb.getInstance(context);
        realm.beginTransaction();

        //delete that chat
        RealmResults<Chat> chats = realm.where(Chat.class)
                .equalTo(FIELD_ID, chat.getId())
                .findAll();

        for (Chat chatItem : chats) {
            if (chatItem.getStatus() == MessageState.SEEN) {
                return;
            }
        }
        for (Chat chat1 : chats) {
            chat1.setDelete(true);
        }


        //update last message to conversation
        Chat lastMessage = realm.where(Chat.class)
                .equalTo(FIELD_CHANNEL_ID, chat.getChannelId())
                .sort(FIELD_DATE, Sort.DESCENDING)
                .findFirst();


        if (lastMessage != null) {
            Conversation conversation = realm.where(Conversation.class)
                    .equalTo(FIELD_ID, lastMessage.getChannelId())
                    .findFirst();
            if (conversation != null) {
                conversation.setContent(lastMessage.getActualContent(context));
                conversation = realm.copyFromRealm(conversation);
                listener.onConversationUpdate(conversation);
            }
        }
        realm.commitTransaction();
        realm.close();
    }

    public static Chat queryById(Context context, String id) {
        Realm realm = RealmDb.getInstance(context);
        Chat chat = realm.where(Chat.class)
                .equalTo(FIELD_ID, id)
                .findFirst();
        if (chat == null) {
            return null;
        } else {
            chat = realm.copyFromRealm(chat);
            realm.close();
            return chat;
        }
    }

    public static List<Chat> queryUnreadMessageFromId(Context context,
                                                      String id,
                                                      String channelId,
                                                      boolean isGroupConversation) {
        Realm realm = RealmDb.getInstance(context);


        Chat currentChat = realm.where(Chat.class)
                .equalTo(FIELD_ID, id)
                .findFirst();

        if (currentChat == null) {
            return new ArrayList<>();
        }

        RealmQuery<Chat> chatRealmQuery = realm.where(Chat.class)
                .lessThanOrEqualTo(FIELD_DATE, currentChat.getDate())
                .equalTo(FIELD_STATUS, MessageState.SENT.getValue())
                .equalTo(FIELD_CHANNEL_ID, channelId);

        //Ignore checking the sender id for the group conversation.
        if (!isGroupConversation) {
            chatRealmQuery.equalTo(FIELD_SENDER_ID, SharedPrefUtils.getUserId(context));
        }

        List<Chat> chatList = chatRealmQuery.findAll();

        chatList = realm.copyFromRealm(chatList);
        for (Chat chat : chatList) {
            chat.setStatus(MessageState.SEEN);
        }
        Collections.sort(chatList);
        realm.beginTransaction();
        realm.insertOrUpdate(chatList);
        realm.commitTransaction();
        realm.close();
        return chatList;
    }

    public static long getMessageCount(Context context, String conversationId) {
        Realm realm = RealmDb.getInstance(context);
        long count = realm.where(Chat.class)
                .equalTo(FIELD_CHANNEL_ID, conversationId)
                .notEqualTo(FIELD_STATUS, MessageState.SENDING.getValue())
                .count();
        realm.close();
        return count;
    }

    @Nullable
    public static Chat getLatestRecipientMessage(Context context, String conversationId) {
        Realm realm = RealmDb.getInstance(context);
        Chat chat;
        try {
            chat = realm.where(Chat.class)
                    .equalTo(FIELD_CHANNEL_ID, conversationId)
                    .notEqualTo(FIELD_SENDER_ID, SharedPrefUtils.getUserId(context))
                    .sort(FIELD_DATE, Sort.DESCENDING)
                    .findFirst();
            if (chat != null) {
                chat = realm.copyFromRealm(chat);
                realm.close();
            }
        } catch (Exception ex) {
            chat = null;
        }
        return chat;
    }

    @Nullable
    public static Chat getLatestGroupRecipientMessage(Context context, String conversationId) {
        Realm realm = RealmDb.getInstance(context);
        Chat chat;
        try {
            chat = realm.where(Chat.class)
                    .equalTo(FIELD_CHANNEL_ID, conversationId)
                    .sort(FIELD_DATE, Sort.DESCENDING)
                    .findFirst();
            if (chat != null) {
                chat = realm.copyFromRealm(chat);
                realm.close();
            }
        } catch (Exception ex) {
            chat = null;
        }
        return chat;
    }


    @Nullable
    public static Chat getLastUnseenMessage(Context context, String conversationId) {
        Realm realm = RealmDb.getInstance(context);
        Chat chat;
        try {
            chat = realm.where(Chat.class)
                    .equalTo(FIELD_CHANNEL_ID, conversationId)
                    .equalTo(FIELD_STATUS, MessageState.SENT.getValue())
                    .sort(FIELD_DATE, Sort.ASCENDING)
                    .findFirst();
            if (chat == null) {
                chat = realm.where(Chat.class)
                        .equalTo(FIELD_CHANNEL_ID, conversationId)
                        .and()
                        .beginGroup()
                        .notEqualTo(FIELD_STATUS, MessageState.SENDING.getValue())
                        .or()
                        .notEqualTo(FIELD_STATUS, MessageState.FAIL.getValue())
                        .endGroup()
                        .sort(FIELD_DATE, Sort.DESCENDING)
                        .findFirst();
            }
            if (chat != null) {
                chat = realm.copyFromRealm(chat);
                realm.close();
            }
        } catch (Exception ex) {
            chat = null;
        }
        return chat;
    }

    public static List<Chat> query(Context context, String conversationId) {
        Realm realm = RealmDb.getInstance(context);
        List<Chat> list = realm.where(Chat.class)
                .equalTo(FIELD_CHANNEL_ID, conversationId)
                .limit(50)
                .findAll()
                .sort(FIELD_DATE, Sort.DESCENDING);
        list = realm.copyFromRealm(list);
        realm.close();
        return list;
    }

    public static List<Chat> queryUnreadMessage(Context context,
                                                String channelId) {
        Realm realm = RealmDb.getInstance(context);
        List<Chat> list = realm.where(Chat.class)
                .equalTo(FIELD_STATUS, MessageState.SENT.getValue())
                .notEqualTo(FIELD_SENDER_ID, SharedPrefUtils.getUserId(context))
                .equalTo(FIELD_CHANNEL_ID, channelId)
                .findAll();
        list = realm.copyFromRealm(list);
        realm.close();
        if (list.size() > MINIMUM_UNREAD) {
            list = list.subList(list.size() - MINIMUM_UNREAD, list.size());
        }
        return list;
    }

    public static List<Chat> queryByStatusAndUserId(Context context,
                                                    MessageState status,
                                                    String senderId) {
        Realm realm = RealmDb.getInstance(context);
        List<Chat> list = realm.where(Chat.class)
                .equalTo(FIELD_STATUS, status.getValue())
                .equalTo(FIELD_SENDER_ID, senderId)
                .findAll();
        list = realm.copyFromRealm(list);
        realm.close();
        User user = SharedPrefUtils.getUser(context);
        for (Chat chat : list) {
            chat.setSender(user);
        }
        return list;
    }

    public static List<Chat> updateSeenMessage(Context context,
                                               Chat chat,
                                               String conversationId) {
        Realm realm = RealmDb.getInstance(context);

        List<Chat> chats;

        //Old code
//        if (chat == null) {
//            chats = realm.where(Chat.class)
//                    .equalTo(FIELD_STATUS, MessageState.SENT.getValue())
//                    .notEqualTo(FIELD_SENDER_ID, SharedPrefUtils.getUserId(context))
//                    .equalTo(FIELD_CHANNEL_ID, conversationId)
//                    .findAll();
//        } else {
//            chats = realm.where(Chat.class)
//                    .equalTo(FIELD_STATUS, MessageState.SENT.getValue())
//                    .lessThanOrEqualTo(FIELD_DATE, chat.getDate())
//                    .equalTo(FIELD_CHANNEL_ID, conversationId)
//                    .findAll();
//        }

        //New code
        //Update all message of conversation as seen either for individual or group chat.
        chats = realm.where(Chat.class)
                .equalTo(FIELD_STATUS, MessageState.SENT.getValue())
                .lessThanOrEqualTo(FIELD_DATE, chat.getDate())
                .equalTo(FIELD_CHANNEL_ID, conversationId)
                .findAll();


        chats = realm.copyFromRealm(chats);

        for (Chat chatItem : chats) {
            chatItem.setStatus(MessageState.SEEN);
        }
        Collections.sort(chats);
        realm.beginTransaction();
        realm.insertOrUpdate(chats);

        //update conversation as read true
        RealmResults<Conversation> list = realm.where(Conversation.class)
                .equalTo(FIELD_ID, conversationId)
                .findAll();
        for (Conversation conversation : list) {
            conversation.setRead(true);
        }

        realm.commitTransaction();
        realm.close();
        return chats;
    }


}
