package com.sompom.tookitup.database;

import android.content.Context;
import android.os.Build;

import com.sompom.tookitup.model.request.QueryProduct;
import com.sompom.tookitup.model.result.Currency;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by imac on 6/19/17.
 */

public final class CurrencyDb {
    private CurrencyDb() {
    }

    public static Currency getSelectedCurrency(Context context) {
        Realm realm = RealmDb.getInstance(context);
        Currency currency =
                realm.where(Currency.class).equalTo("mIsSelected", true).findFirst();

        if (currency == null) {
            return generateDefaultCurrency();
        } else {
            return realm.copyFromRealm(currency);
        }
    }

    public static void updateSelectCurrency(Context context, String currency) {
        Realm realm = RealmDb.getInstance(context);
        realm.beginTransaction();
        Currency currentSelect = getSelectedCurrency(context);

        if (currentSelect == null) {
            Currency currency1 = generateCurrency(currency);
            currency1.setSelected(true);
            realm.insertOrUpdate(currency1);
        } else if (!currentSelect.getName().equals(currency)) {
            currentSelect.setSelected(false);
            realm.insertOrUpdate(currentSelect);

            Currency currency1 = generateCurrency(currency);
            currency1.setSelected(true);
            realm.insertOrUpdate(currency1);
        }
        realm.commitTransaction();
        QueryProduct query = SharedPrefUtils.getQueryProduct(context);
        query.setCurrency(currency);
        SharedPrefUtils.setQueryProduct(query, context);
    }

    public static void insertOrUpdateCurrencies(Context context, List<Currency> currencyList) {
        if (currencyList == null || currencyList.isEmpty()) {
            return;
        }
        Currency selectedCurrency = getSelectedCurrency(context);
        for (Currency currency : currencyList) {
            currency.setSelected(currency.getName().equals(selectedCurrency.getName()));
        }

        Realm realm = RealmDb.getInstance(context);

        realm.beginTransaction();
        realm.insertOrUpdate(currencyList);
        realm.commitTransaction();
    }

    public static List<Currency> getCurrencies(Context context) {
        Realm realm = RealmDb.getInstance(context);
        RealmResults<Currency> currencies = realm.where(Currency.class).findAll();
        return realm.copyFromRealm(currencies);
    }

    public static void shouldUpdateDefaultCurrency(Context context, String currency) {
        Currency selectedCurrency = getSelectedCurrency(context);
        if (selectedCurrency == null) {
            updateSelectCurrency(context, currency);
        } else if (!selectedCurrency.getName().equals(currency)) {
            Realm realm = RealmDb.getInstance(context);
            Currency localCurrency = getCurrencyById(realm, currency);
            if (localCurrency == null) {
                realm.beginTransaction();
                realm.insertOrUpdate(generateCurrency(currency));
                realm.commitTransaction();
            }
        }
    }


    private static Currency generateCurrency(String currency) {
        Currency currency1 = new Currency();
        currency1.setName(currency);
        return currency1;
    }

    private static Currency generateDefaultCurrency() {
        Currency currency = new Currency();
        currency.setName(Currency.DEFAULT_CURRENCY);
        return currency;
    }

    private static Currency getCurrencyById(Realm realm, String currency) {
        return realm.where(Currency.class).equalTo("mName", currency).findFirst();
    }

    public static Locale getLocale(Context context) {
        Locale locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            locale = context.getResources().getConfiguration().getLocales().get(0);
        } else {
            locale = context.getResources().getConfiguration().locale;
        }
        return locale;
    }
}
