package com.sompom.tookitup.database;

import android.content.Context;

import com.sompom.tookitup.model.notification.NotificationAdaptive;
import com.sompom.tookitup.model.notification.NotificationComment;
import com.sompom.tookitup.model.notification.NotificationProduct;
import com.sompom.tookitup.model.notification.NotificationTimeline;
import com.sompom.tookitup.model.notification.NotificationUser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmModel;
import io.realm.RealmResults;

public final class NotificationDb {
    private NotificationDb() {
    }

    public static void save(Context context, List<NotificationAdaptive> adaptives) {
        Realm realm = RealmDb.getInstance(context);
        realm.beginTransaction();

        for (NotificationAdaptive adaptive : adaptives) {
            if (adaptive instanceof RealmModel) {
                realm.insertOrUpdate((RealmModel) adaptive);
            }
        }
        realm.commitTransaction();
        realm.close();
    }

    public static List<NotificationAdaptive> query(Context context) {
        List<NotificationAdaptive> data;

        Realm realm = RealmDb.getInstance(context);
        RealmResults<NotificationTimeline> timeline = realm.where(NotificationTimeline.class).findAll();
        data = new ArrayList<>(realm.copyFromRealm(timeline));

        RealmResults<NotificationProduct> product = realm.where(NotificationProduct.class).findAll();
        data.addAll(realm.copyFromRealm(product));

        RealmResults<NotificationUser> user = realm.where(NotificationUser.class).findAll();
        data.addAll(realm.copyFromRealm(user));

        Collections.sort(data, (o1, o2) -> Long.compare(o2.getPublished().getTime(), o1.getPublished().getTime()));
        return data;
    }

    public static boolean isRead(Context context, NotificationAdaptive obj) {
        Realm realm = RealmDb.getInstance(context);
        NotificationAdaptive data;
        if (obj instanceof NotificationTimeline) {
            data = realm.where(NotificationTimeline.class).equalTo("mId", obj.getId()).findFirst();
        } else if (obj instanceof NotificationProduct) {
            data = realm.where(NotificationProduct.class).equalTo("mId", obj.getId()).findFirst();
        } else if (obj instanceof NotificationComment) {
            data = realm.where(NotificationComment.class).equalTo("mId", obj.getId()).findFirst();
        } else {
            data = realm.where(NotificationUser.class).equalTo("mId", obj.getId()).findFirst();
        }

        boolean isRead = true;
        if (data != null) {
            isRead = data.isRead();
        }
        realm.close();
        return isRead;
    }

    public static void update(Context context, NotificationAdaptive adaptive) {
        Realm realm = RealmDb.getInstance(context);
        realm.beginTransaction();
        realm.insertOrUpdate((RealmModel) adaptive);
        realm.commitTransaction();
        realm.close();
    }
}
