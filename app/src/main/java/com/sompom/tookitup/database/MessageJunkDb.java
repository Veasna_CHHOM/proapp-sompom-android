package com.sompom.tookitup.database;

import android.content.Context;

import com.sompom.tookitup.model.ChatJunk;
import com.sompom.tookitup.model.result.Chat;

import java.util.List;

import io.realm.Realm;

/**
 * Created by He Rotha on 12/28/18.
 */
public final class MessageJunkDb {
    private MessageJunkDb() {
    }

    public static void save(Context context,
                            Chat chat,
                            Chat.SendingType sendingType) {
        ChatJunk chatJunk = new ChatJunk();
        chatJunk.setConversationId(chat.getChannelId());
        chatJunk.setMessageId(chat.getId());
        chatJunk.setSendingType(sendingType);
        chatJunk.setSendTo(chat.getSenderId());
        chatJunk.setGroup(chat.isGroup());

        Realm realm = RealmDb.getInstance(context);
        realm.beginTransaction();
        realm.insertOrUpdate(chatJunk);
        realm.commitTransaction();
        realm.close();
    }

    public static List<ChatJunk> queryAll(Context context) {
        Realm realm = RealmDb.getInstance(context);
        List<ChatJunk> data = realm.where(ChatJunk.class).findAll();
        data = realm.copyFromRealm(data);
        realm.close();
        return data;
    }

    public static void delete(Context context) {
        Realm realm = RealmDb.getInstance(context);
        realm.beginTransaction();
        realm.where(ChatJunk.class)
                .findAll()
                .deleteAllFromRealm();
        realm.commitTransaction();
        realm.close();
    }
}
