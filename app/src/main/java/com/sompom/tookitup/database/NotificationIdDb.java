package com.sompom.tookitup.database;

import android.content.Context;

import com.sompom.tookitup.model.NotificationId;

import io.realm.Realm;

/**
 * Created by He Rotha on 10/30/18.
 */
public final class NotificationIdDb {
    private NotificationIdDb() {

    }


    public static int getId(Context context, String channelId) {
        Realm realm = RealmDb.getInstance(context);
        NotificationId notificationId = realm.where(NotificationId.class)
                .equalTo("mChannelId", channelId)
                .findFirst();
        if (notificationId == null) {
            int id = (int) (realm.where(NotificationId.class).count() + 1);

            NotificationId newId = new NotificationId();
            newId.setNotificationId(id);
            newId.setChannelId(channelId);

            realm.beginTransaction();
            realm.insertOrUpdate(newId);
            realm.commitTransaction();
            realm.close();
            return id;
        } else {
            notificationId = realm.copyFromRealm(notificationId);
            realm.close();
            return notificationId.getNotificationId();
        }
    }

}
