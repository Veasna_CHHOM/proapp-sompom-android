package com.sompom.tookitup.database;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.sinch.gson.Gson;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.Collections;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import timber.log.Timber;

/**
 * Created by He Rotha on 10/30/18.
 */
public final class ConversationDb {
    private ConversationDb() {

    }

    public static void save(Context context, Conversation conversation) {
        assignBackupSeenConversationStatus(conversation);
        Timber.e("save conversation " + new Gson().toJson(conversation));
        Realm realm = RealmDb.getInstance(context);
        realm.beginTransaction();
        realm.insertOrUpdate(conversation);
        realm.commitTransaction();
        realm.close();
    }

    public static boolean isRead(Context context, Chat chat) {
        Realm realm = RealmDb.getInstance(context);
        List<Conversation> list = realm.where(Conversation.class)
                .equalTo("mId", chat.getChannelId()).findAll();
        list = realm.copyFromRealm(list);
        Timber.e("isread " + new Gson().toJson(list));

        long count = realm.where(Conversation.class)
                .equalTo("mId", chat.getChannelId())
                .equalTo("mIsRead", true)
                .count();
        realm.close();
        return count > 0;
    }

    public static Conversation saveConversationFromChat(Context context,
                                                        Chat chat,
                                                        boolean isRead,
                                                        @Nullable User user) {
        Timber.e("start save conversation ");
        Realm realm = RealmDb.getInstance(context);
        realm.beginTransaction();
        Conversation conversation = realm.where(Conversation.class)
                .equalTo("mId", chat.getChannelId())
                .findFirst();

        if (conversation == null) {
            conversation = new Conversation();
            conversation.setId(chat.getChannelId());
        } else {
            conversation = realm.copyFromRealm(conversation);
        }

        conversation.setLastMessage(chat);
        conversation.setContent(chat.getActualContent(context));
        conversation.setDate(chat.getDate());
        conversation.setSenderID(chat.getSenderId());
        conversation.setArchived(false);
        conversation.setRead(isRead);

        if (TextUtils.equals(SharedPrefUtils.getUserId(context), chat.getSenderId())) {
            if (user == null) { // use case: user create new conversation by himself only
                realm.commitTransaction();
                realm.close();
                return conversation;
            }
            conversation.setRecipient(user);
        } else {
            conversation.setRecipient(chat.getSender());
        }

        if (chat.getProduct() != null) {
            conversation.setProduct(chat.getProduct());
            conversation.setSeller(chat.getProduct().getUser().getId());
        }

        realm.insertOrUpdate(conversation);
        realm.commitTransaction();
        realm.close();
        Timber.e("saveConversationFromChat " + new Gson().toJson(conversation));

        return conversation;
    }

    public static Conversation getConversationById(Context context, String conversationId) {
        Timber.e("get counversation by id");

        Realm realm = RealmDb.getInstance(context);
        Conversation conversation = realm.where(Conversation.class)
                .equalTo("mId", conversationId)
                .findFirst();
        if (conversation == null) {
            return null;
        } else {
            conversation = realm.copyFromRealm(conversation);
        }
        realm.close();
        return conversation;
    }


    public static List<Conversation> getConversation(Context context) {
        Timber.e("getconversation list");
        Realm realm = RealmDb.getInstance(context);
        List<Conversation> conversations = realm
                .where(Conversation.class)
                .equalTo("mIsArchived", false)
                .limit(50)
                .findAll();
        conversations = realm.copyFromRealm(conversations);
        realm.close();
        Collections.sort(conversations); //sort by date desc
        return conversations;
    }

    public static void saveConversation(Context context, List<Conversation> conversations) {
        assignBackupSeenConversationStatusList(conversations);
        Timber.e("saveCounter lsit");
        Realm realm = RealmDb.getInstance(context);
        realm.beginTransaction();
        //server doesn't support recipient field, yet. So, we get recipient from local instead
        for (Conversation conversation : conversations) {
            Conversation localConversation = realm
                    .where(Conversation.class)
                    .equalTo("mId", conversation.getId())
                    .findFirst();
            if (localConversation != null) {
                if (conversation.getRecipient(context) == null && localConversation.getRecipient(context) != null) {
                    conversation.setRecipient(realm.copyFromRealm(localConversation.getRecipient(context)));
                }
                //server doesn't handle archived conversation, so it need handle locally
                conversation.setArchived(localConversation.isArchived());
            }

            Chat lastMessage = conversation.getLastMessage();
            boolean isUnread = true;
            String myId = SharedPrefUtils.getUserId(context);

            for (String s : lastMessage.getSeen()) {
                if (s.equalsIgnoreCase(myId)) {
                    isUnread = false;
                    break;
                }
            }
            conversation.setRead(!isUnread);
            conversation.setContent(lastMessage.getActualContent(context));
        }
        realm.insertOrUpdate(conversations);
        realm.commitTransaction();
        realm.close();
    }

    public static void archivedConversation(Context context, String conversationsId) {
        Realm realm = RealmDb.getInstance(context);
        realm.beginTransaction();
        RealmResults<Conversation> data = realm.where(Conversation.class)
                .equalTo("mId", conversationsId)
                .findAll();
        for (Conversation conversation : data) {
            conversation.setArchived(true);
        }
        realm.commitTransaction();
        realm.close();
    }

    private static void assignBackupSeenConversationStatusList(List<Conversation> conversations) {
        for (Conversation conversation : conversations) {
            assignBackupSeenConversationStatus(conversation);
        }
    }

    private static void assignBackupSeenConversationStatus(Conversation conversation) {
        if (conversation.getStatusSeenConversation() != null) {
            conversation.setBackUpStatusSeenConversation(conversation.getStatusSeenConversation().toString());
        }
    }
}
