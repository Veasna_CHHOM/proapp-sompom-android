package com.sompom.tookitup.database;

import android.content.Context;

import io.realm.Realm;

/**
 * Created by imac on 6/20/17.
 */

public final class RealmDb {
    private RealmDb() {
    }

    public static Realm getInstance(Context context) {
        Realm realm;
        try {
            realm = Realm.getDefaultInstance();
        } catch (IllegalStateException ex) {
            Realm.init(context.getApplicationContext());
            realm = Realm.getDefaultInstance();
        }

        return realm;
    }
}
