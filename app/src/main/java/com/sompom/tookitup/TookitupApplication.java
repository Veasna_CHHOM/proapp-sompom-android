package com.sompom.tookitup;

import android.content.Context;
import android.content.res.Configuration;
import android.support.annotation.UiThread;
import android.support.multidex.MultiDexApplication;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.policy.GlobalUploadPolicy;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.flurry.android.FlurryAgent;
import com.onesignal.OneSignal;
import com.sompom.tookitup.database.CurrencyDb;
import com.sompom.tookitup.helper.AppPauseResumeLifecycle;
import com.sompom.tookitup.helper.LocaleManager;
import com.sompom.tookitup.injection.application.ApplicationComponent;
import com.sompom.tookitup.injection.application.DaggerApplicationComponent;
import com.sompom.videomanager.helper.ExoPlayerBuilder;

import java.util.Locale;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import timber.log.Timber;

/**
 * Created by he.rotha on 2/8/16.
 */
public class TookitupApplication extends MultiDexApplication {
    public static final float MAP_ZOOM = 14.0f;
    public static Locale sLocal;
    private ApplicationComponent mApplicationComponent;

    @UiThread
    public ApplicationComponent getApplicationComponent() {
        if (mApplicationComponent == null) {
            mApplicationComponent = DaggerApplicationComponent.builder().build();
        }
        return mApplicationComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //init Realm database
        Realm.init(this);

        //init One Signal push notification
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

        //init Timber for logging
        Timber.plant(new Timber.DebugTree());

        //init Fabric for Crashlytics
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
                .build();

        Fabric.with(this, crashlyticsKit);
        new FlurryAgent.Builder()
                .withLogEnabled(true)
                .build(this, getString(R.string.flurry_key));

        //register lifecycle to track activity lifecycle, and to detect application pause or resume
        this.registerActivityLifecycleCallbacks(new AppPauseResumeLifecycle());

        //Initialize cloudinary
        MediaManager.init(this);
        MediaManager.get().setGlobalUploadPolicy(
                new GlobalUploadPolicy.Builder()
                        .maxRetries(0)
                        .build());

        ExoPlayerBuilder.initApp(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        sLocal = CurrencyDb.getLocale(base);
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleManager.setLocale(this);
    }
}
