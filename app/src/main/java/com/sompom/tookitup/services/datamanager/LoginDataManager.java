package com.sompom.tookitup.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.sompom.tookitup.helper.UserHelper;
import com.sompom.tookitup.helper.upload.ProfileUploader;
import com.sompom.tookitup.listener.OnCompleteListener;
import com.sompom.tookitup.model.request.LoginBody;
import com.sompom.tookitup.model.request.PhoneLoginRequest;
import com.sompom.tookitup.model.request.SocialRequest;
import com.sompom.tookitup.model.result.AuthResponse;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.observable.ResponseHandleErrorFunc;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.io.FileNotFoundException;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * Created by nuonveyo on 4/25/18.
 */

public class LoginDataManager extends AbsDataManager {
    public LoginDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<Response<User>> loginWithPhoneNumber(PhoneLoginRequest request) {
        SharedPrefUtils.setAccessToken(null, getContext());
        return mApiService.loginWithPhone(request);
    }

    public Observable<Response<AuthResponse>> getLoginService(LoginBody loginBody) {
        return mApiService.login(loginBody);
    }

    public Observable<Response<Object>> checkFbExist(SocialRequest socialRequest) {
        return mApiService.checkFbExist(socialRequest);
    }

    public Observable<Response<User>> registerUser(User user) {
        return mApiService.registerUser(user);
    }

    public Observable<Response<User>> updateUser(String userId, User user) {
        user.setAccountStatus(null); //make sure doesn't update account status to server
        return mApiService.updateUser(userId, UserHelper.validateUserPostBody(user));
    }

    public Observable<Response<User>> loginOrRegister(User user, OnCompleteListener<String> uploadImageListener) {
        if (TextUtils.isEmpty(user.getId())) {
            final String localProfileUrl = user.getUserProfile();
            if (TextUtils.isEmpty(localProfileUrl) || localProfileUrl.startsWith("http")) {
                user.setUserProfile(localProfileUrl);
            } else {
                user.setUserProfile(null);
            }

            return registerUser(user)
                    .concatMap(new ResponseHandleErrorFunc<>(getContext(), true))
                    .concatMap((Function<Response<User>, ObservableSource<Response<User>>>) userResult -> {
                        User registeredUser = userResult.body();
                        registeredUser.setUserProfile(localProfileUrl);
                        if (TextUtils.isEmpty(registeredUser.getUserProfile()) || registeredUser.getUserProfile().startsWith("http")) {
                            return Observable.just(userResult);
                        } else {
                            return uploadImageAndUpdate(registeredUser, uploadImageListener);
                        }
                    });
        } else {
            if (TextUtils.isEmpty(user.getUserProfile()) || user.getUserProfile().startsWith("http")) {
                SharedPrefUtils.setAccessToken(user.getAccessToken(), mContext);
                return updateUser(user.getId(), user);
            } else {
                return uploadImageAndUpdate(user, uploadImageListener);
            }
        }
    }

    private Observable<Response<User>> uploadImageAndUpdate(User user, OnCompleteListener<String> uploadImageListener) {
        SharedPrefUtils.setAccessToken(user.getAccessToken(), mContext);
        return ProfileUploader.uploadProfileImage(mContext, user.getUserProfile())
                .onErrorResumeNext(throwable -> {
                    if (throwable.getCause() instanceof FileNotFoundException) {
                        return Observable.just(new ProfileUploader.UploadResult());
                    }
                    return Observable.error(throwable);
                })
                .concatMap((Function<ProfileUploader.UploadResult, ObservableSource<Response<User>>>) s -> {
                    if (s != null) {
                        user.setUserProfile(s.getUrl());
                        uploadImageListener.onComplete(s.getUrl());
                    } else {
                        uploadImageListener.onComplete(null);
                    }
                    return updateUser(user.getId(), user)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread());
                });
    }
}
