package com.sompom.tookitup.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.google.android.gms.ads.AdView;
import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.SoundEffectService;
import com.sompom.tookitup.injection.game.MoreGameAPIQualifier;
import com.sompom.tookitup.injection.productserialize.ProductSerializeQualifier;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.emun.ContentType;
import com.sompom.tookitup.model.emun.Range;
import com.sompom.tookitup.model.request.QueryProduct;
import com.sompom.tookitup.model.request.ReportRequest;
import com.sompom.tookitup.model.result.LoadMoreWrapper;
import com.sompom.tookitup.model.result.MoreGame;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.WallLoadMoreWrapper;
import com.sompom.tookitup.observable.ResponseHandleErrorFunc;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.utils.AdsLoaderUtil;
import com.sompom.tookitup.utils.BannerGenerator;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by He Rotha on 7/10/18.
 */
public class WallStreetDataManager extends StoreDataManager {
    private static final int DISTANCE = 20000; //20000 meter
    private final ApiService mMoreGameService;
    private final ApiService mSerialApiService;
    private List<AdView> mAdViews;
    private String mCurrentWallPage = "0";
    private String mCurrentProductPage = "0";
    private double mLat;
    private double mLng;
    private boolean mIsEnableQueryOption;

    public WallStreetDataManager(Context context,
                                 ApiService apiService,
                                 @ProductSerializeQualifier ApiService serialApiService,
                                 @MoreGameAPIQualifier ApiService moreGame) {
        super(context, apiService);
        setPaginationSize(5);
        mSerialApiService = serialApiService;
        mMoreGameService = moreGame;
    }

    public void setEnableQueryOption(boolean enableQueryOption) {
        mIsEnableQueryOption = enableQueryOption;
    }

    public List<AdView> getAdViews() {
        if (mAdViews == null) {
            mAdViews = BannerGenerator.get(getContext());
        }
        return mAdViews;
    }

    public Observable<WallLoadMoreWrapper<Adaptive>> getData(double lat, double lng) {
        mLat = lat;
        mLng = lng;
        if (mIsEnableQueryOption) {

            QueryProduct query = SharedPrefUtils.getQueryProduct(getContext());
            query.initCurrency(getContext());
            query.setLatitude(lat);
            query.setLongitude(lng);
            if (query.getMaxPrice() == 0) {
                query.setMaxPrice(Range.RANGE_10.getValue());
            }

            return mSerialApiService.getProduct(query, getCurrentPage(), getPaginationSize())
                    .concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                    .concatMap(adaptiveResultList -> {
                        checkPagination(adaptiveResultList.body());
                        return Observable.just(adaptiveResultList.body());
                    }).concatMap(adaptiveLoadMoreWrapper -> AdsLoaderUtil.loadStreetWall(mContext, mApiService, adaptiveLoadMoreWrapper))
                    .onErrorResumeNext(throwable -> {
                        Timber.e(throwable);
                        WallLoadMoreWrapper<Adaptive> data = new WallLoadMoreWrapper<>();
                        data.setData(Collections.emptyList());
                        return Observable.just(data);
                    });
        } else {
            return mSerialApiService.getWallStreet(getPaginationSize(), lat, lng, DISTANCE, mCurrentWallPage, mCurrentProductPage)
                    .concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                    .concatMap(adaptiveResultList -> {
                        checkPage(adaptiveResultList.body());
                        return Observable.just(adaptiveResultList.body());
                    }).concatMap(adaptiveLoadMoreWrapper -> AdsLoaderUtil.loadStreetWall(mContext, mApiService, adaptiveLoadMoreWrapper))
                    .onErrorResumeNext(throwable -> {
                        WallLoadMoreWrapper<Adaptive> data = new WallLoadMoreWrapper<>();
                        data.setData(Collections.emptyList());
                        return Observable.just(data);
                    });
        }

    }

    public Observable<LoadMoreWrapper<Adaptive>> getMyWallStreet(String userId) {
        return mSerialApiService.getWallStreetByUserId(userId, getCurrentPage(), getPaginationSize())
                .concatMap(adaptiveResultList -> {
                    checkPagination(adaptiveResultList.body());
                    return Observable.just(adaptiveResultList.body());
                });
    }

    private void checkPage(WallLoadMoreWrapper<?> data) {
        setCanLoadMore(!TextUtils.isEmpty(data.getNextProduct()) && !TextUtils.isEmpty(data.getNextPage()));
        if (isCanLoadMore()) {
            mCurrentProductPage = data.getNextProduct();
            mCurrentWallPage = data.getNextPage();
        }
    }

    @Override
    public void resetPagination() {
        super.resetPagination();
        mCurrentWallPage = "0";
        mCurrentProductPage = "0";
    }

    public Observable<LoadMoreWrapper<Adaptive>> loadMoreMyWallStreet(String userId) {
        if (isCanLoadMore()) {
            return getMyWallStreet(userId);
        }
        return Observable.error(new Throwable("Cannot load more"));
    }

    public Observable<WallLoadMoreWrapper<Adaptive>> loadMoreData() {
        if (isCanLoadMore()) {
            return getData(mLat, mLng);
        }
        return Observable.error(new Throwable("Cannot load more"));
    }

    public Observable<List<MoreGame>> getMoreGame() {
        return mMoreGameService.getMoreGame(getContext().getString(R.string.more_game_url));
    }

    public Observable<Response<Object>> deletePost(String id) {
        return mApiService.deleteLifeStream(id);
    }

    public Observable<Response<Object>> deleteProduct(Product product) {
        return mApiService.deleteProduct(product.getId());
    }

    public Observable<Response<Object>> likePost(String postId, ContentType contentType, boolean isLike) {
        if (isLike) {
            SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.WOOSH);
            return mApiService.likeContentType(contentType.getValue(), postId);
        } else {
            SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
            return mApiService.unlikeContentType(contentType.getValue(), postId);
        }
    }

    public Observable<Response<Object>> reportProduct(String id, ReportRequest reportRequest) {
        return mApiService.report(ContentType.POST.getValue(), id, reportRequest);
    }

    public Observable<Response<Object>> reportPost(String id, ReportRequest reportRequest) {
        return mApiService.report(ContentType.PRODUCT.getValue(), id, reportRequest);
    }
}
