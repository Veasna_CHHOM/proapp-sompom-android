package com.sompom.tookitup.services.datamanager;

import android.content.Context;

import com.sompom.tookitup.broadcast.SoundEffectService;
import com.sompom.tookitup.model.request.FollowBody;
import com.sompom.tookitup.model.result.LoadMoreWrapper;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.observable.ResponseHandleErrorFunc;
import com.sompom.tookitup.services.ApiService;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 6/8/18.
 */

public class LikeViewerDataManager extends AbsLoadMoreDataManager {
    public LikeViewerDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<List<User>> getUserWhoLikeProduct(String contentID) {
        return mApiService.getLikingUser(contentID,
                getCurrentPage(),
                getPaginationSize())
                .concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                .concatMap(userResultList -> {
                    LoadMoreWrapper<User> loadMoreWrapper = userResultList.body();
                    if (loadMoreWrapper != null) {
                        checkPagination(loadMoreWrapper);
                        List<User> conversationList = loadMoreWrapper.getData();
                        return Observable.just(conversationList);
                    }
                    return Observable.empty();
                });
    }

    public Observable<Response<Object>> postFollow(String id) {
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.WOOSH);
        FollowBody followBody = new FollowBody(id);
        return mApiService.postFollow(followBody);
    }

    public Observable<Response<Object>> unFollow(String id) {
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
        FollowBody followBody = new FollowBody(id);
        return mApiService.unFollow(followBody);
    }
}
