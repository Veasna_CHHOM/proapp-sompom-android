package com.sompom.tookitup.services.datamanager;

import android.content.Context;

import com.example.usermentionable.model.UserMentionable;
import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.SoundEffectService;
import com.sompom.tookitup.model.emun.ContentType;
import com.sompom.tookitup.model.request.CommentBody;
import com.sompom.tookitup.model.result.Comment;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.utils.ConvertUserToMentionableUtil;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 6/8/18.
 */

public class PopUpCommentDataManager extends AbsLoadMoreDataManager {
    //TODO: need to remove test service
    private final ApiService mApiServiceTest;

    public PopUpCommentDataManager(Context context, ApiService apiService, ApiService apiServiceTest) {
        super(context, apiService);
        mApiServiceTest = apiServiceTest;
        int pageSize = getContext().getResources().getInteger(R.integer.number_request_item);
        setPaginationSize(pageSize);
    }

    public Observable<Response<Comment>> postComment(String mainCommentId, Comment comment) {
        return mApiService.postComment(mainCommentId, Comment.getNewComment(comment, false));
    }

    public Observable<Response<List<Comment>>> postSubComment(String contentId, String mainCommentId, Comment comment) {
        return mApiService.postSubComment(contentId, mainCommentId, Comment.getNewComment(comment, true));
    }

    public Observable<Response<Comment>> updateComment(String id, CommentBody comment) {
        return mApiService.updateComment(id, comment);
    }

    public Observable<Response<Object>> deleteComment(String id) {
        return mApiService.deleteComment(id);
    }

    public Observable<Response<Object>> deleteDeleteComment(String commentId, String subCommentId) {
        return mApiService.deleteSubComment(commentId, subCommentId);
    }

    public Observable<Response<List<Comment>>> getCommentList(String contentId, String lastCommentId) {
        resetPagination();
        return mApiService.getCommentByContentId(contentId, lastCommentId, getPaginationSize())
                .concatMap(commentResultList -> {
                    List<Comment> commentList = commentResultList.body();
                    if (commentList == null || commentList.isEmpty() || commentList.size() < getPaginationSize()) {
                        setCanLoadMore(false);
                    } else {
                        setCanLoadMore(true);
                    }
                    return Observable.just(commentResultList);
                });
    }

    public Observable<List<UserMentionable>> getUserMentionList(String userName) {
        return ConvertUserToMentionableUtil.getUserMentionList(mContext, mApiService, userName);
    }

    public Observable<Response<List<Comment>>> getOldSubCommentList(String rootCommentId, String lastSubCommentId) {
        return mApiService.getPreviousSubCommentList(rootCommentId, lastSubCommentId, getPaginationSize())
                .concatMap(listResponse -> {
                    List<Comment> commentList = listResponse.body();
                    if (commentList == null || commentList.isEmpty() || commentList.size() < getPaginationSize()) {
                        setCanLoadMore(false);
                    } else {
                        setCanLoadMore(true);
                    }
                    return Observable.just(listResponse);
                });
    }

    public Observable<Response<Object>> postLikeComment(String commentId, boolean isLike) {
        if (isLike) {
            SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.WOOSH);
            return mApiService.likeContentType(ContentType.COMMENT.getValue(), commentId);
        } else {
            SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
            return mApiService.unlikeContentType(ContentType.COMMENT.getValue(), commentId);
        }
    }

    public Observable<Response<Object>> postLikeSubComment(String commentId, boolean isLike) {
        if (isLike) {
            SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.WOOSH);
            return mApiService.likeContentType(ContentType.SUB_COMMENT.getValue(), commentId);
        } else {
            SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
            return mApiService.unlikeContentType(ContentType.SUB_COMMENT.getValue(), commentId);
        }
    }

    public Observable<Response<List<Comment>>> updateSubComment(String contentId,
                                                                String commentId,
                                                                CommentBody commentBody) {
        return mApiService.updateSubComment(contentId, commentId, commentBody);
    }
}
