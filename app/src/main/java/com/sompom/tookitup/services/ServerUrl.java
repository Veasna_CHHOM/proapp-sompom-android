package com.sompom.tookitup.services;


import android.support.annotation.NonNull;

import com.sompom.tookitup.BuildConfig;

/**
 * Created by imac on 12/9/15.
 */
public final class ServerUrl {
    private static final String LOCAL_SERVER = "http://209.97.161.132:7580/";
    private static final String LIVE_SERVER = "http://209.97.161.132:7580/";

    private ServerUrl() {

    }


    @NonNull
    public static String generateUrl() {
        return getDomainUrl();
    }

    @NonNull
    public static String getDomainUrl() {
        String serverUrl;
        if (BuildConfig.DEBUG) {
            serverUrl = LOCAL_SERVER;
        } else {
            serverUrl = LIVE_SERVER;
        }
        return serverUrl;
    }

}
