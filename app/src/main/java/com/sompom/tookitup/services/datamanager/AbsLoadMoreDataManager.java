package com.sompom.tookitup.services.datamanager;

import android.content.Context;

import com.sompom.tookitup.R;
import com.sompom.tookitup.model.result.LoadMoreWrapper;
import com.sompom.tookitup.model.result.Pagination;
import com.sompom.tookitup.model.result.ResultList;
import com.sompom.tookitup.services.ApiService;

import javax.annotation.Nullable;

/**
 * Created by He Rotha on 11/22/17.
 */

public abstract class AbsLoadMoreDataManager extends AbsDataManager {
    private int mSize;
    private boolean mCanLoadMore = false;
    private Pagination mPagination;
    private String mCurrentPage = "0";

    public AbsLoadMoreDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }


    public int getPaginationSize() {
        if (mSize == 0) {
            mSize = mContext.getResources().getInteger(R.integer.number_request_item);
        }
        return mSize;
    }

    public void setPaginationSize(int size) {
        mSize = size;
    }


    public boolean isCanLoadMore() {
        return mCanLoadMore;
    }

    public void setCanLoadMore(boolean canLoadMore) {
        mCanLoadMore = canLoadMore;
    }

    public void checkPagination(ResultList<?> list) {
        mCanLoadMore = Pagination.isHasLoadMore(list.getData(), getPaginationSize(), list.getPagination());
        mPagination = list.getPagination();
    }

    @Nullable
    public Pagination getPagination() {
        return mPagination;
    }

    public String getCurrentPage() {
        return mCurrentPage;
    }

    public void setCurrentPage(String currentPage) {
        mCurrentPage = currentPage;
    }

    public void resetPagination() {
        mCanLoadMore = false;
        setCurrentPage("0");
    }

    public void checkPagination(LoadMoreWrapper<?> list) {
        if (list.getData().size() >= mSize) {
            mCurrentPage += 1;
            mCanLoadMore = true;
        } else {
            mCanLoadMore = false;
        }
    }
}
