package com.sompom.tookitup.services;

import com.sompom.tookitup.model.searchaddress.google.AddressDetailResponse;
import com.sompom.tookitup.model.searchaddress.google.AutoCompleteResponse;
import com.sompom.tookitup.model.searchaddress.mapbox.MapBoxData;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by He Rotha on 1/4/18.
 */

public interface ApiGoogle {
    @GET("https://maps.googleapis.com/maps/api/place/autocomplete/json")
    Observable<Response<AutoCompleteResponse>> searchGoogleAddress(@Query("key") String key,
                                                                   @Query("input") String input);

    @GET("https://maps.googleapis.com/maps/api/place/details/json")
    Observable<Response<AddressDetailResponse>> searchGoogleAddressDetail(@Query("key") String key,
                                                                          @Query("placeid") String input);

    @GET("https://api.mapbox.com/geocoding/v5/mapbox.places/{input}.json")
    Observable<Response<MapBoxData>> searchMapBoxAddress(@Path("input") String input, @Query("access_token") String key);
}

