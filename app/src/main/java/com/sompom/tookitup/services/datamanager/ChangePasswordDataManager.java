package com.sompom.tookitup.services.datamanager;

import android.content.Context;

import com.sompom.tookitup.model.request.ChangeOrResetPasswordBody;
import com.sompom.tookitup.model.request.PhoneLoginRequest;
import com.sompom.tookitup.model.result.AuthResponse;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.utils.SharedPrefUtils;

import io.reactivex.Observable;
import retrofit2.Response;

public class ChangePasswordDataManager extends AbsDataManager {

    public ChangePasswordDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<Response<AuthResponse>> getChangePassswordService(ChangeOrResetPasswordBody body) {
        return mApiService.changePassword(body);
    }

}
