package com.sompom.tookitup.services.datamanager;

import android.content.Context;

import com.sompom.tookitup.model.request.ForgotPasswordBody;
import com.sompom.tookitup.model.result.ForgotPasswordResponse;
import com.sompom.tookitup.services.ApiService;

import io.reactivex.Observable;
import retrofit2.Response;

public class ForgotPasswordDataManager extends AbsDataManager {

    public ForgotPasswordDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<Response<ForgotPasswordResponse>> getForgotPasswordService(ForgotPasswordBody body) {
        return mApiService.forgotPassword(body);
    }

}
