package com.sompom.tookitup.services.datamanager;

import android.content.Context;

import com.sompom.tookitup.model.ForwardResult;
import com.sompom.tookitup.services.ApiService;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by He Rotha on 9/13/18.
 */
public class ForwardDataManager extends AbsLoadMoreDataManager {

    public ForwardDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<Response<ForwardResult>> getSuggestion() {
        return mApiService.getDefaultOrSearch(null);
    }

    public Observable<Response<ForwardResult>> searchPeople(String query) {
        return mApiService.getDefaultOrSearch(query);
    }


}
