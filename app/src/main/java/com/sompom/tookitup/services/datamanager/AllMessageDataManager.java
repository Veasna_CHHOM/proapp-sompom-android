package com.sompom.tookitup.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.sompom.tookitup.R;
import com.sompom.tookitup.database.ConversationDb;
import com.sompom.tookitup.model.ActiveUser;
import com.sompom.tookitup.model.ActiveUserWrapper;
import com.sompom.tookitup.model.emun.SegmentedControlItem;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.ConversationWrapper;
import com.sompom.tookitup.model.result.LoadMoreWrapper;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.observable.ResponseHandleErrorFunc;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by nuonveyo on 6/27/18.
 */

public class AllMessageDataManager extends AbsLoadMoreDataManager {
    private final SegmentedControlItem mItemType;
    private int mUnreadMessageSize;
    private String mNextPage = "0";
    private boolean mIsFromDb;

    public AllMessageDataManager(Context context,
                                 ApiService apiService,
                                 SegmentedControlItem itemType) {
        super(context, apiService);
        mItemType = itemType;
        setPaginationSize(8);
    }

    private static User getRecipient(Context context, List<User> participants) {
        if (participants != null && !participants.isEmpty()) {
            for (User participant : participants) {
                if (!SharedPrefUtils.checkIsMe(context, participant.getId())) {
                    return participant;
                }
            }
        }
        return null;
    }

    public Observable<ActiveUserWrapper> getSeller(boolean isShowMoreTitle) {
        Observable<ActiveUser> yourSellerOb = handleError(mApiService.getSuggestionMySeller());
        Observable<ActiveUser> activeSellerOb = handleError(mApiService.getSuggestionSeller());

        return Observable.zip(yourSellerOb, activeSellerOb, (yourSeller, activeSeller) -> {
            yourSeller.setShowMore(activeSeller.isEmpty() && isShowMoreTitle);
            yourSeller.setTitle(getContext().getString(R.string.message_your_sellers_title));
            yourSeller.setSellerItemType(ActiveUser.SellerItemType.YOUR_SELLER);

            activeSeller.setShowMore(isShowMoreTitle);
            activeSeller.setTitle(getContext().getString(R.string.message_activate_seller_title));
            activeSeller.setSellerItemType(ActiveUser.SellerItemType.ACTIVE_SELLER);

            final ActiveUserWrapper wrapper = new ActiveUserWrapper();
            wrapper.setYourSeller(yourSeller);
            wrapper.setActiveSeller(activeSeller);
            return wrapper;
        });
    }

    private Observable<ActiveUser> handleError(Observable<Response<ActiveUser>> ob) {
        return ob.concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                .concatMap((Function<Response<ActiveUser>, ObservableSource<ActiveUser>>) activeUserResponse -> {
                    ActiveUser activeUser = activeUserResponse.body();
                    if (activeUser == null || activeUser.isEmpty()) {
                        return Observable.error(new NullPointerException());
                    }
                    return Observable.just(activeUser);
                })
                .onErrorResumeNext((Function<Throwable, ObservableSource<ActiveUser>>) throwable -> Observable.just(new ActiveUser()));
    }

    public boolean isFromDb() {
        return mIsFromDb;
    }

    public int getUnreadMessageSize() {
        return mUnreadMessageSize;
    }

    public Observable<List<Conversation>> getAllMessageList() {
        if (mItemType == SegmentedControlItem.All) {
            return getAllConversation();
        } else {
            return getConversationByType();
        }
    }

    private Observable<List<Conversation>> getAllConversation() {
        mUnreadMessageSize = 0;

        return mApiService.getAllMessageList(mNextPage, getPaginationSize())
                .concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                .concatMap(conversationWrapperResponse -> {

                    ConversationWrapper conversationWrapper = conversationWrapperResponse.body();
                    if (conversationWrapper != null) {
                        checkPagination(conversationWrapper.getConversation().getNextPage());

                        List<Conversation> conversations = new ArrayList<>();
                        if (conversationWrapper.getUnreadConversationList() != null
                                && !conversationWrapper.getUnreadConversationList().isEmpty()) {
                            mUnreadMessageSize = conversationWrapper.getUnreadConversationList().size();
                            conversations.addAll(conversationWrapper.getUnreadConversationList());
                        }

                        LoadMoreWrapper<Conversation> responseConversation = conversationWrapper.getConversation();
                        if (responseConversation.getData() != null && !responseConversation.getData().isEmpty()) {
                            conversations.addAll(responseConversation.getData());
                        }
                        for (Conversation conversation : conversations) {
                            conversation.setRecipient(getRecipient(mContext, conversation.getParticipants()));
                        }
                        ConversationDb.saveConversation(getContext(), conversations);
//                        Collections.sort(conversations);
                        mIsFromDb = false;
                        return Observable.just(conversations);
                    }
                    return Observable.error(new NullPointerException());
                }).onErrorResumeNext(throwable -> {
                    throwable.printStackTrace();
                    mIsFromDb = true;
                    List<Conversation> conversationList = ConversationDb.getConversation(getContext());
                    return Observable.just(conversationList);
                });
    }

    public Observable<List<Conversation>> loadMore() {
        if (isCanLoadMore()) {
            if (mItemType == SegmentedControlItem.All) {
                return mApiService.getAllMessageList(mNextPage, getPaginationSize())
                        .concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                        .concatMap(conversationWrapperResponse -> {
                            ConversationWrapper conversationWrapper = conversationWrapperResponse.body();
                            List<Conversation> conversations = new ArrayList<>();
                            if (conversationWrapper != null) {
                                LoadMoreWrapper<Conversation> responseConversation = conversationWrapper.getConversation();
                                checkPagination(responseConversation.getNextPage());

                                conversations = responseConversation.getData();
                                for (Conversation conversation : conversations) {
                                    conversation.setRecipient(getRecipient(mContext, conversation.getParticipants()));
                                }
                                ConversationDb.saveConversation(getContext(), conversations);
//                                Collections.sort(conversations);
                                mIsFromDb = false;
                            }
                            return Observable.just(conversations);
                        });
            } else {
                return getConversationByType();
            }
        }
        return Observable.error(new Throwable("cannot load more"));
    }

    private Observable<List<Conversation>> getConversationByType() {
        //Old code
//        return mApiService.getConversationList(mItemType.getValue(), mNextPage, getPaginationSize())
        //New code which manage to retrieve user group conversation list
        return getConversationServiceByType()
                .concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                .concatMap(conversationResponse -> {
                    List<Conversation> conversations = new ArrayList<>();
                    LoadMoreWrapper<Conversation> responseConversation = conversationResponse.body();
                    if (responseConversation != null) {
                        checkPagination(responseConversation.getNextPage());
                        conversations = responseConversation.getData();
                    }
                    for (Conversation conversation : conversations) {
                        conversation.setRecipient(getRecipient(mContext, conversation.getParticipants()));
                    }
                    Collections.sort(conversations);
                    ConversationDb.saveConversation(getContext(), conversations);
                    return Observable.just(conversations);
                });
    }

    private Observable<Response<LoadMoreWrapper<Conversation>>> getConversationServiceByType() {
        if (mItemType == SegmentedControlItem.Buying) {
            //Group
            return mApiService.getGroupConversationList(getMyUserId());
        } else {
            return mApiService.getConversationList(mItemType.getValue(), mNextPage, getPaginationSize());
        }
    }

    public void resetNextPage() {
        setCanLoadMore(false);
        mNextPage = "0";
    }

    private void checkPagination(String nextPage) {
        if (!TextUtils.isEmpty(nextPage)) {
            setCanLoadMore(true);
            mNextPage = nextPage;
        } else {
            setCanLoadMore(false);
        }

        Timber.e("mNextPage: %s", mNextPage);
    }
}
