package com.sompom.tookitup.services.datamanager;

import android.content.Context;

import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.UserListAdaptive;
import com.sompom.tookitup.model.UserGroup;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.ApiService;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import timber.log.Timber;

public class UserListManager extends AbsLoadMoreDataManager {

    public UserListManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<List<UserListAdaptive>> getUserGroupListService() {
        return mApiService.getUserGroupList(getMyUserId()).concatMap(listResponse -> observer -> {
            Timber.e("listResponse: " + listResponse.code());
            Timber.e("isSuccessful: " + listResponse.isSuccessful());
            if (listResponse.isSuccessful()) {
                List<UserListAdaptive> newData = new ArrayList<>();
                if (listResponse.body() != null) {
                    for (UserGroup userGroup : listResponse.body()) {
                        newData.add(userGroup);
                        if (userGroup.getParticipants() != null) {
                            newData.addAll(userGroup.getParticipants());
                        }
                    }
                }
                observer.onNext(newData);
                observer.onComplete();
            } else {
                observer.onError(new ErrorThrowable(listResponse.code(),
                        mContext.getString(R.string.error_general)));
            }
        });
    }
}
