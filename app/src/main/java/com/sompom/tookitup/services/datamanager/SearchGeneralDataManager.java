package com.sompom.tookitup.services.datamanager;

import android.content.Context;

import com.sompom.tookitup.R;
import com.sompom.tookitup.model.ActiveUser;
import com.sompom.tookitup.model.Locations;
import com.sompom.tookitup.model.result.SearchGeneralUserWrapper;
import com.sompom.tookitup.observable.ResponseHandleErrorFunc;
import com.sompom.tookitup.services.ApiService;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by nuonveyo on 6/27/18.
 */

public class SearchGeneralDataManager extends AbsLoadMoreDataManager {
    public SearchGeneralDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<List<ActiveUser>> getSearchGeneralUser(Locations locations) {
        return mApiService.getSearchGeneralUser(locations.getLatitude(), locations.getLongtitude())
                .concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                .concatMap(searchGeneralUserWrapperResponse -> {
                    SearchGeneralUserWrapper wrapper = searchGeneralUserWrapperResponse.body();
                    if (wrapper != null) {
                        List<ActiveUser> activeUser = new ArrayList<>();

//                        ActiveUser nearby = wrapper.getNearbyUsers();
//                        if (nearby != null && !nearby.isEmpty()) {
//                            nearby.setTitle(getContext().getString(R.string.search_general_nearby_title));
//                            nearby.setSellerItemType(ActiveUser.SellerItemType.PEOPLE_NEARBY);
//                            activeUser.add(nearby);
//                        }

                        ActiveUser suggestion = wrapper.getSuggestedUsers();
                        if (suggestion != null && !suggestion.isEmpty()) {
                            suggestion.setTitle(getContext().getString(R.string.search_general_suggested_people_title));
                            suggestion.setSellerItemType(ActiveUser.SellerItemType.SUGGESTED_PEOPLE);
                            activeUser.add(suggestion);
                        }

//                        ActiveUser lastSearch = wrapper.getLastSearchUsers();
//                        if (lastSearch != null && !lastSearch.isEmpty()) {
//                            wrapper.getLastSearchUsers().setTitle(getContext().getString(R.string.search_general_last_search_title));
//                            wrapper.getLastSearchUsers().setSellerItemType(ActiveUser.SellerItemType.LAST_SEARCH);
//                            activeUser.add(lastSearch);
//                        }

                        return Observable.just(activeUser);
                    }
                    return Observable.error(new NullPointerException());
                });
    }
}
