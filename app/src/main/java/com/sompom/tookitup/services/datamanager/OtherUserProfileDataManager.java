package com.sompom.tookitup.services.datamanager;

import android.content.Context;

import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.services.ApiService;

import io.reactivex.Observable;
import retrofit2.Response;


public class OtherUserProfileDataManager extends AbsLoadMoreDataManager {

    public OtherUserProfileDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<Response<User>> getUserProfile(String id) {
        return mApiService.getUserById(id);
    }

}
