package com.sompom.tookitup.services.datamanager;

import android.content.Context;

import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.LoadMoreWrapper;
import com.sompom.tookitup.services.ApiService;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by nuonveyo on 6/8/18.
 */

public class ProductHistoryConversationDataManager extends AbsLoadMoreDataManager {
    public ProductHistoryConversationDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<List<Conversation>> getUserWhoChatWithProduct(String productId) {
        return mApiService.getConversationProductHistory(productId,
                getCurrentPage(),
                getPaginationSize()).concatMap(loadMoreWrapperResponse -> {
            LoadMoreWrapper<Conversation> loadMoreWrapper = loadMoreWrapperResponse.body();
            if (loadMoreWrapper != null) {
                checkPagination(loadMoreWrapper);
                List<Conversation> conversationList = loadMoreWrapper.getData();
                return Observable.just(conversationList);
            }
            return Observable.empty();
        });
    }
}
