package com.sompom.tookitup.services.datamanager;

import android.content.Context;

import com.sompom.tookitup.database.CurrencyDb;
import com.sompom.tookitup.model.Price;
import com.sompom.tookitup.model.result.Currency;
import com.sompom.tookitup.services.ApiService;

import io.reactivex.Observable;
import retrofit2.Response;

public class PriceDataManager extends AbsDataManager {
    public PriceDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<Response<Price>> getPrice() {
        Currency currency = CurrencyDb.getSelectedCurrency(mContext);
        return mApiService.getMinMaxPrice(currency.getName());
    }
}
