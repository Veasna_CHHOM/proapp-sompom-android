package com.sompom.tookitup.services.datamanager;

import android.content.Context;

import com.example.usermentionable.model.UserMentionable;
import com.sompom.tookitup.model.LifeStream;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.utils.ConvertUserToMentionableUtil;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 11/29/18.
 */

public class CreateWallStreetDataManager extends StoreDataManager {
    private ApiService mApiServiceTest;

    public CreateWallStreetDataManager(Context context, ApiService apiService, ApiService apiServiceTest) {
        super(context, apiService);
        mApiServiceTest = apiServiceTest;
    }

    public Observable<Response<LifeStream>> updateWallStreet(String postId, LifeStream lifeStream) {
        return mApiService.updateWallStreet(postId, lifeStream);
    }

    public Observable<List<UserMentionable>> getUserMentionList(String userName) {
        return ConvertUserToMentionableUtil.getUserMentionList(mContext, mApiService, userName);
    }
}
