package com.sompom.tookitup.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.sompom.tookitup.broadcast.SoundEffectService;
import com.sompom.tookitup.model.Locations;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.emun.ContentType;
import com.sompom.tookitup.model.request.ReportRequest;
import com.sompom.tookitup.model.result.Category;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.observable.ResponseHandleErrorFunc;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by He Rotha on 10/9/17.
 */

public class ProductManager extends AbsDataManager {

    public ProductManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<Response<Product>> getProductById(String productId, Locations locations) {
        Observable<Response<Product>> productReq;
        productReq = mApiService.getProductById(productId);
        productReq = productReq.concatMap(new ResponseHandleErrorFunc<>(mContext, false));
        return productReq;
    }

    public Observable<Response<List<Product>>> getRelatedProduct(String productId) {
        return mApiService.getRelateProduct(productId);
    }

    public Observable<Response<Product>> postOrUpdate(Product product) {
        if (TextUtils.isEmpty(product.getId())) {
            for (Media media : product.getMedia()) {
                media.setId(null);
            }
            return mApiService.postProduct(product);
        } else {
            return mApiService.updateProduct(product.getId(), product);
        }
    }

    public Observable<Response<List<Category>>> getCategories() {
        return mApiService.getCategoryList(SharedPrefUtils.getProperLanguage(getContext()));
    }

    public Observable<Response<Object>> reportProduct(String id, ReportRequest reportRequest) {
        return mApiService.report(ContentType.PRODUCT.getValue(), id, reportRequest);
    }

    public Observable<Response<Object>> postLikeProduct(String productId, boolean isLike) {
        if (isLike) {
            SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.WOOSH);
            return mApiService.likeContentType(ContentType.PRODUCT.getValue(), productId);
        } else {
            SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
            return mApiService.unlikeContentType(ContentType.PRODUCT.getValue(), productId);
        }
    }

}
