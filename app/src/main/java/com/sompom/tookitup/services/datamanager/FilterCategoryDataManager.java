package com.sompom.tookitup.services.datamanager;

import android.content.Context;

import com.sompom.tookitup.model.result.Category;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 6/20/18.
 */

public class FilterCategoryDataManager extends AbsDataManager {
    public FilterCategoryDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<Response<List<Category>>> getCategoryList() {
        return mApiService.getCategoryList(SharedPrefUtils.getProperLanguage(getContext()));
    }
}
