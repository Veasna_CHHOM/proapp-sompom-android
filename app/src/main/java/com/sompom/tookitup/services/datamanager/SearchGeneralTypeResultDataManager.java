package com.sompom.tookitup.services.datamanager;

import android.content.Context;

import com.sompom.tookitup.broadcast.SoundEffectService;
import com.sompom.tookitup.model.SearchGeneralTypeResult;
import com.sompom.tookitup.model.request.FollowBody;
import com.sompom.tookitup.services.ApiService;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 7/2/18.
 */

public class SearchGeneralTypeResultDataManager extends AbsLoadMoreDataManager {
    private String mProductNextPage;
    private String mUserNextPage;
    private String mBuyingConversationNextPage;
    private String mSellingConversationNextPage;
    private String mConversationNextPage;

    public SearchGeneralTypeResultDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<Response<SearchGeneralTypeResult>> searchGeneralByType(String keyword) {
        resetPagination();
        return mApiService.searchGeneralByType(keyword,
                mProductNextPage,
                mUserNextPage,
                mBuyingConversationNextPage,
                mSellingConversationNextPage,
                mConversationNextPage,
                getPaginationSize());
    }

    public Observable<Response<Object>> postFollow(String followStoreId) {
        FollowBody body = new FollowBody(followStoreId);
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.WOOSH);
        return mApiService.postFollow(body);
    }


    public Observable<Response<Object>> unFollow(String followStoreId) {
        FollowBody body = new FollowBody(followStoreId);
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
        return mApiService.unFollow(body);
    }

    @Override
    public void resetPagination() {
        mProductNextPage = "0";
        mUserNextPage = "0";
        mBuyingConversationNextPage = "0";
        mSellingConversationNextPage = "0";
        mConversationNextPage = "0";
    }
}
