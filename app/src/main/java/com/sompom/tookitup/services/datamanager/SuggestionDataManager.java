package com.sompom.tookitup.services.datamanager;

import android.content.Context;

import com.sompom.tookitup.model.Locations;
import com.sompom.tookitup.model.emun.SuggestionTab;
import com.sompom.tookitup.model.request.FollowBody;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.observable.ResponseHandleErrorFunc;
import com.sompom.tookitup.services.ApiService;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 7/20/18.
 */

public class SuggestionDataManager extends AbsLoadMoreDataManager {
    private SuggestionTab mSuggestionTab;
    private Locations mLocations;


    public SuggestionDataManager(Context context, ApiService apiService, SuggestionTab suggestionTab) {
        super(context, apiService);
        mSuggestionTab = suggestionTab;
    }

    public Observable<List<User>> getListSuggestion(Locations locations) {
        mLocations = locations;
        return mApiService.getSuggestionList(mSuggestionTab.getQueryType(), mLocations.getLatitude(), mLocations.getLongtitude())
                .concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                .concatMap(activeUserResponse -> Observable.just(activeUserResponse.body()));
    }

    public Observable<Response<Object>> follow(User user) {
        FollowBody followBody = new FollowBody(user.getId());
        if (user.isFollow()) {
            return mApiService.postFollow(followBody);
        } else {
            return mApiService.unFollow(followBody);
        }
    }

    public Observable<List<User>> loadMore() {
        if (!isCanLoadMore()) {
            return Observable.error(new Throwable("cannot load more"));
        }
        return getListSuggestion(mLocations);
    }
}
