package com.sompom.tookitup.services.datamanager;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.text.TextUtils;

import com.sompom.tookitup.R;
import com.sompom.tookitup.model.Locations;
import com.sompom.tookitup.model.SearchAddressResult;
import com.sompom.tookitup.model.searchaddress.google.AddressComponent;
import com.sompom.tookitup.model.searchaddress.google.AddressDetailResponse;
import com.sompom.tookitup.model.searchaddress.google.AutoCompleteResponse;
import com.sompom.tookitup.model.searchaddress.google.Prediction;
import com.sompom.tookitup.model.searchaddress.mapbox.Feature;
import com.sompom.tookitup.services.ApiGoogle;
import com.sompom.tookitup.services.ApiService;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.reactivex.Observable;
import timber.log.Timber;

/**
 * Created by nuonveyo on 6/12/18.
 */

public class SelectAddressDataManager extends AbsDataManager {
    private static final String COUNTRY = "country";
    private static final String LOCALITY = "locality";
    private ApiGoogle mApiGoogle;
    private String mGoogleApiKey;
    private String mMapBoxToken;
    private Strategy mStrategy;

    public SelectAddressDataManager(Context context,
                                    ApiService apiService,
                                    ApiGoogle apiGoogle,
                                    Strategy strategy) {
        super(context, apiService);
        mApiGoogle = apiGoogle;
        mStrategy = strategy;
    }

    public Observable<List<SearchAddressResult>> searchAddress(String query) {
        if (mStrategy == Strategy.Google) {
            return mApiGoogle.searchGoogleAddress(getGoogleApiKey(), query)
                    .concatMap(autoCompleteResponseResponse -> {
                        List<SearchAddressResult> searchAddressResults = new ArrayList<>();
                        AutoCompleteResponse autoCompleteResponse = autoCompleteResponseResponse.body();
                        if (autoCompleteResponse != null &&
                                autoCompleteResponse.getPredictions() != null &&
                                autoCompleteResponse.getPredictions() != null) {

                            for (Prediction prediction : autoCompleteResponse.getPredictions()) {
                                SearchAddressResult searchAddressResult = new SearchAddressResult();
                                searchAddressResult.setId(prediction.getPlaceId());
                                if (prediction.getTerms() != null && prediction.getTerms().size() > 2) {
                                    String address = null;
                                    for (int i = 0; i < prediction.getTerms().size(); i++) {
                                        if (i < prediction.getTerms().size() - 2) {
                                            if (TextUtils.isEmpty(address)) {
                                                address = prediction.getTerms().get(i).getValue();
                                            } else {
                                                address = address + ", " + prediction.getTerms().get(i).getValue();
                                            }
                                        } else {
                                            if (TextUtils.isEmpty(searchAddressResult.getCity())) {
                                                searchAddressResult.setCity(prediction.getTerms().get(i).getValue());
                                            } else {
                                                searchAddressResult.setCountry(prediction.getTerms().get(i).getValue());
                                            }
                                        }
                                    }
                                    searchAddressResult.setFullAddress(address);
                                } else {
                                    searchAddressResult.setFullAddress(prediction.getDescription());
                                }
                                searchAddressResults.add(searchAddressResult);
                            }
                        }
                        return Observable.just(searchAddressResults);
                    });
        } else {
            return mApiGoogle.searchMapBoxAddress(query, getMapBoxToken())
                    .concatMap(mapBoxDataResponse -> {
                        List<SearchAddressResult> searchAddressResults = new ArrayList<>();
                        if (mapBoxDataResponse.body() != null && mapBoxDataResponse.body().getFeatures() != null) {
                            for (Feature feature : mapBoxDataResponse.body().getFeatures()) {
                                SearchAddressResult searchAddressResult = new SearchAddressResult();
                                searchAddressResult.setId(feature.getId());
                                searchAddressResult.setFullAddress(feature.getPlaceName());

                                try {
                                    //because Mapbox doesn't provide city name, so we need split text our self
                                    if (!TextUtils.isEmpty(feature.getPlaceName())) {
                                        String[] places = feature.getPlaceName().split(",");
                                        searchAddressResult.setCountry(places[places.length - 1]);
                                        String city = places[places.length - 2];
                                        searchAddressResult.setCity(city);
                                    }
                                } catch (Exception ignore) {
                                }

                                Locations locations = new Locations();
                                locations.setLatitude(feature.getCenter().get(1));
                                locations.setLongtitude(feature.getCenter().get(0));

                                searchAddressResult.setLocations(locations);
                                searchAddressResults.add(searchAddressResult);

                            }
                        }
                        return Observable.just(searchAddressResults);
                    });
        }
    }

    public Observable<SearchAddressResult> searchAddressDetail(SearchAddressResult data) {
        if (data.getLocations() == null) {
            return mApiGoogle.searchGoogleAddressDetail(getGoogleApiKey(), data.getId())
                    .concatMap(addressDetailResponseResponse -> {
                        AddressDetailResponse detailResponse = addressDetailResponseResponse.body();
                        SearchAddressResult searchAddressResult = new SearchAddressResult();

                        if (detailResponse != null) {
                            double lat = detailResponse.getResult().getGeometry().getLocation().getLng();
                            double lng = detailResponse.getResult().getGeometry().getLocation().getLng();
                            String countryCode = null;
                            Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
                            List<Address> addresses;
                            try {
                                addresses = geocoder.getFromLocation(lat, lng, 1);

                                if (addresses != null && !addresses.isEmpty()) {
                                    countryCode = addresses.get(0).getCountryCode();
                                }

                            } catch (Exception ignored) {
                                Timber.e(ignored.toString());
                            }

                            Locations locations = new Locations();
                            locations.setLongtitude(lat);
                            locations.setLatitude(lng);
                            locations.setCountry(countryCode);

                            searchAddressResult.setFullAddress(data.getFullAddress());
                            searchAddressResult.setLocations(locations);
                            for (AddressComponent addressComponent : detailResponse.getResult().getAddressComponents()) {
                                for (String s : addressComponent.getTypes()) {
                                    if (s.equalsIgnoreCase(COUNTRY)) {
                                        searchAddressResult.setCountry(addressComponent.getLongName());
                                    } else if (s.equalsIgnoreCase(LOCALITY)) {
                                        searchAddressResult.setCity(addressComponent.getLongName());
                                    }
                                }
                            }
                        }
                        return Observable.just(searchAddressResult);
                    });
        } else {
            return Observable.just(data);
        }
    }

    private String getGoogleApiKey() {
        if (TextUtils.isEmpty(mGoogleApiKey)) {
            mGoogleApiKey = mContext.getString(R.string.google_place_key);
        }
        return mGoogleApiKey;
    }

    private String getMapBoxToken() {
        if (TextUtils.isEmpty(mMapBoxToken)) {
            mMapBoxToken = mContext.getString(R.string.mapbox_token);
        }
        return mMapBoxToken;
    }

    public enum Strategy {
        Google, Mapbox
    }
}
