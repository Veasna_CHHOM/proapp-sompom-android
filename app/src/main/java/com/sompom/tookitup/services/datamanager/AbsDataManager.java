package com.sompom.tookitup.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.utils.SharedPrefUtils;

/**
 * Created by He Rotha on 10/10/17.
 */

public class AbsDataManager {
    public Context mContext;
    public ApiService mApiService;
    private String mMyUserId;

    public AbsDataManager(Context context, ApiService apiService) {
        mContext = context;
        mApiService = apiService;
    }


    public String getMyUserId() {
        if (TextUtils.isEmpty(mMyUserId)) {
            mMyUserId = SharedPrefUtils.getUserId(mContext);
        }
        return mMyUserId;
    }

    public Context getContext() {
        return mContext;
    }
}
