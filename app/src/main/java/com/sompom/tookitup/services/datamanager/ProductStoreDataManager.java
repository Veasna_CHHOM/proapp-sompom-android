package com.sompom.tookitup.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.SoundEffectService;
import com.sompom.tookitup.model.emun.ContentType;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.services.ApiService;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;


/**
 * Created by He Rotha on 10/5/17.
 */

public class ProductStoreDataManager extends AbsLoadMoreDataManager {
    private String mCurrentUserId;
    private int mStatus;

    public ProductStoreDataManager(Context context, ApiService apiService) {
        super(context, apiService);
        int size = context.getResources().getInteger(R.integer.number_request_item);
        setPaginationSize(size);
    }

    public Observable<Response<List<Product>>> getProductByUserId(User user, int status) {
        mStatus = status;
        mCurrentUserId = user.getId();
        resetPagination();
        return mApiService.getProductListByUserId(mCurrentUserId, 1, mStatus);
    }

    public Observable<Response<Product>> soldProduct(String productId) {
        Product product = new Product();
        return mApiService.updateProduct(productId, product);
    }

    public Observable<Response<List<Product>>> loadMore() {
        if (TextUtils.isEmpty(mCurrentUserId)) {
            return Observable.error(new Throwable("cannot load more data"));
        }
        return mApiService.getProductListByUserId(mCurrentUserId, 1, mStatus);

    }

    public Observable<Response<Object>> delete(Product product) {
        return mApiService.deleteProduct(product.getId());
    }

    public Observable<Response<Product>> closeProduct(String productId) {
        return mApiService.cloneProduct(productId);
    }

    public Observable<Response<Object>> postLikeProduct(String productId, boolean isLike) {
        if (isLike) {
            SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.WOOSH);
            return mApiService.likeContentType(ContentType.PRODUCT.getValue(), productId);
        } else {
            SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
            return mApiService.unlikeContentType(ContentType.PRODUCT.getValue(), productId);
        }
    }
}
