package com.sompom.tookitup.services.datamanager;

import android.content.Context;
import android.databinding.ObservableArrayList;

import com.sompom.tookitup.database.NotificationDb;
import com.sompom.tookitup.model.emun.NotificationItem;
import com.sompom.tookitup.model.notification.NotificationAdaptive;
import com.sompom.tookitup.model.notification.NotificationHeader;
import com.sompom.tookitup.observable.ResponseHandleErrorFunc;
import com.sompom.tookitup.services.ApiService;

import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import timber.log.Timber;


/**
 * Created by He Rotha on 10/19/17.
 */

public class NotificationDataManager extends AbsLoadMoreDataManager {

    public NotificationDataManager(Context context, ApiService apiService) {
        super(context, apiService);
    }

    public Observable<List<NotificationAdaptive>> loadMore() {
        if (!isCanLoadMore()) {
            return Observable.error(new Throwable("can not load more data"));
        }
        return getHistoryNotification();
    }

    private Observable<List<NotificationAdaptive>> getHistoryNotification() {
        return mApiService.getHistoryNotification(getCurrentPage(), getPaginationSize())
                .concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                .concatMap(data -> {
                    checkPagination(data.body());
                    return Observable.create((ObservableOnSubscribe<List<NotificationAdaptive>>) e -> {
                        List<NotificationAdaptive> list = data.body().getData();
                        for (NotificationAdaptive notificationAdaptive : list) {
                            notificationAdaptive.setRead(NotificationDb.isRead(getContext(), notificationAdaptive));
                        }
                        NotificationDb.save(mContext, list);
                        e.onNext(list);
                        e.onComplete();
                    });
                })
                .onErrorResumeNext(throwable -> {
                    Timber.e(throwable);
                    setCanLoadMore(false);
                    return Observable.just(Collections.emptyList());
                });
    }

    public Observable<List<NotificationAdaptive>> getNotificationList() {
        //TODO: refactor thia api after server done
        Observable<List<NotificationAdaptive>> newNotify = mApiService.getNewNotification()
                .concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                .concatMap(data -> {
                    for (NotificationAdaptive datum : data.body().getData()) {
                        datum.setRead(false);
                    }
                    NotificationDb.save(mContext, data.body().getData());
                    return Observable.just(data.body().getData());
                })
                .onErrorResumeNext(throwable -> {
                    Timber.e(throwable);
                    return Observable.just(Collections.emptyList());
                });

        Observable<List<NotificationAdaptive>> historyNotify = getHistoryNotification();

        return Observable.zip(historyNotify, newNotify, (historyNotification, newNotifications) -> {
            List<NotificationAdaptive> adaptives = new ObservableArrayList<>();
            if (!newNotifications.isEmpty()) {
                NotificationHeader notificationHeaderNew = new NotificationHeader();
                notificationHeaderNew.setNotificationItem(NotificationItem.New);
                adaptives.add(notificationHeaderNew);
                adaptives.addAll(newNotifications);
            }
            if (!historyNotification.isEmpty()) {
                NotificationHeader notificationHeaderNew = new NotificationHeader();
                notificationHeaderNew.setNotificationItem(NotificationItem.Earlier);
                adaptives.add(notificationHeaderNew);
                adaptives.addAll(historyNotification);
            }
            if (adaptives.isEmpty()) {
                adaptives.addAll(NotificationDb.query(getContext()));
            }
            return adaptives;
        });
    }
}
