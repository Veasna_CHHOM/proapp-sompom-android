package com.sompom.tookitup.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.JsonElement;
import com.sompom.tookitup.database.MessageDb;
import com.sompom.tookitup.model.BaseChatModel;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.LoadMoreWrapper;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.services.ApiService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by nuonveyo on 6/25/18.
 */

public class ChatDataManager extends AbsLoadMoreDataManager {

    private static final int PAGINATION_SIZE = 18;

    private Conversation mConversation;
    private String mConversationId;

    public ChatDataManager(Context context,
                           ApiService apiService,
                           Conversation conversation) {
        super(context, apiService);
        mConversation = conversation;
        setPaginationSize(PAGINATION_SIZE);
    }

    public Observable<List<BaseChatModel>> getChatList(String conversationId) {
        mConversationId = conversationId;
        final Observable<List<BaseChatModel>> ob;
        if (MessageDb.getMessageCount(getContext(), mConversationId) > 0) {
            ob = getLatestMessage(mConversationId);
        } else {
            ob = getOldestChat(mConversationId, getCurrentPage());
        }

        return ob.onErrorResumeNext(throwable -> {
            Timber.e(throwable);
            return Observable.create(e -> {
                setCanLoadMore(true);
                List<Chat> list = MessageDb.query(getContext(), mConversationId);
                bindSeenStatus(list);
                Collections.sort(list);
                setCanLoadMore(list.size() > getPaginationSize());
                e.onNext(new ArrayList<>(list));
                e.onComplete();
            });
        });
    }

    private Observable<List<BaseChatModel>> getOldestChat(String conversationId, String page) {
        Observable<List<BaseChatModel>> ob;
        ob = mApiService.getChatList(conversationId, page, getPaginationSize())
                .concatMap((Function<Response<LoadMoreWrapper<Chat>>, ObservableSource<List<BaseChatModel>>>) listResponse -> {
                    checkPagination(listResponse.body());
                    List<Chat> chatList = listResponse.body().getData();
                    MessageDb.save(getContext(), chatList);
                    List<Chat> list = MessageDb.query(getContext(), conversationId);
                    bindSeenStatus(list);
                    Collections.sort(list);
                    checkPagination(listResponse.body());
                    return Observable.just(new ArrayList<>(list));
                });
        return ob;
    }

    private Observable<List<BaseChatModel>> getLatestMessage(String conversationId) {
        setCanLoadMore(true);
        return Observable.create((ObservableOnSubscribe<Chat>) e -> {
            e.onNext(MessageDb.getLastUnseenMessage(getContext(), conversationId));
            e.onComplete();
        }).concatMap(new Function<Chat, ObservableSource<Response<List<Chat>>>>() {
            @Override
            public ObservableSource<Response<List<Chat>>> apply(Chat chat) throws Exception {
                Timber.i("getLatestChatList");
                return mApiService.getLatestChatList(conversationId, chat.getId());
            }
        }).concatMap((Function<Response<List<Chat>>, ObservableSource<List<BaseChatModel>>>) o -> Observable.create(e -> {
//            Timber.i(" o.body(): " + new Gson().toJson(o.body()));
            if (o.body() != null && !o.body().isEmpty()) {
                MessageDb.save(getContext(), o.body());
            }
            List<Chat> list = MessageDb.query(getContext(), conversationId);
            bindSeenStatus(list);
            setCanLoadMore(list.size() > getPaginationSize());
            Collections.sort(list);
            e.onNext(new ArrayList<>(list));
            e.onComplete();
        })).onErrorResumeNext(throwable -> {
            Timber.i("onErrorResumeNext: " + throwable.getMessage());
            return Observable.create(e -> {
                List<Chat> list = MessageDb.query(getContext(), conversationId);
                bindSeenStatus(list);
                Collections.sort(list);
                setCanLoadMore(false);
                e.onNext(new ArrayList<>(list));
                e.onComplete();
            });
        });
    }


    public Observable<List<BaseChatModel>> loadMore(Chat lastChatDate) {
        if (lastChatDate == null) {
            return Observable.error(new Throwable("cannot load more pagination is null or empty"));
        }
        Timber.e("load more at " + lastChatDate.getContent());
        SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        mDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

        String date = mDateFormat.format(lastChatDate.getDate());
        Observable<List<BaseChatModel>> ob;
        ob = mApiService.getChatList(mConversationId, date, getPaginationSize())
                .concatMap((Function<Response<LoadMoreWrapper<Chat>>, ObservableSource<List<BaseChatModel>>>) listResponse -> {
                    checkPagination(listResponse.body());
                    List<Chat> chatList = listResponse.body().getData();
                    bindSeenStatus(chatList);
                    MessageDb.save(getContext(), chatList);
                    Collections.sort(chatList);
                    checkPagination(listResponse.body());
                    return Observable.just(new ArrayList<>(chatList));
                });
        return ob;
    }

    private void bindSeenStatus(List<Chat> chatList) {
        if (chatList != null &&
                !chatList.isEmpty() &&
                mConversation != null &&
                mConversation.getParticipants() != null &&
                mConversation.getStatusSeenConversation() != null) {
            for (Map.Entry<String, JsonElement> elementEntry : mConversation.getStatusSeenConversation().entrySet()) {
                if (elementEntry != null &&
                        !TextUtils.isEmpty(elementEntry.getKey()) &&
                        elementEntry.getValue() != null &&
                        !elementEntry.getValue().isJsonNull()) {
                    String chatId = elementEntry.getValue().getAsString();
                    if (!TextUtils.isEmpty(chatId)) {
                        for (Chat chat : chatList) {
                            if (chat.getId().matches(chatId)) {
                                //Retrieve participant from group and assign to each chat.
                                for (User participant : mConversation.getParticipants()) {
                                    //Sender will not put in the seen message list.
                                    if (!chat.getSenderId().matches(elementEntry.getKey()) &&
                                            elementEntry.getKey().matches(participant.getId())) {
                                        if (chat.getSeenParticipants() == null) {
                                            chat.setSeenParticipants(new LinkedList<>());
                                        }
                                        if (!chat.getSeenParticipants().contains(participant)) {
                                            chat.getSeenParticipants().add(participant.cloneForSeenStatus());
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }
    }
}
