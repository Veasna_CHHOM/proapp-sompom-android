package com.sompom.tookitup.services.datamanager;

import android.content.Context;

import com.sompom.tookitup.broadcast.SoundEffectService;
import com.sompom.tookitup.model.LifeStream;
import com.sompom.tookitup.model.emun.ContentType;
import com.sompom.tookitup.model.request.FollowBody;
import com.sompom.tookitup.model.request.ReportRequest;
import com.sompom.tookitup.services.ApiService;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 7/13/18.
 */

public class TimelineDetailDataManager extends PopUpCommentDataManager {

    public TimelineDetailDataManager(Context context, ApiService apiService, ApiService testService) {
        super(context, apiService, testService);
    }

    public Observable<Response<Object>> postFollow(String id) {
        FollowBody body = new FollowBody(id);
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.WOOSH);
        return mApiService.postFollow(body);
    }

    public Observable<Response<Object>> unFollow(String id) {
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
        FollowBody body = new FollowBody(id);
        return mApiService.unFollow(body);
    }

    public Observable<Response<Object>> reportPost(String id, ReportRequest reportRequest) {
        return mApiService.report(ContentType.POST.getValue(), id, reportRequest);
    }

    public Observable<Response<Object>> unFollowShop(String id) {
        FollowBody body = new FollowBody(id);
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
        return mApiService.unFollowShop(body);
    }

    public Observable<Response<LifeStream>> getWallStreetDetail(String id) {
        return mApiService.getWallStreetDetail(id);
    }


    public Observable<Response<Object>> deletePost(String id) {
        return mApiService.deleteLifeStream(id);
    }
}
