package com.sompom.tookitup.services;

import com.sompom.tookitup.model.ActiveUser;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.ForwardResult;
import com.sompom.tookitup.model.LifeStream;
import com.sompom.tookitup.model.Price;
import com.sompom.tookitup.model.SearchGeneralTypeResult;
import com.sompom.tookitup.model.UserGroup;
import com.sompom.tookitup.model.notification.NotificationAdaptive;
import com.sompom.tookitup.model.request.ChangeOrResetPasswordBody;
import com.sompom.tookitup.model.request.CommentBody;
import com.sompom.tookitup.model.request.FollowBody;
import com.sompom.tookitup.model.request.ForgotPasswordBody;
import com.sompom.tookitup.model.request.LoginBody;
import com.sompom.tookitup.model.request.Mention;
import com.sompom.tookitup.model.request.PhoneLoginRequest;
import com.sompom.tookitup.model.request.QueryProduct;
import com.sompom.tookitup.model.request.RefreshTokenRequest;
import com.sompom.tookitup.model.request.ReportRequest;
import com.sompom.tookitup.model.request.RequestBuyProductRequest;
import com.sompom.tookitup.model.request.SocialRequest;
import com.sompom.tookitup.model.result.AuthResponse;
import com.sompom.tookitup.model.result.Category;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.model.result.Comment;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.ConversationWrapper;
import com.sompom.tookitup.model.result.ForgotPasswordResponse;
import com.sompom.tookitup.model.result.LoadMoreWrapper;
import com.sompom.tookitup.model.result.MoreGame;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.Result;
import com.sompom.tookitup.model.result.SearchGeneralUserWrapper;
import com.sompom.tookitup.model.result.Token;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.model.result.UserBadge;
import com.sompom.tookitup.model.result.WallLoadMoreWrapper;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by he.rotha on 4/19/16.
 */
public interface ApiService {
    @GET("/wp-content/uploads/ads/{moregame-service}")
    Observable<List<MoreGame>> getMoreGame(@Path("moregame-service") String moregameService);

    @POST("auth/local")
    Observable<Response<AuthResponse>> login(@Body LoginBody body);

    @POST("auth/forgot-password")
    Observable<Response<ForgotPasswordResponse>> forgotPassword(@Body ForgotPasswordBody body);

    @POST("auth/reset-password")
    Observable<Response<AuthResponse>> changePassword(@Body ChangeOrResetPasswordBody body);

    @POST("auth/local/phone")
    Observable<Response<User>> loginWithPhone(@Body PhoneLoginRequest phoneLoginRequest);

    @POST("auth/local/phone/register")
    Observable<Response<User>> registerUser(@Body User user);

    @PUT("users/{id}")
    Observable<Response<User>> updateUser(@Path("id") String id, @Body User user);

    @GET("users/{id}")
    Observable<Response<User>> getUserById(@Path("id") String userId);

    @POST("auth/local/social")
    Observable<Response<Object>> checkFbExist(@Body SocialRequest request);

    @POST("auth/local/phoneexists/")
    Observable<Response<Object>> checkPhoneExist(@Body PhoneLoginRequest request);

    @POST("api/products")
    Observable<Response<Product>> postProduct(@Body Product product);

    @PUT("api/products/{id}")
    Observable<Response<Product>> updateProduct(@Path("id") String proId, @Body Product product);

    @GET("api/category/get/{language}")
    Observable<Response<List<Category>>> getCategoryList(@Path("language") String lang);


    @GET("api/products/list/seller/{sellerId}")
    Observable<Response<List<Product>>> getProductListByUserId(@Path("sellerId") String userID,
                                                               @Query("_page") int pageNumber,
                                                               @Query("status") int status);

    @GET("api/products/{id}")
    Observable<Response<Product>> getProductById(@Path("id") String id);

    @DELETE("api/products/{id}")
    Observable<Response<Object>> deleteProduct(@Path("id") String id);

    @GET("api/conversation/seller/{productId}/{next}/{value}")
    Observable<Response<LoadMoreWrapper<Conversation>>> getConversationProductHistory(@Path("productId") String porductId,
                                                                                      @Path("next") String next,
                                                                                      @Path("value") int value);

    @GET("like/users/{contentId}/{page}/{limit}")
    Observable<Response<LoadMoreWrapper<User>>> getLikingUser(@Path("contentId") String contentId,
                                                              @Path("page") String page,
                                                              @Path("limit") int limit);

    @POST("posts")
    Observable<Response<LifeStream>> postWallStreet(@Body LifeStream lifeStream);

    @PUT("posts/{id}")
    Observable<Response<LifeStream>> updateWallStreet(@Path("id") String id,
                                                      @Body LifeStream lifeStream);

    @GET("posts/{id}")
    Observable<Response<LifeStream>> getWallStreetDetail(@Path("id") String id);

    @GET("comments/content/{contentId}/{lastCommentId}/{limit}")
    Observable<Response<List<Comment>>> getCommentByContentId(@Path("contentId") String mContentId,
                                                              @Path("lastCommentId") String mLastCommentId,
                                                              @Path("limit") int limit);

    @DELETE("comments/{id}")
    Observable<Response<Object>> deleteComment(@Path("id") String id);

    @PUT("comments/{id}")
    Observable<Response<Comment>> updateComment(@Path("id") String id,
                                                @Body CommentBody comment);

    @POST("comments/{id}")
    Observable<Response<Comment>> postComment(@Path("id") String id,
                                              @Body Comment comment);

    @POST("comments/{contentId}/{commentId}")
    Observable<Response<List<Comment>>> postSubComment(@Path("contentId") String id,
                                                       @Path("commentId") String commentId,
                                                       @Body Comment comment);

    @PUT("comments/{contentId}/{commentId}")
    Observable<Response<List<Comment>>> updateSubComment(@Path("contentId") String contentId,
                                                         @Path("commentId") String id,
                                                         @Body CommentBody comment);

    @DELETE("comments/{commentId}/{subcommentId}")
    Observable<Response<Object>> deleteSubComment(@Path("commentId") String commentId,
                                                  @Path("subcommentId") String subCommetId);

    @GET("subcomments/{commentId}/{lasSubCommentId}/{limit}")
    Observable<Response<List<Comment>>> getPreviousSubCommentList(@Path("commentId") String commentId,
                                                                  @Path("lasSubCommentId") String lastSubCommentId,
                                                                  @Path("limit") int limit);

    @POST("api/unlike/{contentType}/{contentId}")
    Observable<Response<Object>> unlikeContentType(@Path("contentType") String contentType,
                                                   @Path("contentId") String contentId);

    @POST("api/like/{contentType}/{contentId}")
    Observable<Response<Object>> likeContentType(@Path("contentType") String contentType,
                                                 @Path("contentId") String contentId);


    @GET("api/conversation/{conversationId}/{page}/{limit}")
    Observable<Response<LoadMoreWrapper<Chat>>> getChatList(@Path("conversationId") String conversationId,
                                                            @Path("page") String page,
                                                            @Path("limit") int limit);

    @GET("api/conversation/{conversationId}/{messageId}")
    Observable<Response<List<Chat>>> getLatestChatList(@Path("conversationId") String conversationId,
                                                       @Path("messageId") String latestMessageId);

    @GET("api/messages/all/{page}/{limit}")
    Observable<Response<ConversationWrapper>> getAllMessageList(@Path("page") String page,
                                                                @Path("limit") int limit);

    @GET("api/conversation/{type}/{page}/{limit}")
    Observable<Response<LoadMoreWrapper<Conversation>>> getConversationList(@Path("type") String type,
                                                                            @Path("page") String page,
                                                                            @Path("limit") int limit);

    @GET("api/pro/group/authorized/{userId}")
    Observable<Response<LoadMoreWrapper<Conversation>>> getGroupConversationList(@Path("userId") String userId);

    @GET("api/suggestion/seller")
    Observable<Response<ActiveUser>> getSuggestionSeller();

    @GET("api/suggestion/myseller")
    Observable<Response<ActiveUser>> getSuggestionMySeller();

    @GET("api/currencies/{country_code}")
    Observable<Response<List<String>>> getCurrencyByCountryCode(@Path("country_code") String countryCode);

    @GET("api/notifications/badges ")
    Observable<Response<UserBadge>> getUserBadge();

    @GET("api/products/related/{itemId}")
    Observable<Response<List<Product>>> getRelateProduct(@Path("itemId") String itemId);

    @POST("api/products/clone/{id}")
    Observable<Response<Product>> cloneProduct(@Path("id") String id);


    @POST("report/{reportType}/{contentId}")
    Observable<Response<Object>> report(@Path("reportType") String reportType,
                                        @Path("contentId") String contentId,
                                        @Body ReportRequest reportRequest);

    @POST("api/follow")
    Observable<Response<Object>> postFollow(@Body FollowBody request);

    @POST("api/unfollow")
    Observable<Response<Object>> unFollow(@Body FollowBody request);

    @POST("api/unfollowshop")
    Observable<Response<Object>> unFollowShop(@Body FollowBody request);

    @GET("api/newnotifications")
    Observable<Response<LoadMoreWrapper<NotificationAdaptive>>> getNewNotification();

    @GET("api/notifications/history/{page}/{limit}")
    Observable<Response<LoadMoreWrapper<NotificationAdaptive>>> getHistoryNotification(@Path("page") String page,
                                                                                       @Path("limit") int limit);


    @GET("users/following/{userId}/{page}/{limit}")
    Observable<Response<LoadMoreWrapper<User>>> getFollowingByUserId(@Path("userId") String userId,
                                                                     @Path("page") String page,
                                                                     @Path("limit") int limit);

    @GET("users/followers/{userId}/{page}/{limit}")
    Observable<Response<LoadMoreWrapper<User>>> getFollowerByUserId(@Path("userId") String userId,
                                                                    @Path("page") String page,
                                                                    @Path("limit") int limit);

    @GET("view/users/{id}/{page}/{limit}")
    Observable<Response<LoadMoreWrapper<User>>> getUserView(@Path("id") String userId,
                                                            @Path("page") String page,
                                                            @Path("limit") int limit);


    @DELETE("posts/{id}")
    Observable<Response<Object>> deleteLifeStream(@Path("id") String id);


    @GET("api/wall/{limit}")
    Observable<Response<WallLoadMoreWrapper<Adaptive>>> getWallStreet(@Path("limit") int limit,
                                                                      @Query("latitude") double latitude,
                                                                      @Query("longitude") double longitude,
                                                                      @Query("distance") int distance,
                                                                      @Query("nextPost") String nextPost,
                                                                      @Query("nextProduct") String nextProduct);

    @GET("api/userwall/{id}/{page}/{limit}")
    Observable<Response<LoadMoreWrapper<Adaptive>>> getWallStreetByUserId(@Path("id") String userId,
                                                                          @Path("page") String page,
                                                                          @Path("limit") int limit);

    @GET("api/search")
    Observable<Response<SearchGeneralTypeResult>> searchGeneralByType(@Query("keyword") String keyword,
                                                                      @Query("nextProduct") String nextProduct,
                                                                      @Query("nextPeople") String nextPeople,
                                                                      @Query("nextBuying") String nextBuying,
                                                                      @Query("nextSelling") String nextSelling,
                                                                      @Query("nextConversation") String nextConversation,
                                                                      @Query("limit") int limit);

    @POST("api/product/search/{page}/{limit}")
    Observable<Response<WallLoadMoreWrapper<Adaptive>>> getProduct(@Body QueryProduct queryProduct,
                                                                   @Path("page") String page,
                                                                   @Path("limit") int limit);

    @GET("api/product/prices")
    Observable<Response<Price>> getMinMaxPrice(@Query("currency") String currency);

    @GET("api/suggested/")
    Observable<Response<List<User>>> getSuggestionList(@Query("type") String type,
                                                       @Query("lat") double lat,
                                                       @Query("long") double lng);

    @POST("api/post/mention/")
    Observable<Response<List<User>>> getUserMentionList(@Body Mention mention);

    @GET("api/pro/search/default")
    Observable<Response<SearchGeneralUserWrapper>> getSearchGeneralUser(@Query("lat") double lat,
                                                                        @Query("long") double lng);

    @GET("api/share/suggested/")
    Observable<Response<ForwardResult>> getDefaultOrSearch(@Query("keyword") String query);

    @GET("api/map")
    Observable<Response<List<User>>> getNearbyUser(@Query("lat") double lat,
                                                   @Query("long") double lng);

    /**
     * Use for request to buy a product.
     */
    @POST("private/buyingrequest.json")
    Observable<Result<Product>> requestBuyProduct(@Body RequestBuyProductRequest request);

    //
    @POST("public/refreshtoken/user_id.json")
    Call<Result<Token>> getRefreshToken(@Body RefreshTokenRequest refreshTokenRequest);

    @GET("api/pro/users/authorized/{userId}")
    Observable<Response<List<UserGroup>>> getUserGroupList(@Path("userId") String userId);
}
