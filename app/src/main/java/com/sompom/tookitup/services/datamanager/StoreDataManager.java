package com.sompom.tookitup.services.datamanager;

import android.content.Context;

import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.SoundEffectService;
import com.sompom.tookitup.helper.UserHelper;
import com.sompom.tookitup.model.emun.StoreStatus;
import com.sompom.tookitup.model.request.FollowBody;
import com.sompom.tookitup.model.request.PhoneLoginRequest;
import com.sompom.tookitup.model.result.LoadMoreWrapper;
import com.sompom.tookitup.model.result.NotificationSettingModel;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.observable.ResponseHandleErrorFunc;
import com.sompom.tookitup.services.ApiService;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;


/**
 * Created by He Rotha on 10/10/17.
 */

public class StoreDataManager extends AbsLoadMoreDataManager {

    public StoreDataManager(Context context, ApiService apiService) {
        super(context, apiService);
        int size = context.getResources().getInteger(R.integer.number_request_item);
        setPaginationSize(size);
    }

    public Observable<Response<Object>> postFollow(String id) {
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.WOOSH);
        FollowBody body = new FollowBody(id);
        return mApiService.postFollow(body);
    }

    public Observable<Response<Object>> unFollow(String id) {
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
        FollowBody body = new FollowBody(id);
        return mApiService.unFollow(body);
    }

    public Observable<Response<Object>> unFollowShop(String id) {
        FollowBody body = new FollowBody(id);
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
        return mApiService.unFollowShop(body);
    }

    public Observable<Response<User>> getUserById(String userId) {
        return mApiService.getUserById(userId);
    }

    public Observable<Response<User>> getMyUserProfile() {
        return mApiService.getUserById(getMyUserId());
    }


    public Observable<Response<User>> updateMyProfile(User user) {
        user.setAccountStatus(null); //make sure doesn't update account status to server
        return mApiService.updateUser(getMyUserId(), UserHelper.validateUserPostBody(user));
    }

    public Observable<List<User>> getFollower(String userId) {
        return mApiService.getFollowerByUserId(userId,
                getCurrentPage(),
                getPaginationSize())
                .concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                .concatMap(userResultList -> {
                    LoadMoreWrapper<User> loadMoreWrapper = userResultList.body();
                    if (loadMoreWrapper != null) {
                        checkPagination(loadMoreWrapper);
                        List<User> conversationList = loadMoreWrapper.getData();
                        return Observable.just(conversationList);
                    }
                    return Observable.empty();
                });
    }

    public Observable<List<User>> getFollowing(String userId) {
        return mApiService.getFollowingByUserId(userId,
                getCurrentPage(),
                getPaginationSize())
                .concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                .concatMap(userResultList -> {
                    LoadMoreWrapper<User> loadMoreWrapper = userResultList.body();
                    if (loadMoreWrapper != null) {
                        checkPagination(loadMoreWrapper);
                        List<User> conversationList = loadMoreWrapper.getData();
                        return Observable.just(conversationList);
                    }
                    return Observable.empty();
                });
    }

    public Observable<List<User>> getUserView(String userId) {
        return mApiService.getUserView(userId,
                getCurrentPage(),
                getPaginationSize())
                .concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                .concatMap(userResultList -> {
                    LoadMoreWrapper<User> loadMoreWrapper = userResultList.body();
                    if (loadMoreWrapper != null) {
                        checkPagination(loadMoreWrapper);
                        List<User> conversationList = loadMoreWrapper.getData();
                        return Observable.just(conversationList);
                    }
                    return Observable.empty();
                });
    }

    public Observable<List<User>> getUserLike(String userId) {
        return mApiService.getLikingUser(userId,
                getCurrentPage(),
                getPaginationSize())
                .concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                .concatMap(userResultList -> {
                    LoadMoreWrapper<User> loadMoreWrapper = userResultList.body();
                    if (loadMoreWrapper != null) {
                        checkPagination(loadMoreWrapper);
                        List<User> conversationList = loadMoreWrapper.getData();
                        return Observable.just(conversationList);
                    }
                    return Observable.empty();
                });
    }


    public Observable<Response<Object>> loginWithPhoneNumber(String phone, String token) {
        final PhoneLoginRequest phoneLoginRequest = new PhoneLoginRequest(phone, token);
        return mApiService.checkPhoneExist(phoneLoginRequest);
    }

    public Observable<Response<User>> changeMyStoreStatus(StoreStatus status) {
        User user = new User();
        user.setStatus(status);
        return mApiService.updateUser(getMyUserId(), UserHelper.validateUserPostBody(user));
    }

    public Observable<Response<User>> updateNotification(NotificationSettingModel data) {
        User user = new User();
        user.setNotificationSettingModel(data);
        return mApiService.updateUser(getMyUserId(), UserHelper.validateUserPostBody(user));
    }
}
