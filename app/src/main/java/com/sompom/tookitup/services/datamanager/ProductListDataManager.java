package com.sompom.tookitup.services.datamanager;

import android.content.Context;
import android.text.TextUtils;

import com.sompom.tookitup.broadcast.SoundEffectService;
import com.sompom.tookitup.helper.UserHelper;
import com.sompom.tookitup.injection.game.MoreGameAPIQualifier;
import com.sompom.tookitup.injection.productserialize.ProductSerializeQualifier;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.Locations;
import com.sompom.tookitup.model.emun.ContentType;
import com.sompom.tookitup.model.emun.Range;
import com.sompom.tookitup.model.request.FollowBody;
import com.sompom.tookitup.model.request.QueryProduct;
import com.sompom.tookitup.model.request.ReportRequest;
import com.sompom.tookitup.model.result.Currency;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.model.result.UserBadge;
import com.sompom.tookitup.model.result.WallLoadMoreWrapper;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseHandleErrorFunc;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.utils.AdsLoaderUtil;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;


/**
 * Created by He Rotha on 10/5/17.
 */

public class ProductListDataManager extends AbsLoadMoreDataManager {
    private final ApiService mMoreGameService;
    private final ApiService mProductService;
    private ApiService mApiServiceTest;
    private Locations mLocations;

    public ProductListDataManager(Context context,
                                  @MoreGameAPIQualifier ApiService moreGame,
                                  @ProductSerializeQualifier ApiService product,
                                  ApiService apiService) {
        super(context, apiService);
        mMoreGameService = moreGame;
        mProductService = product;
    }

    public ProductListDataManager(Context context,
                                  @MoreGameAPIQualifier ApiService moreGame,
                                  @ProductSerializeQualifier ApiService product,
                                  ApiService apiService,
                                  ApiService apiServiceTest) {
        super(context, apiService);
        mMoreGameService = moreGame;
        mProductService = product;
        mApiServiceTest = apiServiceTest;
    }

    public Observable<WallLoadMoreWrapper<Adaptive>> getProductList(final Locations locations) {
        mLocations = locations;
        QueryProduct query = SharedPrefUtils.getQueryProduct(getContext());
        query.initCurrency(getContext());
        query.setLatitude(locations.getLatitude());
        query.setLongitude(locations.getLongtitude());
        if (query.getMaxPrice() == 0) {
            query.setMaxPrice(Range.RANGE_10.getValue());
        }
        return mProductService.getProduct(query, getCurrentPage(), getPaginationSize())
                .concatMap(new ResponseHandleErrorFunc<>(getContext(), false))
                .concatMap(adaptiveResultList -> {
                    checkPagination(adaptiveResultList.body());
                    return Observable.just(adaptiveResultList.body());
                }).concatMap(adaptiveLoadMoreWrapper -> AdsLoaderUtil.loadStreetWall(mContext, mApiService, adaptiveLoadMoreWrapper))
                .onErrorResumeNext(throwable -> {
                    Timber.e(throwable);
                    WallLoadMoreWrapper<Adaptive> data = new WallLoadMoreWrapper<>();
                    data.setData(Collections.emptyList());
                    setCanLoadMore(false);
                    return Observable.just(data);
                });
    }

    public Observable<Response<Object>> postFollow(String id) {
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.WOOSH);
        FollowBody body = new FollowBody(id);
        return mApiService.postFollow(body);
    }

    public Observable<Response<Object>> unFollow(String id) {
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
        FollowBody body = new FollowBody(id);
        return mApiService.unFollow(body);
    }

    public Observable<Response<Object>> unFollowShop(String id) {
        FollowBody body = new FollowBody(id);
        SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
        return mApiService.unFollowShop(body);
    }

    public Observable<Response<Object>> reportPost(String id, ReportRequest reportRequest) {
        return mApiService.report(ContentType.PRODUCT.getValue(), id, reportRequest);
    }

    public Observable<WallLoadMoreWrapper<Adaptive>> loadMore() {
        if (!isCanLoadMore()) {
            return Observable.error(new Throwable("cannot load more pagination is null or empty"));
        }
        return getProductList(mLocations);
    }

    public Observable<Response<User>> updateUserLocation(Locations location) {
        if (SharedPrefUtils.isLogin(getContext()) && !location.isEmpty()) {
            User user = new User();
            user.setCountry(location.getCountry());
            user.setLatitude(location.getLatitude());
            user.setLongitude(location.getLongtitude());
            return mApiService.updateUser(getMyUserId(), UserHelper.validateUserPostBody(user));
        }
        return Observable.error(new ErrorThrowable(-1, "user might be not login, or locations is empty"));
    }

    public Observable<List<Currency>> insertOrUpdateCurrency(Locations location) {
        if (!TextUtils.isEmpty(location.getCountry())) {
            return mApiServiceTest.getCurrencyByCountryCode(location.getCountry())
                    .concatMap(new ResponseHandleErrorFunc<>(mContext, false))
                    .concatMap(listResponse -> {
                        List<String> currencyString = listResponse.body();
                        List<Currency> currencies = new ArrayList<>();
                        if (currencyString != null && !currencyString.isEmpty()) {
                            for (String s : currencyString) {
                                currencies.add(new Currency(s));
                            }
                        }
                        return Observable.just(currencies);
                    });
        } else {
            return Observable.error(new ErrorThrowable(-1, "locations is empty"));
        }
    }

    public Observable<Response<Object>> postLikeProduct(String productId, boolean isLike) {
        if (isLike) {
            SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.WOOSH);
            return mApiService.likeContentType(ContentType.PRODUCT.getValue(), productId);
        } else {
            SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.DROP);
            return mApiService.unlikeContentType(ContentType.PRODUCT.getValue(), productId);
        }
    }

    public Observable<Response<UserBadge>> getUerBadge() {
        return mApiService.getUserBadge();
    }
}
