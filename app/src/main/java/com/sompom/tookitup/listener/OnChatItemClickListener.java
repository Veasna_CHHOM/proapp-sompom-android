package com.sompom.tookitup.listener;

import com.sompom.tookitup.model.result.Conversation;

/**
 * Created by He Rotha on 9/14/17.
 */
public interface OnChatItemClickListener {
    void onChatItemClicked(Conversation chatItemModel);
}
