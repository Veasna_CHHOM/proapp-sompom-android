package com.sompom.tookitup.listener;

import android.net.Uri;

import com.desmond.squarecamera.model.MediaType;

/**
 * Created by He Rotha on 2/26/18.
 */
public interface MediaChooserFileListener {
    /**
     * call back of chooser file
     *
     * @param uri:    file path
     * @param type:   type of file {@link MediaType}
     * @param width:  file width
     * @param height: file height
     */
    void onSuccess(Uri uri,
                   int type,
                   int width,
                   int height);
}
