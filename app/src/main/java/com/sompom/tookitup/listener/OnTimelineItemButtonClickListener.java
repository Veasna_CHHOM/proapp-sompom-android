package com.sompom.tookitup.listener;

import com.sompom.tookitup.model.Adaptive;

/**
 * Created by nuonveyo on 7/12/18.
 */

public interface OnTimelineItemButtonClickListener extends OnTimelineHeaderItemClickListener {
    void onCommentButtonClick(Adaptive adaptive, int position);

    void onShareButtonClick(Adaptive adaptive, int position);

    void onUserViewMediaClick(Adaptive adaptive);

    void onLikeViewerClick(Adaptive adaptive);
}
