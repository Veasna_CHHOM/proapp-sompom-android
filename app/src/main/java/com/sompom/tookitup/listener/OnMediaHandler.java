package com.sompom.tookitup.listener;

import com.sompom.tookitup.viewmodel.BaseMediaViewModel;

/**
 * Created by He Rotha on 8/29/17.
 */

public interface OnMediaHandler {
    void onPlay(BaseMediaViewModel model);

    void onClick(BaseMediaViewModel model);
}
