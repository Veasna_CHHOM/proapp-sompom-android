package com.sompom.tookitup.listener;

import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.result.Product;

/**
 * Created by He Rotha on 9/11/17.
 */
public interface OnProductItemClickListener {
    void onProductItemClick(Adaptive product);

    void onProductItemEdit(Product product);

}
