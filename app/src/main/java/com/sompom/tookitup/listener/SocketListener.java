package com.sompom.tookitup.listener;

import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFrame;

import java.util.List;
import java.util.Map;

import io.github.sac.BasicListener;
import io.github.sac.Socket;
import timber.log.Timber;

/**
 * Created by He Rotha on 12/24/18.
 */
public abstract class SocketListener implements BasicListener {
    @Override
    public void onConnected(Socket socket, Map<String, List<String>> headers) {
        Timber.v("onConnected " + socket.hashCode());
        onConnected();
    }

    @Override
    public void onDisconnected(Socket socket, WebSocketFrame serverCloseFrame, WebSocketFrame clientCloseFrame, boolean closedByServer) {
        Timber.v("onDisconnected " + socket.hashCode());
    }

    @Override
    public void onConnectError(Socket socket, WebSocketException exception) {
        Timber.v("onConnectError " + exception);
        onConnectError(exception);
    }

    @Override
    public void onAuthentication(Socket socket, Boolean status) {

    }

    @Override
    public void onSetAuthToken(String token, Socket socket) {

    }

    protected abstract void onConnected();

    protected abstract void onConnectError(Exception ex);
}
