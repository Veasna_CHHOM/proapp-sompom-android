package com.sompom.tookitup.listener;

import com.sompom.tookitup.model.result.Category;

public interface OnCategoryClickListener {
    void onCategoryClick(Category category);
}
