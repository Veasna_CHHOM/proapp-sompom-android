package com.sompom.tookitup.listener;

import com.sompom.tookitup.model.emun.SegmentedControlItem;

/**
 * Created by nuonveyo on 6/26/18.
 */

public interface OnSegmentedClickListener {
    void onClick(SegmentedControlItem item);
}
