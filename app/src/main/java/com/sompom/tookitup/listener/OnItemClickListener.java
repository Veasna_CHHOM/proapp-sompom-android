package com.sompom.tookitup.listener;

/**
 * Created by nuonveyo on 6/12/18.
 */

public interface OnItemClickListener<T> {
    void onClick(T t);
}
