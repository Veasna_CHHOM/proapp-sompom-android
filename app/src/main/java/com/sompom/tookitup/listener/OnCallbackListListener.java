package com.sompom.tookitup.listener;


import com.sompom.tookitup.observable.ErrorThrowable;

/**
 * Created by He Rotha on 11/6/17.
 */

public interface OnCallbackListListener<T> extends OnCompleteListListener<T> {
    void onFail(ErrorThrowable ex);
}
