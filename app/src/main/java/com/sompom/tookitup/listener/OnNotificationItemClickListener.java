package com.sompom.tookitup.listener;

import com.sompom.tookitup.model.notification.NotificationAdaptive;

/**
 * Created by nuonveyo
 * on 2/13/19.
 */

public interface OnNotificationItemClickListener {
    void onItemClick(NotificationAdaptive data, int position);
}
