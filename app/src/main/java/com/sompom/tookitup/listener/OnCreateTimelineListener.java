package com.sompom.tookitup.listener;

/**
 * Created by He Rotha on 11/16/18.
 */
public interface OnCreateTimelineListener {
    void onPostWallStreetClick();

    void onCameraClick();
}
