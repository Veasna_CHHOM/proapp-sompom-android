package com.sompom.tookitup.listener;

import com.sompom.tookitup.model.LoadPreviousComment;

/**
 * Created by nuonveyo
 * on 3/12/19.
 */
public interface OnLoadPreviousCommentClickListener {
    void onClick(LoadPreviousComment previousComment, int position);
}
