package com.sompom.tookitup.listener;

import com.sompom.tookitup.model.result.User;

public interface OnUserClickListener {
    void onUserClick(User user);
}
