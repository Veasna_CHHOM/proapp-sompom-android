package com.sompom.tookitup.listener;

import android.support.annotation.Nullable;

import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.model.result.User;

import java.util.List;

/**
 * Created by nuonveyo on 8/30/18.
 */

public interface OnChatItemListener {
    void onImageClick(List<Media> list,
                      int productMediaPosition);

    void onChatItemLongPressClick(Chat chat,
                                  @Nullable Media media,
                                  int chatItemPosition);

    void onForwardClick(Chat chat);

    void onMessageRetryClick(Chat chat);

    User getUser(String id);
}
