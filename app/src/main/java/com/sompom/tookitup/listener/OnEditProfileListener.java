package com.sompom.tookitup.listener;

import com.sompom.tookitup.model.result.NotificationSettingModel;

/**
 * Created by He Rotha on 11/29/18.
 */
public interface OnEditProfileListener extends OnCompleteListener<NotificationSettingModel> {
    void onLogoutClick();
}
