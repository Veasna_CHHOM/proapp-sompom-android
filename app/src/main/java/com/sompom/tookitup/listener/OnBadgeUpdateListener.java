package com.sompom.tookitup.listener;

public interface OnBadgeUpdateListener {
    void onConversationBadgeUpdate(int value);
}
