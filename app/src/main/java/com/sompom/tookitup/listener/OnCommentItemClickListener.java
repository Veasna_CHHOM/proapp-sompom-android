package com.sompom.tookitup.listener;

import com.sompom.tookitup.model.result.Comment;

/**
 * Created by nuonveyo on 7/23/18.
 */

public interface OnCommentItemClickListener {
    void onReplyButtonClick(Comment comment, int position);

    void onLikeButtonClick(Comment comment, int position);

    void onLongPress(Comment comment, int position);
}
