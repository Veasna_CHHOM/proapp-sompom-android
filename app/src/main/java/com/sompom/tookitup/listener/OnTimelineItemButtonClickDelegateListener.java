package com.sompom.tookitup.listener;

import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.result.User;
import com.sompom.videomanager.widget.MediaRecyclerView;

public class OnTimelineItemButtonClickDelegateListener implements OnTimelineItemButtonClickListener {
    private OnTimelineItemButtonClickListener mListener;

    public OnTimelineItemButtonClickDelegateListener(OnTimelineItemButtonClickListener listener) {
        mListener = listener;
    }

    @Override
    public void onCommentButtonClick(Adaptive adaptive, int position) {
        mListener.onCommentButtonClick(adaptive, position);
    }

    @Override
    public void onShareButtonClick(Adaptive adaptive, int position) {
        mListener.onShareButtonClick(adaptive, position);
    }

    @Override
    public void onUserViewMediaClick(Adaptive adaptive) {
        mListener.onUserViewMediaClick(adaptive);
    }

    @Override
    public void onLikeViewerClick(Adaptive adaptive) {
        mListener.onLikeViewerClick(adaptive);
    }

    @Override
    public void onFollowButtonClick(String id, boolean isFollowing) {
        mListener.onFollowButtonClick(id, isFollowing);
    }

    @Override
    public void onShowPostContextMenuClick(Adaptive adaptive, User user, int position) {
        mListener.onShowPostContextMenuClick(adaptive, user, position);
    }

    @Override
    public void onCommentClick(Adaptive adaptive) {
        mListener.onCommentClick(adaptive);
    }

    @Override
    public MediaRecyclerView getRecyclerView() {
        return mListener.getRecyclerView();
    }
}
