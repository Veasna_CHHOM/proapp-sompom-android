package com.sompom.tookitup.listener;

/**
 * Created by nuonveyo on 7/11/18.
 */

public interface OnTimelineActiveUserItemClick extends OnMessageItemClick {
    void onLiveClick();
}
