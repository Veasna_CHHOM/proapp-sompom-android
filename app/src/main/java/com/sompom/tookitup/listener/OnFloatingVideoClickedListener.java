package com.sompom.tookitup.listener;

/**
 * Created by nuonveyo on 5/9/18.
 */

public interface OnFloatingVideoClickedListener {
    void onFloatingVideo(int position, long time);

    void onFullScreenClicked(int position, long time);
}
