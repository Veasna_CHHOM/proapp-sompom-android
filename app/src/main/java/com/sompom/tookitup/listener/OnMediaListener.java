package com.sompom.tookitup.listener;

/**
 * Created by He Rotha on 9/1/17.
 */

public interface OnMediaListener {
    void onMediaPlay(boolean isWait);

    void onMediaPlay();

    void onMediaPause();

    boolean isPlaying();
}
