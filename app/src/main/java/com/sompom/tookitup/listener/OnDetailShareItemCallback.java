package com.sompom.tookitup.listener;

import com.sompom.tookitup.model.WallStreetAdaptive;

/**
 * Created by nuonveyo on 12/5/18.
 */

public interface OnDetailShareItemCallback extends OnClickListener {
    void onMediaClick(WallStreetAdaptive adaptive, int position);
}
