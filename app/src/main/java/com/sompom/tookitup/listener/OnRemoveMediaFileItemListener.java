package com.sompom.tookitup.listener;

/**
 * Created by nuonveyo on 5/8/18.
 */

public interface OnRemoveMediaFileItemListener {
    void onRemoveItem(int position);
}
