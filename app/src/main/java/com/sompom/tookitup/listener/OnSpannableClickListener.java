package com.sompom.tookitup.listener;

/**
 * Created by nuonveyo on 4/30/18.
 */

public interface OnSpannableClickListener {
    void onClick();
}
