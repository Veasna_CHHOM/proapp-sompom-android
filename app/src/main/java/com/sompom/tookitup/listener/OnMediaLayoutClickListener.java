package com.sompom.tookitup.listener;

import com.sompom.tookitup.widget.lifestream.MediaLayout;

/**
 * Created by He Rotha on 9/7/17.
 */

public interface OnMediaLayoutClickListener {
    void onMediaLayoutClick(MediaLayout mediaLayout);
}
