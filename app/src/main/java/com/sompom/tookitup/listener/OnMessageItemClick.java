package com.sompom.tookitup.listener;

import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.User;

/**
 * Created by He Rotha on 7/6/18.
 */
public interface OnMessageItemClick {
    default void onConversationClick(Conversation conversation) {
        //nothing to do on this stuff
    }

    default void onConversationLongClick(Conversation conversation) {
        //nothing to do on this stuff
    }

    void onConversationUserClick(User user);
}
