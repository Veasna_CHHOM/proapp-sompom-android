package com.sompom.tookitup.listener;

/**
 * Created by nuonveyo on 8/27/18.
 */

public interface OnClickListener {
    void onClick();
}
