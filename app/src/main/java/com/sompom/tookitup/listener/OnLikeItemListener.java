package com.sompom.tookitup.listener;

import com.sompom.tookitup.model.result.User;

/**
 * Created by He Rotha on 6/14/18.
 */
public interface OnLikeItemListener {
    void onFollowClick(User user, boolean isFollow);

    void onNotifyItem();
}
