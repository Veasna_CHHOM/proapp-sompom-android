package com.sompom.tookitup.listener;

import com.sompom.tookitup.model.result.User;

import java.util.List;

/**
 * Created by He Rotha on 6/15/18.
 */
public interface OnLayoutLikeListener extends OnCompleteListListener<List<User>> {
    void onNotifyItem();
}
