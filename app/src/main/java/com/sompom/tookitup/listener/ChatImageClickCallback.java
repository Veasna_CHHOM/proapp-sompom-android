package com.sompom.tookitup.listener;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;

/**
 * Created by nuonveyo on 9/4/18.
 */

public class ChatImageClickCallback extends ImageMatrixTouchHandler {
    private OnClickListener mOnClickListener;
    private GestureDetector mGestureDetector;

    public ChatImageClickCallback(Context context, OnClickListener onClickListener) {
        super(context);
        mOnClickListener = onClickListener;
        ViewGesture viewGesture = new ViewGesture();
        mGestureDetector = new GestureDetector(context, viewGesture);
        mGestureDetector.setOnDoubleTapListener(viewGesture);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View view, MotionEvent event) {
        mGestureDetector.onTouchEvent(event);
        return super.onTouch(view, event);
    }

    private class ViewGesture extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            if (mOnClickListener != null) {
                mOnClickListener.onClick();
            }
            return super.onDoubleTap(e);
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (mOnClickListener != null) {
                mOnClickListener.onClick();
            }
            return super.onSingleTapConfirmed(e);
        }
    }
}
