package com.sompom.tookitup.listener;

import com.sompom.tookitup.model.result.Comment;

/**
 * Created by nuonveyo on 11/7/18.
 */

public interface OnEditTextCommentListener {
    void onSendText(Comment comment);

    void onPostCommentFail();

    void onPostCommentSuccess(Comment comment);

    void onUpdateCommentSuccess(Comment comment, int position);
}
