package com.sompom.tookitup.listener;

import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.result.User;
import com.sompom.videomanager.widget.MediaRecyclerView;

/**
 * Created by nuonveyo on 7/12/18.
 */

public interface OnTimelineHeaderItemClickListener {
    void onFollowButtonClick(String id, boolean isFollowing);

    void onShowPostContextMenuClick(Adaptive adaptive,
                                    User user,
                                    int position);

    void onCommentClick(Adaptive adaptive);

    MediaRecyclerView getRecyclerView();
}
