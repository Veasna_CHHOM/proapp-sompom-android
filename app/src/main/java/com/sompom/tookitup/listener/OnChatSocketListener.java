package com.sompom.tookitup.listener;

import com.sompom.tookitup.model.emun.Presence;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.model.result.Conversation;

import java.util.List;

/**
 * Created by He Rotha on 12/24/18.
 */
public interface OnChatSocketListener {
    void onMessageReceive(Chat chat);

    void onMessageTyping(Chat chat);

    void onConversationCreateOrUpdate(Conversation conversation);

    void onConversationDelete(String conversationId);

    void onPresenceUpdate(String userId, Presence presence, long lastActiveTime);

    void onMessageSent(Chat chat);

    boolean isInBg(String channelId);

    List<String> getCurrentChannel();

    void onBadgeUpdate(int value);
}
