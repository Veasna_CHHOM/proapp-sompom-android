package com.sompom.tookitup.listener;

import com.sompom.tookitup.model.Media;

import java.util.List;

/**
 * Created by He Rotha on 6/13/18.
 */
public interface OnProductFormListener {
    void onSetImage(List<Media> list);
}
