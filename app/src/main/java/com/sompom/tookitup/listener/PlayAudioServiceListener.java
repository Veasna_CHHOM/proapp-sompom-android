package com.sompom.tookitup.listener;

/**
 * Created by i-dell on 3/3/18.
 */

public interface PlayAudioServiceListener {

    void onPause(String currentId);

    void onPlaying(String currentId, long duration);

    void onFinish();

    void onResume(String currentId);

    void onError();

    void hideLoading(String currentId);
}
