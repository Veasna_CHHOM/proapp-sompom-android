package com.sompom.tookitup.listener;

import com.sompom.tookitup.model.result.Conversation;

/**
 * Created by He Rotha on 12/26/18.
 */
public interface OnMessageDbListener {
    void onConversationDelete(String conversationId);

    void onConversationUpdate(Conversation conversation);
}
