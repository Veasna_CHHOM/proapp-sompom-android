package com.sompom.tookitup.listener;

import com.sompom.tookitup.model.result.Comment;

/**
 * Created by nuonveyo on 7/23/18.
 */

public interface OnCommentDialogClickListener {
    void onReplaceCommentFragment(Comment comment, int position);

    void onDismissDialog(boolean isDismiss);

    void onNotifyCommentItemChange(Comment comment);

    void onDismissAndRemoveItem(int position);
}
