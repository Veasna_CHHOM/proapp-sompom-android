package com.sompom.tookitup.listener;

import com.sompom.tookitup.model.result.Product;

/**
 * Created by He Rotha on 6/13/18.
 */
public interface OnEditDialogClickListener {
    void onEditClick(Product product);

    void onDeleteClick(Product product);
}
