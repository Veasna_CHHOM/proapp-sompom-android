package com.sompom.tookitup.listener;

/**
 * Created by nuonveyo on 5/9/18.
 */

public interface OnFloatingVideoRemoveListener {
    void onRemove();
}
