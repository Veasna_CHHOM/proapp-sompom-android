package com.sompom.tookitup.listener;

import com.leocardz.link.preview.library.LinkPreviewCallback;

/**
 * Created by nuonveyo on 11/2/18.
 */

public abstract class AbsLinkPreviewCallback implements LinkPreviewCallback {
    public abstract void onRemoveListener();

    public abstract void onStartDownloadLinkPreview(String textLink);

    public abstract void onShowGifLinkFormKeyboard(String link);

    public abstract void onSetEditText(String text);
}
