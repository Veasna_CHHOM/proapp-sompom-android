package com.sompom.tookitup.listener;

/**
 * Created by He Rotha on 11/6/17.
 */

public interface OnCompleteListener<T> {
    void onComplete(T result);
}
