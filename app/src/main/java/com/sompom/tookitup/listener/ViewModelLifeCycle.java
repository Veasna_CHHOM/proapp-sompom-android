package com.sompom.tookitup.listener;

import com.sompom.tookitup.model.Media;

/**
 * Created by He Rotha on 8/29/17.
 */

public interface ViewModelLifeCycle {
    void onPause();

    void onResume();

    void onResumePlaybackMedia(Media media);
}
