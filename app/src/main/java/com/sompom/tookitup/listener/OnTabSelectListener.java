package com.sompom.tookitup.listener;

/**
 * Created by He Rotha on 2/25/19.
 */
public interface OnTabSelectListener {
    void onTabSelected(boolean isSelected);
}
