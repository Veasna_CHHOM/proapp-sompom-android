package com.sompom.tookitup.newui.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.newadapter.UserStoreDialogAdapter;
import com.sompom.tookitup.databinding.DialogUserStoreBottomSheetBinding;
import com.sompom.tookitup.intent.newintent.ProductDetailIntent;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.ProductStoreDataManager;
import com.sompom.tookitup.services.datamanager.StoreDataManager;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.newviewmodel.DialogUserStoreBottomSheetViewModel;
import com.sompom.tookitup.widget.LoaderMoreLayoutManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by nuonveyo on 10/24/18.
 */

public class UserStoreBottomSheetDialog extends AbsBindingBottomSheetDialog<DialogUserStoreBottomSheetBinding>
        implements OnCallbackListListener<List<Product>> {
    @Inject
    public ApiService mApiService;
    private LoaderMoreLayoutManager mLoaderMoreLayoutManager;
    private DialogUserStoreBottomSheetViewModel mViewModel;
    private UserStoreDialogAdapter mUserStoreDialogAdapter;

    public static UserStoreBottomSheetDialog newInstance(User user) {

        Bundle args = new Bundle();
        args.putParcelable(SharedPrefUtils.DATA, user);

        UserStoreBottomSheetDialog fragment = new UserStoreBottomSheetDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0f;
        windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(windowParams);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_user_store_bottom_sheet;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);

        User user = getArguments().getParcelable(SharedPrefUtils.DATA);
        StoreDataManager dataManager = new StoreDataManager(getActivity(), mApiService);
        ProductStoreDataManager storeDataManager = new ProductStoreDataManager(getActivity(), mApiService);
        mViewModel = new DialogUserStoreBottomSheetViewModel(dataManager,
                storeDataManager,
                user,
                this);
        setVariable(BR.viewModel, mViewModel);
    }

    private void iniRecyclerView(List<Product> productList, boolean canLoadMore) {
        mLoaderMoreLayoutManager = new LoaderMoreLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mLoaderMoreLayoutManager.setOnLoadMoreListener(() -> mViewModel.loadMore(new OnCallbackListListener<List<Product>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mUserStoreDialogAdapter.setCanLoadMore(false);
                mUserStoreDialogAdapter.notifyDataSetChanged();
                mLoaderMoreLayoutManager.loadingFinished();
                mLoaderMoreLayoutManager.setOnLoadMoreListener(null);
            }

            @Override
            public void onComplete(List<Product> result, boolean canLoadMore) {
                mUserStoreDialogAdapter.addLoadMoreData(new ArrayList<>(result));
                mUserStoreDialogAdapter.setCanLoadMore(canLoadMore);
                mLoaderMoreLayoutManager.loadingFinished();
            }
        }));
        getBinding().recyclerView.setLayoutManager(mLoaderMoreLayoutManager);
        mUserStoreDialogAdapter = new UserStoreDialogAdapter(productList, result -> {
            if (!TextUtils.isEmpty(result.getId())) {
                startActivity(new ProductDetailIntent(getActivity(),
                        result,
                        false,
                        false));
            }
        });
        mUserStoreDialogAdapter.setCanLoadMore(canLoadMore);
        getBinding().recyclerView.setAdapter(mUserStoreDialogAdapter);
    }

    @Override
    public void onComplete(List<Product> result, boolean canLoadMore) {
        iniRecyclerView(result, canLoadMore);
    }

    @Override
    public void onFail(ErrorThrowable ex) {

    }
}
