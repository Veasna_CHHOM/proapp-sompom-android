package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.desmond.squarecamera.model.AppTheme;
import com.desmond.squarecamera.utils.ThemeManager;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.FragmentSelectAddressBinding;
import com.sompom.tookitup.helper.AnimationHelper;
import com.sompom.tookitup.model.SearchAddressResult;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.SelectAddressFragmentViewModel;

/**
 * Created by nuonveyo on 5/10/18.
 */

public class SelectAddressFragment extends com.sompom.tookitup.newui.fragment.AbsBindingFragment<FragmentSelectAddressBinding>
        implements OnMapReadyCallback {
    public static final String TAG = SelectAddressFragment.class.getName();

    private SearchAddressResult mSearchAddressResult;

    public static SelectAddressFragment newInstance(SearchAddressResult searchAddressResult) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(SharedPrefUtils.DATA, searchAddressResult);
        SelectAddressFragment fragment = new SelectAddressFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_select_address;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            mSearchAddressResult = getArguments().getParcelable(SharedPrefUtils.DATA);
        }

        getBinding().mapView.onCreate(savedInstanceState);
        getBinding().mapView.onResume();
        getBinding().mapView.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (ThemeManager.getAppTheme(getContext()) == AppTheme.Black) {
            googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.google_map_dark));
        }
        int paddingTop = getResources().getInteger(R.integer.google_map_select_address_padding_top);
        googleMap.setPadding(0, paddingTop, 0, 0);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        googleMap.setOnCameraMoveStartedListener(i -> AnimationHelper.slideDown(getBinding().address));
        SelectAddressFragmentViewModel viewModel = new SelectAddressFragmentViewModel(getActivity(),
                googleMap,
                mSearchAddressResult,
                () -> AnimationHelper.slideUp(getBinding().address));
        setVariable(BR.viewModel, viewModel);
    }
}
