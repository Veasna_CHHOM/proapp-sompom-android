package com.sompom.tookitup.newui.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.sompom.tookitup.R;

/**
 * Created by He Rotha on 6/19/18.
 */
public class ChangeLanguageDialog extends MessageDialog {
    private OnLanguageChooseListener mOnLanguageChooseListener;
    private String[] mLanguages;
    private int mPosition;

    public void setLanguages(String[] languages) {
        mLanguages = languages;
    }

    public void setPosition(int position) {
        mPosition = position;
    }

    public void setOnLanguageChooseListener(OnLanguageChooseListener onLanguageChooseListener) {
        mOnLanguageChooseListener = onLanguageChooseListener;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setTitle(getString(R.string.change_language_title));
        setMessage(getString(R.string.change_language_title_description));
        setSingleChoiceItems(mLanguages, mPosition, (dialog, which) -> mPosition = which);
        setLeftText(getString(R.string.change_language_button_ok), dialog -> mOnLanguageChooseListener.onSelect(mPosition));
        setRightText(getString(R.string.change_language_button_cancel), null);
        super.onViewCreated(view, savedInstanceState);
    }

    public interface OnLanguageChooseListener {
        void onSelect(int position);
    }
}
