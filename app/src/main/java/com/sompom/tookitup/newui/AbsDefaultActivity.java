package com.sompom.tookitup.newui;

import android.support.v4.app.Fragment;

import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.ActivityDefaultBinding;

/**
 * Created by He Rotha on 10/20/17.
 */

public abstract class AbsDefaultActivity extends AbsBindingActivity<ActivityDefaultBinding> {
    @Override
    public int getLayoutRes() {
        return R.layout.activity_default;
    }

    public void setFragment(Fragment fragment, String tag) {
        setFragment(R.id.containerView, fragment, tag);
    }
}
