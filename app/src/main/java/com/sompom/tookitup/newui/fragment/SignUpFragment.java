package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.FragmentSignUpBinding;
import com.sompom.tookitup.injection.controller.PublicQualifier;
import com.sompom.tookitup.intent.newintent.HomeIntent;
import com.sompom.tookitup.model.request.PhoneLoginRequest;
import com.sompom.tookitup.model.result.AuthResponse;
import com.sompom.tookitup.newui.dialog.ChangePasswordDialog;
import com.sompom.tookitup.newui.dialog.ForgotPasswordDialog;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.LoginDataManager;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.utils.SnackBarUtil;
import com.sompom.tookitup.viewmodel.newviewmodel.SignUpViewModel;

import javax.inject.Inject;

/**
 * Created by He Rotha on 6/4/18.
 */
public class SignUpFragment extends AbsBindingFragment<FragmentSignUpBinding> implements SignUpViewModel.SignUpViewModelListener {

    @Inject
    @PublicQualifier
    public ApiService mApiService;
    private SignUpViewModel mViewModel;

    public static SignUpFragment newInstance(PhoneLoginRequest request) {
        Bundle args = new Bundle();
        args.putParcelable(SharedPrefUtils.PRODUCT, request);
        SignUpFragment fragment = new SignUpFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static SignUpFragment newInstance() {

        Bundle args = new Bundle();

        SignUpFragment fragment = new SignUpFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_sign_up;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);

//        if (getArguments() != null) {
//            PhoneLoginRequest phone = getArguments().getParcelable(SharedPrefUtils.PRODUCT);
//
//            LoginDataManager loginDataManager = new LoginDataManager(getActivity(), mApiService);
//            SignUpViewModel viewModel = new SignUpViewModel(getActivity(), phone, loginDataManager);
//            setVariable(BR.viewModel, viewModel);
//
//            getBinding().firstNameEditText.setOnFocusChangeListener((v, hasFocus) -> {
//                if (!hasFocus) {
//                    viewModel.setStoreNamePlaceHolder();
//                }
//            });
//        }
//
//        getBinding().firstNameEditText.post(() -> showKeyboard(getBinding().firstNameEditText));


        LoginDataManager loginDataManager = new LoginDataManager(getActivity(), mApiService);
        mViewModel = new SignUpViewModel(getActivity(), null, loginDataManager);
        mViewModel.setListener(this);
        setVariable(BR.viewModel, mViewModel);
    }

    @Override
    public void onLoginError(String message) {
        SnackBarUtil.showSnackBar(getBinding().getRoot(), message);
    }

    @Override
    public void onLoginSuccess() {
        startActivity(new HomeIntent(requireContext()));
        requireActivity().finish();
    }

    @Override
    public void onChangePasswordRequire(AuthResponse authResponse) {
        ChangePasswordDialog dialog = ChangePasswordDialog.newInstance();
        dialog.setListener(() -> {
            showSnackBar(R.string.password_change_success);
            SharedPrefUtils.addLoggedInEmail(requireContext(), authResponse.getUser().getEmail());
            mViewModel.saveUserLoggedIn(authResponse);
        });
        dialog.show(getFragmentManager());
    }

    @Override
    public void onForgotPasswordClicked() {
        ForgotPasswordDialog dialog = new ForgotPasswordDialog();
        dialog.setListener(() -> {
            ChangePasswordDialog dialog1 = new ChangePasswordDialog();
            dialog1.setListener(() -> {
                startActivity(new HomeIntent(requireContext()));
                requireActivity().finish();
            });
            dialog1.show(getFragmentManager());
        });
        dialog.show(getFragmentManager());
    }
}
