package com.sompom.tookitup.newui.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.DialogOtherUserProfileBinding;
import com.sompom.tookitup.intent.CallingIntent;
import com.sompom.tookitup.intent.ChatIntent;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.OtherUserProfileDataManager;
import com.sompom.tookitup.viewmodel.newviewmodel.OtherProfileViewModel;

import javax.inject.Inject;

public class OtherUserProfileDialog extends AbsBindingFragmentDialog<DialogOtherUserProfileBinding>
        implements OtherProfileViewModel.OnCallback {

    private static final String USER_ID = "USER_ID";

    private OtherProfileViewModel mViewModel;
    @Inject
    public ApiService mApiService;

    public static OtherUserProfileDialog newInstance(String userId) {

        Bundle args = new Bundle();
        args.putString(USER_ID, userId);

        OtherUserProfileDialog fragment = new OtherUserProfileDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_other_user_profile;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        if (getArguments() != null) {
            String userId = getArguments().getString(USER_ID);
            mViewModel = new OtherProfileViewModel(new OtherUserProfileDataManager(requireContext(),
                    mApiService),
                    userId,
                    this);
            setVariable(BR.viewModel, mViewModel);
        }

    }

    @Override
    public void onCallClicked(User user) {
        requireActivity().startActivity(new CallingIntent(requireContext(), user, null));
    }

    @Override
    public void onMessageClicked(User user) {
        requireActivity().startActivity(new ChatIntent(requireContext(), user));
    }

    @Override
    public void onCloseClicked() {
        dismiss();
    }

    @Override
    public void onRootClicked() {
        if (isCancelable()) {
            dismiss();
        }
    }
}
