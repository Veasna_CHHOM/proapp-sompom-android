package com.sompom.tookitup.newui;

import android.os.Bundle;

import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.ActivityDefaultBinding;
import com.sompom.tookitup.intent.newintent.DetailHistoryProductConversationIntent;
import com.sompom.tookitup.newui.fragment.DetailHistoryProductConversationFragment;

public class DetailHistoryProductConversationActivity extends AbsSoundControllerActivity<ActivityDefaultBinding> {

    @Override
    public int getLayoutRes() {
        return R.layout.activity_default;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DetailHistoryProductConversationIntent intent = new DetailHistoryProductConversationIntent(getIntent());
        setFragment(R.id.containerView,
                DetailHistoryProductConversationFragment.newInstance(intent.getProduct()),
                DetailHistoryProductConversationFragment.TAG);
    }
}
