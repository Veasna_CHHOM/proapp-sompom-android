package com.sompom.tookitup.newui.dialog;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.PartialForgotLayoutBinding;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.ForgotPasswordDataManager;
import com.sompom.tookitup.utils.SnackBarUtil;
import com.sompom.tookitup.viewmodel.newviewmodel.ForgotPasswordViewModel;

import javax.inject.Inject;

public class ForgotPasswordDialog extends MessageDialog implements ForgotPasswordViewModel.ForgotPasswordViewModelListener {

    private ForgotPasswordViewModel mViewModel;
    @Inject
    public ApiService mApiService;
    private View mRoot;
    private ForgotPasswordDialogListener mListener;

    public static ForgotPasswordDialog newInstance() {

        Bundle args = new Bundle();

        ForgotPasswordDialog fragment = new ForgotPasswordDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setCancelable(false);
        mRoot = view;
        getControllerComponent().inject(this);
        final LayoutInflater inflater = LayoutInflater.from(getContext());
        PartialForgotLayoutBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.partial_forgot_layout,
                null,
                true);

        setDismissWhenClickOnButtonLeft(false);
        mViewModel = new ForgotPasswordViewModel(requireContext(),
                new ForgotPasswordDataManager(requireContext(), mApiService),
                this);

        binding.setVariable(BR.viewModel, mViewModel);

        setTitle(getString(R.string.forgot_password));
        setMessage(getString(R.string.set_your_new_password));
        setLeftText(getString(R.string.search_address_button_confirm), dialog -> {
            if (mViewModel.isAllFieldValid()) {
                mViewModel.requestForgotPassword();
            }
        });
        setRightText(getString(R.string.change_language_button_cancel), null);
        getBinding().view.addView(binding.getRoot());
        super.onViewCreated(view, savedInstanceState);
    }

    public void setListener(ForgotPasswordDialogListener listener) {
        mListener = listener;
    }

    @Override
    public void onFailed(String error) {
        SnackBarUtil.showSnackBar(mRoot, error);
    }

    @Override
    public void onSuccess() {
        dismiss();
        if (mListener != null) {
            mListener.onSuccess();
        }
    }

    public interface ForgotPasswordDialogListener {
        void onSuccess();
    }
}
