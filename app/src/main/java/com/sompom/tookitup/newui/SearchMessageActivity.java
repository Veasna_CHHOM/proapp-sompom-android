package com.sompom.tookitup.newui;

import android.os.Bundle;

import com.sompom.tookitup.newui.fragment.SearchGeneralContainerFragment;

public class SearchMessageActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(SearchGeneralContainerFragment.newInstance(), SearchGeneralContainerFragment.TAG);
    }
}
