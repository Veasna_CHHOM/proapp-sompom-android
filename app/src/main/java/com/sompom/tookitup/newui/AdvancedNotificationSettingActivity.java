package com.sompom.tookitup.newui;

import android.os.Bundle;

import com.sompom.tookitup.newui.fragment.AdvancedNotificationSettingFragment;

public class AdvancedNotificationSettingActivity extends AbsDefaultActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(AdvancedNotificationSettingFragment.newInstance(), AdvancedNotificationSettingFragment.TAG);
    }

}
