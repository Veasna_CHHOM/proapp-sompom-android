package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.newadapter.SuggestionAdapter;
import com.sompom.tookitup.databinding.FragmentTabSuggestionBinding;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnUserClickListener;
import com.sompom.tookitup.model.emun.SuggestionTab;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.SuggestionDataManager;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.newviewmodel.TabSuggestionFragmentViewModel;
import com.sompom.tookitup.widget.LoadMoreGridLayoutManager;

import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by nuonveyo on 7/20/18.
 */

public class TabSuggestionFragment extends AbsBindingFragment<FragmentTabSuggestionBinding>
        implements OnCallbackListListener<List<User>> {
    @Inject
    public ApiService mApiService;
    private SuggestionTab mSuggestionTab;
    private TabSuggestionFragmentViewModel mViewModel;
    private LoadMoreGridLayoutManager mLoadMoreLayoutManager;
    private SuggestionAdapter mSuggestionAdapter;

    public static TabSuggestionFragment newInstance(SuggestionTab suggestionTab) {
        Timber.e(suggestionTab.toString() + "  " + suggestionTab.getQueryType());

        Bundle args = new Bundle();
        args.putInt(SharedPrefUtils.ID, suggestionTab.getId());
        TabSuggestionFragment fragment = new TabSuggestionFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_tab_suggestion;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        initLayoutManager();

        if (getArguments() != null) {
            int id = getArguments().getInt(SharedPrefUtils.ID);
            mSuggestionTab = SuggestionTab.getSuggestionTab(id);
        }

        SuggestionDataManager dataManager = new SuggestionDataManager(getActivity(), mApiService, mSuggestionTab);
        mViewModel = new TabSuggestionFragmentViewModel(dataManager, this);
        setVariable(BR.viewModel, mViewModel);
    }

    @Override
    public void onComplete(List<User> result, boolean canLoadMore) {
        mSuggestionAdapter = new SuggestionAdapter(result, mSuggestionTab, new OnUserClickListener() {
            @Override
            public void onUserClick(User user) {
                if (mViewModel != null) {
                    mViewModel.follow(user);
                }
            }
        });
        mSuggestionAdapter.setCanLoadMore(canLoadMore);
        getBinding().recyclerView.setAdapter(mSuggestionAdapter);

    }

    @Override
    public void onFail(ErrorThrowable ex) {
        //Stub
    }

    private void initLayoutManager() {
        int columnCount = getContext().getResources().getInteger(R.integer.number_of_column);
        mLoadMoreLayoutManager = new LoadMoreGridLayoutManager(getActivity(), columnCount);
        mLoadMoreLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == mSuggestionAdapter.getItemCount() - 1 && mSuggestionAdapter.canLoadMore()) {
                    return columnCount;
                } else {
                    return 1;
                }
            }
        });
        getBinding().recyclerView.setLayoutManager(mLoadMoreLayoutManager);
    }

}
