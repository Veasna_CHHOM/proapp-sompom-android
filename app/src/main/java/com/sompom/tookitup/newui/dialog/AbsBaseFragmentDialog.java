package com.sompom.tookitup.newui.dialog;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.annotation.UiThread;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.sompom.tookitup.R;
import com.sompom.tookitup.TookitupApplication;
import com.sompom.tookitup.databinding.DialogBaseBinding;
import com.sompom.tookitup.injection.controller.ControllerComponent;
import com.sompom.tookitup.injection.controller.ControllerModule;
import com.sompom.tookitup.utils.KeyboardUtil;
import com.sompom.tookitup.utils.SnackBarUtil;

/**
 * Created by He Rotha on 10/25/17.
 */

public abstract class AbsBaseFragmentDialog extends DialogFragment {
    public static final String TAG = AbsBaseFragmentDialog.class.getName();
    private ControllerComponent mControllerComponent;

    @UiThread
    public ControllerComponent getControllerComponent() {
        if (mControllerComponent == null) {
            mControllerComponent = ((TookitupApplication) getActivity().getApplication())
                    .getApplicationComponent()
                    .newControllerComponent(new ControllerModule(getActivity()));
        }
        return mControllerComponent;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getDialog().getWindow() != null) {
            getDialog()
                    .getWindow()
                    .setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                            WindowManager.LayoutParams.MATCH_PARENT);

        }
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogBaseBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_base, container, false);
        binding.layoutContainer.removeAllViews();
        binding.layoutContainer.addView(onCreateView(inflater, container));
        binding.root.setOnClickListener(v -> {
            if (isCancelable()) {
                dismiss();
            }
        });
        return binding.getRoot();
    }

    public abstract View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container);

    protected void showKeyboard(EditText editText) {
        editText.setFocusable(true);
        editText.setClickable(true);
        editText.requestFocus();
        KeyboardUtil.showKeyboard(getActivity(), editText);
    }

    public void showSnackBar(String text) {
        if (getActivity() != null) {
            View view = getActivity().findViewById(android.R.id.content);
            if (view != null) {
                SnackBarUtil.showSnackBar(view, text);
            }
        }
    }

    public void showSnackBar(@StringRes int message) {
        SnackBarUtil.showSnackBar(getView(), message);
    }


    public void setFragment(@IdRes int id, Fragment fragment, String tag) {
        if (!isVisible() && !isFragmentAlreadyReplaced(tag)) {
            getChildFragmentManager()
                    .beginTransaction()
                    .replace(id, fragment, tag)
                    .commit();
        }
    }

    private boolean isFragmentAlreadyReplaced(String tag) {
        Fragment fragment = getChildFragmentManager().findFragmentByTag(tag);
        return fragment != null && fragment.isVisible();
    }

    public Fragment getFragment(String tag) {
        return getChildFragmentManager().findFragmentByTag(tag);
    }

    public void showFragmentWithAnimation(FragmentTransaction fragmentTransaction,
                                          Fragment showFragment,
                                          Fragment hideFragment) {
        if (hideFragment == null || showFragment == null || hideFragment == showFragment) {
            return;
        }
        fragmentTransaction
                .hide(hideFragment)
                .show(showFragment)
                .commit();
        //make sure fragment know fragment is paused, or resumed
        hideFragment.onPause();
        showFragment.onResume();
    }
}
