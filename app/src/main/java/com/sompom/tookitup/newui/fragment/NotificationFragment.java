package com.sompom.tookitup.newui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.newadapter.NotificationAdapter;
import com.sompom.tookitup.database.NotificationDb;
import com.sompom.tookitup.databinding.FragmentHomeNotificationBinding;
import com.sompom.tookitup.helper.NavigateSellerStoreHelper;
import com.sompom.tookitup.injection.notificationserialize.NotificationSerializeQualifier;
import com.sompom.tookitup.intent.newintent.SellerStoreIntent;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.model.notification.NotificationAdaptive;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.NotificationDataManager;
import com.sompom.tookitup.viewmodel.newviewmodel.HomeNotificationFragmentViewModel;
import com.sompom.tookitup.widget.LoaderMoreLayoutManager;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by nuonveyo on 7/4/18.
 */

public class NotificationFragment extends AbsBindingFragment<FragmentHomeNotificationBinding> {
    @NotificationSerializeQualifier
    @Inject
    public ApiService mApiService;
    private HomeNotificationFragmentViewModel mViewModel;
    private LoaderMoreLayoutManager mLoaderMoreLayoutManager;
    private NotificationAdapter mNotificationAdapter;
    private boolean mIsShow;
    private OnClickListener mOnUpdateBadgeValueListener;

    public static NotificationFragment newInstance() {
        return new NotificationFragment();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_home_notification;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        getBinding().layoutToolbar.toolbarTitle.setText(getString(R.string.notification));
        setToolbar(getBinding().layoutToolbar.toolbar, true);
        initViewModel();
    }

    private void initViewModel() {
        NotificationDataManager dataManager = new NotificationDataManager(getActivity(), mApiService);
        mViewModel = new HomeNotificationFragmentViewModel(dataManager,
                this::iniRecyclerView);
        setVariable(BR.viewModel, mViewModel);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            requireActivity().finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public void setOnUpdateBadgeValueListener(OnClickListener onUpdateBadgeValueListener) {
        mOnUpdateBadgeValueListener = onUpdateBadgeValueListener;
    }

    private void iniRecyclerView(List<NotificationAdaptive> baseNotificationModels, boolean canLoadMore) {
        mLoaderMoreLayoutManager = new LoaderMoreLayoutManager(getActivity());
        mLoaderMoreLayoutManager.setOnLoadMoreListener(() -> {
            getBinding().recyclerView.stopScroll();
            mViewModel.loadMore(new OnCallbackListListener<List<NotificationAdaptive>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    mNotificationAdapter.setCanLoadMore(false);
                    mNotificationAdapter.notifyDataSetChanged();
                    mLoaderMoreLayoutManager.loadingFinished();
                    mLoaderMoreLayoutManager.setOnLoadMoreListener(null);
                }

                @Override
                public void onComplete(List<NotificationAdaptive> result, boolean canLoadMore) {
                    mNotificationAdapter.addLoadMoreData(result);
                    mNotificationAdapter.setCanLoadMore(canLoadMore);
                    mLoaderMoreLayoutManager.loadingFinished();
                }
            });
        });
        getBinding().recyclerView.setLayoutManager(mLoaderMoreLayoutManager);

        mNotificationAdapter = new NotificationAdapter(baseNotificationModels, (data, position) -> {
            //TODO: call api update notification after server done and handle type
            if (!data.isRead()) {
                data.setRead(true);
                mNotificationAdapter.updateItem(position, data);
                NotificationDb.update(getContext(), data);
                if (mOnUpdateBadgeValueListener != null) {
                    mOnUpdateBadgeValueListener.onClick();
                }
            }


            /*
                Manage to open only other profile popup
                And need to check the notification item is only user notification and normally
                we should have user id.
             */
//            startActivity(data.getIntent(getActivity())); //Old code
            Intent intent = data.getIntent(getActivity());
            if (intent instanceof SellerStoreIntent) {
                NavigateSellerStoreHelper.openSellerStore((AppCompatActivity) requireActivity(),
                        ((SellerStoreIntent) intent).getUserId());
            }
        });
        mNotificationAdapter.setCanLoadMore(canLoadMore);
        getBinding().recyclerView.setAdapter(mNotificationAdapter);
    }
}
