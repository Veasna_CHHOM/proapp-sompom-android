package com.sompom.tookitup.newui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.inputmethod.InputMethodManager;

import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.ActivityDefaultBinding;
import com.sompom.tookitup.intent.newintent.FilterCategoryIntent;
import com.sompom.tookitup.newui.fragment.FilterCategoryFragment;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.ArrayList;

public class FilterCategoryActivity extends AbsBindingActivity<ActivityDefaultBinding> {

    @Override
    public int getLayoutRes() {
        return R.layout.activity_default;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FilterCategoryIntent intent = new FilterCategoryIntent(getIntent());
        setFragment(R.id.containerView,
                FilterCategoryFragment.newInstance(intent.getCategoryList(), intent.isEmpty()),
                FilterCategoryFragment.TAG);
    }

    @Override
    public void finish() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null)
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }

        Intent resultIntent = new Intent();
        FilterCategoryFragment filterCategoryFragment = (FilterCategoryFragment) getFragment(FilterCategoryFragment.TAG);
        if (filterCategoryFragment != null) {
            resultIntent.putParcelableArrayListExtra(SharedPrefUtils.CATEGORY,
                    (ArrayList<? extends Parcelable>) filterCategoryFragment.getSelectedCategory());
            setResult(Activity.RESULT_OK, resultIntent);
        }
        super.finish();
    }
}
