package com.sompom.tookitup.newui;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.sompom.tookitup.newui.fragment.NotificationFragment;

/**
 * Created by Chhom Veasna on 6/27/2019.
 */
public class NotificationActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(NotificationFragment.newInstance(), NotificationFragment.class.getName());
    }
}
