package com.sompom.tookitup.newui.dialog;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.DialogEditProductBinding;
import com.sompom.tookitup.listener.OnEditDialogClickListener;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.newviewmodel.EditProductDialogViewModel;

/**
 * Created by He Rotha on 6/13/18.
 */
public class EditProductDialog extends BottomSheetDialogFragment {
    public static final String TAG = EditProductDialog.class.getName();
    private OnEditDialogClickListener mListener;
    private DialogEditProductBinding mBinding;

    public static EditProductDialog newInstance(Product product, int productStatus) {
        Bundle args = new Bundle();
        args.putParcelable(SharedPrefUtils.PRODUCT, product);
        args.putInt(SharedPrefUtils.ID, productStatus);
        EditProductDialog fragment = new EditProductDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public void setListener(OnEditDialogClickListener listener) {
        mListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_edit_product, container, false);
        return getBinding().getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Product product = getArguments().getParcelable(SharedPrefUtils.PRODUCT);
        final int productStatus = getArguments().getInt(SharedPrefUtils.ID);
        final EditProductDialogViewModel viewModel = new EditProductDialogViewModel(getContext(),
                product,
                productStatus,
                mListener);
        getBinding().setViewModel(viewModel);
    }

    public DialogEditProductBinding getBinding() {
        return mBinding;
    }
}
