package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.desmond.squarecamera.model.AppTheme;
import com.desmond.squarecamera.utils.ThemeManager;
import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.FragmentSellerStore2Binding;
import com.sompom.tookitup.helper.ScrimColorAppbarListener;
import com.sompom.tookitup.intent.ChatIntent;
import com.sompom.tookitup.model.emun.FollowItemType;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.dialog.ListFollowDialog;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.StoreDataManager;
import com.sompom.tookitup.utils.FormatNumber;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.newviewmodel.SellerStoreFragmentViewModel;

import javax.inject.Inject;

/**
 * Created by nuonveyo on 6/4/18.
 */

public class SellerStoreFragment extends AbsBindingFragment<FragmentSellerStore2Binding> {

    @Inject
    public ApiService mApiService;
    private SellerStoreFragmentViewModel mUserViewModel;
    private String mUserId;
    private boolean mIsMe;
    private User mUser;
    private String mMyUserId;

    public static SellerStoreFragment newInstance(String userId) {
        Bundle args = new Bundle();
        args.putString(SharedPrefUtils.PRODUCT, userId);
        SellerStoreFragment fragment = new SellerStoreFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_seller_store2;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        setToolbar(getBinding().layoutToolbar.toolbar, false);

        if (getArguments() != null) {
            mUserId = getArguments().getString(SharedPrefUtils.PRODUCT);
            mMyUserId = SharedPrefUtils.getUserId(getActivity());
            mIsMe = SharedPrefUtils.checkIsMe(getActivity(), mUserId);
        }

        StoreDataManager manager = new StoreDataManager(getActivity(), mApiService);
        mUserViewModel = new SellerStoreFragmentViewModel(manager,
                mUserId,
                mIsMe,
                new MyStoreListener());
        setVariable(BR.viewModel, mUserViewModel);

        if (ThemeManager.getAppTheme(getContext()) == AppTheme.White) {
            getBinding().appbarlayout.addOnOffsetChangedListener(new ScrimColorAppbarListener(getBinding().collapse) {

                @Override
                public void onStateChange(State state) {
                    if (!mUserViewModel.mIsMe.get()) {
                        if (state == State.Collapse) {
                            getBinding().layoutToolbar.imageViewProfile.animate().alpha(1f).start();
                        } else {
                            getBinding().layoutToolbar.imageViewProfile.animate().alpha(0f).start();
                        }
                    }
                }

                @Override
                public void onColorChange(int color) {
                    getBinding().layoutToolbar.textviewUserStore.setTextColor(color);
                    getBinding().layoutToolbar.backButton.setTextColor(color);
                }
            });
        }

    }

    public User getUser() {
        return mUser;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!TextUtils.equals(mMyUserId, SharedPrefUtils.getUserId(getActivity()))) {
            mUserViewModel.getData();
        }
    }

    public boolean onBackPress() {
        //cos 0 position is wall street fragment
        if (getBinding().viewpager.getCurrentItem() == 0) {
            return getBinding().tabs.isShouldScrollToTop();
        } else {
            getBinding().viewpager.setCurrentItem(0, true);
            return true;
        }
    }

    public void addNewProduct() {
        getBinding().tabs.addNewProduct();
    }

    public void deleteProduct() {
        getBinding().tabs.deleteProduct();
    }

    private class MyStoreListener implements SellerStoreFragmentViewModel.OnSellerStoreFrListener {
        @Override
        public void onChatClick(User user) {
            ChatIntent chatIntent = new ChatIntent(getActivity(), user);
            startActivity(chatIntent);
        }

        @Override
        public void onGetUserData(User user) {
            mUser = user;
            if (isAdded()) {
                initTabItem();
            }
        }

        @Override
        public void onShowListFollowUser(FollowItemType type) {
            ListFollowDialog dialog = ListFollowDialog.newInstance(mUser, type);
            dialog.setOnCallbackListener(isFollowing -> {
                int followingCount = Integer.parseInt(mUserViewModel.mFollowing.get());
                if (isFollowing) {
                    followingCount = followingCount + 1;
                } else {
                    followingCount = followingCount - 1;
                }
                mUserViewModel.mFollowing.set(FormatNumber.format(followingCount));
            });
            dialog.show(getChildFragmentManager(), ListFollowDialog.TAG);
        }

        private void initTabItem() {
            getBinding().tabs.setUpViewPager(getChildFragmentManager(), getBinding().viewpager, mUser);
        }
    }
}
