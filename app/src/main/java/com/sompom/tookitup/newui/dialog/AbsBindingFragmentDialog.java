package com.sompom.tookitup.newui.dialog;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sompom.tookitup.viewmodel.AbsBaseViewModel;

/**
 * Created by He Rotha on 10/25/17.
 */

public abstract class AbsBindingFragmentDialog<T extends ViewDataBinding> extends AbsBaseFragmentDialog {
    private T mBinding;
    private AbsBaseViewModel mViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container) {
        mBinding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false);
        return getBinding().getRoot();
    }

    @LayoutRes
    public abstract int getLayoutRes();

    public void setVariable(int id, Object value) {
        if (value instanceof AbsBaseViewModel) {
            mViewModel = (AbsBaseViewModel) value;
            mViewModel.setAbsBaseFragmentDialog(this);
        }
        getBinding().setVariable(id, value);
    }

    public T getBinding() {
        return mBinding;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mViewModel != null) {
            mViewModel.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mViewModel != null) {
            mViewModel.onPause();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mViewModel != null) {
            mViewModel.onDestroy();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mViewModel != null) {
            mViewModel.onCreate();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mViewModel != null) {
            mViewModel.onActivityResult(requestCode, resultCode, data);
        }
    }
}
