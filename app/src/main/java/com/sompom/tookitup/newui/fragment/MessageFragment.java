package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.FragmentMessageBinding;
import com.sompom.tookitup.listener.OnHomeMenuClick;
import com.sompom.tookitup.model.emun.SegmentedControlItem;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.newviewmodel.MessageFragmentViewModel;

/**
 * Created by nuonveyo on 6/25/18.
 */

public class MessageFragment extends AbsBindingFragment<FragmentMessageBinding>
        implements OnHomeMenuClick {
    private AllMessageFragment mAllMessageFragment;
    private AllMessageFragment mMessageFragment;
    private AllMessageFragment mBuyingFragment;
    private AllMessageFragment mSellingFragment;
    private Fragment mCurrentFragment;
    private boolean mIsShow;

    public static MessageFragment newInstance(boolean isLoadData) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(SharedPrefUtils.STATUS, isLoadData);
        MessageFragment fragment = new MessageFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_message;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            boolean isLoadData = getArguments().getBoolean(SharedPrefUtils.STATUS, false);
            if (isLoadData) {
                onTabMenuClick();
            }
        }
    }

    private Fragment getAllMessageFragment() {
        if (mAllMessageFragment == null) {
            mAllMessageFragment = AllMessageFragment.newInstance(SegmentedControlItem.All);
        }
        return mAllMessageFragment;
    }

    private Fragment getMessageFragment() {
        if (mMessageFragment == null) {
            mMessageFragment = AllMessageFragment.newInstance(SegmentedControlItem.Message);
        }
        return mMessageFragment;
    }

    private Fragment getBuyingFragment() {
        if (mBuyingFragment == null) {
            mBuyingFragment = AllMessageFragment.newInstance(SegmentedControlItem.Buying);
        }
        return mBuyingFragment;
    }

//    Hide this feature for now
//    private Fragment getSellingFragment() {
//        if (mSellingFragment == null) {
//            mSellingFragment = AllMessageFragment.newInstance(SegmentedControlItem.Selling);
//        }
//        return mSellingFragment;
//    }

    @Override
    public void onTabMenuClick() {
        if (mIsShow || !isAdded()) {
            return;
        }
        mIsShow = true;

        initFragment();
        MessageFragmentViewModel viewModel = new MessageFragmentViewModel(getActivity(), item -> {
            if (item.getId() == SegmentedControlItem.All.getId()) {
                showFragment(getAllMessageFragment(), mCurrentFragment);
                mCurrentFragment = getAllMessageFragment();

            } else if (item.getId() == SegmentedControlItem.Message.getId()) {
                showFragment(getMessageFragment(), mCurrentFragment);
                mCurrentFragment = getMessageFragment();

            } else if (item.getId() == SegmentedControlItem.Buying.getId()) {
                showFragment(getBuyingFragment(), mCurrentFragment);
                mCurrentFragment = getBuyingFragment();

            }

//            Hide this feature for now
//
//            else if (item.getId() == SegmentedControlItem.Selling.getId()) {
//                showFragment(getSellingFragment(), mCurrentFragment);
//                mCurrentFragment = getSellingFragment();
//            }

            if (mCurrentFragment instanceof OnHomeMenuClick) {
                ((OnHomeMenuClick) mCurrentFragment).onTabMenuClick();
            }
        });
        setVariable(BR.viewModel, viewModel);
    }

    private void initFragment() {
        mCurrentFragment = getAllMessageFragment();
        getChildFragmentManager().beginTransaction()
                .add(R.id.containerLayout, getMessageFragment(), AllMessageFragment.TAG)
                .hide(getMessageFragment())
                .add(R.id.containerLayout, getBuyingFragment(), AllMessageFragment.TAG)
                .hide(getBuyingFragment())
//                Hide this feature for now
//                .add(R.id.containerLayout, getSellingFragment(), AllMessageFragment.TAG)
//                .hide(getSellingFragment())
                .add(R.id.containerLayout, mCurrentFragment, AllMessageFragment.TAG)
                .commit();
    }
}
