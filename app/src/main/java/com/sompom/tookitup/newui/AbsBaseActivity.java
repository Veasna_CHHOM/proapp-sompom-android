package com.sompom.tookitup.newui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.v4.app.Fragment;

import com.desmond.squarecamera.model.AppTheme;
import com.desmond.squarecamera.utils.ThemeManager;
import com.sompom.baseactivity.ResultCallback;
import com.sompom.baseactivity.ResultPermissionActivity;
import com.sompom.tookitup.TookitupApplication;
import com.sompom.tookitup.broadcast.NetworkBroadcastReceiver;
import com.sompom.tookitup.broadcast.upload.FloatingVideoService;
import com.sompom.tookitup.chat.AbsChatBinder;
import com.sompom.tookitup.chat.call.CallingService;
import com.sompom.tookitup.chat.listener.ServiceStateListener;
import com.sompom.tookitup.chat.service.SocketService;
import com.sompom.tookitup.helper.LocaleManager;
import com.sompom.tookitup.helper.LoginActivityResultCallback;
import com.sompom.tookitup.helper.SingleRequestLocation;
import com.sompom.tookitup.injection.controller.ControllerComponent;
import com.sompom.tookitup.injection.controller.ControllerModule;
import com.sompom.tookitup.intent.newintent.LoginIntent;
import com.sompom.tookitup.intent.newintent.TimelineDetailIntent;
import com.sompom.tookitup.listener.OnCompleteListener;
import com.sompom.tookitup.listener.OnFloatingVideoRemoveListener;
import com.sompom.tookitup.model.LifeStream;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public abstract class AbsBaseActivity extends ResultPermissionActivity implements ServiceConnection {
    private final List<OnServiceListener> mOnServiceListener = new ArrayList<>();
    private AbsChatBinder mChatBinder;
    private SingleRequestLocation mSingleRequestLocation;
    private ControllerComponent mControllerComponent;
    private FloatingVideoService.FloatingVideoBinder mBinder;
    private OnFloatingVideoRemoveListener mOnFloatingVideoRemoveListener;
    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private final ServiceConnection mFloatingVideoServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to FloatingVideoService, cast the IBinder and get FloatingVideoService instance
            mBinder = (FloatingVideoService.FloatingVideoBinder) service;
            mBinder.setListener(new FloatingVideoService.OnVideoClickListener() {
                @Override
                public void onVideoClick(int position, LifeStream lifeStream) {
                    TimelineDetailIntent intent = new TimelineDetailIntent(AbsBaseActivity.this,
                            lifeStream,
                            position);
                    startActivity(intent);
                }

                @Override
                public void onFloatingVideoPlayingFinish() {
                    if (mOnFloatingVideoRemoveListener != null) {
                        mOnFloatingVideoRemoveListener.onRemove();
                    }
                }
            });
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            // default implementation ignored
        }
    };
    private List<NetworkBroadcastReceiver.NetworkListener> mNetworkListeners;
    private NetworkBroadcastReceiver mNetworkBroadcastReceiver;
    private NetworkBroadcastReceiver.NetworkState mNetworkState = NetworkBroadcastReceiver.NetworkState.Nothing;
    private String mCurrentLocalize;
    private AppTheme mCurrentAppTheme;

    public FloatingVideoService.FloatingVideoBinder getFloatingVideo() {
        return mBinder;
    }

    public void setOnFloatingVideoRemoveListener(OnFloatingVideoRemoveListener onFloatingVideoRemoveListener) {
        mOnFloatingVideoRemoveListener = onFloatingVideoRemoveListener;
    }

    public AbsBaseActivity getThis() {
        return this;
    }

    public void setFragment(@IdRes int id, Fragment fragment, String tag) {
        if (!isFinishing() && !isFragmentAlreadyReplaced(tag)) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(id, fragment, tag)
                    .commitAllowingStateLoss();
        }
    }

    private boolean isFragmentAlreadyReplaced(String tag) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        return fragment != null && fragment.isVisible();
    }

    public Fragment getFragment(String tag) {
        return getSupportFragmentManager().findFragmentByTag(tag);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mCurrentLocalize.equalsIgnoreCase(SharedPrefUtils.getLanguage(this))
                || mCurrentAppTheme != ThemeManager.getAppTheme(this)) {
            recreate();
        }
    }

    public void requestLocation(SingleRequestLocation.Callback callback) {
        if (mSingleRequestLocation == null) {
            mSingleRequestLocation = new SingleRequestLocation(this);
        }
        mSingleRequestLocation.execute(callback);
    }

    public void forceRequestLocation(SingleRequestLocation.Callback callback) {
        if (mSingleRequestLocation == null) {
            mSingleRequestLocation = new SingleRequestLocation(this);
        }
        mSingleRequestLocation.forceExecute(callback);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mSingleRequestLocation != null) {
            mSingleRequestLocation.onActivityResult(requestCode, resultCode, data);
        }

        /*
          Pass {@link #onActivityResult} to every activity's fragment which are
          added and visible.
         */
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment != null && fragment.isAdded() && fragment.isVisible()) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (mSingleRequestLocation != null) {
            mSingleRequestLocation.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        /*
          Pass {@link #onActivityResult} to every activity's fragment which are
          added and visible.
         */
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment != null && fragment.isAdded() && fragment.isVisible()) {
                fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCurrentAppTheme = ThemeManager.getAppTheme(this);
        mCurrentLocalize = SharedPrefUtils.getLanguage(this);

        setTheme(mCurrentAppTheme.getThemeRes());
        getApplicationContext().bindService(new Intent(this, SocketService.class), this,
                BIND_AUTO_CREATE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to FloatingVideoService
        Intent intent = new Intent(this, FloatingVideoService.class);
        bindService(intent, mFloatingVideoServiceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(mFloatingVideoServiceConnection);
    }

    @UiThread
    public ControllerComponent getControllerComponent() {
        if (mControllerComponent == null) {
            mControllerComponent = ((TookitupApplication) getApplication())
                    .getApplicationComponent()
                    .newControllerComponent(new ControllerModule(this));
        }
        return mControllerComponent;
    }

    public void startLoginWizardActivity(LoginActivityResultCallback callback) {
        if (SharedPrefUtils.isLogin(this)) {
            callback.onActivityResultSuccess(true);
        } else {
            startActivityForResult(new LoginIntent(this), new ResultCallback() {
                @Override
                public void onActivityResultSuccess(int resultCode, Intent data) {
                    if (SharedPrefUtils.isLogin(AbsBaseActivity.this)) {
                        callback.onActivityResultSuccess(false);
                    } else {
                        callback.onActivityResultFail();
                    }
                }

                @Override
                public void onActivityResultFail() {
                    callback.onActivityResultFail();
                }
            });
        }
    }

    public AbsChatBinder getSinchBinder() {
        return mChatBinder;
    }

    /**
     * Sub-Class override to perform remove floating video this is playing
     */
    public void onFloatingVideoServiceConnected() {

    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

        if (SocketService.class.getName().equals(componentName.getClassName())) {
            mChatBinder = (AbsChatBinder) iBinder;
            mChatBinder.startService(new ServiceStateListener() {
                @Override
                public void onServiceStart() {
                    Timber.e("Chat onServiceStart");
                }

                @Override
                public void onServiceFail(Exception ex) {
                    Timber.e("Chat onServiceFail %s", ex.toString());

                }
            });
            getApplicationContext().startService(new Intent(getApplicationContext(), SocketService.class));

            if (!mOnServiceListener.isEmpty()) {
                for (int i = mOnServiceListener.size() - 1; i >= 0; i--) {
                    mOnServiceListener.get(i).onServiceReady(mChatBinder);
                    mOnServiceListener.remove(i);
                }
            }
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {

    }

    public void addOnServiceListener(OnServiceListener onServiceListener) {
        if (!mOnServiceListener.contains(onServiceListener)) {
            mOnServiceListener.add(onServiceListener);
        }
        if (mChatBinder != null) {
            for (int i = mOnServiceListener.size() - 1; i >= 0; i--) {
                mOnServiceListener.get(i).onServiceReady(mChatBinder);
                mOnServiceListener.remove(i);
            }
        }
    }

    public void removeServiceListener(OnServiceListener onServiceListener) {
        mOnServiceListener.remove(onServiceListener);
    }

    public void addNetworkStateChangeListener(NetworkBroadcastReceiver.NetworkListener networkListener) {
        if (mNetworkState == NetworkBroadcastReceiver.NetworkState.Connected
                || mNetworkState == NetworkBroadcastReceiver.NetworkState.Disconnected) {
            networkListener.onNetworkState(mNetworkState);
        }

        if (mNetworkListeners == null) {
            mNetworkListeners = new ArrayList<>();
        }

        mNetworkListeners.add(networkListener);
        if (mNetworkBroadcastReceiver == null) {
            mNetworkBroadcastReceiver = new NetworkBroadcastReceiver();
            registerReceiver(mNetworkBroadcastReceiver, mNetworkBroadcastReceiver.getIntentFilter());
            mNetworkBroadcastReceiver.setListener(networkState -> {
                mNetworkState = networkState;
                for (NetworkBroadcastReceiver.NetworkListener listener : mNetworkListeners) {
                    listener.onNetworkState(mNetworkState);
                }
            });
        }
    }

    public void removeStateChangeListener(NetworkBroadcastReceiver.NetworkListener networkListener) {
        if (mNetworkListeners == null) {
            mNetworkListeners = new ArrayList<>();
        }
        mNetworkListeners.remove(networkListener);
        if (mNetworkListeners.isEmpty() && mNetworkBroadcastReceiver != null) {
            unregisterReceiver(mNetworkBroadcastReceiver);
            mNetworkBroadcastReceiver = null;
        }
    }

    public void bindCall(OnCompleteListener<CallingService.SinchBinder> listener) {
        getApplicationContext()
                .bindService(new Intent(this, CallingService.class), new ServiceConnection() {

                    @Override
                    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                        CallingService.SinchBinder binder = (CallingService.SinchBinder) iBinder;
                        binder.start(SharedPrefUtils.getUserId(getApplicationContext()));

                        if (listener != null) {
                            listener.onComplete(binder);
                        }
                    }

                    @Override
                    public void onServiceDisconnected(ComponentName componentName) {

                    }
                }, BIND_AUTO_CREATE);
    }

    public interface OnServiceListener {
        void onServiceReady(AbsChatBinder binder);
    }
}
