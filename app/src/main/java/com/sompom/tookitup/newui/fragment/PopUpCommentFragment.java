package com.sompom.tookitup.newui.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.newadapter.CommentAdapter;
import com.sompom.tookitup.databinding.LayoutPopUpCommentBinding;
import com.sompom.tookitup.helper.LoginActivityResultCallback;
import com.sompom.tookitup.injection.controller.PublicQualifier;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnCommentDialogClickListener;
import com.sompom.tookitup.listener.OnCommentItemClickListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.LoadPreviousComment;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.emun.MediaType;
import com.sompom.tookitup.model.emun.PostContextMenuItemType;
import com.sompom.tookitup.model.result.Comment;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.dialog.MessageDialog;
import com.sompom.tookitup.newui.dialog.PostContextMenuDialog;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.PopUpCommentDataManager;
import com.sompom.tookitup.utils.CopyTextUtil;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.newviewmodel.PopUpCommentLayoutViewModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;

/**
 * Created by he.rotha on 4/6/16.
 */
public class PopUpCommentFragment extends AbsBindingFragment<LayoutPopUpCommentBinding>
        implements EditText.OnEditorActionListener,
        OnCommentItemClickListener {
    @Inject
    public ApiService mApiService;
    @Inject
    @PublicQualifier
    public ApiService mApiServiceTest;
    private Activity mContext;
    private CommentAdapter mAdapter;
    private PopUpCommentLayoutViewModel mViewModel;
    private OnCommentDialogClickListener mCommentDialogClickListener;
    private boolean mIsCanLoadMore;

    public static PopUpCommentFragment newInstance(String itemID) {
        Bundle args = new Bundle();
        args.putString(SharedPrefUtils.ID, itemID);
        PopUpCommentFragment fragment = new PopUpCommentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setCommentDialogClickListener(OnCommentDialogClickListener commentDialogClickListener) {
        mCommentDialogClickListener = commentDialogClickListener;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        bindGifView();

        String itemId = "";
        if (getArguments() != null) {
            itemId = getArguments().getString(SharedPrefUtils.ID);
        }

        mContext = getActivity();
        getBinding().textviewClose.setOnClickListener(v -> {
            if (mCommentDialogClickListener != null) {
                mCommentDialogClickListener.onDismissDialog(true);
            }
        });

        PopUpCommentDataManager dataManager = new PopUpCommentDataManager(mContext, mApiService, mApiServiceTest);
        mViewModel = new PopUpCommentLayoutViewModel(dataManager,
                itemId,
                new PopUpCommentLayoutViewModel.OnCallback() {
                    @Override
                    public void onLoadCommentSuccess(List<Comment> listComment, boolean isCanLoadMore) {
                        initAdapter(listComment, isCanLoadMore);
                    }

                    @Override
                    public void onLoadCommentFail() {
                        initAdapter(new ArrayList<>(), false);
                    }

                    @Override
                    public void onSendText(Comment comment) {
                        mAdapter.addLatestComment(comment);
                        getBinding().recyclerview.scrollToPosition(mAdapter.getItemCount() - 1);
                    }

                    @Override
                    public void onPostCommentFail() {
                        mAdapter.removeInstanceComment();
                        if (mAdapter.getItemCount() <= 0) {
                            mViewModel.mIsShowEmpty.set(true);
                        }
                    }

                    @Override
                    public void onPostCommentSuccess(Comment comment) {
                        notifyItemChanged(mAdapter.getItemCount() - 1, comment);
                    }

                    @Override
                    public void onUpdateCommentSuccess(Comment comment, int position) {
                        hideSoftKeyboard();
                        mAdapter.notifyItemChanged(position, comment);
                    }
                });
        setVariable(BR.viewModel, mViewModel);
        getBinding().includeComment.messageInput.getMentionsEditText().post(() -> showKeyboard(getBinding().includeComment.messageInput.getMentionsEditText()));
    }

    public int getNumberOfAddComment() {
        if (mViewModel != null) {
            return mViewModel.mNumberOfCommentAdd;
        }
        return 0;
    }


    private void initAdapter(List<Comment> commentList, boolean isCanLoadMore) {
        mIsCanLoadMore = isCanLoadMore;
        getBinding().recyclerview.setOnTouchListener((v, e) -> {
            hideSoftKeyboard();
            v.performClick();
            return false;
        });

        List<Adaptive> adaptiveList = new ArrayList<>();
        if (mIsCanLoadMore) {
            adaptiveList.add(new LoadPreviousComment());
        }
        adaptiveList.addAll(commentList);

        getBinding().recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new CommentAdapter(adaptiveList,
                PopUpCommentFragment.this,
                (loadPreviousComment, position) -> mViewModel.loadMore(new OnCallbackListListener<Response<List<Comment>>>() {
                    @Override
                    public void onFail(ErrorThrowable ex) {
                        mAdapter.checkToRemoveLoadPreviousItem(mIsCanLoadMore, loadPreviousComment, position);
                    }

                    @Override
                    public void onComplete(Response<List<Comment>> result, boolean canLoadMore) {
                        mIsCanLoadMore = canLoadMore;
                        mAdapter.checkToRemoveAndAddNewComment(mIsCanLoadMore, loadPreviousComment, position, result.body());
                    }
                }));
        mAdapter.setCanLoadMore(false);
        getBinding().recyclerview.setAdapter(mAdapter);
        if (!adaptiveList.isEmpty()) {
            getBinding().recyclerview.scrollToPosition(adaptiveList.size() - 1);
        }
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (textView.getId() == R.id.message_input && i == EditorInfo.IME_ACTION_SEND) {
            getBinding().includeComment.sendButton.performClick();
        }
        return false;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.layout_pop_up_comment;
    }

    @Override
    public void onReplyButtonClick(Comment comment, int position) {
        if (mCommentDialogClickListener != null) {
            mCommentDialogClickListener.onReplaceCommentFragment(comment, position);
        }
    }

    @Override
    public void onLikeButtonClick(Comment comment, int position) {
        mViewModel.checkToLikeComment(comment);
    }

    @Override
    public void onLongPress(Comment comment, int position) {
        PostContextMenuDialog dialog = PostContextMenuDialog.newInstance();
        boolean isOwner = comment.getUser() != null && SharedPrefUtils.checkIsMe(getActivity(), comment.getUser().getId());
        dialog.addItem(PostContextMenuItemType.getCommentPopupMenuItem(isOwner));
        dialog.setOnItemClickListener(result -> {
            dialog.dismiss();
            PostContextMenuItemType itemType = PostContextMenuItemType.getItemType(result);
            if (itemType == PostContextMenuItemType.COPY_TEXT_CLICK) {
                CopyTextUtil.copyTextToClipboard(getActivity(), comment.getContent());

            } else if (itemType == PostContextMenuItemType.EDIT_POST_CLICK) {
                getBinding().includeComment.messageInput.post(() -> showKeyboard(getBinding().includeComment.messageInput.getMentionsEditText()));
                getBinding().includeComment.messageInput.setText(comment.getContent());
                mViewModel.setUpdateComment(comment, position);

            } else {
                MessageDialog messageDialog = new MessageDialog();
                messageDialog.setTitle(getString(R.string.comment_delete_title));
                messageDialog.setMessage(getString(R.string.comment_delete_message));
                messageDialog.setRightText(getString(R.string.change_language_button_cancel), null);
                messageDialog.setLeftText(getString(R.string.seller_store_button_delete),
                        v -> mViewModel.onDeleteComment(comment, () -> notifyItemRemove(position)));
                messageDialog.show(getChildFragmentManager());

            }
        });
        dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
    }

    @Override
    public void onPause() {
        super.onPause();
        getBinding().includeComment.messageInput.removeListPopupWindow();
    }

    public void notifyItemChanged(int position, Comment comment) {
        mAdapter.notifyItemChanged(position, comment);
    }

    public void notifyItemRemove(int position) {
        mAdapter.notifyItemRemove(position);
    }

    private void bindGifView() {
        getBinding().includeComment.containerSearchGif.onBackPressClickListener(v -> {
            getBinding().includeComment.sendLayout.setVisibility(View.VISIBLE);
            getBinding().includeComment.messageInput.requestFocus();
        });

        getBinding().includeComment.gifImageView.setOnClickListener(v -> {
            getBinding().includeComment.sendLayout.setVisibility(View.GONE);
            getBinding().includeComment.containerSearchGif.loadTrendingGifList(baseGifModel -> {
                getBinding().includeComment.sendLayout.setVisibility(View.VISIBLE);
                getBinding().includeComment.messageInput.requestFocus();

                ((AbsBaseActivity) mContext).startLoginWizardActivity(new LoginActivityResultCallback() {
                    @Override
                    public void onActivityResultSuccess(boolean isAlreadyLogin) {
                        Media media = new Media();
                        media.setWidth(baseGifModel.getGifWidth());
                        media.setHeight(baseGifModel.getGifHeight());
                        media.setUrl(baseGifModel.getGifUrl());
                        media.setType(MediaType.TENOR_GIF);

                        Comment comment = new Comment();
                        comment.setContent("gif");
                        comment.setUser(SharedPrefUtils.getUser(mContext));
                        comment.setDate(new Date());
                        comment.setMedia(media);
                        mViewModel.postComment(comment);
                        if (mAdapter != null) {
                            mAdapter.addLatestComment(comment);
                            getBinding().recyclerview.scrollToPosition(mAdapter.getItemCount() - 1);
                        }
                    }
                });
            });
        });
    }

    public boolean isCloseSearchGifView() {
        if (getBinding().includeComment.containerSearchGif.getVisibility() == View.VISIBLE) {
            getBinding().includeComment.containerSearchGif.clearData();
            getBinding().includeComment.sendLayout.setVisibility(View.VISIBLE);
            getBinding().includeComment.messageInput.requestFocus();
            return true;
        } else {
            return false;
        }
    }

    private void hideSoftKeyboard() {
        if (getContext() != null) {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null)
                imm.hideSoftInputFromWindow(getBinding().includeComment.messageInput.getWindowToken(), 0);
        }
    }
}
