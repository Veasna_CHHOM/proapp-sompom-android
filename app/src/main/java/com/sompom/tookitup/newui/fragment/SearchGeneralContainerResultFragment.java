package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.newadapter.ViewPagerAdapter;
import com.sompom.tookitup.databinding.FragmentSearchGeneralContainerResultBinding;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.model.Search;
import com.sompom.tookitup.model.SearchGeneralTypeResult;
import com.sompom.tookitup.model.emun.SearchGeneralResultType;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.SearchGeneralTypeResultDataManager;
import com.sompom.tookitup.utils.AttributeConverter;
import com.sompom.tookitup.viewmodel.newviewmodel.SearchMessageResultFragmentViewModel;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by nuonveyo on 8/2/18.
 */

public class SearchGeneralContainerResultFragment extends AbsBindingFragment<FragmentSearchGeneralContainerResultBinding> {
    public static final String TAG = SearchGeneralContainerResultFragment.class.getName();
    @Inject
    public ApiService mApiService;
    private ViewPagerAdapter mViewPagerAdapter;
    private String mSearchText;
    private SearchMessageResultFragmentViewModel mViewModel;

    public static SearchGeneralContainerResultFragment newInstance() {
        return new SearchGeneralContainerResultFragment();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_search_general_container_result;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        initTabFail();

        SearchGeneralTypeResultDataManager dataManager = new SearchGeneralTypeResultDataManager(getActivity(), mApiService);
        mViewModel = new SearchMessageResultFragmentViewModel(dataManager, new OnCallbackListListener<SearchGeneralTypeResult>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                initTabFail();
            }

            @Override
            public void onComplete(SearchGeneralTypeResult result, boolean canLoadMore) {
                initTabLayout(result);
            }
        });
        setVariable(BR.viewModel, mViewModel);

    }

    public void showLoading() {
        if (mViewModel == null) {
            return;
        }
        mViewModel.mIsLoading.set(true);
    }

    public void onSearch(String searchText) {
        mSearchText = searchText;
        if (mViewModel != null) {
            mViewModel.getData(searchText);
        }
    }

    private void initTabLayout(SearchGeneralTypeResult result) {
        if (mViewModel == null) {
            return;
        }
        mViewModel.mIsLoading.set(false);

        if (getActivity() == null || mViewPagerAdapter == null) {
            return;
        }

        ArrayList<Search> data;

        data = new ArrayList<>();
//        data.addAll(result.getProduct().getData());
        data.addAll(result.getUser().getData());
//        data.addAll(result.getBuyingConversation().getData());
//        data.addAll(result.getSellingConversation().getData());
        data.addAll(result.getConversation().getData());

        ((SearchGeneralTypeResultFragment) mViewPagerAdapter.getFr(0)).refresh(data);

//        ((SearchGeneralTypeResultFragment) mViewPagerAdapter.getFr(1)).refresh(new ArrayList<>(result.getProduct().getData()));

        ((SearchGeneralTypeResultFragment) mViewPagerAdapter.getFr(1)).refresh(new ArrayList<>(result.getUser().getData()));

//        ((SearchGeneralTypeResultFragment) mViewPagerAdapter.getFr(3)).refresh(new ArrayList<>(result.getBuyingConversation().getData()));
//
//        ((SearchGeneralTypeResultFragment) mViewPagerAdapter.getFr(4)).refresh(new ArrayList<>(result.getSellingConversation().getData()));

        ((SearchGeneralTypeResultFragment) mViewPagerAdapter.getFr(2)).refresh(new ArrayList<>(result.getConversation().getData()));

    }


    private void initTabFail() {
        if (mViewPagerAdapter == null) {
            mViewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
            mViewPagerAdapter.addFragment(SearchGeneralTypeResultFragment.newInstance(new ArrayList<>()),
                    getActivity().getString(SearchGeneralResultType.ALL.getTitle()));

//            mViewPagerAdapter.addFragment(SearchGeneralTypeResultFragment.newInstance(new ArrayList<>()),
//                    getActivity().getString(SearchGeneralResultType.ITEMS.getTitle()));

            mViewPagerAdapter.addFragment(SearchGeneralTypeResultFragment.newInstance(new ArrayList<>()),
                    getActivity().getString(SearchGeneralResultType.PEOPLE.getTitle()));

//            mViewPagerAdapter.addFragment(SearchGeneralTypeResultFragment.newInstance(new ArrayList<>()),
//                    getActivity().getString(SearchGeneralResultType.BUYING.getTitle()));
//
//            mViewPagerAdapter.addFragment(SearchGeneralTypeResultFragment.newInstance(new ArrayList<>()),
//                    getActivity().getString(SearchGeneralResultType.SELLING.getTitle()));

            mViewPagerAdapter.addFragment(SearchGeneralTypeResultFragment.newInstance(new ArrayList<>()),
                    getActivity().getString(SearchGeneralResultType.CONVERSATION.getTitle()));

            getBinding().viewPager.setAdapter(mViewPagerAdapter);
            getBinding().tabs.setTabTextColors(ContextCompat.getColor(getActivity(), R.color.darkGrey),
                    AttributeConverter.convertAttrToColor(getActivity(), R.attr.colorContrast));
            getBinding().tabs.setupWithViewPager(getBinding().viewPager);
            getBinding().viewPager.setOffscreenPageLimit(5);
        } else {
            for (int i = 0; i < mViewPagerAdapter.getCount(); i++) {
                ((SearchGeneralTypeResultFragment) mViewPagerAdapter.getFr(i)).refresh(new ArrayList<>());
            }
        }
    }

}
