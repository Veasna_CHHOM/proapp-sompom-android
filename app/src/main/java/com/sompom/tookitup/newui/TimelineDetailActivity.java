package com.sompom.tookitup.newui;

import android.app.Activity;
import android.os.Bundle;

import com.sompom.tookitup.intent.newintent.TimelineDetailIntent;
import com.sompom.tookitup.intent.newintent.TimelineDetailIntentResult;
import com.sompom.tookitup.model.WallStreetAdaptive;
import com.sompom.tookitup.newui.fragment.TimelineDetailFragment;

public class TimelineDetailActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WallStreetAdaptive lifeStream = null;
        String id;
        int position = 0;

        if (getIntent().getData() != null) {
            id = getIntent().getData().getLastPathSegment();
        } else {
            TimelineDetailIntent intent = new TimelineDetailIntent(getIntent());
            lifeStream = intent.getLifeStream();
            position = intent.getPosition();
            id = intent.getWallStreetId();

        }

        setFragment(TimelineDetailFragment.newInstance(lifeStream, position, id),
                TimelineDetailFragment.TAG);
    }

    @Override
    public void onBackPressed() {
        TimelineDetailFragment fragment = (TimelineDetailFragment) getFragment(TimelineDetailFragment.TAG);
        if (fragment != null) {
            if (!fragment.isCloseSearchGifView()) {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void finish() {
        setResult();
        super.finish();
    }

    private void setResult() {
        TimelineDetailFragment fragment = (TimelineDetailFragment) getFragment(TimelineDetailFragment.TAG);
        if (fragment != null) {
            fragment.getBinding().recyclerView.pause();
            TimelineDetailIntentResult intentResult = new TimelineDetailIntentResult(fragment.getWallStreetAdaptive(),
                    fragment.isRemoveItem(),
                    fragment.isGrantPermissionFloatingVideoFirstTime());
            setResult(Activity.RESULT_OK, intentResult);
        }
    }

    @Override
    public void onFloatingVideoServiceConnected() {
        if (getFloatingVideo() != null
                && getFloatingVideo().isFloating()) {
            getFloatingVideo().removeVideo();
        }
    }
}
