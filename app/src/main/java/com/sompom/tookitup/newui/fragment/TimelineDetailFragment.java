package com.sompom.tookitup.newui.fragment;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.sompom.baseactivity.ResultCallback;
import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.newadapter.TimelineDetailAdapter;
import com.sompom.tookitup.broadcast.upload.FloatingVideoService;
import com.sompom.tookitup.databinding.FragmentTimelineDetailBinding;
import com.sompom.tookitup.helper.LoginActivityResultCallback;
import com.sompom.tookitup.helper.OpenCommentDialogHelper;
import com.sompom.tookitup.injection.controller.PublicQualifier;
import com.sompom.tookitup.intent.CreateTimelineIntent;
import com.sompom.tookitup.intent.CreateTimelineIntentResult;
import com.sompom.tookitup.intent.newintent.ProductDetailIntent;
import com.sompom.tookitup.intent.newintent.TimelineDetailIntent;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnCommentItemClickListener;
import com.sompom.tookitup.listener.OnDetailShareItemCallback;
import com.sompom.tookitup.listener.OnEditTextCommentListener;
import com.sompom.tookitup.listener.OnFloatingVideoClickedListener;
import com.sompom.tookitup.listener.OnTimelineItemButtonClickListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.LifeStream;
import com.sompom.tookitup.model.LoadPreviousComment;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.SharedProduct;
import com.sompom.tookitup.model.SharedTimeline;
import com.sompom.tookitup.model.WallStreetAdaptive;
import com.sompom.tookitup.model.emun.ContentType;
import com.sompom.tookitup.model.emun.CreateWallStreetScreenType;
import com.sompom.tookitup.model.emun.FollowItemType;
import com.sompom.tookitup.model.emun.MediaType;
import com.sompom.tookitup.model.emun.PostContextMenuItemType;
import com.sompom.tookitup.model.emun.TimelinePostItem;
import com.sompom.tookitup.model.result.Comment;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.TimelineDetailActivity;
import com.sompom.tookitup.newui.dialog.BigPlayerFragment;
import com.sompom.tookitup.newui.dialog.CommentDialog;
import com.sompom.tookitup.newui.dialog.ListFollowDialog;
import com.sompom.tookitup.newui.dialog.MessageDialog;
import com.sompom.tookitup.newui.dialog.PostContextMenuDialog;
import com.sompom.tookitup.newui.dialog.ReportDialog;
import com.sompom.tookitup.newui.dialog.ShareLinkMenuDialog;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.TimelineDetailDataManager;
import com.sompom.tookitup.utils.CopyTextUtil;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.utils.TimelinePostItemUtil;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemTimelineHeaderViewModel;
import com.sompom.tookitup.viewmodel.newviewmodel.TimelineDetailViewModel;
import com.sompom.videomanager.widget.MediaRecyclerView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;

/**
 * Created by nuonveyo on 7/12/18.
 */

public class TimelineDetailFragment extends AbsBindingFragment<FragmentTimelineDetailBinding>
        implements OnFloatingVideoClickedListener,
        BigPlayerFragment.Callback,
        OnEditTextCommentListener,
        OnCommentItemClickListener,
        OnTimelineItemButtonClickListener {

    private static final int OVERLAY_PERMISSION_REQUEST_CODE = 0X001;
    @Inject
    public ApiService mApiService;
    @Inject
    @PublicQualifier
    public ApiService mApiServiceTest;
    private WallStreetAdaptive mWallStreetAdaptive;
    private TimelineDetailViewModel mViewModel;
    private TimelineDetailAdapter mAdapter;

    private int mMediaPosition;
    private boolean mIsRemoveItem;
    private boolean mIsGrantPermissionFloatingVideoFirstTime;
    private boolean mIsShowComment;
    private OpenCommentDialogHelper mCommentDialog;
    private boolean mIsCanLoadMore;

    public static TimelineDetailFragment newInstance(WallStreetAdaptive adaptive, int clickPosition, String wallStreetId) {
        Bundle args = new Bundle();
        args.putParcelable(SharedPrefUtils.DATA, adaptive);
        args.putInt(SharedPrefUtils.ID, clickPosition);
        args.putString(SharedPrefUtils.STATUS, wallStreetId);
        TimelineDetailFragment fragment = new TimelineDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_timeline_detail;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);

        String wallStreetId = null;
        if (getArguments() != null) {
            mWallStreetAdaptive = getArguments().getParcelable(SharedPrefUtils.DATA);
            mMediaPosition = getArguments().getInt(SharedPrefUtils.ID);
            wallStreetId = getArguments().getString(SharedPrefUtils.STATUS);
        }

        TimelineDetailDataManager dataManager = new TimelineDetailDataManager(getActivity(), mApiService, mApiServiceTest);
        mViewModel = new TimelineDetailViewModel((AbsBaseActivity) getActivity(), wallStreetId,
                dataManager,
                mWallStreetAdaptive,
                new TimelineDetailViewModel.Callback() {
                    @Override
                    public void onFollowButtonClick(String id, boolean isFollowing) {
                        checkUserFollow(isFollowing, false);
                        mViewModel.checkToPostFollow(id, isFollowing);
                    }

                    @Override
                    public void onShowPostContextMenuClick(Adaptive adaptive, User user, int position) {
                        showPostContextMenuClick(adaptive, user);
                    }

                    @Override
                    public void onCommentClick(Adaptive adaptive) {

                    }

                    @Override
                    public MediaRecyclerView getRecyclerView() {
                        return getBinding().recyclerView;
                    }

                    @Override
                    public void onShowAdapter(List<Adaptive> adaptiveList, boolean isShowEditText) {
                        mIsShowComment = isShowEditText;
                        if (mIsShowComment) {
                            bindGifView();
                            showKeyboard(getBinding().includeComment.messageInput.getMentionsEditText());
                        }

                        initAdapter(adaptiveList);
                    }

                    @Override
                    public void onLoadCommentSuccess(List<Comment> commentList, boolean isCanLoadMore) {
                        mIsCanLoadMore = isCanLoadMore;
                        List<Adaptive> adaptiveList = new ArrayList<>();
                        if (mIsCanLoadMore) {
                            adaptiveList.add(0, new LoadPreviousComment());
                        }
                        adaptiveList.addAll(commentList);
                        mAdapter.addLoadMoreData(adaptiveList);
                    }

                    @Override
                    public void onGetCommentDetailSuccess(WallStreetAdaptive adaptive) {
                        mWallStreetAdaptive = adaptive;
                    }
                },
                this);
        setVariable(BR.viewModel, mViewModel);
    }

    @Override
    public void onResume() {
        super.onResume();
        getBinding().recyclerView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        getBinding().recyclerView.onPause();
        getBinding().includeComment.messageInput.removeListPopupWindow();

    }

    private boolean isFloatingVideoPlaying() {
        if (getActivity() instanceof AbsBaseActivity) {
            FloatingVideoService.FloatingVideoBinder floatingVideoService = ((AbsBaseActivity) getActivity()).getFloatingVideo();
            return floatingVideoService != null && floatingVideoService.isFloating();
        }
        return false;
    }

    private void setOnFloatingVideoRemoveListener() {
        if (getActivity() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getActivity()).setOnFloatingVideoRemoveListener(this::resumeVideo);
        }
    }

    private void resumeVideo() {
        getBinding().recyclerView.setAutoPlay(true);
        getBinding().recyclerView.resumeAfterRecycle();
    }

    private void checkToResumeAudioPlayVideo() {
        boolean isPlaying = isFloatingVideoPlaying();
        if (isPlaying) {
            getBinding().recyclerView.setAutoPlay(false);
            setOnFloatingVideoRemoveListener();
        } else {
            resumeVideo();
        }
    }

    private void initAdapter(List<Adaptive> adaptiveList) {
        getBinding().recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        getBinding().recyclerView.requestAudioFocus();

        mAdapter = new TimelineDetailAdapter((AbsBaseActivity) getActivity(),
                mApiService,
                adaptiveList,
                this,
                this,
                this,
                new OnDetailShareItemCallback() {
                    @Override
                    public void onMediaClick(WallStreetAdaptive adaptive, int position) {
                        if (getActivity() != null) {
                            if (adaptive instanceof LifeStream) {
                                ((AbsBaseActivity) getActivity()).startActivityForResult(new TimelineDetailIntent(getContext(), adaptive, position),
                                        new ResultCallback() {
                                            @Override
                                            public void onActivityResultSuccess(int resultCode, Intent data) {
                                                checkToResumeAudioPlayVideo();
                                            }
                                        });
                            } else {
                                getActivity().startActivity(new ProductDetailIntent(getActivity(),
                                        (Product) adaptive,
                                        false,
                                        false));
                            }
                        }
                    }

                    @Override
                    public void onClick() {
                        //Click like from item
                    }
                },
                (loadPreviousComment, position) -> {
                    mViewModel.loadMoreCommentList(new OnCallbackListListener<Response<List<Comment>>>() {
                        @Override
                        public void onFail(ErrorThrowable ex) {
                            mAdapter.checkToRemoveLoadPreviousItem(mIsCanLoadMore, loadPreviousComment, position);
                        }

                        @Override
                        public void onComplete(Response<List<Comment>> result, boolean canLoadMore) {
                            mIsCanLoadMore = canLoadMore;
                            mAdapter.checkToRemoveAndAddNewComment(mIsCanLoadMore, loadPreviousComment, position, result.body());
                        }
                    });
                });
        getBinding().recyclerView.setAdapter(mAdapter);
        mAdapter.setCanLoadMore(mIsCanLoadMore);
        getBinding().recyclerView.scrollToPosition(getScrollToMediaPosition());
        getBinding().recyclerView.setOnTouchListener((v, e) -> {
            if (getContext() != null) {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
            v.performClick();
            return false;
        });
    }

    private int getScrollToMediaPosition() {
        /*
          {@link mMediaPosition != - 1} true need to scroll to media position
         */
        if (mMediaPosition != ListItemTimelineHeaderViewModel.CLICK_ON_TEXT_POSITION) {
            return mMediaPosition + 1;
        } else {
            return 0;
        }
    }

    public WallStreetAdaptive getWallStreetAdaptive() {
        return mWallStreetAdaptive;
    }

    public boolean isRemoveItem() {
        return mIsRemoveItem;
    }

    @Override
    public void onFloatingVideo(int position, long time) {
        mMediaPosition = position;
        mWallStreetAdaptive.getMedia().get(position).setTime(time);
        requestPermission();
    }

    @Override
    public void onFullScreenClicked(int position, long time) {
        getBinding().recyclerView.pause();
        mWallStreetAdaptive.getMedia().get(position).setTime(time);
        BigPlayerFragment playerFragment =
                BigPlayerFragment.newInstance(position,
                        mWallStreetAdaptive.getMedia().get(position),
                        false);
        playerFragment.show(getChildFragmentManager(), BigPlayerFragment.TAG);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == OVERLAY_PERMISSION_REQUEST_CODE && Settings.canDrawOverlays(getActivity())) {
            //Start floating video
            mIsGrantPermissionFloatingVideoFirstTime = true;
            popupVideo();
        }
    }

    private void requestPermission() {
        // SYSTEM_ALERT_WINDOW permission need to request run-time with android Marshmallow and above
        if (getActivity() != null
                && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && !Settings.canDrawOverlays(getActivity())) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getActivity().getPackageName()));
            getActivity().startActivityForResult(intent, OVERLAY_PERMISSION_REQUEST_CODE);
        } else {
            popupVideo();
        }
    }

    private void popupVideo() {
        FloatingVideoService.startFloating(getActivity(), (LifeStream) mWallStreetAdaptive, mMediaPosition);
        if (getActivity() != null) {
            getActivity().finish();
        }
    }

    @Override
    public void onBigPlayerDestroyed(int position, boolean isAutoPlay, Media media) {
        if (isAutoPlay) {
            getBinding().recyclerView.resume();
        }
    }

    public boolean isGrantPermissionFloatingVideoFirstTime() {
        return mIsGrantPermissionFloatingVideoFirstTime;
    }

    @Override
    public void onSendText(Comment comment) {
        if (mAdapter != null) {
            hideKeyboard();
            mAdapter.addLatestComment(comment);
            getBinding().recyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
        }
    }

    @Override
    public void onPostCommentFail() {
        if (mAdapter != null) {
            mAdapter.removeInstanceComment();
        }
    }

    @Override
    public void onPostCommentSuccess(Comment comment) {
        mAdapter.notifyItemChanged(mAdapter.getItemCount() - 1, comment);
    }

    @Override
    public void onUpdateCommentSuccess(Comment comment, int position) {
        hideKeyboard();
        mAdapter.notifyItemChanged(position, comment);
    }

    @Override
    public void onReplyButtonClick(Comment comment, int position) {
        if (mCommentDialog == null) {
            mCommentDialog = new OpenCommentDialogHelper(getActivity());
        }
        mCommentDialog.show(mWallStreetAdaptive,
                comment,
                mAdapter,
                new CommentDialog.OnCloseReplyCommentFragmentListener() {
                    @Override
                    public void onDeleteComment() {
                        mAdapter.notifyItemRemove(position);
                    }

                    @Override
                    public void onComplete(Comment result) {
                        mAdapter.notifyItemChanged(position, result);
                    }
                });
    }

    @Override
    public void onLikeButtonClick(Comment comment, int position) {
        //TODO: implement api like comment after server done
    }

    @Override
    public void onLongPress(Comment comment, int position) {
        PostContextMenuDialog dialog = PostContextMenuDialog.newInstance();
        boolean isOwner = comment.getUser() != null && SharedPrefUtils.checkIsMe(getActivity(), comment.getUser().getId());
        dialog.addItem(PostContextMenuItemType.getCommentPopupMenuItem(isOwner));
        dialog.setOnItemClickListener(result -> {
            dialog.dismiss();
            PostContextMenuItemType itemType = PostContextMenuItemType.getItemType(result);
            if (itemType == PostContextMenuItemType.COPY_TEXT_CLICK) {
                CopyTextUtil.copyTextToClipboard(getActivity(), comment.getContent());
            } else if (itemType == PostContextMenuItemType.EDIT_POST_CLICK) {
                getBinding().includeComment.messageInput.post(() -> showKeyboard(getBinding().includeComment.messageInput.getMentionsEditText()));
                getBinding().includeComment.messageInput.setText(comment.getContent());
                mViewModel.setUpdateComment(comment, position);

            } else {
                MessageDialog messageDialog = new MessageDialog();
                messageDialog.setTitle(getString(R.string.comment_delete_title));
                messageDialog.setMessage(getString(R.string.comment_delete_message));
                messageDialog.setRightText(getString(R.string.change_language_button_cancel), null);
                messageDialog.setLeftText(getString(R.string.seller_store_button_delete),
                        v -> mViewModel.onDeleteComment(comment, () -> mAdapter.notifyItemRemove(position)));
                messageDialog.show(getChildFragmentManager());
            }
        });
        dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
    }

    private void checkUserFollow(boolean isFollowing, boolean isSubWallStreet) {
        if (mWallStreetAdaptive instanceof SharedTimeline || mWallStreetAdaptive instanceof SharedProduct) {
            if (isSubWallStreet) {
                if (mWallStreetAdaptive instanceof SharedTimeline) {
                    ((SharedTimeline) mWallStreetAdaptive).getLifeStream().getUser().setFollow(isFollowing);
                } else {
                    ((SharedProduct) mWallStreetAdaptive).getProduct().getUser().setFollow(isFollowing);
                }
            } else {
                mWallStreetAdaptive.getUser().setFollow(isFollowing);
            }
        } else {
            ((LifeStream) mWallStreetAdaptive).getStoreUser().setFollow(isFollowing);
        }
    }

    private void bindGifView() {
        getBinding().includeComment.containerSearchGif.onBackPressClickListener(v -> {
            getBinding().includeComment.sendLayout.setVisibility(View.VISIBLE);
            getBinding().includeComment.messageInput.requestFocus();
        });

        getBinding().includeComment.gifImageView.setOnClickListener(v -> {
            getBinding().includeComment.sendLayout.setVisibility(View.GONE);
            getBinding().includeComment.containerSearchGif.loadTrendingGifList(baseGifModel -> {
                getBinding().includeComment.sendLayout.setVisibility(View.VISIBLE);
                getBinding().includeComment.messageInput.requestFocus();
                if (getActivity() != null) {
                    ((AbsBaseActivity) getActivity()).startLoginWizardActivity(new LoginActivityResultCallback() {
                        @Override
                        public void onActivityResultSuccess(boolean isAlreadyLogin) {
                            Media media = new Media();
                            media.setWidth(baseGifModel.getGifWidth());
                            media.setHeight(baseGifModel.getGifHeight());
                            media.setUrl(baseGifModel.getGifUrl());
                            media.setType(MediaType.TENOR_GIF);
                            Comment comment = new Comment();

                            comment.setContent("gif");
                            comment.setUser(SharedPrefUtils.getUser(getContext()));
                            comment.setDate(new Date());
                            comment.setMedia(media);
                            mViewModel.postComment(comment);

                            hideKeyboard();
                            mAdapter.addLatestComment(comment);
                            getBinding().recyclerView.scrollToPosition(mAdapter.getItemCount() - 1);

                        }
                    });
                }
            });
        });
    }

    public boolean isCloseSearchGifView() {
        if (getBinding().includeComment.containerSearchGif.getVisibility() == View.VISIBLE) {
            getBinding().includeComment.containerSearchGif.clearData();
            getBinding().includeComment.sendLayout.setVisibility(View.VISIBLE);
            getBinding().includeComment.messageInput.requestFocus();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onFollowButtonClick(String id, boolean isFollowing) {
        //Follow in Shared Timeline Item
        checkUserFollow(isFollowing, true);
        mViewModel.checkToPostFollow(id, isFollowing);
    }

    @Override
    public void onShowPostContextMenuClick(Adaptive adaptive, User user, int position) {
        //Context Men in Shared Timeline Item
        showPostContextMenuClick(adaptive, user);
    }

    @Override
    public void onCommentClick(Adaptive adaptive) {

    }

    @Override
    public MediaRecyclerView getRecyclerView() {
        return getBinding().recyclerView;
    }

    @Override
    public void onCommentButtonClick(Adaptive adaptive, int position) {
        if (mIsShowComment) {
            showKeyboard(getBinding().includeComment.messageInput.getMentionsEditText());
        } else {
            if (mCommentDialog == null) {
                mCommentDialog = new OpenCommentDialogHelper(getActivity());
            }
            mCommentDialog.show(adaptive, mAdapter);
        }
    }

    @Override
    public void onShareButtonClick(Adaptive adaptive, int position) {
        ShareLinkMenuDialog dialog = ShareLinkMenuDialog.newInstance(mWallStreetAdaptive);
        dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
    }

    @Override
    public void onUserViewMediaClick(Adaptive adaptive) {
        ListFollowDialog dialog = ListFollowDialog.newInstance(adaptive, FollowItemType.USER_ACTION);
        dialog.show(getChildFragmentManager(), ListFollowDialog.TAG);
    }

    @Override
    public void onLikeViewerClick(Adaptive adaptive) {
        ListFollowDialog dialog = ListFollowDialog.newInstance(adaptive, FollowItemType.LIKE_VIEWER);
        dialog.show(getChildFragmentManager(), ListFollowDialog.TAG);
    }

    private void showPostContextMenuClick(Adaptive adaptive, User user) {
        if (user == null) {
            return;
        }

        TimelinePostItem timelinePostItem = TimelinePostItemUtil.getPostItem(adaptive);
        mWallStreetAdaptive = (WallStreetAdaptive) adaptive;
        PostContextMenuDialog dialog = PostContextMenuDialog.newInstance();
        dialog.addItem(PostContextMenuItemType.getItems(adaptive, SharedPrefUtils.checkIsMe(getContext(), user.getId()), timelinePostItem));
        dialog.setOnItemClickListener(result -> {
            PostContextMenuItemType itemType = PostContextMenuItemType.getItemType(result);
            if (itemType == PostContextMenuItemType.EDIT_POST_CLICK) {
                if (mWallStreetAdaptive != null
                        && getActivity() != null
                        && getActivity() instanceof AbsBaseActivity) {
                    CreateTimelineIntent intent = new CreateTimelineIntent(getActivity(), mWallStreetAdaptive, CreateWallStreetScreenType.EDIT_POST);
                    ((AbsBaseActivity) getActivity()).startActivityForResult(intent, new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            CreateTimelineIntentResult result1 = new CreateTimelineIntentResult(data);
                            mWallStreetAdaptive = result1.getLifeStream();
                            mAdapter.updateItem(0, (Adaptive) mWallStreetAdaptive);
                        }
                    });
                }
            } else if (itemType == PostContextMenuItemType.UNFOLLOW_CLICK) {
                mViewModel.checkToPostFollow(user.getId(), false);
                if (adaptive instanceof WallStreetAdaptive && ((WallStreetAdaptive) adaptive).getUser() != null) {
                    ((WallStreetAdaptive) adaptive).getUser().setFollow(false);
                    mAdapter.update(adaptive);
                }
            } else if (itemType == PostContextMenuItemType.EDIT_PRIVACY_CLICK) {
                showEditPrivacyDialog(result1 -> {
                    //TODO: implement api edit privacy
                });
            } else if (itemType == PostContextMenuItemType.REPORT_CLICK) {
                if (mWallStreetAdaptive != null) {
                    ReportDialog reportDialog = ReportDialog.newInstance(ContentType.POST);
                    reportDialog.setOnReportClickListener(reportRequest -> mViewModel.reportPost(mWallStreetAdaptive.getId(), reportRequest));
                    reportDialog.show(getChildFragmentManager());
                }
            } else if (itemType == PostContextMenuItemType.TURNOFF_NOTIFICATION_CLICK) {
                mViewModel.unFollowShop(adaptive.getId());
            } else if (itemType == PostContextMenuItemType.DELETE_CLICK) {
                if (mWallStreetAdaptive != null) {
                    showMessageDialog(R.string.post_menu_delete_post_title,
                            R.string.post_menu_delete_post_description,
                            R.string.seller_store_button_delete,
                            R.string.change_language_button_cancel, dialog1 -> {
                                if (getActivity() instanceof TimelineDetailActivity) {
                                    mViewModel.deletePost(mWallStreetAdaptive);
                                    mIsRemoveItem = true;
                                    getActivity().finish();
                                }
                            });
                }
            }
            dialog.dismiss();

        });
        dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
    }
}

