package com.sompom.tookitup.newui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.ActivityProductDetail2Binding;
import com.sompom.tookitup.helper.BaseBuyingProcessHelper;
import com.sompom.tookitup.helper.LoginActivityResultCallback;
import com.sompom.tookitup.helper.OpenCommentDialogHelper;
import com.sompom.tookitup.injection.controller.PublicQualifier;
import com.sompom.tookitup.intent.CallingIntent;
import com.sompom.tookitup.intent.ChatIntent;
import com.sompom.tookitup.intent.newintent.ProductDetailIntent;
import com.sompom.tookitup.intent.newintent.ProductDetailResultIntent;
import com.sompom.tookitup.model.emun.ContentType;
import com.sompom.tookitup.model.emun.FollowItemType;
import com.sompom.tookitup.model.result.ContentStat;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.dialog.ListFollowDialog;
import com.sompom.tookitup.newui.dialog.ReportDialog;
import com.sompom.tookitup.newui.fragment.ProductDetailFragment;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.ProductManager;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.utils.SnackBarUtil;
import com.sompom.tookitup.viewmodel.newviewmodel.ProductDetailActivityViewModel;

import javax.inject.Inject;

public class ProductDetailActivity extends AbsSoundControllerActivity<ActivityProductDetail2Binding> {

    @Inject
    public ApiService mApiService;
    @Inject
    @PublicQualifier
    public ApiService mPublicApiService;
    private ProductDetailActivityViewModel mViewModel;
    private Product mProduct;
    private Menu mMenu;
    private ReportDialog mReportDialog;

    private boolean mIsThisProductBelongToOwner;
    private OpenCommentDialogHelper mCommentDialog;


    @Override
    public int getLayoutRes() {
        return R.layout.activity_product_detail2;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getControllerComponent().inject(this);
        setSupportActionBar(getBinding().toolbar);

        ProductDetailIntent intent = new ProductDetailIntent(getIntent());
        mProduct = intent.getProduct();
        if (mProduct == null && intent.getData() != null) {
            String lastPath = intent.getData().getLastPathSegment();
            mProduct = new Product();
            mProduct.setId(lastPath);
        }

        setFragment(R.id.container_view_top,
                ProductDetailFragment.newInstance(mProduct),
                ProductDetailFragment.TAG);

        ProductManager productManager = new ProductManager(this, mApiService);
        mViewModel = new ProductDetailActivityViewModel(productManager,
                new ProductDetailActivityViewModel.OnProductListener() {

                    @Override
                    public void onChatClick() {
                        findViewById(android.R.id.content).post(ProductDetailActivity.this::openChat);
                    }

                    @Override
                    public void onBuyClick(View view) {
                        view.setEnabled(false);
                        BaseBuyingProcessHelper processHelper = new BaseBuyingProcessHelper(ProductDetailActivity.this, mApiService);
                        processHelper.setListener(new BaseBuyingProcessHelper.OnBuyingProcessListener() {
                            @Override
                            public void onBuyOrCancelSuccess(boolean isBuying, String message) {
                                SnackBarUtil.showSnackBar(view, message);
                                mProduct.setRequestBuy(isBuying);
                                mViewModel.setProduct(mProduct);
                                view.setEnabled(true);
                            }

                            @Override
                            public void onError(String errorMessage) {
                                SnackBarUtil.showSnackBar(view, errorMessage);
                                view.setEnabled(true);
                            }

                            @Override
                            public void shouldEnableButton(boolean isEnable) {
                                view.setEnabled(isEnable);
                            }
                        });

                        processHelper.startBuyOnProduct(mProduct);
                    }

                    @Override
                    public void onCallClick(User user) {
                        startLoginWizardActivity(new LoginActivityResultCallback() {
                            @Override
                            public void onActivityResultSuccess(boolean isAlreadyLogin) {
                                if (TextUtils.equals(SharedPrefUtils.getUserId(getThis()), user.getId())) {
                                    return;
                                }
                                startActivity(new CallingIntent(getThis(), user, mProduct));
                            }
                        });
                    }

                    @Override
                    public void onCheckComplete(boolean isBelongToOwner) {
                        mIsThisProductBelongToOwner = isBelongToOwner;
                        MenuItem menuItem = mMenu.findItem(R.id.action_report);
                        menuItem.setVisible(!isBelongToOwner);
                    }

                    @Override
                    public void onPopUpCommentLayout() {
                        showCommentLayout();
                    }

                    @Override
                    public void onPopUpLikeViewerLayout() {
//                        LikeViewerLayout dialog = LikeViewerLayout.newInstance(mProduct.getId());
//                        dialog.show(getSupportFragmentManager(), LikeViewerLayout.TAG);
                        ListFollowDialog dialog = ListFollowDialog.newInstance(mProduct, FollowItemType.LIKE_VIEWER);
                        dialog.show(getSupportFragmentManager(), ListFollowDialog.TAG);
                    }
                });
        setVariable(BR.viewModel, mViewModel);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mMenu = menu;
        getMenuInflater().inflate(R.menu.menu_product_detail, menu);
        hideMenuReport();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_report) {
            startReport();
        }

        return super.onOptionsItemSelected(item);
    }

    public void setShown(boolean isShow) {
        if (mViewModel == null) {
            return;
        }
        mViewModel.setShow(isShow);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        ProductDetailIntent intent1 = new ProductDetailIntent(intent);
        if (intent1.getProduct() != null && mProduct != null) {
            Product product = intent1.getProduct();
            if (!product.getId().equals(mProduct.getId())) {
                startActivity(new ProductDetailIntent(this, intent1.getProduct(),
                        intent1.isOpenComment(), intent1.isOpenFromNotification()));
            }
        }
    }

    private void startReport() {
        if (mProduct != null) {
            startLoginWizardActivity(new LoginActivityResultCallback() {
                @Override
                public void onActivityResultSuccess(boolean isAlreadyLogin) {
                    if (mProduct == null) {
                        return;
                    }
                    if (mProduct.getUser().getId().equals(SharedPrefUtils.getUserId(ProductDetailActivity.this))) {
                        return;
                    }
                    mReportDialog = ReportDialog.newInstance(ContentType.PRODUCT);
                    mReportDialog.setOnReportClickListener(reportRequest -> mViewModel.reportProduct(mProduct.getId(), reportRequest));
                    mReportDialog.show(getSupportFragmentManager());

                }
            });
        }
    }

    @Override
    public void finish() {
        ProductDetailResultIntent result = new ProductDetailResultIntent(getFragment().getProduct());
        setResult(Activity.RESULT_OK, result);
        super.finish();
    }

    private ProductDetailFragment getFragment() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(ProductDetailFragment.TAG);
        return (ProductDetailFragment) fragment;
    }

    private void openChat() {
        if (!mIsThisProductBelongToOwner) {
            startLoginWizardActivity(new LoginActivityResultCallback() {
                @Override
                public void onActivityResultSuccess(boolean isAlreadyLogin) {
                    if (mProduct.getUser() == null) {
                        return;
                    }
                    if (SharedPrefUtils.isLogin(ProductDetailActivity.this)
                            && !TextUtils.equals(SharedPrefUtils.getUserId(ProductDetailActivity.this), mProduct.getUser().getId())) {
                        ChatIntent intent = new ChatIntent(ProductDetailActivity.this, mProduct);
                        startActivity(intent);
                    }
                }
            });
        }
    }

    public void setValue(Product product) {
        if (product != null && !TextUtils.isEmpty(product.getId())) {
            mProduct = product;
            checkProductIsBelongToOwner();
        }
    }

    public void checkProductIsBelongToOwner() {
        if (mProduct == null) {
            return;
        }
        if (mProduct.getUser() == null) {
            return;
        }

        if (mMenu == null) {
            return;
        }
        mViewModel.setProduct(mProduct);
    }

    public void showCommentLayout() {
        if (mCommentDialog == null) {
            mCommentDialog = new OpenCommentDialogHelper(this);
        }
        mCommentDialog.show(mProduct, result -> {
            if (result instanceof Product && ((Product) result).getContentStat() != null) {
                Product product = (Product) result;
                mViewModel.mCommentCount.set(String.valueOf(product.getContentStat().getTotalComments()));
            }
        });
    }

    public void updateLikeCount(ContentStat contentStat, boolean isLike) {
        if (mProduct != null) {
            mProduct.setContentStat(contentStat);
            mProduct.setLike(isLike);
        }
        mViewModel.mLikeCount.set(String.valueOf(contentStat.getTotalLikes()));
        mViewModel.mIsLike.set(isLike);
    }

    private void hideMenuReport() {
        if (mMenu == null) {
            return;
        }

        MenuItem menuItem = mMenu.findItem(R.id.action_report);
        menuItem.setVisible(false);
    }
}
