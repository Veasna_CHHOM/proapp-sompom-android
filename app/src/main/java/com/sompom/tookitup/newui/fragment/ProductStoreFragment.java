package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.text.TextUtils;
import android.view.View;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.newadapter.ProductStoreAdapter;
import com.sompom.tookitup.databinding.FragmentProductStoreBinding;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnEditDialogClickListener;
import com.sompom.tookitup.listener.OnHomeMenuClick;
import com.sompom.tookitup.listener.OnProductItemClickListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.AdsItem;
import com.sompom.tookitup.model.emun.Status;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.SellerStoreActivity;
import com.sompom.tookitup.newui.dialog.EditProductDialog;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.ProductStoreDataManager;
import com.sompom.tookitup.utils.MailUtils;
import com.sompom.tookitup.utils.OpenWebsiteUtil;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.newviewmodel.ProductStoreFragmentViewModel;
import com.sompom.tookitup.widget.LoadMoreGridLayoutManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by nuonveyo on 6/5/18.
 */

public class ProductStoreFragment extends AbsBindingFragment<FragmentProductStoreBinding>
        implements OnProductItemClickListener, OnHomeMenuClick {

    @Inject
    public ApiService mApiService;
    private ProductStoreFragmentViewModel mViewModel;
    private User mUser;
    private Status mProductStatus = Status.ON_SALE;
    private boolean mIsMe;
    private ProductStoreAdapter mProductStoreAdapter;
    private LoadMoreGridLayoutManager mLoadMoreLayoutManager;
    private boolean mIsShow;
    private int columnCount;

    public static ProductStoreFragment newInstance(User user, Status status) {
        Bundle args = new Bundle();
        args.putParcelable(SharedPrefUtils.USER_ID, user);
        args.putInt(SharedPrefUtils.PRODUCT, status.getStatusProduct());
        ProductStoreFragment fragment = new ProductStoreFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_product_store;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        if (getActivity() != null) {
            columnCount = getActivity().getResources().getInteger(R.integer.number_of_column);
        }
    }

    @Override
    public void onProductItemClick(Adaptive product) {
        if (product instanceof Product) {

        } else {
            if (product instanceof AdsItem) {
                OpenWebsiteUtil.openWebsite(getActivity(), ((AdsItem) product).getClickUrl());
            }
        }
    }

    @Override
    public void onProductItemEdit(Product product) {
        final EditProductDialog editProductDialog = EditProductDialog.newInstance(product, mProductStatus.getStatusProduct());
        editProductDialog.setListener(new OnEditDialogClickListener() {
            @Override
            public void onEditClick(Product product) {
                editProductDialog.dismiss();
                if (mProductStatus == Status.ON_SALE) {
                    mViewModel.onEditClick(product);
                } else {
                    mViewModel.cloneProduct(product, result -> {
                        if (getParentFragment() instanceof SellerStoreFragment) {
                            ((SellerStoreFragment) getParentFragment()).addNewProduct();
                        }
                    });
                }
            }

            @Override
            public void onDeleteClick(Product product) {
                editProductDialog.dismiss();
                mViewModel.onDeleteClick(product);
                if (getParentFragment() instanceof SellerStoreFragment) {
                    ((SellerStoreFragment) getParentFragment()).deleteProduct();
                }

            }
        });
        editProductDialog.show(getChildFragmentManager(), EditProductDialog.TAG);
    }

    public void refreshData() {
        mViewModel.getData();
    }

    @Override
    public void onTabMenuClick() {
        if (mIsShow || !isAdded()) {
            return;
        }

        mIsShow = true;

        if (getArguments() != null) {
            mUser = getArguments().getParcelable(SharedPrefUtils.USER_ID);
            int id = getArguments().getInt(SharedPrefUtils.PRODUCT);
            mProductStatus = Status.getValueFromId(id);
        }

        if (SharedPrefUtils.isLogin(getActivity()) && mUser.getId().equals(SharedPrefUtils.getUserId(getActivity()))) {
            mIsMe = true;
        }

        ProductStoreDataManager dataManager = new ProductStoreDataManager(getActivity(), mApiService);
        mViewModel = new ProductStoreFragmentViewModel(dataManager,
                mProductStatus,
                mUser,
                new ProductStoreFragmentViewModel.OnCallback() {
                    @Override
                    public void onGetProductSuccess(List<Product> list, boolean isCanLoadMore) {
                        Status productStatus;
                        if (TextUtils.equals(mUser.getId(), SharedPrefUtils.getUserId(getActivity()))) {
                            productStatus = Status.ALL;
                        } else {
                            productStatus = mProductStatus;
                        }
                        bindAdapter(list, productStatus, isCanLoadMore);
                    }

                    @Override
                    public void onItemRemoved(Product product) {
                        mProductStoreAdapter.remove(product);
                        if (mProductStoreAdapter.getItemCount() <= 0) {
                            mViewModel.setReadUserDataReady();
                            mViewModel.showNoItemError(getActivity().getString(R.string.error_product_list_404));
                        }
                    }

                    @Override
                    public void onContactSupportEmailClick() {
                        MailUtils.contactSupport(ProductStoreFragment.this);
                    }
                });
        setVariable(BR.viewModel, mViewModel);
        setUploadProductListener();
    }

    private void bindAdapter(List<Product> products, Status status, boolean isCanLoadMore) {
        mLoadMoreLayoutManager = new LoadMoreGridLayoutManager(getActivity(), columnCount);
        mLoadMoreLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == mProductStoreAdapter.getItemCount() - 1
                        && mProductStoreAdapter.canLoadMore()) {
                    return columnCount;
                } else {
                    return 1;
                }
            }
        });
        if (isCanLoadMore) {
            mLoadMoreLayoutManager.setOnLoadMoreListener(() -> {
                getBinding().recyclerview.stopScroll();
                mViewModel.loadMore(new OnCallbackListListener<List<Product>>() {
                    @Override
                    public void onFail(ErrorThrowable ex) {
                        mProductStoreAdapter.setCanLoadMore(false);
                        mProductStoreAdapter.notifyDataSetChanged();
                        mLoadMoreLayoutManager.loadingFinished();
                        mLoadMoreLayoutManager.setOnLoadMoreListener(null);
                    }

                    @Override
                    public void onComplete(List<Product> result, boolean canLoadMore) {
                        mProductStoreAdapter.addLoadMoreData(new ArrayList<>(result));
                        mProductStoreAdapter.setCanLoadMore(canLoadMore);
                        mLoadMoreLayoutManager.loadingFinished();
                    }
                });
            });
        }
        getBinding().recyclerview.setLayoutManager(mLoadMoreLayoutManager);

        mProductStoreAdapter = new ProductStoreAdapter((AbsBaseActivity) getActivity(),
                new ArrayList<>(products),
                mUser.getId(),
                mIsMe,
                true,
                false,
                status.getStatusProduct(),
                ProductStoreFragment.this);
        mProductStoreAdapter.setOnLikeProductListener((product, position) -> mViewModel.checkToPostLike(product));
        mProductStoreAdapter.setCanLoadMore(isCanLoadMore);
        getBinding().recyclerview.setAdapter(mProductStoreAdapter);
    }

    private void setUploadProductListener() {
        if (mProductStatus == Status.ON_SALE && getActivity() instanceof SellerStoreActivity) {
            ((SellerStoreActivity) getActivity()).setOnUploadProductCompleteListener((product, isUpdate) -> {
                if (isUpdate) {
                    int position = mProductStoreAdapter.getIndex(product);
                    mProductStoreAdapter.updateItem(position, product);
                } else {
                    if (mProductStoreAdapter == null || mProductStoreAdapter.getItemCount() == 0) {
                        mViewModel.mIsError.set(false);
                        List<Product> productList = new ArrayList<>();
                        productList.add(product);
                        bindAdapter(productList, mProductStatus, false);
                    } else {
                        mProductStoreAdapter.addFirstIndex(product);
                    }
                    if (getParentFragment() instanceof SellerStoreFragment) {
                        ((SellerStoreFragment) getParentFragment()).addNewProduct();
                    }
                }
            });
        }
    }
}
