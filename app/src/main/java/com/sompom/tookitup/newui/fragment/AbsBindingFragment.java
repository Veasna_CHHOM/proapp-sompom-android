package com.sompom.tookitup.newui.fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sompom.tookitup.viewmodel.AbsBaseViewModel;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

/**
 * Created by He Rotha on 10/20/17.
 */

public abstract class AbsBindingFragment<T extends ViewDataBinding> extends AbsBaseFragment {
    private T mBinding;
    private AbsBaseViewModel mViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false);
        return getBinding().getRoot();
    }

    public T getBinding() {
        return mBinding;
    }

    @LayoutRes
    public abstract int getLayoutRes();

    public void setVariable(int id, Object value) {
        if (value instanceof AbsBaseViewModel) {
            mViewModel = (AbsBaseViewModel) value;
            mViewModel.setAbsBaseFragment(this);
        }
        if (mViewModel instanceof AbsLoadingViewModel) {
            ((AbsLoadingViewModel) mViewModel).mIsLoading.setOnValueSetListener(isSet -> {
                if (isSet) {
                    hideKeyboard();
                }
            });
        }
        getBinding().setVariable(id, value);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mViewModel != null) {
            mViewModel.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mViewModel != null) {
            mViewModel.onPause();
        }
    }

    @Override
    public void onDestroy() {
        if (mViewModel != null) {
            mViewModel.onDestroy();
        }
        super.onDestroy();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mViewModel != null) {
            mViewModel.onCreate();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mViewModel != null) {
            mViewModel.onActivityResult(requestCode, resultCode, data);
        }
    }
}
