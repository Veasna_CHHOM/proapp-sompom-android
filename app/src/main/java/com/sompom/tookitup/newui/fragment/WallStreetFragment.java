package com.sompom.tookitup.newui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.View;

import com.sompom.baseactivity.ResultCallback;
import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.newadapter.HomeTimelineAdapter;
import com.sompom.tookitup.databinding.FragmentWallStreetBinding;
import com.sompom.tookitup.helper.OpenCommentDialogHelper;
import com.sompom.tookitup.injection.game.MoreGameAPIQualifier;
import com.sompom.tookitup.injection.productserialize.ProductSerializeQualifier;
import com.sompom.tookitup.intent.CreateTimelineIntent;
import com.sompom.tookitup.intent.CreateTimelineIntentResult;
import com.sompom.tookitup.intent.newintent.MyCameraIntent;
import com.sompom.tookitup.intent.newintent.ProductFormIntent;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnTabSelectListener;
import com.sompom.tookitup.listener.OnTimelineActiveUserItemClick;
import com.sompom.tookitup.listener.OnTimelineItemButtonClickListener;
import com.sompom.tookitup.model.ActiveUser;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.LifeStream;
import com.sompom.tookitup.model.MoreGameItem;
import com.sompom.tookitup.model.WallStreetAdaptive;
import com.sompom.tookitup.model.emun.ContentType;
import com.sompom.tookitup.model.emun.CreateWallStreetScreenType;
import com.sompom.tookitup.model.emun.FollowItemType;
import com.sompom.tookitup.model.emun.PostContextMenuItemType;
import com.sompom.tookitup.model.emun.TimelinePostItem;
import com.sompom.tookitup.model.result.LoadMoreWrapper;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.dialog.ListFollowDialog;
import com.sompom.tookitup.newui.dialog.PostContextMenuDialog;
import com.sompom.tookitup.newui.dialog.ReportDialog;
import com.sompom.tookitup.newui.dialog.ShareLinkMenuDialog;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.WallStreetDataManager;
import com.sompom.tookitup.utils.BetterSmoothScrollUtil;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.utils.TimelinePostItemUtil;
import com.sompom.tookitup.viewmodel.newviewmodel.WallStreetViewModel;
import com.sompom.tookitup.widget.LoaderMoreLayoutManager;
import com.sompom.videomanager.widget.MediaRecyclerView;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by nuonveyo on 6/22/18.
 */

public class WallStreetFragment extends AbsBindingFragment<FragmentWallStreetBinding>
        implements WallStreetViewModel.OnCallback,
        OnTimelineActiveUserItemClick,
        OnTimelineItemButtonClickListener, OnTabSelectListener {

    @ProductSerializeQualifier
    @Inject
    public ApiService mSerialApiService;
    @MoreGameAPIQualifier
    @Inject
    public ApiService mMoreGameApiService;
    @Inject
    public ApiService mApiService;
    private HomeTimelineAdapter mTimelineAdapter;
    private WallStreetViewModel mViewModel;
    private LoaderMoreLayoutManager mLoadMoreLayoutManager;
    private WallStreetDataManager mDataManager;
    private OpenCommentDialogHelper mCommentDialog;
    private boolean mIsCurrentTabDisplay = true;
    private LoaderMoreLayoutManager.OnLoadMoreCallback mLoadMore = () -> {
        getBinding().recyclerView.stopScroll();
        mViewModel.loadMoreData(new OnCallbackListListener<LoadMoreWrapper<Adaptive>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mTimelineAdapter.setCanLoadMore(false);
                mTimelineAdapter.notifyDataSetChanged();
                mLoadMoreLayoutManager.loadingFinished();
                mLoadMoreLayoutManager.setOnLoadMoreListener(null);
            }

            @Override
            public void onComplete(LoadMoreWrapper<Adaptive> result, boolean canLoadMore) {
                mTimelineAdapter.addLoadMoreData(result.getData());
                mTimelineAdapter.setCanLoadMore(canLoadMore);
                mLoadMoreLayoutManager.loadingFinished();
            }
        });
    };

    public static WallStreetFragment newInstance() {
        Bundle args = new Bundle();
        WallStreetFragment fragment = new WallStreetFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_wall_street;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);

        mLoadMoreLayoutManager = new LoaderMoreLayoutManager(getActivity());
        getBinding().recyclerView.setLayoutManager(mLoadMoreLayoutManager);
        ((SimpleItemAnimator) getBinding().recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        mTimelineAdapter = new HomeTimelineAdapter((AbsBaseActivity) getActivity(), mApiService, this, this);
        mTimelineAdapter.setCanLoadMore(false);
        getBinding().recyclerView.setAdapter(mTimelineAdapter);

        mDataManager = new WallStreetDataManager(getActivity(),
                mApiService,
                mSerialApiService,
                mMoreGameApiService);
        mViewModel = new WallStreetViewModel(getActivity(), mDataManager, this);
        getBinding().setVariable(BR.viewModel, mViewModel);
    }

    public void setEnableQueryOption(boolean isEnable) {
        if (mDataManager != null) {
            mDataManager.resetPagination();
            mDataManager.setEnableQueryOption(isEnable);
        }
        if (mViewModel != null) {
            mViewModel.onRefresh();
        }
    }

    @Override
    public void onGetMoreGameSuccess(MoreGameItem moreGames) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(() -> mTimelineAdapter.setMoreGame(getChildFragmentManager(), moreGames));
        }
    }

    @Override
    public void onGetActiveUserSuccess(ActiveUser activeUser) {
        //TODO: re-enable after live feature done
//        if (getActivity() != null) {
//            getActivity().runOnUiThread(() -> mTimelineAdapter.setActiveUser(activeUser));
//        }
    }

    @Override
    public void onGetTimelineSuccess(List<Adaptive> list, boolean isCanLoadMore) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(() -> {
                mTimelineAdapter.setAdViews(mDataManager.getAdViews());
                mTimelineAdapter.clearTimeline();
                mTimelineAdapter.setCanLoadMore(isCanLoadMore);
                mTimelineAdapter.addLoadMoreData(list);
                if (isCanLoadMore) {
                    mLoadMoreLayoutManager.setOnLoadMoreListener(mLoadMore);
                }
            });
        }
    }

    @Override
    public void onLiveClick() {
        Intent cameraIntent = MyCameraIntent.newLiveInstance(getContext());
        if (getActivity() != null) {
            ((AbsBaseActivity) getActivity()).startActivityForResult(cameraIntent, new ResultCallback() {
                @Override
                public void onActivityResultSuccess(int resultCode, Intent data) {
                    //TODO: handle live click
                }
            });
        }
    }

    @Override
    public void onCommentButtonClick(Adaptive adaptive, int position) {
        onCommentClick(adaptive);
    }

    @Override
    public void onShareButtonClick(Adaptive adaptive, int position) {
        ShareLinkMenuDialog dialog = ShareLinkMenuDialog.newInstance((WallStreetAdaptive) adaptive);
        dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
    }

    @Override
    public void onUserViewMediaClick(Adaptive adaptive) {
        ListFollowDialog dialog = ListFollowDialog.newInstance(adaptive, FollowItemType.USER_ACTION);
        dialog.show(getChildFragmentManager(), ListFollowDialog.TAG);
    }

    @Override
    public void onLikeViewerClick(Adaptive adaptive) {
        ListFollowDialog dialog = ListFollowDialog.newInstance(adaptive, FollowItemType.LIKE_VIEWER);
        dialog.show(getChildFragmentManager(), ListFollowDialog.TAG);
    }

    @Override
    public void onFollowButtonClick(String id, boolean isFollowing) {
        mViewModel.checkTopPostFollow(id, isFollowing);
    }

    @Override
    public void onShowPostContextMenuClick(Adaptive adaptive, User user, int position) {
        if (getActivity() == null || user == null) {
            return;
        }

        TimelinePostItem timelinePostItem = TimelinePostItemUtil.getPostItem(adaptive);
        LifeStream lifeStream = TimelinePostItemUtil.getLifeStream(adaptive);
        Product product = TimelinePostItemUtil.getProduct(adaptive);
        PostContextMenuDialog dialog = PostContextMenuDialog.newInstance();
        dialog.addItem(PostContextMenuItemType.getItems(adaptive, SharedPrefUtils.checkIsMe(getActivity(), user.getId()),
                timelinePostItem));
        dialog.setOnItemClickListener(result -> {
            PostContextMenuItemType itemType = PostContextMenuItemType.getItemType(result);
            if (itemType == PostContextMenuItemType.EDIT_POST_CLICK) {
                if (getActivity() != null) {
                    if (product != null) {
                        getActivity().startActivity(new ProductFormIntent(getContext(), product));
                    } else {
                        CreateTimelineIntent intent = new CreateTimelineIntent(getActivity(),
                                (WallStreetAdaptive) adaptive,
                                CreateWallStreetScreenType.EDIT_POST);
                        ((AbsBaseActivity) getActivity()).startActivityForResult(intent, new ResultCallback() {
                            @Override
                            public void onActivityResultSuccess(int resultCode, Intent data) {
                                CreateTimelineIntentResult result1 = new CreateTimelineIntentResult(data);
                                mTimelineAdapter.updateItem(result1.getLifeStream(), position);
                            }
                        });
                    }
                }
            } else if (itemType == PostContextMenuItemType.UNFOLLOW_CLICK) {
                mViewModel.checkTopPostFollow(user.getId(), false);
                if (adaptive instanceof WallStreetAdaptive && ((WallStreetAdaptive) adaptive).getUser() != null) {
                    ((WallStreetAdaptive) adaptive).getUser().setFollow(false);
                    mTimelineAdapter.update(adaptive);
                }
            } else if (itemType == PostContextMenuItemType.EDIT_PRIVACY_CLICK) {
                showEditPrivacyDialog(result1 -> {
                    //TODO: implement api edit privacy
                });
            } else if (itemType == PostContextMenuItemType.REPORT_CLICK) {
                if (product != null) {
                    ReportDialog reportDialog = ReportDialog.newInstance(ContentType.PRODUCT);
                    reportDialog.setOnReportClickListener(reportRequest -> mViewModel.reportProduct(product.getId(), reportRequest));
                    reportDialog.show(getChildFragmentManager());
                } else {
                    ReportDialog reportDialog = ReportDialog.newInstance(ContentType.POST);
                    reportDialog.setOnReportClickListener(reportRequest -> mViewModel.reportProduct(lifeStream.getId(), reportRequest));
                    reportDialog.show(getChildFragmentManager());
                }
            } else if (itemType == PostContextMenuItemType.TURNOFF_NOTIFICATION_CLICK) {
                mViewModel.unFollowShop(adaptive.getId());
            } else if (itemType == PostContextMenuItemType.DELETE_CLICK) {
                if (product != null) {
                    showMessageDialog(R.string.post_menu_delete_product_title,
                            R.string.post_menu_delete_product_description,
                            R.string.seller_store_button_delete,
                            R.string.change_language_button_cancel,
                            dialog12 -> {
                                mTimelineAdapter.removeData(position);
                                mViewModel.deleteProduct(product);
                            });
                } else {
                    showMessageDialog(R.string.post_menu_delete_post_title,
                            R.string.post_menu_delete_post_description,
                            R.string.seller_store_button_delete,
                            R.string.change_language_button_cancel,
                            dialog12 -> {
                                mTimelineAdapter.removeData(position);
                                mViewModel.deletePost(lifeStream);
                            });
                }
            }
            dialog.dismiss();

        });
        dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
    }

    @Override
    public void onCommentClick(Adaptive adaptive) {
        if (mCommentDialog == null) {
            mCommentDialog = new OpenCommentDialogHelper(getActivity());
        }
        mCommentDialog.show(adaptive, mTimelineAdapter);
    }

    @Override
    public MediaRecyclerView getRecyclerView() {
        return getBinding().recyclerView;
    }

    @Override
    public void onConversationUserClick(User user) {
        //TODO: handle story user click
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mIsCurrentTabDisplay) {
            getBinding().recyclerView.onResume();
            if (getActivity() instanceof AbsBaseActivity) {
                AbsBaseActivity base = (AbsBaseActivity) getActivity();
                if (base.getFloatingVideo() != null && base.getFloatingVideo().isFloating()) {
                    getBinding().recyclerView.setAutoPlay(false);
                    getBinding().recyclerView.setAutoResume(false);
                    base.setOnFloatingVideoRemoveListener(() -> {
                        getBinding().recyclerView.setAutoPlay(true);
                        getBinding().recyclerView.setAutoResume(true);
                        getBinding().recyclerView.resume();
                    });
                }
            }
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if (getBinding() != null) {
            getBinding().recyclerView.onPause();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getActivity() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getActivity()).removeStateChangeListener(mViewModel.getNetworkStateListener());
        }
    }

    public void shouldScrollTop(boolean isPullToRefresh) {
        if (findFirstVisibleItemPosition() != 0) {
            if (isPullToRefresh) {
                mViewModel.onRefresh();
            }
            BetterSmoothScrollUtil.startScroll(getBinding().recyclerView,
                    0,
                    findFirstVisibleItemPosition());
        } else {
            mViewModel.onRefresh();
        }
    }

    public int findFirstVisibleItemPosition() {
        return mLoadMoreLayoutManager.findFirstVisibleItemPosition();
    }

    @Override
    public void onTabSelected(boolean isSelected) {
        mIsCurrentTabDisplay = isSelected;
    }

}
