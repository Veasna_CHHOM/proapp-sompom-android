package com.sompom.tookitup.newui.dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;

import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.DialogPostContextMenuBinding;
import com.sompom.tookitup.listener.OnCompleteListener;
import com.sompom.tookitup.model.PostContextMenuItem;
import com.sompom.tookitup.widget.PostContextMenuLayout;

import java.util.List;

/**
 * Created by nuonveyo on 8/1/18.
 */

public class PostContextMenuDialog extends AbsBindingBottomSheetDialog<DialogPostContextMenuBinding> {
    private OnCompleteListener<Integer> mOnItemClickListener;
    private OnDismissListener mOnDismissListener;
    private List<PostContextMenuItem> mPostContextMenuItems;

    public static PostContextMenuDialog newInstance() {
        return new PostContextMenuDialog();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_post_context_menu;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LinearLayout layout = getBinding().containerLayout;
        layout.removeAllViews();
        for (PostContextMenuItem postContextMenuItem : getPostContextMenuItems()) {
            PostContextMenuLayout postContextMenuLayout = new PostContextMenuLayout(getActivity());
            postContextMenuLayout.setId(postContextMenuItem.getId());
            postContextMenuLayout.setData(postContextMenuItem);
            postContextMenuLayout.setOnClickListener(v -> {
                if (mOnItemClickListener != null) mOnItemClickListener.onComplete(v.getId());
            });
            layout.addView(postContextMenuLayout);
        }
    }

    public List<PostContextMenuItem> getPostContextMenuItems() {
        return mPostContextMenuItems;
    }

    public void addItem(List<PostContextMenuItem> items) {
        mPostContextMenuItems = items;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (mOnDismissListener != null) {
            mOnDismissListener.onDismiss();
        }
        super.onDismiss(dialog);
    }

    public void setOnItemClickListener(OnCompleteListener<Integer> onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    public void setOnDismissListener(OnDismissListener onDismissListener) {
        mOnDismissListener = onDismissListener;
    }

    public interface OnDismissListener {
        void onDismiss();
    }
}
