package com.sompom.tookitup.newui.dialog;

import android.support.annotation.UiThread;
import android.support.design.widget.BottomSheetDialogFragment;

import com.sompom.tookitup.TookitupApplication;
import com.sompom.tookitup.injection.controller.ControllerComponent;
import com.sompom.tookitup.injection.controller.ControllerModule;

/**
 * Created by nuonveyo on 2/21/18.
 */

public class AbsBaseBottomSheetDialog extends BottomSheetDialogFragment {
    public static final String TAG = AbsBaseBottomSheetDialog.class.getName();

    private ControllerComponent mControllerComponent;

    @UiThread
    public ControllerComponent getControllerComponent() {
        if (mControllerComponent == null) {
            mControllerComponent = ((TookitupApplication) getActivity().getApplication())
                    .getApplicationComponent()
                    .newControllerComponent(new ControllerModule(getActivity()));
        }
        return mControllerComponent;
    }
}
