package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.desmond.squarecamera.model.AppTheme;
import com.desmond.squarecamera.utils.ThemeManager;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sompom.tookitup.R;
import com.sompom.tookitup.TookitupApplication;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.utils.BitmapConverter;
import com.sompom.tookitup.utils.SharedPrefUtils;


/**
 * Created by he.rotha on 2/18/16.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback {
    public static final String TAG = MapFragment.class.getName();
    private Product mProduct;

    public static MapFragment getInstance(Product product) {
        final MapFragment fragment = new MapFragment();
        final Bundle bundle = new Bundle();
        bundle.putParcelable(SharedPrefUtils.PRODUCT, product);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_map, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            mProduct = getArguments().getParcelable(SharedPrefUtils.PRODUCT);
        }

        SupportMapFragment supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (supportMapFragment != null) {
            supportMapFragment.getMapAsync(this);
        }
        FlurryAgent.logEvent(getString(R.string.screen_map));
    }

    @Override
    public void onMapReady(final GoogleMap map) {
        if (getActivity() != null) {
            if (ThemeManager.getAppTheme(getActivity()) == AppTheme.Black) {
                map.setMapStyle(MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.google_map_dark));
            }

            final LatLng productLocation = new LatLng(mProduct.getLatitude(), mProduct.getLongitude());
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(productLocation, TookitupApplication.MAP_ZOOM));
            map.addMarker(new MarkerOptions().position(productLocation)
                    .icon(BitmapConverter.bitmapDescriptorFromVector(getActivity(), R.drawable.ic_pin_location_star)));
        }

    }
}
