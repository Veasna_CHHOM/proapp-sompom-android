package com.sompom.tookitup.newui.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;

import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.DialogConfirmationBinding;
import com.sompom.tookitup.viewmodel.newviewmodel.ConfirmationDialogViewModel;

/**
 * Created by he.rotha on 4/28/16.
 */
public class ConfirmationDialog extends AbsBindingFragmentDialog<DialogConfirmationBinding> {
    public static final String TAG = ConfirmationDialog.class.getName();
    private ConfirmationDialogViewModel.DialogModel mModel;

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_confirmation;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        getBinding().setViewModel(new ConfirmationDialogViewModel(this, mModel));
    }

    public static class Builder {
        ConfirmationDialog mConfirmationDialog = new ConfirmationDialog();

        public ConfirmationDialog build(ConfirmationDialogViewModel.DialogModel model) {
            mConfirmationDialog.mModel = model;
            return mConfirmationDialog;
        }
    }

}
