package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.annotation.UiThread;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.desmond.squarecamera.ui.dialog.EditPostPrivacyDialog;
import com.flurry.android.FlurryAgent;
import com.sompom.tookitup.R;
import com.sompom.tookitup.TookitupApplication;
import com.sompom.tookitup.injection.controller.ControllerComponent;
import com.sompom.tookitup.injection.controller.ControllerModule;
import com.sompom.tookitup.listener.OnCompleteListener;
import com.sompom.tookitup.model.PostContextMenuItem;
import com.sompom.tookitup.model.emun.PostContextMenuItemType;
import com.sompom.tookitup.newui.dialog.MessageDialog;
import com.sompom.tookitup.newui.dialog.PostContextMenuDialog;
import com.sompom.tookitup.utils.AttributeConverter;
import com.sompom.tookitup.utils.KeyboardUtil;
import com.sompom.tookitup.utils.SnackBarUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by He Rotha on 10/20/17.
 */

public abstract class AbsBaseFragment extends Fragment {

    public static final String TAG = AbsBaseFragment.class.getName();
    private ControllerComponent mControllerComponent;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final String flurryLogEvent = getFlurryLogEvent();
        if (!TextUtils.isEmpty(flurryLogEvent)) {
            FlurryAgent.logEvent(flurryLogEvent);
        }
    }

    @UiThread
    public ControllerComponent getControllerComponent() {
        if (mControllerComponent == null) {
            mControllerComponent = ((TookitupApplication) getActivity().getApplication())
                    .getApplicationComponent()
                    .newControllerComponent(new ControllerModule(getActivity()));
        }
        return mControllerComponent;
    }

    protected void showKeyboard(EditText editText) {
        editText.setFocusable(true);
        editText.setClickable(true);
        editText.requestFocus();
        KeyboardUtil.showKeyboard(getActivity(), editText);
    }

    protected void hideKeyboard() {
        KeyboardUtil.hideKeyboard(getActivity());
    }

    public void setToolbar(Toolbar toolbar, boolean isShowButtonBack) {
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        setHasOptionsMenu(true);
        if (isShowButtonBack) {
            ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setDisplayShowHomeEnabled(true);
            }
            toolbar.setNavigationIcon(AttributeConverter.convertAttrToDrawable(getActivity(), R.attr.iconBackPressSmall));
        }
    }

    public void showSnackBar(String text) {
        if (getActivity() != null) {
            View view = getActivity().findViewById(android.R.id.content);
            if (view != null) {
                SnackBarUtil.showSnackBar(view, text);
            }
        }
    }

    public void showSnackBar(@StringRes int message) {
        SnackBarUtil.showSnackBar(getView(), message);
    }

    /**
     * for flurry log of screen
     *
     * @return if null, there is no log. otherwise, log text that super class overwrite
     */
    @Nullable
    private String getFlurryLogEvent() {
        return null;
    }

    public void showFragment(Fragment showFragment, Fragment hideFragment) {
        if (hideFragment == null || showFragment == null || hideFragment == showFragment) {
            return;
        }
        getChildFragmentManager()
                .beginTransaction()
                .hide(hideFragment)
                .show(showFragment)
                .commit();
        //make sure fragment know fragment is paused, or resumed
        hideFragment.onPause();
        showFragment.onResume();
    }

    public Fragment getFragment(String tag) {
        return getChildFragmentManager().findFragmentByTag(tag);
    }

    public void removeFragment(Fragment fragment) {
        if (fragment != null && fragment.isAdded()) {
            getChildFragmentManager().beginTransaction().remove(fragment).commit();
        }
    }

    public void setFragment(@IdRes int id, Fragment fragment, String tag) {
        if (!getActivity().isFinishing() && !isFragmentAlreadyReplaced(tag)) {
            getChildFragmentManager()
                    .beginTransaction()
                    .replace(id, fragment, tag)
                    .commit();
        }
    }

    private boolean isFragmentAlreadyReplaced(String tag) {
        Fragment fragment = getChildFragmentManager().findFragmentByTag(tag);
        return fragment != null && fragment.isVisible();
    }

    public void showMessageDialog(@StringRes int title,
                                  @StringRes int description,
                                  @StringRes int ok,
                                  @StringRes int cancel,
                                  View.OnClickListener listener) {
        MessageDialog dialog = new MessageDialog();
        dialog.setTitle(getString(title));
        dialog.setMessage(getString(description));
        dialog.setLeftText(getString(ok), listener);
        dialog.setRightText(getString(cancel), null);
        dialog.show(getChildFragmentManager(), MessageDialog.TAG);
    }

    public void showEditPrivacyDialog(OnCompleteListener<Integer> onClickListener) {
        PostContextMenuDialog dialog = PostContextMenuDialog.newInstance();
        List<PostContextMenuItem> items = new ArrayList<>();
        items.add(new PostContextMenuItem(PostContextMenuItemType.FOLLOWER_CLICK.getId(), R.string.code_follower, R.string.post_menu_followers_title));
        items.add(new PostContextMenuItem(PostContextMenuItemType.EVERYONE_CLICK.getId(), R.string.code_earth, R.string.post_menu_everyone_title));
        dialog.addItem(items);
        dialog.setOnItemClickListener(result -> {
            dialog.dismiss();
            onClickListener.onComplete(result);
        });
        dialog.show(getChildFragmentManager(), EditPostPrivacyDialog.TAG);
    }
}
