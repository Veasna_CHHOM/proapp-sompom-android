package com.sompom.tookitup.newui.dialog;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.View;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.newadapter.ChatImageFullScreenPagerAdapter;
import com.sompom.tookitup.databinding.DialogChatImageFullScreenBinding;
import com.sompom.tookitup.helper.SaveImageHelper;
import com.sompom.tookitup.intent.ForwardIntent;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.PostContextMenuItem;
import com.sompom.tookitup.model.emun.PostContextMenuItemType;
import com.sompom.tookitup.utils.IntentUtil;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.newviewmodel.ChatImageFullScreeDialogViewModel;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by nuonveyo on 8/30/18.
 */

public class ChatImageFullScreenDialog extends AbsBindingFragmentDialog<DialogChatImageFullScreenBinding> {
    private SaveImageHelper mSaveImageHelper;
    private List<Media> mProductMedias;

    public static ChatImageFullScreenDialog newInstance(List<Media> list, int position) {
        Bundle args = new Bundle();
        ArrayList<Media> media = new ArrayList<>(list);
        args.putParcelableArrayList(SharedPrefUtils.DATA, media);
        args.putInt(SharedPrefUtils.ID, position);
        ChatImageFullScreenDialog fragment = new ChatImageFullScreenDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.AppTheme_ChatImageFullScreenDialogStyle);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_chat_image_full_screen;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSaveImageHelper = new SaveImageHelper(getActivity());
        if (getArguments() != null) {
            mProductMedias = getArguments().getParcelableArrayList(SharedPrefUtils.DATA);
            int position = getArguments().getInt(SharedPrefUtils.ID);
            ChatImageFullScreenPagerAdapter adapter = new ChatImageFullScreenPagerAdapter(getActivity(), mProductMedias);
            adapter.setOnClickListener(this::checkToHideToolbar);
            getBinding().viewPager.setAdapter(adapter);
            getBinding().viewPager.setCurrentItem(position);
        }

        ChatImageFullScreeDialogViewModel viewModel = new ChatImageFullScreeDialogViewModel(getActivity(),
                new ChatImageFullScreeDialogViewModel.Callback() {
                    @Override
                    public void onMoreButtonClick() {
                        PostContextMenuDialog dialog = PostContextMenuDialog.newInstance();
                        List<PostContextMenuItem> items = new ArrayList<>();
                        items.add(new PostContextMenuItem(PostContextMenuItemType.SAVE_IMAGE_CLICK.getId(), R.string.code_bookmark, R.string.edit_profile_button_save));
                        items.add(new PostContextMenuItem(PostContextMenuItemType.SHARE_CLICK.getId(), R.string.code_share, R.string.product_detail_button_share));
                        items.add(new PostContextMenuItem(PostContextMenuItemType.FORWARD_CLICK.getId(), R.string.code_share, R.string.chat_forward_title));
                        dialog.addItem(items);
                        dialog.setOnItemClickListener(result -> {
                            PostContextMenuItemType itemType = PostContextMenuItemType.getItemType(result);
                            if (itemType == PostContextMenuItemType.SAVE_IMAGE_CLICK) {
                                dialog.dismiss();
                                mSaveImageHelper.startSave(mProductMedias.get(getCurrentItem()));
                            } else if (itemType == PostContextMenuItemType.SHARE_CLICK) {
                                mSaveImageHelper.download(mProductMedias.get(getCurrentItem()), new SaveImageHelper.OnCallback() {
                                    @Override
                                    public void onFail() {
                                        Timber.e("share image was fail");
                                    }

                                    @Override
                                    public void onSuccess(@NonNull Bitmap resource) {
                                        dialog.dismiss();
                                        IntentUtil.shareIntent(getActivity(), resource);
                                    }
                                });
                            } else if (itemType == PostContextMenuItemType.FORWARD_CLICK) {
                                startActivity(new ForwardIntent(getActivity(),
                                        mProductMedias.get(getBinding().viewPager.getCurrentItem())));
                            }
                        });
                        dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
                    }

                    @Override
                    public void onClick() {
                        dismiss();
                    }
                });
        setVariable(BR.viewModel, viewModel);
    }

    private int getCurrentItem() {
        return getBinding().viewPager.getCurrentItem();
    }

    private void checkToHideToolbar() {
        if (getBinding().backContainer.getVisibility() == View.GONE) {
            getBinding().backContainer.setVisibility(View.VISIBLE);
            getBinding().more.setVisibility(View.VISIBLE);
        } else {
            getBinding().backContainer.setVisibility(View.GONE);
            getBinding().more.setVisibility(View.GONE);
        }
    }
}
