package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.sompom.tookitup.R;
import com.sompom.tookitup.helper.GlideApp;
import com.sompom.tookitup.utils.SharedPrefUtils;

/**
 * Created by he.rotha on 3/9/16.
 */
public class ImageFragment extends Fragment {

    public static ImageFragment getInstance(String product) {
        final ImageFragment fragment = new ImageFragment();
        Bundle bundle = new Bundle();
        bundle.putString(SharedPrefUtils.PRODUCT, product);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_image, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String imageUrl = "";
        if (getArguments() != null && getArguments().containsKey(SharedPrefUtils.PRODUCT)) {
            imageUrl = getArguments().getString(SharedPrefUtils.PRODUCT);
        }

        final ImageView imageView = view.findViewById(R.id.imageview);
        GlideApp.with(ImageFragment.this)
                .load(imageUrl)
                .placeholder(R.drawable.ic_photo_placeholder_400dp)
                .into(imageView);
    }
}
