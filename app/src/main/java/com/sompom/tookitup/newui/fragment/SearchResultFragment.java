package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.newadapter.TimelineAdapter;
import com.sompom.tookitup.databinding.FragmentSearchResultBinding;
import com.sompom.tookitup.injection.productserialize.ProductSerializeQualifier;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnCompleteListListener;
import com.sompom.tookitup.listener.OnProductItemClickListener;
import com.sompom.tookitup.listener.OnTimelineItemButtonClickListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.WallStreetAdaptive;
import com.sompom.tookitup.model.emun.ContentType;
import com.sompom.tookitup.model.emun.FollowItemType;
import com.sompom.tookitup.model.emun.PostContextMenuItemType;
import com.sompom.tookitup.model.emun.TimelinePostItem;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.model.result.WallLoadMoreWrapper;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.dialog.ListFollowDialog;
import com.sompom.tookitup.newui.dialog.PostContextMenuDialog;
import com.sompom.tookitup.newui.dialog.ReportDialog;
import com.sompom.tookitup.newui.dialog.ShareLinkMenuDialog;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.ProductListDataManager;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.utils.TimelinePostItemUtil;
import com.sompom.tookitup.viewmodel.newviewmodel.SearchResultViewModel;
import com.sompom.tookitup.widget.LoaderMoreLayoutManager;
import com.sompom.videomanager.widget.MediaRecyclerView;

import javax.inject.Inject;

/**
 * Created by He Rotha on 8/21/18.
 */
public class SearchResultFragment extends AbsBindingFragment<FragmentSearchResultBinding>
        implements OnProductItemClickListener, OnCompleteListListener<WallLoadMoreWrapper<Adaptive>>, OnTimelineItemButtonClickListener {

    @Inject
    public ApiService mApiService;
    @Inject
    @ProductSerializeQualifier
    public ApiService mProductSerialize;
    private SearchResultViewModel mViewModel;
    private TimelineAdapter mProductStoreAdapter;
    private LoaderMoreLayoutManager mLayoutManager;

    public static SearchResultFragment newInstance() {
        return new SearchResultFragment();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_search_result;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);

        ProductListDataManager manager = new ProductListDataManager(getContext(), null, mProductSerialize, mApiService);

        mViewModel = new SearchResultViewModel(getActivity(), manager, this);
        setVariable(BR.viewModel, mViewModel);
    }


    @Override
    public void onProductItemClick(Adaptive product) {

    }


    @Override
    public void onProductItemEdit(Product product) {

    }

    @Override
    public void onComplete(WallLoadMoreWrapper<Adaptive> result, boolean canLoadMore) {
        mLayoutManager = new LoaderMoreLayoutManager(getContext());
        mLayoutManager.setOnLoadMoreListener(() -> mViewModel.loadMore(new OnCallbackListListener<WallLoadMoreWrapper<Adaptive>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mProductStoreAdapter.setCanLoadMore(false);
                mProductStoreAdapter.notifyDataSetChanged();
                mLayoutManager.loadingFinished();
                mLayoutManager.setOnLoadMoreListener(null);
            }

            @Override
            public void onComplete(WallLoadMoreWrapper<Adaptive> result, boolean canLoadMore) {
                mProductStoreAdapter.addLoadMoreData(result.getData());
                mProductStoreAdapter.setCanLoadMore(canLoadMore);
                mLayoutManager.loadingFinished();
            }
        }));

        getBinding().recyclerView.setLayoutManager(mLayoutManager);
        mProductStoreAdapter = new TimelineAdapter((AbsBaseActivity) getActivity(), mApiService, result.getData(), this);
        mProductStoreAdapter.setCanLoadMore(canLoadMore);
        getBinding().recyclerView.setAdapter(mProductStoreAdapter);
    }

    @Override
    public void onCommentButtonClick(Adaptive adaptive, int position) {

    }

    @Override
    public void onShareButtonClick(Adaptive adaptive, int position) {
        ShareLinkMenuDialog dialog = ShareLinkMenuDialog.newInstance((WallStreetAdaptive) adaptive);
        dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
    }

    @Override
    public void onUserViewMediaClick(Adaptive adaptive) {
        ListFollowDialog dialog = ListFollowDialog.newInstance(adaptive, FollowItemType.USER_ACTION);
        dialog.show(getChildFragmentManager(), ListFollowDialog.TAG);
    }

    @Override
    public void onLikeViewerClick(Adaptive adaptive) {
        ListFollowDialog dialog = ListFollowDialog.newInstance(adaptive, FollowItemType.LIKE_VIEWER);
        dialog.show(getChildFragmentManager(), ListFollowDialog.TAG);
    }

    @Override
    public void onFollowButtonClick(String id, boolean isFollowing) {
        mViewModel.checkToPostFollow(id, isFollowing);
    }

    @Override
    public void onShowPostContextMenuClick(Adaptive adaptive, User user, int position) {
        if (user == null) {
            return;
        }

        TimelinePostItem timelinePostItem = TimelinePostItemUtil.getPostItem(adaptive);
        PostContextMenuDialog dialog = PostContextMenuDialog.newInstance();
        dialog.addItem(PostContextMenuItemType.getItems(adaptive, SharedPrefUtils.checkIsMe(getActivity(), user.getId()), timelinePostItem));
        dialog.setOnItemClickListener(result -> {
            PostContextMenuItemType itemType = PostContextMenuItemType.getItemType(result);
            if (itemType == PostContextMenuItemType.REPORT_CLICK) {
                ReportDialog reportDialog = ReportDialog.newInstance(ContentType.PRODUCT);
                reportDialog.setOnReportClickListener(reportRequest -> mViewModel.reportPost(adaptive.getId(), reportRequest));
                reportDialog.show(getChildFragmentManager());
            } else if (itemType == PostContextMenuItemType.UNFOLLOW_CLICK) {
                mViewModel.checkToPostFollow(user.getId(), false);
                if (adaptive instanceof WallStreetAdaptive && ((WallStreetAdaptive) adaptive).getUser() != null) {
                    ((WallStreetAdaptive) adaptive).getUser().setFollow(false);
                    mProductStoreAdapter.update(adaptive);
                }
            } else if (itemType == PostContextMenuItemType.TURNOFF_NOTIFICATION_CLICK) {
                mViewModel.unFollowShop(adaptive.getId());
            }
            dialog.dismiss();
        });
        dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
    }

    @Override
    public void onCommentClick(Adaptive adaptive) {

    }

    @Override
    public MediaRecyclerView getRecyclerView() {
        return getBinding().recyclerView;
    }
}
