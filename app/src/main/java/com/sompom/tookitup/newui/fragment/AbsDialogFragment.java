package com.sompom.tookitup.newui.fragment;

import android.support.annotation.UiThread;
import android.support.v4.app.DialogFragment;

import com.sompom.tookitup.TookitupApplication;
import com.sompom.tookitup.injection.controller.ControllerComponent;
import com.sompom.tookitup.injection.controller.ControllerModule;

import java.util.Objects;

public abstract class AbsDialogFragment extends DialogFragment {

    private ControllerComponent mControllerComponent;

    @UiThread
    public ControllerComponent getControllerComponent() {
        if (mControllerComponent == null) {
            mControllerComponent = ((TookitupApplication) Objects.requireNonNull(getActivity()).getApplication())
                    .getApplicationComponent()
                    .newControllerComponent(new ControllerModule(getActivity()));
        }
        return mControllerComponent;
    }
}
