package com.sompom.tookitup.newui.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.sompom.tookitup.R;
import com.sompom.tookitup.model.emun.ContentType;
import com.sompom.tookitup.model.request.ReportRequest;
import com.sompom.tookitup.utils.NetworkStateUtil;
import com.sompom.tookitup.utils.SharedPrefUtils;

/**
 * Created by He Rotha on 3/18/19.
 */
public class ReportDialog extends MessageDialog {
    private OnReportClickListener mOnReportClickListener;

    public static ReportDialog newInstance(ContentType re) {
        Bundle args = new Bundle();
        args.putInt(SharedPrefUtils.ID, re.ordinal());
        ReportDialog fragment = new ReportDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (getArguments() == null) {
            return;
        }
        View addView = LayoutInflater.from(getActivity()).inflate(R.layout.layout_report, (ViewGroup) view, false);
        EditText editText = addView.findViewById(R.id.editText);
        getBinding().view.addView(addView);
        setDismissWhenClickOnButtonLeft(false);

        ContentType re = ContentType.values()[getArguments().getInt(SharedPrefUtils.ID)];
        if (re == ContentType.POST) {
            setTitle(getString(R.string.post_menu_report_post_title));
            setMessage(getString(R.string.post_menu_report_post_description));
        } else if (re == ContentType.PRODUCT) {
            setTitle(getString(R.string.product_detail_menu_report_title));
            setMessage(getString(R.string.product_detail_are_you_sure_you_want_to_report_this_product_title));
        }
        setLeftText(getString(R.string.change_language_button_ok), view1 -> {
            if (!NetworkStateUtil.isNetworkAvailable(getActivity())) {
                Toast.makeText(getActivity(), R.string.error_no_internet_connection_message, Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(editText.getText())) {
                editText.setError(getString(R.string.this_field_is_required));
                return;
            }
            ReportRequest reportRequest = new ReportRequest(editText.getText().toString());
            mOnReportClickListener.onReportClick(reportRequest);
            dismiss();
        });
        setRightText(getString(R.string.change_language_button_cancel), null);
        super.onViewCreated(view, savedInstanceState);
    }

    public void setOnReportClickListener(OnReportClickListener onReportClickListener) {
        mOnReportClickListener = onReportClickListener;
    }

    public interface OnReportClickListener {
        void onReportClick(ReportRequest reportRequest);
    }
}
