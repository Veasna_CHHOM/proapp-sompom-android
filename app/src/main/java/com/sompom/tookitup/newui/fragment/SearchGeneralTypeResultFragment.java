package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.desmond.squarecamera.utils.Keys;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.newadapter.SearchGeneralTypeResultAdapter;
import com.sompom.tookitup.databinding.FragmentSearchMessageResultBinding;
import com.sompom.tookitup.listener.OnLikeItemListener;
import com.sompom.tookitup.model.Search;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.SearchGeneralTypeResultDataManager;
import com.sompom.tookitup.utils.KeyboardUtil;
import com.sompom.tookitup.viewmodel.newviewmodel.SearchMessageResultFragmentViewModel;
import com.sompom.tookitup.widget.LoaderMoreLayoutManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by nuonveyo on 7/2/18.
 */

public class SearchGeneralTypeResultFragment extends AbsBindingFragment<FragmentSearchMessageResultBinding>
        implements OnLikeItemListener {
    @Inject
    public ApiService mApiService;

    private SearchMessageResultFragmentViewModel mViewModel;
    private SearchGeneralTypeResultAdapter mAdapter;
    private LoaderMoreLayoutManager mLoaderMoreLayoutManager;

    public static SearchGeneralTypeResultFragment newInstance(ArrayList<Search> searches) {
        Bundle args = new Bundle();
        args.putParcelableArrayList(Keys.DATA, searches);
        SearchGeneralTypeResultFragment fragment = new SearchGeneralTypeResultFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_search_message_result;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);

        List<Search> data = null;
        if (getArguments() != null && getArguments().containsKey(Keys.DATA)) {
            data = getArguments().getParcelableArrayList(Keys.DATA);
        }

        if (data == null) {
            data = Collections.emptyList();
        }

        mLoaderMoreLayoutManager = new LoaderMoreLayoutManager(getActivity());
//        mLoaderMoreLayoutManager.setOnLoadMoreListener(() ->
//                mViewModel.loadMore(new OnCallbackListListener<List<SearchGeneralTypeResult>>() {
//                    @Override
//                    public void onFail(ErrorThrowable ex) {
//                        mAdapter.setCanLoadMore(false);
//                        mAdapter.notifyDataSetChanged();
//                        mLoaderMoreLayoutManager.loadingFinished();
//                        mLoaderMoreLayoutManager.setOnLoadMoreListener(null);
//                    }
//
//                    @Override
//                    public void onComplete(List<SearchGeneralTypeResult> result, boolean canLoadMore) {
//                        mAdapter.addLoadMoreData(result);
//                        mAdapter.setCanLoadMore(canLoadMore);
//                        mLoaderMoreLayoutManager.loadingFinished();
//                    }
//                }));
        getBinding().recyclerView.setLayoutManager(mLoaderMoreLayoutManager);
        getBinding().recyclerView.setOnTouchListener((view1, motionEvent) -> {
            KeyboardUtil.hideKeyboard(getActivity());
            view1.performClick();
            return false;
        });

        SearchGeneralTypeResultDataManager dataManager = new SearchGeneralTypeResultDataManager(getActivity(), mApiService);
        mViewModel = new SearchMessageResultFragmentViewModel(dataManager);

        mAdapter = new SearchGeneralTypeResultAdapter(getActivity(), data, SearchGeneralTypeResultFragment.this);
        mAdapter.setCanLoadMore(false);
        getBinding().recyclerView.setAdapter(mAdapter);
    }


    @Override
    public void onFollowClick(User user, boolean isFollow) {
        mViewModel.onFollowClick(user, isFollow);
    }

    @Override
    public void onNotifyItem() {
        if (mAdapter != null) {
            mAdapter.notifyData();
        }
    }

    public void refresh(List<Search> searches) {
        if (mAdapter != null) {
            mAdapter.setData(searches);
        }
    }
}
