package com.sompom.tookitup.newui;

import android.os.Bundle;

import com.sompom.tookitup.newui.fragment.SuggestionFragment;

public class SuggestionActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(SuggestionFragment.newInstance(), SuggestionFragment.TAG);
    }
}
