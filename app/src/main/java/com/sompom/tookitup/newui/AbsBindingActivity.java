package com.sompom.tookitup.newui;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.sompom.tookitup.viewmodel.AbsBaseViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by He Rotha on 10/20/17.
 */

public abstract class AbsBindingActivity<T extends ViewDataBinding> extends AbsBaseActivity {
    private T mBinding;
    private List<AbsBaseViewModel> mViewModels = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int layout = getLayoutRes();
        mBinding = DataBindingUtil.setContentView(this, layout);
    }

    @LayoutRes
    public abstract int getLayoutRes();

    public T getBinding() {
        return mBinding;
    }

    public void setVariable(int id, Object value) {
        if (value instanceof AbsBaseViewModel) {
            mViewModels.add((AbsBaseViewModel) value);
        }
        getBinding().setVariable(id, value);
    }

    @Override
    public void onResume() {
        super.onResume();
        for (AbsBaseViewModel viewModel : mViewModels) {
            viewModel.onResume();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        for (AbsBaseViewModel viewModel : mViewModels) {
            viewModel.onPause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        for (AbsBaseViewModel viewModel : mViewModels) {
            viewModel.onDestroy();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (AbsBaseViewModel viewModel : mViewModels) {
            viewModel.onActivityResult(requestCode, resultCode, data);
        }
    }
}
