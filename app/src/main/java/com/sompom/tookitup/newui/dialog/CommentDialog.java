package com.sompom.tookitup.newui.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.DialogCommentBinding;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.listener.OnCommentDialogClickListener;
import com.sompom.tookitup.listener.OnCompleteListener;
import com.sompom.tookitup.model.result.Comment;
import com.sompom.tookitup.newui.fragment.PopUpCommentFragment;
import com.sompom.tookitup.newui.fragment.ReplyCommentFragment;
import com.sompom.tookitup.utils.SharedPrefUtils;

/**
 * Created by nuonveyo on 7/20/18.
 */

public class CommentDialog extends AbsBindingFragmentDialog<DialogCommentBinding> implements OnCommentDialogClickListener {
    private PopUpCommentFragment mPopUpCommentFragment;
    private ReplyCommentFragment mReplyCommentFragment;
    private int mPosition;
    private CommentType mCommentType;
    private OnCloseReplyCommentFragmentListener mOnNotifyCommentItemChangeListener;
    private OnClickListener mOnDismissDialogListener;

    public static CommentDialog newInstance(String itemID) {
        Bundle args = new Bundle();
        args.putString(SharedPrefUtils.ID, itemID);
        CommentDialog fragment = new CommentDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public static CommentDialog newInstance(String itemID, Comment comment) {
        Bundle args = new Bundle();
        args.putInt(SharedPrefUtils.STATUS, CommentType.REPLY.getId());
        args.putParcelable(SharedPrefUtils.DATA, comment);
        args.putString(SharedPrefUtils.ID, itemID);

        CommentDialog fragment = new CommentDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_comment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (getActivity() == null) {
            return super.onCreateDialog(savedInstanceState);
        }

        return new Dialog(getActivity(), getTheme()) {
            @Override
            public void onBackPressed() {
                Fragment fragment = getFragment(ReplyCommentFragment.TAG);
                if (fragment instanceof ReplyCommentFragment && fragment.isVisible()) {
                    if (!((ReplyCommentFragment) fragment).isCloseSearchGifView()) {
                        if (mCommentType == CommentType.REPLY) {
                            super.onBackPressed();
                        } else {
                            backToCommentFragment();
                        }
                    }
                } else {
                    PopUpCommentFragment popUpCommentFragment = (PopUpCommentFragment) getFragment(PopUpCommentFragment.TAG);
                    boolean isCloseGifView = (popUpCommentFragment).isCloseSearchGifView();
                    if (!isCloseGifView) {
                        super.onBackPressed();
                    }
                }
            }
        };
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            String itemId = getArguments().getString(SharedPrefUtils.ID);
            mCommentType = CommentType.getType(getArguments().getInt(SharedPrefUtils.STATUS, CommentType.NORMAL.getId()));
            if (mCommentType == CommentType.REPLY) {
                Comment comment = getArguments().getParcelable(SharedPrefUtils.DATA);
                mReplyCommentFragment = ReplyCommentFragment.newInstance(comment);
                mReplyCommentFragment.setOnCommentDialogClickListener(this);
                setFragment(R.id.containerCommentView,
                        mReplyCommentFragment,
                        ReplyCommentFragment.TAG);

            } else {
                mReplyCommentFragment = ReplyCommentFragment.newInstance(itemId);
                mReplyCommentFragment.setOnCommentDialogClickListener(this);
                mPopUpCommentFragment = PopUpCommentFragment.newInstance(itemId);
                mPopUpCommentFragment.setCommentDialogClickListener(this);

                getChildFragmentManager().beginTransaction()
                        .add(R.id.containerCommentView, mReplyCommentFragment, ReplyCommentFragment.TAG)
                        .hide(mReplyCommentFragment)
                        .add(R.id.containerCommentView, mPopUpCommentFragment, PopUpCommentFragment.TAG)
                        .commit();
            }
        }
    }

    @Override
    public void onReplaceCommentFragment(Comment comment, int position) {
        mPosition = position;
        final FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right,
                R.anim.exit_to_left);
        showFragmentWithAnimation(fragmentTransaction, mReplyCommentFragment, mPopUpCommentFragment);
        mReplyCommentFragment.setData(comment, position);
    }

    @Override
    public void onDismissDialog(boolean isDismiss) {
        if (isDismiss) {
            dismiss();
        } else {
            if (mCommentType == CommentType.REPLY) {
                dismiss();
            } else {
                backToCommentFragment();
            }
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mOnDismissDialogListener != null) {
            mOnDismissDialogListener.onClick();
        }
    }

    @Override
    public void onDismissAndRemoveItem(int position) {
        if (mPopUpCommentFragment != null) {
            backToCommentFragment();
            mPopUpCommentFragment.notifyItemRemove(position);
        } else {
            dismiss();
            if (mOnNotifyCommentItemChangeListener != null) {
                mOnNotifyCommentItemChangeListener.onDeleteComment();
            }
        }
    }

    @Override
    public void onNotifyCommentItemChange(Comment comment) {
        if (mPopUpCommentFragment != null) {
            mPopUpCommentFragment.notifyItemChanged(mPosition, comment);
        }

        if (mOnNotifyCommentItemChangeListener != null) {
            mOnNotifyCommentItemChangeListener.onComplete(comment);
        }
    }

    private void backToCommentFragment() {
        final FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_left,
                R.anim.exit_to_right, 0, 0);
        showFragmentWithAnimation(fragmentTransaction,
                mPopUpCommentFragment,
                mReplyCommentFragment);
    }

    public void setOnNotifyCommentItemChangeListener(OnCloseReplyCommentFragmentListener onNotifyCommentItemChangeListener) {
        mOnNotifyCommentItemChangeListener = onNotifyCommentItemChangeListener;
    }

    public void setOnDismissDialogListener(OnClickListener onDismissDialogListener) {
        mOnDismissDialogListener = onDismissDialogListener;
    }


    public int getNumberOfAddComment() {
        if (mPopUpCommentFragment != null) {
            return mPopUpCommentFragment.getNumberOfAddComment();
        }
        return 0;
    }

    public enum CommentType {
        NORMAL(1),
        REPLY(2);

        private final int mId;

        CommentType(int id) {
            this.mId = id;
        }

        public static CommentType getType(int id) {
            for (CommentType commentType : CommentType.values()) {
                if (commentType.getId() == id) {
                    return commentType;
                }
            }
            return NORMAL;
        }

        public int getId() {
            return mId;
        }
    }

    public interface OnCloseReplyCommentFragmentListener extends OnCompleteListener<Comment> {
        void onDeleteComment();
    }
}
