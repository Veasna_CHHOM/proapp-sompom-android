package com.sompom.tookitup.newui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.sompom.tookitup.helper.FacebookSMSValidation;
import com.sompom.tookitup.newui.fragment.SignUpFragment;

public class SignUpActivity extends AbsDefaultActivity {

    //No longer use
//    private FacebookSMSValidation mSMSValidationUtil;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(SignUpFragment.newInstance(), SignUpFragment.TAG);

        //No need more
//        FacebookSMSValidation.logOut();
//        if (BuildConfig.DEBUG) {
//            AlertDialog.Builder alert = new AlertDialog.Builder(this);
//            final EditText edittext = new EditText(this);
//            alert.setMessage("input debug phone number");
//            edittext.setHint("input debug phone number");
//            edittext.setInputType(InputType.TYPE_CLASS_PHONE);
//            alert.setView(edittext);
//
//            alert.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int whichButton) {
//                    String YouEditTextValue = edittext.getText().toString();
//                    if (!TextUtils.isEmpty(YouEditTextValue)) {
//                        setFragment(SignUpFragment.newInstance(new PhoneLoginRequest(YouEditTextValue, null)), SignUpFragment.TAG);
//                    }
//                }
//            });
//
//            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int whichButton) {
//                    // what ever you want to do with No option.
//                    finish();
//                }
//            });
//
//            alert.show();
//
//        } else {
//            mSMSValidationUtil = new FacebookSMSValidation(this);
//            mSMSValidationUtil.doValidation(new FacebookSMSValidation.OnSMSValidationResult() {
//
//                @Override
//                public void onSuccess(String phoneNumber, String token) {
//                    setFragment(SignUpFragment.newInstance(new PhoneLoginRequest(phoneNumber, token)), SignUpFragment.TAG);
//                }
//
//                @Override
//                public void onFail() {
//                    finish();
//                }
//
//                @Override
//                public void onCancel() {
//                    finish();
//                }
//            });
//        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        No longer use
//        if (mSMSValidationUtil != null) {
//            mSMSValidationUtil.onActivityResult(requestCode, resultCode, data);
//        }
    }
}
