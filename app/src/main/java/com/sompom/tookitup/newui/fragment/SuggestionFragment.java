package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.newadapter.ViewPagerAdapter;
import com.sompom.tookitup.databinding.FragmentSuggestionBinding;
import com.sompom.tookitup.model.emun.SuggestionTab;
import com.sompom.tookitup.utils.AttributeConverter;

/**
 * Created by nuonveyo on 7/20/18.
 */

public class SuggestionFragment extends AbsBindingFragment<FragmentSuggestionBinding> {
    private ViewPagerAdapter mViewPagerAdapter;

    public static SuggestionFragment newInstance() {
        return new SuggestionFragment();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_suggestion;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        initTab();

        getBinding().backPressButton.setOnClickListener(v -> getActivity().onBackPressed());
        getBinding().doneTextView.setOnClickListener(this::onClick);

    }

    private void initTab() {
        getBinding().title.setText(SuggestionTab.People.getTitle());

        mViewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        mViewPagerAdapter.addFragment(TabSuggestionFragment.newInstance(SuggestionTab.People),
                getActivity().getString(SuggestionTab.People.getTabTitle()));

        mViewPagerAdapter.addFragment(TabSuggestionFragment.newInstance(SuggestionTab.Store),
                getActivity().getString(SuggestionTab.Store.getTabTitle()));

//        mViewPagerAdapter.addFragment(TabSuggestionFragment.newInstance(SuggestionTab.Live),
//                getActivity().getString(SuggestionTab.Live.getTabTitle()));

        getBinding().viewPager.setOffscreenPageLimit(mViewPagerAdapter.getCount());
        getBinding().viewPager.setAdapter(mViewPagerAdapter);
        getBinding().tabs.setTabTextColors(ContextCompat.getColor(getActivity(), R.color.darkGrey),
                AttributeConverter.convertAttrToColor(getActivity(), R.attr.colorContrast));
        getBinding().tabs.setupWithViewPager(getBinding().viewPager);

        getBinding().tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                getBinding().title.setText(SuggestionTab.getSuggestionTab(tab.getPosition()).getTitle());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void onClick(View v) {
        getActivity().finish();
    }
}
