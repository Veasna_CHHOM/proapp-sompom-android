package com.sompom.tookitup.newui.fragment;


import android.app.Activity;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.newadapter.SearchAddressAdapter;
import com.sompom.tookitup.databinding.FragmentSearchAddressBinding;
import com.sompom.tookitup.helper.CheckRequestLocationCallbackHelper;
import com.sompom.tookitup.injection.google.GoogleQualifier;
import com.sompom.tookitup.intent.SelectAddressIntentResult;
import com.sompom.tookitup.model.SearchAddressResult;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.services.ApiGoogle;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.SelectAddressDataManager;
import com.sompom.tookitup.utils.KeyboardUtil;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.newviewmodel.SearchAddressFragmentViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by nuonveyo on 6/11/18.
 */

public class SearchAddressFragment extends AbsBindingFragment<FragmentSearchAddressBinding> {
    public static final String TAG = SearchAddressFragment.class.getName();
    @Inject
    public ApiService mApiService;
    @Inject
    @GoogleQualifier
    public ApiGoogle mApiGoogle;
    private SearchAddressAdapter mAdapter;
    private SearchAddressResult mSearchAddressResult;
    private SearchAddressFragmentViewModel mViewModel;

    public static SearchAddressFragment newInstance(SearchAddressResult searchAddressResult) {

        Bundle args = new Bundle();
        args.putParcelable(SharedPrefUtils.DATA, searchAddressResult);
        SearchAddressFragment fragment = new SearchAddressFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_search_address;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        if (getArguments() != null) {
            mSearchAddressResult = getArguments().getParcelable(SharedPrefUtils.DATA);
        }

        getBinding().recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        getBinding().recyclerview.setOnTouchListener((v, event) -> {
            KeyboardUtil.hideKeyboard(getActivity());
            v.performClick();
            return false;
        });
        mAdapter = new SearchAddressAdapter(new ArrayList<>(),
                searchAddressResult -> {
                    if (searchAddressResult.getLocations() == null) {
                        mViewModel.searchDetailAddress(searchAddressResult, this::setSearchAddressResult);
                    } else {
                        setSearchAddressResult(searchAddressResult);
                    }
                });
        getBinding().recyclerview.setAdapter(mAdapter);

        if (getActivity() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getActivity()).requestLocation(new CheckRequestLocationCallbackHelper(getActivity(), location -> {
                SelectAddressDataManager.Strategy strategy;
//                if (!location.isEmpty() && location.getCountry().equalsIgnoreCase("kh")) {
//                    strategy = SelectAddressDataManager.Strategy.Google;
//                } else {
                strategy = SelectAddressDataManager.Strategy.Mapbox;
//                }

                SelectAddressDataManager selectAddressDataManager
                        = new SelectAddressDataManager(getActivity(), mApiService, mApiGoogle, strategy);
                mViewModel = new SearchAddressFragmentViewModel(selectAddressDataManager,
                        mSearchAddressResult,
                        list -> mAdapter.setData(list));
                setVariable(BR.viewModel, mViewModel);
            }));
        }
    }

    private void setSearchAddressResult(SearchAddressResult addressResult) {
        if (getActivity() instanceof AbsBaseActivity) {
            if (addressResult.getLocations() != null && TextUtils.isEmpty(addressResult.getLocations().getCountry())) {
                Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
                List<Address> addresses;
                try {
                    addresses = geocoder.getFromLocation(addressResult.getLocations().getLatitude(), addressResult.getLocations().getLongtitude(), 1);

                    if (addresses != null && !addresses.isEmpty()) {
                        String countryCode = addresses.get(0).getCountryCode();
                        addressResult.getLocations().setCountry(countryCode);
                    }

                } catch (Exception ignored) {
                    Timber.e(ignored.toString());
                }


            }

            SelectAddressIntentResult intentResult = new SelectAddressIntentResult(addressResult);
            getActivity().setResult(Activity.RESULT_OK, intentResult);
            getActivity().finish();
        }
    }
}
