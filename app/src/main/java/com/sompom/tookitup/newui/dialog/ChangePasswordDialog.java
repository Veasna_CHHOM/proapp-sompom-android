package com.sompom.tookitup.newui.dialog;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.PartialChangePasswordLayoutBinding;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.ChangePasswordDataManager;
import com.sompom.tookitup.utils.SnackBarUtil;
import com.sompom.tookitup.viewmodel.newviewmodel.ChangePasswordViewModel;

import javax.inject.Inject;

public class ChangePasswordDialog extends MessageDialog implements ChangePasswordViewModel.ChangePasswordViewModelListener {

    private ChangePasswordViewModel mViewModel;
    @Inject
    public ApiService mApiService;
    private View mRoot;
    private ChangePasswordDialogListener mListener;

    public static ChangePasswordDialog newInstance() {

        Bundle args = new Bundle();

        ChangePasswordDialog fragment = new ChangePasswordDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setCancelable(false);
        mRoot = view;
        getControllerComponent().inject(this);
        final LayoutInflater inflater = LayoutInflater.from(getContext());
        PartialChangePasswordLayoutBinding binding = DataBindingUtil.inflate(inflater, R.layout.partial_change_password_layout,
                null,
                true);

        setDismissWhenClickOnButtonLeft(false);
        mViewModel = new ChangePasswordViewModel(requireContext(),
                new ChangePasswordDataManager(requireContext(), mApiService),
                this);

        binding.setVariable(BR.viewModel, mViewModel);

        setTitle(getString(R.string.change_password));
        setMessage(getString(R.string.set_your_new_password));
        setLeftText(getString(R.string.search_address_button_confirm), dialog -> {
            if (mViewModel.isReadyToChangePassword()) {
                mViewModel.requestChangePassword();
            }
        });
        setRightText(getString(R.string.change_language_button_cancel), null);
        getBinding().view.addView(binding.getRoot());
        super.onViewCreated(view, savedInstanceState);
    }

    public void setListener(ChangePasswordDialogListener listener) {
        mListener = listener;
    }

    @Override
    public void onChangePasswordFailed(String error) {
        SnackBarUtil.showSnackBar(mRoot, error);
    }

    @Override
    public void onChangePasswordSuccess() {
        dismiss();
        if (mListener != null) {
            mListener.onChangePasswordSuccess();
        }
    }

    public interface ChangePasswordDialogListener {
        void onChangePasswordSuccess();
    }
}
