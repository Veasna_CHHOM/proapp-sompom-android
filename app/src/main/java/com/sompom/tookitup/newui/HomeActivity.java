package com.sompom.tookitup.newui;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.ads.MobileAds;
import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.upload.FloatingVideoService;
import com.sompom.tookitup.databinding.ActivityDefaultBinding;
import com.sompom.tookitup.intent.DeeplinkIntent;
import com.sompom.tookitup.intent.newintent.HomeIntent;
import com.sompom.tookitup.intent.newintent.ProductDetailIntent;
import com.sompom.tookitup.intent.newintent.SellerStoreIntent;
import com.sompom.tookitup.intent.newintent.TimelineDetailIntent;
import com.sompom.tookitup.model.emun.DeeplinkType;
import com.sompom.tookitup.newui.fragment.HomeFragment;

public class HomeActivity extends AbsSoundControllerActivity<ActivityDefaultBinding> {

    @Override
    public int getLayoutRes() {
        return R.layout.activity_default;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MobileAds.initialize(this, getString(R.string.admob_app_id));

        //stay in home
        boolean isShouldOpenConversation = isShouldOpenConversation(getIntent());

        HomeFragment fragment = HomeFragment.newInstance(isShouldOpenConversation);
        setFragment(R.id.containerView,
                fragment,
                HomeFragment.TAG);

        FloatingVideoService.init(this);
        //end stay in home


        //Move to Splash activity
//        DeeplinkIntent intent = new DeeplinkIntent(getIntent());
//        DeeplinkType deepLink = intent.getDeeplinkType();
//        if (deepLink != DeeplinkType.None) {
//            String id = intent.getId();
//            if (deepLink == DeeplinkType.Feed) {
//                startActivity(new TimelineDetailIntent(getThis(), id));
//            } else if (deepLink == DeeplinkType.Product) {
//                startActivity(new ProductDetailIntent(getThis(), id, false, false));
//            } else if (deepLink == DeeplinkType.User) {
//                startActivity(new SellerStoreIntent(getThis(), id));
//            }
//        }
    }

    @Override
    public void onBackPressed() {
        HomeFragment fragment = (HomeFragment) getFragment(HomeFragment.TAG);
        if (fragment != null && fragment.onBackPress()) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        shouldOpenConversation(intent);
    }

    private void shouldOpenConversation(Intent intent) {
        if (isShouldOpenConversation(intent)) {

            HomeFragment fragment = (HomeFragment) getFragment(HomeFragment.TAG);
            if (fragment != null) {
                fragment.checkGoToConversationTab();
            }

            //no need to open Chat Screen anymore, cos notification already start it
        }
    }

    public void shouldOpenConversation() {
        shouldOpenConversation(getIntent());
    }

    private boolean isShouldOpenConversation(Intent intent) {
        if (intent != null) {
            HomeIntent homeIntent = new HomeIntent(intent);
            HomeIntent.Redirection direction = homeIntent.getRedirection();
            return direction != null && direction == HomeIntent.Redirection.Conversation;
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FloatingVideoService.stop(this);

    }
}
