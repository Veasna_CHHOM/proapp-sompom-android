/*
 * Copyright (c) 2017 Nam Nguyen, nam@ene.im
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sompom.tookitup.newui.dialog;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.PlayerView;
import com.sompom.tookitup.R;
import com.sompom.tookitup.helper.CustomExoPlayerEventListener;
import com.sompom.tookitup.helper.ScreenOrientationHelper;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.videomanager.helper.ExoPlayerBuilder;

import timber.log.Timber;

/**
 * @author eneim | 6/21/17.
 */

// TODO: from multi window mode to normal mode --> how the "state saving" flow goes?
public class BigPlayerFragment extends AbsBasePlayVideFullScreenFragmentDialog {
    public static final String TAG = BigPlayerFragment.class.getName();
    private int mVideoOrder;
    private boolean mIsDestroyWhenPortraitMode = false;
    private PlayerView mExoPlayerView;
    private SimpleExoPlayer mPlayer;
    private Callback mCallback;
    private ScreenOrientationHelper mOrientationHelper;
    private int mOrientation;
    private boolean mAutoPlay;
    private boolean mPlayWhenReadyStatus;
    private Media mMedia;

    public static BigPlayerFragment newInstance(int order,
                                                @NonNull Media media,
                                                boolean isDestroyWhenPortraitMode) {
        BigPlayerFragment fragment = new BigPlayerFragment();
        Bundle args = new Bundle();
        args.putInt(SharedPrefUtils.ID, order);
        args.putParcelable(SharedPrefUtils.DATA, media);
        args.putBoolean(SharedPrefUtils.TITLE, isDestroyWhenPortraitMode);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getTheme() {
        return R.style.FullScreenPlayer;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getParentFragment() != null && getParentFragment() instanceof Callback) {
            this.mCallback = (Callback) getParentFragment();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return inflater.inflate(R.layout.fragment_dialog_facebook_bigplayer, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            mVideoOrder = getArguments().getInt(SharedPrefUtils.ID);
            mMedia = getArguments().getParcelable(SharedPrefUtils.DATA);
            mIsDestroyWhenPortraitMode = getArguments().getBoolean(SharedPrefUtils.TITLE);
        }
        if (mMedia == null) {
            throw new IllegalArgumentException("Require a Video item.");
        }

        mExoPlayerView = view.findViewById(R.id.big_player);
        mExoPlayerView.setKeepScreenOn(true);

        if (mMedia.getWidth() > mMedia.getHeight()) {
            if (getActivity() != null) {
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        }
        hideSystemUI();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mOrientationHelper == null) {
            mOrientationHelper = new ScreenOrientationHelper(getActivity());
            mOrientationHelper.setCallback(orientation -> {
                if (getActivity() != null) {
                    mOrientation = orientation;
                    if (orientation == ScreenOrientationHelper.ORIENTATION_LANDSCAPE_REVERSE) {
                        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
                    } else if (orientation == ScreenOrientationHelper.ORIENTATION_LANDSCAPE) {
                        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    } else if (orientation == ScreenOrientationHelper.ORIENTATION_PORTRAIT_REVERSE) {
                        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT);
                    } else {
                        if (mIsDestroyWhenPortraitMode) {
                            dismiss();
                        } else {
                            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                        }
                    }
                }
            });
        }
        mOrientationHelper.enable();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mOrientationHelper != null) {
            mOrientationHelper.disable();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        initNewMedia();
        mPlayer.addListener(new CustomExoPlayerEventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                super.onPlayerStateChanged(playWhenReady, playbackState);
                if (playbackState != Player.STATE_ENDED) {
                    // For check play video pause or not when rotate screen
                    if (mOrientation != ScreenOrientationHelper.ORIENTATION_PORTRAIT) {
                        mAutoPlay = playWhenReady;
                    }

                    // For check play video pause or not when click on full screen icon
                    if (!mIsDestroyWhenPortraitMode) {
                        mAutoPlay = mPlayWhenReadyStatus;
                    }
                    mPlayWhenReadyStatus = playWhenReady;
                }
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mPlayer != null) {
            mPlayer.stop();
        }
    }

    @Override
    public void onDestroyView() {
        if (getActivity() != null) {
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        if (mCallback != null) {
            mMedia.setTime(mPlayer.getCurrentPosition());
            mCallback.onBigPlayerDestroyed(mVideoOrder, mAutoPlay, mMedia);
        }

        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
        super.onDestroyView();
    }

    // This snippet hides the system bars.
    private void hideSystemUI() {
        if (getView() != null) {
            int flags = View.SYSTEM_UI_FLAG_IMMERSIVE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;

            getView().setSystemUiVisibility(flags);
            getView().setOnSystemUiVisibilityChangeListener(visibility -> {
                if (visibility == View.SYSTEM_UI_FLAG_VISIBLE) {
                    new Handler().postDelayed(() -> {
                        if (getView() != null) {
                            getView().setSystemUiVisibility(flags);
                        }
                    }, 2000);
                }
            });
        }
    }

    private void initPlayer() {
        if (mPlayer != null) {
            mPlayer.stop();
        }
        mExoPlayerView.setPlayer(null);
        mPlayer = setupPlayer(getActivity(), mMedia.getUrl());
        mPlayer.setVolume(1f);
    }

    private void initNewMedia() {
        initPlayer();
        mPlayer.setPlayWhenReady(true);
        mPlayer.seekTo(mMedia.getTime());
    }

    private SimpleExoPlayer setupPlayer(final Context context, String url) {
        mExoPlayerView.setUseController(true);
        Timber.e("url: %s", url);
        SimpleExoPlayer exoPlayer = ExoPlayerBuilder.build(context, url, isPlaying -> mMedia.setPause(!isPlaying));
        mExoPlayerView.setPlayer(exoPlayer);
        return exoPlayer;
    }

    public interface Callback {
        void onBigPlayerDestroyed(int position, boolean isAutoPlay, Media media);
    }
}
