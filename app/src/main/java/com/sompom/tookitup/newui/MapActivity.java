package com.sompom.tookitup.newui;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.ActivityMapBinding;
import com.sompom.tookitup.intent.MapIntent;
import com.sompom.tookitup.newui.fragment.MapFragment;

public class MapActivity extends AbsBindingActivity<ActivityMapBinding> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MapIntent intent = new MapIntent(getIntent());

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_view, MapFragment.getInstance(intent.getProduct()),
                            MapFragment.TAG).commit();
        }
        //Init toolbar
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        final TextView title = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        title.setText(R.string.search_address_map_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_map;
    }
}
