package com.sompom.tookitup.newui.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.desmond.squarecamera.model.AppTheme;
import com.desmond.squarecamera.utils.ThemeManager;
import com.sompom.tookitup.R;

/**
 * Created by He Rotha on 6/19/18.
 */
public class ThemeDialog extends MessageDialog {
    private OnThemeChooseListener mOnThemeChooseListener;
    private AppTheme mCurrentTheme;


    public void setOnThemeChooseListener(OnThemeChooseListener onThemeChooseListener) {
        mOnThemeChooseListener = onThemeChooseListener;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        setTitle(getString(R.string.edit_profile_theme));
        setMessage(getString(R.string.edit_profile_change_theme_description));
        mCurrentTheme = ThemeManager.getAppTheme(getContext());
        final String[] currencies = new String[AppTheme.values().length];
        int selectPosition = 0;
        for (int i = 0; i < currencies.length; i++) {
            if (getContext() == null) {
                break;
            }
            currencies[i] = getContext().getString(AppTheme.values()[i].getStringRes());
            if (mCurrentTheme == AppTheme.values()[i]) {
                selectPosition = i;
            }
        }

        setSingleChoiceItems(currencies, selectPosition, (dialog, which) -> mCurrentTheme = AppTheme.values()[which]);
        setLeftText(getString(R.string.change_language_button_ok), view1 -> mOnThemeChooseListener.onSelect(mCurrentTheme));
        setRightText(getString(R.string.change_language_button_cancel), null);

        super.onViewCreated(view, savedInstanceState);

    }

    public interface OnThemeChooseListener {
        void onSelect(AppTheme currency);
    }
}
