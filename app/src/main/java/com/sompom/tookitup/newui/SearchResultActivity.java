package com.sompom.tookitup.newui;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.sompom.tookitup.newui.fragment.AbsBaseFragment;
import com.sompom.tookitup.newui.fragment.SearchResultFragment;

/**
 * Created by He Rotha on 8/21/18.
 */
public class SearchResultActivity extends AbsDefaultActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(SearchResultFragment.newInstance(), AbsBaseFragment.TAG);
    }
}
