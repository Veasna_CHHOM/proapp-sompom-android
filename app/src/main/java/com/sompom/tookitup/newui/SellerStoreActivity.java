package com.sompom.tookitup.newui;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;

import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.upload.ProductUploadService;
import com.sompom.tookitup.databinding.ActivityDefaultBinding;
import com.sompom.tookitup.intent.newintent.SellerStoreIntent;
import com.sompom.tookitup.listener.OnCompleteListListener;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.newui.fragment.SellerStoreFragment;

import timber.log.Timber;

public class SellerStoreActivity extends AbsSoundControllerActivity<ActivityDefaultBinding> {

    private OnCompleteListListener<Product> mOnUploadProductCompleteListener;

    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Timber.e("onServiceConnected");
            ProductUploadService.UploadBinder binder = (ProductUploadService.UploadBinder) iBinder;
            binder.setListener((result, isUpdate) -> {
                if (mOnUploadProductCompleteListener != null) {
                    mOnUploadProductCompleteListener.onComplete(result, isUpdate);
                }
            });
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Timber.e("onServiceDisconnected");
        }
    };

    @Override
    public int getLayoutRes() {
        return R.layout.activity_default;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getControllerComponent().inject(this);

        String userId;
        if (getIntent().getData() != null) {
            userId = getIntent().getData().getLastPathSegment();
        } else {
            final SellerStoreIntent intent = new SellerStoreIntent(getIntent());
            userId = intent.getUserId();
        }

        setFragment(R.id.containerView,
                SellerStoreFragment.newInstance(userId),
                SellerStoreFragment.TAG);

        Intent intent = new Intent(this, ProductUploadService.class);
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void finish() {
        setResult(Activity.RESULT_OK);
        super.finish();
    }

    @Override
    public void onBackPressed() {
        SellerStoreFragment fragment = (SellerStoreFragment) getFragment(SellerStoreFragment.TAG);
        if (fragment != null && !fragment.onBackPress()) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnection);
    }

    public void setOnUploadProductCompleteListener(OnCompleteListListener<Product> onUploadProductCompleteListener) {
        mOnUploadProductCompleteListener = onUploadProductCompleteListener;
    }
}
