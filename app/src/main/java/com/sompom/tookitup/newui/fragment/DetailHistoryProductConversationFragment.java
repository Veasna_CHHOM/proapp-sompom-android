package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.newadapter.ProductHistoryConversationAdapter;
import com.sompom.tookitup.chat.AbsChatBinder;
import com.sompom.tookitup.chat.listener.ChannelListener;
import com.sompom.tookitup.chat.service.SocketService;
import com.sompom.tookitup.databinding.FragmentDetailHistoryProductConversationBinding;
import com.sompom.tookitup.intent.ChatIntent;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnCompleteListener;
import com.sompom.tookitup.model.emun.SegmentedControlItem;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.ProductHistoryConversationDataManager;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.newviewmodel.DetailHistoryProductConversationFragmentViewModel;
import com.sompom.tookitup.widget.LoaderMoreLayoutManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by nuonveyo on 7/2/18.
 */

public class DetailHistoryProductConversationFragment extends AbsBindingFragment<FragmentDetailHistoryProductConversationBinding>
        implements OnCompleteListener<Conversation>, AbsBaseActivity.OnServiceListener, ChannelListener {

    @Inject
    public ApiService mApiService;

    private ProductHistoryConversationAdapter mAdapter;
    private DetailHistoryProductConversationFragmentViewModel mViewModel;
    private Product mProduct;
    private LoaderMoreLayoutManager mLoaderMoreLayoutManager;
    private SocketService.SocketBinder mSocketBinder;

    public static DetailHistoryProductConversationFragment newInstance(Product product) {

        Bundle args = new Bundle();
        args.putParcelable(SharedPrefUtils.DATA, product);
        DetailHistoryProductConversationFragment fragment = new DetailHistoryProductConversationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_detail_history_product_conversation;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        setToolbar(getBinding().toolbar, false);

        if (getArguments() != null) {
            mProduct = getArguments().getParcelable(SharedPrefUtils.DATA);
        }
        mLoaderMoreLayoutManager = new LoaderMoreLayoutManager(getActivity());
        mLoaderMoreLayoutManager.setOnLoadMoreListener(() -> mViewModel.loadMore(new OnCallbackListListener<List<Conversation>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mAdapter.setCanLoadMore(false);
                mLoaderMoreLayoutManager.setOnLoadMoreListener(null);
                mLoaderMoreLayoutManager.loadingFinished();
            }

            @Override
            public void onComplete(List<Conversation> result, boolean canLoadMore) {
                mAdapter.addLoadMoreData(result);
                mAdapter.setCanLoadMore(canLoadMore);
                mLoaderMoreLayoutManager.loadingFinished();
            }
        }));
        getBinding().recyclerView.setLayoutManager(mLoaderMoreLayoutManager);

        ProductHistoryConversationDataManager dataManager = new ProductHistoryConversationDataManager(getActivity(), mApiService);
        mViewModel = new DetailHistoryProductConversationFragmentViewModel(dataManager,
                mProduct,
                new OnCallbackListListener<List<Conversation>>() {
                    @Override
                    public void onFail(ErrorThrowable ex) {
                        mAdapter = new ProductHistoryConversationAdapter(getActivity(), new ArrayList<>(), null);
                        mAdapter.setCanLoadMore(false);
                        getBinding().recyclerView.setAdapter(mAdapter);
                    }

                    @Override
                    public void onComplete(List<Conversation> result, boolean canLoadMore) {
                        mAdapter = new ProductHistoryConversationAdapter(getActivity(), result, DetailHistoryProductConversationFragment.this);
                        mAdapter.setCanLoadMore(canLoadMore);
                        getBinding().recyclerView.setAdapter(mAdapter);
                        if (!canLoadMore) {
                            mLoaderMoreLayoutManager.setOnLoadMoreListener(null);
                        }
                    }
                });
        setVariable(BR.viewModel, mViewModel);

        if (getActivity() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getActivity()).addOnServiceListener(this);
        }
    }

    @Override
    public void onComplete(Conversation result) {
        if (TextUtils.isEmpty(result.getId())) {
            return;
        }
        ChatIntent intent = new ChatIntent(getActivity(), result.getLastMessage());
        startActivity(intent);
    }

    @Override
    public void onServiceReady(AbsChatBinder binder) {
        mSocketBinder = (SocketService.SocketBinder) binder;
        mSocketBinder.addChannelListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mSocketBinder != null) {
            mSocketBinder.removeChannelListener(this);
        }
    }

    @Override
    public void onChannelCreateOrUpdate(Conversation channel) {

    }

    @Override
    public void onChannelRemoved(String channelId) {

    }

    @Override
    public SegmentedControlItem getCurrentSegmentedControlItem() {
        return SegmentedControlItem.Selling;
    }
}
