package com.sompom.tookitup.newui.fragment;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;

import com.google.android.gms.maps.OnMapReadyCallback;
import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.newadapter.UserStoreDialogAdapter;
import com.sompom.tookitup.databinding.FragmentNearbyMapBinding;
import com.sompom.tookitup.injection.controller.PublicQualifier;
import com.sompom.tookitup.intent.newintent.ProductDetailIntent;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.listener.OnHomeMenuClick;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.NearByDataManager;
import com.sompom.tookitup.services.datamanager.ProductStoreDataManager;
import com.sompom.tookitup.services.datamanager.StoreDataManager;
import com.sompom.tookitup.viewmodel.newviewmodel.DialogUserStoreBottomSheetViewModel;
import com.sompom.tookitup.viewmodel.newviewmodel.NearbyMapFragmentViewModel;
import com.sompom.tookitup.widget.LoaderMoreLayoutManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by nuonveyo on 7/3/18.
 */

public class NearbyMapFragment extends AbsMapFragment<FragmentNearbyMapBinding>
        implements OnMapReadyCallback, OnHomeMenuClick, NearbyMapFragmentViewModel.OnNearByMapListener {
    @Inject
    @PublicQualifier
    public ApiService mApiService;
    private boolean mIsShow;
    private boolean mIsMapReady;
    private BottomSheetBehavior mBottomSheetUserStore;
    private DialogUserStoreBottomSheetViewModel mSheetViewModel;
    private LoaderMoreLayoutManager mLoaderMoreLayoutManager;
    private UserStoreDialogAdapter mUserStoreDialogAdapter;
    private String mCurrentUserStoreSelectedId;
    private OnClickListener mOnClickListener;
    private NearbyMapFragmentViewModel mViewModel;
    private StoreDataManager mStoreDataManager;
    private ProductStoreDataManager mProductStoreDataManager;
    private OnBottomSheetStateChangeListener mOnBottomSheetStateChangeListener;

    public static NearbyMapFragment newInstance() {
        return new NearbyMapFragment();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_nearby_map;
    }

    @Override
    public void onViewAndMapCreated() {
        mIsMapReady = true;
        if (mIsShow) {
            initViewModel();
        }
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    @Override
    public void onTabMenuClick() {
        if (mIsShow || !isAdded()) {
            return;
        }
        mIsShow = true;
        if (!mIsMapReady) {
            return;
        }
        initViewModel();
    }

    private void initViewModel() {
        getControllerComponent().inject(this);
        mBottomSheetUserStore = BottomSheetBehavior.from(getBinding().includeLayoutBottomSheet.containerBottomSheet);
        mBottomSheetUserStore.setHideable(true);
        mBottomSheetUserStore.setState(BottomSheetBehavior.STATE_HIDDEN);
        mBottomSheetUserStore.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (BottomSheetBehavior.STATE_HIDDEN == newState) {
                    getBinding().includeLayoutBottomSheet.containerBottomSheet.setVisibility(View.GONE);
                    mSheetViewModel = null;
                    mCurrentUserStoreSelectedId = null;
                }
                if (mOnBottomSheetStateChangeListener != null) {
                    mOnBottomSheetStateChangeListener.onStateChange(newState);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                if (Double.isNaN(slideOffset)) {
                    slideOffset = 0;
                }
                float v = Math.abs(slideOffset) * bottomSheet.getHeight();
                v = v - bottomSheet.getHeight();
                v = v / 4;
                getBinding().mapView.setTranslationY(v);
            }
        });

        setViewModel();
    }

    private void setViewModel() {
        NearByDataManager dataManager = new NearByDataManager(getContext(), mApiService);
        mStoreDataManager = new StoreDataManager(getActivity(), mApiService);
        mProductStoreDataManager = new ProductStoreDataManager(getActivity(), mApiService);
        mViewModel = new NearbyMapFragmentViewModel(getActivity(),
                getGoogleMap(),
                dataManager,
                NearbyMapFragment.this);
        setVariable(BR.viewModel, mViewModel);
    }

    private void iniRecyclerView(List<Product> productList, boolean canLoadMore) {
        mLoaderMoreLayoutManager = new LoaderMoreLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mLoaderMoreLayoutManager.setOnLoadMoreListener(() -> mSheetViewModel.loadMore(new OnCallbackListListener<List<Product>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mUserStoreDialogAdapter.setCanLoadMore(false);
                mUserStoreDialogAdapter.notifyDataSetChanged();
                mLoaderMoreLayoutManager.loadingFinished();
                mLoaderMoreLayoutManager.setOnLoadMoreListener(null);
            }

            @Override
            public void onComplete(List<Product> result, boolean canLoadMore) {
                mUserStoreDialogAdapter.addLoadMoreData(new ArrayList<>(result));
                mUserStoreDialogAdapter.setCanLoadMore(canLoadMore);
                mLoaderMoreLayoutManager.loadingFinished();
            }
        }));
        getBinding().includeLayoutBottomSheet.recyclerView.setLayoutManager(mLoaderMoreLayoutManager);
        mUserStoreDialogAdapter = new UserStoreDialogAdapter(productList, result -> {
            if (!TextUtils.isEmpty(result.getId())) {
                startActivity(new ProductDetailIntent(getActivity(),
                        result,
                        false,
                        false));
            }
        });
        mUserStoreDialogAdapter.setCanLoadMore(canLoadMore);
        getBinding().includeLayoutBottomSheet.recyclerView.setAdapter(mUserStoreDialogAdapter);
    }

    public void onMoveToCurrentLocationClick() {
        if (mViewModel != null) {
            mViewModel.onMoveToCurrentLocationClick();
        }
    }

    @Override
    public void onHideBottomSheet() {
        mBottomSheetUserStore.setState(BottomSheetBehavior.STATE_HIDDEN);
    }

    @Override
    public void onComplete(User result) {
        if (mOnClickListener != null) {
            mOnClickListener.onClick();
        }
        if (getBinding().includeLayoutBottomSheet.containerBottomSheet.getVisibility() != View.VISIBLE) {
            getBinding().includeLayoutBottomSheet.containerBottomSheet.setVisibility(View.VISIBLE);
            new Handler().postDelayed(() -> {
                if (isAdded()) {
                    mBottomSheetUserStore.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }, 300);
        } else {
            mBottomSheetUserStore.setState(BottomSheetBehavior.STATE_EXPANDED);
        }

        if (mSheetViewModel == null || !TextUtils.equals(mCurrentUserStoreSelectedId, result.getId())) {
            mSheetViewModel = new DialogUserStoreBottomSheetViewModel(mStoreDataManager,
                    mProductStoreDataManager,
                    result,
                    this::iniRecyclerView);
            setVariable(BR.bottomSheetViewModel, mSheetViewModel);
        }
        mCurrentUserStoreSelectedId = result.getId();
    }

    public void setOnBottomSheetStateChangeListener(OnBottomSheetStateChangeListener onBottomSheetStateChangeListener) {
        mOnBottomSheetStateChangeListener = onBottomSheetStateChangeListener;
    }

    public int getCurrentBottomState() {
        return mBottomSheetUserStore.getState();
    }

    public interface OnBottomSheetStateChangeListener {
        void onStateChange(int state);
    }
}
