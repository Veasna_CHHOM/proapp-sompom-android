package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.example.usermentionable.model.UserMentionable;
import com.example.usermentionable.tokenization.QueryToken;
import com.example.usermentionable.tokenization.interfaces.QueryTokenReceiver;
import com.leocardz.link.preview.library.SourceContent;
import com.leocardz.link.preview.library.TextCrawler;
import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.FragmentCreateTimelineBinding;
import com.sompom.tookitup.injection.controller.PublicQualifier;
import com.sompom.tookitup.listener.AbsLinkPreviewCallback;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.model.WallStreetAdaptive;
import com.sompom.tookitup.model.emun.CreateWallStreetScreenType;
import com.sompom.tookitup.model.result.LinkPreviewModel;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.CreateWallStreetDataManager;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.CreateTimelineFragmentViewModel;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by nuonveyo on 5/4/18.
 */

public class CreateTimelineFragment extends com.sompom.tookitup.newui.fragment.AbsBindingFragment<FragmentCreateTimelineBinding>
        implements QueryTokenReceiver {
    public static final String TAG = CreateTimelineFragment.class.getName();

    @Inject
    public ApiService mApiService;
    @Inject
    @PublicQualifier
    public ApiService mApiServiceTest;
    private CreateTimelineFragmentViewModel mViewModel;
    private TextCrawler mTextCrawler;
    private final AbsLinkPreviewCallback mLinkPreviewCallback = new AbsLinkPreviewCallback() {

        @Override
        public void onSetEditText(String text) {
            getBinding().richEditorView.setText(text);
        }

        @Override
        public void onRemoveListener() {
            getBinding().linkPreviewLayout.hideLayout();
        }

        @Override
        public void onStartDownloadLinkPreview(String link) {
            mTextCrawler.makePreview(mLinkPreviewCallback, link);
        }

        @Override
        public void onPre() {
            getBinding().linkPreviewLayout.showLayout();
        }

        @Override
        public void onShowGifLinkFormKeyboard(String link) {
            getBinding().linkPreviewLayout.showGifLinkFromKeyboard(link);
        }

        @Override
        public void onPos(SourceContent sourceContent, boolean isNull) {
            if (isNull) {
                getBinding().linkPreviewLayout.hideLayout();
            } else {
                mViewModel.setShowedLinkPreview(true);
                LinkPreviewModel linkPreviewModel = new LinkPreviewModel();
                linkPreviewModel.setTitle(sourceContent.getTitle());
                linkPreviewModel.setDescription(sourceContent.getDescription());
                linkPreviewModel.setLink(sourceContent.getUrl());
                linkPreviewModel.setLinkDownloaded(true);

                if (sourceContent.getImages() != null && !sourceContent.getImages().isEmpty()) {
                    for (String s : sourceContent.getImages()) {
                        if (!TextUtils.isEmpty(s)) {
                            linkPreviewModel.setLogo(s);
                            break;
                        }
                    }
                }
                getBinding().linkPreviewLayout.setLinkPreviewModel(linkPreviewModel, true);
            }
        }
    };

    public static CreateTimelineFragment newInstance(WallStreetAdaptive adaptive, int screenTypeId) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(SharedPrefUtils.DATA, adaptive);
        bundle.putInt(SharedPrefUtils.ID, screenTypeId);
        CreateTimelineFragment fragment = new CreateTimelineFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_create_timeline;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        mTextCrawler = new TextCrawler();

        WallStreetAdaptive wallStreetAdaptive = null;
        CreateWallStreetScreenType screenType = CreateWallStreetScreenType.CREATE_POST;
        if (getArguments() != null) {
            wallStreetAdaptive = getArguments().getParcelable(SharedPrefUtils.DATA);
            int id = getArguments().getInt(SharedPrefUtils.ID, CreateWallStreetScreenType.CREATE_POST.getId());
            screenType = CreateWallStreetScreenType.getType(id);
        }

        setToolbar(getBinding().layoutToolbar.toolbar, true);
        if (getActivity() != null) {
            if (screenType == CreateWallStreetScreenType.EDIT_POST) {
                getBinding().layoutToolbar.textViewTitle.setText(getActivity().getString(R.string.create_wall_street_edit_title));
            } else {
                getBinding().layoutToolbar.textViewTitle.setText(getActivity().getString(R.string.create_wall_street_title));
            }
        }

        CreateWallStreetDataManager dataManager = new CreateWallStreetDataManager(getActivity(), mApiService, mApiServiceTest);
        mViewModel = new CreateTimelineFragmentViewModel((AbsBaseActivity) getActivity(),
                dataManager,
                wallStreetAdaptive,
                screenType,
                mLinkPreviewCallback);
        setVariable(BR.viewModel, mViewModel);
        getBinding().linkPreviewLayout.setOnCloseLinkPreviewListener(() -> mViewModel.setShowedLinkPreview(false));

        getBinding().richEditorView.setQueryTokenReceiver(this);
        getBinding().richEditorView.addOnTextChangeListener(mViewModel.onTextChanged());
        getBinding().richEditorView.setOnKeyboardInputCallback(mViewModel);
    }

    @Override
    public void onPause() {
        super.onPause();
        getBinding().richEditorView.removeListPopupWindow();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTextCrawler.cancel();
        getBinding().richEditorView.removeListPopupWindow();
    }

    @Override
    public void onQueryReceived(@NonNull QueryToken queryToken) {
        if (queryToken.getExplicitChar() == '@') {
            mViewModel.getMentionUserList(queryToken.getKeywords(), new OnCallbackListener<List<UserMentionable>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    getBinding().richEditorView.hideListPopupWindow();
                }

                @Override
                public void onComplete(List<UserMentionable> result) {
                    if (result != null && !result.isEmpty()) {
                        getBinding().richEditorView.showListPopupWindow(result);
                    } else {
                        getBinding().richEditorView.hideListPopupWindow();
                    }
                }
            });
        } else if (queryToken.getExplicitChar() == '#') {
            getBinding().richEditorView.checkToRenderHashTag(queryToken.getTokenString());
        } else {
            getBinding().richEditorView.hideListPopupWindow();
        }
    }

    @Override
    public void invalidToken() {
        getBinding().richEditorView.removeListPopupWindow();
    }
}
