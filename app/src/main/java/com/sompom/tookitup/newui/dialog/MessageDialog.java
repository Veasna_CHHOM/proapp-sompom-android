package com.sompom.tookitup.newui.dialog;

import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ListView;

import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.SingleChoiceAdapter;
import com.sompom.tookitup.databinding.DialogFragmentMessageBinding;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.newui.fragment.AbsDialogFragment;

/**
 * Created by he.rotha on 4/28/16.
 */
public class MessageDialog extends AbsDialogFragment {
    public static final String TAG = MessageDialog.class.getName();

    private DialogFragmentMessageBinding mBinding;
    private String mTitle;
    private String mMessage;
    private String mLeftText;
    private String mRightText;
    private View.OnClickListener mLeftClickListener;
    private View.OnClickListener mRightClickListener;
    private boolean mIsDismissWhenClickOnButtonRight = true;
    private boolean mIsDismissWhenClickOnButtonLeft = true;

    private String[] mTitles;
    private int mSelectedPosition = 0;
    private DialogInterface.OnClickListener mSingleChoiceListener;
    private OnClickListener mOnClickListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_fragment_message, container, false);
        return getBinding().getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        if (TextUtils.isEmpty(mTitle)) {
            getBinding().textviewTitle.setVisibility(View.GONE);
        } else {
            getBinding().textviewTitle.setText(mTitle);
        }

        if (TextUtils.isEmpty(mMessage)) {
            getBinding().textviewMessage.setVisibility(View.GONE);
        } else {
            getBinding().textviewMessage.setText(mMessage);
        }

        if (TextUtils.isEmpty(mLeftText)) {
            getBinding().buttonLeft.setVisibility(View.GONE);
        } else {
            getBinding().buttonLeft.setText(mLeftText);
            getBinding().buttonLeft.setOnClickListener(view1 -> {
                if (mLeftClickListener != null) {
                    mLeftClickListener.onClick(view1);
                }
                if (mIsDismissWhenClickOnButtonLeft) {
                    dismiss();
                }
            });
        }

        if (TextUtils.isEmpty(mRightText)) {
            getBinding().buttonRight.setVisibility(View.GONE);
        } else {
            getBinding().buttonRight.setText(mRightText);
            getBinding().buttonRight.setOnClickListener(view12 -> {
                if (mRightClickListener != null) {
                    mRightClickListener.onClick(view12);
                }
                if (mIsDismissWhenClickOnButtonRight) {
                    dismiss();
                }
            });
        }

        if (mTitles != null) {
            ListView listView = new ListView(getContext());
            listView.setVerticalScrollBarEnabled(false);
            listView.setOverScrollMode(View.OVER_SCROLL_NEVER);
            listView.setDivider(null);
            listView.setDividerHeight(0);

            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            getBinding().view.addView(listView, params);

            listView.setAdapter(new SingleChoiceAdapter(getContext(), mTitles, mSelectedPosition, mSingleChoiceListener));
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mOnClickListener != null) {
            mOnClickListener.onClick();
        }
    }

    public DialogFragmentMessageBinding getBinding() {
        return mBinding;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public void setLeftText(String leftText, View.OnClickListener listener) {
        mLeftText = leftText;
        mLeftClickListener = listener;
    }

    public void setRightText(String rightText, View.OnClickListener listener) {
        mRightText = rightText;
        mRightClickListener = listener;
    }

    public void show(FragmentManager manager) {
        super.show(manager, TAG);
    }

    public void setSingleChoiceItems(String[] titles,
                                     int selectedPosition,
                                     DialogInterface.OnClickListener listener) {
        mTitles = titles;
        mSelectedPosition = selectedPosition;
        mSingleChoiceListener = listener;
    }

    public void setDismissWhenClickOnButtonRight(boolean dismissWhenClickOnButtonRight) {
        mIsDismissWhenClickOnButtonRight = dismissWhenClickOnButtonRight;
    }

    public void setDismissWhenClickOnButtonLeft(boolean dismissWhenClickOnButtonLeft) {
        mIsDismissWhenClickOnButtonLeft = dismissWhenClickOnButtonLeft;
    }

    public void setOnDialogDismiss(OnClickListener onDialogDismiss) {
        mOnClickListener = onDialogDismiss;
    }
}
