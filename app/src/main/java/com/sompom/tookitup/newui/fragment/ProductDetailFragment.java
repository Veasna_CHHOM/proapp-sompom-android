package com.sompom.tookitup.newui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.desmond.squarecamera.model.AppTheme;
import com.desmond.squarecamera.utils.ThemeManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.TookitupApplication;
import com.sompom.tookitup.adapter.ImageAdapter;
import com.sompom.tookitup.adapter.newadapter.ProductStoreAdapter;
import com.sompom.tookitup.databinding.FragmentProductDetail2Binding;
import com.sompom.tookitup.intent.MapIntent;
import com.sompom.tookitup.intent.SearchResultIntent;
import com.sompom.tookitup.listener.OnProductItemClickListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.request.QueryProduct;
import com.sompom.tookitup.model.result.Category;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.ProductDetailActivity;
import com.sompom.tookitup.newui.dialog.ProductDetailActionButtonDialog;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.ProductManager;
import com.sompom.tookitup.services.datamanager.StoreDataManager;
import com.sompom.tookitup.utils.BitmapConverter;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.StoreListingViewModel;
import com.sompom.tookitup.viewmodel.newviewmodel.ProductDetailFragmentViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by nuonveyo on 6/6/18.
 */

public class ProductDetailFragment extends AbsBindingFragment<FragmentProductDetail2Binding>
        implements OnMapReadyCallback {
    @Inject
    public ApiService mApiService;
    private Product mProduct;
    private ProductDetailFragmentViewModel mViewModel;
    private GoogleMap mGoogleMap;
    private boolean mIsMapReady;
    private boolean mIsDrawReady;

    public static ProductDetailFragment newInstance(Product product) {
        final ProductDetailFragment fragment = new ProductDetailFragment();
        final Bundle bundle = new Bundle();
        bundle.putParcelable(SharedPrefUtils.PRODUCT, product);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_product_detail2;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);

        SupportMapFragment supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (supportMapFragment != null) {
            supportMapFragment.getMapAsync(this);
        }
        if (getArguments() != null) {
            mProduct = getArguments().getParcelable(SharedPrefUtils.PRODUCT);
        }

        if (!getResources().getBoolean(R.bool.is_phone)) {
            getBinding().scrollview.post(() -> {
                getBinding().scrollview.fullScroll(View.FOCUS_DOWN);
                getBinding().scrollview.scrollBy(0, getBinding().storeContainer.getHeight());
            });
        }

        ProductManager manager = new ProductManager(getActivity(), mApiService);
        StoreDataManager storeDataManager = new StoreDataManager(getActivity(), mApiService);
        mViewModel = new ProductDetailFragmentViewModel((ProductDetailActivity) getActivity(), manager,
                storeDataManager,
                mProduct,
                new ProductDetailFragmentViewModel.OnCallback() {
                    @Override
                    public void onGetRelatedProductSuccess(List<Product> products) {
                        initRelateProduct(products);
                    }

                    @Override
                    public void onGetProductDetailSuccess(Product product) {
                        mProduct = product;
                        setStoreViewModel();
                        initProductImage();
                    }

                    @Override
                    public void onUpdateProduct(Product product) {
                        mProduct = product;
                    }
                });
        setVariable(BR.viewModel, mViewModel);

        //TODO: need to remove after test
        getBinding().containerPrice.setOnClickListener(v -> {
            ProductDetailActionButtonDialog dialog = ProductDetailActionButtonDialog.newInstance();
            dialog.show(getChildFragmentManager(), ProductDetailActionButtonDialog.TAG);
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (getActivity() == null) {
            return;
        }
        if (ThemeManager.getAppTheme(getActivity()) == AppTheme.Black) {
            googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.google_map_dark));
        }

        mIsMapReady = true;
        mGoogleMap = googleMap;
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.getUiSettings().setZoomGesturesEnabled(false);
        googleMap.getUiSettings().setTiltGesturesEnabled(false);
        googleMap.getUiSettings().setScrollGesturesEnabled(false);
        googleMap.getUiSettings().setAllGesturesEnabled(false);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setCompassEnabled(false);
        initMap();
    }

    private void setStoreViewModel() {
        StoreListingViewModel viewModel = new StoreListingViewModel(getActivity(), mProduct.getUser());
        setVariable(BR.storeModel, viewModel);
    }

    private void initMap() {
        if (mIsMapReady && mGoogleMap != null) {
            if (getActivity() == null) {
                return;
            }
            if (!mIsDrawReady) {
                final LatLng productLocation = new LatLng(mProduct.getLatitude(), mProduct.getLongitude());
                mGoogleMap.clear();
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(productLocation, TookitupApplication.MAP_ZOOM));
                mGoogleMap.addMarker(new MarkerOptions().position(productLocation)
                        .icon(BitmapConverter.bitmapDescriptorFromVector(getActivity(), R.drawable.ic_pin_location_star)));

                mGoogleMap.setOnMapClickListener(latLng -> startActivity(new MapIntent(getActivity(), mProduct)));

                mGoogleMap.setOnMarkerClickListener(marker -> {
                    startActivity(new MapIntent(getActivity(), mProduct));
                    return true;
                });
                mIsDrawReady = true;
            }
        }
    }

    private void initRelateProduct(List<Product> value) {
        int columnCount = getResources().getInteger(R.integer.number_of_column);
        getBinding().recyclerviewRelateditem.setLayoutManager(new GridLayoutManager(getActivity(), columnCount));
        ProductStoreAdapter adapter = ProductStoreAdapter.newRelatedInstance((AbsBaseActivity) getActivity(),
                value,
                new OnProductItemClickListener() {
                    @Override
                    public void onProductItemClick(Adaptive product) {

                    }

                    @Override
                    public void onProductItemEdit(Product product) {
                        //no handle item edit on product detail
                    }
                });
        adapter.setOnLikeProductListener((product, position) -> {
            mViewModel.checkToPostLike(product);
        });
        adapter.setCanLoadMore(false);
        getBinding().recyclerviewRelateditem.setAdapter(adapter);
    }

    private void initProductImage() {
        try {
            //init image viewpager
            List<ImageFragment> fragments = new ArrayList<>();
            if (mProduct.getMedia() != null) {
                for (int i = 0; i < mProduct.getMedia().size(); i++) {
                    ImageFragment fragment = ImageFragment.getInstance(mProduct.getMedia().get(i).getUrl());
                    fragments.add(fragment);
                }
            }
            final ImageAdapter adapter = new ImageAdapter(getChildFragmentManager(), fragments);
            getBinding().viewpager.setAdapter(adapter);
            getBinding().viewpager.setOffscreenPageLimit(adapter.getCount());

            getBinding().indicator.setViewPager(getBinding().viewpager);
            getBinding().indicator.setEnabled(false);

            //init map
            mIsDrawReady = false;
            initMap();

            //init category
            getBinding().horizontalLayout.removeAllViews();
            getBinding().horizontalLayout1.removeAllViews();
            for (int i = 0; i < mProduct.getCategories().size(); i++) {
                final View v = LayoutInflater.from(getActivity()).inflate(R.layout.list_item_category, null);
                final View v1 = LayoutInflater.from(getActivity()).inflate(R.layout.list_item_category, null);
                final TextView textView = v.findViewById(R.id.textview);
                final TextView textView1 = v1.findViewById(R.id.textview);
                textView.setText(mProduct.getCategories().get(i).getName());
                textView1.setText(mProduct.getCategories().get(i).getName());

                final Category prd = mProduct.getCategories().get(i);
                View.OnClickListener listener = v2 -> {
                    QueryProduct query = SharedPrefUtils.getQueryProduct(getContext());
                    query.setCategory(prd);
                    SharedPrefUtils.setQueryProduct(query, getContext());
                    SearchResultIntent intent = new SearchResultIntent(getActivity());
                    startActivity(intent);
                };
                v.setOnClickListener(listener);
                v1.setOnClickListener(listener);
                getBinding().horizontalLayout.addView(v);
                getBinding().horizontalLayout1.addView(v1);
            }
            if (getActivity() instanceof ProductDetailActivity) {
                ((ProductDetailActivity) getActivity()).setValue(mProduct);
            }
            mViewModel.hideLoading();
        } catch (Exception ex) {
            // java.lang.IllegalStateException: Fragment ProductDetailFragment{2e7e3161} not attached to Activity
            // when the activity close before it return from the background threat.
            mViewModel.showError(getString(R.string.error_general));
        }
    }

    public Product getProduct() {
        return mProduct;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (getActivity() instanceof ProductDetailActivity) {
            ((ProductDetailActivity) getActivity()).checkProductIsBelongToOwner();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getActivity() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getActivity()).removeStateChangeListener(mViewModel.getNetworkStateListener());
        }
    }
}
