package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SimpleItemAnimator;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;

import com.desmond.squarecamera.utils.Keys;
import com.leocardz.link.preview.library.LinkPreviewCallback;
import com.leocardz.link.preview.library.SourceContent;
import com.leocardz.link.preview.library.TextCrawler;
import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.newadapter.ForwardAdapter;
import com.sompom.tookitup.chat.MessageState;
import com.sompom.tookitup.databinding.FragmentForwardBinding;
import com.sompom.tookitup.listener.ConversationDataAdaptive;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.model.result.LinkPreviewModel;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.ForwardDataManager;
import com.sompom.tookitup.utils.KeyboardUtil;
import com.sompom.tookitup.viewmodel.newviewmodel.ForwardViewModel;
import com.sompom.tookitup.widget.LoaderMoreLayoutManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by He Rotha on 9/13/18.
 */
public class ForwardFragment extends AbsBindingFragment<FragmentForwardBinding>
        implements ForwardViewModel.OnViewModelCallback {

    @Inject
    public ApiService mApiService;

    private ForwardAdapter mForwardAdapter;
    private ForwardViewModel mViewModel;
    private TextCrawler mTextCrawler;

    public static ForwardFragment newInstance(Chat chat,
                                              List<Media> media,
                                              boolean isFromOutSideApp) {
        ArrayList<Media> media1 = (ArrayList<Media>) media;
        Bundle args = new Bundle();
        args.putParcelable(Keys.DATA, chat);
        args.putParcelableArrayList(Keys.PATH, media1);
        args.putBoolean(Keys.SOURCE_TYPE, isFromOutSideApp);
        ForwardFragment fragment = new ForwardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_forward;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);

        LoaderMoreLayoutManager loaderMoreLayoutManager = new LoaderMoreLayoutManager(getActivity());
        getBinding().recyclerView.setLayoutManager(loaderMoreLayoutManager);
        getBinding().recyclerView.setOnTouchListener((view1, motionEvent) -> {
            KeyboardUtil.hideKeyboard(getActivity());
            view1.performClick();
            return false;
        });
        SimpleItemAnimator simpleItemAnimator = (SimpleItemAnimator) getBinding().recyclerView.getItemAnimator();
        if (simpleItemAnimator != null) {
            simpleItemAnimator.setSupportsChangeAnimations(false);
        }

        mForwardAdapter = new ForwardAdapter();
        mForwardAdapter.setOnForwardItemClickListener(user -> mViewModel.onForwardClick(user));
        getBinding().recyclerView.setAdapter(mForwardAdapter);

        if (getArguments() != null) {
            Chat chat = getArguments().getParcelable(Keys.DATA);
            ArrayList<Media> media = getArguments().getParcelableArrayList(Keys.PATH);
            boolean isFromOutSideApp = getArguments().getBoolean(Keys.SOURCE_TYPE);
            String content = "";
            if (chat != null) {
                content = chat.getContent();
            }

            final ForwardDataManager manager = new ForwardDataManager(getActivity(), mApiService);
            mViewModel = new ForwardViewModel((AbsBaseActivity) getActivity(),
                    content,
                    media,
                    isFromOutSideApp,
                    manager,
                    this);

            // Download link from inside app
            if (chat != null && chat.getLinkPreviewModel() != null) {
//            // Share from other app
//            if (isFromOutSideApp) {
//                startDownloadLinkPreview(chat, chat.getContent());
//            } else {
//                // Share from inside app
//                if (chat.getLinkPreviewModel().isLinkDownloaded()) {
//                    mViewModel.setDisplayLink(chat.getLinkPreviewModel());
//                } else {
//                    startDownloadLinkPreview(chat, chat.getLinkPreviewModel().getLink());
//                }
//            }
                if (chat.getLinkPreviewModel().isLinkDownloaded()) {
                    mViewModel.setDisplayLink(chat.getContent(), chat.getLinkPreviewModel());
                } else {
                    startDownloadLinkPreview(chat, chat.getLinkPreviewModel().getLink());
                }
            } else {
                // Download link from other app
                if (isFromOutSideApp
                        && !TextUtils.isEmpty(content)
                        && Patterns.WEB_URL.matcher(content).matches()) {
                    startDownloadLinkPreview(chat, content);
                }
            }
            setVariable(BR.viewModel, mViewModel);
        }
    }

    private void startDownloadLinkPreview(Chat chat, String link) {
        mViewModel.mIsDownloadingLinkPreview.set(true);
        mViewModel.mForwardChat.set(null);
        mTextCrawler = new TextCrawler();
        mTextCrawler.makePreview(new LinkPreviewCallback() {
            @Override
            public void onPre() {

            }

            @Override
            public void onPos(SourceContent data, boolean b) {
                String title = data.getTitle();
                if (TextUtils.isEmpty(title)) {
                    title = data.getCannonicalUrl();
                }
                LinkPreviewModel linkPreviewModel = new LinkPreviewModel();
                linkPreviewModel.setTitle(title);
                linkPreviewModel.setDescription(data.getDescription());
                linkPreviewModel.setLink(data.getUrl());
                linkPreviewModel.setLinkDownloaded(true);

                if (data.getImages() != null && !data.getImages().isEmpty()) {
                    for (String s : data.getImages()) {
                        if (!TextUtils.isEmpty(s)) {
                            linkPreviewModel.setLogo(s);
                            break;
                        }
                    }
                }
                chat.setLinkPreviewModel(linkPreviewModel);
                mViewModel.setDisplayLink(chat.getContent(), chat.getLinkPreviewModel());
            }
        }, link);
    }

    @Override
    public void onSuggestPeopleComplete(List<ConversationDataAdaptive> suggest) {
        mForwardAdapter.setCanLoadMore(true);
        mForwardAdapter.addSuggestion(suggest);
    }

    @Override
    public void onPeopleComplete(List<ConversationDataAdaptive> people, boolean isLoadMore) {
        mForwardAdapter.setCanLoadMore(isLoadMore);
        mForwardAdapter.addPeople(people);
    }

    @Override
    public void onPeopleSearch(List<ConversationDataAdaptive> people) {
        mForwardAdapter.update(people);
    }

    @Override
    public void onForwardComplete(String messageId, MessageState messageState) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(() -> mForwardAdapter.update(messageId, messageState));
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTextCrawler != null) {
            mTextCrawler.cancel();
        }
    }
}
