package com.sompom.tookitup.newui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CompoundButton;

import com.desmond.squarecamera.model.AppTheme;
import com.desmond.squarecamera.utils.ThemeManager;
import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.FragmentEditProfile2Binding;
import com.sompom.tookitup.helper.ScrimColorAppbarListener;
import com.sompom.tookitup.intent.AdvancedNotificationSettingIntent;
import com.sompom.tookitup.intent.newintent.HomeIntent;
import com.sompom.tookitup.intent.newintent.LoginIntent;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnEditProfileListener;
import com.sompom.tookitup.listener.OnSpannableClickListener;
import com.sompom.tookitup.model.emun.AccountStatus;
import com.sompom.tookitup.model.emun.StoreStatus;
import com.sompom.tookitup.model.result.NotificationSettingModel;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.dialog.MessageDialog;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.StoreDataManager;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.EditProfileFragmentViewModel;

import javax.inject.Inject;

import retrofit2.Response;

/**
 * Created by nuonveyo on 6/13/18.
 */

public class EditProfileFragment extends AbsBindingFragment<FragmentEditProfile2Binding>
        implements OnEditProfileListener, OnSpannableClickListener {

    @Inject
    public ApiService mApiService;

    private EditProfileFragmentViewModel mViewModel;
    private MessageDialog mDisableNotificationDialog;
    private MessageDialog mActivateDialog;
    private CompoundButton.OnCheckedChangeListener mCheckListener;
    private StoreStatus mStatus;
    private boolean mIsChangeValue = false;

    public static EditProfileFragment newInstance() {
        return new EditProfileFragment();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_edit_profile2;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);

        StoreDataManager storeDataManager = new StoreDataManager(getActivity(), mApiService);
        mViewModel = new EditProfileFragmentViewModel(storeDataManager,
                getChildFragmentManager(),
                EditProfileFragment.this,
                EditProfileFragment.this);
        setVariable(BR.viewModel, mViewModel);
        mCheckListener = (buttonView, isChecked) -> {
            if (buttonView.getId() == R.id.switch_disable_notification) {
                if (isChecked) {
                    getBinding().switchDisableNotification.setChecked(false);
                    mDisableNotificationDialog.show(getChildFragmentManager(), "");
                } else {
                    mViewModel.pushNotificationSetting(false);
                }
            } else if (buttonView.getId() == R.id.switch_activate) {
                getBinding().switchActivate.setChecked(!isChecked);
                mActivateDialog.show(getChildFragmentManager(), "");
            }
        };

        if (getActivity() != null && ThemeManager.getAppTheme(getActivity()) == AppTheme.White) {
            getBinding().appBar.addOnOffsetChangedListener(new ScrimColorAppbarListener(getBinding().collapse) {

                @Override
                public void onColorChange(int color) {
                    getBinding().layoutToolbar.backButton.setTextColor(color);
                    getBinding().layoutToolbar.title.setTextColor(color);
                }
            });
        }
    }

    @Override
    public void onLogoutClick() {
        showMessageDialog(R.string.edit_profile_log_out,
                R.string.edit_profile_log_out_description,
                R.string.edit_profile_yes,
                R.string.edit_profile_no, dialog -> {
                    if (getActivity() == null) {
                        return;
                    }
                    SharedPrefUtils.clearValue(getActivity());
                    if (getActivity() instanceof AbsBaseActivity && ((AbsBaseActivity) getActivity()).getSinchBinder() != null) {
                        ((AbsBaseActivity) getActivity()).getSinchBinder().stopService();
                        ((AbsBaseActivity) getActivity()).bindCall(binder -> {
                            binder.logout();

                            LoginIntent intent = new LoginIntent(getActivity());
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            getActivity().startActivity(intent);
                            getActivity().finish();
                        });
                    }

                });
    }

    @Override
    public void onComplete(NotificationSettingModel result) {
        initActivateStoreDialog();
        initDisableNotificationDialog();
        initNotificationSetting(result);
    }

    private void initDisableNotificationDialog() {
        mDisableNotificationDialog = new MessageDialog();
        mDisableNotificationDialog.setMessage(getString(R.string.edit_profile_are_you_sure_you_want_to_disable_all_notification_title));
        mDisableNotificationDialog.setTitle(getString(R.string.edit_profile_disable_all_notification_menu));
        mDisableNotificationDialog.setLeftText(getString(R.string.change_language_button_ok),
                dialog -> {
                    getBinding().switchDisableNotification.setOnCheckedChangeListener(null);
                    getBinding().switchDisableNotification.setChecked(true);
                    getBinding().switchDisableNotification.setOnCheckedChangeListener(mCheckListener);
                    mViewModel.pushNotificationSetting(true);
                });
    }

    private void initNotificationSetting(NotificationSettingModel value) {
        SharedPrefUtils.setNotificationSetting(getActivity(), value);
        if (value != null && getBinding().switchDisableNotification != null) {
            getBinding().switchDisableNotification.setChecked(value.isDisableAllNotification());
        }

        if (getBinding().switchDisableNotification != null) {
            getBinding().switchDisableNotification.setOnCheckedChangeListener(mCheckListener);
        }
    }

    private void initActivateStoreDialog() {
        if (SharedPrefUtils.getAccountStatus(getContext()) == AccountStatus.BLOCK) {
            getBinding().switchActivate.setEnabled(false);
        }
        mStatus = SharedPrefUtils.getStatus(getContext());
        String textDesc;
        String textDialogDesc;
        mActivateDialog = new MessageDialog();
        getBinding().switchActivate.setOnCheckedChangeListener(null);
        if (mStatus == StoreStatus.ACTIVE) {
            textDesc = getString(R.string.edit_profile_store_is_activate_right_now) + "\n" +
                    getString(R.string.edit_profile_account_activate_description);
            mActivateDialog.setTitle(getString(R.string.edit_profile_deactivate_store_title));
            textDialogDesc = getString(R.string.edit_profile_are_you_sure_you_want_to_deactivate_your_store_title);
            getBinding().switchActivate.setChecked(true);
        } else {
            textDesc = getString(R.string.edit_profile_store_is_deactivate_right_now) + "\n" +
                    getString(R.string.edit_profile_account_activate_description);
            mActivateDialog.setTitle(getString(R.string.edit_profile_activate_store_title));
            textDialogDesc = getString(R.string.edit_profile_are_you_sure_you_want_to_activate_your_store_title);
            getBinding().switchActivate.setChecked(false);
        }
        getBinding().switchActivate.setOnCheckedChangeListener(mCheckListener);

        getBinding().textviewActivateDesc.setText(textDesc);
        mActivateDialog.setMessage(textDialogDesc);

        mActivateDialog.setLeftText(getString(R.string.change_language_button_ok), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StoreStatus status;
                if (mStatus == StoreStatus.ACTIVE) {
                    status = StoreStatus.DEACTIVATE;
                } else {
                    status = StoreStatus.ACTIVE;
                }
                mViewModel.changeMyStoreStatus(status, new OnCallbackListener<Response<User>>() {
                    @Override
                    public void onComplete(Response<User> result) {
                        if (result.body() == null) {
                            return;
                        }
                        mStatus = result.body().getStatus();
                        SharedPrefUtils.setStatus(mStatus, getActivity());
                        mIsChangeValue = true;
                        initActivateStoreDialog();
                    }

                    @Override
                    public void onFail(ErrorThrowable ex) {
                        showSnackBar(R.string.error_cannot_change_this_setting);
                    }
                });
            }
        });
    }

    public boolean isChangeValue() {
        return mIsChangeValue;
    }

    @Override
    public void onClick() {
        if (getBinding().switchDisableNotification.isChecked()) {
            showSnackBar(R.string.edit_profile_advanced_notification_setting_click_warning_title);
        } else {
            Intent intent = new AdvancedNotificationSettingIntent(getActivity());
            startActivity(intent);
        }
    }
}
