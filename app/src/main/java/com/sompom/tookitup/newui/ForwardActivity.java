package com.sompom.tookitup.newui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.sompom.tookitup.helper.CheckPermissionCallbackHelper;
import com.sompom.tookitup.helper.LoginActivityResultCallback;
import com.sompom.tookitup.intent.ForwardIntent;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.newui.fragment.ForwardFragment;

import java.util.ArrayList;

/**
 * Created by He Rotha on 9/13/18.
 */
public class ForwardActivity extends AbsDefaultActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkPermission(new CheckPermissionCallbackHelper(this,
                CheckPermissionCallbackHelper.Type.STORAGE) {

            @Override
            public void onPermissionGranted() {
                ForwardIntent intent = new ForwardIntent(getThis(), getIntent());
                ArrayList<Media> medias = intent.getMedia();
                if (medias != null && medias.isEmpty() && intent.getChat() == null) {
                    finish();
                    return;
                }

                startLoginWizardActivity(new LoginActivityResultCallback() {
                    @Override
                    public void onActivityResultSuccess(boolean isAlreadyLogin) {
                        final Fragment fr = ForwardFragment.newInstance(intent.getChat(),
                                medias,
                                intent.isFromOutSideApplication());
                        setFragment(fr, ForwardFragment.TAG);
                    }

                    @Override
                    public void onActivityResultFail() {
                        finish();
                    }
                });
            }

            @Override
            public void onCancelClick() {
                finish();
            }
        }, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }
}
