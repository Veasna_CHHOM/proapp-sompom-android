package com.sompom.tookitup.newui;

import android.os.Bundle;

import com.sompom.tookitup.newui.fragment.FilterDetailFragment;

public class FilterDetailActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(FilterDetailFragment.newInstance(), FilterDetailFragment.TAG);
    }
}
