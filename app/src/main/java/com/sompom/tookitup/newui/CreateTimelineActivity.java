package com.sompom.tookitup.newui;

import android.os.Bundle;

import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.ActivityCreateTimelineBinding;
import com.sompom.tookitup.helper.LoginActivityResultCallback;
import com.sompom.tookitup.intent.CreateTimelineIntent;
import com.sompom.tookitup.newui.fragment.CreateTimelineFragment;

public class CreateTimelineActivity extends AbsBindingActivity<ActivityCreateTimelineBinding> {

    @Override
    public int getLayoutRes() {
        return R.layout.activity_create_timeline;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startLoginWizardActivity(new LoginActivityResultCallback() {
            @Override
            public void onActivityResultSuccess(boolean isAlreadyLogin) {
                CreateTimelineIntent intent = new CreateTimelineIntent(CreateTimelineActivity.this, getIntent());
                setFragment(R.id.container_view,
                        CreateTimelineFragment.newInstance(intent.getLifeStream(), intent.getScreenTypeId()),
                        CreateTimelineFragment.TAG);
            }

            @Override
            public void onActivityResultFail() {
                finish();
            }
        });

    }

    @Override
    public void onFloatingVideoServiceConnected() {
        if (getFloatingVideo() != null
                && getFloatingVideo().isFloating()) {
            getFloatingVideo().removeVideo();
        }
    }

}
