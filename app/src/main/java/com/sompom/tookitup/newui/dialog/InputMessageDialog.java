package com.sompom.tookitup.newui.dialog;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;

import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.LayoutInputBinding;
import com.sompom.tookitup.utils.KeyboardUtil;
import com.sompom.tookitup.viewmodel.binding.TextViewBindingUtil;

/**
 * Created by He Rotha on 11/30/18.
 */
public class InputMessageDialog extends MessageDialog {
    private String mHint;
    private EditText mEditText;
    private Validation mValidation;

    public void setInputLayout(String defaultValue,
                               Validation validation) {
        mHint = defaultValue;
        mValidation = validation;
    }


    public String getCurrentInputText() {
        if (mEditText == null || TextUtils.isEmpty(mEditText.getText())) {
            return null;
        }
        return mEditText.getText().toString().trim();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setDismissWhenClickOnButtonLeft(false);

        final LayoutInflater inflater = LayoutInflater.from(getContext());
        LayoutInputBinding binding = DataBindingUtil.inflate(inflater, R.layout.layout_input, null, true);
        mEditText = binding.editText;
        if (mValidation == Validation.Email) {
            mEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
            mEditText.setFilters(new InputFilter[]{});
        }

        if (mValidation == Validation.Store) {
            TextViewBindingUtil.setStoreNameText(binding.title, mHint);
        } else {
            binding.title.setText(mHint);
        }
        getBinding().view.addView(binding.getRoot());

        mEditText.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                new Handler().post(() -> KeyboardUtil.showKeyboard(getActivity(), mEditText));
                mEditText.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

    }

    @Override
    public void setLeftText(String leftText, View.OnClickListener listener) {
        super.setLeftText(leftText, view -> {
            if (mEditText != null) {
                if (TextUtils.isEmpty(mEditText.getText())) {
                    mEditText.setError(getString(R.string.edit_profile_error_field_required));
                } else if (mValidation == Validation.Email
                        && !android.util.Patterns.EMAIL_ADDRESS.matcher(mEditText.getText()).matches()) {
                    mEditText.setError(getString(R.string.edit_profile_invalid_email_title));
                } else {
                    if (listener != null) {
                        listener.onClick(view);
                    }
                    dismiss();
                }
            }
        });
    }

    public enum Validation {
        Required, Email, Store
    }
}
