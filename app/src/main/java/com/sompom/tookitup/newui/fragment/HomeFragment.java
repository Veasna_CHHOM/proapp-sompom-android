package com.sompom.tookitup.newui.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.view.View;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.chat.AbsChatBinder;
import com.sompom.tookitup.chat.service.SocketService;
import com.sompom.tookitup.databinding.FragmentHomeBinding;
import com.sompom.tookitup.helper.AnimationHelper;
import com.sompom.tookitup.injection.controller.PublicQualifier;
import com.sompom.tookitup.listener.OnBadgeUpdateListener;
import com.sompom.tookitup.listener.OnHomeMenuClick;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.ProductListDataManager;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.newviewmodel.HomeFragmentViewModel;
import com.sompom.tookitup.widget.HomeTabLayout;

import javax.inject.Inject;

/**
 * Created by nuonveyo on 6/22/18.
 */

public class HomeFragment extends AbsBindingFragment<FragmentHomeBinding> implements AbsBaseActivity.OnServiceListener, OnBadgeUpdateListener {
    @Inject
    public ApiService mApiService;
    @Inject
    @PublicQualifier
    public ApiService mApiServiceTest;
    private boolean mIsLoadMessageData;
    private SocketService.SocketBinder mSocketBinder;

    public static HomeFragment newInstance(boolean isLoadMessageData) {
        Bundle args = new Bundle();
        args.putBoolean(SharedPrefUtils.STATUS, isLoadMessageData);
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_home;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        setToolbar(getBinding().toolbar, false);
        if (getArguments() != null) {
            mIsLoadMessageData = getArguments().getBoolean(SharedPrefUtils.STATUS, false);
        }
        ProductListDataManager manager = new ProductListDataManager(getActivity(),
                mApiService,
                mApiService,
                mApiService,
                mApiServiceTest);
        HomeFragmentViewModel viewModel = new HomeFragmentViewModel(manager, new HomeFragmentViewModel.OnQueryProductListener() {
            @Override
            public void onQueryProductOption(boolean isEnable) {
                AbsBaseFragment fr = getBinding().viewpager.getFragmentAt(0);
                if (fr instanceof WallStreetFragment) {
                    ((WallStreetFragment) fr).setEnableQueryOption(isEnable);
                }
            }
        });
        setVariable(BR.viewModel, viewModel);

        initTab();
        if (getActivity() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getActivity()).addOnServiceListener(this);
        }
    }

    public void checkGoToConversationTab() {
        if (!(getBinding().viewpager.getCurrentFragment() instanceof MessageFragment)) {
            getBinding().viewpager.setCurrentItem(2, true);
        }
    }

    private void initTab() {
        getBinding().viewpager.setupAdapter(getActivity(),
                getChildFragmentManager(),
                () -> getBinding().appBar.setExpanded(false),
                mIsLoadMessageData,
                getBinding().tabs);

        getBinding().tabs.setTabLayoutChangeListener(new HomeTabLayout.OnTabLayoutChangeListener() {
            @Override
            public void onPageReselect(AbsBaseFragment fragment, int position) {
                if (fragment instanceof WallStreetFragment) {
                    WallStreetFragment wallStreetFr = (WallStreetFragment) fragment;

                    //No need to show post floating button
//                    shouldScroll(wallStreetFr, false);
                }
                getBinding().appBar.setExpanded(!(fragment instanceof NearbyMapFragment), true);
            }

            @Override
            public void onPageChanged(AbsBaseFragment fragment, int position) {
                if (fragment instanceof OnHomeMenuClick) {
                    ((OnHomeMenuClick) fragment).onTabMenuClick();
                }
                getBinding().appBar.setExpanded(!(fragment instanceof NearbyMapFragment), true);

                //No need to show post floating button
//                checkToHideFloatingButton(fragment);
            }
        });
        getBinding().tabs.setupWithViewPager(getBinding().viewpager);
    }

    private void shouldScroll(WallStreetFragment wallStreetFr, boolean isPullToRefresh) {
        if (wallStreetFr.findFirstVisibleItemPosition() > 0 && !getBinding().fab.isShown()) {
            AnimationHelper.showFabButton(getBinding().fab, new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    getBinding().fab.show();
                }
            });
        }
        wallStreetFr.shouldScrollTop(isPullToRefresh);
    }


    private void checkToHideFloatingButton(Fragment fragment) {
        if (fragment instanceof WallStreetFragment) {
            if (!getBinding().fab.isShown()) {
                AnimationHelper.showFabButton(getBinding().fab, new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        getBinding().fab.show();
                    }
                });
            }
        } else {
            if (getBinding().fab.isShown()) {
                AnimationHelper.hideFabButton(getBinding().fab, new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        getBinding().fab.show();
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        getBinding().fab.hide();
                    }
                });
            } else {
                getBinding().fab.hide();
            }
        }

        if (fragment instanceof NearbyMapFragment) {
            if (((NearbyMapFragment) fragment).getCurrentBottomState() == BottomSheetBehavior.STATE_HIDDEN) {
                getBinding().textViewCurrentLocation.setVisibility(View.VISIBLE);
            }
            getBinding().textViewCurrentLocation.setOnClickListener(view -> ((NearbyMapFragment) fragment).onMoveToCurrentLocationClick());
            ((NearbyMapFragment) fragment).setOnBottomSheetStateChangeListener(state -> {
                if (state == BottomSheetBehavior.STATE_HIDDEN) {
                    getBinding().textViewCurrentLocation.setVisibility(View.VISIBLE);
                } else {
                    getBinding().textViewCurrentLocation.setVisibility(View.GONE);
                }
            });
        } else {
            getBinding().textViewCurrentLocation.setVisibility(View.GONE);
            getBinding().textViewCurrentLocation.setOnClickListener(null);
        }
    }

    /**
     * @return true: mean can back press, otherwise, cannot
     */
    public boolean onBackPress() {
        if (getBinding().viewpager.getCurrentFragment() instanceof WallStreetFragment) {
            WallStreetFragment wallStreetFr = (WallStreetFragment) getBinding().viewpager.getCurrentFragment();
            if (wallStreetFr.findFirstVisibleItemPosition() > 0) {
                //No need to show post floating button
                //shouldScroll(wallStreetFr, true);
                return false;
            }
        } else {
            getBinding().viewpager.setCurrentItem(0, true);
            return false;
        }
        return true;
    }

    @Override
    public void onServiceReady(AbsChatBinder binder) {
        mSocketBinder = (SocketService.SocketBinder) binder;
        mSocketBinder.addBadgeListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mSocketBinder != null) {
            mSocketBinder.removeBadgeListener(this);
        }
    }

    @Override
    public void onConversationBadgeUpdate(int value) {
        getBinding().tabs.addConversationBadge(value);
    }
}
