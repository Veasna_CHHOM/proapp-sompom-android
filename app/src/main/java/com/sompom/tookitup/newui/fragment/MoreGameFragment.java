package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.FragmentMoreGameBinding;
import com.sompom.tookitup.model.result.MoreGame;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.MoreGameFragmentViewModel;

/**
 * Created by he.rotha on 4/29/16.
 */
public class MoreGameFragment extends AbsBindingFragment<FragmentMoreGameBinding> {

    public static MoreGameFragment getInstance(MoreGame moreGame) {
        MoreGameFragment fragment = new MoreGameFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(SharedPrefUtils.PATH, moreGame);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_more_game;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MoreGame moreGame = null;
        if (getArguments() != null) {
            moreGame = getArguments().getParcelable(SharedPrefUtils.PATH);
        }
        MoreGameFragmentViewModel viewModel = new MoreGameFragmentViewModel(getActivity(), moreGame);
        setVariable(BR.viewModel, viewModel);
    }

}
