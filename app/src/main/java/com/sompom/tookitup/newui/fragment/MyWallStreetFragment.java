package com.sompom.tookitup.newui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.desmond.squarecamera.utils.Keys;
import com.sompom.baseactivity.ResultCallback;
import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.newadapter.TimelineAdapter;
import com.sompom.tookitup.databinding.FragmentMyWallStreetBinding;
import com.sompom.tookitup.helper.OpenCommentDialogHelper;
import com.sompom.tookitup.injection.mywallserialize.productserialize.MyWallSerializeQualifier;
import com.sompom.tookitup.intent.CreateTimelineIntent;
import com.sompom.tookitup.intent.CreateTimelineIntentResult;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnTabSelectListener;
import com.sompom.tookitup.listener.OnTimelineItemButtonClickListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.LifeStream;
import com.sompom.tookitup.model.WallStreetAdaptive;
import com.sompom.tookitup.model.emun.ContentType;
import com.sompom.tookitup.model.emun.CreateWallStreetScreenType;
import com.sompom.tookitup.model.emun.FollowItemType;
import com.sompom.tookitup.model.emun.PostContextMenuItemType;
import com.sompom.tookitup.model.emun.TimelinePostItem;
import com.sompom.tookitup.model.result.LoadMoreWrapper;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.dialog.ListFollowDialog;
import com.sompom.tookitup.newui.dialog.PostContextMenuDialog;
import com.sompom.tookitup.newui.dialog.ReportDialog;
import com.sompom.tookitup.newui.dialog.ShareLinkMenuDialog;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.WallStreetDataManager;
import com.sompom.tookitup.utils.BetterSmoothScrollUtil;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.utils.TimelinePostItemUtil;
import com.sompom.tookitup.viewmodel.newviewmodel.MyWallStreetFragmentViewModel;
import com.sompom.tookitup.widget.LoaderMoreLayoutManager;
import com.sompom.videomanager.widget.MediaRecyclerView;

import javax.inject.Inject;

/**
 * Created by nuonveyo on 8/1/18.
 */

public class MyWallStreetFragment extends AbsBindingFragment<FragmentMyWallStreetBinding>
        implements OnTimelineItemButtonClickListener, OnTabSelectListener {
    @MyWallSerializeQualifier
    @Inject
    public ApiService mSerialApiService;
    @Inject
    public ApiService mApiService;

    private MyWallStreetFragmentViewModel mViewModel;
    private LoaderMoreLayoutManager mLoadMoreLayoutManager;
    private TimelineAdapter mTimelineAdapter;
    private OpenCommentDialogHelper mCommentDialog;
    private boolean mIsTabSelected = true;

    public static MyWallStreetFragment newInstance(String userId) {
        MyWallStreetFragment fr = new MyWallStreetFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Keys.ACTOR, userId);
        fr.setArguments(bundle);
        return fr;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_my_wall_street;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);

        String userId = getArguments().getString(Keys.ACTOR);

        initLayout();

        WallStreetDataManager dataManager = new WallStreetDataManager(getActivity(), mApiService, mSerialApiService, null);
        mViewModel = new MyWallStreetFragmentViewModel(userId, dataManager, (result, canLoadMore) -> {
            mTimelineAdapter = new TimelineAdapter((AbsBaseActivity) getActivity(),
                    mApiService,
                    result,
                    MyWallStreetFragment.this);
            mTimelineAdapter.setAdViews(dataManager.getAdViews());
            mTimelineAdapter.setCanLoadMore(canLoadMore);
            getBinding().recyclerView.setAdapter(mTimelineAdapter);
        });
        setVariable(BR.viewModel, mViewModel);

    }

    @Override
    public void onCommentButtonClick(Adaptive adaptive, int position) {
        onCommentClick(adaptive);
    }

    @Override
    public void onShareButtonClick(Adaptive adaptive, int position) {
        ShareLinkMenuDialog dialog = ShareLinkMenuDialog.newInstance((WallStreetAdaptive) adaptive);
        dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
    }

    @Override
    public void onUserViewMediaClick(Adaptive adaptive) {
        ListFollowDialog dialog = ListFollowDialog.newInstance(adaptive, FollowItemType.USER_ACTION);
        dialog.show(getChildFragmentManager(), ListFollowDialog.TAG);
    }

    @Override
    public void onLikeViewerClick(Adaptive adaptive) {
        ListFollowDialog dialog = ListFollowDialog.newInstance(adaptive, FollowItemType.LIKE_VIEWER);
        dialog.show(getChildFragmentManager(), ListFollowDialog.TAG);
    }

    @Override
    public void onFollowButtonClick(String id, boolean isFollowing) {
        mViewModel.checkToPostFollow(id, isFollowing);
    }

    @Override
    public void onShowPostContextMenuClick(Adaptive adaptive, User user, int position) {
        if (user == null) {
            return;
        }

        TimelinePostItem timelinePostItem = TimelinePostItemUtil.getPostItem(adaptive);
        LifeStream lifeStream = TimelinePostItemUtil.getLifeStream(adaptive);
        PostContextMenuDialog dialog = PostContextMenuDialog.newInstance();
        dialog.addItem(PostContextMenuItemType.getItems(adaptive, SharedPrefUtils.checkIsMe(getActivity(), user.getId()), timelinePostItem));
        dialog.setOnItemClickListener(result -> {
            PostContextMenuItemType itemType = PostContextMenuItemType.getItemType(result);
            if (itemType == PostContextMenuItemType.EDIT_POST_CLICK) {
                startCreateTimelineActivity(adaptive, position);
            } else if (itemType == PostContextMenuItemType.UNFOLLOW_CLICK) {
                mViewModel.checkToPostFollow(user.getId(), false);
                if (adaptive instanceof WallStreetAdaptive && ((WallStreetAdaptive) adaptive).getUser() != null) {
                    ((WallStreetAdaptive) adaptive).getUser().setFollow(false);
                    mTimelineAdapter.update(adaptive);
                }
            } else if (itemType == PostContextMenuItemType.EDIT_PRIVACY_CLICK) {
                showEditPrivacyDialog(result1 -> {
                    //TODO: implement api edit privacy
                });
            } else if (itemType == PostContextMenuItemType.REPORT_CLICK) {
                if (lifeStream != null) {
                    ReportDialog reportDialog = ReportDialog.newInstance(ContentType.POST);
                    reportDialog.setOnReportClickListener(reportRequest -> mViewModel.reportPost(lifeStream.getId(), reportRequest));
                    reportDialog.show(getChildFragmentManager());
                }
            } else if (itemType == PostContextMenuItemType.TURNOFF_NOTIFICATION_CLICK) {
                mViewModel.unFollowShop(adaptive.getId());
            } else if (itemType == PostContextMenuItemType.DELETE_CLICK) {
                if (lifeStream != null) {
                    showMessageDialog(R.string.post_menu_delete_post_title,
                            R.string.post_menu_delete_post_description,
                            R.string.seller_store_button_delete,
                            R.string.change_language_button_cancel,
                            view -> {
                                mTimelineAdapter.removeData(position);
                                mViewModel.deletePost(lifeStream);
                            });
                }
            }
            dialog.dismiss();

        });
        dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
    }

    @Override
    public void onCommentClick(Adaptive adaptive) {
        if (mCommentDialog == null) {
            mCommentDialog = new OpenCommentDialogHelper(getActivity());
        }
        mCommentDialog.show(adaptive, mTimelineAdapter);
    }

    @Override
    public MediaRecyclerView getRecyclerView() {
        return getBinding().recyclerView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mIsTabSelected) {
            getBinding().recyclerView.onResume();
            if (getActivity() instanceof AbsBaseActivity) {
                AbsBaseActivity base = (AbsBaseActivity) getActivity();
                if (base.getFloatingVideo() != null && base.getFloatingVideo().isFloating()) {
                    getBinding().recyclerView.setAutoPlay(false);
                    getBinding().recyclerView.setAutoResume(false);
                    base.setOnFloatingVideoRemoveListener(() -> {
                        getBinding().recyclerView.setAutoPlay(true);
                        getBinding().recyclerView.setAutoResume(true);
                        getBinding().recyclerView.resume();
                    });
                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        getBinding().recyclerView.onPause();
    }

    private void initLayout() {
        mLoadMoreLayoutManager = new LoaderMoreLayoutManager(getActivity());
        mLoadMoreLayoutManager.setOnLoadMoreListener(() -> {
            getBinding().recyclerView.stopScroll();
            mViewModel.loadMoreData(new OnCallbackListListener<LoadMoreWrapper<Adaptive>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    mTimelineAdapter.setCanLoadMore(false);
                    mTimelineAdapter.notifyDataSetChanged();
                    mLoadMoreLayoutManager.loadingFinished();
                    mLoadMoreLayoutManager.setOnLoadMoreListener(null);
                }

                @Override
                public void onComplete(LoadMoreWrapper<Adaptive> result, boolean canLoadMore) {
                    mTimelineAdapter.addLoadMoreData(result.getData());
                    mTimelineAdapter.setCanLoadMore(canLoadMore);
                    mLoadMoreLayoutManager.loadingFinished();
                }
            });
        });

        getBinding().recyclerView.setLayoutManager(mLoadMoreLayoutManager);
    }

    public void shouldScrollTop(boolean isPullToRefresh) {
        if (findFirstVisibleItemPosition() != 0) {
            if (isPullToRefresh) {
                mViewModel.onRefresh();
            }
            BetterSmoothScrollUtil.startScroll(getBinding().recyclerView,
                    0,
                    findFirstVisibleItemPosition());
        } else {
            mViewModel.onRefresh();
        }
    }

    public int findFirstVisibleItemPosition() {
        return mLoadMoreLayoutManager.findFirstVisibleItemPosition();
    }

    private void startCreateTimelineActivity(Adaptive adaptive, int position) {
        if (getActivity() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getActivity()).startActivityForResult(new CreateTimelineIntent(getActivity(),
                            (WallStreetAdaptive) adaptive,
                            CreateWallStreetScreenType.EDIT_POST),
                    new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            CreateTimelineIntentResult result1 = new CreateTimelineIntentResult(data);
                            mTimelineAdapter.updateItem(result1.getLifeStream(), position);
                        }
                    });
        }
    }

    @Override
    public void onTabSelected(boolean isSelected) {
        mIsTabSelected = isSelected;
    }
}
