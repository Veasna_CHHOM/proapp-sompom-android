package com.sompom.tookitup.newui;

import android.os.Bundle;

import com.sompom.tookitup.intent.DeeplinkIntent;
import com.sompom.tookitup.intent.newintent.ProductDetailIntent;
import com.sompom.tookitup.intent.newintent.SellerStoreIntent;
import com.sompom.tookitup.intent.newintent.TimelineDetailIntent;
import com.sompom.tookitup.model.emun.DeeplinkType;
import com.sompom.tookitup.newui.fragment.SplashFragment;
import com.sompom.tookitup.utils.SharedPrefUtils;

public class SplashActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Move from Home activity
        DeeplinkIntent intent = new DeeplinkIntent(getIntent());
        DeeplinkType deepLink = intent.getDeeplinkType();
        if (deepLink != DeeplinkType.None) {
            String id = intent.getId();
            if (deepLink == DeeplinkType.Feed) {
                startActivity(new TimelineDetailIntent(getThis(), id));
            } else if (deepLink == DeeplinkType.Product) {
                startActivity(new ProductDetailIntent(getThis(), id, false, false));
            } else if (deepLink == DeeplinkType.User) {
                startActivity(new SellerStoreIntent(getThis(), id));
            }
        }

        setFragment(SplashFragment.newInstance(), SplashFragment.class.getSimpleName());
    }
}
