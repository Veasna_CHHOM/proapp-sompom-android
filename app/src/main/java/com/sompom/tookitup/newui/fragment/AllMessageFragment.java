package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SimpleItemAnimator;
import android.text.TextUtils;
import android.view.View;

import com.google.gson.Gson;
import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.newadapter.AllMessageAdapter;
import com.sompom.tookitup.chat.AbsChatBinder;
import com.sompom.tookitup.chat.listener.ChannelListener;
import com.sompom.tookitup.chat.service.SocketService;
import com.sompom.tookitup.databinding.FragmentAllMessageBinding;
import com.sompom.tookitup.intent.ChatIntent;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnHomeMenuClick;
import com.sompom.tookitup.listener.OnMessageItemClick;
import com.sompom.tookitup.model.ActiveUserWrapper;
import com.sompom.tookitup.model.emun.PostContextMenuItemType;
import com.sompom.tookitup.model.emun.SegmentedControlItem;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.dialog.PostContextMenuDialog;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.AllMessageDataManager;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.newviewmodel.AllMessageFragmentViewModel;
import com.sompom.tookitup.widget.LoaderMoreLayoutManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by nuonveyo on 6/26/18.
 */

public class AllMessageFragment extends AbsBindingFragment<FragmentAllMessageBinding>
        implements OnMessageItemClick,
        AllMessageFragmentViewModel.OnCallback,
        AbsBaseActivity.OnServiceListener,
        ChannelListener,
        OnHomeMenuClick {

    @Inject
    public ApiService mApiService;
    private AllMessageAdapter mAllMessageAdapter;
    private SocketService.SocketBinder mSocketBinder;
    private LoaderMoreLayoutManager mLayoutManager;
    private AllMessageFragmentViewModel mViewModel;
    private SegmentedControlItem mItemType = SegmentedControlItem.All;
    private boolean mIsShow;

    public static AllMessageFragment newInstance(SegmentedControlItem itemType) {
        Bundle bundle = new Bundle();
        bundle.putInt(SharedPrefUtils.ID, itemType.getId());
        AllMessageFragment fragment = new AllMessageFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_all_message;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);

        if (getArguments() != null) {
            int id = getArguments().getInt(SharedPrefUtils.ID);
            mItemType = SegmentedControlItem.getSegmentedControlItem(id);
        }
        if (mItemType == SegmentedControlItem.All) {
            initView();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mSocketBinder != null) {
            mSocketBinder.removeChannelListener(this);
        }
    }

    @Override
    public void onConversationClick(Conversation conversation) {
        Timber.i("onConversationClick: " + new Gson().toJson(conversation));
        User recipient = conversation.getRecipient(getContext());

        if (recipient != null && !TextUtils.isEmpty(recipient.getId())) {
            ChatIntent chatIntent = new ChatIntent(getActivity(), conversation);
            startActivity(chatIntent);
        }
    }

    @Override
    public void onConversationLongClick(Conversation conversation) {
        PostContextMenuDialog dialog = PostContextMenuDialog.newInstance();
        dialog.addItem(PostContextMenuItemType.getConversationPopupMenuItem());
        dialog.setOnItemClickListener(result -> {
            dialog.dismiss();
            PostContextMenuItemType itemType = PostContextMenuItemType.getItemType(result);
            if (itemType == PostContextMenuItemType.ARCHIVE_CONVERSATION) {
                mSocketBinder.archivedConversation(conversation.getId());
            }

        });
        dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
    }

    @Override
    public void onConversationUserClick(User user) {
        ChatIntent chatIntent = new ChatIntent(getActivity(), user);
        startActivity(chatIntent);
    }

    @Override
    public void getGetRecentMessageSuccess(List<Conversation> chatMessages, boolean isCanLoadMore) {
        setAdapter(chatMessages, isCanLoadMore);
    }

    @Override
    public void onGetActivateSellerStoreSuccess(ActiveUserWrapper seller, int index) {
        if (mAllMessageAdapter == null) {
            setAdapter(new ArrayList<>(), false);
        }

        final boolean isScrollToTop = mLayoutManager.findFirstVisibleItemPosition() == 0;
        mAllMessageAdapter.addChatSeller(seller, index);
        if (isScrollToTop) {
            getBinding().recyclerViewRecentMessage.scrollToPosition(0);
        }
    }


    @Override
    public boolean isHasData() {
        return mAllMessageAdapter != null && mAllMessageAdapter.getItemCount() > 0;
    }

    @Override
    public void onServiceReady(AbsChatBinder binder) {
        mSocketBinder = (SocketService.SocketBinder) binder;
        mSocketBinder.addChannelListener(this);
    }

    @Override
    public void onChannelCreateOrUpdate(Conversation channel) {
        if (getActivity() == null) {
            return;
        }
        getActivity().runOnUiThread(() -> {
            if (mAllMessageAdapter == null) {
                if (mViewModel.mIsLoading.get()) {
                    return;
                } else if (mViewModel.mIsError.get()) {
                    setAdapter(new ArrayList<>(), false);
                }
            }

            mAllMessageAdapter.updateOrCreateConversation(channel);
            int currentVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
            if (currentVisibleItem > -1 && currentVisibleItem < mAllMessageAdapter.getItemCount()) {
                getBinding().recyclerViewRecentMessage.scrollToPosition(0);
            }
        });

    }

    @Override
    public void onChannelRemoved(String channelId) {
        if (getActivity() == null) {
            return;
        }
        getActivity().runOnUiThread(() -> {
            if (mAllMessageAdapter == null) {
                return;
            }
            mAllMessageAdapter.removeConversation(channelId);
        });
    }

    @Override
    public SegmentedControlItem getCurrentSegmentedControlItem() {
        return mItemType;
    }

    @Override
    public void onTabMenuClick() {
        if (mIsShow) {
            return;
        }
        initView();
    }

    private void setAdapter(List<Conversation> chatMessages, boolean isCanLoadMore) {
        mAllMessageAdapter = new AllMessageAdapter(getActivity(), new ArrayList<>(chatMessages), AllMessageFragment.this, mItemType);
        mAllMessageAdapter.setCanLoadMore(isCanLoadMore);
        mLayoutManager = new LoaderMoreLayoutManager(getActivity());
        mLayoutManager.setOnLoadMoreListener(() -> {
            getBinding().recyclerViewRecentMessage.stopScroll();
            mViewModel.loadMore(new OnCallbackListListener<List<Conversation>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    if (mAllMessageAdapter == null) {
                        return;
                    }
                    mAllMessageAdapter.setCanLoadMore(false);
                    mAllMessageAdapter.notifyDataSetChanged();
                    mLayoutManager.loadingFinished();
                    mLayoutManager.setOnLoadMoreListener(null);
                }

                @Override
                public void onComplete(List<Conversation> result, boolean canLoadMore) {
                    if (mAllMessageAdapter == null) {
                        return;
                    }
                    mAllMessageAdapter.addLoadMoreData(new ArrayList<>(result));
                    mAllMessageAdapter.setCanLoadMore(canLoadMore);
                    mLayoutManager.loadingFinished();
                    if (!canLoadMore) {
                        mLayoutManager.setOnLoadMoreListener(null);
                    }
                }
            });
        });
        getBinding().recyclerViewRecentMessage.setLayoutManager(mLayoutManager);
        getBinding().recyclerViewRecentMessage.setAdapter(mAllMessageAdapter);
    }

    private void initView() {
        mIsShow = true;
        if (getBinding().recyclerViewRecentMessage.getItemAnimator() != null) {
            ((SimpleItemAnimator) getBinding().recyclerViewRecentMessage.getItemAnimator()).setSupportsChangeAnimations(false);
        }
        AllMessageDataManager dataManager = new AllMessageDataManager(getActivity(),
                mApiService,
                mItemType);
        mViewModel = new AllMessageFragmentViewModel(dataManager,
                this,
                mItemType);
        setVariable(BR.viewModel, mViewModel);

        if (getActivity() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getActivity()).addOnServiceListener(this);
        }
    }
}
