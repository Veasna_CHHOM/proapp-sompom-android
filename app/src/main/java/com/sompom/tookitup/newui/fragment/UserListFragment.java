package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.newadapter.UserListAdapter;
import com.sompom.tookitup.databinding.FragmentUserListBinding;
import com.sompom.tookitup.listener.OnHomeMenuClick;
import com.sompom.tookitup.listener.UserListAdaptive;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.UserListManager;
import com.sompom.tookitup.viewmodel.newviewmodel.UserListViewModel;

import java.util.List;

import javax.inject.Inject;

public class UserListFragment extends AbsBindingFragment<FragmentUserListBinding>
        implements UserListViewModel.UserListViewModelListener, OnHomeMenuClick {

    private UserListViewModel mViewModel;
    @Inject
    public ApiService mApiService;
    private boolean mIsShow;

    public static UserListFragment newInstance() {

        Bundle args = new Bundle();

        UserListFragment fragment = new UserListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_user_list;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        getControllerComponent().inject(this);
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onLoadSuccess(List<UserListAdaptive> userGroups) {
        UserListAdapter adapter = new UserListAdapter(userGroups);
        getBinding().recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        getBinding().recyclerView.setAdapter(adapter);
    }

    @Override
    public void onTabMenuClick() {
        if (mIsShow || !isAdded()) {
            return;
        }
        mIsShow = true;
        initViewModel();
    }

    private void initViewModel() {
        mViewModel = new UserListViewModel(requireContext(),
                new UserListManager(requireContext(), mApiService),
                this);
        setVariable(BR.viewModel, mViewModel);
    }
}
