package com.sompom.tookitup.newui;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.sompom.tookitup.intent.newintent.ProductFormIntent;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.newui.fragment.ProductFormFragment;

public class ProductFormActivity extends AbsDefaultActivity {

    private boolean mIsEnableBackPress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ProductFormIntent intent = new ProductFormIntent(getIntent());

        Product product = intent.getProduct();
        Fragment fragment;
        if (product == null) {
            fragment = ProductFormFragment.newInstance(intent.getImagePaths());
        } else {
            fragment = ProductFormFragment.newInstance(product);
        }
        setFragment(fragment, ProductFormFragment.TAG);
    }

    public void setEnableBackPressed() {
        mIsEnableBackPress = true;
    }

    @Override
    public void onBackPressed() {
        if (mIsEnableBackPress) {
            super.onBackPressed();
        }
    }
}
