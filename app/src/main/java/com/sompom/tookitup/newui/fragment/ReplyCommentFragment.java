package com.sompom.tookitup.newui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.newadapter.ReplyCommentAdapter;
import com.sompom.tookitup.databinding.FragmentReplyCommentBinding;
import com.sompom.tookitup.helper.LoginActivityResultCallback;
import com.sompom.tookitup.injection.controller.PublicQualifier;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnCommentDialogClickListener;
import com.sompom.tookitup.listener.OnCommentItemClickListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.LoadPreviousComment;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.emun.MediaType;
import com.sompom.tookitup.model.emun.PostContextMenuItemType;
import com.sompom.tookitup.model.result.Comment;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.dialog.MessageDialog;
import com.sompom.tookitup.newui.dialog.PostContextMenuDialog;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.PopUpCommentDataManager;
import com.sompom.tookitup.utils.CopyTextUtil;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.newviewmodel.ReplyCommentFragmentViewModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;

/**
 * Created by nuonveyo on 7/20/18.
 */

public class ReplyCommentFragment extends AbsBindingFragment<FragmentReplyCommentBinding>
        implements ReplyCommentFragmentViewModel.OnCallback,
        OnCommentItemClickListener {
    public static final String TAG = ReplyCommentFragment.class.getName();
    @Inject
    public ApiService mApiService;
    @Inject
    @PublicQualifier
    public ApiService mApiServiceTest;
    private OnCommentDialogClickListener mOnCommentDialogClickListener;
    private ReplyCommentFragmentViewModel mViewModel;
    private ReplyCommentAdapter mReplyCommentAdapter;
    private Comment mComment;
    private int mPosition;
    private String mContentId;
    private boolean mIsCanLoadMore;

    public static ReplyCommentFragment newInstance(String itemID) {
        Bundle args = new Bundle();
        args.putString(SharedPrefUtils.ID, itemID);
        ReplyCommentFragment fragment = new ReplyCommentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static ReplyCommentFragment newInstance(Comment comment) {
        Bundle args = new Bundle();
        args.putParcelable(SharedPrefUtils.DATA, comment);
        ReplyCommentFragment fragment = new ReplyCommentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnCommentDialogClickListener(OnCommentDialogClickListener onCommentDialogClickListener) {
        mOnCommentDialogClickListener = onCommentDialogClickListener;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_reply_comment;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (enter && nextAnim > 0) {
            Animation anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    bindViewModel();
                }
            });
            return anim;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        bindGifView();
        getBinding().loadingLayout.loading.setVisibility(View.VISIBLE);

        if (getArguments() != null) {
            mContentId = getArguments().getString(SharedPrefUtils.ID);
            mComment = getArguments().getParcelable(SharedPrefUtils.DATA);
            if (mComment != null) {
                bindViewModel();
            }
        }

        getBinding().textviewClose.setOnClickListener(v -> {
            if (mOnCommentDialogClickListener != null) {
                mOnCommentDialogClickListener.onDismissDialog(false);
            }
        });
    }

    public void setData(Comment comment, int position) {
        getBinding().loadingLayout.loading.setVisibility(View.VISIBLE);
        mPosition = position;
        mComment = comment;
    }

    private void bindViewModel() {
        PopUpCommentDataManager dataManager = new PopUpCommentDataManager(getActivity(), mApiService, mApiServiceTest);
        mViewModel = new ReplyCommentFragmentViewModel(mContentId,
                mComment,
                dataManager,
                ReplyCommentFragment.this);
        setVariable(BR.viewModel, mViewModel);
        requestShowKeyboard();
    }

    @Override
    public void onReplyButtonClick(Comment comment, int position) {
        requestShowKeyboard();
    }

    @Override
    public void onLikeButtonClick(Comment comment, int position) {
        mViewModel.checkToLikeComment(comment, position);
        if (position == 0) {
            mComment.setContentStat(mComment.getContentStat());
            mComment.setLike(comment.isLike());
            if (mOnCommentDialogClickListener != null) {
                mOnCommentDialogClickListener.onNotifyCommentItemChange(mComment);
            }
        }
    }

    @Override
    public void onLongPress(Comment comment, int position) {
        PostContextMenuDialog dialog = PostContextMenuDialog.newInstance();
        boolean isOwner = comment.getUser() != null && SharedPrefUtils.checkIsMe(getActivity(), comment.getUser().getId());
        dialog.addItem(PostContextMenuItemType.getCommentPopupMenuItem(isOwner));
        dialog.setOnItemClickListener(result -> {
            dialog.dismiss();
            PostContextMenuItemType itemType = PostContextMenuItemType.getItemType(result);
            if (itemType == PostContextMenuItemType.COPY_TEXT_CLICK) {
                CopyTextUtil.copyTextToClipboard(getActivity(), comment.getContent());
            } else if (itemType == PostContextMenuItemType.EDIT_POST_CLICK) {
                requestShowKeyboard();
                getBinding().includeComment.messageInput.setText(comment.getContent());
                mViewModel.setUpdateComment(comment, position);

            } else {
                MessageDialog messageDialog = new MessageDialog();
                messageDialog.setTitle(getString(R.string.comment_delete_title));
                messageDialog.setMessage(getString(R.string.comment_delete_message));
                messageDialog.setRightText(getString(R.string.change_language_button_cancel), null);
                messageDialog.setLeftText(getString(R.string.seller_store_button_delete),
                        v -> mViewModel.onDeleteComment(comment, position, () -> {
                            if (position == 0) {
                                if (mOnCommentDialogClickListener != null) {
                                    mOnCommentDialogClickListener.onDismissAndRemoveItem(mPosition);
                                }
                            } else {
                                mReplyCommentAdapter.notifyItemRemove(position);
                                mComment.getReplyComment().remove(position - 1);
                                int totalSubComment = mComment.getTotalSubComment() - 1;
                                mComment.setTotalSubComment(totalSubComment);
                                if (mOnCommentDialogClickListener != null) {
                                    mOnCommentDialogClickListener.onNotifyCommentItemChange(mComment);
                                }
                            }
                        }));
                messageDialog.show(getChildFragmentManager());
            }
        });
        dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
    }

    @Override
    public void onPause() {
        super.onPause();
        getBinding().includeComment.messageInput.removeListPopupWindow();
    }

    private void bindGifView() {
        getBinding().includeComment.containerSearchGif.onBackPressClickListener(v -> {
            getBinding().includeComment.sendLayout.setVisibility(View.VISIBLE);
            getBinding().includeComment.messageInput.requestFocus();
        });

        getBinding().includeComment.gifImageView.setOnClickListener(v -> {
            getBinding().includeComment.sendLayout.setVisibility(View.GONE);
            getBinding().includeComment.containerSearchGif.loadTrendingGifList(baseGifModel -> {
                getBinding().includeComment.sendLayout.setVisibility(View.VISIBLE);
                getBinding().includeComment.messageInput.requestFocus();

                AbsBaseActivity absBaseActivity = (AbsBaseActivity) getActivity();
                if (absBaseActivity != null) {
                    absBaseActivity.startLoginWizardActivity(new LoginActivityResultCallback() {
                        @Override
                        public void onActivityResultSuccess(boolean isAlreadyLogin) {
                            Media media = new Media();
                            media.setWidth(baseGifModel.getGifWidth());
                            media.setHeight(baseGifModel.getGifHeight());
                            media.setUrl(baseGifModel.getGifUrl());
                            media.setType(MediaType.TENOR_GIF);

                            Comment comment = new Comment();
                            comment.setContent("gif");
                            comment.setUser(SharedPrefUtils.getUser(getActivity()));
                            comment.setDate(new Date());
                            comment.setMedia(media);
                            onSendText(comment);
                            mViewModel.postComment(comment);

                        }
                    });
                }
            });
        });
    }

    public boolean isCloseSearchGifView() {
        if (getBinding().includeComment.containerSearchGif.getVisibility() == View.VISIBLE) {
            getBinding().includeComment.containerSearchGif.clearData();
            getBinding().includeComment.sendLayout.setVisibility(View.VISIBLE);
            getBinding().includeComment.messageInput.requestFocus();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onSendText(Comment comment) {
        if (mReplyCommentAdapter != null) {
            mReplyCommentAdapter.addLatestComment(comment);
            getBinding().recyclerView.scrollToPosition(mReplyCommentAdapter.getItemCount() - 1);
        }
    }

    @Override
    public void onPostCommentFail() {
        if (mReplyCommentAdapter != null) {
            mReplyCommentAdapter.removeInstanceComment();
        }
    }

    @Override
    public void onPostCommentSuccess(Comment comment) {

    }

    @Override
    public void onPostSubCommentSuccess(List<Comment> subCommentList) {
        mReplyCommentAdapter.removeInstanceComment();
        mReplyCommentAdapter.addNewComment(subCommentList);
        updateTotalCommentCount(subCommentList);
    }

    private void updateTotalCommentCount(List<Comment> subCommentList) {
        List<Comment> commentList = mComment.getReplyComment();
        if (commentList == null) {
            commentList = new ArrayList<>();
        }
        commentList.addAll(subCommentList);
        mComment.setReplyComment(commentList);
        mComment.setTotalSubComment(mComment.getTotalSubComment() + subCommentList.size());
        mOnCommentDialogClickListener.onNotifyCommentItemChange(mComment);
    }

    @Override
    public void onUpdateComment(List<Comment> newSubCommentList, int position) {
        mReplyCommentAdapter.addNewComment(newSubCommentList);
        updateTotalCommentCount(newSubCommentList);
    }

    @Override
    public void onUpdateCommentSuccess(Comment comment, int position) {
        hideSoftKeyboard();
        mReplyCommentAdapter.notifyItemChanged(position, comment);
        if (position == 0) {
            mComment.setContent(comment.getContent());
            mOnCommentDialogClickListener.onNotifyCommentItemChange(mComment);
        }

    }

    @Override
    public void onLoadCommentSuccess(List<Comment> listComment, boolean isCanLoadMore) {
        mIsCanLoadMore = isCanLoadMore;
        getBinding().recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        getBinding().recyclerView.setOnTouchListener((v, e) -> {
            hideSoftKeyboard();
            v.performClick();
            return false;
        });

        List<Adaptive> adaptiveList = new ArrayList<>();
        adaptiveList.add(mComment);
        if (mIsCanLoadMore) {
            adaptiveList.add(new LoadPreviousComment());
        }
        if (listComment != null) {
            adaptiveList.addAll(listComment);
        }
        mReplyCommentAdapter = new ReplyCommentAdapter(adaptiveList,
                this,
                (loadPreviousComment, position) -> mViewModel.loadMore(new OnCallbackListListener<Response<List<Comment>>>() {
                    @Override
                    public void onFail(ErrorThrowable ex) {
                        mReplyCommentAdapter.checkToRemoveLoadPreviousItem(mIsCanLoadMore, loadPreviousComment, position);
                    }

                    @Override
                    public void onComplete(Response<List<Comment>> result, boolean canLoadMore) {
                        mIsCanLoadMore = canLoadMore;
                        mReplyCommentAdapter.checkToRemoveAndAddNewComment(mIsCanLoadMore, loadPreviousComment, position, result.body());
                    }
                }));
        mReplyCommentAdapter.setCanLoadMore(false);
        getBinding().recyclerView.scrollToPosition(adaptiveList.size() - 1);
        getBinding().recyclerView.setAdapter(mReplyCommentAdapter);
    }

    private void hideSoftKeyboard() {
        if (getContext() != null) {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(getBinding().includeComment.messageInput.getWindowToken(), 0);
            }
        }
    }

    private void requestShowKeyboard() {
        getBinding().includeComment.messageInput.getMentionsEditText().post(() ->
                showKeyboard(getBinding().includeComment.messageInput.getMentionsEditText()));
    }
}
