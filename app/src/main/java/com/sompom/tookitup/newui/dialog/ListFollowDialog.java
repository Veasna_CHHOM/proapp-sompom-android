package com.sompom.tookitup.newui.dialog;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.view.View;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.newadapter.LikeViewerAdapter;
import com.sompom.tookitup.databinding.DialogListFollowBinding;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnCompleteListListener;
import com.sompom.tookitup.listener.OnLikeItemListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.emun.FollowItemType;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.StoreDataManager;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.newviewmodel.ListFollowDialogViewModel;
import com.sompom.tookitup.widget.LoaderMoreLayoutManager;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by nuonveyo on 7/10/18.
 */

public class ListFollowDialog extends AbsBindingFragmentDialog<DialogListFollowBinding>
        implements OnCompleteListListener<List<User>> {

    @Inject
    public ApiService mApiService;
    private ListFollowDialogViewModel mListFollowDialogViewModel;
    private LikeViewerAdapter mViewerAdapter;
    private OnCallbackListener mOnCallbackListener;
    private FollowItemType mFollowItemType;

    public static ListFollowDialog newInstance(User user, FollowItemType type) {
        Bundle args = new Bundle();
        args.putInt(SharedPrefUtils.ID, type.ordinal());
        args.putParcelable(SharedPrefUtils.DATA, user);
        ListFollowDialog fragment = new ListFollowDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public static ListFollowDialog newInstance(Adaptive adaptive, FollowItemType type) {
        Bundle args = new Bundle();
        args.putInt(SharedPrefUtils.ID, type.ordinal());
        args.putParcelable(SharedPrefUtils.PRODUCT, (Parcelable) adaptive);
        ListFollowDialog fragment = new ListFollowDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_list_follow;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        if (getArguments() != null) {
            User user = getArguments().getParcelable(SharedPrefUtils.DATA);
            Adaptive adaptive = getArguments().getParcelable(SharedPrefUtils.PRODUCT);
            mFollowItemType = FollowItemType.values()[getArguments().getInt(SharedPrefUtils.ID)];

            StoreDataManager dataManager = new StoreDataManager(getActivity(), mApiService);
            mListFollowDialogViewModel = new ListFollowDialogViewModel(dataManager,
                    adaptive,
                    user,
                    mFollowItemType,
                    this);
            setVariable(BR.viewModel, mListFollowDialogViewModel);
        }
        getBinding().textviewClose.setOnClickListener(v -> dismiss());
    }

    @Override
    public void onComplete(List<User> result, boolean canLoadMore) {
        LoaderMoreLayoutManager layoutManager = new LoaderMoreLayoutManager(getActivity());
        if (canLoadMore) {
            layoutManager.setOnLoadMoreListener(() -> mListFollowDialogViewModel.loadMore(new OnCallbackListListener<List<User>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    mViewerAdapter.setCanLoadMore(false);
                    layoutManager.setOnLoadMoreListener(null);
                    layoutManager.loadingFinished();
                }

                @Override
                public void onComplete(List<User> result, boolean canLoadMore) {
                    mViewerAdapter.addLoadMoreData(result);
                    mViewerAdapter.setCanLoadMore(canLoadMore);
                    layoutManager.loadingFinished();
                }
            }));
        }
        getBinding().recyclerview.setLayoutManager(layoutManager);

        mViewerAdapter = new LikeViewerAdapter(getActivity(),
                LikeViewerAdapter.ItemType.FOLLOW_ITEM,
                result,
                mFollowItemType,
                new OnLikeItemListener() {
                    @Override
                    public void onFollowClick(User user, boolean isFollow) {
                        mListFollowDialogViewModel.onFollowClick(user, isFollow);
                        if (mOnCallbackListener != null) {
                            mOnCallbackListener.onUpdateFollowing(isFollow);
                        }
                    }

                    @Override
                    public void onNotifyItem() {
                        if (mViewerAdapter != null) {
                            mViewerAdapter.notifyData();
                        }
                    }
                });
        mViewerAdapter.setCanLoadMore(canLoadMore);
        getBinding().recyclerview.setAdapter(mViewerAdapter);
    }

    public void setOnCallbackListener(OnCallbackListener onCallbackListener) {
        mOnCallbackListener = onCallbackListener;
    }

    public interface OnCallbackListener {
        void onUpdateFollowing(boolean isFollowing);
    }
}
