package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.FragmentSearchGeneralContainerBinding;
import com.sompom.tookitup.viewmodel.newviewmodel.SearchGeneralContainerFragmentViewModel;

/**
 * Created by nuonveyo on 7/2/18.
 */

public class SearchGeneralContainerFragment extends AbsBindingFragment<FragmentSearchGeneralContainerBinding> {
    private SearchGeneralContainerResultFragment mResultFragment;

    public static SearchGeneralContainerFragment newInstance() {
        return new SearchGeneralContainerFragment();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_search_general_container;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setToolbar(getBinding().toolbar, false);
        replaceFragment(SearchGeneralFragment.newInstance(),
                SearchGeneralFragment.TAG);

        SearchGeneralContainerFragmentViewModel viewModel = new SearchGeneralContainerFragmentViewModel(getActivity(),
                new SearchGeneralContainerFragmentViewModel.Callback() {
                    @Override
                    public void onReplaceFragment() {
                        mResultFragment = SearchGeneralContainerResultFragment.newInstance();
                        getChildFragmentManager()
                                .beginTransaction()
                                .add(R.id.containerLayout,
                                        mResultFragment,
                                        SearchGeneralContainerResultFragment.TAG)
                                .addToBackStack(SearchGeneralContainerResultFragment.TAG)
                                .commit();
                    }

                    @Override
                    public void onRemoveFragment() {
                        removeFragment(getFragment(SearchGeneralContainerResultFragment.TAG));
                    }

                    @Override
                    public void onSearch(String text) {
                        if (mResultFragment != null && mResultFragment.isAdded()) {
                            mResultFragment.onSearch(text);
                        }
                    }

                    @Override
                    public void showLoading() {
                        if (mResultFragment != null && mResultFragment.isAdded()) {
                            mResultFragment.showLoading();
                        }
                    }
                });
        setVariable(BR.viewModel, viewModel);
    }

    private void replaceFragment(AbsBaseFragment fragment, String tag) {
        setFragment(R.id.containerLayout,
                fragment,
                tag);
    }
}
