package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.ChatAdapter;
import com.sompom.tookitup.broadcast.SoundEffectService;
import com.sompom.tookitup.broadcast.chat.PlayAudioService;
import com.sompom.tookitup.chat.AbsChatBinder;
import com.sompom.tookitup.chat.MessageState;
import com.sompom.tookitup.chat.listener.ChatListener;
import com.sompom.tookitup.chat.service.SocketService;
import com.sompom.tookitup.databinding.DialogFragmentChatBinding;
import com.sompom.tookitup.helper.LoginActivityResultCallback;
import com.sompom.tookitup.helper.MyHandler;
import com.sompom.tookitup.helper.RecordAudioHelper;
import com.sompom.tookitup.intent.ForwardIntent;
import com.sompom.tookitup.listener.OnChatItemListener;
import com.sompom.tookitup.model.BaseChatModel;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.emun.MediaType;
import com.sompom.tookitup.model.emun.PostContextMenuItemType;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.AbsSoundControllerActivity;
import com.sompom.tookitup.newui.dialog.ChatImageFullScreenDialog;
import com.sompom.tookitup.newui.dialog.PostContextMenuDialog;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.ChatDataManager;
import com.sompom.tookitup.utils.CopyTextUtil;
import com.sompom.tookitup.utils.MediaUtil;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.newviewmodel.ChatDialogViewModel;
import com.sompom.tookitup.widget.MyItemAnimator;
import com.sompom.tookitup.widget.TwoWaysRefreshableLayoutManager;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by nuonveyo on 6/25/18.
 */

public class ChatFragment extends AbsBindingFragment<DialogFragmentChatBinding>
        implements ChatListener, AbsBaseActivity.OnServiceListener {

    private final RecyclerView.SimpleOnItemTouchListener mDisableTouchrecyclerViewListener = new RecyclerView.SimpleOnItemTouchListener() {
        @Override
        public boolean onInterceptTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {
            return true;
        }
    };
    @Inject
    public ApiService mApiService;
    private ChatDialogViewModel mViewModel;
    private ChatAdapter mAdapter;
    private final MyHandler mScrollToBottomHandler = new MyHandler(200, () -> {
        getBinding().recyclerView.scrollBy(0, getBinding().recyclerView.getContext()
                .getResources().getDimensionPixelSize(R.dimen.chat_radius));
        getBinding().recyclerView.smoothScrollToPosition(mAdapter.getItemCount());
    });
    private TwoWaysRefreshableLayoutManager.OnLoadMoreCallback mLoadMoreListener;
    private TwoWaysRefreshableLayoutManager mLayoutManager;
    private SocketService.SocketBinder mSocketBinder;
    private User mRecipient;
    private final MyHandler mRemoveTypingHandler = new MyHandler(10000, () -> mAdapter.displayTyping(mRecipient, false));
    private User mMyUser;
    private String mConversationID;
    private RecordAudioHelper mRecordAudioHelper;
    private Product mProduct;
    private boolean mIsInBg = false;
    private PlayAudioService mPlayAudioService;
    private Conversation mConversation;

    public static ChatFragment newInstance(Conversation conversation,
                                           User recipient,
                                           Product product,
                                           String conversationID) {
        Bundle args = new Bundle();
        args.putParcelable(SharedPrefUtils.CONVERSATION, conversation);
        args.putParcelable(SharedPrefUtils.USER_ID, recipient);
        args.putParcelable(SharedPrefUtils.PRODUCT, product);
        args.putString(SharedPrefUtils.ID, conversationID);
        ChatFragment fragment = new ChatFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_fragment_chat;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        bindGifView();

        if (getActivity() instanceof AbsBaseActivity) {
            mPlayAudioService = ((AbsSoundControllerActivity) getActivity()).getPlayAudioService();
            ((AbsBaseActivity) getActivity()).addOnServiceListener(this);
        }

        mRecordAudioHelper = new RecordAudioHelper(getActivity(),
                getBinding().voiceRippleView,
                getBinding().duration,
                getBinding().textViewSlide,
                new RecordAudioHelper.OnCallback() {
                    @Override
                    public void onStopRecord(File recordFile) {
                        getBinding().recyclerView.removeOnItemTouchListener(mDisableTouchrecyclerViewListener);
                        mViewModel.mIsRecording.set(false);

                        if (recordFile == null) {
                            return;
                        }

                        if (TextUtils.isEmpty(mConversationID)) {
                            showSnackBar(R.string.chat_not_available_now);
                            return;
                        }

                        if (getContext() instanceof AbsBaseActivity) {
                            ((AbsBaseActivity) getContext()).startLoginWizardActivity(new LoginActivityResultCallback() {
                                @Override
                                public void onActivityResultSuccess(boolean isAlreadyLogin) {
                                    Media productMedia = new Media();
                                    productMedia.setUrl(recordFile.getAbsolutePath());
                                    productMedia.setType(MediaType.AUDIO);
                                    productMedia.setDuration(MediaUtil.getAudioDuration(recordFile.getAbsolutePath()));

                                    if (isGroupChat()) {
                                        sendMessages(generateChatList(null, Collections.singletonList(productMedia)));
                                    } else {
                                        Chat chat = Chat.getNewChat(getContext(), mRecipient, mProduct);
                                        chat.setChannelId(mConversationID);
                                        chat.setMediaList(Collections.singletonList(productMedia));

                                        sendMessage(chat);
                                    }
                                }
                            });
                        }
                    }

                    @Override
                    public void onStartRecord() {
                        getBinding().recyclerView.addOnItemTouchListener(mDisableTouchrecyclerViewListener);
                        mViewModel.mIsRecording.set(true);

                    }

                    @Override
                    public void onClick() {
                        mViewModel.onSendButtonClick();
                    }
                });

        if (getArguments() != null) {
            mConversation = getArguments().getParcelable(SharedPrefUtils.CONVERSATION);
            mProduct = getArguments().getParcelable(SharedPrefUtils.PRODUCT);
            mRecipient = getArguments().getParcelable(SharedPrefUtils.USER_ID);
            if (isGroupChat()) {
                mConversationID = mConversation.getGroupId();
            } else {
                mConversationID = getArguments().getString(SharedPrefUtils.ID);
            }
        }

        getBinding().backPressTextView.setOnClickListener(v -> getActivity().finish());
        mLoadMoreListener = () -> mViewModel.loadMoreData(new ChatDialogViewModel.OnLoadMoreCallback() {
            @Override
            public Chat getLastChat() {
                return mAdapter.getLatestChat();
            }

            @Override
            public void onFail(ErrorThrowable ex) {
                stopLoadMore();
            }

            @Override
            public void onComplete(List<BaseChatModel> result, boolean canLoadMore) {
                if (result.isEmpty()) {
                    stopLoadMore();
                } else {
                    mAdapter.addByLoadMore(result, canLoadMore);
                    mLayoutManager.loadingFinished();
                }
            }

            private void stopLoadMore() {
                mLayoutManager.setOnLoadMoreListener(null);
                mLayoutManager.loadingFinished();
                mAdapter.setCanLoadMore(false);
                mAdapter.notifyDataSetChanged();
            }
        });

        initAdapter();

        getBinding().voiceRippleView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (getActivity() == null) {
                    return;
                }
                int space = getActivity().getResources().getDimensionPixelSize(R.dimen.chat_edit_text_margin_end);

                int sendLayoutHeight = getBinding().sendLayout.getHeight();

                final int height = getBinding().voiceRippleView.getHeight();

                float spaceX = getBinding().voiceRippleView.getTranslationX() + (height - space) / 2;
                float spaceY = getBinding().voiceRippleView.getTranslationY() + (height - sendLayoutHeight) / 2;

                getBinding().voiceRippleView.setTranslationX(spaceX);
                getBinding().voiceRippleView.setTranslationY(spaceY);
                mRecordAudioHelper.setMinScroll(getBinding().sendLayout.getWidth() / 2 - space);
                getBinding().voiceRippleView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
        showKeyboard(getBinding().messageInput);
    }

    private void initAdapter() {
        // find the ListView in the popup layout
        mLayoutManager = new TwoWaysRefreshableLayoutManager(getActivity());
        mLayoutManager.setStackFromEnd(true);
        getBinding().recyclerView.setLayoutManager(mLayoutManager);
        getBinding().recyclerView.setItemAnimator(new MyItemAnimator());
        mMyUser = SharedPrefUtils.getUser(getContext());
        mAdapter = new ChatAdapter(getActivity(),
                mConversation,
                SharedPrefUtils.getUser(getActivity()),
                new ArrayList<>(),
                new OnChatItemListener() {
                    @Override
                    public void onImageClick(List<Media> list, int productMediaPosition) {
                        ChatImageFullScreenDialog dialog =
                                ChatImageFullScreenDialog.newInstance(list, productMediaPosition);
                        if (!dialog.isAdded()) {
                            dialog.show(getChildFragmentManager(), ChatImageFullScreenDialog.TAG);
                        }
                    }

                    @Override
                    public void onChatItemLongPressClick(Chat chat, @Nullable Media media, int chatItemPosition) {
                        if ((chat.getStatus() == MessageState.SENDING ||
                                chat.getStatus() == MessageState.FAIL) &&
                                TextUtils.equals(chat.getSenderId(), mMyUser.getId())) {
                            //do not show any pop up if message is in sending
                            return;
                        }
                        boolean isMultiImage = chat.getMediaList() != null && chat.getMediaList().size() > 1;
                        PostContextMenuDialog dialog = PostContextMenuDialog.newInstance();

                        final boolean isShowDelete = TextUtils.equals(chat.getSenderId(), mMyUser.getId()) && chat.getStatus() != MessageState.SEEN;
                        dialog.addItem(PostContextMenuItemType.getItems(isMultiImage, isShowDelete, chat.getType()));
                        dialog.setOnItemClickListener(result -> {
                            checkPostContextMenuItemClick(chat, media, result);
                            dialog.dismiss();
                        });
                        dialog.show(getChildFragmentManager(), PostContextMenuDialog.TAG);
                    }

                    @Override
                    public void onForwardClick(Chat chat) {
                        injectIsGroupProperty(chat);
                        startForwardIntent(chat, null);
                    }

                    @Override
                    public void onMessageRetryClick(Chat chat) {
                        sendMessage(chat);
                    }

                    @Override
                    public User getUser(String id) {
                        if (TextUtils.equals(mMyUser.getId(), id)) {
                            return mMyUser;
                        } else {
                            return mRecipient;
                        }
                    }
                });
//        if (getActivity() != null) {
//            final ChatStatusItemDecoration decoration = new ChatStatusItemDecoration(getActivity(), mAdapter, mRecipient);
//            getBinding().recyclerView.addItemDecoration(decoration);
//        }
        mAdapter.setShowEmpty(false);
        mAdapter.setCanLoadMore(true);
        getBinding().recyclerView.setAdapter(mAdapter);
    }

    private void checkPostContextMenuItemClick(Chat chat, Media media, int result) {
        PostContextMenuItemType itemType = PostContextMenuItemType.getItemType(result);
        if (itemType == PostContextMenuItemType.COPY_TEXT_CLICK) {
            CopyTextUtil.copyTextToClipboard(getActivity(), chat.getContent());
        } else if (itemType == PostContextMenuItemType.SAVE_IMAGE_CLICK) {
            mViewModel.saveImageIntoLocal(chat.getMediaList());
        } else if (itemType == PostContextMenuItemType.FORWARD_CLICK) {
            startForwardIntent(chat, media);
        } else if (itemType == PostContextMenuItemType.DELETE_CLICK) {
            mAdapter.deleteItem(chat);
            if (mSocketBinder != null) {
                //because server doesn't store sendTo field
                chat.setSendTo(mRecipient.getId());
                mSocketBinder.removeMessage(chat);
            }
        }
    }

    private void setAdapter(List<BaseChatModel> value, boolean isCanLoadMore) {
        if (value == null || value.isEmpty()) {
            return;
        }
        if (isCanLoadMore) {
            mLayoutManager.setOnLoadMoreListener(mLoadMoreListener);
        } else {
            mLayoutManager.setOnLoadMoreListener(null);
        }
        mAdapter.setShowEmpty(value.isEmpty());
        mAdapter.setData(value);
        mAdapter.setCanLoadMore(isCanLoadMore);

        getBinding().recyclerView.setAdapter(mAdapter);
        getBinding().recyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
    }

    private void sendMessage(Chat comment) {
        injectIsGroupProperty(comment);
        comment.setStatus(MessageState.SENDING);
        if (mAdapter.addLatestChat(comment)) {
            scrollToBottom(false);
            getBinding().messageInput.requestFocus();
        }

        SoundEffectService.playSound(getActivity(), SoundEffectService.SoundFile.SEND_MESSAGE);
        mSocketBinder.sendMessage(mRecipient, comment);

        //when send message, need to reset new typing handler
        String productId = null;
        if (mProduct != null) {
            productId = mProduct.getId();
        }
        mSocketBinder.stopTyping(mRecipient.getId(), productId, mConversationID);
    }

    private List<Chat> generateChatList(String content, List<Media> mediaList) {
        List<Chat> chatList = new ArrayList<>();
        Chat baseChat = Chat.getNewChat(requireContext(), mConversation.getParticipants().get(0), null);
        baseChat.setContent(content);
        baseChat.setChannelId(mConversation.getGroupId());
        baseChat.setMediaList(mediaList);
        baseChat.setGroup(true);
        baseChat.setGroupName(mConversation.getGroupName());
        for (User participant : mConversation.getParticipants()) {
            Chat newChat = baseChat.cloneNewChat();
            newChat.setSendTo(participant.getId());
            newChat.setRecipient(participant);

            chatList.add(newChat);
        }

        return chatList;
    }

    private List<String> getAllParticipantIds() {
        List<String> idList = new ArrayList<>();
        for (User participant : mConversation.getParticipants()) {
            idList.add(participant.getId());
        }

        return idList;
    }

    private boolean isGroupChat() {
        return mConversation != null && !TextUtils.isEmpty(mConversation.getGroupId());
    }

    private void sendMessages(List<Chat> messages) {
        //Add only first chat in the list
        Chat firstChat = messages.get(0);
        firstChat.setStatus(MessageState.SENDING);
        if (mAdapter.addLatestChat(firstChat)) {
            scrollToBottom(false);
            getBinding().messageInput.requestFocus();
        }
        SoundEffectService.playSound(requireContext(), SoundEffectService.SoundFile.SEND_MESSAGE);
        mSocketBinder.sendMessage(messages);
        mSocketBinder.stopTyping(null, null, null);
    }

    private void scrollToBottom(boolean forceScroll) {
        if (forceScroll) {
            final int last = mLayoutManager.findLastVisibleItemPosition();
            final int count = mAdapter.getItemCount() - 1;
            if (count - last <= 2) {
                mScrollToBottomHandler.startDelay();
            }
        } else {
            mScrollToBottomHandler.startDelay();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mIsInBg = false;
        if (mSocketBinder != null) {
            mSocketBinder.onResume(mConversationID);
        }
    }


    @Override
    public void onReceiveMessage(Chat message) {
        if (getActivity() == null) {
            return;
        }
        getActivity().runOnUiThread(() -> {
            if (TextUtils.equals(message.getChannelId(), mConversationID)) {
                boolean isAdd = mAdapter.addLatestChat(message);
                if (isAdd) {
                    //scroll to button
                    scrollToBottom(true);

                    //remove typing
                    mAdapter.displayTyping(mRecipient, false);
                }
            }
        });
    }

    @Override
    public void onUserTyping(Chat chat) {
        if (getActivity() == null) {
            return;
        }
        getActivity().runOnUiThread(() -> {
            if (TextUtils.equals(chat.getChannelId(), mConversationID)) {
                mAdapter.displayTyping(mRecipient, chat.getTyping());
                if (chat.getTyping()) {
                    scrollToBottom(true);
                    mRemoveTypingHandler.startDelay();
                }
            }
        });
    }

    @Override
    public boolean isInBackground() {
        return mIsInBg;
    }

    @Override
    public String getChannelId() {
        return mConversationID;
    }

    @Override
    public void onServiceReady(AbsChatBinder binder) {
        if (binder instanceof SocketService.SocketBinder) {
            mSocketBinder = (SocketService.SocketBinder) binder;

            mSocketBinder.addChatListener(this);

            ChatDataManager dataManager = new ChatDataManager(getActivity(), mApiService, mConversation);
            mViewModel = new ChatDialogViewModel(dataManager,
                    mSocketBinder,
                    mConversation,
                    mConversationID,
                    mRecipient,
                    mProduct,
                    new ChatDialogViewModel.OnCallback() {
                        @Override
                        public void onSendButtonClick(Chat chat) {
                            //One to one chatting
                            mRecordAudioHelper.setIcon(RecordAudioHelper.Type.RECORD);
                            sendMessage(chat);
                        }

                        @Override
                        public void onSendButtonClick(String content, String conversationId) {
                            //Group Chatting
                            sendMessages(generateChatList(content, null));
                        }

                        @Override
                        public void onStartTyping() {
                            if (mSocketBinder != null) {

                                //Old code
//                                String productId = null;
//                                if (mProduct != null) {
//                                    productId = mProduct.getId();
//                                }
//                                mSocketBinder.startTyping(mRecipient.getId(), productId, mConversationID);

                                if (isGroupChat()) {
                                    mSocketBinder.startTyping(getAllParticipantIds(), mConversation.getGroupId());
                                } else {
                                    mSocketBinder.startTyping(mRecipient.getId(), null, mConversationID);
                                }
                            }
                        }

                        @Override
                        public void onFail(ErrorThrowable ex) {
                            if (getActivity() != null && ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                                mAdapter.setEmptyMessage(getActivity().getString(R.string.error_internet_connection));
                            }
                            setAdapter(new ArrayList<>(), false);
                        }

                        @Override
                        public void onComplete(List<BaseChatModel> result, boolean canLoadMore) {
                            setAdapter(result, canLoadMore);
                            mSocketBinder.onResume(mConversationID);
                        }

                        @Override
                        public void onReplaceRecordIcon() {
                            mRecordAudioHelper.setIcon(RecordAudioHelper.Type.RECORD);
                        }

                        @Override
                        public void onReplaceSendIcon() {
                            mRecordAudioHelper.setIcon(RecordAudioHelper.Type.SEND);
                        }
                    });
            setVariable(BR.viewModel, mViewModel);
        }
    }

    private void injectIsGroupProperty(Chat chat) {
        chat.setGroup(isGroupChat());
        chat.setGroupName(mConversation.getGroupName());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mPlayAudioService != null) {
            mPlayAudioService.stop();
        }

        if (mSocketBinder != null) {
            mSocketBinder.removeChatListener(this);
        }

        if (getActivity() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getActivity()).removeServiceListener(this);
        }

        mRecordAudioHelper.destroy();
        mScrollToBottomHandler.destroy();
        mRemoveTypingHandler.destroy();
    }

    @Override
    public void onPause() {
        super.onPause();
        mIsInBg = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        mRecordAudioHelper.checkToCancelRecord();
    }

    private void startForwardIntent(Chat chat, Media productMedia) {
        if (mPlayAudioService != null
                && (mPlayAudioService.isPlaying() || mPlayAudioService.isPause())) {
            mPlayAudioService.stop();
        }
        new Handler().post(() -> {
            if (productMedia != null) {
                startActivity(new ForwardIntent(getActivity(), productMedia));
            } else {
                startActivity(new ForwardIntent(getActivity(), chat));
            }
        });
    }

    private void bindGifView() {
        getBinding().containerSearchGif.onBackPressClickListener(v -> {
            getBinding().containerEditText.setVisibility(View.VISIBLE);
            getBinding().messageInput.requestFocus();
            getBinding().voiceRippleView.setVisibility(View.VISIBLE);
        });

        getBinding().gifImageView.setOnClickListener(v -> {
            getBinding().containerEditText.setVisibility(View.GONE);
            getBinding().voiceRippleView.setVisibility(View.GONE);
            getBinding().containerSearchGif.loadTrendingGifList(baseGifModel -> {
                getBinding().containerEditText.setVisibility(View.VISIBLE);
                getBinding().messageInput.requestFocus();
                getBinding().voiceRippleView.setVisibility(View.VISIBLE);

                ArrayList<Media> productMedias = new ArrayList<>();
                Media media = new Media();
                media.setWidth(baseGifModel.getGifWidth());
                media.setHeight(baseGifModel.getGifHeight());
                media.setUrl(baseGifModel.getGifUrl());
                media.setType(MediaType.TENOR_GIF);
                productMedias.add(media);

                if (isGroupChat()) {
                    sendMessages(generateChatList(null, productMedias));
                } else {
                    Chat chat = Chat.getNewChat(getContext(), mRecipient, mProduct);
                    chat.setChannelId(mConversationID);
                    chat.setMediaList(productMedias);

                    sendMessage(chat);
                }
            });
        });
    }

    public boolean onBackPress() {
        if (getBinding().containerSearchGif.getVisibility() == View.VISIBLE) {
            getBinding().containerSearchGif.clearData();
            getBinding().containerEditText.setVisibility(View.VISIBLE);
            getBinding().messageInput.requestFocus();
            getBinding().voiceRippleView.setVisibility(View.VISIBLE);
            return false;
        }
        return true;
    }
}
