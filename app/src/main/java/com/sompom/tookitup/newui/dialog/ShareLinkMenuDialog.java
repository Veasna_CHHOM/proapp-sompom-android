package com.sompom.tookitup.newui.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.sompom.tookitup.intent.CreateTimelineIntent;
import com.sompom.tookitup.intent.ForwardIntent;
import com.sompom.tookitup.model.PostContextMenuItem;
import com.sompom.tookitup.model.WallStreetAdaptive;
import com.sompom.tookitup.model.emun.CreateWallStreetScreenType;
import com.sompom.tookitup.model.emun.PostContextMenuItemType;
import com.sompom.tookitup.utils.CopyTextUtil;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.List;

/**
 * Created by nuonveyo on 11/5/18.
 */

public class ShareLinkMenuDialog extends PostContextMenuDialog {
    private WallStreetAdaptive mWallStreetAdaptive;

    public static ShareLinkMenuDialog newInstance(WallStreetAdaptive adaptive) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(SharedPrefUtils.DATA, adaptive);
        ShareLinkMenuDialog fragment = new ShareLinkMenuDialog();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public List<PostContextMenuItem> getPostContextMenuItems() {
        return PostContextMenuItemType.getItemsShare(!TextUtils.isEmpty(mWallStreetAdaptive.getShareUrl()) || mWallStreetAdaptive == null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            mWallStreetAdaptive = getArguments().getParcelable(SharedPrefUtils.DATA);
        }
        super.onViewCreated(view, savedInstanceState);
        setOnItemClickListener(result -> {

            PostContextMenuItemType itemType = PostContextMenuItemType.getItemType(result);
            if (itemType == PostContextMenuItemType.WRITE_POST_CLICK) {
                dismiss();
                if (getActivity() != null) {
                    getActivity().startActivity(new CreateTimelineIntent(getActivity(), mWallStreetAdaptive, CreateWallStreetScreenType.SHARE_POST));
                }
            } else if (itemType == PostContextMenuItemType.SENDTO_MESSENGER_CLICK) {
                dismiss();
                if (getActivity() != null) {
                    String testLink = mWallStreetAdaptive.getShareUrl();
                    if (TextUtils.isEmpty(testLink)) {
                        return;
                    }
                    getActivity().startActivity(new ForwardIntent(getActivity(), testLink));
                }
            } else if (itemType == PostContextMenuItemType.COPY_TEXT_CLICK) {
                dismiss();
                String testLink = mWallStreetAdaptive.getShareUrl();
                if (TextUtils.isEmpty(testLink)) {
                    return;
                }
                CopyTextUtil.copyTextToClipboard(getActivity(), testLink);
            }
        });
    }
}
