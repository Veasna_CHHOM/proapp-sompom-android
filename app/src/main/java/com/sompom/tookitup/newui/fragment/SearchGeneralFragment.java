package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.android.databinding.library.baseAdapters.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.newadapter.SearchGeneralAdapter;
import com.sompom.tookitup.databinding.FragmentSearchGeneralBinding;
import com.sompom.tookitup.helper.NavigateSellerStoreHelper;
import com.sompom.tookitup.listener.OnMessageItemClick;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.SearchGeneralDataManager;
import com.sompom.tookitup.utils.KeyboardUtil;
import com.sompom.tookitup.viewmodel.SearchGeneralFragmentViewModel;

import java.util.Objects;

import javax.inject.Inject;

/**
 * Created by nuonveyo on 8/2/18.
 */

public class SearchGeneralFragment extends AbsBindingFragment<FragmentSearchGeneralBinding> implements OnMessageItemClick {
    @Inject
    public ApiService mApiService;

    public static SearchGeneralFragment newInstance() {
        return new SearchGeneralFragment();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_search_general;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);

        getBinding().recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        getBinding().recyclerView.setOnTouchListener((v, event) -> {
            KeyboardUtil.hideKeyboard(getActivity());
            v.performClick();
            return false;
        });
        SearchGeneralDataManager dataManager = new SearchGeneralDataManager(getActivity(), mApiService);
        SearchGeneralFragmentViewModel viewModel = new SearchGeneralFragmentViewModel(dataManager, result -> {
            SearchGeneralAdapter searchGeneralAdapter = new SearchGeneralAdapter(result, SearchGeneralFragment.this);
            getBinding().recyclerView.setAdapter(searchGeneralAdapter);
        });
        setVariable(BR.viewModel, viewModel);
    }

    @Override
    public void onConversationUserClick(User user) {
        //Old code
        //startActivity(new SellerStoreIntent(getContext(), user.getId()));
        NavigateSellerStoreHelper.openSellerStore((AppCompatActivity) Objects.requireNonNull(getContext()),
                user.getId());
    }


}
