package com.sompom.tookitup.newui;

import android.Manifest;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.sompom.baseactivity.PermissionCheckHelper;
import com.sompom.tookitup.chat.call.CallingService;
import com.sompom.tookitup.chat.call.NotificationCallVo;
import com.sompom.tookitup.intent.CallingIntent;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.fragment.CallingFragment;

public class CallingActivity extends AbsDefaultActivity {
    private NotificationCallVo mCallVo;
    private User mUser;
    private CallingService.SinchBinder mBinder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CallingIntent callingIntent = new CallingIntent(getIntent());
        mCallVo = callingIntent.getNotification();
        mUser = callingIntent.getRecipient();
        Product product = callingIntent.getProduct();

        checkPermission(new PermissionCheckHelper.OnPermissionCallback() {
            @Override
            public void onPermissionGranted() {
                bindCall(result -> {
                    mBinder = result;
                    Fragment fragment;
                    if (mCallVo != null) {
                        fragment = CallingFragment.newInstance(mCallVo);
                    } else {
                        fragment = CallingFragment.newInstance(mUser, product);
                    }
                    setFragment(fragment, CallingFragment.TAG);
                });
            }

            @Override
            public void onPermissionDenied(String[] grantedPermission, String[] deniedPermission, boolean isUserPressNeverAskAgain) {
                finish();
            }
        }, android.Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_PHONE_STATE);


    }

    public CallingService.SinchBinder getBinder() {
        return mBinder;
    }
}
