package com.sompom.tookitup.newui.dialog;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.sompom.tookitup.R;
import com.sompom.tookitup.intent.newintent.LoginIntent;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.utils.SharedPrefUtils;

import timber.log.Timber;

/**
 * Created by He Rotha on 3/18/19.
 */
public class SessionDialog extends MessageDialog {
    public static SessionDialog newInstance() {
        return new SessionDialog();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setTitle(getString(R.string.session_title));
        setCancelable(false);
        setMessage(getString(R.string.session_description));
        setLeftText(getString(R.string.change_language_button_ok), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getActivity() == null) {
                    return;
                }
                SharedPrefUtils.clearValue(getActivity());
                if (getActivity() instanceof AbsBaseActivity && ((AbsBaseActivity) getActivity()).getSinchBinder() != null) {
                    Timber.e("stopService ");

                    ((AbsBaseActivity) getActivity()).getSinchBinder().stopService();
                    ((AbsBaseActivity) getActivity()).bindCall(binder -> {
                        binder.logout();

                        LoginIntent intent = new LoginIntent(getActivity());
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        getActivity().startActivity(intent);
                    });
                }

            }
        });
        super.onViewCreated(view, savedInstanceState);
    }
}
