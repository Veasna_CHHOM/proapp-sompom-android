package com.sompom.tookitup.newui.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.View;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.DialogProductDetailActionButtonBinding;
import com.sompom.tookitup.viewmodel.newviewmodel.ProductDetailActionButtonDialogViewModel;

/**
 * Created by nuonveyo on 8/1/18.
 */

public class ProductDetailActionButtonDialog extends AbsBindingBottomSheetDialog<DialogProductDetailActionButtonBinding> {
    public static ProductDetailActionButtonDialog newInstance() {
        return new ProductDetailActionButtonDialog();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_product_detail_action_button;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getBinding().closeButton.setOnClickListener(v -> dismiss());
        ProductDetailActionButtonDialogViewModel viewModel = new ProductDetailActionButtonDialogViewModel();
        setVariable(BR.viewModel, viewModel);
    }
}
