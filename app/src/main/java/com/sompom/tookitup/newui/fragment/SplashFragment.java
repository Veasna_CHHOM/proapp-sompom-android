package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;

import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.FragmentSplashBinding;
import com.sompom.tookitup.intent.newintent.HomeIntent;
import com.sompom.tookitup.intent.newintent.LoginIntent;
import com.sompom.tookitup.utils.SharedPrefUtils;

public class SplashFragment extends AbsBindingFragment<FragmentSplashBinding> {

    private static final long DELAY = 1000;

    public static SplashFragment newInstance() {

        Bundle args = new Bundle();

        SplashFragment fragment = new SplashFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_splash;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        new Handler().postDelayed(() -> {
            if (SharedPrefUtils.isLogin(requireContext())) {
                startActivity(new HomeIntent(requireContext()));
            } else {
                startActivity(new LoginIntent(requireContext()));
            }
            requireActivity().finish();
        }, DELAY);
    }
}
