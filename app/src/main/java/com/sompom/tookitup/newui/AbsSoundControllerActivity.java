package com.sompom.tookitup.newui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.sompom.tookitup.broadcast.chat.PlayAudioService;
import com.sompom.tookitup.listener.PlayAudioServiceListener;

import timber.log.Timber;

/**
 * Created by He Rotha on 10/16/18.
 */
public abstract class AbsSoundControllerActivity<T extends ViewDataBinding> extends AbsBindingActivity<T> {

    private PlayAudioService mPlayAudioService;
    private PlayAudioServiceListener mPlayAudioServiceListener;
    private ServiceConnection mPlayAudioConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Timber.e("play audio onServiceConnected");
            PlayAudioService.PlayAudioBinder binder = (PlayAudioService.PlayAudioBinder) service;
            mPlayAudioService = binder.getService();
            binder.setListener(new PlayAudioServiceListener() {

                @Override
                public void onPause(String currentId) {
                    if (mPlayAudioServiceListener != null) {
                        mPlayAudioServiceListener.onPause(currentId);
                    }
                }

                @Override
                public void onPlaying(String currentId, long duration) {
                    if (mPlayAudioServiceListener != null) {
                        mPlayAudioServiceListener.onPlaying(currentId, duration);
                    }
                }

                @Override
                public void onFinish() {
                    if (mPlayAudioServiceListener != null) {
                        mPlayAudioServiceListener.onFinish();
                    }
                }

                @Override
                public void onResume(String currentId) {
                    if (mPlayAudioServiceListener != null) {
                        mPlayAudioServiceListener.onResume(currentId);
                    }
                }

                @Override
                public void onError() {
                    if (mPlayAudioServiceListener != null) {
                        mPlayAudioServiceListener.onError();
                    }
                }

                @Override
                public void hideLoading(String currentId) {
                    if (mPlayAudioServiceListener != null) {
                        mPlayAudioServiceListener.hideLoading(currentId);
                    }
                }
            });
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Timber.e("play audio onServiceDisconnected");
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent1 = new Intent(this, PlayAudioService.class);
        bindService(intent1, mPlayAudioConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mPlayAudioConnection);
    }

    public void setPlayAudioServiceListener(PlayAudioServiceListener playAudioServiceListener) {
        mPlayAudioServiceListener = playAudioServiceListener;
    }

    public PlayAudioService getPlayAudioService() {
        return mPlayAudioService;
    }

}
