package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.FilterCategoryAdapter;
import com.sompom.tookitup.databinding.FragmentFilterCategory2Binding;
import com.sompom.tookitup.helper.MyTextWatcher;
import com.sompom.tookitup.model.result.Category;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.FilterCategoryDataManager;
import com.sompom.tookitup.utils.KeyboardUtil;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.newviewmodel.FilterCategoryFragmentViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by nuonveyo on 6/19/18.
 */

public class FilterCategoryFragment extends AbsBindingFragment<FragmentFilterCategory2Binding> {
    @Inject
    public ApiService mApiService;
    private List<Category> mCategories = new ArrayList<>();
    private FilterCategoryAdapter mCategoryAdapter;
    private boolean mIsEmpty;

    public static FilterCategoryFragment newInstance(ArrayList<Category> categoryArrayList, boolean isEmpty) {
        Bundle args = new Bundle();
        args.putParcelableArrayList(SharedPrefUtils.CATEGORY, categoryArrayList);
        args.putBoolean(SharedPrefUtils.IS_ACTIVITY, isEmpty);
        FilterCategoryFragment fragment = new FilterCategoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_filter_category2;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);

        if (getArguments() != null) {
            mIsEmpty = getArguments().getBoolean(SharedPrefUtils.IS_ACTIVITY);
            mCategories = getArguments().getParcelableArrayList(SharedPrefUtils.CATEGORY);
            if (mCategories == null) {
                mCategories = new ArrayList<>();
            }
        }

        initView();

        FilterCategoryDataManager dataManager = new FilterCategoryDataManager(getActivity(), mApiService);
        FilterCategoryFragmentViewModel viewModel = new FilterCategoryFragmentViewModel(dataManager,
                categoryList -> {
                    for (Category category : mCategories) {
                        for (Category category1 : categoryList) {
                            if (category1.getId().equalsIgnoreCase(category.getId())) {
                                category1.setCheck(category.isCheck());
                                break;
                            }
                        }
                    }

                    mCategories = categoryList;
                    initAdapter();
                });
        setVariable(BR.viewModel, viewModel);

        if (!mIsEmpty) {
            initAdapter();
        } else {
            viewModel.getData();
        }
    }

    private void initAdapter() {
        mCategoryAdapter = new FilterCategoryAdapter(mCategories);
        getBinding().recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        getBinding().recyclerview.setOnTouchListener((v, event) -> {
            KeyboardUtil.hideKeyboard(getActivity());
            v.performClick();
            return false;
        });
        getBinding().recyclerview.setAdapter(mCategoryAdapter);
    }

    public List<Category> getSelectedCategory() {
        if (mCategoryAdapter == null) {
            return mCategories;
        }
        return mCategoryAdapter.getSelectedCategory();
    }

    private void initView() {
        getBinding().closeTextView.setOnClickListener(v -> {
            getBinding().containerEditText.setVisibility(View.GONE);
            getBinding().containerTitle.setVisibility(View.VISIBLE);
            getBinding().edittextCategory.setText(null);
            hideKeyboard();
        });

        getBinding().searchIamgeView.setOnClickListener(v -> {
            getBinding().containerTitle.setVisibility(View.GONE);
            getBinding().containerEditText.setVisibility(View.VISIBLE);
            showKeyboard(getBinding().edittextCategory);
        });

        getBinding().edittextCategory.addTextChangedListener(new MyTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s) {
                if (mCategoryAdapter != null) {
                    mCategoryAdapter.setFilter(s);
                }
            }
        });
    }
}
