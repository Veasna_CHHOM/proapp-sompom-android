package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.EditText;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.ProductPreviewAdapter;
import com.sompom.tookitup.databinding.FragmentProductFormBinding;
import com.sompom.tookitup.listener.OnProductFormListener;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.newui.ProductFormActivity;
import com.sompom.tookitup.services.ApiService;
import com.sompom.tookitup.services.datamanager.ProductManager;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.newviewmodel.ProductFormFragmentViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by nuonveyo on 6/11/18.
 */

public class ProductFormFragment extends AbsBindingFragment<FragmentProductFormBinding>
        implements OnProductFormListener {
    @Inject
    public ApiService mApiService;
    private ProductFormFragmentViewModel mViewModel;

    public static ProductFormFragment newInstance(List<Media> imagePaths) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(SharedPrefUtils.DATA, (ArrayList<? extends Parcelable>) imagePaths);
        ProductFormFragment fragment = new ProductFormFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public static ProductFormFragment newInstance(Product imagePaths) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(SharedPrefUtils.PRODUCT, imagePaths);
        ProductFormFragment fragment = new ProductFormFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_product_form;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getControllerComponent().inject(this);
        if (getActivity() != null) {
            ((ProductFormActivity) getActivity()).setEnableBackPressed();
        }

        Product product = null;
        ArrayList<Media> imagePaths = null;
        if (getArguments() != null) {
            imagePaths = getArguments().getParcelableArrayList(SharedPrefUtils.DATA);
            product = getArguments().getParcelable(SharedPrefUtils.PRODUCT);
        }

        ProductManager productManager = new ProductManager(getActivity(), mApiService);
        if (product == null) {
            mViewModel = new ProductFormFragmentViewModel(productManager, imagePaths, this);
        } else {
            mViewModel = new ProductFormFragmentViewModel(productManager, product, this);
        }

        setVariable(BR.viewModel, mViewModel);

        EditText editText = view.findViewById(R.id.EdittextCurrency);
        view.findViewById(R.id.priceContainer).setOnClickListener(view1 -> showKeyboard(editText));
    }

    @Override
    public void onSetImage(List<Media> list) {
        final LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        getBinding().recyclerview.setLayoutManager(linearLayoutManager);
        getBinding().recyclerview.setHasFixedSize(true);
        ProductPreviewAdapter adapter = new ProductPreviewAdapter(list);
        adapter.setProductItemClickListener(() -> mViewModel.onItemEditClick());
        getBinding().recyclerview.setAdapter(adapter);
    }
}
