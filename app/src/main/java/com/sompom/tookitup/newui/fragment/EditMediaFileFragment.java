package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.WindowManager;

import com.android.databinding.library.baseAdapters.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.EditMediaFileAdapter;
import com.sompom.tookitup.databinding.FragmentEditMediaFileBinding;
import com.sompom.tookitup.model.LifeStream;
import com.sompom.tookitup.newui.EditMediaFileActivity;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.EditMediaFileViewModel;

/**
 * Created by nuonveyo on 5/7/18.
 */

public class EditMediaFileFragment extends AbsBindingFragment<FragmentEditMediaFileBinding> {
    public static final String TAG = EditMediaFileFragment.class.getName();
    private LifeStream mLifeStream;
    private int mClickPosition;
    private EditMediaFileAdapter mDetailAdapter;
    private boolean mIsEdit;

    public static EditMediaFileFragment newInstance(LifeStream lifeStream, int position, boolean isEdit) {

        Bundle args = new Bundle();
        args.putParcelable(SharedPrefUtils.CATEGORY, lifeStream);
        args.putInt(SharedPrefUtils.USER_ID, position);
        args.putBoolean(SharedPrefUtils.STATUS, isEdit);

        EditMediaFileFragment fragment = new EditMediaFileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_edit_media_file;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hideKeyboard();
        if (getArguments() != null) {
            mLifeStream = getArguments().getParcelable(SharedPrefUtils.CATEGORY);
            mClickPosition = getArguments().getInt(SharedPrefUtils.USER_ID);
            mIsEdit = getArguments().getBoolean(SharedPrefUtils.STATUS);
        }

        setToolbar(getBinding().layoutToolbar.toolbar, true);
        getBinding().layoutToolbar.textViewTitle.setText(R.string.seller_store_button_edit);

        EditMediaFileViewModel viewModel = new EditMediaFileViewModel(getActivity(), this::closeScreen);
        setVariable(BR.viewModel, viewModel);
        initData();
    }

    public LifeStream getLifeStream() {
        return mLifeStream;
    }

    public void initData() {
        mDetailAdapter = new EditMediaFileAdapter(mLifeStream,
                mIsEdit,
                position -> {
                    mLifeStream.getMedia().remove(position);
                    mDetailAdapter.notifyItemRemoved(position);
                    if (mLifeStream.getMedia().isEmpty()) {
                        closeScreen();
                    }
                });

        getBinding().recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        getBinding().recyclerView.smoothScrollToPosition(mClickPosition);
        getBinding().recyclerView.setAdapter(mDetailAdapter);
    }

    private void closeScreen() {
        if (getActivity() instanceof EditMediaFileActivity) {
            ((EditMediaFileActivity) getActivity()).onSetResult(getLifeStream());
            getActivity().finish();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getBinding().recyclerView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        getBinding().recyclerView.onPause();

    }
}
