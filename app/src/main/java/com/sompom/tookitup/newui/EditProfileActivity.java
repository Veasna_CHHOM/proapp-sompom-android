package com.sompom.tookitup.newui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.sompom.tookitup.helper.LocaleManager;
import com.sompom.tookitup.intent.newintent.EditProfileIntentResult;
import com.sompom.tookitup.newui.fragment.EditProfileFragment;

public class EditProfileActivity extends AbsDefaultActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(EditProfileFragment.newInstance(), EditProfileFragment.TAG);
    }

    public void recreateFragment(String local) {
        LocaleManager.applyChangeLanguage(this, local);
        recreate();
    }

    @Override
    public void finish() {
        sendResult();
        super.finish();
    }

    private void sendResult() {
        boolean isChange = false;
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(EditProfileFragment.TAG);
        if (fragment instanceof EditProfileFragment) {
            isChange = ((EditProfileFragment) fragment).isChangeValue();
        }

        EditProfileIntentResult intentResult = new EditProfileIntentResult(isChange);
        setResult(Activity.RESULT_OK, intentResult);
    }
}
