package com.sompom.tookitup.newui;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.google.gson.Gson;
import com.sompom.tookitup.intent.EditMediaFileIntent;
import com.sompom.tookitup.model.LifeStream;
import com.sompom.tookitup.newui.fragment.EditMediaFileFragment;

import timber.log.Timber;

public class EditMediaFileActivity extends AbsDefaultActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EditMediaFileIntent intent = new EditMediaFileIntent(getIntent());
        setFragment(EditMediaFileFragment.newInstance(intent.getLifeStream(),
                intent.getPosition(),
                intent.isEdit()),
                EditMediaFileFragment.TAG);
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getFragment(EditMediaFileFragment.TAG);
        if (fragment != null && fragment instanceof EditMediaFileFragment) {
            onSetResult(((EditMediaFileFragment) fragment).getLifeStream());
        }
        super.onBackPressed();
    }

    public void onSetResult(LifeStream lifeStream) {
        Timber.e("onSetResult: %s", new Gson().toJson(lifeStream));
        EditMediaFileIntent intent = new EditMediaFileIntent(lifeStream);
        setResult(Activity.RESULT_OK, intent);
    }
}
