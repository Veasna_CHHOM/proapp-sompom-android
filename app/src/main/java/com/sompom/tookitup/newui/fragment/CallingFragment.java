package com.sompom.tookitup.newui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.sinch.android.rtc.calling.Call;
import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.chat.call.NotificationCallVo;
import com.sompom.tookitup.chat.call.CallingService;
import com.sompom.tookitup.databinding.FragmentIncomingCallBinding;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.CallingActivity;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.CallingViewModel;

public class CallingFragment extends AbsBindingFragment<FragmentIncomingCallBinding>
        implements CallingService.CallingListener {

    public static CallingFragment newInstance(NotificationCallVo isChangeActivateStore) {
        Bundle args = new Bundle();
        CallingFragment fragment = new CallingFragment();
        args.putParcelable(SharedPrefUtils.STATUS, isChangeActivateStore);
        fragment.setArguments(args);
        return fragment;
    }

    public static CallingFragment newInstance(User callTo, Product product) {
        Bundle args = new Bundle();
        CallingFragment fragment = new CallingFragment();
        args.putParcelable(SharedPrefUtils.ID, callTo);
        args.putParcelable(SharedPrefUtils.PRODUCT, product);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_incoming_call;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() == null) {
            return;
        }
        NotificationCallVo callVo = getArguments().getParcelable(SharedPrefUtils.STATUS);
        User recipientId = getArguments().getParcelable(SharedPrefUtils.ID);
        Product product = getArguments().getParcelable(SharedPrefUtils.PRODUCT);

        CallingService.SinchBinder binder = null;

        if (getActivity() instanceof CallingActivity) {
            binder = ((CallingActivity) getActivity()).getBinder();
        }

        if (binder == null) {
            return;
        }

        setVariable(BR.viewModel, new CallingViewModel(getActivity(), callVo, recipientId, product, binder, this));
    }

    @Override
    public void onCallEnd() {
        getActivity().finish();
    }

    @Override
    public void onIncomingCall(Call call) {

    }

    @Override
    public void onCallEstablished(Call call) {

    }

    @Override
    public void onCallProgressing(Call call) {

    }
}
