package com.sompom.tookitup.newui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.ActivityDefaultBinding;
import com.sompom.tookitup.intent.ChatIntent;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.fragment.ChatFragment;

/**
 * Created by He Rotha on 12/18/18.
 */
public class ChatActivity extends AbsSoundControllerActivity<ActivityDefaultBinding> {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ChatIntent intent = new ChatIntent(getIntent());
        intent.init(getThis());
        final Product product = intent.getProduct();
        final User user = intent.getRecipient();
        final String conversationId = intent.getConversationId(this);

        setFragment(R.id.containerView, ChatFragment.newInstance(intent.getConversation(),
                user,
                product,
                conversationId),
                ChatFragment.TAG);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_default;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        ChatIntent chatIntent = new ChatIntent(intent);
        chatIntent.init(getThis());

        final Product product = chatIntent.getProduct();
        final User user = chatIntent.getRecipient();
        final String conversationId = chatIntent.getConversationId(this);

        Fragment fr = getSupportFragmentManager().findFragmentByTag(ChatFragment.TAG);
        if (fr instanceof ChatFragment) {
            if (!((ChatFragment) fr).getChannelId().equals(chatIntent.getConversationId(this))) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .remove(fr)
                        .replace(R.id.containerView, ChatFragment.newInstance(chatIntent.getConversation(),
                                user,
                                product,
                                conversationId),
                                ChatFragment.TAG)
                        .commit();
            }
        } else {
            setFragment(R.id.containerView, ChatFragment.newInstance(chatIntent.getConversation(),
                    user,
                    product,
                    conversationId),
                    ChatFragment.TAG);
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fr = getSupportFragmentManager().findFragmentByTag(ChatFragment.TAG);
        if (fr instanceof ChatFragment && !((ChatFragment) fr).onBackPress()) {
            return;
        }
        super.onBackPressed();

    }
}
