package com.sompom.tookitup.newui.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.sompom.tookitup.R;
import com.sompom.tookitup.database.CurrencyDb;
import com.sompom.tookitup.model.result.Currency;

import java.util.List;

/**
 * Created by He Rotha on 6/19/18.
 */
public class CurrencyDialog extends MessageDialog {
    private String mCurrency;
    private OnCurrencyChooseListener mOnCurrencyChooseListener;

    public void setCurrency(String currency) {
        mCurrency = currency;
    }

    public void setOnCurrencyChooseListener(OnCurrencyChooseListener onCurrencyChooseListener) {
        mOnCurrencyChooseListener = onCurrencyChooseListener;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setTitle(getString(R.string.edit_profile_currency));
        setMessage(getString(R.string.edit_profile_currency_description));
        List<Currency> list = CurrencyDb.getCurrencies(getContext());
        final String[] currencies = new String[list.size()];
        int selectPosition = 0;
        for (int i = 0; i < currencies.length; i++) {
            currencies[i] = list.get(i).getName();
            if (list.get(i).getName().equals(mCurrency)) {
                selectPosition = i;
            }
        }
        setSingleChoiceItems(currencies, selectPosition, (dialog, which) -> mCurrency = currencies[which]);
        setLeftText(getString(R.string.change_language_button_ok), dialog -> mOnCurrencyChooseListener.onSelect(mCurrency));
        setRightText(getString(R.string.change_language_button_cancel), null);

        super.onViewCreated(view, savedInstanceState);
    }

    public interface OnCurrencyChooseListener {
        void onSelect(String currency);
    }
}
