package com.sompom.tookitup.viewholder;

import android.databinding.ViewDataBinding;
import android.support.annotation.Nullable;

import com.google.android.exoplayer2.ui.PlayerView;
import com.sompom.tookitup.utils.VolumePlaying;
import com.sompom.tookitup.widget.lifestream.MediaLayout;
import com.sompom.videomanager.helper.OnButtonPlayClickListener;
import com.sompom.videomanager.model.MediaListItem;

import timber.log.Timber;

/**
 * Created by He Rotha on 7/19/18.
 */
public class TimelineViewHolder extends BindingViewHolder implements MediaListItem {
    private MediaLayout mMediaLayout;

    public TimelineViewHolder(ViewDataBinding itemView) {
        super(itemView);
        Timber.e("create");
    }

    public void setMediaLayout(MediaLayout mediaLayout) {
        mMediaLayout = mediaLayout;
        Timber.e("setMediaLayout " + hashCode());

    }

    @Override
    public void onMediaPlay(boolean isStartPlay) {
        Timber.e("onMediaPlay " + isStartPlay + "  " + hashCode());
        if (mMediaLayout != null) {
            mMediaLayout.setMute(VolumePlaying.isMute(mMediaLayout.getContext()));
            mMediaLayout.onMediaPlay(isStartPlay);
        }
    }

    @Override
    public void onMediaPause() {
        Timber.e("onMediaPause " + hashCode());

        if (mMediaLayout != null) {
            mMediaLayout.onMediaPause();
        }
    }

    @Override
    public void onVolumeClick(OnButtonPlayClickListener listener) {
        if (mMediaLayout != null) {
            mMediaLayout.setOnVolumeClickListener(new MediaLayout.OnVolumeClickListener() {
                @Override
                public void onButtonPlayClick(boolean isPlay) {
                    listener.onButtonPlayClick(isPlay);
                }

                @Override
                public void onVolumeClick(boolean isMute) {
                    listener.onButtonMuteClick(isMute);
                }
            });
        }
    }

    @Nullable
    @Override
    public PlayerView getPlayerView() {
        if (mMediaLayout == null) {
            return null;
        }
        return mMediaLayout.getPlayerView();
    }

    public void stopOrRelease() {
        if (mMediaLayout != null) {
            mMediaLayout.release();
        }
    }


}
