package com.sompom.tookitup.model.request;

/**
 * Created by imac on 7/31/17.
 */

public class LikeLifeStreamRequest {
    //CHECKSTYLE:OFF
    private String lifeStream;
    private String user;
    //CHECKSTYLE:OFF

    public LikeLifeStreamRequest(String lifeStream, String user) {
        this.lifeStream = lifeStream;
        this.user = user;
    }
}
