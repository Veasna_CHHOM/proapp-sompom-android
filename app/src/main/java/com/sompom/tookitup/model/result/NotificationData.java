package com.sompom.tookitup.model.result;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.sompom.tookitup.model.BaseNotificationModel;
import com.sompom.tookitup.model.LifeStream;

import java.util.Date;


public class NotificationData extends ErrorSupportModel implements Parcelable, BaseNotificationModel {

    public static final Creator<NotificationData> CREATOR = new Creator<NotificationData>() {
        @Override
        public NotificationData createFromParcel(Parcel source) {
            return new NotificationData(source);
        }

        @Override
        public NotificationData[] newArray(int size) {
            return new NotificationData[size];
        }
    };
    private static final String FIELD_PRODUCT = "product";
    private static final String FIELD_BUYER = "buyer";
    private static final String FIELD_LIFE_STREAM = "lifeStream";
    private static final String FIELD_USER = "user";
    private static final String FIELD_TYPE = "type";
    private static final String FIELD_DATE = "date";
    private static final String FIELD_CONVERSATION = "conversation";
    private static final String FIELD_IS_READ = "isRead";

    @SerializedName(FIELD_PRODUCT)
    private Product mProduct;
    @SerializedName(FIELD_BUYER)
    private User mBuyer;
    @SerializedName(FIELD_USER)
    private User mUser;
    @SerializedName(FIELD_LIFE_STREAM)
    private LifeStream mLifeStream;
    @SerializedName(FIELD_TYPE)
    private String mType;
    @SerializedName(FIELD_DATE)
    private Date mDate;
    @SerializedName(FIELD_CONVERSATION)
    private Conversation mConversation;
    @SerializedName(FIELD_IS_READ)
    private boolean mIsRead;

    public NotificationData() {

    }

    protected NotificationData(Parcel in) {
        this.mProduct = in.readParcelable(Product.class.getClassLoader());
        this.mBuyer = in.readParcelable(User.class.getClassLoader());
        this.mUser = in.readParcelable(User.class.getClassLoader());
        this.mLifeStream = in.readParcelable(LifeStream.class.getClassLoader());
        this.mType = in.readString();
        long tmpMCreateDate = in.readLong();
        this.mDate = tmpMCreateDate == -1 ? null : new Date(tmpMCreateDate);
        this.mConversation = in.readParcelable(Conversation.class.getClassLoader());
        this.mIsRead = in.readByte() != 0;
    }

    public Product getProduct() {
        return mProduct;
    }

    public void setProduct(Product product) {
        mProduct = product;
    }

    public User getBuyer() {
        return mBuyer;
    }

    public void setBuyer(User buyer) {
        mBuyer = buyer;
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser = user;
    }

    public OneSignalModel.Type getType() {
        return OneSignalModel.Type.fromValue(mType);
    }

    public void setType(String type) {
        mType = type;
    }

    public long getDate() {
        return mDate.getTime();
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public Conversation getConversation() {
        return mConversation;
    }

    public void setConversation(Conversation conversation) {
        mConversation = conversation;
    }

    public LifeStream getLifeStream() {
        return mLifeStream;
    }

    public void setLifeStream(LifeStream lifeStream) {
        mLifeStream = lifeStream;
    }

    public boolean isRead() {
        return mIsRead;
    }

    public void setRead(boolean read) {
        mIsRead = read;
    }

    @Override
    public String toString() {
        return "product = " + mProduct +
                ", buyer = " + mBuyer +
                ", user = " + mUser +
                ", type = " + mType +
                ", date = " + mDate.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mProduct, flags);
        dest.writeParcelable(this.mBuyer, flags);
        dest.writeParcelable(this.mUser, flags);
        dest.writeParcelable(this.mLifeStream, flags);
        dest.writeString(this.mType);
        dest.writeLong(this.mDate != null ? this.mDate.getTime() : -1);
        dest.writeParcelable(this.mConversation, flags);
        dest.writeByte(this.mIsRead ? (byte) 1 : (byte) 0);
    }
}