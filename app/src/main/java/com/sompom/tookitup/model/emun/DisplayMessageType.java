package com.sompom.tookitup.model.emun;

/**
 * Created by He Rotha on 9/11/17.
 */
public enum DisplayMessageType {
    Default, MyStoreBlock, MyStoreDeactivate, OtherBlockOrDeactivate
}
