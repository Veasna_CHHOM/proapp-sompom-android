package com.sompom.tookitup.model;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.annotation.StringRes;

import com.sompom.tookitup.R;


/**
 * Created by He Rotha on 3/28/18.
 */
public enum NotificationChannelSetting {
    UploadingLifeStream("1", R.string.product_form_notification_uploading_in_progress),
    UploadingProduct("1", R.string.product_form_notification_uploading_in_progress),
    Chat("chat", R.string.chat_notification_channel_name),
    Home("home", R.string.comment_notification);

    private String mNotificationID;
    @StringRes
    private int mNotificationName;

    NotificationChannelSetting(String notificationID,
                               @StringRes int notificationName) {
        mNotificationID = notificationID;
        mNotificationName = notificationName;
    }

    public String getNotificationID() {
        return mNotificationID;
    }

    @StringRes
    public int getNotificationName() {
        return mNotificationName;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public NotificationChannel getNotificationChannel(Context context) {
        NotificationChannel channel =
                new NotificationChannel(mNotificationID, context.getString(mNotificationName), NotificationManager.IMPORTANCE_LOW);
        channel.setVibrationPattern(new long[]{0L});
        channel.setSound(null, null);
        return channel;
    }
}
