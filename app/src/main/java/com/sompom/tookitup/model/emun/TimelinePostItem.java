package com.sompom.tookitup.model.emun;

/**
 * Created by nuonveyo on 8/8/18.
 */

public enum TimelinePostItem {
    TimelineItem(1),
    ProductItem(2);
    private int mId;

    TimelinePostItem(int id) {
        mId = id;
    }

    public static TimelinePostItem getTimelineItem(int id) {
        for (TimelinePostItem timelinePostItem : TimelinePostItem.values()) {
            if (timelinePostItem.getId() == id) {
                return timelinePostItem;
            }
        }
        return TimelineItem;
    }

    public int getId() {
        return mId;
    }
}
