package com.sompom.tookitup.model.result;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ContentStat implements Parcelable {

    public static final Creator<ContentStat> CREATOR = new Creator<ContentStat>() {
        @Override
        public ContentStat createFromParcel(Parcel source) {
            return new ContentStat(source);
        }

        @Override
        public ContentStat[] newArray(int size) {
            return new ContentStat[size];
        }
    };
    @SerializedName("contentType")
    private String mContentType;
    @SerializedName("createdAt")
    private String mCreatedAt;
    @SerializedName("totalComments")
    private Long mTotalComments;
    @SerializedName("totalFollowers")
    private Long mTotalFollowers;
    @SerializedName("totalFollowings")
    private Long mTotalFollowings;
    @SerializedName("totalLikes")
    private Long mTotalLikes;
    @SerializedName("totalViews")
    private Long mTotalViews;
    @SerializedName("updatedAt")
    private String mUpdatedAt;
    @SerializedName("viewers")
    private List<User> mViewers;
    @SerializedName("_id")
    private String mId;
    @SerializedName("totalShares")
    private Long mTotalShares;

    public ContentStat() {
    }

    protected ContentStat(Parcel in) {
        this.mContentType = in.readString();
        this.mCreatedAt = in.readString();
        this.mTotalComments = (Long) in.readValue(Long.class.getClassLoader());
        this.mTotalFollowers = (Long) in.readValue(Long.class.getClassLoader());
        this.mTotalFollowings = (Long) in.readValue(Long.class.getClassLoader());
        this.mTotalLikes = (Long) in.readValue(Long.class.getClassLoader());
        this.mTotalViews = (Long) in.readValue(Long.class.getClassLoader());
        this.mUpdatedAt = in.readString();
        this.mViewers = in.createTypedArrayList(User.CREATOR);
        this.mId = in.readString();
        this.mTotalShares = (Long) in.readValue(Long.class.getClassLoader());
    }

    public String getContentType() {
        return mContentType;
    }

    public void setContentType(String contentType) {
        mContentType = contentType;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public long getTotalComments() {
        if (mTotalComments == null) {
            return 0;
        }
        return mTotalComments;
    }

    public void setTotalComments(Long totalComments) {
        mTotalComments = totalComments;
    }

    public long getTotalFollowers() {
        if (mTotalFollowers == null) {
            return 0;
        }
        return mTotalFollowers;
    }

    public void setTotalFollowers(Long totalFollowers) {
        mTotalFollowers = totalFollowers;
    }

    public long getTotalFollowings() {
        if (mTotalFollowings == null) {
            return 0;
        }
        return mTotalFollowings;
    }

    public void setTotalFollowings(Long totalFollowings) {
        mTotalFollowings = totalFollowings;
    }

    public long getTotalLikes() {
        if (mTotalLikes == null) {
            return 0;
        }
        return mTotalLikes;
    }

    public void setTotalLikes(Long totalLikes) {
        mTotalLikes = totalLikes;
    }

    public long getTotalViews() {
        if (mTotalViews == null) {
            return 0;
        }
        return mTotalViews;
    }

    public void setTotalViews(Long totalViews) {
        mTotalViews = totalViews;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        mUpdatedAt = updatedAt;
    }

    public List<User> getViewers() {
        return mViewers;
    }

    public void setViewers(List<User> viewers) {
        mViewers = viewers;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public long getTotalShares() {
        if (mTotalShares == null) {
            return 0;
        }
        return mTotalShares;
    }

    public void setTotalShares(Long totalShares) {
        mTotalShares = totalShares;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mContentType);
        dest.writeString(this.mCreatedAt);
        dest.writeValue(this.mTotalComments);
        dest.writeValue(this.mTotalFollowers);
        dest.writeValue(this.mTotalFollowings);
        dest.writeValue(this.mTotalLikes);
        dest.writeValue(this.mTotalViews);
        dest.writeString(this.mUpdatedAt);
        dest.writeTypedList(this.mViewers);
        dest.writeString(this.mId);
        dest.writeValue(this.mTotalShares);
    }
}
