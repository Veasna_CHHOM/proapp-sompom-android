package com.sompom.tookitup.model;

import com.sompom.tookitup.model.result.User;

/**
 * Created by He Rotha on 9/6/18.
 */
public class TypingChatItem implements BaseChatModel {
    private User mSender;

    public User getSender() {
        return mSender;
    }

    public void setSender(User sender) {
        mSender = sender;
    }

    @Override
    public long getTime() {
        return 0;
    }
}
