package com.sompom.tookitup.model;

import android.view.View;

import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.utils.ChatUtility;

/**
 * Created by He Rotha on 9/14/17.
 */

public class SelectedChat {
    private View mSelectView;
    private Chat mSelectChat;
    private ChatUtility.GroupMessage mGroupMessage;

    public SelectedChat() {
    }

    public void setSelect(View selectView,
                          Chat selectChat,
                          ChatUtility.GroupMessage groupMessage) {
        mSelectView = selectView;
        mSelectChat = selectChat;
        mGroupMessage = groupMessage;
    }

    public View getSelectView() {
        return mSelectView;
    }

    public Chat getSelectChat() {
        return mSelectChat;
    }

    public ChatUtility.GroupMessage getGroupMessage() {
        return mGroupMessage;
    }
}
