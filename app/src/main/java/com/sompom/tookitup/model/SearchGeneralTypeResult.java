package com.sompom.tookitup.model;

import com.google.gson.annotations.SerializedName;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.LoadMoreWrapper;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;

/**
 * Created by nuonveyo on 7/2/18.
 */

public class SearchGeneralTypeResult {
    @SerializedName("products")
    private LoadMoreWrapper<Product> mProduct;
    @SerializedName("people")
    private LoadMoreWrapper<User> mUser;
    @SerializedName("buying")
    private LoadMoreWrapper<Conversation> mBuyingConversation;
    @SerializedName("selling")
    private LoadMoreWrapper<Conversation> mSellingConversation;
    @SerializedName("conversation")
    private LoadMoreWrapper<Conversation> mConversation;

    public LoadMoreWrapper<Product> getProduct() {
        return mProduct;
    }

    public void setProduct(LoadMoreWrapper<Product> product) {
        mProduct = product;
    }

    public LoadMoreWrapper<User> getUser() {
        return mUser;
    }

    public void setUser(LoadMoreWrapper<User> user) {
        mUser = user;
    }

    public LoadMoreWrapper<Conversation> getBuyingConversation() {
        return mBuyingConversation;
    }

    public void setBuyingConversation(LoadMoreWrapper<Conversation> buyingConversation) {
        mBuyingConversation = buyingConversation;
    }

    public LoadMoreWrapper<Conversation> getSellingConversation() {
        return mSellingConversation;
    }

    public void setSellingConversation(LoadMoreWrapper<Conversation> sellingConversation) {
        mSellingConversation = sellingConversation;
    }

    public LoadMoreWrapper<Conversation> getConversation() {
        return mConversation;
    }

    public void setConversation(LoadMoreWrapper<Conversation> conversation) {
        mConversation = conversation;
    }
}
