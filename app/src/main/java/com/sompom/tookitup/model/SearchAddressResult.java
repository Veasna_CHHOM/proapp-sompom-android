package com.sompom.tookitup.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

/**
 * Created by nuonveyo on 6/11/18.
 */

public class SearchAddressResult implements Parcelable {
    private String mId;
    private String mFullAddress;
    private String mCity;
    private String mCountry;
    private Locations mLocations;

    public SearchAddressResult() {
    }

    public String getFullAddress() {
        return mFullAddress;
    }

    public void setFullAddress(String fullAddress) {
        mFullAddress = fullAddress;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getCountry() {
        return mCountry;
    }


    public void setCountry(String country) {
        mCountry = country;
    }

    public Locations getLocations() {
        return mLocations;
    }

    public void setLocations(Locations locations) {
        mLocations = locations;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getDisplayAddress() {
        if (!TextUtils.isEmpty(getCity()) && !TextUtils.isEmpty(getCountry())) {
            return getCity() + ", " + getCountry();
        } else if (TextUtils.isEmpty(getCity())) {
            return getCountry();
        } else {
            return getCity();
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeString(this.mFullAddress);
        dest.writeString(this.mCity);
        dest.writeString(this.mCountry);
        dest.writeParcelable(this.mLocations, flags);
    }

    protected SearchAddressResult(Parcel in) {
        this.mId = in.readString();
        this.mFullAddress = in.readString();
        this.mCity = in.readString();
        this.mCountry = in.readString();
        this.mLocations = in.readParcelable(Locations.class.getClassLoader());
    }

    public static final Creator<SearchAddressResult> CREATOR = new Creator<SearchAddressResult>() {
        @Override
        public SearchAddressResult createFromParcel(Parcel source) {
            return new SearchAddressResult(source);
        }

        @Override
        public SearchAddressResult[] newArray(int size) {
            return new SearchAddressResult[size];
        }
    };
}
