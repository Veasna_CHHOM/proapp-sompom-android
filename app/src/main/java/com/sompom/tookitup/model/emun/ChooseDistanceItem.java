package com.sompom.tookitup.model.emun;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.text.TextUtils;

import com.sompom.tookitup.R;

/**
 * Created by nuonveyo on 7/5/18.
 */

public enum ChooseDistanceItem {
    WALK(1,
            R.string.filter_detail_walk_title,
            R.string.walk_type,
            R.drawable.ic_man_walking_check,
            R.drawable.ic_man_walking),
    BIKE(2,
            R.string.filter_detail_bike_title,
            R.string.bike_type,
            R.drawable.ic_bicycle_check,
            R.drawable.ic_bicycle),
    MOTOR(3,
            R.string.filter_detail_motor_title,
            R.string.motor_type,
            R.drawable.ic_scooter_check,
            R.drawable.ic_scooter),
    CAR(4,
            R.string.filter_detail_car_title,
            R.string.car_type,
            R.drawable.ic_sports_car_check,
            R.drawable.ic_sports_car),
    TRUCK(5,
            R.string.filter_detail_truck_title,
            R.string.truck_type,
            R.drawable.ic_logistics_delivery_truck_in_movement_check,
            R.drawable.ic_logistics_delivery_truck_in_movement),
    WORLDWIDE(6,
            R.string.filter_detail_worldwide_title,
            R.string.worldwide_type,
            R.drawable.ic_airplane_around_earth_check,
            R.drawable.ic_airplane_around_earth);

    private int mId;
    @StringRes
    private int mTitle;
    @StringRes
    private int mType;
    @DrawableRes
    private int mCheckIcon;
    @DrawableRes
    private int mUnCheckIcon;

    ChooseDistanceItem(int id, int title, int type, int checkIcon, int unCheckIcon) {
        mId = id;
        mTitle = title;
        mType = type;
        mCheckIcon = checkIcon;
        mUnCheckIcon = unCheckIcon;
    }

    public static ChooseDistanceItem getValueFromId(int id) {
        for (ChooseDistanceItem chooseDistanceItem : ChooseDistanceItem.values()) {
            if (chooseDistanceItem.getId() == id) {
                return chooseDistanceItem;
            }
        }
        return WALK;
    }

    public static ChooseDistanceItem getChooseDistanceItemFromType(Context context, String type) {
        for (ChooseDistanceItem chooseDistanceItem : ChooseDistanceItem.values()) {
            if (TextUtils.equals(type, context.getString(chooseDistanceItem.getType()))) {
                return chooseDistanceItem;
            }
        }
        return WALK;
    }

    public int getId() {
        return mId;
    }

    public int getTitle() {
        return mTitle;
    }

    public int getType() {
        return mType;
    }

    public int getCheckIcon() {
        return mCheckIcon;
    }

    public int getUnCheckIcon() {
        return mUnCheckIcon;
    }
}
