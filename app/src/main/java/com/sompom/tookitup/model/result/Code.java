package com.sompom.tookitup.model.result;

/**
 * Created by he.rotha on 5/10/16.
 */
public class Code {
    //CHECKSTYLE:OFF
    private int code;
    private String message;
    //CHECKSTYLE:OFF

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setSuccess(String message) {
        this.message = message;
    }
}
