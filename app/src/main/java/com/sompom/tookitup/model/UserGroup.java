package com.sompom.tookitup.model;

import com.google.gson.annotations.SerializedName;
import com.sompom.tookitup.listener.UserListAdaptive;
import com.sompom.tookitup.model.result.User;

import java.util.List;

public class UserGroup implements UserListAdaptive {

    @SerializedName("_id")
    private String mId;

    @SerializedName("name")
    private String mName;

    @SerializedName("participants")
    private List<User> mParticipants;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public List<User> getParticipants() {
        return mParticipants;
    }

    public void setParticipants(List<User> participants) {
        mParticipants = participants;
    }
}
