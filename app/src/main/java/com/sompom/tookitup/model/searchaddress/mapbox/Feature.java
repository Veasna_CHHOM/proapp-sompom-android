package com.sompom.tookitup.model.searchaddress.mapbox;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Feature {

    @SerializedName("bbox")
    private List<Double> mBbox;
    @SerializedName("center")
    private List<Double> mCenter;
    @SerializedName("geometry")
    private Geometry mGeometry;
    @SerializedName("id")
    private String mId;
    @SerializedName("matching_place_name")
    private String mMatchingPlaceName;
    @SerializedName("matching_text")
    private String mMatchingText;
    @SerializedName("place_name")
    private String mPlaceName;
    @SerializedName("place_type")
    private List<String> mPlaceType;
    @SerializedName("properties")
    private Properties mProperties;
    @SerializedName("relevance")
    private Double mRelevance;
    @SerializedName("text")
    private String mText;
    @SerializedName("type")
    private String mType;

    public List<Double> getBbox() {
        return mBbox;
    }

    public void setBbox(List<Double> bbox) {
        mBbox = bbox;
    }

    public List<Double> getCenter() {
        return mCenter;
    }

    public void setCenter(List<Double> center) {
        mCenter = center;
    }

    public Geometry getGeometry() {
        return mGeometry;
    }

    public void setGeometry(Geometry geometry) {
        mGeometry = geometry;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getMatchingPlaceName() {
        return mMatchingPlaceName;
    }

    public void setMatchingPlaceName(String matchingPlaceName) {
        mMatchingPlaceName = matchingPlaceName;
    }

    public String getMatchingText() {
        return mMatchingText;
    }

    public void setMatchingText(String matchingText) {
        mMatchingText = matchingText;
    }

    public String getPlaceName() {
        return mPlaceName;
    }

    public void setPlaceName(String placeName) {
        mPlaceName = placeName;
    }

    public List<String> getPlaceType() {
        return mPlaceType;
    }

    public void setPlaceType(List<String> placeType) {
        mPlaceType = placeType;
    }

    public Properties getProperties() {
        return mProperties;
    }

    public void setProperties(Properties properties) {
        mProperties = properties;
    }

    public Double getRelevance() {
        return mRelevance;
    }

    public void setRelevance(Double relevance) {
        mRelevance = relevance;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

}
