package com.sompom.tookitup.model.notification;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class CommentTemp implements RealmModel {

    private static final String FIELD_ID = "_id";
    private static final String FIELD_USER = "authorInfo";
    @PrimaryKey
    @SerializedName(FIELD_ID)
    private String mId;
    @SerializedName(FIELD_USER)
    private UserTemp mUser;

    public CommentTemp() {
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public UserTemp getUser() {
        return mUser;
    }

    public void setUser(UserTemp user) {
        mUser = user;
    }

}