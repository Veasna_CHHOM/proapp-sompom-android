package com.sompom.tookitup.model.emun;

/**
 * Created by He Rotha on 3/18/19.
 */
public enum ContentType {
    PRODUCT("product"),
    POST("post"),
    USER("user"),
    COMMENT("comment"),
    MEDIA("media"),
    SUB_COMMENT("subcomment");

    private String mValue;

    ContentType(String value) {
        mValue = value;
    }

    public String getValue() {
        return mValue;
    }
}
