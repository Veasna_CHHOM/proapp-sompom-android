package com.sompom.tookitup.model.emun;

/**
 * Created by nuonveyo on 11/6/18.
 */

public enum CreateWallStreetScreenType {
    CREATE_POST(1),
    EDIT_POST(2),
    SHARE_POST(4),
    SHARE_LINK(5);

    private final int mId;

    CreateWallStreetScreenType(int id) {
        mId = id;
    }

    public static CreateWallStreetScreenType getType(int id) {
        for (CreateWallStreetScreenType createWallStreetScreenType : CreateWallStreetScreenType.values()) {
            if (createWallStreetScreenType.getId() == id) {
                return createWallStreetScreenType;
            }
        }
        return CREATE_POST;
    }

    public int getId() {
        return mId;
    }
}
