package com.sompom.tookitup.model.emun;

import android.text.TextUtils;

/**
 * Created by nuonveyo
 * on 1/18/19.
 */

public enum OneSignalPushType {
    Conversation("message"),
    Comment("comment");

    private String mType;

    OneSignalPushType(String type) {
        mType = type;
    }

    public static OneSignalPushType getType(String type) {
        for (OneSignalPushType type1 : OneSignalPushType.values()) {
            if (TextUtils.equals(type1.getType(), type)) {
                return type1;
            }
        }
        return Conversation;
    }

    public String getType() {
        return mType;
    }
}
