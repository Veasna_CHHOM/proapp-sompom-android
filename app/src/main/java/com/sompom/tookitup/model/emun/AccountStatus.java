package com.sompom.tookitup.model.emun;

/**
 * Created by He Rotha on 2/12/19.
 */
public enum AccountStatus {
    OPEN(1), BLOCK(0);
    private final int mStatus;

    AccountStatus(int i) {
        mStatus = i;
    }

    public static AccountStatus fromValue(Integer value) {
        if (value == null) {
            return OPEN;
        }
        for (AccountStatus s : AccountStatus.values()) {
            if (value == s.mStatus) {
                return s;
            }
        }
        return OPEN;
    }

    public int getStatus() {
        return mStatus;
    }
}
