package com.sompom.tookitup.model.request;

import android.text.TextUtils;

/**
 * Created by he.rotha on 4/28/16.
 */
public class SoldProductRequest {
    //CHECKSTYLE:OFF
    private String buyer;
    //CHECKSTYLE:OFF

    public SoldProductRequest(String buyer) {
        if (TextUtils.isEmpty(buyer)) {
            //buyer == -1 or buyer == anonymous, for sell to Anonymous Buyers
            this.buyer = "-1";
        } else {
            this.buyer = buyer;
        }
    }
}
