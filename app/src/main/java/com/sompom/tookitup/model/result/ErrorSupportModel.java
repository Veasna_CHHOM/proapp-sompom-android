package com.sompom.tookitup.model.result;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imac on 5/31/16.
 */
public class ErrorSupportModel {
    @SerializedName("code")
    private int mCode;
    @SerializedName("errorMessage")
    private String mMessage;
    private Object mTag;

    public ErrorSupportModel() {
    }

    public ErrorSupportModel(String message) {
        mCode = -1;
        mMessage = message;
    }

    public ErrorSupportModel(int code, String message) {
        mCode = code;
        mMessage = message;
    }


    public int getCode() {
        return mCode;
    }

    public void setCode(final int code) {
        mCode = code;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(final String message) {
        mMessage = message;
    }

    public boolean isError() {
        return mCode >= 400 && mCode <= 600;
    }

    @Override
    public String toString() {
        return mMessage;
    }

    public Object getTag() {
        return mTag;
    }

    public void setTag(Object tag) {
        mTag = tag;
    }
}
