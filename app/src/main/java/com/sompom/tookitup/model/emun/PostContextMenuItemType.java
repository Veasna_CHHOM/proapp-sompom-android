package com.sompom.tookitup.model.emun;

import com.sompom.tookitup.R;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.PostContextMenuItem;
import com.sompom.tookitup.model.WallStreetAdaptive;
import com.sompom.tookitup.model.result.Chat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nuonveyo on 10/31/18.
 */

public enum PostContextMenuItemType {
    EDIT_POST_CLICK(1),
    EDIT_PRIVACY_CLICK(2),
    REPORT_CLICK(3),
    TURNOFF_NOTIFICATION_CLICK(4),
    DELETE_CLICK(5),

    FOLLOWER_CLICK(6),
    EVERYONE_CLICK(7),

    COPY_TEXT_CLICK(8),
    SAVE_IMAGE_CLICK(9),
    FORWARD_CLICK(11),
    SHARE_CLICK(12),

    WRITE_POST_CLICK(13),
    SENDTO_MESSENGER_CLICK(14),

    ARCHIVE_CONVERSATION(15),
    DELETE_CONVERSATION(16),

    UNFOLLOW_CLICK(17);


    private final int mId;

    PostContextMenuItemType(int id) {
        mId = id;
    }

    public static PostContextMenuItemType getItemType(int id) {
        for (PostContextMenuItemType postContextMenuItemType : PostContextMenuItemType.values()) {
            if (postContextMenuItemType.getId() == id) {
                return postContextMenuItemType;
            }
        }
        return EDIT_POST_CLICK;
    }

    public static List<PostContextMenuItem> getItems(Adaptive adaptive, boolean isOwner, TimelinePostItem timelinePostItem) {
        boolean isProduct = timelinePostItem == TimelinePostItem.ProductItem;
        List<PostContextMenuItem> items = new ArrayList<>();
        if (isOwner) {
            items.add(new PostContextMenuItem(PostContextMenuItemType.EDIT_POST_CLICK.getId(),
                    R.string.code_pencil,
                    isProduct ? R.string.post_menu_edit_product_title : R.string.post_menu_edit_post_title));
            items.add(new PostContextMenuItem(PostContextMenuItemType.EDIT_PRIVACY_CLICK.getId(),
                    R.string.code_earth,
                    R.string.post_menu_edit_privacy_title));
            items.add(new PostContextMenuItem(PostContextMenuItemType.DELETE_CLICK.getId(),
                    R.string.code_trash,
                    isProduct ? R.string.post_menu_delete_product_title : R.string.post_menu_delete_post_title));
        } else {
            if (adaptive instanceof WallStreetAdaptive) {
                if (((WallStreetAdaptive) adaptive).getUser().isFollow()) {
                    items.add(new PostContextMenuItem(PostContextMenuItemType.UNFOLLOW_CLICK.getId(),
                            R.string.code_flag,
                            R.string.unfollow));
                }
            }
            items.add(new PostContextMenuItem(PostContextMenuItemType.REPORT_CLICK.getId(),
                    R.string.code_flag,
                    isProduct ? R.string.product_detail_menu_report_title : R.string.post_menu_report_post_title));
        }
        items.add(new PostContextMenuItem(PostContextMenuItemType.TURNOFF_NOTIFICATION_CLICK.getId(),
                R.string.code_alarm,
                isProduct ? R.string.post_menu_turn_off_product_notification_title : R.string.post_menu_turn_off_notification_title));
        return items;
    }

    public static List<PostContextMenuItem> getItems(boolean isMultiImage, boolean isShowDelete, Chat.Type type) {
        List<PostContextMenuItem> items = new ArrayList<>();
        if (type == Chat.Type.TEXT) {
            items.add(new PostContextMenuItem(PostContextMenuItemType.COPY_TEXT_CLICK.getId(),
                    R.string.code_bookmark,
                    R.string.chat_copy_title));
        } else if (type == Chat.Type.IMAGE) {
            items.add(new PostContextMenuItem(PostContextMenuItemType.SAVE_IMAGE_CLICK.getId(),
                    R.string.code_bookmark,
                    isMultiImage ? R.string.chat_save_all_image_title : R.string.edit_profile_button_save));
        }
        items.add(new PostContextMenuItem(PostContextMenuItemType.FORWARD_CLICK.getId(),
                R.string.code_share,
                R.string.chat_forward_title));
        if (isShowDelete) {
            items.add(new PostContextMenuItem(PostContextMenuItemType.DELETE_CLICK.getId(),
                    R.string.code_trash,
                    R.string.seller_store_button_delete));
        }
        return items;
    }

    public static List<PostContextMenuItem> getItemsShare(boolean isIncludeShare) {
        List<PostContextMenuItem> items = new ArrayList<>();
        items.add(new PostContextMenuItem(PostContextMenuItemType.WRITE_POST_CLICK.getId(),
                R.string.code_pencil,
                R.string.post_menu_write_post));
        if (isIncludeShare) {
            items.add(new PostContextMenuItem(PostContextMenuItemType.SENDTO_MESSENGER_CLICK.getId(),
                    R.string.code_comment,
                    R.string.post_menu_send_to_messenger));
            items.add(new PostContextMenuItem(PostContextMenuItemType.COPY_TEXT_CLICK.getId(),
                    R.string.code_bookmark,
                    R.string.post_menu_copy_link));
        }
        return items;
    }

    public static List<PostContextMenuItem> getCommentPopupMenuItem(boolean isOwner) {
        List<PostContextMenuItem> items = new ArrayList<>();
        items.add(new PostContextMenuItem(PostContextMenuItemType.COPY_TEXT_CLICK.getId(),
                R.string.code_bookmark,
                R.string.chat_copy_title));
        if (isOwner) {
            items.add(new PostContextMenuItem(PostContextMenuItemType.EDIT_POST_CLICK.getId(),
                    R.string.code_pencil,
                    R.string.seller_store_button_edit));
            items.add(new PostContextMenuItem(PostContextMenuItemType.DELETE_CLICK.getId(),
                    R.string.code_trash,
                    R.string.seller_store_button_delete));
        }
        return items;
    }

    public static List<PostContextMenuItem> getConversationPopupMenuItem() {
        List<PostContextMenuItem> items = new ArrayList<>();
        items.add(new PostContextMenuItem(PostContextMenuItemType.ARCHIVE_CONVERSATION.getId(),
                R.string.code_trash,
                R.string.message_archived));

        //TODO: currently, Delete feature is postponed
//        items.add(new PostContextMenuItem(PostContextMenuItemType.DELETE_CONVERSATION.getId(),
//                R.string.code_trash,
//                R.string.seller_store_button_delete));

        return items;
    }


    public int getId() {
        return mId;
    }
}
