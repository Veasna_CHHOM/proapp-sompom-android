package com.sompom.tookitup.model.notification;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class ProductTemp implements RealmModel {

    private static final String FIELD_ID = "_id";
    private static final String FIELD_PRICE = "price";
    private static final String FIELD_USER = "seller";
    private static final String FIELD_CURRENCY = "currency";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_MEDIA = "media";
    @PrimaryKey
    @SerializedName(FIELD_ID)
    private String mId;
    @SerializedName(FIELD_PRICE)
    private double mPrice;
    @SerializedName(FIELD_NAME)
    private String mName;
    @SerializedName(FIELD_MEDIA)
    private RealmList<MediaTemp> mProductMedia;
    @SerializedName(FIELD_CURRENCY)
    private String mCurrency;
    @SerializedName(FIELD_USER)
    private UserTemp mUser;

    public String getId() {
        return mId;
    }

    public double getPrice() {
        return mPrice;
    }

    public String getName() {
        return mName;
    }

    public RealmList<MediaTemp> getProductMedia() {
        return mProductMedia;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public UserTemp getUser() {
        return mUser;
    }
}