package com.sompom.tookitup.model;

import com.google.gson.annotations.SerializedName;
import com.sompom.tookitup.model.emun.OneSignalPushType;

/**
 * Created by nuonveyo
 * on 1/18/19.
 */

public class OneSignalWrapperData<T> {
    @SerializedName("content")
    private T mData;
    @SerializedName("type")
    private String mType;

    public T getData() {
        return mData;
    }

    public void setData(T data) {
        mData = data;
    }

    public OneSignalPushType getType() {
        return OneSignalPushType.getType(mType);
    }

    public void setType(String type) {
        mType = type;
    }
}
