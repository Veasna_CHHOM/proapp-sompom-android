package com.sompom.tookitup.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by imac on 6/16/17.
 */

public class Follow implements Parcelable {
    public static final Parcelable.Creator<Follow> CREATOR = new Parcelable.Creator<Follow>() {
        @Override
        public Follow createFromParcel(Parcel source) {
            return new Follow(source);
        }

        @Override
        public Follow[] newArray(int size) {
            return new Follow[size];
        }
    };
    //"follow": {
//        "id": "17f4bb05-3d03-4311-b523-8da5235284aa",
//                "buyer": "3b7ff6aa-241e-431d-8fcf-1c31608e75c1",
//                "seller": "2234c71e-75df-48a3-9283-5c0bb0ca26be",
//                "isFollow": true
//    },
    @SerializedName("_id")
    private String mId;
    @SerializedName("buyer")
    private String mBuyer;
    @SerializedName("seller")
    private String mSeller;
    @SerializedName("isFollow")
    private boolean mIsFollow;

    public Follow() {
    }

    private Follow(Parcel in) {
        this.mId = in.readString();
        this.mBuyer = in.readString();
        this.mSeller = in.readString();
        this.mIsFollow = in.readByte() != 0;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getBuyer() {
        return mBuyer;
    }

    public void setBuyer(String buyer) {
        mBuyer = buyer;
    }

    public String getSeller() {
        return mSeller;
    }

    public void setSeller(String seller) {
        mSeller = seller;
    }

    public boolean isFollow() {
        return mIsFollow;
    }

    public void setFollow(boolean follow) {
        mIsFollow = follow;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeString(this.mBuyer);
        dest.writeString(this.mSeller);
        dest.writeByte(this.mIsFollow ? (byte) 1 : (byte) 0);
    }
}
