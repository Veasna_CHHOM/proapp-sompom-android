package com.sompom.tookitup.model.result;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ChhomVeasna on 10/31/17.
 */

public class ForgotPasswordResponse {

    @SerializedName("token")
    private String mToken;

    public String getToken() {
        return mToken;
    }
}
