package com.sompom.tookitup.model.request;

/**
 * Created by he.rotha on 4/26/16.
 */
public class SocialRequest {
    //CHECKSTYLE:OFF
    private String socialId;
    //CHECKSTYLE:OFF

    public SocialRequest(String socialId) {
        this.socialId = socialId;
    }
}
