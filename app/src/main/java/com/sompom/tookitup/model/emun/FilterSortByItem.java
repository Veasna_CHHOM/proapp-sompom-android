package com.sompom.tookitup.model.emun;

import android.support.annotation.StringRes;

import com.sompom.tookitup.R;

/**
 * Created by nuonveyo on 7/5/18.
 */

public enum FilterSortByItem {
    Nearest(1, R.string.filter_detail_sort_nearest_title),
    Recent(2, R.string.filter_detail_sort_recent_title),
    Popular(3, R.string.filter_detail_sort_popular_title),
    LowestPrice(4, R.string.filter_detail_sort_lowest_price_title),
    HighestPrice(5, R.string.filter_sort_highest_price_title);

    private int mId;
    @StringRes
    private int mTitle;

    FilterSortByItem(int id, int title) {
        mId = id;
        mTitle = title;
    }

    public static FilterSortByItem getValueFromId(int id) {
        for (FilterSortByItem filterSortByItem : FilterSortByItem.values()) {
            if (filterSortByItem.getId() == id) {
                return filterSortByItem;
            }
        }
        return FilterSortByItem.Nearest;
    }

    public static FilterSortByItem getValueFromIndex(int index) {
        for (FilterSortByItem filterSortByItem : FilterSortByItem.values()) {
            if (filterSortByItem.getId() == index) {
                return filterSortByItem;
            }
        }
        return FilterSortByItem.Nearest;
    }

    public int getId() {
        return mId;
    }

    public int getTitle() {
        return mTitle;
    }


}
