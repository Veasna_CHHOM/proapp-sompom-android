package com.sompom.tookitup.model;

import com.google.gson.annotations.SerializedName;

public class Price {
    @SerializedName("min")
    private double mMinPrice;
    @SerializedName("max")
    private double mMaxPrice;

    public double getMinPrice() {
        return mMinPrice;
    }

    public double getMaxPrice() {
        return mMaxPrice;
    }
}
