package com.sompom.tookitup.model.emun;

import android.support.annotation.Nullable;

/**
 * Created by He Rotha on 8/25/17.
 */
public enum ItemType {

    TIMELINE(101, "POST", 3, 3, null),
    ITEM(102, "PRODUCT", 1, 1, null),
    ADS_ITEM(103, "ADS_TIMELINE", 1, 1, null),
    LIVE_VIDEO(104, "LIVE_VIDEO", 1, 1, AdsUnit.STREET_WALL),

    SHARED_TIMELINE(201, "SHARED_TIMELINE", 1, 1, null),
    SHARED_ITEM(202, "SHARED_PRODUCT", 1, 1, null),
    SHARED_ADS_ITEM(203, "SHARE_ADS_TIMELINE", 1, 1, null),
    SHARED_LIVE_VIDEO(204, "SHARED_LIVE_VIDEO", 1, 1, AdsUnit.STREET_WALL),

    VIEW_PAGGER_AD(303, "VIEW_PAGGER_AD", 1, -1, null),

    ADS_FULL_MOBILE(401, "FULL_MOBILE", 2, 3, AdsUnit.STREET_WALL), //no use
    ADS_FOUR_SQUARE(402, "FOUR_SQUARE", 2, 2, AdsUnit.STREET_WALL),//no use
    ADS_FULL_H(403, "FULL_H", 1, 3, AdsUnit.STREET_WALL),//no use
    ADS_TWO_VERTICAL(404, "TWO_VERTICAL", 2, 1, AdsUnit.STREET_WALL),//no use
    ADS_TWO_HORIZONTAL(405, "TWO_HORIZONTAL", 1, 2, AdsUnit.STREET_WALL),//no use
    ADS_THREE_HORIZONTAL(406, "THREE_HORIZONTAL", 1, 3, AdsUnit.STREET_WALL),//no use
    ADS_BANNER(407, "BANNER", 1, -1, AdsUnit.STREET_WALL);

    private String mValue;
    private int mRow;
    private int mCol;
    private int mViewType;
    private AdsUnit mAdsUnit;

    ItemType(int viewType,
             String value,
             int row,
             int col,
             AdsUnit adsUnit) {
        mValue = value;
        mViewType = viewType;
        mRow = row;
        mCol = col;
        mAdsUnit = adsUnit;
    }

    public static ItemType fromValue(String value) {
        for (ItemType s : ItemType.values()) {
            if (s.getValue().equals(value)) {
                return s;
            }
        }
        return ITEM;
    }

    public String getValue() {
        return mValue;
    }

    public int getViewType() {
        return mViewType;
    }

    public int getRow() {
        return mRow;
    }

    public int getCol() {
        return mCol;
    }

    @Nullable
    public AdsUnit getAdsUnit() {
        return mAdsUnit;
    }
}
