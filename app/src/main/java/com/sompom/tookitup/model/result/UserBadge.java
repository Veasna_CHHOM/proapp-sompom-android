package com.sompom.tookitup.model.result;

import com.google.gson.annotations.SerializedName;

public class UserBadge {
    @SerializedName("badgeAroundMe")
    private Long mNearBy;
    @SerializedName("badgeMessages")
    private Long mUnreadMessage;
    @SerializedName("badgeNotifications")
    private Long mUnreadNotification;

    public Long getNearBy() {
        if (mNearBy == null) {
            return 0L;
        }
        return mNearBy;
    }

    public void setNearBy(Long nearBy) {
        mNearBy = nearBy;
    }

    public Long getUnreadMessage() {
        if (mUnreadMessage == null) {
            return 0L;
        }
        return mUnreadMessage;
    }

    public void setUnreadMessage(Long unreadMessage) {
        mUnreadMessage = unreadMessage;
    }

    public Long getUnreadNotification() {
        if (mUnreadNotification == null) {
            return 0L;
        }
        return mUnreadNotification;
    }

    public void setUnreadNotification(Long unreadNotification) {
        mUnreadNotification = unreadNotification;
    }

}
