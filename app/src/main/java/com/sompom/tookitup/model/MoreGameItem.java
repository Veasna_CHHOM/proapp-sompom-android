package com.sompom.tookitup.model;

import com.sompom.tookitup.helper.IntentData;
import com.sompom.tookitup.model.emun.ViewType;
import com.sompom.tookitup.model.result.MoreGame;

import java.util.ArrayList;

/**
 * Created by He Rotha on 9/5/17.
 */

public class MoreGameItem extends ArrayList<MoreGame> implements Adaptive {
    @Override
    public String getId() {
        return null;
    }

    @Override
    public ViewType getTimelineViewType() {
        return ViewType.ViewPagerAd;
    }

    @Override
    public void startActivityForResult(IntentData requiredIntentData) {
        //no starting activity
    }
}
