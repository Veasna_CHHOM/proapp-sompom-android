package com.sompom.tookitup.model;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.sompom.baseactivity.ResultCallback;
import com.sompom.tookitup.helper.IntentData;
import com.sompom.tookitup.intent.newintent.TimelineDetailIntent;
import com.sompom.tookitup.intent.newintent.TimelineDetailIntentResult;
import com.sompom.tookitup.model.emun.ItemShape;
import com.sompom.tookitup.model.emun.MediaType;
import com.sompom.tookitup.model.emun.PublishItem;
import com.sompom.tookitup.model.emun.ViewType;
import com.sompom.tookitup.model.result.ContentStat;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.widget.lifestream.Format;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class LifeStream implements Adaptive, Parcelable, WallStreetAdaptive {

    public static final Creator<LifeStream> CREATOR = new Creator<LifeStream>() {
        @Override
        public LifeStream createFromParcel(Parcel source) {
            return new LifeStream(source);
        }

        @Override
        public LifeStream[] newArray(int size) {
            return new LifeStream[size];
        }
    };
    private static final String FIELD_MEDIA = "media";
    private static final String FIELD_TITLE = "text";
    private static final String FIELD_USER = "userstore";
    private static final String FIELD_VIDEO_SHAPE = "shape";
    @SerializedName(FIELD_ID)
    private String mId;
    @SerializedName(FIELD_IS_LIKE)
    private boolean mIsLike;
    @SerializedName(FIELD_CREATE_DATE)
    private Date mCreateDate;
    @SerializedName(FIELD_USER_VIEW)
    private List<User> mUserView;
    @SerializedName(FIELD_PUBLISH)
    private int mPublish;
    @SerializedName(FIELD_LATITUDE)
    private Double mLatitude;
    @SerializedName(FIELD_ADDRESS)
    private String mAddress;
    @SerializedName(FIELD_LONGITUDE)
    private Double mLongtitude;
    @SerializedName(FIELD_MEDIA)
    private List<Media> mMedia;
    @SerializedName(FIELD_TITLE)
    private String mTitle;
    @SerializedName(FIELD_USER)
    private User mStoreUser;
    @SerializedName(FIELD_SHARE_URL)
    private String mShareUrl;
    @SerializedName(FIELD_VIDEO_SHAPE)
    private String mItemShape;
    @SerializedName(FIELD_CITY)
    private String mCity;
    @SerializedName(FIELD_COUNTRY)
    private String mCountry;
    @SerializedName(FIELD_CONTENT_STAT)
    private ContentStat mContentStat;

    public LifeStream() {
    }

    public LifeStream(LifeStream lifeStream) {
        setAddress(lifeStream.getAddress());
        setCity(lifeStream.getCity());
        setCountry(lifeStream.getCountry());
        setTitle(lifeStream.getTitle());
        setPublish(lifeStream.mPublish);
        List<Media> medias = new ArrayList<>();
        for (Media media : lifeStream.getMedia()) {
            medias.add(new Media(media));
        }
        setMedia(medias);
        setLatitude(lifeStream.getLatitude());
        setLongitude(lifeStream.getLongitude());
        setStoreUser(lifeStream.getStoreUser());
        setCountry(lifeStream.getCountry());
    }


    protected LifeStream(Parcel in) {
        this.mId = in.readString();
        this.mIsLike = in.readByte() != 0;
        long tmpMCreateDate = in.readLong();
        this.mCreateDate = tmpMCreateDate == -1 ? null : new Date(tmpMCreateDate);
        this.mUserView = in.createTypedArrayList(User.CREATOR);
        this.mPublish = in.readInt();
        this.mLatitude = (Double) in.readValue(Double.class.getClassLoader());
        this.mAddress = in.readString();
        this.mLongtitude = (Double) in.readValue(Double.class.getClassLoader());
        this.mMedia = in.createTypedArrayList(Media.CREATOR);
        this.mTitle = in.readString();
        this.mStoreUser = in.readParcelable(User.class.getClassLoader());
        this.mShareUrl = in.readString();
        this.mItemShape = in.readString();
        this.mCity = in.readString();
        this.mCountry = in.readString();
        this.mContentStat = in.readParcelable(ContentStat.class.getClassLoader());
    }

    @Override
    public String getId() {
        return mId;
    }

    @Override
    public void setId(String id) {
        mId = id;
    }

    @Override
    public boolean isLike() {
        return mIsLike;
    }

    @Override
    public void setLike(boolean like) {
        mIsLike = like;
    }

    @Override
    public Date getCreateDate() {
        return mCreateDate;
    }

    @Override
    public void setCreateDate(Date createDate) {
        mCreateDate = createDate;
    }

    @Override
    public List<User> getUserView() {
        return mUserView;
    }

    @Override
    public void setUserView(List<User> userView) {
        mUserView = userView;
    }

    @Override
    public PublishItem getPublish() {
        return PublishItem.getItem(mPublish);
    }

    @Override
    public void setPublish(int publish) {
        mPublish = publish;
    }

    @Override
    public ContentStat getContentStat() {
        if (mContentStat == null) {
            mContentStat = new ContentStat();
        }
        return mContentStat;
    }

    @Override
    public void setContentStat(ContentStat contentStat) {
        mContentStat = contentStat;
    }

    @Override
    public double getLatitude() {
        if (mLatitude == null) {
            return 0;
        }
        return mLatitude;
    }

    @Override
    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }

    @Override
    public String getAddress() {
        return mAddress;
    }

    @Override
    public void setAddress(String address) {
        mAddress = address;
    }

    @Override
    public String getCity() {
        return mCity;
    }

    @Override
    public void setCity(String city) {
        mCity = city;
    }

    @Override
    public double getLongitude() {
        if (mLongtitude == null) {
            return 0;
        }
        return mLongtitude;
    }

    @Override
    public void setLongitude(double longitude) {
        mLongtitude = longitude;
    }

    @Override
    public User getUser() {
        return mStoreUser;
    }

    public List<Media> getMedia() {
        return mMedia;
    }

    public void setMedia(List<Media> media) {
        mMedia = media;
    }

    @Override
    public String getDescription() {
        return getTitle();
    }

    @Override
    public String getShareUrl() {
        return mShareUrl;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public User getStoreUser() {
        return mStoreUser;
    }

    public void setStoreUser(User storeUser) {
        mStoreUser = storeUser;
    }

    public ItemShape getItemShape() {
        return ItemShape.fromValue(mItemShape);
    }

    public void setItemShape(ItemShape itemShape) {
        mItemShape = itemShape.getValue();
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof LifeStream && (obj == this || ((LifeStream) obj).getId().equals(getId()));
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public ViewType getTimelineViewType() {
        if (getMedia() == null || getMedia().isEmpty()) {
            return ViewType.Timeline;
        }

/*
    WallStreet Style 1
 */
//        if (getMedia().size() == 1) {
//            return ViewType.TimelineThreeOneItem;
//        } else if (getMedia().size() == 2) {
//            return ViewType.TimelineThreeTwoItem;
//        } else {
//            return ViewType.TimelineThreeThreeItem;
//        }

/*
    WallStreet Style 2
 */
        else {
            if (getMedia().size() == 1 && getMedia().get(0).getType() == MediaType.LIVE_VIDEO) {
                return ViewType.LiveVideoTimeline;
            } else {
                int firstWidth = getMedia().get(0).getWidth();
                int firstHeight = getMedia().get(0).getHeight();
                if (firstWidth != firstHeight) {
                    switch (getMedia().size()) {
                        case 1:
                            return ViewType.Timeline_FreeStyle_1;
                        case 2:
                            return firstWidth > firstHeight ?
                                    ViewType.Timeline_FreeStyle_2_Vertical :
                                    ViewType.Timeline_FreeStyle_2_Horizontal;
                        case 3:
                            return firstWidth > firstHeight ?
                                    ViewType.Timeline_FreeStyle_3_Vertical :
                                    ViewType.Timeline_FreeStyle_3_Horizontal;
                        case 4:
                            return firstWidth > firstHeight ?
                                    ViewType.Timeline_FreeStyle_4_Vertical :
                                    ViewType.Timeline_FreeStyle_4_Horizontal;
                        default:
                            return firstWidth > firstHeight ?
                                    ViewType.Timeline_FreeStyle_5_Vertical :
                                    ViewType.Timeline_FreeStyle_5_Horizontal;
                    }
                } else {
                    boolean isAllSquare = true;
                    for (int i = 1; i < getMedia().size(); i++) {
                        if (i > Format.SquareAll.getMaxDisplay() - 1) {
                            break;
                        }
                        if (getMedia().get(i).getWidth() != getMedia().get(i).getHeight()) {
                            isAllSquare = false;
                            break;
                        }
                    }
                    if (isAllSquare) {
                        switch (getMedia().size()) {
                            case 1:
                                return ViewType.Timeline_FreeStyle_1;
                            case 2:
                                return ViewType.Timeline_SquareAll_2;
                            case 3:
                                return ViewType.Timeline_SquareAll_3;
                            case 4:
                                return ViewType.Timeline_SquareAll_4;
                            default:
                                return ViewType.Timeline_SquareAll_5;
                        }
                    } else {
                        switch (getMedia().size()) {
                            case 1:
                                return ViewType.Timeline_FreeStyle_1;
                            case 2:
                                return ViewType.Timeline_SquareFirst_2;
                            case 3:
                                return ViewType.Timeline_SquareFirst_3;
                            case 4:
                                return ViewType.Timeline_SquareFirst_4;
                            default:
                                return ViewType.Timeline_SquareFirst_5;
                        }
                    }
                }
            }

        }
    }

    @Override
    public void startActivityForResult(IntentData requiredIntentData) {
        //no starting activity
        AbsBaseActivity activity = requiredIntentData.getActivity();
        TimelineDetailIntent intent = new TimelineDetailIntent(activity,
                this,
                requiredIntentData.getMediaClickPosition());

        requiredIntentData.getActivity().startActivityForResult(intent, new ResultCallback() {
            @Override
            public void onActivityResultSuccess(int resultCode, Intent data) {
                TimelineDetailIntentResult intentResult = new TimelineDetailIntentResult(data);
                requiredIntentData.onDataResultCallBack((Adaptive) intentResult.getLifeStream(), intentResult.isRemoveItem());
            }
        });

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeByte(this.mIsLike ? (byte) 1 : (byte) 0);
        dest.writeLong(this.mCreateDate != null ? this.mCreateDate.getTime() : -1);
        dest.writeTypedList(this.mUserView);
        dest.writeInt(this.mPublish);
        dest.writeValue(this.mLatitude);
        dest.writeString(this.mAddress);
        dest.writeValue(this.mLongtitude);
        dest.writeTypedList(this.mMedia);
        dest.writeString(this.mTitle);
        dest.writeParcelable(this.mStoreUser, flags);
        dest.writeString(this.mShareUrl);
        dest.writeString(this.mItemShape);
        dest.writeString(this.mCity);
        dest.writeString(this.mCountry);
        dest.writeParcelable(this.mContentStat, flags);
    }
}