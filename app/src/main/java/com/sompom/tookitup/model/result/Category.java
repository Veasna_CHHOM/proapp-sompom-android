package com.sompom.tookitup.model.result;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;


public class Category extends ErrorSupportModel implements Parcelable {

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel source) {
            return new Category(source);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
    //TODO: temporary id & _id are the same, cos in some api response _id, otherwise, id
    private static final String FIELD_ID = "id";
    private static final String FIELD_ID2 = "_id";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_COUNT = "count";
    @SerializedName(FIELD_ID2)
    private String mId;
    @SerializedName(FIELD_ID)
    private String mId2;
    @SerializedName(FIELD_NAME)
    private String mName;
    @SerializedName(FIELD_COUNT)
    private int mCount;
    private boolean mIsCheck;

    public Category() {

    }

    protected Category(Parcel in) {
        this.mId = in.readString();
        this.mId2 = in.readString();
        this.mName = in.readString();
        this.mCount = in.readInt();
        this.mIsCheck = in.readByte() != 0;
    }

    public static Category clone(Category category) {
        Category clone = new Category();
        clone.setId(category.getId());
        clone.setName(category.getName());
        clone.setCount(category.getCount());
        clone.setCheck(category.isCheck());
        return clone;
    }

    public String getId() {
        if (TextUtils.isEmpty(mId)) {
            mId = mId2;
        }
        return mId;
    }

    public void setId(String id) {
        mId = id;
        mId2 = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public int getCount() {
        return mCount;
    }

    public void setCount(int count) {
        mCount = count;
    }

    public boolean isCheck() {
        return mIsCheck;
    }

    public void setCheck(boolean check) {
        mIsCheck = check;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Category) {
            return ((Category) obj).getId() == getId();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return mId.hashCode();
    }

    @Override
    public String toString() {
        return "id = " + mId + ", name = " + mName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeString(this.mId2);
        dest.writeString(this.mName);
        dest.writeInt(this.mCount);
        dest.writeByte(this.mIsCheck ? (byte) 1 : (byte) 0);
    }
}