package com.sompom.tookitup.model.searchaddress.google;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Result {

    @SerializedName("address_components")
    private List<AddressComponent> mAddressComponents;
    @SerializedName("geometry")
    private Geometry mGeometry;
    @SerializedName("formatted_address")
    private String mFormatAddress;

    public List<AddressComponent> getAddressComponents() {
        return mAddressComponents;
    }

    public void setAddressComponents(List<AddressComponent> addressComponents) {
        mAddressComponents = addressComponents;
    }

    public Geometry getGeometry() {
        return mGeometry;
    }

    public void setGeometry(Geometry geometry) {
        mGeometry = geometry;
    }

    public String getFormatAddress() {
        return mFormatAddress;
    }
}
