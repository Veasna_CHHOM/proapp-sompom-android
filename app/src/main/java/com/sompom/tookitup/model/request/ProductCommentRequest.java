package com.sompom.tookitup.model.request;

/**
 * Created by he.rotha on 4/20/16.
 */
public class ProductCommentRequest {
    //CHECKSTYLE:OFF
    private String comment;
    private String item;
    private String user;
    //CHECKSTYLE:OFF

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
