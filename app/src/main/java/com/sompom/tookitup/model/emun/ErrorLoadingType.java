package com.sompom.tookitup.model.emun;

import com.sompom.tookitup.R;

/**
 * Created by nuonveyo on 6/5/18.
 */

public enum ErrorLoadingType {
    GENERAL_ERROR(R.drawable.ic_error_medium),
    NO_ITEM(R.drawable.ic_error_no_item);

    private int mIcon;

    ErrorLoadingType(int icon) {
        mIcon = icon;
    }

    public int getIcon() {
        return mIcon;
    }
}
