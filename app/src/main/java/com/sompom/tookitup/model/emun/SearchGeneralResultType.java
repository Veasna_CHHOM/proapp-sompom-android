package com.sompom.tookitup.model.emun;

import android.support.annotation.StringRes;

import com.sompom.tookitup.R;

/**
 * Created by nuonveyo on 7/2/18.
 */

public enum SearchGeneralResultType {
    ALL(0, R.string.message_all_title, "all"),
    //    ITEMS(1, R.string.search_general_items_title, "item"),
    PEOPLE(1, R.string.search_general_people_title, "people"),
    //    BUYING(3, R.string.message_buying_title, "buying"),
//    SELLING(4, R.string.message_selling_title, "selling"),
    CONVERSATION(2, R.string.search_general_conversation_title, "conversation");

    private final int mId;
    @StringRes
    private final int mTitle;
    private final String mFilter;

    SearchGeneralResultType(int id, int title, String filter) {
        mId = id;
        mTitle = title;
        mFilter = filter;
    }

    public static SearchGeneralResultType getType(int id) {
        for (SearchGeneralResultType searchGeneralResultType : SearchGeneralResultType.values()) {
            if (searchGeneralResultType.getId() == id) {
                return searchGeneralResultType;
            }
        }
        return SearchGeneralResultType.ALL;
    }

    public int getId() {
        return mId;
    }

    public int getTitle() {
        return mTitle;
    }

    public String getFilter() {
        return mFilter;
    }
}
