package com.sompom.tookitup.model.notification;

import com.google.gson.annotations.SerializedName;
import com.sompom.tookitup.model.WallStreetAdaptive;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class LifeStreamTemp implements RealmModel {

    private static final String FIELD_MEDIA = "media";
    private static final String FIELD_USER = "userstore";

    @PrimaryKey
    @SerializedName(WallStreetAdaptive.FIELD_ID)
    private String mId;
    @SerializedName(FIELD_MEDIA)
    private RealmList<MediaTemp> mMedia;
    @SerializedName(FIELD_USER)
    private UserTemp mStoreUser;

    public LifeStreamTemp() {
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public List<MediaTemp> getMedia() {
        return mMedia;
    }

    public UserTemp getStoreUser() {
        return mStoreUser;
    }

    public void setStoreUser(UserTemp storeUser) {
        mStoreUser = storeUser;
    }
}