package com.sompom.tookitup.model.notification;

import com.google.gson.annotations.SerializedName;
import com.sompom.tookitup.model.emun.MediaType;

import java.util.Objects;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class MediaTemp implements RealmModel {

    private static final String FIELD_ID = "_id";
    private static final String FIELD_THUMBNAIL = "thumbnail";
    private static final String FIELD_URL = "url";
    private static final String FIELD_TYPE = "type";
    private static final String FILED_INDEX = "index";
    @PrimaryKey
    @SerializedName(FIELD_ID)
    private String mId;
    @SerializedName(FIELD_URL)
    private String mUrl;
    @SerializedName(FIELD_TYPE)
    private String mType;
    @SerializedName(FILED_INDEX)
    private Integer mIndex;
    @SerializedName(FIELD_THUMBNAIL)
    private String mThumbnail;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public int getIndex() {
        if (mIndex == null) {
            return 0;
        }
        return mIndex;
    }

    public void setIndex(int index) {
        mIndex = index;
    }

    public String getThumbnail() {
        return mThumbnail;
    }

    public MediaType getType() {
        return MediaType.fromValue(mType);
    }

    public void setType(MediaType type) {
        mType = type.getValue();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof MediaTemp && Objects.equals(((MediaTemp) obj).getId(), mId);
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

}