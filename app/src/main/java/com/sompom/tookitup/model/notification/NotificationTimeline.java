package com.sompom.tookitup.model.notification;

import android.content.Context;
import android.content.Intent;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;

import com.desmond.squarecamera.utils.Keys;
import com.google.gson.annotations.SerializedName;
import com.sompom.tookitup.intent.newintent.SellerStoreIntent;
import com.sompom.tookitup.intent.newintent.TimelineDetailIntent;
import com.sompom.tookitup.model.emun.MediaType;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by He Rotha on 3/12/19.
 */
@RealmClass
public class NotificationTimeline implements NotificationAdaptive, RealmModel {
    @PrimaryKey
    @SerializedName(Keys.ID)
    private String mId;
    @SerializedName(Keys.ACTOR)
    private RealmList<UserTemp> mActor;
    @SerializedName(Keys.VERB)
    private String mVerb;
    @SerializedName(Keys.PUBLISH)
    private Date mPublished;
    @SerializedName(Keys.IS_READ)
    private boolean mIsRead = true;
    @SerializedName(Keys.OBJECT)
    private RealmList<LifeStreamTemp> mObject;

    public String getId() {
        return mId;
    }

    public RealmList<UserTemp> getActor() {
        return mActor;
    }

    public String getVerb() {
        return mVerb;
    }

    public Date getPublished() {
        return mPublished;
    }

    public boolean isRead() {
        return mIsRead;
    }

    public void setRead(boolean read) {
        mIsRead = read;
    }

    @Override
    public Intent getIntent(Context context) {
        if (TextUtils.equals(getVerb(), Keys.VERB_POST)) {
            return new SellerStoreIntent(context, mActor.get(0).getId());
        } else {
            return new TimelineDetailIntent(context, mObject.get(0).getId());
        }
    }

    @Override
    public CharSequence getObjectText(Context context) {
        if (TextUtils.equals(getVerb(), Keys.VERB_POST)) {
            if (mObject.size() <= 1) {
                return "a new post";
            } else {
                return String.format("%d new posts", mObject.size());
            }
        } else {
            String myUserId = SharedPrefUtils.getUserId(context);
            if (TextUtils.equals(myUserId, mObject.get(0).getStoreUser().getId())) {
                return "your post";
            } else {
                String user = mObject.get(0).getStoreUser().getFullName();
                String value = String.format("%s's post", user);
                Spannable spannable = new SpannableString(value);
                bold(context, spannable, value, user);
                return spannable;
            }
        }
    }

    @Override
    public String getThumbUrl() {
        if (mObject == null || mObject.isEmpty()) {
            return null;
        }

        LifeStreamTemp temp = mObject.get(0);
        if (temp == null) {
            return null;
        }

        if (temp.getMedia() == null || temp.getMedia().isEmpty()) {
            return null;
        }
        MediaTemp media = temp.getMedia().get(0);
        if (media == null) {
            return null;
        }
        if (media.getType() == MediaType.VIDEO) {
            return media.getThumbnail();
        } else {
            return media.getUrl();
        }
    }

}
