package com.sompom.tookitup.model.emun;

/**
 * Created by nuonveyo on 8/30/18.
 */

public enum MultiChatImageItemType {
    OneCol(1),
    TwoCol(2),
    ThreeCol(3);

    private int mCol;

    MultiChatImageItemType(int id) {
        mCol = id;
    }

    public static MultiChatImageItemType getMultiImageType(int totalItem) {
        if (totalItem == 1) {
            return OneCol;
        } else if (totalItem == 2 || totalItem == 4) {
            return TwoCol;
        } else {
            return ThreeCol;
        }
    }

    public int getCol() {
        return mCol;
    }

    public void setCol(int col) {
        mCol = col;
    }
}
