package com.sompom.tookitup.model;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.OnNotificationItemClickListener;
import com.sompom.tookitup.model.emun.HideNotificationOptionItem;
import com.sompom.tookitup.model.notification.NotificationAdaptive;
import com.sompom.tookitup.model.notification.NotificationHeader;
import com.sompom.tookitup.model.notification.UserTemp;
import com.sompom.tookitup.utils.DateTimeUtils;
import com.sompom.tookitup.viewmodel.AbsBaseViewModel;

import timber.log.Timber;

/**
 * Created by nuonveyo on 7/4/18.
 */

public class ListItemNotificationViewModel extends AbsBaseViewModel
        implements PopupMenu.OnMenuItemClickListener {
    public final ObservableBoolean mIsRead = new ObservableBoolean();
    public final ObservableBoolean mIsJustNow = new ObservableBoolean();
    public final ObservableField<String> mThumbUrl = new ObservableField<>();
    public final ObservableField<UserTemp> mUser = new ObservableField<>();

    private final Context mContext;
    private final int mPosition;
    private final OnNotificationItemClickListener mOnItemClickListener;

    private NotificationHeader mNotificationHeader;
    private NotificationAdaptive mNotificationModel;
    private boolean mIsEnableMargin;

    public ListItemNotificationViewModel(Context context,
                                         NotificationAdaptive baseNotificationModel,
                                         int position,
                                         OnNotificationItemClickListener onItemClickListener) {
        mContext = context;
        mPosition = position;
        mOnItemClickListener = onItemClickListener;
        if (baseNotificationModel instanceof NotificationHeader) {
            mNotificationHeader = (NotificationHeader) baseNotificationModel;
        } else {
            mNotificationModel = baseNotificationModel;
            mIsRead.set(mNotificationModel.isRead());

            mThumbUrl.set(mNotificationModel.getThumbUrl());
            mUser.set(mNotificationModel.getActor().get(0));
        }
    }

    public String getHeaderTitle() {
        return mContext.getString(mNotificationHeader.getNotificationItem().getTitle());
    }

    public void setEnableMargin(boolean enableMargin) {
        mIsEnableMargin = enableMargin;
    }

    public int getMarginTop() {
        if (mIsEnableMargin) {
            return mContext.getResources().getDimensionPixelSize(R.dimen.space_medium);
        } else {
            return 0;
        }
    }

    public String getDateTime() {
        String date = DateTimeUtils.parse(mContext,
                mNotificationModel.getPublished().getTime());
        if (TextUtils.equals(date, mContext.getString(R.string.date_just_now))) {
            mIsJustNow.set(true);
        }
        return date;
    }

    public void onOptionButtonClick(View view) {
        Context wrapper = new ContextThemeWrapper(mContext, R.style.PopupCustomStyle);
        PopupMenu popupMenu = new PopupMenu(wrapper, view);
        popupMenu.inflate(R.menu.menu_notification);
        Menu menu = popupMenu.getMenu();
        menu.add(0,
                HideNotificationOptionItem.HideNotification.getId(),
                0,
                HideNotificationOptionItem.HideNotification.getTitle());
        menu.add(0,
                HideNotificationOptionItem.HideAllNotification.getId(),
                1,
                mContext.getString(HideNotificationOptionItem.HideAllNotification.getTitle()));
        popupMenu.setOnMenuItemClickListener(this);
        popupMenu.show();
    }

    public CharSequence getNotificationModel() {
        return mNotificationModel.getText(mContext);
    }

    public void onItemClick() {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onItemClick(mNotificationModel, mPosition);
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        //TODO: call api after server done
        if (item.getItemId() == HideNotificationOptionItem.HideNotification.getId()) {
            Timber.e("Hide");
            return true;
        } else if (item.getItemId() == HideNotificationOptionItem.HideAllNotification.getId()) {
            Timber.e("Hide all");
            return true;
        }
        return false;
    }

}
