package com.sompom.tookitup.model.emun;

import com.sompom.tookitup.R;

/**
 * Created by He Rotha on 7/9/18.
 */
public enum ChatLinkBg {
    ChatMeTop(R.drawable.link_title_chat_me_top, R.drawable.link_chat_me_top),
    ChatMeMid(R.drawable.link_title_chat_me_mid, R.drawable.link_chat_me_mid),
    ChatMeBottom(R.drawable.link_title_chat_me_mid, R.drawable.link_chat_me_bottom),
    ChatMeSingle(R.drawable.link_title_chat_me_top, R.drawable.link_chat_single),

    ChatYouTop(R.drawable.link_title_chat_you_top, R.drawable.link_chat_you_top),
    ChatYouMid(R.drawable.link_title_chat_you_mid, R.drawable.link_chat_you_mid),
    ChatYouBottom(R.drawable.link_title_chat_you_mid, R.drawable.link_chat_you_bottom),
    ChatYouSingle(R.drawable.link_title_chat_you_top, R.drawable.link_chat_single);

    private int mDrawableTitle;
    private int mDrawableBody;

    ChatLinkBg(int drawableTitle, int drawableBody) {
        mDrawableTitle = drawableTitle;
        mDrawableBody = drawableBody;
    }

    public static ChatLinkBg getChatLinkBg(ChatBg chatBg) {
        if (chatBg == ChatBg.ChatMeTop) {
            return ChatMeTop;
        } else if (chatBg == ChatBg.ChatMeMid) {
            return ChatMeMid;
        } else if (chatBg == ChatBg.ChatMeBottom) {
            return ChatMeBottom;
        } else if (chatBg == ChatBg.ChatMeSingle) {
            return ChatMeSingle;
        } else if (chatBg == ChatBg.ChatYouTop) {
            return ChatYouTop;
        } else if (chatBg == ChatBg.ChatYouMid) {
            return ChatYouMid;
        } else if (chatBg == ChatBg.ChatYouBottom) {
            return ChatYouBottom;
        } else {
            return ChatYouSingle;
        }
    }

    public int getDrawableTitle() {
        return mDrawableTitle;
    }

    public int getDrawableBody() {
        return mDrawableBody;
    }
}
