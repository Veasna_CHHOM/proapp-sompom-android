package com.sompom.tookitup.model.request;

/**
 * Created by he.rotha on 6/3/16.
 */
public class RefreshTokenRequest {
    //CHECKSTYLE:OFF
    private String user_id;

    //CHECKSTYLE:OFF
    public RefreshTokenRequest(String userId) {
        this.user_id = userId;
    }
}
