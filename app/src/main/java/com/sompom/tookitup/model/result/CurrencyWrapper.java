package com.sompom.tookitup.model.result;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CurrencyWrapper extends ErrorSupportModel {

    @SerializedName("country_code")
    private String mCountryCode;
    @SerializedName("country_name")
    private String mCountryName;
    @SerializedName("createDate")
    private Long mCreateDate;
    @SerializedName("currencies")
    private List<Currency> mCurrencies;
    @SerializedName("_id")
    private String mId;

    public String getCountryCode() {
        return mCountryCode;
    }

    public void setCountryCode(String countryCode) {
        mCountryCode = countryCode;
    }

    public String getCountryName() {
        return mCountryName;
    }

    public void setCountryName(String countryName) {
        mCountryName = countryName;
    }

    public Long getCreateDate() {
        return mCreateDate;
    }

    public void setCreateDate(Long createDate) {
        mCreateDate = createDate;
    }

    public List<Currency> getCurrencies() {
        return mCurrencies;
    }

    public void setCurrencies(List<Currency> currencies) {
        mCurrencies = currencies;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

}
