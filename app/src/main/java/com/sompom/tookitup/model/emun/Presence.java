package com.sompom.tookitup.model.emun;

import android.text.TextUtils;

/**
 * Created by He Rotha on 12/31/18.
 */
public enum Presence {
    Online("online"), Offline("offline"), Unknown("unknown");

    private final String mValue;

    Presence(String value) {
        mValue = value;
    }

    public static Presence from(String value) {
        for (Presence presence : Presence.values()) {
            if (TextUtils.equals(presence.mValue, value)) {
                return presence;
            }
        }
        return Presence.Unknown;
    }

    public String getValue() {
        return mValue;
    }
}
