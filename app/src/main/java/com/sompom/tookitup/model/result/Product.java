package com.sompom.tookitup.model.result;

import android.content.Context;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.sompom.baseactivity.ResultCallback;
import com.sompom.tookitup.helper.IntentData;
import com.sompom.tookitup.intent.newintent.ProductDetailIntent;
import com.sompom.tookitup.intent.newintent.ProductDetailResultIntent;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.Follow;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.Search;
import com.sompom.tookitup.model.WallStreetAdaptive;
import com.sompom.tookitup.model.emun.PublishItem;
import com.sompom.tookitup.model.emun.Status;
import com.sompom.tookitup.model.emun.ViewType;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.widget.lifestream.Format;

import java.util.Date;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class Product implements WallStreetAdaptive, Adaptive, Search, Parcelable, RealmModel {

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel source) {
            return new Product(source);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    private static final String FIELD_ID = "_id";
    private static final String FIELD_STATUS = "status";
    private static final String FIELD_PHOTOS = "photos";
    private static final String FIELD_PRICE = "price";
    private static final String FIELD_USER = "seller";
    private static final String FIELD_CURRENCY = "currency";
    private static final String FIELD_RATE = "rate";
    private static final String FIELD_CATEGORIES = "categories";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_NUMBER_OF_REQUEST = "numberOfRequest";
    private static final String FIELD_IS_REQUEST_BUY = "isRequestBuy";
    private static final String FIELD_DESCRIPTION = "description";
    private static final String FIELD_FOLLOW = "follow";
    private static final String FIELD_MEDIA = "media";
    @PrimaryKey
    @SerializedName(FIELD_ID)
    private String mId;
    @SerializedName(FIELD_PRICE)
    private double mPrice;
    @SerializedName(FIELD_NAME)
    private String mName;
    @SerializedName(FIELD_MEDIA)
    private RealmList<Media> mProductMedia;
    @SerializedName(FIELD_CURRENCY)
    private String mCurrency;
    @SerializedName(FIELD_USER)
    private User mUser;
    @Ignore
    @SerializedName(FIELD_IS_LIKE)
    private Boolean mIsLike;
    @Ignore
    @SerializedName(FIELD_CREATE_DATE)
    private Date mCreateDate;
    @Ignore
    @SerializedName(FIELD_USER_VIEW)
    private List<User> mUserView;
    @Ignore
    @SerializedName(FIELD_PUBLISH)
    private int mPublish;
    @Ignore
    @SerializedName(FIELD_LATITUDE)
    private Double mLatitude;
    @Ignore
    @SerializedName(FIELD_ADDRESS)
    private String mAddress;
    @Ignore
    @SerializedName(FIELD_CITY)
    private String mCity;
    @Ignore
    @SerializedName(FIELD_LONGITUDE)
    private Double mLongitude;
    @Ignore
    @SerializedName(FIELD_IS_REQUEST_BUY)
    private Boolean mIsRequestBuy;
    @Ignore
    @SerializedName(FIELD_STATUS)
    private Integer mStatus;
    @Ignore
    @SerializedName(FIELD_PHOTOS)
    private List<String> mPhotos;
    @Ignore
    @SerializedName(FIELD_RATE)
    private Double mRate;
    @Ignore
    @SerializedName(FIELD_CATEGORIES)
    private List<Category> mCategories;
    @Ignore
    @SerializedName(FIELD_NUMBER_OF_REQUEST)
    private Integer mNumberOfRequest;
    @Ignore
    @SerializedName(FIELD_SHARE_URL)
    private String mShareUrl;
    @Ignore
    @SerializedName(FIELD_DESCRIPTION)
    private String mDescription;
    @Ignore
    @SerializedName(FIELD_FOLLOW)
    private com.sompom.tookitup.model.Follow mFollow;
    @Ignore
    private String mUploadId;
    @Ignore
    @SerializedName(FIELD_CONTENT_STAT)
    private ContentStat mContentStat;
    @Ignore
    @SerializedName(FIELD_COUNTRY)
    private String mCountry;

    public Product() {
    }

    protected Product(Parcel in) {
        this.mId = in.readString();
        this.mPrice = in.readDouble();
        this.mName = in.readString();
        mProductMedia = new RealmList<>();
        this.mProductMedia.addAll(in.createTypedArrayList(Media.CREATOR));
        this.mIsLike = (Boolean) in.readValue(Boolean.class.getClassLoader());
        long tmpMCreateDate = in.readLong();
        this.mCreateDate = tmpMCreateDate == -1 ? null : new Date(tmpMCreateDate);
        this.mUserView = in.createTypedArrayList(User.CREATOR);
        this.mPublish = in.readInt();
        this.mLatitude = (Double) in.readValue(Double.class.getClassLoader());
        this.mAddress = in.readString();
        this.mCity = in.readString();
        this.mLongitude = (Double) in.readValue(Double.class.getClassLoader());
        this.mIsRequestBuy = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mStatus = (Integer) in.readValue(Integer.class.getClassLoader());
        this.mPhotos = in.createStringArrayList();
        this.mUser = in.readParcelable(User.class.getClassLoader());
        this.mCurrency = in.readString();
        this.mRate = (Double) in.readValue(Double.class.getClassLoader());
        this.mCategories = in.createTypedArrayList(Category.CREATOR);
        this.mNumberOfRequest = (Integer) in.readValue(Integer.class.getClassLoader());
        this.mShareUrl = in.readString();
        this.mDescription = in.readString();
        this.mFollow = in.readParcelable(Follow.class.getClassLoader());
        this.mUploadId = in.readString();
        this.mContentStat = in.readParcelable(ContentStat.class.getClassLoader());
    }

    @Override
    public String getCountry() {
        return mCountry;
    }

    @Override
    public void setCountry(String country) {
        mCountry = country;
    }

    @Override
    public String getId() {
        if (TextUtils.isEmpty(mId)) {
            return "";
        }
        return mId;
    }

    @Override
    public void setId(String id) {
        mId = id;
    }

    @Override
    public boolean isLike() {
        if (mIsLike == null) {
            return false;
        }
        return mIsLike;
    }

    @Override
    public void setLike(boolean like) {
        mIsLike = like;
    }

    @Override
    public Date getCreateDate() {
        return mCreateDate;
    }

    @Override
    public void setCreateDate(Date createDate) {
        mCreateDate = createDate;
    }

    @Override
    public List<User> getUserView() {
        return mUserView;
    }

    @Override
    public void setUserView(List<User> userView) {
        mUserView = userView;
    }

    @Override
    public PublishItem getPublish() {
        return PublishItem.getItem(mPublish);
    }

    @Override
    public void setPublish(int publish) {
        mPublish = publish;
    }

    @Override
    public ContentStat getContentStat() {
        if (mContentStat == null) {
            mContentStat = new ContentStat();
        }
        return mContentStat;
    }

    @Override
    public void setContentStat(ContentStat contentStat) {
        mContentStat = contentStat;
    }

    @Override
    public double getLatitude() {
        if (mLatitude == null) {
            return 0;
        }
        return mLatitude;
    }

    @Override
    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }

    @Override
    public String getAddress() {
        return mAddress;
    }

    @Override
    public void setAddress(String address) {
        mAddress = address;
    }

    @Override
    public double getLongitude() {
        if (mLongitude == null) {
            return 0;
        }
        return mLongitude;
    }

    @Override
    public void setLongitude(double longitude) {
        mLongitude = longitude;
    }

    @Override
    public List<Media> getMedia() {
        return mProductMedia;
    }

    public boolean isRequestBuy() {
        if (mIsRequestBuy == null) {
            return false;
        }
        return mIsRequestBuy;
    }

    public void setRequestBuy(boolean requestBuy) {
        mIsRequestBuy = requestBuy;
    }

    public Status getStatus() {
        return Status.fromValue(mStatus);
    }

    public void setStatus(int status) {
        mStatus = status;
    }

    @Override
    public String getCity() {
        return mCity;
    }

    @Override
    public void setCity(String city) {
        mCity = city;
    }

    public double getPrice() {
        return mPrice;
    }

    public void setPrice(double price) {
        mPrice = price;
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser = user;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String currency) {
        mCurrency = currency;
    }

    public double getRate() {
        return mRate;
    }

    public void setRate(double rate) {
        mRate = rate;
    }

    public List<Category> getCategories() {
        return mCategories;
    }

    public void setCategories(List<Category> categories) {
        mCategories = categories;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public int getNumberOfRequest() {
        return mNumberOfRequest;
    }

    public void setNumberOfRequest(int numberOfRequest) {
        mNumberOfRequest = numberOfRequest;
    }

    public String getUploadId() {
        if (TextUtils.isEmpty(mUploadId)) {
            return "";
        }
        return mUploadId;
    }

    public void setUploadId(String uploadId) {
        mUploadId = uploadId;
    }

    @Override
    public String getShareUrl() {
        return mShareUrl;
    }

    public void setShareUrl(final String shareUrl) {
        mShareUrl = shareUrl;
    }

    public String getShareUrl(Context context, boolean isPost) {
        String language = SharedPrefUtils.getProperLanguage(context);
        if (isPost) {
            return mShareUrl + "?typeshare=post&lang=" + language;
        } else {
            return mShareUrl + "?typeshare=link&lang=" + language;
        }
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public void setProductMedia(List<Media> productMedia) {
        if (mProductMedia == null) {
            mProductMedia = new RealmList<>();
        }
        mProductMedia.addAll(productMedia);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof Product) {
            if (!TextUtils.isEmpty(((Product) obj).getId())) {
                return ((Product) obj).getId().equals(getId());
            } else {
                return ((Product) obj).getUploadId().equals(getUploadId());
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    public Follow getFollow() {
        if (mFollow == null) {
            mFollow = new Follow();
            mFollow.setFollow(false);
        }
        return mFollow;
    }

    public void setFollow(Follow follow) {
        mFollow = follow;
    }

    @Override
    public ViewType getTimelineViewType() {
//    WallStreet Style 2
//        if (getMedia().size() == 1) {
//            return ViewType.ProductThreeOneItem;
//        } else if (getMedia().size() == 2) {
//            return ViewType.ProductThreeTwoItem;
//        } else {
//            return ViewType.ProductThreeThreeItem;
//        }

//        WallStreet Style 1
        int firstWidth = getMedia().get(0).getWidth();
        int firstHeight = getMedia().get(0).getHeight();
        if (firstWidth != firstHeight) {
            switch (getMedia().size()) {
                case 1:
                    return ViewType.Product_FreeStyle_1;
                case 2:
                    return firstWidth > firstHeight ?
                            ViewType.Product_FreeStyle_2_Vertical :
                            ViewType.Product_FreeStyle_2_Horizontal;
                case 3:
                    return firstWidth > firstHeight ?
                            ViewType.Product_FreeStyle_3_Vertical :
                            ViewType.Product_FreeStyle_3_Horizontal;
                case 4:
                    return firstWidth > firstHeight ?
                            ViewType.Product_FreeStyle_4_Vertical :
                            ViewType.Product_FreeStyle_4_Horizontal;
                default:
                    return firstWidth > firstHeight ?
                            ViewType.Product_FreeStyle_5_Vertical :
                            ViewType.Product_FreeStyle_5_Horizontal;
            }
        } else {
            boolean isAllSquare = true;
            for (int i = 1; i < getMedia().size(); i++) {
                if (i > Format.SquareAll.getMaxDisplay() - 1) {
                    break;
                }
                if (getMedia().get(i).getWidth() != getMedia().get(i).getHeight()) {
                    isAllSquare = false;
                    break;
                }
            }
            if (isAllSquare) {
                switch (getMedia().size()) {
                    case 1:
                        return ViewType.Product_FreeStyle_1;
                    case 2:
                        return ViewType.Product_SquareAll_2;
                    case 3:
                        return ViewType.Product_SquareAll_3;
                    case 4:
                        return ViewType.Product_SquareAll_4;
                    default:
                        return ViewType.Product_SquareAll_5;
                }
            } else {
                switch (getMedia().size()) {
                    case 1:
                        return ViewType.Product_FreeStyle_1;
                    case 2:
                        return ViewType.Product_SquareFirst_2;
                    case 3:
                        return ViewType.Product_SquareFirst_3;
                    case 4:
                        return ViewType.Product_SquareFirst_4;
                    default:
                        return ViewType.Product_SquareFirst_5;
                }
            }
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeDouble(this.mPrice);
        dest.writeString(this.mName);
        dest.writeTypedList(this.mProductMedia);
        dest.writeValue(this.mIsLike);
        dest.writeLong(this.mCreateDate != null ? this.mCreateDate.getTime() : -1);
        dest.writeTypedList(this.mUserView);
        dest.writeInt(this.mPublish);
        dest.writeValue(this.mLatitude);
        dest.writeString(this.mAddress);
        dest.writeString(this.mCity);
        dest.writeValue(this.mLongitude);
        dest.writeValue(this.mIsRequestBuy);
        dest.writeValue(this.mStatus);
        dest.writeStringList(this.mPhotos);
        dest.writeParcelable(this.mUser, flags);
        dest.writeString(this.mCurrency);
        dest.writeValue(this.mRate);
        dest.writeTypedList(this.mCategories);
        dest.writeValue(this.mNumberOfRequest);
        dest.writeString(this.mShareUrl);
        dest.writeString(this.mDescription);
        dest.writeParcelable(this.mFollow, flags);
        dest.writeString(this.mUploadId);
        dest.writeParcelable(this.mContentStat, flags);
    }

    @Override
    public void startActivityForResult(IntentData requiredIntentData) {
        //no starting activity
        AbsBaseActivity activity = requiredIntentData.getActivity();
        if (activity == null) {
            return;
        }
        ProductDetailIntent intent = new ProductDetailIntent(activity, this, false, false);
        activity.startActivityForResult(intent, new ResultCallback() {
            @Override
            public void onActivityResultSuccess(int resultCode, Intent data) {
                ProductDetailResultIntent resultIntent = new ProductDetailResultIntent(data);
                Product productResult = resultIntent.getProduct();
                requiredIntentData.onDataResultCallBack(productResult, false);
            }
        });

    }
}