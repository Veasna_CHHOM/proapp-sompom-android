package com.sompom.tookitup.model.request;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.sompom.tookitup.database.CurrencyDb;
import com.sompom.tookitup.model.emun.ChooseDistanceItem;
import com.sompom.tookitup.model.emun.FilterSortByItem;
import com.sompom.tookitup.model.result.Category;
import com.sompom.tookitup.model.result.Currency;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by he.rotha on 3/24/17.
 */

public class QueryProduct {

    @SerializedName("lowPrice")
    private double mMinPrice = 0;
    @SerializedName("topPrice")
    private double mMaxPrice = 0;
    @SerializedName("distance")
    private String mDistanceOption;
    @SerializedName("categories")
    private String[] mCategoriesId;
    @SerializedName("sort")
    private int mSort;
    @SerializedName("currency")
    private String mCurrency;
    @SerializedName("lat")
    private double mLatitude;
    @SerializedName("long")
    private double mLongitude;

    //fields below doesn't need to use in API, but need to use for local saving
    private int mSlideMin = 0;
    private int mSlideMax = 0;
    private List<Category> mCategoriesObj;

    //CHECKSTYLE:OFF
    public QueryProduct(Context context) {
        mSort = FilterSortByItem.Nearest.getId();
        mDistanceOption = context.getString(ChooseDistanceItem.getValueFromId(ChooseDistanceItem.WALK.getId()).getType());
    }

    public double getMinPrice() {
        return mMinPrice;
    }

    public void setMinPrice(double minPrice) {
        this.mMinPrice = minPrice;
    }

    public double getMaxPrice() {
        return mMaxPrice;
    }

    public void setMaxPrice(double maxPrice) {
        this.mMaxPrice = maxPrice;
    }

    public String getDistanceOption() {
        return mDistanceOption;
    }

    public void setDistanceOption(String distanceOption) {
        this.mDistanceOption = distanceOption;
    }

    public List<Category> getCategories() {
        return mCategoriesObj;
    }

    public void setCategories(List<Category> categoryId) {
        this.mCategoriesObj = categoryId;
        if (mCategoriesObj != null) {
            mCategoriesId = new String[mCategoriesObj.size()];
            for (int i = 0; i < mCategoriesObj.size(); i++) {
                mCategoriesId[i] = mCategoriesObj.get(i).getId();
            }
        }
    }

    public void setCategory(Category category) {
        mCategoriesObj = new ArrayList<>();
        mCategoriesObj.add(category);
        mCategoriesId = new String[]{category.getId()};
    }

    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }

    public void setLongitude(double longitude) {
        mLongitude = longitude;
    }

    public int getSort() {
        return mSort;
    }

    public void setSort(int sort) {
        this.mSort = sort;
    }

    public int getSlideMin() {
        return mSlideMin;
    }

    public void setSlideMin(int slideMin) {
        mSlideMin = slideMin;
    }

    public int getSlideMax() {
        return mSlideMax;
    }

    public void setSlideMax(int slideMax) {
        mSlideMax = slideMax;
    }

    public String getCurrency(Context context) {
        initCurrency(context);
        return mCurrency;
    }

    /**
     * init currency to make sure it doesn't null
     *
     * @param context
     */
    public void initCurrency(Context context) {
        if (TextUtils.isEmpty(mCurrency)) {
            Currency selected = CurrencyDb.getSelectedCurrency(context);
            if (selected == null) {
                mCurrency = Currency.DEFAULT_CURRENCY;
            } else {
                mCurrency = selected.getName();
            }
        }
    }

    public void setCurrency(String currency) {
        this.mCurrency = currency;
    }

    public void reset() {
        mMinPrice = 0;
        mMaxPrice = 0;
        mDistanceOption = null;
        mCategoriesObj = null;
        mSort = 0;
        mSlideMin = 0;
        mSlideMax = 0;
        mCurrency = null;
    }
}
