package com.sompom.tookitup.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ChhomVeasna on 10/31/17.
 */

public class ForgotPasswordBody {

    @SerializedName("email")
    private String mEmail;

    public ForgotPasswordBody(String email) {
        mEmail = email;
    }
}
