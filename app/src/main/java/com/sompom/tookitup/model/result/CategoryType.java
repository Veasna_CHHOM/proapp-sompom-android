package com.sompom.tookitup.model.result;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by he.rotha on 7/14/16.
 */
public class CategoryType implements Parcelable {

    //CHECKSTYLE:OFF
    private int total_count;
    private List<Category> category;
    //CHECKSTYLE:OFF

    public CategoryType() {
    }

    protected CategoryType(Parcel in) {
        this.total_count = in.readInt();
        this.category = in.createTypedArrayList(Category.CREATOR);
    }

    public List<Category> getCategory() {
        return category;
    }

    public void setCategory(List<Category> category) {
        this.category = category;
    }

    public int getTotalCount() {
        return total_count;
    }

    public void setTotalCount(int totalCount) {
        this.total_count = totalCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.total_count);
        dest.writeTypedList(this.category);
    }

}
