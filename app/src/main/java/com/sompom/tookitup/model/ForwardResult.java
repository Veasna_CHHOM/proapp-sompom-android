package com.sompom.tookitup.model;

import com.google.gson.annotations.SerializedName;
import com.sompom.tookitup.model.result.User;

import java.util.List;

public class ForwardResult {
    @SerializedName("suggested")
    private List<User> mSuggested;

    @SerializedName("people")
    private List<User> mPeople;

    @SerializedName("result")
    private List<User> mResult;

    public List<User> getSuggested() {
        return mSuggested;
    }

    public List<User> getPeople() {
        return mPeople;
    }

    public List<User> getResult() {
        return mResult;
    }
}
