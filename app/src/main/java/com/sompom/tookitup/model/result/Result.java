package com.sompom.tookitup.model.result;

import com.google.gson.annotations.SerializedName;


/**
 * Created by imac on 6/9/16.
 */
public class Result<T> extends ErrorSupportModel {
    private static final String FIELD_DATA = "data";

    @SerializedName(FIELD_DATA)
    private T mData;

    public Result() {

    }

    public T getData() {
        return mData;
    }

    public void setData(T data) {
        mData = data;
    }

}
