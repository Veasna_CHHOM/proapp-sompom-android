package com.sompom.tookitup.model.notification;

import android.content.Context;
import android.content.Intent;
import android.text.Spannable;

import com.sompom.tookitup.model.BaseNotificationModel;
import com.sompom.tookitup.model.emun.NotificationItem;

import java.util.Date;

import io.realm.RealmList;

/**
 * Created by nuonveyo on 7/4/18.
 */

public class NotificationHeader implements BaseNotificationModel, NotificationAdaptive {
    private NotificationItem mNotificationItem;

    public NotificationItem getNotificationItem() {
        return mNotificationItem;
    }

    public void setNotificationItem(NotificationItem notificationItem) {
        mNotificationItem = notificationItem;
    }

    @Override
    public Spannable getText(Context context) {
        return null;
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public RealmList<UserTemp> getActor() {
        return null;
    }

    @Override
    public String getVerb() {
        return null;
    }

    @Override
    public Date getPublished() {
        return null;
    }

    @Override
    public boolean isRead() {
        return false;
    }

    @Override
    public void setRead(boolean read) {

    }

    @Override
    public Intent getIntent(Context context) {
        return null;
    }

    @Override
    public CharSequence getObjectText(Context context) {
        return null;
    }

    @Override
    public String getThumbUrl() {
        return null;
    }
}
