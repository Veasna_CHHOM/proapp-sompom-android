package com.sompom.tookitup.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nuonveyo
 * on 3/11/19.
 */
public class CommentBody {
    @SerializedName("message")
    private String mMessage;
    @SerializedName("subCommentId")
    private String mSubCommentId;
    @SerializedName("lastCommentId")
    private String mLastCommentId;

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getSubCommentId() {
        return mSubCommentId;
    }

    public void setSubCommentId(String subCommentId) {
        mSubCommentId = subCommentId;
    }

    public String getLastCommentId() {
        return mLastCommentId;
    }

    public void setLastCommentId(String lastCommentId) {
        mLastCommentId = lastCommentId;
    }
}
