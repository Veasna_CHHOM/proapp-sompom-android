package com.sompom.tookitup.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nuonveyo
 * on 2/28/19.
 */
public class FollowBody {
    @SerializedName("id")
    private String mId;

    public FollowBody(String id) {
        mId = id;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }
}
