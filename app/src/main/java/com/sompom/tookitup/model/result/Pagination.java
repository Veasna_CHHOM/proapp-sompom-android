package com.sompom.tookitup.model.result;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Pagination implements Parcelable {

    public static final Creator<Pagination> CREATOR = new Creator<Pagination>() {
        public Pagination createFromParcel(Parcel in) {
            return new Pagination(in);
        }

        public Pagination[] newArray(int size) {
            return new Pagination[size];
        }
    };
    private static final String FIELD_NEXT_PAGE_URL = "nextPageUrl";
    private static final String FIELD_PULL_TO_REFRESH_URL = "pullToRefreshUrl";
    @SerializedName(FIELD_NEXT_PAGE_URL)
    private String mNextPageUrl;
    @SerializedName(FIELD_PULL_TO_REFRESH_URL)
    private String mPullToRefreshUrl;

    public Pagination() {

    }

    public Pagination(Parcel in) {
        mNextPageUrl = in.readString();
        mPullToRefreshUrl = in.readString();
    }

    public static <T extends Parcelable> boolean canLoadMore(ResultList<T> value, int requestCount) {
        if (value == null) {
            return false;
        } else {
            if (value.getData().size() < requestCount) {
                return false;
            } else if (value.getPagination() == null) {
                return false;
            } else {
                return !TextUtils.isEmpty(value.getPagination().getNextPageUrl());
            }
        }
    }

    public static boolean isHasLoadMore(Pagination pagination) {
        return pagination != null && !TextUtils.isEmpty(pagination.getNextPageUrl());
    }

    public static boolean isHasLoadMore(List<?> listOfData, int loadMoreSize, Pagination pagination) {
        return listOfData != null && listOfData.size() >= loadMoreSize && isHasLoadMore(pagination);
    }

    public static boolean isHasLoadMore(ResultList<?> datas, int loadMoreSize) {
        return isHasLoadMore(datas.getData(), loadMoreSize, datas.getPagination());
    }

    public String getNextPageUrl() {
        return mNextPageUrl;
    }

    public void setNextPageUrl(String nextPageUrl) {
        mNextPageUrl = nextPageUrl;
    }

    public String getPullToRefreshUrl() {
        return mPullToRefreshUrl;
    }

    public void setPullToRefreshUrl(String pullToRefreshUrl) {
        mPullToRefreshUrl = pullToRefreshUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mNextPageUrl);
        dest.writeString(mPullToRefreshUrl);
    }

    @Override
    public String toString() {
        return "nextPageUrl = " + mNextPageUrl + ", pullToRefreshUrl = " + mPullToRefreshUrl;
    }
}