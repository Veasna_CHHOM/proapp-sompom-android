package com.sompom.tookitup.model;

import android.support.annotation.Nullable;
import android.support.v4.util.Pair;

import com.google.android.gms.ads.formats.NativeCustomTemplateAd;

/**
 * Created by He Rotha on 3/23/18.
 */
public class PairAds extends Pair<AdsItem, NativeCustomTemplateAd> {

    /**
     * Constructor for a Pair.
     *
     * @param first  the first object in the Pair
     * @param second the second object in the pair
     */
    public PairAds(@Nullable AdsItem first, @Nullable NativeCustomTemplateAd second) {
        super(first, second);
    }
}
