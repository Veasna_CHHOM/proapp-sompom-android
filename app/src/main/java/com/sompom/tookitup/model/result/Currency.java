package com.sompom.tookitup.model.result;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Currency extends RealmObject {
    public static final String DEFAULT_CURRENCY = "USD";
    @PrimaryKey
    @SerializedName("createDate")
    private String mName;
    private boolean mIsSelected;

    public Currency() {
    }

    public Currency(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public void setSelected(boolean selected) {
        mIsSelected = selected;
    }
}
