package com.sompom.tookitup.model;

import android.os.Parcel;
import android.text.TextUtils;

import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.google.gson.annotations.SerializedName;
import com.sompom.tookitup.helper.IntentData;
import com.sompom.tookitup.model.emun.PublishItem;
import com.sompom.tookitup.model.emun.ViewType;
import com.sompom.tookitup.model.result.ContentStat;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.utils.OpenWebsiteUtil;

import java.util.Date;
import java.util.List;

/**
 * Created by He Rotha on 11/1/18.
 */
public class WallStreetAds implements Adaptive, WallStreetAdaptive {

    public static final Creator<WallStreetAds> CREATOR = new Creator<WallStreetAds>() {
        @Override
        public WallStreetAds createFromParcel(Parcel source) {
            return new WallStreetAds(source);
        }

        @Override
        public WallStreetAds[] newArray(int size) {
            return new WallStreetAds[size];
        }
    };
    private static final String FIELD_MEDIA = "media";
    private static final String FIELD_TITLE = "text";
    @SerializedName(FIELD_ID)
    private String mId;
    @SerializedName(FIELD_IS_LIKE)
    private boolean mIsLike;
    @SerializedName(FIELD_USER_VIEW)
    private List<User> mUserView;
    @SerializedName(FIELD_PUBLISH)
    private int mPublish;
    @SerializedName(FIELD_MEDIA)
    private List<Media> mMedia;
    @SerializedName(FIELD_TITLE)
    private String mTitle;
    @SerializedName(FIELD_SHARE_URL)
    private String mShareUrl;
    @SerializedName(FIELD_CONTENT_STAT)
    private ContentStat mContentStat;
    private String mSubTitle;
    private String mCreativeIconUrl;
    private String mCreativeBannerUrl;
    private String mRedicationUrl;
    private NativeCustomTemplateAd mNativeAd;


    public WallStreetAds() {
    }

    protected WallStreetAds(Parcel in) {
        this.mId = in.readString();
        this.mIsLike = in.readByte() != 0;
        this.mUserView = in.createTypedArrayList(User.CREATOR);
        this.mPublish = in.readInt();
        this.mMedia = in.createTypedArrayList(Media.CREATOR);
        this.mTitle = in.readString();
        this.mShareUrl = in.readString();
        this.mContentStat = in.readParcelable(ContentStat.class.getClassLoader());
        this.mSubTitle = in.readString();
        this.mCreativeIconUrl = in.readString();
        this.mCreativeBannerUrl = in.readString();
        this.mRedicationUrl = in.readString();
    }

    @Override
    public String getId() {
        return mId;
    }

    @Override
    public void setId(String id) {
        mId = id;
    }

    @Override
    public boolean isLike() {
        return mIsLike;
    }

    @Override
    public void setLike(boolean like) {
        mIsLike = like;
    }

    @Override
    public Date getCreateDate() {
        return null;
    }

    @Override
    public void setCreateDate(Date createDate) {
    }

    @Override
    public List<User> getUserView() {
        return mUserView;
    }

    @Override
    public void setUserView(List<User> userView) {
        mUserView = userView;
    }

    @Override
    public PublishItem getPublish() {
        return PublishItem.getItem(mPublish);
    }

    @Override
    public void setPublish(int publish) {
        mPublish = publish;
    }

    public NativeCustomTemplateAd getNativeAd() {
        return mNativeAd;
    }

    public void setNativeAd(NativeCustomTemplateAd nativeAd) {
        mNativeAd = nativeAd;
    }

    public String getCreativeIconUrl() {
        return mCreativeIconUrl;
    }

    public void setCreativeIconUrl(String creativeIconUrl) {
        mCreativeIconUrl = creativeIconUrl;
    }

    public String getCreativeBannerUrl() {
        return mCreativeBannerUrl;
    }

    public void setCreativeBannerUrl(String creativeBannerUrl) {
        mCreativeBannerUrl = creativeBannerUrl;
    }

    public String getRedicationUrl() {
        return mRedicationUrl;
    }

    public void setRedicationUrl(String redicationUrl) {
        mRedicationUrl = redicationUrl;
    }

    @Override
    public ContentStat getContentStat() {
        if (mContentStat == null) {
            mContentStat = new ContentStat();
        }
        return mContentStat;
    }

    @Override
    public void setContentStat(ContentStat contentStat) {
        mContentStat = contentStat;
    }

    @Override
    public double getLatitude() {
        return 0;
    }

    @Override
    public void setLatitude(double latitude) {
    }

    @Override
    public String getAddress() {
        return null;
    }

    @Override
    public void setAddress(String address) {
    }

    @Override
    public String getCity() {
        return null;
    }

    @Override
    public void setCity(String city) {
    }

    @Override
    public double getLongitude() {
        return 0;
    }

    @Override
    public void setLongitude(double longitude) {
    }

    @Override
    public User getUser() {
        return null;
    }

    public List<Media> getMedia() {
        return mMedia;
    }

    public void setMedia(List<Media> media) {
        mMedia = media;
    }

    @Override
    public String getDescription() {
        return getTitle();
    }

    @Override
    public String getShareUrl() {
        return mShareUrl;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getSubTitle() {
        return mSubTitle;
    }

    public void setSubTitle(String subTitle) {
        mSubTitle = subTitle;
    }

    public String getCountry() {
        return null;
    }

    public void setCountry(String country) {
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof WallStreetAds && (obj == this || ((WallStreetAds) obj).getId().equals(getId()));
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public ViewType getTimelineViewType() {
        if (getMedia() == null || getMedia().isEmpty()) {
            return ViewType.Timeline;
        } else {
            return ViewType.FreeStyle;
//            if (getMedia().size() == 1 && getMedia().get(0).getType() == MediaType.LIVE_VIDEO) {
//                return ViewType.LiveVideoTimeline;
//            } else {
//                int firstWidth = getMedia().get(0).getWidth();
//                int firstHeight = getMedia().get(0).getHeight();
//                if (firstWidth != firstHeight) {
//                    switch (getMedia().size()) {
//                        case 1:
//                            return ViewType.Timeline_FreeStyle_1;
//                        case 2:
//                            return firstWidth > firstHeight ?
//                                    ViewType.Timeline_FreeStyle_2_Vertical :
//                                    ViewType.Timeline_FreeStyle_2_Horizontal;
//                        case 3:
//                            return firstWidth > firstHeight ?
//                                    ViewType.Timeline_FreeStyle_3_Vertical :
//                                    ViewType.Timeline_FreeStyle_3_Horizontal;
//                        case 4:
//                            return firstWidth > firstHeight ?
//                                    ViewType.Timeline_FreeStyle_4_Vertical :
//                                    ViewType.Timeline_FreeStyle_4_Horizontal;
//                        default:
//                            return firstWidth > firstHeight ?
//                                    ViewType.Timeline_FreeStyle_5_Vertical :
//                                    ViewType.Timeline_FreeStyle_5_Horizontal;
//                    }
//                } else {
//                    boolean isAllSquare = true;
//                    for (int i = 1; i < getMedia().size(); i++) {
//                        if (i > Format.SquareAll.getMaxDisplay() - 1) {
//                            break;
//                        }
//                        if (getMedia().get(i).getWidth() != getMedia().get(i).getHeight()) {
//                            isAllSquare = false;
//                            break;
//                        }
//                    }
//                    if (isAllSquare) {
//                        switch (getMedia().size()) {
//                            case 1:
//                                return ViewType.Timeline_FreeStyle_1;
//                            case 2:
//                                return ViewType.Timeline_SquareAll_2;
//                            case 3:
//                                return ViewType.Timeline_SquareAll_3;
//                            case 4:
//                                return ViewType.Timeline_SquareAll_4;
//                            default:
//                                return ViewType.Timeline_SquareAll_5;
//                        }
//                    } else {
//                        switch (getMedia().size()) {
//                            case 1:
//                                return ViewType.Timeline_FreeStyle_1;
//                            case 2:
//                                return ViewType.Timeline_SquareFirst_2;
//                            case 3:
//                                return ViewType.Timeline_SquareFirst_3;
//                            case 4:
//                                return ViewType.Timeline_SquareFirst_4;
//                            default:
//                                return ViewType.Timeline_SquareFirst_5;
//                        }
//                    }
//                }
//            }

        }
    }

    @Override
    public void startActivityForResult(IntentData requiredIntentData) {
        //no starting activity
        if (mNativeAd != null && !TextUtils.isEmpty(mRedicationUrl)) {
            mNativeAd.performClick(mRedicationUrl);
            OpenWebsiteUtil.openWebsite(requiredIntentData.getActivity(), mRedicationUrl);
        }

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeByte(this.mIsLike ? (byte) 1 : (byte) 0);
        dest.writeTypedList(this.mUserView);
        dest.writeInt(this.mPublish);
        dest.writeTypedList(this.mMedia);
        dest.writeString(this.mTitle);
        dest.writeString(this.mShareUrl);
        dest.writeParcelable(this.mContentStat, flags);
        dest.writeString(this.mSubTitle);
        dest.writeString(this.mCreativeIconUrl);
        dest.writeString(this.mCreativeBannerUrl);
        dest.writeString(this.mRedicationUrl);
    }
}
