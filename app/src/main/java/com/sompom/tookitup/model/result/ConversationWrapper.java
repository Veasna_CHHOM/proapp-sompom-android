package com.sompom.tookitup.model.result;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by nuonveyo
 * on 1/7/19.
 */

public class ConversationWrapper {
    @SerializedName("unreadMessages")
    private List<Conversation> mUnreadConversationList;
    @SerializedName("conversations")
    private LoadMoreWrapper<Conversation> mConversation;

    public List<Conversation> getUnreadConversationList() {
        return mUnreadConversationList;
    }

    public LoadMoreWrapper<Conversation> getConversation() {
        return mConversation;
    }
}
