package com.sompom.tookitup.model.searchaddress.mapbox;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MapBoxData {

    @SerializedName("attribution")
    private String mAttribution;
    @SerializedName("features")
    private List<Feature> mFeatures;
    @SerializedName("query")
    private List<String> mQuery;
    @SerializedName("type")
    private String mType;

    public String getAttribution() {
        return mAttribution;
    }

    public void setAttribution(String attribution) {
        mAttribution = attribution;
    }

    public List<Feature> getFeatures() {
        return mFeatures;
    }

    public void setFeatures(List<Feature> features) {
        mFeatures = features;
    }

    public List<String> getQuery() {
        return mQuery;
    }

    public void setQuery(List<String> query) {
        mQuery = query;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

}
