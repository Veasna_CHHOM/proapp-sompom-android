package com.sompom.tookitup.model.emun;

public enum Range {
    RANGE_1(10, 100D), //100
    RANGE_2(20, 500D), //500
    RANGE_3(30, 1_000D), //1k
    RANGE_4(40, 5_000D), //5k
    RANGE_5(50, 10_000D), //10k
    RANGE_6(60, 50_00D), //50k
    RANGE_7(70, 100_000D), //100k
    RANGE_8(80, 500_000D), //500k
    RANGE_9(90, 1_000_000D), //1m
    RANGE_10(100, 10_000_000D), //10m
    ;
    private int mPercent;
    private double mValue;

    Range(int percent, double value) {
        mPercent = percent;
        mValue = value;
    }

    public int getPercent() {
        return mPercent;
    }

    public double getValue() {
        return mValue;
    }
}
