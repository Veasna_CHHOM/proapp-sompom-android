package com.sompom.tookitup.model.result;

/**
 * Created by he.rotha on 7/14/16.
 */
public class CategoryTypeWrapper {
    //CHECKSTYLE:OFF
    private CategoryType walk;
    private CategoryType bike;
    private CategoryType motor;
    private CategoryType car;
    private CategoryType truck;
    private CategoryType worldwide;
    //CHECKSTYLE:OFF

    public CategoryTypeWrapper() {
    }

    public CategoryType getWalk() {
        return walk;
    }

    public void setWalk(CategoryType walk) {
        this.walk = walk;
    }

    public CategoryType getBike() {
        return bike;
    }

    public void setBike(CategoryType bike) {
        this.bike = bike;
    }

    public CategoryType getMotor() {
        return motor;
    }

    public void setMotor(CategoryType motor) {
        this.motor = motor;
    }

    public CategoryType getCar() {
        return car;
    }

    public void setCar(CategoryType car) {
        this.car = car;
    }

    public CategoryType getTruck() {
        return truck;
    }

    public void setTruck(CategoryType truck) {
        this.truck = truck;
    }

    public CategoryType getWorldwide() {
        return worldwide;
    }

    public void setWorldwide(CategoryType worldwide) {
        this.worldwide = worldwide;
    }
}
