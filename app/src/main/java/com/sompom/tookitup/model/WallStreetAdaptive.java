package com.sompom.tookitup.model;

import android.os.Parcelable;
import android.text.TextUtils;

import com.sompom.tookitup.model.emun.PublishItem;
import com.sompom.tookitup.model.result.ContentStat;
import com.sompom.tookitup.model.result.User;

import java.util.Date;
import java.util.List;

/**
 * Created by He Rotha on 10/31/18.
 */
public interface WallStreetAdaptive extends Parcelable {
    String FIELD_ID = "_id";
    String FIELD_PUBLISH = "visibility";
    String FIELD_CREATE_DATE = "createdAt";
    String FIELD_IS_LIKE = "isLike";
    String FIELD_USER_VIEW = "userView";
    String FIELD_LONGITUDE = "longitude";
    String FIELD_LATITUDE = "latitude";
    String FIELD_ADDRESS = "address";
    String FIELD_CITY = "city";
    String FIELD_COUNTRY = "country";
    String FIELD_SHARE_URL = "shareUrl";
    String FIELD_CONTENT_STAT = "contentStat";

    String getId();

    void setId(String id);

    boolean isLike();

    void setLike(boolean like);

    Date getCreateDate();

    void setCreateDate(Date createDate);

    List<User> getUserView();

    void setUserView(List<User> userView);

    PublishItem getPublish();

    void setPublish(int publish);

    double getLatitude();

    void setLatitude(double latitude);

    String getAddress();

    void setAddress(String address);

    String getCity();

    void setCity(String city);

    String getCountry();

    void setCountry(String country);

    default String getDisplayAddress() {
        if (!TextUtils.isEmpty(getCity()) && !TextUtils.isEmpty(getCountry())) {
            return getCity() + ", " + getCountry();
        } else if (TextUtils.isEmpty(getCity())) {
            return getCountry();
        } else {
            return getCity();
        }
    }

    double getLongitude();

    void setLongitude(double longitude);

    User getUser();

    List<Media> getMedia();

    String getDescription();

    String getShareUrl();

    ContentStat getContentStat();

    void setContentStat(ContentStat contentStat);
}
