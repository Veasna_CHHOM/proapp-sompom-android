package com.sompom.tookitup.model.result;

import com.google.gson.annotations.SerializedName;
import com.sompom.tookitup.model.ActiveUser;

/**
 * Created by nuonveyo
 * on 2/13/19.
 */

public class SearchGeneralUserWrapper {
    @SerializedName("nearBy")
    private ActiveUser mNearbyUsers;
    @SerializedName("suggested")
    private ActiveUser mSuggestedUsers;
    @SerializedName("lastSearch")
    private ActiveUser mLastSearchUsers;

    public ActiveUser getNearbyUsers() {
        return mNearbyUsers;
    }

    public void setNearbyUsers(ActiveUser nearbyUsers) {
        mNearbyUsers = nearbyUsers;
    }

    public ActiveUser getSuggestedUsers() {
        return mSuggestedUsers;
    }

    public void setSuggestedUsers(ActiveUser suggestedUsers) {
        mSuggestedUsers = suggestedUsers;
    }

    public ActiveUser getLastSearchUsers() {
        return mLastSearchUsers;
    }

    public void setLastSearchUsers(ActiveUser lastSearchUsers) {
        mLastSearchUsers = lastSearchUsers;
    }
}
