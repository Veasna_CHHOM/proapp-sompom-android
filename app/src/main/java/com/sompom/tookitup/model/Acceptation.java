package com.sompom.tookitup.model;

/**
 * Created by imac on 6/14/17.
 */

public class Acceptation {
    //CHECKSTYLE:OFF
    private String id;
    private boolean acceptation;

    //CHECKSTYLE:OFF
    public Acceptation(boolean acceptation) {
        this.acceptation = acceptation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isAcceptation() {
        return acceptation;
    }

    public void setAcceptation(boolean acceptation) {
        this.acceptation = acceptation;
    }
}
