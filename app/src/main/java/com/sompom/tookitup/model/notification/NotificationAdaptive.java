package com.sompom.tookitup.model.notification;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;

import com.desmond.squarecamera.utils.Keys;
import com.sompom.tookitup.R;
import com.sompom.tookitup.utils.CustomTypefaceSpan;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.Date;
import java.util.List;

import io.realm.RealmList;

/**
 * Created by He Rotha on 3/12/19.
 */
public interface NotificationAdaptive {

    static String[] getHighLightName(List<UserTemp> userTemps) {
        if (userTemps.size() > 3) {
            String[] name = new String[3];

            for (int i = 0; i < 3; i++) {
                if (i == 2) {
                    name[i] = String.format("%d others", userTemps.size() - 2);
                } else {
                    name[i] = userTemps.get(i).getFullName();
                }
            }
            return name;
        } else {
            String[] name = new String[userTemps.size()];
            for (int i = 0; i < userTemps.size(); i++) {
                name[i] = userTemps.get(i).getFullName();
            }
            return name;
        }
    }

    String getId();

    RealmList<UserTemp> getActor();

    String getVerb();

    Date getPublished();

    boolean isRead();

    void setRead(boolean read);

    Intent getIntent(Context context);

    CharSequence getObjectText(Context context);

    String getThumbUrl();

    default void bold(Context context,
                      Spannable span,
                      String fullText,
                      String boldText) {

        int startIndex = fullText.indexOf(boldText);
        while (startIndex != -1) {
            int endIndex = startIndex + boldText.length();
            Typeface typeface = ResourcesCompat.getFont(context, R.font.sf_pro_semibold);
            span.setSpan(new CustomTypefaceSpan("", typeface),
                    startIndex,
                    endIndex,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            span.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorPrimary)),
                    startIndex,
                    endIndex,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            startIndex = fullText.indexOf(boldText, startIndex + boldText.length());
        }
    }

    default Spannable getText(Context context) {
        String verb = getVerbText();
        String value = String.format(verb, getSubjectText(context, getActor()), getObjectText(context));
        Spannable spannable = new SpannableString(value);
        String[] list = getHighLightName(getActor());
        for (String s : list) {
            bold(context, spannable, value, s);
        }
        if (this instanceof NotificationUser) {
            list = getHighLightName(((NotificationUser) this).getObject());
            for (String s : list) {
                bold(context, spannable, value, s);
            }
        }
        return spannable;
    }

    default String getSubjectText(Context context, List<UserTemp> users) {
        String myUserId = SharedPrefUtils.getUserId(context);
        if (users.size() == 1) {
            return getUserName(myUserId, users.get(0));
        } else if (users.size() == 2) {
            String user1 = getUserName(myUserId, users.get(0));
            String user2 = getUserName(myUserId, users.get(1));
            return String.format("%s and %s", user1, user2);
        } else if (users.size() == 3) {
            String user1 = getUserName(myUserId, users.get(0));
            String user2 = getUserName(myUserId, users.get(1));
            String user3 = getUserName(myUserId, users.get(2));
            return String.format("%s, %s and %s", user1, user2, user3);
        } else {
            String user1 = getUserName(myUserId, users.get(0));
            String user2 = getUserName(myUserId, users.get(1));
            String other = String.format("%d others", users.size() - 2);
            return String.format("%s, %s and %s", user1, user2, other);
        }
    }

    default String getVerbText() {
        if (TextUtils.equals(getVerb(), Keys.VERB_COMMENT)) {
            return "%s commented on %s";
        } else if (TextUtils.equals(getVerb(), Keys.VERB_LIKE)) {
            return "%s liked %s";
        } else if (TextUtils.equals(getVerb(), Keys.VERB_SHARE)) {
            return "%s shared %s";
        } else if (TextUtils.equals(getVerb(), Keys.VERB_FOLLOW)) {
            return "%s started following %s";
        } else if (TextUtils.equals(getVerb(), Keys.VERB_POST)) {
            return "%s created %s";
        } else if (TextUtils.equals(getVerb(), Keys.VERB_SELL)) {
            return "%s added %s";
        } else if (TextUtils.equals(getVerb(), Keys.VERB_DISCOUNT)) {
            return "%s discounted %s";
        }
        return "";
    }

    default String getUserName(String myUserId, UserTemp user) {
        if (TextUtils.equals(myUserId, user.getId())) {
            return "You";
        } else {
            return user.getFullName();
        }
    }

}
