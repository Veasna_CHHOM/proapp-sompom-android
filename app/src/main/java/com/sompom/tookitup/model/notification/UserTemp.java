package com.sompom.tookitup.model.notification;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class UserTemp implements RealmModel {

    //CHECKSTYLE:OFF
    private static final String FIELD_ID = "_id";
    private static final String FIELD_FIRST_NAME = "firstName";
    private static final String FIELD_LAST_NAME = "lastName";
    private static final String FIELD_USER_PROFILE = "profileUrl";
    private static final String FIELD_STORE_NAME = "storeName";

    @PrimaryKey
    @SerializedName(FIELD_ID)
    private String mId;
    @SerializedName(FIELD_USER_PROFILE)
    private String mUserProfile;
    @SerializedName(FIELD_LAST_NAME)
    private String mLastName;
    @SerializedName(FIELD_FIRST_NAME)
    private String mFirstName;
    @SerializedName(FIELD_STORE_NAME)
    private String mStoreName;

    public UserTemp() {
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getUserProfile() {
        return mUserProfile;
    }

    public void setUserProfile(String userProfile) {
        mUserProfile = userProfile;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getStoreName() {
        return mStoreName;
    }

    public void setStoreName(String storeName) {
        mStoreName = storeName;
    }

    public String getFullName() {
        return mFirstName + " " + mLastName;
    }

}