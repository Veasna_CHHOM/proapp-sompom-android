package com.sompom.tookitup.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.sompom.tookitup.model.result.Chat;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by He Rotha on 12/28/18.
 */
@RealmClass
public class ChatJunk implements Parcelable, RealmModel {
    public static final Parcelable.Creator<ChatJunk> CREATOR = new Parcelable.Creator<ChatJunk>() {
        @Override
        public ChatJunk createFromParcel(Parcel source) {
            return new ChatJunk(source);
        }

        @Override
        public ChatJunk[] newArray(int size) {
            return new ChatJunk[size];
        }
    };
    @PrimaryKey
    private String mConversationId;
    private String mMessageId;
    private String mSendTo;
    private String mSendingType;
    private boolean mIsGroup;

    public ChatJunk() {
    }

    private ChatJunk(Parcel in) {
        this.mConversationId = in.readString();
        this.mMessageId = in.readString();
        this.mSendTo = in.readString();
        this.mSendingType = in.readString();
        this.mIsGroup = in.readByte() != 0;
    }

    public String getConversationId() {
        return mConversationId;
    }

    public void setConversationId(String conversationId) {
        mConversationId = conversationId;
    }

    public String getMessageId() {
        return mMessageId;
    }

    public void setMessageId(String messageId) {
        mMessageId = messageId;
    }

    public String getSendTo() {
        return mSendTo;
    }

    public void setSendTo(String sendTo) {
        mSendTo = sendTo;
    }

    public Chat.SendingType getSendingType() {
        return Chat.SendingType.from(mSendingType);
    }

    public void setSendingType(Chat.SendingType sendingType) {
        mSendingType = sendingType.getValue();
    }

    public boolean isGroup() {
        return mIsGroup;
    }

    public void setGroup(boolean group) {
        mIsGroup = group;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mConversationId);
        dest.writeString(this.mMessageId);
        dest.writeString(this.mSendTo);
        dest.writeString(this.mSendingType);
        dest.writeByte(this.mIsGroup ? (byte) 1 : (byte) 0);
    }
}
