package com.sompom.tookitup.model.request;

/**
 * Created by he.rotha on 4/20/16.
 */
public class LifeStreamCommentRequest {
    //CHECKSTYLE:OFF
    private String comment;
    private String lifeStream;
    private String user;
    //CHECKSTYLE:OFF

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getItem() {
        return lifeStream;
    }

    public void setItem(String lifeStream) {
        this.lifeStream = lifeStream;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
