package com.sompom.tookitup.model.request;

/**
 * Created by he.rotha on 4/28/16.
 */
public class ShareRequest {
    //CHECKSTYLE:OFF
    private String item;
    private String user;

    //CHECKSTYLE:OFF
    public ShareRequest(String user, String item) {
        this.user = user;
        this.item = item;
    }
}
