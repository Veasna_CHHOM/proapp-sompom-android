package com.sompom.tookitup.model.emun;

import com.sompom.tookitup.R;

/**
 * Created by He Rotha on 7/9/18.
 */
public enum ChatBg {
    ChatMeTop(R.drawable.layout_chat_me_top, R.drawable.layout_chat_me_top_selected),
    ChatMeMid(R.drawable.layout_chat_me_middle, R.drawable.layout_chat_me_middle_selected),
    ChatMeBottom(R.drawable.layout_chat_me_bottom, R.drawable.layout_chat_me_bottom_selected),
    ChatMeSingle(R.drawable.layout_chat_me_single, R.drawable.layout_chat_me_single_selected),
    ChatYouTop(R.drawable.layout_chat_you_top, R.drawable.layout_chat_you_top_selected),
    ChatYouMid(R.drawable.layout_chat_you_middle, R.drawable.layout_chat_you_middle_selected),
    ChatYouBottom(R.drawable.layout_chat_you_bottom, R.drawable.layout_chat_you_bottom_selected),
    ChatYouSingle(R.drawable.layout_chat_you_single, R.drawable.layout_chat_you_single_selected);

    private int mDrawable;
    private int mSelectDrawable;

    ChatBg(int drawable, int selectDrawable) {
        mDrawable = drawable;
        mSelectDrawable = selectDrawable;
    }

    public int getDrawable(boolean isSelect) {
        if (isSelect) {
            return mSelectDrawable;
        } else {
            return mDrawable;
        }
    }


}
