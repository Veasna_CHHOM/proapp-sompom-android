package com.sompom.tookitup.model.emun;

import android.support.annotation.StringRes;

import com.sompom.tookitup.R;

/**
 * Created by nuonveyo on 6/26/18.
 */

public enum SegmentedControlItem {
    All(1, R.string.message_all_title, "all"),
    Message(2, R.string.message_title, "private"),
    Buying(3, R.string.message_group_title, "group"),
    //    Buying(3, R.string.message_buying_title, "buyer"); => Change to Group as above
    Selling(4, R.string.message_selling_title, "seller"); // Hide this feature for now

    private int mId;
    @StringRes
    private int mTitle;
    private String mValue;

    SegmentedControlItem(int id, int title, String value) {
        mId = id;
        mTitle = title;
        mValue = value;
    }

    public static SegmentedControlItem getSegmentedControlItem(int id) {
        for (SegmentedControlItem segmentedControlItem : SegmentedControlItem.values()) {
            if (segmentedControlItem.getId() == id) {
                return segmentedControlItem;
            }
        }
        return SegmentedControlItem.All;
    }

    public int getId() {
        return mId;
    }

    public int getTitle() {
        return mTitle;
    }

    public String getValue() {
        return mValue;
    }

    public static SegmentedControlItem[] getVisibleItems() {
        return new SegmentedControlItem[]{SegmentedControlItem.All,
                SegmentedControlItem.Message,
                SegmentedControlItem.Buying};
    }
}
