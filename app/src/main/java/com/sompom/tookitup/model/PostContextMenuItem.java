package com.sompom.tookitup.model;

import android.support.annotation.StringRes;

/**
 * Created by nuonveyo on 10/31/18.
 */

public class PostContextMenuItem {
    private int mId;
    @StringRes
    private int mIcon;
    @StringRes
    private int mTitle;

    public PostContextMenuItem(int id, int icon, int title) {
        mId = id;
        mIcon = icon;
        mTitle = title;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getIcon() {
        return mIcon;
    }

    public void setIcon(int icon) {
        mIcon = icon;
    }

    public int getTitle() {
        return mTitle;
    }

    public void setTitle(int title) {
        mTitle = title;
    }
}
