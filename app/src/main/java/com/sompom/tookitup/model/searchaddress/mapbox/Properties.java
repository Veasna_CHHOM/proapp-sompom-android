package com.sompom.tookitup.model.searchaddress.mapbox;

import com.google.gson.annotations.SerializedName;

public class Properties {

    @SerializedName("short_code")
    private String mShortCode;
    @SerializedName("wikidata")
    private String mWikidata;

    public String getShortCode() {
        return mShortCode;
    }

    public void setShortCode(String shortCode) {
        mShortCode = shortCode;
    }

    public String getWikidata() {
        return mWikidata;
    }

    public void setWikidata(String wikidata) {
        mWikidata = wikidata;
    }

}
