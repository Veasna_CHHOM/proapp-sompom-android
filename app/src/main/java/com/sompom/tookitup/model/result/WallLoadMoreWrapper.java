package com.sompom.tookitup.model.result;

import com.google.gson.annotations.SerializedName;

/**
 * Created by herotha
 * on 2/20/19.
 */

public class WallLoadMoreWrapper<T> extends LoadMoreWrapper<T> {
    @SerializedName("nextProduct")
    private String mNextProduct;

    public String getNextProduct() {
        return mNextProduct;
    }

}
