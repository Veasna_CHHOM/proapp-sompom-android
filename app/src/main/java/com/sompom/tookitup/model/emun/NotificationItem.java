package com.sompom.tookitup.model.emun;

import android.support.annotation.StringRes;

import com.sompom.tookitup.R;

/**
 * Created by nuonveyo on 7/4/18.
 */

public enum NotificationItem {
    New(1, R.string.notification_new_section),
    Earlier(2, R.string.notification_earlier_section);

    private int mId;
    @StringRes
    private int mTitle;

    NotificationItem(int id, int title) {
        mId = id;
        mTitle = title;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getTitle() {
        return mTitle;
    }

    public void setTitle(int title) {
        mTitle = title;
    }
}
