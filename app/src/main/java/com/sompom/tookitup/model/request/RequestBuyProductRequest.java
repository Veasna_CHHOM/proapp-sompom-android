package com.sompom.tookitup.model.request;

/**
 * Use when user request to buy a product from other user.
 * Created by imac on 4/22/16.
 */
public class RequestBuyProductRequest {
    /*
    {
    "buyer": "aeee9a88-5eb5-49d0-8e2b-fd6e04e355a9",
    "saler": "0550062d-2432-4bab-b547-56e61b1b7648",
    "item": "09e0b2e8-a1b8-4799-a4a9-1192fa60cf86"
    }
     */
    //CHECKSTYLE:OFF
    private String buyer;
    private String saler;
    private String item;

    //CHECKSTYLE:OFF
    public RequestBuyProductRequest(String buyerId, String salerId, String itemId) {
        buyer = buyerId;
        saler = salerId;
        item = itemId;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(final String buyer) {
        this.buyer = buyer;
    }

    public String getSaler() {
        return saler;
    }

    public void setSaler(final String saler) {
        this.saler = saler;
    }

    public String getItem() {
        return item;
    }

    public void setItem(final String item) {
        this.item = item;
    }
}
