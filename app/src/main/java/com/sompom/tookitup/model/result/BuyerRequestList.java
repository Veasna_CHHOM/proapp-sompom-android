package com.sompom.tookitup.model.result;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by he.rotha on 4/5/16.
 */
public class BuyerRequestList extends ErrorSupportModel implements Parcelable {
    public static final Parcelable.Creator<BuyerRequestList> CREATOR = new Parcelable.Creator<BuyerRequestList>() {
        public BuyerRequestList createFromParcel(Parcel in) {
            return new BuyerRequestList(in);
        }

        public BuyerRequestList[] newArray(int size) {
            return new BuyerRequestList[size];
        }
    };
    private static final String FIELD_PRODUCT = "product";
    private static final String FIELD_BUYERS = "buyers";
    @SerializedName(FIELD_PRODUCT)
    private Product mProduct;
    @SerializedName(FIELD_BUYERS)
    private List<User> mBuyers;

    public BuyerRequestList() {

    }

    public BuyerRequestList(Parcel in) {
        mProduct = in.readParcelable(Product.class.getClassLoader());
        mBuyers = new ArrayList<User>();
        in.readTypedList(mBuyers, User.CREATOR);
    }

    public Product getProduct() {
        return mProduct;
    }

    public void setProduct(Product product) {
        mProduct = product;
    }

    public List<User> getBuyers() {
        return mBuyers;
    }

    public void setBuyers(List<User> buyers) {
        mBuyers = buyers;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mProduct, flags);
        dest.writeTypedList(mBuyers);
    }

    @Override
    public String toString() {
        return "product = " + mProduct + ", buyers = " + mBuyers;
    }

}
