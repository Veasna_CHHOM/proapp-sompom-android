package com.sompom.tookitup.model.result;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by nuonveyo on 10/26/18.
 */

public class LinkPreviewModel implements Parcelable {
    public static final Parcelable.Creator<LinkPreviewModel> CREATOR = new Parcelable.Creator<LinkPreviewModel>() {
        @Override
        public LinkPreviewModel createFromParcel(Parcel source) {
            return new LinkPreviewModel(source);
        }

        @Override
        public LinkPreviewModel[] newArray(int size) {
            return new LinkPreviewModel[size];
        }
    };
    private String mLogo;
    private String mTitle;
    private String mLink;
    private String mDescription;
    private boolean mIsLinkDownloaded;

    public LinkPreviewModel() {
    }

    protected LinkPreviewModel(Parcel in) {
        this.mLogo = in.readString();
        this.mDescription = in.readString();
        this.mLink = in.readString();
        this.mTitle = in.readString();
        this.mIsLinkDownloaded = in.readByte() != 0;
    }

    public String getLogo() {
        return mLogo;
    }

    public void setLogo(String logo) {
        mLogo = logo;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getLink() {
        return mLink;
    }

    public void setLink(String link) {
        mLink = link;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public boolean isLinkDownloaded() {
        return mIsLinkDownloaded;
    }

    public void setLinkDownloaded(boolean linkDownloaded) {
        mIsLinkDownloaded = linkDownloaded;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mLogo);
        dest.writeString(this.mDescription);
        dest.writeString(this.mLink);
        dest.writeString(this.mTitle);
        dest.writeByte(this.mIsLinkDownloaded ? (byte) 1 : 0);
    }
}
