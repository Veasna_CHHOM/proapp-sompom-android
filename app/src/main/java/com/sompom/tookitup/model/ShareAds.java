package com.sompom.tookitup.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.sompom.tookitup.helper.IntentData;
import com.sompom.tookitup.model.emun.PublishItem;
import com.sompom.tookitup.model.emun.ViewType;
import com.sompom.tookitup.model.result.ContentStat;
import com.sompom.tookitup.model.result.User;

import java.util.Date;
import java.util.List;

/**
 * Created by He Rotha on 11/1/18.
 */
public class ShareAds implements WallStreetAdaptive, Adaptive, Parcelable {
    public static final Creator<ShareAds> CREATOR = new Creator<ShareAds>() {
        @Override
        public ShareAds createFromParcel(Parcel source) {
            return new ShareAds(source);
        }

        @Override
        public ShareAds[] newArray(int size) {
            return new ShareAds[size];
        }
    };
    @SerializedName(FIELD_ID)
    private String mId;
    @SerializedName(FIELD_IS_LIKE)
    private boolean mIsLike;
    @SerializedName(FIELD_CREATE_DATE)
    private Date mCreateDate;
    @SerializedName(FIELD_USER_VIEW)
    private List<User> mUserView;
    @SerializedName(FIELD_PUBLISH)
    private int mPublish;
    @SerializedName(FIELD_LATITUDE)
    private double mLatitude;
    @SerializedName(FIELD_ADDRESS)
    private String mAddress;
    @SerializedName(FIELD_LONGITUDE)
    private double mLongitude;
    @SerializedName("userShared")
    private User mUser;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("timeline")
    private WallStreetAds mLifeStream;
    @SerializedName(FIELD_CITY)
    private String mCity;
    @SerializedName(FIELD_CONTENT_STAT)
    private ContentStat mContentStat;
    @SerializedName(FIELD_COUNTRY)
    private String mCountry;

    public ShareAds() {
    }

    private ShareAds(Parcel in) {
        this.mId = in.readString();
        this.mIsLike = in.readByte() != 0;
        long tmpMCreateDate = in.readLong();
        this.mCreateDate = tmpMCreateDate == -1 ? null : new Date(tmpMCreateDate);
        this.mUserView = in.createTypedArrayList(User.CREATOR);
        this.mPublish = in.readInt();
        this.mLatitude = in.readDouble();
        this.mAddress = in.readString();
        this.mLongitude = in.readDouble();
        this.mUser = in.readParcelable(User.class.getClassLoader());
        this.mDescription = in.readString();
        this.mLifeStream = in.readParcelable(WallStreetAds.class.getClassLoader());
        this.mCity = in.readString();
        this.mContentStat = in.readParcelable(ContentStat.class.getClassLoader());
    }

    @Override
    public String getId() {
        return mId;
    }

    @Override
    public void setId(String id) {
        mId = id;
    }

    @Override
    public boolean isLike() {
        return mIsLike;
    }

    @Override
    public void setLike(boolean like) {
        mIsLike = like;
    }

    @Override
    public Date getCreateDate() {
        return mCreateDate;
    }

    @Override
    public void setCreateDate(Date createDate) {
        mCreateDate = createDate;
    }

    @Override
    public List<User> getUserView() {
        return mUserView;
    }

    @Override
    public void setUserView(List<User> userView) {
        mUserView = userView;
    }

    @Override
    public PublishItem getPublish() {
        return PublishItem.getItem(mPublish);
    }

    @Override
    public void setPublish(int publish) {
        mPublish = publish;
    }

    @Override
    public ContentStat getContentStat() {
        if (mContentStat == null) {
            mContentStat = new ContentStat();
        }
        return mContentStat;
    }

    @Override
    public void setContentStat(ContentStat contentStat) {
        mContentStat = contentStat;
    }

    @Override
    public double getLatitude() {
        return mLatitude;
    }

    @Override
    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }

    @Override
    public String getAddress() {
        return mAddress;
    }

    @Override
    public void setAddress(String address) {
        mAddress = address;
    }

    @Override
    public String getCity() {
        return mCity;
    }

    @Override
    public void setCity(String city) {
        mCity = city;
    }

    @Override
    public String getCountry() {
        return mCountry;
    }

    @Override
    public void setCountry(String country) {
        mCountry = country;
    }

    @Override
    public double getLongitude() {
        return mLongitude;
    }

    @Override
    public void setLongitude(double longitude) {
        mLongitude = longitude;
    }

    @Override
    public String getShareUrl() {
        return null;
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser = user;
    }

    @Override
    public List<Media> getMedia() {
        return mLifeStream.getMedia();
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public WallStreetAds getLifeStream() {
        return mLifeStream;
    }

    public void setLifeStream(WallStreetAds lifeStream) {
        mLifeStream = lifeStream;
    }

    @Override
    public ViewType getTimelineViewType() {
        return getLifeStream().getTimelineViewType();
    }

    @Override
    public void startActivityForResult(IntentData requiredIntentData) {
        //no starting any activity
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeByte(this.mIsLike ? (byte) 1 : (byte) 0);
        dest.writeLong(this.mCreateDate != null ? this.mCreateDate.getTime() : -1);
        dest.writeTypedList(this.mUserView);
        dest.writeInt(this.mPublish);
        dest.writeDouble(this.mLatitude);
        dest.writeString(this.mAddress);
        dest.writeDouble(this.mLongitude);
        dest.writeParcelable(this.mUser, flags);
        dest.writeString(this.mDescription);
        dest.writeParcelable(this.mLifeStream, flags);
        dest.writeString(this.mCity);
        dest.writeParcelable(this.mContentStat, flags);
    }
}
