package com.sompom.tookitup.model;

import io.realm.RealmModel;
import io.realm.annotations.RealmClass;

/**
 * Created by He Rotha on 12/6/18.
 */
@RealmClass
public class NotificationId implements RealmModel {
    private String mChannelId;
    private int mNotificationId;

    public NotificationId() {
    }

    public String getChannelId() {
        return mChannelId;
    }

    public void setChannelId(String channelId) {
        mChannelId = channelId;
    }

    public int getNotificationId() {
        return mNotificationId;
    }

    public void setNotificationId(int notificationId) {
        mNotificationId = notificationId;
    }

}

