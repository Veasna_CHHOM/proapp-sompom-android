package com.sompom.tookitup.model.result;

import com.google.gson.annotations.SerializedName;

public class AuthResponse {

    @SerializedName("jwt")
    private String mAccessToken;

    @SerializedName("user")
    private User mUser;

    public String getAccessToken() {
        return mAccessToken;
    }

    public User getUser() {
        return mUser;
    }
}
