package com.sompom.tookitup.model.emun;

/**
 * Created by He Rotha on 1/23/18.
 */

public enum PlaceholderItem {
    Home(0, "Home"),
    Other(1, "Other");

    private int mId;
    private String mName;

    PlaceholderItem(int id, String name) {
        mId = id;
        mName = name;
    }

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }
}
