package com.sompom.tookitup.model.result;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class ResultList<T> extends ErrorSupportModel {

    private static final String FIELD_DATA = "data";
    private static final String FIELD_PAGINATION = "pagination";


    @SerializedName(FIELD_DATA)
    private List<T> mData;
    @SerializedName(FIELD_PAGINATION)
    private Pagination mPagination;


    public ResultList() {

    }

    public List<T> getData() {
        return mData;
    }

    public void setData(List<T> data) {
        mData = data;
    }

    public Pagination getPagination() {
        return mPagination;
    }

    public void setPagination(Pagination pagination) {
        mPagination = pagination;
    }

    @Override
    public String toString() {
        return "data = " + mData + ", pagination = " + mPagination;
    }


}