package com.sompom.tookitup.model.result;

/**
 * Created by he.rotha on 8/25/16.
 */
public class ConversationExisted extends ErrorSupportModel {
    //CHECKSTYLE:OFF
    private String id;

    //CHECKSTYLE:OFF
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
