package com.sompom.tookitup.model;

import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.google.gson.annotations.SerializedName;
import com.sompom.tookitup.helper.IntentData;
import com.sompom.tookitup.model.emun.ItemType;
import com.sompom.tookitup.model.emun.ViewType;

/**
 * Created by He Rotha on 9/5/17.
 */

public class AdsItem implements Adaptive {
    public static final String FIELD_ITEM_TYPE = "item_type";
    private static final String FIELD_CLICK_URL = "click_url";
    private static final String FIELD_AD = "ad_type";
    private static final String FIELD_CREATIVE_MOBILE = "creative_mobile";
    private static final String FIELD_CREATIVE_TABLET = "creative_tablet";
    @SerializedName(FIELD_ITEM_TYPE)
    private String mItemAdsType;
    @SerializedName(FIELD_CLICK_URL)
    private String mClickUrl;
    @SerializedName(FIELD_AD)
    private String mAdsType;
    @SerializedName(FIELD_CREATIVE_MOBILE)
    private String mCreativeMobile;
    @SerializedName(FIELD_CREATIVE_TABLET)
    private String mCreativeTablet;
    private NativeCustomTemplateAd mAds;
    private int mIndex;

    @Override
    public String getId() {
        return null;
    }

    public ItemType getItemAdsType() {
        return ItemType.fromValue(mItemAdsType);
    }

    public void setItemAdsType(ItemType itemType) {
        mItemAdsType = itemType.getValue();
    }

    public void setItemAdsType(String itemAdsType) {
        mItemAdsType = itemAdsType;
    }

    public String getCreativeTablet() {
        return mCreativeTablet;
    }

    public void setCreativeTablet(String creativeTablet) {
        mCreativeTablet = creativeTablet;
    }

    public String getClickUrl() {
        return mClickUrl;
    }

    public void setClickUrl(String clickUrl) {
        mClickUrl = clickUrl;
    }

    public String getAdsType() {
        return mAdsType;
    }

    public void setAdsType(String adsType) {
        mAdsType = adsType;
    }

    public String getCreativeMobile() {
        return mCreativeMobile;
    }

    public void setCreativeMobile(String creativeMobile) {
        mCreativeMobile = creativeMobile;
    }

    public NativeCustomTemplateAd getAds() {
        return mAds;
    }

    public void setAds(NativeCustomTemplateAd ads) {
        mAds = ads;
    }

    public int getIndex() {
        return mIndex;
    }

    public void setIndex(int index) {
        mIndex = index;
    }

    @Override
    public ViewType getTimelineViewType() {
        return ViewType.Ads;
    }

    @Override
    public void startActivityForResult(IntentData requiredIntentData) {
        //no starting activity
    }
}
