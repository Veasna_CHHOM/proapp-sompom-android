package com.sompom.tookitup.model.emun;

import android.support.annotation.StringRes;

import com.sompom.tookitup.R;

/**
 * Created by nuonveyo on 7/20/18.
 */

public enum SuggestionTab {
    People(0, R.string.suggestion_title_1, R.string.search_general_people_title, "people"),
    Store(1, R.string.suggestion_title_2, R.string.suggestion_tab_2, "store"),
    Live(2, R.string.suggestion_title_3, R.string.home_button_live, "live");

    private int mId;
    @StringRes
    private int mTitle;
    @StringRes
    private int mTabTitle;

    private String mQueryType;

    SuggestionTab(int id, int title, int tabTitle, String queryType) {
        mId = id;
        mTitle = title;
        mTabTitle = tabTitle;
        mQueryType = queryType;
    }

    public static SuggestionTab getSuggestionTab(int id) {
        for (SuggestionTab suggestionTab : SuggestionTab.values()) {
            if (suggestionTab.getId() == id) {
                return suggestionTab;
            }
        }
        return People;
    }

    public int getId() {
        return mId;
    }

    public int getTitle() {
        return mTitle;
    }

    public int getTabTitle() {
        return mTabTitle;
    }

    public String getQueryType() {
        return mQueryType;
    }}
