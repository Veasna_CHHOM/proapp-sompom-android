package com.sompom.tookitup.model.result;

/**
 * Created by he.rotha on 4/28/16.
 */
public class ShareResult extends ErrorSupportModel {
    //CHECKSTYLE:OFF
    private int shares;
    private boolean isShare;

    //CHECKSTYLE:OFF
    public ShareResult() {
    }

    public int getShares() {
        return shares;
    }

    public void setShares(int shares) {
        this.shares = shares;
    }

    public boolean isShare() {
        return isShare;
    }

    public void setShare(boolean share) {
        isShare = share;
    }
}
