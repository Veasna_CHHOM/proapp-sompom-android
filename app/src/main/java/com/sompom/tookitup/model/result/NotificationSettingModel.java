package com.sompom.tookitup.model.result;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ChhomVeasna on 8/29/16.
 */
public class NotificationSettingModel implements Parcelable {
    public static final Creator<NotificationSettingModel> CREATOR = new Creator<NotificationSettingModel>() {
        @Override
        public NotificationSettingModel createFromParcel(Parcel source) {
            return new NotificationSettingModel(source);
        }

        @Override
        public NotificationSettingModel[] newArray(int size) {
            return new NotificationSettingModel[size];
        }
    };

    private static final String DISABLE_ALL_NOTIFICATION = "disableAllNotification";
    private static final String COMMENT_AFTER_ME_ON_PRODUCT = "commentAfterMeOnProduct";
    private static final String COMMENT_ON_PRODUCT_I_AM_SELLING = "commentOnProductIAmSelling";
    private static final String SOMEONE_REQUEST_TO_PURCHASE_MY_PRODUCT = "someoneRequestToPurchaseMyProduct";
    private static final String SELLER_I_FOLLOWED_POST_NEW_PRODUCT = "sellerIFollowedPostNewProduct";
    private static final String SELLER_I_FOLLOWED_UPDATE_PRODUCT = "sellerIFollowedUpdateProduct";

    @SerializedName(DISABLE_ALL_NOTIFICATION)
    private boolean mDisableAllNotification;
    @SerializedName(COMMENT_AFTER_ME_ON_PRODUCT)
    private boolean mCommentAfterMeOnProductEnabled;
    @SerializedName(COMMENT_ON_PRODUCT_I_AM_SELLING)
    private boolean mCommentOnProductIAmSellingEnabled;
    @SerializedName(SOMEONE_REQUEST_TO_PURCHASE_MY_PRODUCT)
    private boolean mSomeoneRequestToPurchaseMyProductEnabled;
    @SerializedName(SELLER_I_FOLLOWED_POST_NEW_PRODUCT)
    private boolean mSellerIFollowedPostNewProduct;
    @SerializedName(SELLER_I_FOLLOWED_UPDATE_PRODUCT)
    private boolean mSellerIFollowedUpdateProduct;

    public NotificationSettingModel() {
        mDisableAllNotification = true;
        mCommentAfterMeOnProductEnabled = true;
        mCommentOnProductIAmSellingEnabled = true;
        mSomeoneRequestToPurchaseMyProductEnabled = true;
        mSellerIFollowedPostNewProduct = true;
        mSellerIFollowedUpdateProduct = true;
    }

    protected NotificationSettingModel(Parcel in) {
        this.mDisableAllNotification = in.readByte() != 0;
        this.mCommentAfterMeOnProductEnabled = in.readByte() != 0;
        this.mCommentOnProductIAmSellingEnabled = in.readByte() != 0;
        this.mSomeoneRequestToPurchaseMyProductEnabled = in.readByte() != 0;
        this.mSellerIFollowedPostNewProduct = in.readByte() != 0;
        this.mSellerIFollowedUpdateProduct = in.readByte() != 0;
    }

    public boolean isDisableAllNotification() {
        return mDisableAllNotification;
    }

    public void setDisableAllNotification(boolean disableAllNotification) {
        mDisableAllNotification = disableAllNotification;
    }

    public boolean isCommentAfterMeOnProductEnabled() {
        return mCommentAfterMeOnProductEnabled;
    }

    public void setCommentAfterMeOnProductEnabled(boolean commentAfterMeOnProductEnabled) {
        mCommentAfterMeOnProductEnabled = commentAfterMeOnProductEnabled;
    }

    public boolean isCommentOnProductIAmSellingEnabled() {
        return mCommentOnProductIAmSellingEnabled;
    }

    public void setCommentOnProductIAmSellingEnabled(boolean commentOnProductIAmSellingEnabled) {
        mCommentOnProductIAmSellingEnabled = commentOnProductIAmSellingEnabled;
    }

    public boolean isSomeoneRequestToPurchaseMyProductEnabled() {
        return mSomeoneRequestToPurchaseMyProductEnabled;
    }

    public void setSomeoneRequestToPurchaseMyProductEnabled(boolean someoneRequestToPurchaseMyProductEnabled) {
        mSomeoneRequestToPurchaseMyProductEnabled = someoneRequestToPurchaseMyProductEnabled;
    }

    public boolean isSellerIFollowedPostNewProduct() {
        return mSellerIFollowedPostNewProduct;
    }

    public void setSellerIFollowedPostNewProduct(boolean sellerIFollowedPostNewProduct) {
        mSellerIFollowedPostNewProduct = sellerIFollowedPostNewProduct;
    }

    public boolean isSellerIFollowedUpdateProduct() {
        return mSellerIFollowedUpdateProduct;
    }

    public void setSellerIFollowedUpdateProduct(boolean sellerIFollowedUpdateProduct) {
        mSellerIFollowedUpdateProduct = sellerIFollowedUpdateProduct;
    }


    @Override
    public String toString() {
        return "mDisableAllNotification = " + mDisableAllNotification +
                ", mCommentAfterMeOnProductEnabled = " + mCommentAfterMeOnProductEnabled +
                ", mCommentOnProductIAmSellingEnabled = " + mCommentOnProductIAmSellingEnabled +
                ", mSomeoneRequestToPurchaseMyProductEnabled = " + mSomeoneRequestToPurchaseMyProductEnabled +
                ", mSellerIFollowedPostNewProduct = " + mSellerIFollowedPostNewProduct +
                ", mSellerIFollowedUpdateProduct = " + mSellerIFollowedUpdateProduct;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.mDisableAllNotification ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mCommentAfterMeOnProductEnabled ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mCommentOnProductIAmSellingEnabled ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mSomeoneRequestToPurchaseMyProductEnabled ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mSellerIFollowedPostNewProduct ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mSellerIFollowedUpdateProduct ? (byte) 1 : (byte) 0);
    }
}
