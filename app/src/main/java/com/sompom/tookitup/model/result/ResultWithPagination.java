package com.sompom.tookitup.model.result;

import com.google.gson.annotations.SerializedName;

/**
 * Created by he.rotha on 6/29/16.
 */
public class ResultWithPagination<T> extends Result<T> {
    private static final String FIELD_PAGINATION = "pagination";

    @SerializedName(FIELD_PAGINATION)
    private Pagination mPagination;

    public Pagination getPagination() {
        return mPagination;
    }

    public void setPagination(Pagination pagination) {
        mPagination = pagination;
    }
}
