package com.sompom.tookitup.model.notification;

import android.content.Context;
import android.content.Intent;

import com.desmond.squarecamera.utils.Keys;
import com.google.gson.annotations.SerializedName;
import com.sompom.tookitup.intent.newintent.SellerStoreIntent;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by He Rotha on 3/12/19.
 */
@RealmClass
public class NotificationUser implements NotificationAdaptive, RealmModel {
    @PrimaryKey
    @SerializedName(Keys.ID)
    private String mId;
    @SerializedName(Keys.ACTOR)
    private RealmList<UserTemp> mActor;
    @SerializedName(Keys.VERB)
    private String mVerb;
    @SerializedName(Keys.PUBLISH)
    private Date mPublished;
    @SerializedName(Keys.IS_READ)
    private boolean mIsRead = true;
    @SerializedName(Keys.OBJECT)
    private RealmList<UserTemp> mObject;

    public NotificationUser() {
    }

    @Override
    public Intent getIntent(Context context) {
        return new SellerStoreIntent(context, mActor.get(0).getId());
    }

    @Override
    public CharSequence getObjectText(Context context) {
        return getSubjectText(context, mObject);
    }

    @Override
    public String getThumbUrl() {
        return null;
    }

    public String getId() {
        return mId;
    }

    public RealmList<UserTemp> getActor() {
        return mActor;
    }

    public String getVerb() {
        return mVerb;
    }

    public Date getPublished() {
        return mPublished;
    }

    public boolean isRead() {
        return mIsRead;
    }

    public void setRead(boolean read) {
        mIsRead = read;
    }

    public RealmList<UserTemp> getObject() {
        return mObject;
    }
}
