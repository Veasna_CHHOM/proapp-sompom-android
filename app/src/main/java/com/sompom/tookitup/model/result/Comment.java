package com.sompom.tookitup.model.result;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.sompom.tookitup.helper.IntentData;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.emun.ViewType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


public class Comment implements Adaptive, Parcelable, Comparable<Comment> {

    public static final Creator<Comment> CREATOR = new Creator<Comment>() {
        @Override
        public Comment createFromParcel(Parcel source) {
            return new Comment(source);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };
    private static final String FIELD_ID = "_id";
    private static final String FIELD_USER = "authorInfo";
    private static final String FIELD_DATE = "createdAt";
    private static final String FIELD_CONTENT = "message";
    private static final String FIELD_REPLAY_COMMENT = "subComments";
    private static final String FIELD_MEDIA = "media";
    private static final String FIELD_IS_LIKE = "isLike";
    private static final String FIELD_LAST_SUBCOMMENT_ID = "lastCommentId";
    private static final String FIELD_TOTAL_SUB_COMMENT = "totalSubComments";
    private static final String FIELD_CONTENT_STAT = "contentStat";
    @SerializedName(FIELD_ID)
    private String mId;
    @SerializedName(FIELD_USER)
    private User mUser;
    @SerializedName(FIELD_DATE)
    private Date mDate;
    @SerializedName(FIELD_CONTENT)
    private String mContent;
    @SerializedName(FIELD_REPLAY_COMMENT)
    private List<Comment> mReplyComment;
    @SerializedName(FIELD_IS_LIKE)
    private Boolean mIsLike;
    @SerializedName(FIELD_LAST_SUBCOMMENT_ID)
    private String mLastSubCommentId;
    @SerializedName(FIELD_MEDIA)
    private List<Media> mMedia;
    @SerializedName(FIELD_TOTAL_SUB_COMMENT)
    private Integer mTotalSubComment;
    private Boolean mIsPosting;
    @SerializedName(FIELD_CONTENT_STAT)
    private ContentStat mContentStat;

    public Comment() {

    }

    protected Comment(Parcel in) {
        this.mId = in.readString();
        this.mUser = in.readParcelable(User.class.getClassLoader());
        long tmpMDate = in.readLong();
        this.mDate = tmpMDate == -1 ? null : new Date(tmpMDate);
        this.mContent = in.readString();
        this.mReplyComment = in.createTypedArrayList(Comment.CREATOR);
        this.mIsLike = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mLastSubCommentId = in.readString();
        this.mMedia = in.createTypedArrayList(Media.CREATOR);
        this.mTotalSubComment = in.readInt();
        this.mIsPosting = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mContentStat = in.readParcelable(User.class.getClassLoader());
    }

    public static Comment getNewComment(Comment comment, boolean isSubComment) {
        Comment comment1 = new Comment();
        comment1.setUser(null);
        comment1.setUser(null);
        comment1.setPosting(null);
        comment1.setLike(null);
        comment1.setTotalSubComment(null);
        comment1.setDate(null);
        comment1.setContentStat(null);

        if (isSubComment) {
            comment1.setLastSubCommentId(comment.getLastSubCommentId());
        }
        if (comment.getMedia() == null || comment.getMedia().isEmpty()) {
            comment1.setContent(comment.getContent());
        } else {
            comment1.setMedia(comment.getMedia());
            comment1.setContent(null);
        }
        return comment1;
    }

    public List<Comment> getReplyComment() {
        return mReplyComment;
    }

    public void setReplyComment(List<Comment> replyComment) {
        mReplyComment = replyComment;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser = user;
    }

    public Date getDate() {
        if (mDate == null) {
            mDate = new Date();
        }
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public boolean isLike() {
        if (mIsLike == null) {
            return false;
        }
        return mIsLike;
    }

    public void setLike(Boolean like) {
        mIsLike = like;
    }

    public List<Media> getMedia() {
        return mMedia;
    }

    public void setMedia(Media media) {
        List<Media> media1 = new ArrayList<>();
        media1.add(media);
        setMedia(media1);
    }

    public void setMedia(List<Media> media) {
        mMedia = media;
    }

    public boolean isPosting() {
        if (mIsPosting == null) {
            return false;
        }
        return mIsPosting;
    }

    public void setPosting(Boolean posting) {
        mIsPosting = posting;
    }

    public String getLastSubCommentId() {
        return mLastSubCommentId;
    }

    public void setLastSubCommentId(String lastSubCommentId) {
        mLastSubCommentId = lastSubCommentId;
    }

    public int getTotalSubComment() {
        if (mTotalSubComment == null) {
            return 0;
        }
        return mTotalSubComment;
    }

    public void setTotalSubComment(Integer totalSubComment) {
        mTotalSubComment = totalSubComment;
    }

    public ContentStat getContentStat() {
        if (mContentStat == null) {
            mContentStat = new ContentStat();
        }
        return mContentStat;
    }

    public void setContentStat(ContentStat contentStat) {
        mContentStat = contentStat;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Comment && Objects.equals(((Comment) obj).getId(), mId);
    }

    @Override
    public int hashCode() {
        return mId.hashCode();
    }

    @Override
    public String toString() {
        return "id = " + mId + ", user = " + mUser + ", date = " + mDate + ", content = " + mContent;
    }

    @Override
    public int compareTo(@NonNull Comment another) {
        double lastContactMicroTime = another.getDate().getTime();

        return (int) (getDate().getTime() - lastContactMicroTime);
    }

    @Override
    public ViewType getTimelineViewType() {
        return null;
    }

    @Override
    public void startActivityForResult(IntentData requiredIntentData) {
        //no starting activity
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeParcelable(this.mUser, flags);
        dest.writeLong(this.mDate != null ? this.mDate.getTime() : -1);
        dest.writeString(this.mContent);
        dest.writeTypedList(this.mReplyComment);
        dest.writeValue(this.mIsLike);
        dest.writeString(this.mLastSubCommentId);
        dest.writeTypedList(this.mMedia);
        dest.writeInt(this.mTotalSubComment);
        dest.writeValue(this.mIsPosting);
        dest.writeParcelable(this.mContentStat, flags);
    }
}