package com.sompom.tookitup.model.emun;

/**
 * Created by nuonveyo
 * on 1/7/19.
 */

public enum RoundCornerType {
    //One Media
    MeTop,
    MeMid,
    MeBottom,
    YouTop,
    YouMid,
    YouBottom,
    Single,

    //Multi Media
    TopLeft,
    TopRight,
    BottomLeft,
    BottomRight,
    Bottom,
    SmallSingle
}