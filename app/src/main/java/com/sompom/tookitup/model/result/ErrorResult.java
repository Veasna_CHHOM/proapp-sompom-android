package com.sompom.tookitup.model.result;

import com.google.gson.annotations.SerializedName;

/**
 * Created by He Rotha on 11/27/18.
 */
public class ErrorResult {
    @SerializedName("message")
    private Message mMessage;

    public Message getMessage() {
        return mMessage;
    }

    public static class Message {
        @SerializedName("error")
        private String mError;

        public String getError() {
            return mError;
        }

        public void setError(String error) {
            mError = error;
        }
    }

}
