package com.sompom.tookitup.model.emun;

/**
 * Created by nuonveyo on 10/19/18.
 */

public enum CommentItemType {
    Text(1),
    Image(2),
    Gif(3);

    private int mId;

    CommentItemType(int id) {
        mId = id;
    }

    public static CommentItemType getType(int id) {
        for (CommentItemType commentItemType : CommentItemType.values()) {
            if (commentItemType.getId() == id) {
                return commentItemType;
            }
        }
        return Text;
    }

    public int getId() {
        return mId;
    }
}
