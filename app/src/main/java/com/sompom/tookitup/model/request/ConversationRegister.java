package com.sompom.tookitup.model.request;

/**
 * Created by he.rotha on 8/25/16.
 */
public class ConversationRegister {
    //CHECKSTYLE:OFF
    private String userOne;
    private String userTwo;

    //CHECKSTYLE:OFF
    public ConversationRegister(String userTwo, String userOne) {
        this.userTwo = userTwo;
        this.userOne = userOne;
    }
}
