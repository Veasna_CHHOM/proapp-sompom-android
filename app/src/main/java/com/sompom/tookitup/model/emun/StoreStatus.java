package com.sompom.tookitup.model.emun;

/**
 * Created by He Rotha on 2/12/19.
 */
public enum StoreStatus {
    DEACTIVATE(0), ACTIVE(1);
    private final Integer mStatusProduct;

    StoreStatus(Integer i) {
        mStatusProduct = i;
    }

    public static StoreStatus fromValue(Integer value) {
        if (value == null) {
            return ACTIVE;
        }
        for (StoreStatus s : StoreStatus.values()) {
            if (s.getUserStatus() == value) {
                return s;
            }
        }
        return ACTIVE;
    }

    public int getUserStatus() {
        return mStatusProduct;
    }
}
