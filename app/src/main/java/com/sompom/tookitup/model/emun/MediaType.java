package com.sompom.tookitup.model.emun;

import android.support.annotation.Nullable;

/**
 * Created by He Rotha on 9/27/18.
 */
public enum MediaType {
    IMAGE("image"),
    VIDEO("video"),
    LIVE_VIDEO("liveVideo"),
    AUDIO("audio"),
    TENOR_GIF("tenorGif", "Tenor Gif");

    private final String mValue;
    /**
     * Name for display in chat or other place.
     * Normally, use for GIF and in Chat Notification or Chat Screen
     */
    private final String mName;

    MediaType(String value) {
        this(value, null);
    }

    MediaType(String value, String name) {
        mValue = value;
        mName = name;
    }

    public static MediaType fromValue(String value) {
        for (MediaType type : MediaType.values()) {
            if (type.mValue.equalsIgnoreCase(value)) {
                return type;
            }
        }
        return IMAGE;
    }

    @Nullable
    public String getName() {
        return mName;
    }

    public String getValue() {
        return mValue;
    }
}
