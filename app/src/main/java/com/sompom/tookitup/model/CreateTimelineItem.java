package com.sompom.tookitup.model;

import android.content.Intent;

import com.sompom.baseactivity.ResultCallback;
import com.sompom.tookitup.helper.IntentData;
import com.sompom.tookitup.helper.LoginActivityResultCallback;
import com.sompom.tookitup.intent.CreateTimelineIntent;
import com.sompom.tookitup.intent.newintent.MyCameraIntent;
import com.sompom.tookitup.intent.newintent.MyCameraResultIntent;
import com.sompom.tookitup.model.emun.CreateWallStreetScreenType;
import com.sompom.tookitup.model.emun.ViewType;
import com.sompom.tookitup.model.result.MoreGame;
import com.sompom.tookitup.newui.AbsBaseActivity;

import java.util.ArrayList;

/**
 * Created by He Rotha on 9/5/17.
 */

public class CreateTimelineItem extends ArrayList<MoreGame> implements Adaptive {
    @Override
    public String getId() {
        return null;
    }

    @Override
    public ViewType getTimelineViewType() {
        return ViewType.NewPost;
    }

    @Override
    public void startActivityForResult(com.sompom.tookitup.helper.IntentData requiredIntentData) {
        AbsBaseActivity context = requiredIntentData.getActivity();
        if (context == null) {
            return;
        }
        context.startLoginWizardActivity(new LoginActivityResultCallback() {
            @Override
            public void onActivityResultSuccess(boolean isAlreadyLogin) {
                if (requiredIntentData.getAction() == IntentData.Action.CREATE_TIMELINE_ITEM) {
                    context.startActivity(new CreateTimelineIntent(context, CreateWallStreetScreenType.CREATE_POST));
                } else if (requiredIntentData.getAction() == IntentData.Action.CREATE_TIMELINE_CAMERA) {
                    Intent cameraIntent = MyCameraIntent.newTimelineInstance(context, null);
                    context.startActivityForResult(cameraIntent, new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            MyCameraResultIntent cameraIntent1 = new MyCameraResultIntent(data);
                            final LifeStream lifeStream = new LifeStream();
                            lifeStream.setMedia(cameraIntent1.getMedia());
                            context.startActivity(new CreateTimelineIntent(context, lifeStream, CreateWallStreetScreenType.CREATE_POST));
                        }
                    });
                }
            }
        });

    }
}
