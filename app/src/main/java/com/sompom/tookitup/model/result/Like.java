package com.sompom.tookitup.model.result;

/**
 * Created by imac on 4/21/16.
 */
public class Like extends ErrorSupportModel {
    //CHECKSTYLE:OFF
    private long likes;
    private boolean isLike;
    //CHECKSTYLE:OFF

    public long getLikes() {
        return likes;
    }

    public void setLikes(final long likes) {
        this.likes = likes;
    }

    public boolean isLike() {
        return isLike;
    }

    public void setLike(final boolean like) {
        isLike = like;
    }
}
