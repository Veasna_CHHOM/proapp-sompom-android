package com.sompom.tookitup.model;

import com.sompom.tookitup.helper.IntentData;
import com.sompom.tookitup.model.emun.ViewType;

/**
 * Created by nuonveyo on 12/17/18.
 */

public class LoadPreviousComment implements Adaptive {
    @Override
    public String getId() {
        return null;
    }

    @Override
    public ViewType getTimelineViewType() {
        return null;
    }

    @Override
    public void startActivityForResult(IntentData requiredIntentData) {
        //no starting activity
    }
}
