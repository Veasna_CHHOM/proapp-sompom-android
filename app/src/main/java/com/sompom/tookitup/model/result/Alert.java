package com.sompom.tookitup.model.result;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by he.rotha on 7/11/16.
 */
public class Alert implements Parcelable {

    public static final Creator<Alert> CREATOR = new Creator<Alert>() {
        @Override
        public Alert createFromParcel(Parcel source) {
            return new Alert(source);
        }

        @Override
        public Alert[] newArray(int size) {
            return new Alert[size];
        }
    };
    private static final String FIELD_ALERT = "isAlert";
    @SerializedName(FIELD_ALERT)
    private boolean mIsAlert;

    public Alert() {
    }

    protected Alert(Parcel in) {
        this.mIsAlert = in.readByte() != 0;
    }

    public boolean isAlert() {
        return mIsAlert;
    }

    public void setAlert(boolean alert) {
        mIsAlert = alert;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.mIsAlert ? (byte) 1 : (byte) 0);
    }
}
