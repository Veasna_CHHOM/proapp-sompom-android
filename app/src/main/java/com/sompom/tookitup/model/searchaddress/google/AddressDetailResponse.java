package com.sompom.tookitup.model.searchaddress.google;

import com.google.gson.annotations.SerializedName;

public class AddressDetailResponse {
    @SerializedName("result")
    private Result mResult;
    @SerializedName("status")
    private String mStatus;

    public Result getResult() {
        return mResult;
    }

    public void setResult(Result result) {
        mResult = result;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

}
