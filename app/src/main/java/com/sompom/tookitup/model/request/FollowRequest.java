package com.sompom.tookitup.model.request;

/**
 * Created by he.rotha on 8/12/16.
 */
public class FollowRequest {
    //CHECKSTYLE:OFF
    private String buyer;
    private String seller;
    private boolean status;
    //CHECKSTYLE:OFF


    public FollowRequest(String buyer, String seller, boolean status) {
        this.buyer = buyer;
        this.seller = seller;
        this.status = status;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
