package com.sompom.tookitup.model.request;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by he.rotha on 4/22/16.
 */
public class PhoneLoginRequest implements Parcelable {
    public static final Creator<PhoneLoginRequest> CREATOR = new Creator<PhoneLoginRequest>() {
        @Override
        public PhoneLoginRequest createFromParcel(Parcel source) {
            return new PhoneLoginRequest(source);
        }

        @Override
        public PhoneLoginRequest[] newArray(int size) {
            return new PhoneLoginRequest[size];
        }
    };
    //CHECKSTYLE:OFF
    private String phone;
    //CHECKSTYLE:OFF
    private String token;

    public PhoneLoginRequest(String phone, String token) {
        this.phone = phone;
        this.token = token;
    }

    protected PhoneLoginRequest(Parcel in) {
        this.phone = in.readString();
        this.token = in.readString();
    }

    public String getPhone() {
        return phone;
    }

    public String getToken() {
        return token;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.phone);
        dest.writeString(this.token);
    }
}
