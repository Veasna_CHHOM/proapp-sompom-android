package com.sompom.tookitup.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.sompom.tookitup.helper.IntentData;
import com.sompom.tookitup.helper.upload.FileType;
import com.sompom.tookitup.model.emun.MediaType;
import com.sompom.tookitup.model.emun.PublishItem;
import com.sompom.tookitup.model.emun.ViewType;
import com.sompom.tookitup.model.result.ContentStat;
import com.sompom.tookitup.model.result.User;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import io.realm.RealmModel;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class Media implements Adaptive, Parcelable, Comparable<Media>, RealmModel, WallStreetAdaptive {

    public static final Creator<Media> CREATOR = new Creator<Media>() {
        @Override
        public Media createFromParcel(Parcel source) {
            return new Media(source);
        }

        @Override
        public Media[] newArray(int size) {
            return new Media[size];
        }
    };
    private static final String FIELD_ID = "_id";
    private static final String FIELD_TITLE = "title";
    private static final String FIELD_THUMBNAIL = "thumbnail";
    private static final String FIELD_URL = "url";
    private static final String FIELD_TYPE = "type";
    private static final String FILED_INDEX = "index";
    private static final String FILED_WIDTH = "width";
    private static final String FILED_HEIGHT = "height";
    private static final String FILED_DURATION = "duration";
    private static final String FIELD_IS_LIKE = "isLike";
    private static final String FIELD_CONTENT_STAT = "contentStat";
    @PrimaryKey
    @SerializedName(FIELD_ID)
    private String mId;
    @SerializedName(FIELD_TITLE)
    private String mTitle;
    @SerializedName(FIELD_URL)
    private String mUrl;
    @SerializedName(FIELD_TYPE)
    private String mType;
    @SerializedName(FILED_INDEX)
    private Integer mIndex;
    @SerializedName(FILED_WIDTH)
    private Integer mWidth;
    @SerializedName(FILED_HEIGHT)
    private Integer mHeight;
    @SerializedName(FIELD_THUMBNAIL)
    private String mThumbnail;
    @SerializedName(FILED_DURATION)
    private Long mDuration;
    @SerializedName(FIELD_IS_LIKE)
    private Boolean mIsLike;
    @Ignore
    @SerializedName(FIELD_USER_VIEW)
    private List<User> mUserView;
    @Ignore
    private Boolean mIsPause;
    @Ignore
    private Long mTime;
    @Ignore
    @SerializedName(FIELD_CONTENT_STAT)
    private ContentStat mContentStat;

    public Media() {
        setId(UUID.randomUUID().toString());
    }

    public Media(Media media) {
        this.mTitle = media.mTitle;
        this.mUrl = media.mUrl;
        this.mType = media.mType;
        this.mIndex = media.mIndex;
        this.mWidth = media.mWidth;
        this.mHeight = media.mHeight;
        this.mThumbnail = media.mThumbnail;
        this.mDuration = media.mDuration;
        this.mTime = media.mTime;
    }

    protected Media(Parcel in) {
        this.mId = in.readString();
        this.mTitle = in.readString();
        this.mUrl = in.readString();
        this.mType = in.readString();
        this.mIndex = (Integer) in.readValue(Integer.class.getClassLoader());
        this.mWidth = (Integer) in.readValue(Integer.class.getClassLoader());
        this.mHeight = (Integer) in.readValue(Integer.class.getClassLoader());
        this.mThumbnail = in.readString();
        this.mDuration = (Long) in.readValue(Long.class.getClassLoader());
        this.mIsLike = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mUserView = in.createTypedArrayList(User.CREATOR);
        this.mIsPause = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mTime = (Long) in.readValue(Long.class.getClassLoader());
        this.mContentStat = in.readParcelable(ContentStat.class.getClassLoader());
    }

    public String getId() {
        return mId;
    }

    @Override
    public void setId(String id) {
        mId = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public int getIndex() {
        if (mIndex == null) {
            return 0;
        }
        return mIndex;
    }

    public void setIndex(int index) {
        mIndex = index;
    }

    public int getWidth() {
        if (mWidth == null) {
            return 0;
        }
        return mWidth;
    }

    public void setWidth(int width) {
        mWidth = width;
    }

    public int getHeight() {
        if (mHeight == null) {
            return 0;
        }
        return mHeight;
    }

    public void setHeight(int height) {
        mHeight = height;
    }

    public String getThumbnail() {
        return mThumbnail;
    }

    public void setThumbnail(String thumbnail) {
        mThumbnail = thumbnail;
    }

    public MediaType getType() {
        return MediaType.fromValue(mType);
    }

    public void setType(MediaType type) {
        mType = type.getValue();
    }

    public long getDuration() {
        if (mDuration == null) {
            return 0;
        }
        return mDuration;
    }

    public void setDuration(long duration) {
        mDuration = duration;
    }

    public ContentStat getContentStat() {
        if (mContentStat == null) {
            mContentStat = new ContentStat();
        }
        return mContentStat;
    }

    public void setContentStat(ContentStat contentStat) {
        mContentStat = contentStat;
    }

    public List<User> getUserView() {
        return mUserView;
    }

    public void setUserView(List<User> userView) {
        mUserView = userView;
    }

    @Override
    public PublishItem getPublish() {
        return null;
    }

    @Override
    public void setPublish(int publish) {

    }

    @Override
    public double getLatitude() {
        return 0;
    }

    @Override
    public void setLatitude(double latitude) {

    }

    @Override
    public String getAddress() {
        return null;
    }

    @Override
    public void setAddress(String address) {

    }

    @Override
    public String getCity() {
        return null;
    }

    @Override
    public void setCity(String city) {

    }

    @Override
    public String getCountry() {
        return null;
    }

    @Override
    public void setCountry(String country) {

    }

    @Override
    public double getLongitude() {
        return 0;
    }

    @Override
    public void setLongitude(double longitude) {

    }

    @Override
    public User getUser() {
        return null;
    }

    @Override
    public List<Media> getMedia() {
        return Collections.emptyList();
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public String getShareUrl() {
        return null;
    }

    public boolean isLike() {
        if (mIsLike == null) {
            return false;
        }
        return mIsLike;
    }

    public void setLike(boolean like) {
        mIsLike = like;
    }

    @Override
    public Date getCreateDate() {
        return null;
    }

    @Override
    public void setCreateDate(Date createDate) {

    }

    public FileType getUploadType() {
        if (mType.equals(MediaType.IMAGE.getValue())) {
            return FileType.IMAGE;
        } else if (mType.equals(MediaType.VIDEO.getValue())) {
            return FileType.VIDEO;
        } else {
            return FileType.LiveVideo;
        }
    }


    @Override
    public boolean equals(Object obj) {
        return obj instanceof Media && Objects.equals(((Media) obj).getId(), mId);
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public String toString() {
        return "id = " + mId + ", title = " + mTitle + ", url = " + mUrl;
    }

    public boolean isPause() {
        if (mIsPause == null) {
            return false;
        }
        return mIsPause;
    }

    public void setPause(boolean pause) {
        mIsPause = pause;
    }

    public long getTime() {
        if (mTime == null) {
            return 0;
        }
        return mTime;
    }

    public void setTime(long time) {
        mTime = time;
    }

    @Override
    public ViewType getTimelineViewType() {
        return null;
    }

    @Override
    public void startActivityForResult(IntentData requiredIntentData) {
        //no starting activity
    }

    @Override
    public int compareTo(@NonNull Media o) {
        return o.getIndex();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeString(this.mTitle);
        dest.writeString(this.mUrl);
        dest.writeString(this.mType);
        dest.writeValue(this.mIndex);
        dest.writeValue(this.mWidth);
        dest.writeValue(this.mHeight);
        dest.writeString(this.mThumbnail);
        dest.writeValue(this.mDuration);
        dest.writeValue(this.mIsLike);
        dest.writeTypedList(this.mUserView);
        dest.writeValue(this.mIsPause);
        dest.writeValue(this.mTime);
        dest.writeParcelable(this.mContentStat, flags);
    }
}