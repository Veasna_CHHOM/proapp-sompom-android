package com.sompom.tookitup.model.result;


import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import com.sompom.tookitup.chat.MessageState;
import com.sompom.tookitup.listener.ConversationDataAdaptive;
import com.sompom.tookitup.model.Search;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.Date;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by he.rotha
 * on 8/25/16.
 */
@RealmClass
public class Conversation implements Parcelable, Search, ConversationDataAdaptive, RealmModel, Comparable<Conversation> {
    private static final String FIELD_ID = "conversationId";
    private static final String FIELD_LAST_MESSAGE = "lastMessage";
    private static final String FIELD_SENDER = "sender";
    private static final String FIELD_LAST_MESSAGE_DATE = "lastMessageDate";
    private static final String FIELD_PARTICIPANTS = "participants";
    private static final String FIELD_PRODUCT = "productInfos";
    private static final String FIELD_SELLER = "seller";
    private static final String GROUP_ID = "groupId";
    private static final String GROUP_NAME = "name";
    private static final String STATUS_SEEN_CONVERSATION = "statusSeenConversation";
    @PrimaryKey
    @SerializedName(FIELD_ID)
    private String mId;
    @Ignore
    @SerializedName(FIELD_LAST_MESSAGE)
    private Chat mLastMessage;
    @SerializedName(FIELD_SENDER)
    private String mSenderID;
    @SerializedName(FIELD_LAST_MESSAGE_DATE)
    private Date mDate;
    @SerializedName(FIELD_PRODUCT)
    private Product mProduct;
    @SerializedName(FIELD_PARTICIPANTS)
    private RealmList<User> mParticipants;
    private User mRecipient;
    private String mContent;
    @SerializedName(FIELD_SELLER)
    private String mSeller;
    private boolean mIsArchived;
    private Boolean mIsRead;
    @SerializedName(GROUP_ID)
    private String mGroupId;
    @SerializedName(GROUP_NAME)
    private String mGroupName;
    @SerializedName(STATUS_SEEN_CONVERSATION)
    @Ignore
    private JsonObject mStatusSeenConversation;
    private String mBackUpStatusSeenConversation;

    //CHECKSTYLE:OFF
    public Conversation() {
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public User getRecipient(Context context) {
        if (mRecipient == null && mParticipants != null) {
            for (User participant : mParticipants) {
                if (!TextUtils.equals(participant.getId(), SharedPrefUtils.getUserId(context))) {
                    mRecipient = participant;
                    break;
                }
            }
        }
        return mRecipient;
    }

    public void setRecipient(User recipient) {
        mRecipient = recipient;
    }

    public Chat getLastMessage() {
        return mLastMessage;
    }

    public void setLastMessage(Chat lastMessage) {
        mLastMessage = lastMessage;
    }

    public String getSenderID() {
        return mSenderID;
    }

    public void setSenderID(String senderID) {
        mSenderID = senderID;
    }

    public String getContent(Context context) {
        if (TextUtils.isEmpty(mContent)) {
            if (getLastMessage() != null) {
                mContent = getLastMessage().getActualContent(context);
            } else {
                //TODO: Need to remove after sever fix response of last message
                mContent = "";
            }
        }
        return mContent;
    }

    public JsonObject getStatusSeenConversation() {
        if (mStatusSeenConversation != null) {
            return mStatusSeenConversation;
        } else {
            if (!TextUtils.isEmpty(mBackUpStatusSeenConversation)) {
                return new Gson().fromJson(mBackUpStatusSeenConversation, JsonObject.class);
            } else {
                return null;
            }
        }
    }

    public void setBackUpStatusSeenConversation(String backUpStatusSeenConversation) {
        mBackUpStatusSeenConversation = backUpStatusSeenConversation;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public Product getProduct() {
        return mProduct;
    }

    public void setProduct(Product product) {
        mProduct = product;
    }

    public boolean isArchived() {
        return mIsArchived;
    }

    public void setArchived(boolean archived) {
        mIsArchived = archived;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public String getGroupId() {
        return mGroupId;
    }

    public void setGroupId(String groupId) {
        mGroupId = groupId;
    }

    public boolean isRead(Context context) {
        if (mIsRead != null) {
            return mIsRead;
        } else if (getLastMessage() != null) {
            mIsRead = SharedPrefUtils.checkIsMe(context, getLastMessage().getSenderId()) ||
                    getLastMessage().getStatus() == MessageState.SEEN;
        } else {
            mIsRead = false;
        }
        return mIsRead;
    }

    public void setRead(boolean read) {
        mIsRead = read;
    }

    public List<User> getParticipants() {
        return mParticipants;
    }

    public String getSeller() {
        return mSeller;
    }

    public void setSeller(String seller) {
        mSeller = seller;
    }

    public String getGroupName() {
        return mGroupName;
    }

    public void setGroupName(String groupName) {
        mGroupName = groupName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Conversation)) return false;
        Conversation that = (Conversation) o;
        return TextUtils.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public int compareTo(@NonNull Conversation conversation) {
        if (conversation.getDate() != null && getDate() != null) {
            return Long.compare(conversation.getDate().getTime(), getDate().getTime());
        }

        return 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeParcelable(this.mLastMessage, flags);
        dest.writeString(this.mSenderID);
        dest.writeLong(this.mDate != null ? this.mDate.getTime() : -1);
        dest.writeParcelable(this.mProduct, flags);
        dest.writeTypedList(this.mParticipants);
        dest.writeParcelable(this.mRecipient, flags);
        dest.writeString(this.mContent);
        dest.writeString(this.mSeller);
        dest.writeByte(this.mIsArchived ? (byte) 1 : (byte) 0);
        dest.writeValue(this.mIsRead);
        dest.writeString(this.mGroupId);
        dest.writeString(this.mGroupName);
        dest.writeString(this.mBackUpStatusSeenConversation);
    }

    protected Conversation(Parcel in) {
        this.mId = in.readString();
        this.mLastMessage = in.readParcelable(Chat.class.getClassLoader());
        this.mSenderID = in.readString();
        long tmpMDate = in.readLong();
        this.mDate = tmpMDate == -1 ? null : new Date(tmpMDate);
        this.mProduct = in.readParcelable(Product.class.getClassLoader());
        this.mParticipants = new RealmList<>();
        this.mParticipants.addAll(in.createTypedArrayList(User.CREATOR));
        this.mRecipient = in.readParcelable(User.class.getClassLoader());
        this.mContent = in.readString();
        this.mSeller = in.readString();
        this.mIsArchived = in.readByte() != 0;
        this.mIsRead = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mGroupId = in.readString();
        this.mGroupName = in.readString();
        this.mBackUpStatusSeenConversation = in.readString();
    }

    public static final Creator<Conversation> CREATOR = new Creator<Conversation>() {
        @Override
        public Conversation createFromParcel(Parcel source) {
            return new Conversation(source);
        }

        @Override
        public Conversation[] newArray(int size) {
            return new Conversation[size];
        }
    };
}