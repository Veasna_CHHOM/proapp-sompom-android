package com.sompom.tookitup.model.result;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class MoreGame implements Parcelable {
    public static final Creator<MoreGame> CREATOR = new Creator<MoreGame>() {
        public MoreGame createFromParcel(Parcel in) {
            return new MoreGame(in);
        }

        public MoreGame[] newArray(int size) {
            return new MoreGame[size];
        }
    };

    private static final String FIELD_CODE_GAME = "codeGame";
    private static final String FIELD_ORDER = "order";
    private static final String FIELD_TITLE = "title";
    private static final String FIELD_SUBTITLE = "subtitle";
    private static final String FIELD_PREVIEW = "preview";
    private static final String FIELD_PREVIEW_IPAD = "previewIpad";
    private static final String FIELD_LINK_PREVIEW_BASE_URL = "linkPreviewBaseUrl";
    private static final String FIELD_LINKI_OS = "linkiOS";
    private static final String FIELD_LINK_ANDROID = "linkAndroid";
    private static final String FIELD_LINK_FACEBOOK = "linkFacebook";

    public String value;
    @SerializedName(FIELD_CODE_GAME)
    private String mCodeGame;
    @SerializedName(FIELD_ORDER)
    private int mOrder;
    @SerializedName(FIELD_TITLE)
    private String mTitle;
    @SerializedName(FIELD_SUBTITLE)
    private String mSubtitle;
    @SerializedName(FIELD_PREVIEW)
    private String mPreview;
    @SerializedName(FIELD_PREVIEW_IPAD)
    private String mPreviewIPad;
    @SerializedName(FIELD_LINK_PREVIEW_BASE_URL)
    private String mLinkPreviewBaseUrl;
    @SerializedName(FIELD_LINKI_OS)
    private String mLinkiO;
    @SerializedName(FIELD_LINK_ANDROID)
    private String mLinkAndroid;
    @SerializedName(FIELD_LINK_FACEBOOK)
    private String mLinkFacebook;


    public MoreGame() {

    }

    public MoreGame(Parcel in) {
        mPreview = in.readString();
        mPreviewIPad = in.readString();
        mLinkFacebook = in.readString();
        mCodeGame = in.readString();
        mTitle = in.readString();
        mSubtitle = in.readString();
        mLinkAndroid = in.readString();
        mLinkPreviewBaseUrl = in.readString();
        mOrder = in.readInt();
        mLinkiO = in.readString();
    }

    public String getPreview() {
        return mPreview;
    }

    public void setPreview(String preview) {
        mPreview = preview;
    }

    public String getPreviewIPad() {
        return mPreviewIPad;
    }

    public void setPreviewIPad(String previewIPad) {
        mPreviewIPad = previewIPad;
    }

    public String getLinkFacebook() {
        return mLinkFacebook;
    }

    public void setLinkFacebook(String linkFacebook) {
        mLinkFacebook = linkFacebook;
    }

    public String getCodeGame() {
        return mCodeGame;
    }

    public void setCodeGame(String codeGame) {
        mCodeGame = codeGame;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getSubtitle() {
        return mSubtitle;
    }

    public void setSubtitle(String subtitle) {
        mSubtitle = subtitle;
    }

    public String getLinkAndroid() {
        return mLinkAndroid;
    }

    public void setLinkAndroid(String linkAndroid) {
        mLinkAndroid = linkAndroid;
    }

    public String getLinkPreviewBaseUrl() {
        return mLinkPreviewBaseUrl;
    }

    public void setLinkPreviewBaseUrl(String linkPreviewBaseUrl) {
        mLinkPreviewBaseUrl = linkPreviewBaseUrl;
    }

    public int getOrder() {
        return mOrder;
    }

    public void setOrder(int order) {
        mOrder = order;
    }

    public String getLinkiO() {
        return mLinkiO;
    }

    public void setLinkiO(String linkiO) {
        mLinkiO = linkiO;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mPreview);
        dest.writeString(mPreviewIPad);
        dest.writeString(mLinkFacebook);
        dest.writeString(mCodeGame);
        dest.writeString(mTitle);
        dest.writeString(mSubtitle);
        dest.writeString(mLinkAndroid);
        dest.writeString(mLinkPreviewBaseUrl);
        dest.writeInt(mOrder);
        dest.writeString(mLinkiO);
    }

    @Override
    public String toString() {
        return "preview = " + mPreview +
                ", previewIPad = " + mPreviewIPad +
                ", linkFacebook = " + mLinkFacebook +
                ", codeGame = " + mCodeGame +
                ", titleEn = " + mTitle +
                ", subtitleEn = " + mSubtitle +
                ", linkAndroid = " + mLinkAndroid +
                ", linkPreviewBaseUrl = " + mLinkPreviewBaseUrl +
                ", order = " + mOrder +
                ", linkiO = " + mLinkiO;
    }
}