package com.sompom.tookitup.model;

import com.google.gson.annotations.SerializedName;
import com.sompom.tookitup.model.result.Chat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by He Rotha on 1/2/19.
 */
//TODO: this class is used for custom push only, make sure delete if server is done
public class OneSignalPushModel<T> {
    @SerializedName("app_id")
    private final String mAppId;
    @SerializedName("contents")
    private final Content mContent;
    @SerializedName("filters")
    private final List<Filter> mFilterList;
    @SerializedName("content_available")
    private final int mContentAvailable;
    @SerializedName("mutable_content")
    private final int mMutableContent;
    @SerializedName("data")
    private OneSignalWrapperData<T> mOneSignalWrapperData;

    public OneSignalPushModel(OneSignalWrapperData<T> data) {
        mOneSignalWrapperData = data;
        mAppId = "4b0a7b2b-44d6-4ce8-b00d-0bf805b43f45";

        Filter filter = new Filter();
        filter.mField = "tag";
        filter.mKey = "userId";
        filter.mRelation = "=";

        if (mOneSignalWrapperData.getData() instanceof Chat) {
            filter.mValue = ((Chat) mOneSignalWrapperData.getData()).getSendTo();
        }

        mFilterList = new ArrayList<>();
        mFilterList.add(filter);

        mContent = new Content();
        mContentAvailable = 1;
        mMutableContent = 1;
    }

    public static class Filter {
        @SerializedName("field")
        private String mField;
        @SerializedName("key")
        private String mKey;
        @SerializedName("relation")
        private String mRelation;
        @SerializedName("value")
        private String mValue;
    }

    public static class Content {
        @SerializedName("en")
        private final String mContent;

        Content() {
            mContent = "test push chat from android phone";
        }
    }
}
