package com.sompom.tookitup.model.result;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.sompom.tookitup.R;
import com.sompom.tookitup.chat.MessageState;
import com.sompom.tookitup.model.BaseChatModel;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.emun.MediaType;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class Chat implements Comparable<Chat>, Parcelable, BaseChatModel, RealmModel {
    public static final Creator<Chat> CREATOR = new Creator<Chat>() {
        @Override
        public Chat createFromParcel(Parcel source) {
            return new Chat(source);
        }

        @Override
        public Chat[] newArray(int size) {
            return new Chat[size];
        }
    };
    private static final String FIELD_ID = "messageId";
    private static final String FIELD_SENDER = "sender";
    private static final String FIELD_SENDER_ID = "senderId";
    private static final String FIELD_PRODUCT_ID = "productId";
    private static final String FIELD_DATE = "date";
    private static final String FIELD_CONTENT = "content";
    private static final String FIELD_PHOTO = "media";
    private static final String FIELD_STATUS = "status";
    private static final String FIELD_IS_TYPING = "isTyping";
    private static final String FIELD_CHANNEL_ID = "conversationId";
    private static final String FIELD_SEND_TO = "sendTo";
    private static final String FIELD_PRODUCT = "product";
    private static final String FIELD_TYPE = "type";
    private static final String FIELD_SEEN = "seen";
    private static final String FIELD_IS_DELETE = "isDelete";
    private static final String IS_GROUP = "isGroup";
    private static final String GROUP_NAME = "groupName";
    @SerializedName(FIELD_ID)
    @PrimaryKey
    private String mId;
    @SerializedName(FIELD_DATE)
    private Date mDate;
    @SerializedName(FIELD_CONTENT)
    private String mContent;
    @SerializedName(FIELD_PHOTO)
    private RealmList<Media> mMediaList;
    @SerializedName(FIELD_STATUS)
    private String mStatus = MessageState.SENT.getValue();
    @SerializedName(FIELD_SENDER_ID)
    private String mSenderId;
    @SerializedName(FIELD_PRODUCT_ID)
    private String mProductId;
    @SerializedName(FIELD_CHANNEL_ID)
    private String mChannelId;
    @SerializedName(FIELD_SEND_TO)
    private String mSendTo;
    @SerializedName(FIELD_SEEN)
    private RealmList<String> mSeen;
    @SerializedName(FIELD_IS_DELETE)
    private Boolean mIsDelete;
    @Ignore
    @SerializedName(FIELD_TYPE)
    private String mSendingType;
    @Ignore
    @SerializedName(FIELD_IS_TYPING)
    private Boolean mIsTyping;
    @Ignore
    @SerializedName(FIELD_SENDER)
    private User mSender;
    @Ignore
    @SerializedName(FIELD_PRODUCT)
    private Product mProduct;
    @Ignore
    private transient boolean mIsExpandHeight; //transient for ignore serialized and parcel
    @Ignore
    private transient LinkPreviewModel mLinkPreviewModel; //transient for ignore serialized and parcel
    @SerializedName(IS_GROUP)
    private boolean mIsGroup;
    @Ignore
    private User mRecipient;
    @SerializedName(GROUP_NAME)
    private String mGroupName;
    @Ignore
    private List<User> mSeenParticipants;

    //CHECKSTYLE:OFF
    public Chat() {

    }

    public Chat(Chat chat) {
        setId(chat.getId());
        setSenderId(chat.getSenderId());
        setSender(chat.getSender());
        setSendTo(chat.getSendTo());
        setStatus(chat.getStatus());
        setSendingType(chat.getSendingType());
        setChannelId(chat.getChannelId());
        setContent(chat.getContent());
        setProductId(chat.getProductId());
        setMediaList(chat.getMediaList());
        setTyping(chat.getTyping());
        setDate(chat.getDate());
        setExpandHeight(isExpandHeight());
        setLinkPreviewModel(chat.getLinkPreviewModel());
        setGroup(chat.isGroup());
        setGroupName(chat.getGroupName());
        setSeenParticipants(chat.getSeenParticipants());

        if (chat.getProduct() != null) {
            Product product = new Product();
            product.setUser(chat.getProduct().getUser());
            product.setId(chat.getProduct().getId());
            product.setPrice(chat.getProduct().getPrice());
            product.setProductMedia(chat.getProduct().getMedia());
            product.setName(chat.getProduct().getName());
            product.setCurrency(chat.getProduct().getCurrency());
            setProduct(product);
        }
    }

    protected Chat(Parcel in) {
        this.mId = in.readString();
        long tmpMDate = in.readLong();
        this.mDate = tmpMDate == -1 ? null : new Date(tmpMDate);
        this.mContent = in.readString();
        this.mMediaList = new RealmList<>();
        ArrayList<Media> mediaList = in.createTypedArrayList(Media.CREATOR);
        if (mediaList != null && !mediaList.isEmpty()) {
            mMediaList.addAll(mediaList);
        }
        this.mStatus = in.readString();
        this.mSenderId = in.readString();
        this.mProductId = in.readString();
        this.mChannelId = in.readString();
        this.mSendTo = in.readString();
        this.mSeen = new RealmList<>();
        ArrayList<String> seen = in.createStringArrayList();
        if (seen != null && !seen.isEmpty()) {
            mSeen.addAll(seen);
        }
        this.mSendingType = in.readString();
        this.mIsTyping = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mSender = in.readParcelable(User.class.getClassLoader());
        this.mProduct = in.readParcelable(Product.class.getClassLoader());
        this.mIsDelete = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mIsGroup = in.readByte() != 0;
        this.mRecipient = in.readParcelable(User.class.getClassLoader());
        this.mGroupName = in.readString();
        this.mSeenParticipants = new RealmList<>();
        ArrayList<User> typedArrayList = in.createTypedArrayList(User.CREATOR);
        if (typedArrayList != null) {
            this.mSeenParticipants.addAll(typedArrayList);
        }
    }

    public Chat cloneNewChat() {
        Chat chat1 = new Chat();

        chat1.setId(getId());
        chat1.setSenderId(getSenderId());
        chat1.setSender(getSender());
        chat1.setDate(getDate());
        chat1.setStatus(getStatus());
        chat1.setRecipient(getRecipient());
        chat1.setSendTo(getSendTo());
        chat1.setChannelId(getChannelId());
        chat1.setMediaList(getMediaList());
        chat1.setContent(getContent());
        chat1.setGroup(isGroup());
        chat1.setGroupName(getGroupName());
        chat1.setSeenParticipants(getSeenParticipants());

        return chat1;
    }

    public static Chat getNewChat(Context context,
                                  User sendTo,
                                  @Nullable Product product) {
        Chat chat = new Chat();
        chat.setId(UUID.randomUUID().toString());
        chat.setSender(SharedPrefUtils.getCurrentUser(context));
        chat.setSenderId(SharedPrefUtils.getUserId(context));
        chat.setDate(new Date());
        chat.setStatus(MessageState.SENDING);
        chat.setRecipient(sendTo);

        if (product != null) {
            chat.setProductId(product.getId());
            chat.setProduct(product);
        }
        if (sendTo != null) {
            chat.setSendTo(sendTo.getId());
        }
        return chat;
    }

    public List<User> getSeenParticipants() {
        return mSeenParticipants;
    }

    public void setSeenParticipants(List<User> seenParticipants) {
        mSeenParticipants = seenParticipants;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public User getSender() {
        return mSender;
    }

    public String getGroupName() {
        return mGroupName;
    }

    public void setGroupName(String groupName) {
        mGroupName = groupName;
    }

    public void setSender(User sender) {
        mSender = sender;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public boolean isExpandHeight() {
        return mIsExpandHeight;
    }

    public void setExpandHeight(boolean expandHeight) {
        mIsExpandHeight = expandHeight;
    }

    public List<Media> getMediaList() {
        return mMediaList;
    }

    public User getRecipient() {
        return mRecipient;
    }

    public void setRecipient(User recipient) {
        mRecipient = recipient;
    }

    public void setMediaList(List<Media> mediaList) {
        if (mediaList != null) {
            mMediaList = new RealmList<>();
            mMediaList.addAll(mediaList);
        }
    }

    public String getSenderId() {
        return mSenderId;
    }

    public void setSenderId(String senderId) {
        mSenderId = senderId;
    }

    public String getChannelId() {
        return mChannelId;
    }

    public void setChannelId(String channelId) {
        mChannelId = channelId;
    }

    public String getProductId() {
        return mProductId;
    }

    public void setProductId(String productId) {
        mProductId = productId;
    }

    public Boolean getTyping() {
        return mIsTyping;
    }

    public void setTyping(Boolean typing) {
        mIsTyping = typing;
    }

    public String getSendTo() {
        return mSendTo;
    }

    public void setSendTo(String sendTo) {
        mSendTo = sendTo;
    }

    public Product getProduct() {
        return mProduct;
    }

    public void setProduct(Product product) {
        mProduct = product;
    }

    public boolean isGroup() {
        return mIsGroup;
    }

    public void setGroup(boolean group) {
        mIsGroup = group;
    }

    public Type getType() {
        if (getMediaList() == null || getMediaList().isEmpty()) {
            return Type.TEXT;
        } else {
            Media media = getMediaList().get(0);
            if (media.getType() == MediaType.IMAGE) {
                return Type.IMAGE;
            } else if (media.getType() == MediaType.TENOR_GIF) {
                return Type.GIF;
            } else if (media.getType() == MediaType.AUDIO) {
                return Type.AUDIO;
            } else {
                return Type.TEXT;
            }
        }
    }

    public RealmList<String> getSeen() {
        return mSeen;
    }

    public void setSeen(RealmList<String> seen) {
        mSeen = seen;
    }

    public String getActualContent(Context context) {
        String message = null;
        if (isDeleted()) {
            if (TextUtils.equals(getSenderId(), SharedPrefUtils.getUserId(context))) {
                message = context.getString(R.string.chat_you_remove_message);
            } else {
                if (getSender() == null) {
                    message = context.getString(R.string.chat_remove_message_title);
                } else {
                    String name = getSender().getFirstName();
                    if (TextUtils.isEmpty(name)) {
                        name = getSender().getLastName();
                    }
                    message = name + " " + context.getString(R.string.chat_remove_message_title);
                }
            }
        } else if (getType() == Chat.Type.TEXT) {
            if (TextUtils.equals(getSenderId(), SharedPrefUtils.getUserId(context))) {
                message = context.getString(R.string.des_mark_as_your_chat) + " " + getContent();
            } else {
                message = getContent();
            }
        } else if (getType() == Chat.Type.AUDIO) {
            if (TextUtils.equals(getSenderId(), SharedPrefUtils.getUserId(context))) {
                message = context.getString(R.string.des_mark_as_your_chat) + " " + context.getString(R.string.chat_voice_message);
            } else {
                message = context.getString(R.string.chat_voice_message);
            }
        } else if (getType() == Chat.Type.IMAGE) {
            if (getMediaList().size() == 1) {
                if (TextUtils.equals(getSenderId(), SharedPrefUtils.getUserId(context))) {
                    message = context.getString(R.string.des_mark_as_your_chat) + " " + context.getString(R.string.chat_photo_message);
                } else {
                    message = context.getString(R.string.chat_photo_message);
                }
            } else {
                if (TextUtils.equals(getSenderId(), SharedPrefUtils.getUserId(context))) {
                    message = context.getString(R.string.des_mark_as_your_chat) + " " + context.getString(R.string.chat_photos_message, getMediaList().size());
                } else {
                    message = context.getString(R.string.chat_photos_message, getMediaList().size());
                }
            }
        } else if (getMediaList().get(0).getType() == MediaType.TENOR_GIF) {
            if (TextUtils.equals(getSenderId(), SharedPrefUtils.getUserId(context))) {
                message = context.getString(R.string.des_mark_as_your_chat) + " " + context.getString(R.string.chat_gif_message, MediaType.TENOR_GIF.getName());
            } else {
                message = context.getString(R.string.chat_gif_message, MediaType.TENOR_GIF.getName());
            }
        }
        return message;
    }

    public SendingType getSendingType() {
        return SendingType.from(mSendingType);
    }

    public void setSendingType(SendingType sendingType) {
        mSendingType = sendingType.mValue;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Chat) {
            final String id = ((Chat) obj).getId();
            return !TextUtils.isEmpty(id) && !TextUtils.isEmpty(mId) && id.equals(mId);
        } else {
            return false;
        }
    }

    public MessageState getStatus() {
        return MessageState.fromValue(mStatus);
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public void setStatus(MessageState status) {
        mStatus = status.getValue();
    }

    public LinkPreviewModel getLinkPreviewModel() {
        return mLinkPreviewModel;
    }

    public void setLinkPreviewModel(LinkPreviewModel linkPreviewModel) {
        mLinkPreviewModel = linkPreviewModel;
    }

    public void updateEveryoneSeen() {
        if (mSeen == null) {
            mSeen = new RealmList<>();
        }
        if (!mSeen.contains(getSendTo())) {
            mSeen.add(getSendTo());
        }
        if (!mSeen.contains(getSenderId())) {
            mSeen.add(getSenderId());
        }
    }

    public boolean isEveryoneSeen() {
        if (mSeen == null) {
            return false;
        } else {
            return mSeen.size() == 2;
        }
    }

    public boolean isDeleted() {
        if (mIsDelete == null) {
            return false;
        }
        return mIsDelete;
    }

    public void setDelete(boolean delete) {
        mIsDelete = delete;
    }

    @Override
    public int hashCode() {
        return mId.hashCode();
    }

    @Override
    public String toString() {
        return "id = " + mId + ", sender = " + mSender + ", date = " + mDate + ", content = " + mContent;
    }

    @Override
    public int compareTo(@NonNull Chat another) {
        return Long.compare(getTime(), another.getTime());
    }

    @Override
    public long getTime() {
        return mDate.getTime();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeLong(this.mDate != null ? this.mDate.getTime() : -1);
        dest.writeString(this.mContent);
        dest.writeTypedList(this.mMediaList);
        dest.writeString(this.mStatus);
        dest.writeString(this.mSenderId);
        dest.writeString(this.mProductId);
        dest.writeString(this.mChannelId);
        dest.writeString(this.mSendTo);
        dest.writeStringList(this.mSeen);
        dest.writeString(this.mSendingType);
        dest.writeValue(this.mIsTyping);
        dest.writeParcelable(this.mSender, flags);
        dest.writeParcelable(this.mProduct, flags);
        dest.writeValue(this.mIsDelete);
        dest.writeByte(this.mIsGroup ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.mRecipient, flags);
        dest.writeString(this.mGroupName);
        dest.writeTypedList(this.mSeenParticipants);
    }

    public enum Type {
        TEXT(1),
        AUDIO(2),
        IMAGE(3),
        GIF(4);

        private final int mId;

        Type(int id) {
            mId = id;
        }

        public static Type getType(int id) {
            for (Type type : Type.values()) {
                if (id == type.getId()) {
                    return type;
                }
            }
            return TEXT;
        }

        public int getId() {
            return mId;
        }

    }

    public enum SendingType {
        NONE(null),
        TYPING("typing"),
        READ_STATUS("readStatus"),
        DELETE("delete");

        private final String mValue;

        SendingType(String s) {
            mValue = s;
        }

        public static SendingType from(String value) {
            for (SendingType sendingType : SendingType.values()) {
                if (TextUtils.equals(sendingType.mValue, value)) {
                    return sendingType;
                }
            }
            return NONE;
        }

        public String getValue() {
            return mValue;
        }
    }
}