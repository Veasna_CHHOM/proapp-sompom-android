package com.sompom.tookitup.model.emun;

import com.sompom.tookitup.widget.lifestream.Format;
import com.sompom.tookitup.widget.lifestream.Orientation;

/**
 * Created by He Rotha on 7/11/18.
 */
public enum ViewType {
    ViewPagerAd(102),
    ActiveUser(103),
    Timeline(104),
    LiveVideoTimeline(105),
    Ads(106),
    NewPost(107),

//    WallStreet Style 2
//    ProductThreeOneItem(201, Format.Three, 1, Orientation.Horizontal),
//    ProductThreeTwoItem(202, Format.Three, 2, Orientation.Horizontal),
//    ProductThreeThreeItem(203, Format.Three, 3, Orientation.Horizontal),
//
//    TimelineThreeOneItem(301, Format.Three, 1, Orientation.Horizontal),
//    TimelineThreeTwoItem(302, Format.Three, 2, Orientation.Horizontal),
//    TimelineThreeThreeItem(303, Format.Three, 3, Orientation.Horizontal),

    //    WallStreet Style 1
    FreeStyle(401, Format.FreeStyle, 1, Orientation.Horizontal),

    Timeline_FreeStyle_1(201, Format.FreeStyle, 1, Orientation.Vertical),
    Timeline_FreeStyle_2_Vertical(202, Format.FreeStyle, 2, Orientation.Vertical),
    Timeline_FreeStyle_2_Horizontal(203, Format.FreeStyle, 2, Orientation.Horizontal),
    Timeline_FreeStyle_3_Vertical(204, Format.FreeStyle, 3, Orientation.Vertical),
    Timeline_FreeStyle_3_Horizontal(205, Format.FreeStyle, 3, Orientation.Horizontal),
    Timeline_FreeStyle_4_Vertical(206, Format.FreeStyle, 4, Orientation.Vertical),
    Timeline_FreeStyle_4_Horizontal(207, Format.FreeStyle, 4, Orientation.Horizontal),
    Timeline_FreeStyle_5_Vertical(208, Format.FreeStyle, 5, Orientation.Vertical),
    Timeline_FreeStyle_5_Horizontal(209, Format.FreeStyle, 5, Orientation.Horizontal),
    Timeline_SquareAll_2(210, Format.SquareAll, 2, Orientation.Vertical),
    Timeline_SquareAll_3(211, Format.SquareAll, 3, Orientation.Vertical),
    Timeline_SquareAll_4(212, Format.SquareAll, 4, Orientation.Vertical),
    Timeline_SquareAll_5(213, Format.SquareAll, 5, Orientation.Vertical),
    Timeline_SquareFirst_2(Timeline_SquareAll_2.mViewType, Format.SquareStart, 2, Orientation.Vertical), //we get the same
    Timeline_SquareFirst_3(Timeline_FreeStyle_3_Horizontal.mViewType, Format.SquareStart, 3, Orientation.Vertical),
    Timeline_SquareFirst_4(Timeline_SquareAll_4.mViewType, Format.SquareStart, 4, Orientation.Vertical),
    Timeline_SquareFirst_5(Timeline_SquareAll_5.mViewType, Format.SquareStart, 5, Orientation.Vertical),

    Product_FreeStyle_1(301, Format.FreeStyle, 1, Orientation.Vertical),
    Product_FreeStyle_2_Vertical(302, Format.FreeStyle, 2, Orientation.Vertical),
    Product_FreeStyle_2_Horizontal(303, Format.FreeStyle, 2, Orientation.Horizontal),
    Product_FreeStyle_3_Vertical(304, Format.FreeStyle, 3, Orientation.Vertical),
    Product_FreeStyle_3_Horizontal(305, Format.FreeStyle, 3, Orientation.Horizontal),
    Product_FreeStyle_4_Vertical(306, Format.FreeStyle, 4, Orientation.Vertical),
    Product_FreeStyle_4_Horizontal(307, Format.FreeStyle, 4, Orientation.Horizontal),
    Product_FreeStyle_5_Vertical(308, Format.FreeStyle, 5, Orientation.Vertical),
    Product_FreeStyle_5_Horizontal(309, Format.FreeStyle, 5, Orientation.Horizontal),
    Product_SquareAll_2(310, Format.SquareAll, 2, Orientation.Vertical),
    Product_SquareAll_3(311, Format.SquareAll, 3, Orientation.Vertical),
    Product_SquareAll_4(312, Format.SquareAll, 4, Orientation.Vertical),
    Product_SquareAll_5(313, Format.SquareAll, 5, Orientation.Vertical),
    Product_SquareFirst_2(Product_SquareAll_2.mViewType, Format.SquareStart, 2, Orientation.Vertical), //we get the same
    Product_SquareFirst_3(Product_FreeStyle_3_Horizontal.mViewType, Format.SquareStart, 3, Orientation.Vertical),
    Product_SquareFirst_4(Product_SquareAll_4.mViewType, Format.SquareStart, 4, Orientation.Vertical),
    Product_SquareFirst_5(Product_SquareAll_5.mViewType, Format.SquareStart, 5, Orientation.Vertical);


    private int mViewType;
    private Format mFormat;
    private int mNumberOfItem;
    private Orientation mOrientation;

    ViewType(int viewType) {
        this(viewType, Format.FreeStyle, -1, Orientation.Vertical);
    }

    ViewType(int id, ViewType viewPagerAd) {
        this(id, viewPagerAd.mFormat, viewPagerAd.mNumberOfItem, viewPagerAd.mOrientation);
    }

    ViewType(int viewType, Format format, int numberOfItem, Orientation orientation) {
        mViewType = viewType;
        mFormat = format;
        mNumberOfItem = numberOfItem;
        mOrientation = orientation;
    }

    public static ViewType fromValue(int value) {
        for (ViewType viewType : ViewType.values()) {
            if (viewType.mViewType == value) {
                return viewType;
            }
        }
        return Timeline;
    }

    public int getViewType() {
        return mViewType;
    }

    public void setViewType(int viewType) {
        mViewType = viewType;
    }

    public int getTimelineViewType() {
        return mViewType;
    }

    public Format getFormat() {
        return mFormat;
    }

    public void setFormat(Format format) {
        mFormat = format;
    }

    public int getNumberOfItem() {
        return mNumberOfItem;
    }

    public void setNumberOfItem(int numberOfItem) {
        mNumberOfItem = numberOfItem;
    }

    public Orientation getOrientation() {
        return mOrientation;
    }

    public void setOrientation(Orientation orientation) {
        mOrientation = orientation;
    }
}
