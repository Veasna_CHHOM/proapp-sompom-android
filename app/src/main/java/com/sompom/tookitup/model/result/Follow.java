package com.sompom.tookitup.model.result;

import com.google.gson.annotations.SerializedName;

/**
 * Created by he.rotha on 8/12/16.
 */
public class Follow {
    private static final String FOLLOWING = "following";

    @SerializedName(FOLLOWING)
    private boolean mIsFollow;

    public static String getFOLLOWING() {
        return FOLLOWING;
    }

    public boolean isFollow() {
        return mIsFollow;
    }

    public void setFollow(boolean follow) {
        mIsFollow = follow;
    }
}
