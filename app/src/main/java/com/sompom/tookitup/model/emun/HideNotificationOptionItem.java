package com.sompom.tookitup.model.emun;

import android.support.annotation.StringRes;

import com.sompom.tookitup.R;

/**
 * Created by nuonveyo on 7/5/18.
 */

public enum HideNotificationOptionItem {
    HideNotification(1, R.string.notification_hide_item_title),
    HideAllNotification(2, R.string.notification_hide_all_item_title);

    private int mId;
    @StringRes
    private int mTitle;

    HideNotificationOptionItem(int id, int title) {
        mId = id;
        mTitle = title;
    }

    public int getId() {
        return mId;
    }

    public int getTitle() {
        return mTitle;
    }
}
