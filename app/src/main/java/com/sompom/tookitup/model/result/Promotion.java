package com.sompom.tookitup.model.result;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ChhomVeasna on 10/28/16.
 */

public class Promotion implements Parcelable {
    //CHECKSTYLE:OFF
    public static final Creator<Promotion> CREATOR = new Creator<Promotion>() {
        @Override
        public Promotion createFromParcel(Parcel source) {
            return new Promotion(source);
        }

        @Override
        public Promotion[] newArray(int size) {
            return new Promotion[size];
        }
    };
    @SerializedName("_id")
    private String mId;
    @SerializedName("store")
    private Store mStore;
    @SerializedName("promotion_format")
    private String mPromotionType;
    @SerializedName("action_link")
    private String mActionLink;
    @SerializedName("creative_mobile")
    private String mMobilePicLink;
    @SerializedName("creative_tablet")
    private String mTablePicLink;

    //CHECKSTYLE:OFF
    public Promotion() {
    }

    private Promotion(Parcel in) {
        this.mId = in.readString();
        this.mStore = in.readParcelable(Store.class.getClassLoader());
        this.mPromotionType = in.readString();
        this.mActionLink = in.readString();
        this.mMobilePicLink = in.readString();
        this.mTablePicLink = in.readString();
    }

    public Store getStore() {
        return mStore;
    }

    public void setStore(Store store) {
        mStore = store;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public PromotionType getPromotionType() {
        return PromotionType.fromValue(mPromotionType);
    }

    public void setPromotionType(String promotionType) {
        mPromotionType = promotionType;
    }

    public String getActionLink() {
        return mActionLink;
    }

    public void setActionLink(String actionLink) {
        mActionLink = actionLink;
    }

    public String getMobilePicLink() {
        return mMobilePicLink;
    }

    public void setMobilePicLink(String mobilePicLink) {
        mMobilePicLink = mobilePicLink;
    }

    public String getTablePicLink() {
        return mTablePicLink;
    }

    public void setTablePicLink(String tablePicLink) {
        mTablePicLink = tablePicLink;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeParcelable(this.mStore, flags);
        dest.writeString(this.mPromotionType);
        dest.writeString(this.mActionLink);
        dest.writeString(this.mMobilePicLink);
        dest.writeString(this.mTablePicLink);
    }

    public enum PromotionType {
        FULL_RECTANGLE("FULL_RECTANGLE"),
        HAFT_RECTANGLE("HAFT_RECTANGLE"),
        SQUARE("SQUARE");

        private final String mValue;
        private int mRow;
        private int mCol;

        PromotionType(String value) {
            this.mValue = value;
        }

        public static Promotion.PromotionType fromValue(String value) {
            for (Promotion.PromotionType s : Promotion.PromotionType.values()) {
                if (s.getValue().equals(value)) {
                    return s;
                }
            }

            return FULL_RECTANGLE;
        }

        public String getValue() {
            return mValue;
        }

        public int getRow() {
            if (getValue().equals(FULL_RECTANGLE.getValue())) {
                mRow = 1;
                mCol = 4;
            }
            if (getValue().equals(HAFT_RECTANGLE.getValue())) {
                mRow = 1;
                mCol = 2;
            }
            if (getValue().equals(SQUARE.getValue())) {
                mRow = 2;
                mCol = 2;
            }

            return mRow;
        }

        public void setRow(int row) {
            mRow = row;
        }

        public int getCol() {
            if (getValue().equals(FULL_RECTANGLE.getValue())) {
                mRow = 1;
                mCol = 4;
            }
            if (getValue().equals(HAFT_RECTANGLE.getValue())) {
                mRow = 1;
                mCol = 2;
            }
            if (getValue().equals(SQUARE.getValue())) {
                mRow = 2;
                mCol = 2;
            }

            return mCol;
        }

        public void setCol(int col) {
            mCol = col;
        }
    }

    public static class Store implements Parcelable {
        public static final Creator<Store> CREATOR = new Creator<Store>() {
            @Override
            public Store createFromParcel(Parcel source) {
                return new Store(source);
            }

            @Override
            public Store[] newArray(int size) {
                return new Store[size];
            }
        };
        private String mId;
        private String mFirstName;
        private String mLastName;

        public Store() {
        }

        private Store(Parcel in) {
            this.mId = in.readString();
            this.mFirstName = in.readString();
            this.mLastName = in.readString();
        }

        public String getId() {
            return mId;
        }

        public void setId(String id) {
            this.mId = id;
        }

        public String getFirstName() {
            return mFirstName;
        }

        public void setFirstName(String firstName) {
            this.mFirstName = firstName;
        }

        public String getLastName() {
            return mLastName;
        }

        public void setLastName(String lastName) {
            this.mLastName = lastName;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.mId);
            dest.writeString(this.mFirstName);
            dest.writeString(this.mLastName);
        }
    }
}
