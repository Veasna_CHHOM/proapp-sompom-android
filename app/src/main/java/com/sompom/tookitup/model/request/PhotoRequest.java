package com.sompom.tookitup.model.request;

/**
 * Created by imac on 4/12/16.
 */
public class PhotoRequest {
    //CHECKSTYLE:OFF
    private String url;
    //CHECKSTYLE:OFF

    public PhotoRequest(String photoUrl) {
        url = photoUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

}
