package com.sompom.tookitup.model;

/**
 * Created by He Rotha on 1/28/19.
 */
public class ActiveUserWrapper {
    private ActiveUser mYourSeller;
    private ActiveUser mActiveSeller;

    public ActiveUser getYourSeller() {
        return mYourSeller;
    }

    public void setYourSeller(ActiveUser yourSeller) {
        mYourSeller = yourSeller;
    }

    public ActiveUser getActiveSeller() {
        return mActiveSeller;
    }

    public void setActiveSeller(ActiveUser activeSeller) {
        mActiveSeller = activeSeller;
    }
}
