package com.sompom.tookitup.model.result;

import java.util.List;

/**
 * Created by he.rotha on 6/29/16.
 */
public class ProductBuyingRequest {
    //CHECKSTYLE:OFF
    private List<Product> product;
    private int totalUnreadMessage;
    //CHECKSTYLE:OFF

    public int getTotalUnreadMessage() {
        return totalUnreadMessage;
    }

    public void setTotalUnreadMessage(int totalUnreadMessage) {
        this.totalUnreadMessage = totalUnreadMessage;
    }

    public List<Product> getProduct() {
        return product;
    }

    public void setProduct(List<Product> product) {
        this.product = product;
    }
}
