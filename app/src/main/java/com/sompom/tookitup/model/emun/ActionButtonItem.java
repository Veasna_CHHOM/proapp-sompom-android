package com.sompom.tookitup.model.emun;

import android.support.annotation.StringRes;

import com.sompom.tookitup.R;

/**
 * Created by nuonveyo on 8/1/18.
 */

public enum ActionButtonItem {
    Sell(1,
            R.string.product_detail_sell_button,
            R.string.code_tag),
    Post(2,
            R.string.create_wall_street_post_button,
            R.string.code_play),
    Media(3,
            R.string.product_detail_media_button,
            R.string.code_camera),
    Live(4,
            R.string.product_detail_live_button,
            R.string.code_eye);

    private int mId;
    @StringRes
    private int mTitle;
    @StringRes
    private int mIcon;

    ActionButtonItem(int id, int title, int icon) {
        mId = id;
        mTitle = title;
        mIcon = icon;
    }

    public static ActionButtonItem getValueFromId(int id) {
        for (ActionButtonItem actionButtonItem : ActionButtonItem.values()) {
            if (actionButtonItem.getId() == id) {
                return actionButtonItem;
            }
        }
        return Sell;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getTitle() {
        return mTitle;
    }

    public void setTitle(int title) {
        mTitle = title;
    }

    public int getIcon() {
        return mIcon;
    }

    public void setIcon(int icon) {
        mIcon = icon;
    }
}
