package com.sompom.tookitup.model;

import com.sompom.tookitup.helper.IntentData;
import com.sompom.tookitup.model.emun.ViewType;

/**
 * Created by imac on 8/11/17.
 */

public interface Adaptive {

    String getId();

    ViewType getTimelineViewType();

    void startActivityForResult(IntentData requiredIntentData);
}
