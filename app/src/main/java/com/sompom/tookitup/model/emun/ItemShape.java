package com.sompom.tookitup.model.emun;

/**
 * Created by He Rotha on 8/25/17.
 */
public enum ItemShape {
    SQUARE("SQUARE"),
    LANDSCAPE("LANDSCAPE"),
    PORTRAIT("PORTRAIT");

    private String mStatusProduct;

    ItemShape(String i) {
        mStatusProduct = i;
    }

    public static ItemShape fromValue(String value) {
        for (ItemShape s : ItemShape.values()) {
            if (s.getValue().equals(value)) {
                return s;
            }
        }
        return SQUARE;
    }

    public String getValue() {
        return mStatusProduct;
    }
}
