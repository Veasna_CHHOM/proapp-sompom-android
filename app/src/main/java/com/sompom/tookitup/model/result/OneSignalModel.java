package com.sompom.tookitup.model.result;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class OneSignalModel implements Parcelable {
    public static final Creator<OneSignalModel> CREATOR = new Creator<OneSignalModel>() {
        public OneSignalModel createFromParcel(Parcel in) {
            return new OneSignalModel(in);
        }

        public OneSignalModel[] newArray(int size) {
            return new OneSignalModel[size];
        }
    };

    private static final String FIELD_TYPE = "type";
    private static final String FIELD_MESSAGE = "message";
    private static final String FIELD_PUSH_HASH = "push_hash";
    private static final String FIELD_DATA = "data";
    private static final String FIELD_PARSE_PUSH_ID = "parsePushId";
    private static final String FIELD_ALERT = "alert";

    @SerializedName(FIELD_TYPE)
    private String mType;
    @SerializedName(FIELD_MESSAGE)
    private String mMessage;
    @SerializedName(FIELD_PUSH_HASH)
    private String mPushHash;
    @SerializedName(FIELD_DATA)
    private NotificationData mNotificationData;
    @SerializedName(FIELD_PARSE_PUSH_ID)
    private String mParsePushId;
    @SerializedName(FIELD_ALERT)
    private String mAlert;


    public OneSignalModel() {

    }

    public OneSignalModel(Parcel in) {
        mType = in.readString();
        mMessage = in.readString();
        mPushHash = in.readString();
        mNotificationData = in.readParcelable(NotificationData.class.getClassLoader());
        mParsePushId = in.readString();
//        mAlert = in.readParcelable(Alert.class.getClassLoader());
        mAlert = in.readString();
    }

    public Type getType() {
        return Type.fromValue(mType);
    }

    public void setType(Type type) {
        mType = type.getComment();
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getPushHash() {
        return mPushHash;
    }

    public void setPushHash(String pushHash) {
        mPushHash = pushHash;
    }

    public NotificationData getNotificationData() {
        return mNotificationData;
    }

    public void setNotificationData(NotificationData notificationData) {
        mNotificationData = notificationData;
    }

    public String getParsePushId() {
        return mParsePushId;
    }

    public void setParsePushId(String parsePushId) {
        mParsePushId = parsePushId;
    }

    public String getAlert() {
        return mAlert;
    }

    public void setAlert(String alert) {
        mAlert = alert;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mType);
        dest.writeString(mMessage);
        dest.writeString(mPushHash);
        dest.writeParcelable(mNotificationData, flags);
        dest.writeString(mParsePushId);
        dest.writeString(mAlert);
    }

    @Override
    public String toString() {
        return "type = " + mType + ", message = " +
                mMessage + ", pushHash = " + mPushHash +
                ", datum = " + mNotificationData + ", parsePushId = " + mParsePushId;
    }

    public enum Type {
        COMMENT("comment"),
        COMMENT_LIFE_STREAM("comment_life_stream"),
        BUY("buy"),
        POST("created_item"),
        WARNING("warning"),
        CONVERSATION("conversation"),
        BLOCK("block");

        private String mComment;

        Type(String comment) {
            mComment = comment;
        }

        public static Type fromValue(String value) {
            for (Type type : Type.values()) {
                if (type.getComment().equalsIgnoreCase(value)) {
                    return type;
                }
            }
            return Type.COMMENT;
        }

        public String getComment() {
            return mComment;
        }
    }
}