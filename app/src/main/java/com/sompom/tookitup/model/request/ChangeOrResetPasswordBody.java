package com.sompom.tookitup.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ChhomVeasna on 10/31/17.
 */

public class ChangeOrResetPasswordBody {

    @SerializedName("code")
    private String mCode;

    @SerializedName("password")
    private String mPwd;

    @SerializedName("passwordConfirmation")
    private String mConfirmPwd;

    public ChangeOrResetPasswordBody(String code, String pwd, String confirmPwd) {
        mCode = code;
        mPwd = pwd;
        mConfirmPwd = confirmPwd;
    }

    public ChangeOrResetPasswordBody(String pwd, String confirmPwd) {
        mPwd = pwd;
        mConfirmPwd = confirmPwd;
    }
}
