package com.sompom.tookitup.model.emun;

import android.text.TextUtils;

public enum DeeplinkType {
    None(""),
    Product("product"),
    Feed("post"),
    User("user");

    private String mType;

    DeeplinkType(String type) {
        mType = type;
    }

    public static DeeplinkType from(String value) {
        for (DeeplinkType deeplinkType : DeeplinkType.values()) {
            if (TextUtils.equals(value, deeplinkType.mType)) {
                return deeplinkType;
            }
        }
        return None;
    }

    public String getType() {
        return mType;
    }
}
