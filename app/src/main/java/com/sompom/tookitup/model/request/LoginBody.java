package com.sompom.tookitup.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by He Rotha on 10/20/17.
 */

public class LoginBody {

    @SerializedName("identifier")
    private String mIdentifier;

    @SerializedName("password")
    private String mPassword;

    public LoginBody(String identifier, String password) {
        mIdentifier = identifier;
        mPassword = password;
    }
}
