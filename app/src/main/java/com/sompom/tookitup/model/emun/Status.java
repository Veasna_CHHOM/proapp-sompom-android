package com.sompom.tookitup.model.emun;

/**
 * Created by He Rotha on 8/25/17.
 */
public enum Status {
    SOLD(0), ON_SALE(1), ALL(2);
    private int mStatusProduct;

    Status(int i) {
        mStatusProduct = i;
    }

    public static Status fromValue(Integer value) {
        if (value == null) {
            return ON_SALE;
        }
        for (Status s : Status.values()) {
            if (s.getStatusProduct() == value) {
                return s;
            }
        }
        return ON_SALE;
    }

    public static Status getValueFromId(int id) {
        for (Status status : Status.values()) {
            if (status.getStatusProduct() == id) {
                return status;
            }
        }
        return SOLD;
    }

    public int getStatusProduct() {
        return mStatusProduct;
    }
}
