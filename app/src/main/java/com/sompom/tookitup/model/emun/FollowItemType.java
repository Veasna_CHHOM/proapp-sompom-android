package com.sompom.tookitup.model.emun;

/**
 * Created by nuonveyo on 4/30/18.
 */

public enum FollowItemType {
    FOLLOWING,
    FOLLOWER,
    FOLLOWING_AND_FOLLOWER,
    USER_ACTION,
    LIKE_VIEWER,


}
