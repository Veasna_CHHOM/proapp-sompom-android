package com.sompom.tookitup.model.emun;

/**
 * Created by He Rotha on 3/23/18.
 */
public enum AdsUnit {
    STREET_WALL("/71197753/streetwall", "11837400");

    private String mAdsUnitID;
    private String mTemplateID;

    AdsUnit(String adsUnit, String templateID) {
        mAdsUnitID = adsUnit;
        mTemplateID = templateID;
    }

    public String getAdsUnitID() {
        return mAdsUnitID;
    }

    public String getTemplateID() {
        return mTemplateID;
    }
}
