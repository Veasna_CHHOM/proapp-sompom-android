package com.sompom.tookitup.model.emun;

import android.support.annotation.DrawableRes;

import com.sompom.tookitup.R;

/**
 * Created by nuonveyo on 10/18/18.
 */

public enum PublishItem {
    Follower(1, R.drawable.ic_user_small),
    EveryOne(2, R.drawable.ic_earth_grid_12dp),
    OnlyMe(3, R.drawable.ic_earth_grid_12dp);

    private int mId;
    @DrawableRes
    private int mIcon;

    PublishItem(int id, int icon) {
        mId = id;
        mIcon = icon;
    }

    public static PublishItem getItem(int id) {
        for (PublishItem publishItem : PublishItem.values()) {
            if (id == publishItem.getId()) {
                return publishItem;
            }
        }
        return Follower;
    }

    public int getId() {
        return mId;
    }

    public int getIcon() {
        return mIcon;
    }
}
