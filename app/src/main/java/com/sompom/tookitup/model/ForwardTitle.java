package com.sompom.tookitup.model;

import com.sompom.tookitup.listener.ConversationDataAdaptive;

/**
 * Created by He Rotha on 9/13/18.
 */
public class ForwardTitle implements ConversationDataAdaptive {
    private int mTitle;

    public ForwardTitle(int title) {
        mTitle = title;
    }

    public int getTitle() {
        return mTitle;
    }

    public void setTitle(int title) {
        mTitle = title;
    }

    @Override
    public String getId() {
        return null;
    }
}
