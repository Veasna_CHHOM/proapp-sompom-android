package com.sompom.tookitup.viewmodel;

import android.databinding.ObservableField;
import android.text.TextWatcher;

import com.sompom.tookitup.helper.MyTextWatcher;
import com.sompom.tookitup.listener.OnCompleteListener;

/**
 * Created by nuonveyo on 5/7/18.
 */

public class ListItemEditMediaFileViewModel extends AbsBaseViewModel {
    public ObservableField<String> mContent = new ObservableField<>();
    private OnCompleteListener<String> mOnCompleteListener;

    public ListItemEditMediaFileViewModel(String title, OnCompleteListener<String> listener) {
        mContent.set(title);
        mOnCompleteListener = listener;
    }

    public TextWatcher onTextChanged() {
        return new MyTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s) {
                if (mOnCompleteListener != null) {
                    mOnCompleteListener.onComplete(s.toString());
                }
            }
        };
    }
}
