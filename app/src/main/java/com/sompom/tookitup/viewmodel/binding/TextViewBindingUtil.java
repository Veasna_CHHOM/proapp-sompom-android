package com.sompom.tookitup.viewmodel.binding;

import android.content.Context;
import android.content.Intent;
import android.databinding.BindingAdapter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;

import com.desmond.squarecamera.model.AppTheme;
import com.sompom.tookitup.R;
import com.sompom.tookitup.helper.AnimationHelper;
import com.sompom.tookitup.helper.LongClickLinkMovementMethod;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.listener.OnItemClickListener;
import com.sompom.tookitup.listener.OnSpannableClickListener;
import com.sompom.tookitup.model.emun.Range;
import com.sompom.tookitup.model.result.MoreGame;
import com.sompom.tookitup.model.result.NotificationData;
import com.sompom.tookitup.model.result.OneSignalModel;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.utils.AttributeConverter;
import com.sompom.tookitup.utils.CurrencyUtils;
import com.sompom.tookitup.utils.FormatNumber;
import com.sompom.tookitup.utils.SpannableUtil;
import com.sompom.tookitup.widget.ImageProfileLayout;
import com.sompom.tookitup.widget.WallStreetDescriptionTextView;

import timber.log.Timber;

/**
 * Created by nuonveyo on 4/24/18.
 */

public final class TextViewBindingUtil {
    private TextViewBindingUtil() {
    }

    @BindingAdapter(value = {"setProductPrice", "isFormatPrice"}, requireAll = false)
    public static void setProductImage(TextView textView, Product product, boolean isFormatPrice) {
        if (product == null || TextUtils.isEmpty(product.getCurrency())) {
            return;
        }
        String price;
        if (isFormatPrice) {
            price = CurrencyUtils.formatPrice(product.getPrice(), product.getCurrency());
        } else {
            price = CurrencyUtils.formatMoney(product.getPrice(), product.getCurrency());
        }
        textView.setText(price);
    }

    @BindingAdapter("setProductFullPrice")
    public static void setProductFullPrice(TextView textView, Product product) {
        if (product == null || TextUtils.isEmpty(product.getCurrency())) {
            return;
        }
        String price = CurrencyUtils.formatFullCurrency(product.getPrice(), product.getCurrency());
        textView.setText(price);
    }

    @BindingAdapter("android:background")
    public static void background(TextView textView, int product) {
        textView.setBackgroundResource(product);
    }

    @BindingAdapter("android:text")
    public static void setProductImage(TextView textView, int product) {
        try {
            textView.setText(product);
        } catch (Exception ex) {
            Timber.e("setProductImage: %s", ex.getMessage());
        }
    }

    @BindingAdapter("setUserName")
    public static void getFullName(TextView textView, User user) {
        if (user == null) {
            return;
        }
        textView.setText(user.getFullName());
    }

    @BindingAdapter("setUserPosition")
    public static void setUserPosition(TextView textView, User user) {
        if (user == null) {
            return;
        }
        textView.setVisibility(TextUtils.isEmpty(user.getUserPosition()) ? View.GONE : View.VISIBLE);
        textView.setText(user.getUserPosition());
    }

    @BindingAdapter("isChangeCustomTabFont")
    public static void isChangeCustomTabFont(TextView textView, boolean isChange) {
        Typeface typeface = ResourcesCompat.getFont(textView.getContext(), R.font.sf_pro_regular);
        if (isChange) {
            typeface = ResourcesCompat.getFont(textView.getContext(), R.font.sf_pro_semibold);
        }
        textView.setTypeface(typeface);
    }


    @BindingAdapter("setTextViewLabel")
    public static void setTextViewLabel(final TextView mTextView, final MoreGame moreGame) {
        mTextView.setText(moreGame.getSubtitle());
        mTextView.setOnClickListener(v -> {
            Context context = mTextView.getContext();
            Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse(moreGame.getLinkAndroid()));
            context.startActivity(browserIntent);
        });
    }


    @BindingAdapter("setFacebookButtonText")
    public static void setFacebookButtonText(TextView textSpannable, String value) {
        if (TextUtils.isEmpty(value)) {
            return;
        }
        String boldText = textSpannable.getContext().getString(R.string.seller_store_authenticated_by_facebook_only_height_light);
        String boldTextPhone = textSpannable.getContext().getString(R.string.seller_store_authenticated_by_phone_only_height_light);
        Spannable spannable = SpannableUtil.setFacebookButtonText(textSpannable.getContext(), value, boldText, boldTextPhone);
        textSpannable.setText(spannable);
    }

    @BindingAdapter(value = {"setNotificationDescriptionText", "setOnClickListener"})
    public static void setNotificationDescriptionText(TextView textView, String text, OnSpannableClickListener listener) {
        if (TextUtils.isEmpty(text)) {
            return;
        }
        Spannable spannable = SpannableUtil.setNotificationDescriptionText(textView.getContext(), text, listener);
        textView.setText(spannable);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }


    @BindingAdapter("setStoreName")
    public static void setStoreName(TextView textView, User user) {
        if (user == null) {
            return;
        }

        if (!TextUtils.isEmpty(user.getStoreName()))
            textView.setText(user.getStoreName());
        else {
            String userName = user.getFirstName() + " "
                    + textView.getContext().getString(R.string.store);
            textView.setText(userName);
        }
    }

    @BindingAdapter("android:text")
    public static void setUserNotificationName(TextView textView, NotificationData notificationModel) {
        Context context = textView.getContext();
        String fullName = getFullNameFromNotification(notificationModel);
        String description = "";
        if (notificationModel != null) {
            OneSignalModel.Type type = notificationModel.getType();
            if (type == OneSignalModel.Type.POST) {
                description = context.getString(R.string.post_new_product_notification, fullName);
            } else if (type == OneSignalModel.Type.BUY) {
                if (notificationModel.getProduct() != null) {
                    description = context.getString(R.string.product_request_notification,
                            fullName, notificationModel.getProduct().getName());
                }
            } else if (type == OneSignalModel.Type.COMMENT) {
                if (notificationModel.getProduct() != null) {
                    description = context.getString(R.string.comment_notification,
                            fullName, notificationModel.getProduct().getName());
                }
            } else if (type == OneSignalModel.Type.COMMENT_LIFE_STREAM) {
                if (notificationModel.getLifeStream() != null) {
                    description = context.getString(R.string.comment_notification,
                            fullName, notificationModel.getLifeStream().getTitle());
                }
            } else if (type == OneSignalModel.Type.WARNING) {
                description = context.getString(R.string.warning_notification);
            } else if (type == OneSignalModel.Type.BLOCK) {
                description = context.getString(R.string.block_notification);
            }
        }

        Spannable spannable = SpannableUtil.getNotificationName(textView.getContext(),
                fullName,
                description);
        textView.setText(spannable);
    }

    private static String getFullNameFromNotification(NotificationData notificationData) {
        if (notificationData != null && notificationData.getUser() != null) {
            return notificationData.getUser().getFullName();
        } else {
            return "";
        }
    }

    @BindingAdapter({"likes"})
    public static void setTimelineLikeCount(TextView textView, long value) {
        if (value == 0) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
            String likeResource = FormatNumber.format(value) + " ";
            if (value == 1) {
                likeResource += textView.getContext().getString(R.string.product_detail_button_like);
            } else {
                likeResource += textView.getContext().getString(R.string.home_button_likes);
            }
            textView.setText(likeResource);
        }
    }

    @BindingAdapter({"shares"})
    public static void setTimelineShareCount(TextView textView, long value) {
        if (value == 0) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
            String likeResource = FormatNumber.format(value) + " ";
            if (value == 1) {
                likeResource += textView.getContext().getString(R.string.product_detail_button_share);
            } else {
                likeResource += textView.getContext().getString(R.string.home_button_shares);
            }
            textView.setText(likeResource);
        }
    }

    @BindingAdapter({"comments"})
    public static void setTimelineCommentCount(TextView textView, long value) {
        if (value == 0) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
            String likeResource = FormatNumber.format(value) + " ";
            if (value == 1) {
                likeResource += textView.getContext().getString(R.string.home_button_comment);
            } else {
                likeResource += textView.getContext().getString(R.string.product_detail_button_comments);
            }
            textView.setText(likeResource);
        }
    }

    @BindingAdapter(value = {"setUserNameTimeline",
            "shareTextTitle",
            "adsTitle",
            "renderType"}, requireAll = false)
    public static void setUserNameTimeline(TextView textView,
                                           User user,
                                           String shareTextTitle,
                                           String adsTitle,
                                           ImageProfileLayout.RenderType renderType) {
        if (user != null) {
            if (TextUtils.isEmpty(shareTextTitle)) {
                textView.setText(SpannableUtil.getUserNameSharedTimeline(textView.getContext(), user, renderType));
            } else {
                textView.setText(SpannableUtil.getUserNameSharedTimeline(textView.getContext(), user, shareTextTitle, renderType));
            }
        } else if (!TextUtils.isEmpty(adsTitle)) {
            textView.setText(adsTitle);
        }
    }

    @BindingAdapter("setSpannable")
    public static void setSpannable(TextView textView, Spannable spannable) {
        textView.setText(spannable);
        textView.setMovementMethod(LongClickLinkMovementMethod.getInstance());
    }

    @BindingAdapter(value = {"setChatContent", "isMe", "onChatContentClickListener"}, requireAll = false)
    public static void setChatContent(TextView textView, String content, boolean isMe, OnItemClickListener<Boolean> onClickListener) {
        if (!TextUtils.isEmpty(content)) {
            int color = isMe ? Color.WHITE : AttributeConverter.convertAttrToColor(textView.getContext(), R.attr.colorFirstText);
            Spannable spannable = SpannableUtil.getTextLink(textView.getContext(),
                    content,
                    color,
                    () -> {
                        if (onClickListener != null) onClickListener.onClick(true);
                    });
            textView.setOnClickListener(v -> {
                if (onClickListener != null) onClickListener.onClick(false);
            });
            textView.setText(spannable);
            textView.setMovementMethod(LongClickLinkMovementMethod.getInstance());
        }
    }

    @BindingAdapter("changeFontIsMessageRead")
    public static void changeFontIsMessageRead(TextView textView, boolean isRead) {
        Timber.e("changeFontIsMessageRead " + isRead);
        Typeface typeface = ResourcesCompat.getFont(textView.getContext(), R.font.sf_pro_semibold);
        if (isRead) {
            typeface = ResourcesCompat.getFont(textView.getContext(), R.font.sf_pro_regular);
        }
        textView.setTypeface(typeface);
    }

    @BindingAdapter("chatIconAnimation")
    public static void chatIconAnimation(View view, boolean isStartAnimation) {
        if (isStartAnimation) {
            AnimationHelper.translationToX(view, null);
        } else {
            AnimationHelper.translationFromX(view, null);
        }
    }

    @BindingAdapter("updateSendButton")
    public static void updateSendButton(View view, boolean isSent) {
        if (isSent) {
            view.setEnabled(false);
            view.setBackgroundResource(R.drawable.round_corner_grey);
        } else {
            view.setEnabled(true);
            view.setBackgroundResource(R.drawable.green_round_corner);
        }
    }

    @BindingAdapter("updateSendText")
    public static void updateSendButton(TextView view, boolean isSent) {
        if (isSent) {
            view.setText(R.string.forward_sent_button);
            view.setTextColor(ContextCompat.getColor(view.getContext(), R.color.colorAccent));
        } else {
            view.setText(R.string.forward_send_button);
            view.setTextColor(Color.WHITE);
        }
    }

    @BindingAdapter("setDialogUserStoreName")
    public static void setDialogUserStoreName(TextView textView, User user) {
        if (user == null) return;
        textView.setText(SpannableUtil.getUserStoreName(textView.getContext(), user));
    }

    @BindingAdapter("android:text")
    public static void text(TextView textView, AppTheme theme) {
        if (theme == null) return;
        textView.setText(theme.getStringRes());
    }

    @BindingAdapter({"setForwardLinkText", "isAudio"})
    public static void setForwardLinkText(TextView textView, String linkText, boolean isAudio) {
        Context context = textView.getContext();
        if (!TextUtils.isEmpty(linkText) || isAudio) {
            Typeface typefaceBold = ResourcesCompat.getFont(context, R.font.sf_pro_regular);
            int color = AttributeConverter.convertAttrToColor(context, R.attr.colorSecondText);
            String text = linkText;

            if (isAudio) {
                typefaceBold = ResourcesCompat.getFont(context, R.font.sf_pro_semibold);
                color = AttributeConverter.convertAttrToColor(context, R.attr.colorFirstText);
                text = context.getString(R.string.forward_audio_recorded_title);
                Drawable drawable = AttributeConverter.convertAttrToDrawable(context, R.attr.audioIcon);
                textView.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
                textView.setCompoundDrawablePadding(context.getResources().getDimensionPixelSize(R.dimen.space_too_small));
            }

            textView.setText(text);
            textView.setTypeface(typefaceBold);
            textView.setTextColor(color);
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
        }
    }

    @BindingAdapter(value = {"setWallStreetDescription", "setMaxLine", "setOnTextViewClickListener"}, requireAll = false)
    public static void setWallStreetDescription(WallStreetDescriptionTextView textView,
                                                String text,
                                                boolean isSetMaxLine,
                                                OnClickListener onClickListener) {
        if (TextUtils.isEmpty(text)) return;
        textView.setDescription(text, isSetMaxLine, onClickListener);
    }

    @BindingAdapter("buttonLockEnable")
    public static void buttonLockEnable(TextView textView, boolean isEnable) {
        if (isEnable) {
            textView.setText(R.string.edit_profile_change_button);
            textView.setTextColor(Color.WHITE);
            textView.setBackgroundResource(R.drawable.button_green_selector);
        } else {
            textView.setText(R.string.edit_profile_locked_button);
            textView.setTextColor(ContextCompat.getColor(textView.getContext(), R.color.darkGrey));
            textView.setBackgroundResource(R.drawable.round_outline_grey);
        }
    }

    @BindingAdapter(value = {"setStoreNameText"})
    public static void setStoreNameText(TextView textView, String text) {
        textView.setText(SpannableUtil.getStoreName(textView.getContext(), text));
    }

    @BindingAdapter("setTextSpannable")
    public static void setTextSpannable(TextView textView, Spannable spannable) {
        textView.setText(spannable);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @BindingAdapter("slidePrice")
    public static void slidePrice(TextView textView, Range range) {
        textView.setText(CurrencyUtils.formatPrice(range.getValue()));
    }

}
