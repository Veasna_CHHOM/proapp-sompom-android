package com.sompom.tookitup.viewmodel.binding;

import android.databinding.BindingAdapter;
import android.text.TextUtils;
import android.text.TextWatcher;

import com.example.usermentionable.model.UserMentionable;
import com.example.usermentionable.tokenization.interfaces.QueryTokenReceiver;
import com.example.usermentionable.widget.KeyboardInputEditor;
import com.example.usermentionable.widget.RichEditorView;

import java.util.List;

/**
 * Created by nuonveyo on 12/14/18.
 */

public final class RichEditorViewBindingUtil {
    private RichEditorViewBindingUtil() {

    }

    @BindingAdapter({"setRichEditorView", "setRichEditorTextWatcher", "setRichEditorKeyboardInputListener", "setRichEditorQueryTokenListener"})
    public static void bindView(RichEditorView richEditorView,
                                String text,
                                TextWatcher textWatcher,
                                KeyboardInputEditor.KeyBoardInputCallbackListener keyBoardInputCallbackListener,
                                QueryTokenReceiver queryTokenReceiver) {
        richEditorView.setText(text);
        richEditorView.setOnKeyboardInputCallback(keyBoardInputCallbackListener);
        richEditorView.setQueryTokenReceiver(queryTokenReceiver);
        richEditorView.addOnTextChangeListener(textWatcher);
    }

    @BindingAdapter({"setRichEditorViewList", "isShowListPopupWindow"})
    public static void setRichEditorViewList(RichEditorView richEditorView, List<UserMentionable> mentionableList, boolean isShowListPopupWindow) {
        richEditorView.setListPopupWindow(isShowListPopupWindow, mentionableList);
    }

    @BindingAdapter("setRenderHashTag")
    public static void setRichEditorViewList(RichEditorView richEditorView, String tokenString) {
        if (!TextUtils.isEmpty(tokenString)) {
            richEditorView.checkToRenderHashTag(tokenString);
        }
    }
}
