package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.view.View;

import com.example.usermentionable.widget.KeyboardInputEditor;
import com.sompom.baseactivity.ResultCallback;
import com.sompom.tookitup.R;
import com.sompom.tookitup.chat.listener.OnPresenceListener;
import com.sompom.tookitup.chat.service.SocketService;
import com.sompom.tookitup.helper.LoginActivityResultCallback;
import com.sompom.tookitup.helper.NavigateSellerStoreHelper;
import com.sompom.tookitup.helper.SaveImageHelper;
import com.sompom.tookitup.intent.CallingIntent;
import com.sompom.tookitup.intent.newintent.MyCameraIntent;
import com.sompom.tookitup.intent.newintent.MyCameraResultIntent;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.model.BaseChatModel;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.emun.MediaType;
import com.sompom.tookitup.model.emun.Presence;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.BaseObserverHelper;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.datamanager.ChatDataManager;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by nuonveyo on 6/25/18.
 */

public class ChatDialogViewModel extends AbsLoadingViewModel {

    private ObservableInt mCallVisibility = new ObservableInt(View.GONE);
    public final ObservableBoolean mIsRecording = new ObservableBoolean();
    public final ObservableBoolean mIsAddIconAnimation = new ObservableBoolean();
    public final ObservableField<String> mContent = new ObservableField<>();
    public final ObservableField<String> mLastActivity = new ObservableField<>();
    public final ObservableInt mVisibleActive = new ObservableInt(View.GONE);
    public String mDisplayName;
    public final User mRecipient;
    private final Product mProduct;
    private final String mConversationId;
    private final ChatDataManager mChatDataManager;
    private final OnCallback mCallbackListener;
    private final SaveImageHelper mSaveImageHelper;
    private final SocketService.SocketBinder mSocketBinder;
    private Conversation mConversation;
    private final OnPresenceListener mOnPresenceListener = new OnPresenceListener() {
        @Override
        public void onPresenceUpdate(String userId, Presence presence, Date lastOnlineTime) {
            if (lastOnlineTime == null || presence == Presence.Online) {
                mVisibleActive.set(View.GONE);
            } else {
                mVisibleActive.set(View.VISIBLE);
                //TODO: this is temporary format of relevant time, need to discuss and verify it
                final CharSequence date = DateUtils.getRelativeTimeSpanString(lastOnlineTime.getTime(),
                        System.currentTimeMillis(),
                        60 * 1000, //min 1 min
                        DateUtils.FORMAT_ABBREV_ALL);
                mLastActivity.set(date.toString());
            }
        }

        @Override
        public String getUserId() {
            return mRecipient.getId();
        }
    };

    public ChatDialogViewModel(ChatDataManager dataManager,
                               SocketService.SocketBinder binder,
                               Conversation conversation,
                               String conversationId,
                               User recipient,
                               Product product,
                               OnCallback callbackListener) {
        super(dataManager.getContext());
        mConversation = conversation;
        mSaveImageHelper = new SaveImageHelper(getContext());
        mChatDataManager = dataManager;
        mConversationId = conversationId;
        mRecipient = recipient;
        mProduct = product;
        mCallbackListener = callbackListener;
        mSocketBinder = binder;
        getData();
        mSocketBinder.addPresenceListener(mOnPresenceListener);
        //Bind display name
        if (isGroupChat()) {
            mDisplayName = mConversation.getGroupName();
        } else {
            mDisplayName = mRecipient.getFullName();
        }
        mCallVisibility.set(isGroupChat() ? View.GONE : View.VISIBLE);
    }

    public ObservableInt getCallVisibility() {
        return mCallVisibility;
    }

    private boolean isGroupChat() {
        return mConversation != null && !TextUtils.isEmpty(mConversation.getGroupId());
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mSocketBinder.removePresenceListener(mOnPresenceListener);
    }

    public void onCall() {
        Context context = mChatDataManager.mContext;
        context.startActivity(new CallingIntent(context, mRecipient, mProduct));
    }

    public void onProfileClick() {
        if (TextUtils.isEmpty(mConversation.getGroupId())) {
            //Old code
//        getContext().startActivity(new SellerStoreIntent(getContext(), mRecipient.getId()));
            NavigateSellerStoreHelper.openSellerStore((AppCompatActivity) mChatDataManager.getContext(),
                    mRecipient.getId());
        }

        //TODO: Else need to mange group profile dialog after.
    }

    public TextWatcher onTextChange() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s)) {
                    mCallbackListener.onStartTyping();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(s)) {
                    if (!mIsAddIconAnimation.get()) {
                        mIsAddIconAnimation.set(true);
                        if (mCallbackListener != null) {
                            mCallbackListener.onReplaceSendIcon();
                        }
                    }
                } else {
                    if (mIsAddIconAnimation.get()) {
                        mIsAddIconAnimation.set(false);
                        if (mCallbackListener != null) {
                            mCallbackListener.onReplaceRecordIcon();
                        }
                    }
                }
            }
        };
    }

    private void getData() {
        mIsLoading.set(true);
        getChatHistory();
    }

    private void getChatHistory() {
        Observable<List<BaseChatModel>> callback = mChatDataManager.getChatList(mConversationId);
        BaseObserverHelper<List<BaseChatModel>> helper = new BaseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<List<BaseChatModel>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mIsLoading.set(false);
                showSnackBar(R.string.chat_not_available_now);
                if (mCallbackListener != null) {
                    mCallbackListener.onFail(ex);
                }
            }

            @Override
            public void onComplete(List<BaseChatModel> value) {
                mIsLoading.set(false);
                if (mCallbackListener != null) {
                    mCallbackListener.onComplete(value, mChatDataManager.isCanLoadMore());
                }
            }


        }));
    }

    public void loadMoreData(OnLoadMoreCallback listener) {
        Observable<List<BaseChatModel>> callback = mChatDataManager.loadMore(listener.getLastChat());
        BaseObserverHelper<List<BaseChatModel>> helper = new BaseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<List<BaseChatModel>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                listener.onFail(ex);
            }

            @Override
            public void onComplete(List<BaseChatModel> value) {
                listener.onComplete(value, mChatDataManager.isCanLoadMore());
            }
        }));
    }

    public void onSendButtonClick() {

        if (TextUtils.isEmpty(mConversationId)) {
            showSnackBar(R.string.chat_not_available_now);
            return;
        }

        final String content = mContent.get();
        if (!TextUtils.isEmpty(content)) {
            if (TextUtils.isEmpty(content.trim())) {
                return;
            }

            if (getContext() instanceof AbsBaseActivity) {
                ((AbsBaseActivity) getContext()).startLoginWizardActivity(new LoginActivityResultCallback() {
                    @Override
                    public void onActivityResultSuccess(boolean isAlreadyLogin) {
                        if (mConversation != null && !TextUtils.isEmpty(mConversation.getGroupId())) {
                            mContent.set("");
                            mIsAddIconAnimation.set(false);
                            if (mCallbackListener != null) {
                                mCallbackListener.onSendButtonClick(content.trim(), mConversationId);
                            }
                        } else {
                            Chat chat = Chat.getNewChat(getContext(), mRecipient, mProduct);
                            chat.setChannelId(mConversationId);
                            chat.setContent(content.trim());
                            mContent.set("");
                            mIsAddIconAnimation.set(false);
                            if (mCallbackListener != null) {
                                mCallbackListener.onSendButtonClick(chat);
                            }
                        }
                    }
                });
            }
        }
    }

    public void onGalleryClick() {
        if (getContext() instanceof AbsBaseActivity) {
            Intent intent = MyCameraIntent.newChatGalleryInstance(getContext());
            ((AbsBaseActivity) getContext()).startActivityForResult(intent, new ResultCallback() {

                @Override
                public void onActivityResultSuccess(int resultCode, Intent data) {
                    MyCameraResultIntent resultIntent = new MyCameraResultIntent(data);
                    ArrayList<Media> media = resultIntent.getMedia();

                    Chat chat = Chat.getNewChat(getContext(), mRecipient, mProduct);
                    chat.setChannelId(mConversationId);
                    chat.setMediaList(media);

                    if (mCallbackListener != null) {
                        mCallbackListener.onSendButtonClick(chat);
                    }
                }
            });
        }
    }

    public void onCameraClick() {
        if (getContext() instanceof AbsBaseActivity) {
            Intent intent = MyCameraIntent.newChatCameraInstance(getContext());

            ((AbsBaseActivity) getContext()).startActivityForResult(intent, new ResultCallback() {

                @Override
                public void onActivityResultSuccess(int resultCode, Intent data) {
                    MyCameraResultIntent resultIntent = new MyCameraResultIntent(data);
                    ArrayList<Media> media = resultIntent.getMedia();

                    Chat chat = Chat.getNewChat(getContext(), mRecipient, mProduct);
                    chat.setChannelId(mConversationId);
                    chat.setMediaList(media);

                    if (mCallbackListener != null) {
                        mCallbackListener.onSendButtonClick(chat);
                    }
                }
            });
        }
    }

    public void saveImageIntoLocal(List<Media> medias) {
        if (getContext() != null) {
            mSaveImageHelper.startSave(medias);
        }
    }

    public KeyboardInputEditor.KeyBoardInputCallbackListener onKeyBoardInputCallbackListener() {
        return (inputContentInfo, flags, opts) -> {
            Uri uri = inputContentInfo.getLinkUri();
            if (uri != null) {
                ArrayList<Media> mediaArrayList = new ArrayList<>();
                Media media = new Media();
                media.setUrl(String.valueOf(uri));
                media.setType(MediaType.IMAGE);
                mediaArrayList.add(media);

                Chat chat = Chat.getNewChat(getContext(), mRecipient, mProduct);
                chat.setChannelId(mConversationId);
                chat.setMediaList(mediaArrayList);

                if (mCallbackListener != null) {
                    mCallbackListener.onSendButtonClick(chat);
                }
            }
        };
    }

    public interface OnCallback extends OnCallbackListListener<List<BaseChatModel>> {
        void onSendButtonClick(Chat chat);

        void onStartTyping();

        void onReplaceRecordIcon();

        void onReplaceSendIcon();

        void onSendButtonClick(String content, String conversationId);
    }

    public interface OnLoadMoreCallback extends OnCallbackListListener<List<BaseChatModel>> {
        Chat getLastChat();
    }

}
