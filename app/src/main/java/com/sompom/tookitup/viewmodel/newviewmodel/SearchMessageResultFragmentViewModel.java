package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.ObservableBoolean;
import android.text.TextUtils;

import com.sompom.tookitup.broadcast.NetworkBroadcastReceiver;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.model.SearchGeneralTypeResult;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.SearchGeneralTypeResultDataManager;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 7/2/18.
 */

public class SearchMessageResultFragmentViewModel extends AbsLoadingViewModel {
    public final ObservableBoolean mIsRefresh = new ObservableBoolean();
    private final SearchGeneralTypeResultDataManager mDataManager;
    private OnCallbackListListener<SearchGeneralTypeResult> mOnCallback;
    private String mSearchText;
    private NetworkBroadcastReceiver.NetworkListener mNetworkListener;
    private boolean mIsLoadedData;
    private boolean mIsRefreshData;

    public SearchMessageResultFragmentViewModel(SearchGeneralTypeResultDataManager dataManager,
                                                OnCallbackListListener<SearchGeneralTypeResult> listener) {
        super(dataManager.getContext());
        setShowKeyboardWhileLoadingScreen();
        mDataManager = dataManager;
        mOnCallback = listener;
        showLoading();

        if (getContext() instanceof AbsBaseActivity) {
            mNetworkListener = networkState -> {
                if (!mIsLoadedData) {
                    if (mIsRefreshData) {
                        if (networkState == NetworkBroadcastReceiver.NetworkState.Connected) {
                            getSearchData(mSearchText);
                        }
                    } else if (networkState == NetworkBroadcastReceiver.NetworkState.Disconnected) {
                        showNetworkError();
                    }
                }
            };
            ((AbsBaseActivity) getContext()).addNetworkStateChangeListener(mNetworkListener);
        }
    }

    public SearchMessageResultFragmentViewModel(SearchGeneralTypeResultDataManager dataManager) {
        super(dataManager.getContext());
        mDataManager = dataManager;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getContext() instanceof AbsBaseActivity && mNetworkListener != null) {
            ((AbsBaseActivity) getContext()).removeStateChangeListener(mNetworkListener);
        }
    }

    public void onRefresh() {
        mIsRefresh.set(true);
        getSearchData(mSearchText);
    }

    public void getData(String searchText) {
        if (!TextUtils.equals(searchText, mSearchText)) {
            mIsRefreshData = true;
            getSearchData(searchText);
        }
    }

    public void onFollowClick(User user, boolean isFollow) {
        Observable<Response<Object>> callback;
        if (isFollow) {
            callback = mDataManager.postFollow(user.getId());
        } else {
            callback = mDataManager.unFollow(user.getId());
        }
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute());
    }

    private void getSearchData(String searchText) {
        if (!mIsRefresh.get()) {
            showLoading();
        }
        mSearchText = searchText;
        Observable<Response<SearchGeneralTypeResult>> callback = mDataManager.searchGeneralByType(mSearchText);
        ResponseObserverHelper<Response<SearchGeneralTypeResult>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<SearchGeneralTypeResult>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (mIsLoadedData) {
                    mIsRefresh.set(false);
                    return;
                }
                hideLoading();
                showError(ex.toString());
                mIsLoadedData = false;
                mIsRefresh.set(false);
            }

            @Override
            public void onComplete(Response<SearchGeneralTypeResult> result) {
                hideLoading();
                mIsRefresh.set(false);
                mIsLoadedData = true;
                mOnCallback.onComplete(result.body(), mDataManager.isCanLoadMore());
            }
        }));
    }

    public OnClickListener onRetryButtonClick() {
        return () -> getSearchData(mSearchText);
    }
}
