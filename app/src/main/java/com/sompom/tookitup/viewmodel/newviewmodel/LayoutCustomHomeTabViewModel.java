package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.support.annotation.StringRes;

import com.sompom.tookitup.viewmodel.AbsBaseViewModel;

/**
 * Created by nuonveyo on 6/22/18.
 */

public class LayoutCustomHomeTabViewModel extends AbsBaseViewModel {
    private static final int LIMIT_DISPLAY_VALUE = 999;
    public final ObservableBoolean mIsChange = new ObservableBoolean();
    public final ObservableField<String> mIcon = new ObservableField<>();
    public final ObservableField<String> mTitle = new ObservableField<>();
    public final ObservableField<String> mBadgeValue = new ObservableField<>();
    private long mSaveBadge;

    public LayoutCustomHomeTabViewModel(Context context,
                                        @StringRes int title,
                                        @StringRes int icon) {
        mTitle.set(context.getString(title));
        mIcon.set(context.getString(icon));
    }

    public void changeStyle(boolean isChange) {
        mIsChange.set(isChange);
    }

    public void setBadgeValue(long badgeValue) {
        mSaveBadge = badgeValue;
        if (mSaveBadge > 0) {
            if (mSaveBadge > LIMIT_DISPLAY_VALUE) {
                mBadgeValue.set(String.valueOf(LIMIT_DISPLAY_VALUE + "+"));
            } else {
                mBadgeValue.set(String.valueOf(mSaveBadge));
            }
        } else {
            mBadgeValue.set(null);
        }
    }

    public long getSaveBadge() {
        return mSaveBadge;
    }

    public void updateBadgeValue() {
        mSaveBadge -= 1;
        setBadgeValue(mSaveBadge);
    }
}
