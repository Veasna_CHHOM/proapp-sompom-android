package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Intent;
import android.text.TextUtils;

import com.sompom.baseactivity.ResultCallback;
import com.sompom.tookitup.helper.LoginActivityResultCallback;
import com.sompom.tookitup.intent.newintent.MyCameraIntent;
import com.sompom.tookitup.intent.newintent.MyCameraResultIntent;
import com.sompom.tookitup.intent.newintent.ProductFormIntent;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.SellerStoreActivity;
import com.sompom.tookitup.services.datamanager.StoreDataManager;
import com.sompom.tookitup.utils.SharedPrefUtils;

/**
 * Created by He Rotha on 10/9/17.
 */

public class SellerStoreFragmentViewModel extends ListItemSellerStoreViewModel {
    private final String mCurrentUserId;
    private final OnSellerStoreFrListener mOnSellerStoreFrListener;

    public SellerStoreFragmentViewModel(StoreDataManager dataManager,
                                        String currentUserId,
                                        boolean isMe,
                                        OnSellerStoreFrListener onCallback) {
        super(dataManager, currentUserId, isMe, onCallback);
        mOnSellerStoreFrListener = onCallback;
        mCurrentUserId = currentUserId;
    }

    @Override
    public void onResume() {
        super.onResume();
        mIsMe.set(TextUtils.equals(SharedPrefUtils.getUserId(getContext()), mCurrentUserId));
    }

    public void onFloatingButtonClick() {
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).startLoginWizardActivity(new LoginActivityResultCallback() {
                @Override
                public void onActivityResultSuccess(boolean isAlreadyLogin) {
                    String mMyUserId = SharedPrefUtils.getUserId(getContext());
                    if (mCurrentUserId.equals(mMyUserId)) {
                        Intent intent = MyCameraIntent.newProductInstance(getContext(), null);
                        ((AbsBaseActivity) getContext()).startActivityForResult(intent, new ResultCallback() {
                            @Override
                            public void onActivityResultSuccess(int resultCode, Intent data) {
                                MyCameraResultIntent resultIntent = new MyCameraResultIntent(data);
                                getContext().startActivity(new ProductFormIntent(getContext(), resultIntent.getMedia()));
                            }
                        });
                    } else {
                        mOnSellerStoreFrListener.onChatClick(mUser);
                    }
                }
            });
        }
    }

    @Override
    public void onBackButtonClick() {
        if (getContext() instanceof SellerStoreActivity) {
            ((SellerStoreActivity) getContext()).onBackPressed();
        }
    }

    public interface OnSellerStoreFrListener extends ListItemSellerStoreViewModel.OnCallback {
        void onChatClick(User user);
    }
}
