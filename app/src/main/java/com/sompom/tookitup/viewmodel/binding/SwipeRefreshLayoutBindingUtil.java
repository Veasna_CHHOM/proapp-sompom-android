package com.sompom.tookitup.viewmodel.binding;

import android.databinding.BindingAdapter;
import android.support.v4.widget.SwipeRefreshLayout;

/**
 * Created by He Rotha on 4/26/18.
 */
public final class SwipeRefreshLayoutBindingUtil {
    private SwipeRefreshLayoutBindingUtil() {
    }

    @BindingAdapter("setRefreshing")
    public static void setRefreshing(SwipeRefreshLayout layout, boolean isRefreshing) {
        layout.setRefreshing(isRefreshing);

    }

    @BindingAdapter("setOnRefreshListener")
    public static void setOnRefreshListener(SwipeRefreshLayout layout, SwipeRefreshLayout.OnRefreshListener listener) {
        layout.setOnRefreshListener(listener);

    }

}
