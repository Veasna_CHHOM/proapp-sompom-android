package com.sompom.tookitup.viewmodel.binding;

import android.databinding.BindingAdapter;

import com.sompom.tookitup.model.result.UserBadge;
import com.sompom.tookitup.widget.HomeTabLayout;

/**
 * Created by nuonveyo
 * on 2/13/19.
 */

public final class HomeTabLayoutBindingUtil {
    private HomeTabLayoutBindingUtil() {

    }

    @BindingAdapter("setBadgeValue")
    public static void setBadgeValue(HomeTabLayout tabLayout, UserBadge userBadge) {
        if (userBadge != null) {
            tabLayout.setBadgeValue(userBadge);
        }
    }
}
