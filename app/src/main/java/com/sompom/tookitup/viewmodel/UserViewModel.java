package com.sompom.tookitup.viewmodel;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.view.View;

import com.sompom.tookitup.R;
import com.sompom.tookitup.helper.AnimationHelper;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.utils.SharedPrefUtils;

/**
 * Created by He Rotha on 10/9/17.
 */

public class UserViewModel extends StoreListingViewModel {
    public final ObservableBoolean mIsFollow = new ObservableBoolean(false);
    public final ObservableBoolean mIsMe = new ObservableBoolean(false);
    private OnStoreListener mListener;
    private Context mContext;

    public UserViewModel(Context context,
                         User user,
                         OnStoreListener listener) {
        super(context, user);
        mContext = context;
        mIsFollow.set(mUser.isFollow());

        checkIsMe();
        mListener = listener;
    }

    private void checkIsMe() {
        if (SharedPrefUtils.isLogin(mContext)) {
            if (mUser.getId().equals(SharedPrefUtils.getUserId(mContext))) {
                mIsMe.set(true);
            } else {
                mIsMe.set(false);
            }
        } else {
            mIsMe.set(false);
        }
    }

    public int getVisibilityButtonFollow(boolean isMe) {
        if (isMe) {
            return View.GONE;
        } else {
            return View.VISIBLE;
        }
    }

    public int getVisibilityControl(boolean isMe) {
        if (isMe) {
            return View.VISIBLE;
        } else {
            return View.GONE;
        }
    }

    public String getFollowButtonText(boolean isFollow) {
        if (isFollow) {
            return mContext.getString(R.string.following);
        } else {
            return mContext.getString(R.string.seller_store_follow_title);
        }
    }

    public String getCoverUrl() {
        return mUser.getUserCoverProfile();
    }

    public String getMyStoreLabel(boolean isMe) {
        if (isMe) {
            return mContext.getString(R.string.my_store);
        } else {
            return mContext.getString(R.string.store);
        }
    }

    public void onFollowClick(View view) {
        AnimationHelper.animateBounce(view);
//        if (mContext instanceof AbsBaseActivity) {
//            ((AbsBaseActivity) mContext).startLoginWizardActivity(new LoginActivityResultCallback() {
//                @Override
//                public void onActivityResultSuccess(boolean isAlreadyLogin) {
//                    if (!isAlreadyLogin) {
//                        checkIsMe();
//                    }
//                    if (!mIsMe.get()) {
//                        mIsFollow.set(!mIsFollow.get());
//                        boolean isFollow = mIsFollow.get();
//                        if (mUser.getFollow() != null) {
//                            mUser.getFollow().setFollow(isFollow);
//                        }
//                        mListener.onFollowClick(isFollow);
//                    }
//                }
//            });
//        }
    }

    public void onChangeProfileClick() {
        if (mIsMe.get()) {
            mListener.onChangeProfileClick();
        }
    }

    public void onChangeCoverClick() {
        if (mIsMe.get()) {
            mListener.onChangeCoverClick();
        }
    }

    public interface OnStoreListener {
        void onFollowClick(boolean isFollow);

        void onChangeProfileClick();

        void onChangeCoverClick();
    }


}
