package com.sompom.tookitup.viewmodel.newviewmodel;

import com.sompom.tookitup.listener.OnTimelineActiveUserItemClick;

/**
 * Created by nuonveyo on 7/11/18.
 */

public class ListItemActiveUserViewModel {

    private OnTimelineActiveUserItemClick mOnTimelineActiveUserItemClick;

    public ListItemActiveUserViewModel(OnTimelineActiveUserItemClick onTimelineActiveUserItemClick) {
        mOnTimelineActiveUserItemClick = onTimelineActiveUserItemClick;
    }

    public void onItemClick() {
        if (mOnTimelineActiveUserItemClick != null) {
            mOnTimelineActiveUserItemClick.onLiveClick();
        }
    }
}
