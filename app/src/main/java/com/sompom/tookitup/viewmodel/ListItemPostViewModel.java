package com.sompom.tookitup.viewmodel;

import com.sompom.tookitup.helper.IntentData;
import com.sompom.tookitup.listener.OnCreateTimelineListener;
import com.sompom.tookitup.model.CreateTimelineItem;
import com.sompom.tookitup.newui.AbsBaseActivity;

/**
 * Created by He Rotha on 11/16/18.
 */
public class ListItemPostViewModel implements OnCreateTimelineListener {
    private AbsBaseActivity mActivity;
    private CreateTimelineItem mCreateTimelineItem;

    public ListItemPostViewModel(AbsBaseActivity activity, CreateTimelineItem createTimelineItem) {
        mActivity = activity;
        mCreateTimelineItem = createTimelineItem;
    }

    @Override
    public void onPostWallStreetClick() {
        mCreateTimelineItem.startActivityForResult(new IntentData() {
            @Override
            public AbsBaseActivity getActivity() {
                return mActivity;
            }

            @Override
            public Action getAction() {
                return Action.CREATE_TIMELINE_ITEM;
            }

        });
    }

    @Override
    public void onCameraClick() {
        mCreateTimelineItem.startActivityForResult(new IntentData() {
            @Override
            public AbsBaseActivity getActivity() {
                return mActivity;
            }

            @Override
            public Action getAction() {
                return Action.CREATE_TIMELINE_CAMERA;
            }
        });
    }


}
