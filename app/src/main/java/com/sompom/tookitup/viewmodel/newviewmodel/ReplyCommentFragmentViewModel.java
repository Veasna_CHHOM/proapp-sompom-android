package com.sompom.tookitup.viewmodel.newviewmodel;

import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.SoundEffectService;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.listener.OnEditTextCommentListener;
import com.sompom.tookitup.model.request.CommentBody;
import com.sompom.tookitup.model.result.Comment;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.PopUpCommentDataManager;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 7/20/18.
 */

public class ReplyCommentFragmentViewModel extends LayoutEditTextCommentViewModel {
    private final PopUpCommentDataManager mCommentDataManager;
    private final OnCallback mListOnCallbackListListener;
    private final Comment mComment;
    private boolean mIsLoadedData;
    private String mLastSubCommentId = "0";
    private String mNewestSubCommentId;

    public ReplyCommentFragmentViewModel(String contentId,
                                         Comment comment,
                                         PopUpCommentDataManager dataManager,
                                         OnCallback onCallbackListListener) {
        super(dataManager, contentId, onCallbackListListener);
        mComment = comment;
        mCommentDataManager = dataManager;
        mListOnCallbackListListener = onCallbackListListener;
        mNewestSubCommentId = getNewestSubCommentId(mComment.getReplyComment());

        showLoading();
        getData();
    }

    @Override
    public void onRefresh() {
        super.onRefresh();
        getData();
    }

    @Override
    public void postComment(Comment comment) {
        comment.setPosting(true);
        comment.setLastSubCommentId(mNewestSubCommentId);
        Observable<Response<List<Comment>>> callback = mDataManager.postSubComment(mContentId, mComment.getId(), comment);
        ResponseObserverHelper<Response<List<Comment>>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<Comment>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mErrorMessage.set(null);
                if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                    mErrorMessage.set(ex.toString());
                } else {
                    mErrorMessage.set(getContext().getString(R.string.error_cannot_post_comment));
                }
                mEditTextCommentListener.onPostCommentFail();
            }

            @Override
            public void onComplete(Response<List<Comment>> result) {
                SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.SEND_MESSAGE);
                List<Comment> subCommentList = result.body();
                if (subCommentList == null || subCommentList.isEmpty()) {
                    onFail(new ErrorThrowable(404, null));
                    return;
                }
                mNewestSubCommentId = getNewestSubCommentId(subCommentList);
                mListOnCallbackListListener.onPostSubCommentSuccess(subCommentList);
            }
        }));
        mMessage.set("");
        mIsShowEmpty.set(false);
    }

    @Override
    public void updateComment(Comment comment) {
        if (mUpdateCommentPosition == 0) {
            super.updateComment(comment);
        } else {
            CommentBody commentBody = new CommentBody();
            commentBody.setMessage(comment.getContent());
            commentBody.setLastCommentId(mNewestSubCommentId);
            commentBody.setSubCommentId(comment.getId());
            Observable<Response<List<Comment>>> callback = mDataManager.updateSubComment(mContentId, mComment.getId(), commentBody);
            ResponseObserverHelper<Response<List<Comment>>> helper = new ResponseObserverHelper<>(getContext(), callback);
            addDisposable(helper.execute(new OnCallbackListener<Response<List<Comment>>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                }

                @Override
                public void onComplete(Response<List<Comment>> result) {
                    List<Comment> comments = result.body();
                    if (comments != null && !comments.isEmpty()) {
                        mNewestSubCommentId = getNewestSubCommentId(result.body());
                        setUpdateComment(null, mUpdateCommentPosition);
                        mListOnCallbackListListener.onUpdateComment(comments, mUpdateCommentPosition);
                    }
                }
            }));
        }
    }

    private void getData() {
        mLastSubCommentId = getLastSubCommentId(mComment.getReplyComment());
        Observable<Response<List<Comment>>> observable = mCommentDataManager.getOldSubCommentList(mComment.getId(), mLastSubCommentId);
        ResponseObserverHelper<Response<List<Comment>>> helper = new ResponseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<Comment>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                mIsRefresh.set(false);
                if (mIsLoadedData) {
                    return;
                }
                mIsLoadedData = false;
                mIsError.set(false);
                if (mListOnCallbackListListener != null) {
                    mListOnCallbackListListener.onLoadCommentSuccess(mComment.getReplyComment(), false);
                }
            }

            @Override
            public void onComplete(Response<List<Comment>> result) {
                hideLoading();
                mIsRefresh.set(false);
                mIsLoadedData = true;
                mLastSubCommentId = getLastSubCommentId(result.body());
                List<Comment> commentList = result.body();
                if (commentList == null) {
                    commentList = new ArrayList<>();
                }

                if (mComment.getReplyComment() != null && !mComment.getReplyComment().isEmpty()) {
                    commentList.addAll(mComment.getReplyComment());
                }

                if (mListOnCallbackListListener != null) {
                    mListOnCallbackListListener.onLoadCommentSuccess(commentList, mCommentDataManager.isCanLoadMore());
                }
            }
        }));
    }

    public void loadMore(OnCallbackListListener<Response<List<Comment>>> listListener) {
        Observable<Response<List<Comment>>> observable = mCommentDataManager.getOldSubCommentList(mComment.getId(), mLastSubCommentId);
        ResponseObserverHelper<Response<List<Comment>>> helper = new ResponseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<Comment>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                listListener.onFail(ex);
            }

            @Override
            public void onComplete(Response<List<Comment>> result) {
                mLastSubCommentId = getLastSubCommentId(result.body());
                listListener.onComplete(result, mDataManager.isCanLoadMore());
            }
        }));
    }

    public void onDeleteComment(Comment comment, int position, OnClickListener onClickListener) {
        showLoading();
        Observable<Response<Object>> callback;
        if (position == 0) {
            callback = mCommentDataManager.deleteComment(comment.getId());
        } else {
            callback = mCommentDataManager.deleteDeleteComment(mComment.getId(), comment.getId());
        }
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mErrorMessage.set(null);
                if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                    mErrorMessage.set(ex.toString());
                } else {
                    mErrorMessage.set(getContext().getString(R.string.error_cannot_delete_comment));
                }
                hideLoading();
            }

            @Override
            public void onComplete(Response<Object> result) {
                hideLoading();
                onClickListener.onClick();
            }
        }));
    }

    public void checkToLikeComment(Comment comment, int position) {
        Observable<Response<Object>> callback;
        if (position == 0) {
            callback = mDataManager.postLikeComment(comment.getId(), comment.isLike());
        } else {
            callback = mDataManager.postLikeSubComment(comment.getId(), comment.isLike());
        }
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute());
    }

    public interface OnCallback extends OnEditTextCommentListener {
        void onLoadCommentSuccess(List<Comment> listComment, boolean isCanLoadMore);

        void onPostSubCommentSuccess(List<Comment> subCommentList);

        void onUpdateComment(List<Comment> comments, int position);
    }
}
