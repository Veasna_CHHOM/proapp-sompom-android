package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;
import android.databinding.ObservableBoolean;

import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.OnUserClickListener;
import com.sompom.tookitup.model.emun.SuggestionTab;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.utils.FormatNumber;
import com.sompom.tookitup.viewmodel.AbsBaseViewModel;

/**
 * Created by nuonveyo on 7/20/18.
 */

public class ListItemSuggestionViewModel extends AbsBaseViewModel {
    public final ObservableBoolean mIsActive = new ObservableBoolean();
    public final ObservableBoolean mIsShowCover = new ObservableBoolean();
    public final User mUser;
    private OnUserClickListener mOnUserClickListener;

    public ListItemSuggestionViewModel(User user, SuggestionTab tab, OnUserClickListener listener) {
        mUser = user;
        mIsActive.set(mUser.isFollow());
        mIsShowCover.set(tab == SuggestionTab.Store);
        mOnUserClickListener = listener;
    }

    public String getFollowers(Context context) {
        long totalFollowers = mUser.getContentStat().getTotalFollowers();
        if (totalFollowers > 0) {
            String followers;
            if (totalFollowers == 1) {
                followers = context.getString(R.string.suggestion_follower);
            } else {
                followers = context.getString(R.string.seller_store_followers_title);
            }
            return FormatNumber.format(totalFollowers) + " " + followers;
        }
        return "";
    }

    public void onItemClick() {
        mUser.setFollow(!mUser.isFollow());
        mIsActive.set(mUser.isFollow());
        mOnUserClickListener.onUserClick(mUser);
    }

    public String getCoverUrl() {
        return mUser.getUserCoverProfile();
    }

    public int getCornerRadius(Context context) {
        return context.getResources().getDimensionPixelSize(R.dimen.medium_padding);
    }
}
