package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;
import android.databinding.ObservableField;

import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.OnEditDialogClickListener;
import com.sompom.tookitup.model.emun.Status;
import com.sompom.tookitup.model.result.Product;

/**
 * Created by He Rotha on 6/13/18.
 */
public class EditProductDialogViewModel {
    public ObservableField<Product> mProduct = new ObservableField<>();
    public ObservableField<String> mProductName = new ObservableField<>();
    public ObservableField<String> mEditButtonText = new ObservableField<>();
    private OnEditDialogClickListener mListener;

    public EditProductDialogViewModel(Context context,
                                      Product product,
                                      int productStatus,
                                      OnEditDialogClickListener listener) {
        mListener = listener;
        mProduct.set(product);
        mProductName.set(mProduct.get().getName());
        if (productStatus == Status.ON_SALE.getStatusProduct()) {
            mEditButtonText.set(context.getString(R.string.seller_store_button_edit));
        } else {
            mEditButtonText.set(context.getString(R.string.seller_store_button_clone));
        }
    }

    public void onEditClick() {
        mListener.onEditClick(mProduct.get());
    }

    public void onDeleteClick() {
        mListener.onDeleteClick(mProduct.get());
    }

}
