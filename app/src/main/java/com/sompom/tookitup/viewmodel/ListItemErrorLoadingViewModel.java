package com.sompom.tookitup.viewmodel;

import android.content.Context;

import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.model.emun.ErrorLoadingType;

/**
 * Created by nuonveyo on 8/27/18.
 */

public class ListItemErrorLoadingViewModel extends AbsLoadingViewModel {
    private OnClickListener mOnClickListener;

    public ListItemErrorLoadingViewModel(Context context,
                                         ErrorLoadingType errorLoadingType,
                                         String errorMessage,
                                         OnClickListener onClickListener) {
        super(context);
        mErrorLoadingType.set(errorLoadingType);
        mOnClickListener = onClickListener;
        showError(errorMessage);
    }

    @Override
    public void onRetryClick() {
        if (mOnClickListener != null) {
            mOnClickListener.onClick();
        }
    }
}
