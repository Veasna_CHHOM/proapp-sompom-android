package com.sompom.tookitup.viewmodel.newviewmodel;

import android.app.Activity;
import android.content.Context;
import android.databinding.ObservableField;

import com.sompom.tookitup.R;
import com.sompom.tookitup.helper.CheckRequestLocationCallbackHelper;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnCompleteListListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.request.ReportRequest;
import com.sompom.tookitup.model.result.Category;
import com.sompom.tookitup.model.result.WallLoadMoreWrapper;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.BaseObserverHelper;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.ProductListDataManager;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by He Rotha on 8/21/18.
 */
public class SearchResultViewModel extends AbsLoadingViewModel {
    public final ObservableField<String> mCategoryName = new ObservableField<>();
    private final ProductListDataManager mManager;
    private final OnCompleteListListener<WallLoadMoreWrapper<Adaptive>> mCallback;

    public SearchResultViewModel(Context context,
                                 ProductListDataManager manager,
                                 OnCompleteListListener<WallLoadMoreWrapper<Adaptive>> callback) {
        super(context);
        this.mManager = manager;
        List<Category> category = SharedPrefUtils.getQueryProduct(context).getCategories();
        if (category != null && !category.isEmpty()) {
            String value = "";
            for (int i = 0; i < category.size(); i++) {
                value += category.get(i).getName();

                if (i + 1 < category.size()) {
                    value += ", ";
                }
            }
            mCategoryName.set(value);
        }
        mCallback = callback;
        getData();
    }

    @Override
    public void onRetryClick() {
        getData();
    }

    private void getData() {
        showLoading();
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).requestLocation(new CheckRequestLocationCallbackHelper((Activity) getContext(),
                    location -> {
                        Observable<WallLoadMoreWrapper<Adaptive>> call = mManager.getProductList(location);
                        BaseObserverHelper<WallLoadMoreWrapper<Adaptive>> helper = new BaseObserverHelper<>(getContext(), call);
                        addDisposable(helper.execute(new OnCallbackListener<WallLoadMoreWrapper<Adaptive>>() {
                            @Override
                            public void onFail(ErrorThrowable ex) {
                                if (ex.getCode() == 404) {
                                    showError(getContext().getString(R.string.error_product_list_404));
                                } else {
                                    showError(ex.toString());
                                }
                            }

                            @Override
                            public void onComplete(WallLoadMoreWrapper<Adaptive> result) {
                                hideLoading();
                                mCallback.onComplete(result, mManager.isCanLoadMore());
                            }
                        }));
                    }));
        }
    }

    public void loadMore(OnCallbackListListener<WallLoadMoreWrapper<Adaptive>> callback) {
        Observable<WallLoadMoreWrapper<Adaptive>> call = mManager.loadMore();
        BaseObserverHelper<WallLoadMoreWrapper<Adaptive>> helper = new BaseObserverHelper<>(getContext(), call);
        addDisposable(helper.execute(new OnCallbackListener<WallLoadMoreWrapper<Adaptive>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                callback.onFail(ex);
            }

            @Override
            public void onComplete(WallLoadMoreWrapper<Adaptive> result) {
                callback.onComplete(result, mManager.isCanLoadMore());
            }
        }));
    }

    public void onBackClick() {
        if (getContext() instanceof Activity) {
            ((Activity) getContext()).finish();
        }
    }


    public void reportPost(String id, ReportRequest reportRequest) {
        Observable<Response<Object>> request = mManager.reportPost(id, reportRequest);
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), request);
        addDisposable(helper.execute());
    }

    public void unFollowShop(String id) {
        Observable<Response<Object>> observable = mManager.unFollowShop(id);
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute());
    }

    public void checkToPostFollow(String id, boolean isFollow) {
        Observable<Response<Object>> callback;
        if (isFollow) {
            callback = mManager.postFollow(id);
        } else {
            callback = mManager.unFollow(id);
        }
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute());
    }

}
