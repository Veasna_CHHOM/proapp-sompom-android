package com.sompom.tookitup.viewmodel.binding;

import android.app.Activity;
import android.databinding.BindingAdapter;
import android.text.TextUtils;

import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.widget.chat.ChatMediaAudioLayout;

/**
 * Created by nuonveyo on 9/20/18.
 */

public final class ChatMediaAudioLayoutBindingUtil {
    private ChatMediaAudioLayoutBindingUtil() {
    }

    @BindingAdapter({"setChatData", "setActivity", "setOnLongClickListener"})
    public static void setChatData(ChatMediaAudioLayout layout,
                                   Chat chat,
                                   Activity activity,
                                   OnClickListener listener) {
        layout.setActivity(activity);
        boolean isMe = TextUtils.equals(chat.getSenderId(), SharedPrefUtils.getUserId(activity));
        layout.setChat(chat, isMe, listener);
    }
}
