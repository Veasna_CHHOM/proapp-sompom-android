package com.sompom.tookitup.viewmodel.binding;

import android.databinding.BindingAdapter;

import com.sompom.tookitup.listener.OnItemClickListener;
import com.sompom.tookitup.model.emun.ActionButtonItem;
import com.sompom.tookitup.widget.actionbutton.ActionButtonContainer;

/**
 * Created by nuonveyo on 8/1/18.
 */

public final class ActionButtonContainerBindingUtil {
    private ActionButtonContainerBindingUtil() {
    }

    @BindingAdapter({"addActionButtonItem", "onActionButtonItemClick"})
    public static void addItem(ActionButtonContainer container,
                               ActionButtonItem[] items,
                               OnItemClickListener<ActionButtonItem> listener) {
        container.removeAllViews();
        for (ActionButtonItem item : items) {
            container.addView(item);
        }
        container.setOnItemClickListener(listener);
    }
}
