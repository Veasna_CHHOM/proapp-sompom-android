package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;

import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

/**
 * Created by nuonveyo on 8/30/18.
 */

public class ChatImageFullScreeDialogViewModel extends AbsLoadingViewModel {
    private Callback mOnClickListener;

    public ChatImageFullScreeDialogViewModel(Context context, Callback onClickListener) {
        super(context);
        mIsError.set(true);
        mOnClickListener = onClickListener;
    }

    public void onMoreButtonClick() {
        if (mOnClickListener != null) {
            mOnClickListener.onMoreButtonClick();
        }
    }

    public void onBackClick() {
        if (mOnClickListener != null) {
            mOnClickListener.onClick();
        }
    }

    public interface Callback extends OnClickListener {
        void onMoreButtonClick();
    }
}
