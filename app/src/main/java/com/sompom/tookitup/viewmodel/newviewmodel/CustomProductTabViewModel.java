package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.sompom.tookitup.viewmodel.AbsBaseViewModel;

/**
 * Created by nuonveyo on 6/4/18.
 */

public class CustomProductTabViewModel extends AbsBaseViewModel {
    public final ObservableField<String> mTitle = new ObservableField<>();
    public final ObservableField<String> mValue = new ObservableField<>();
    public final ObservableBoolean mIsChange = new ObservableBoolean();
    private int mCurrentCounter = 0;

    public CustomProductTabViewModel(String title, int value) {
        mTitle.set(title);
        addCounter(value);
    }

    public void setChangeTitleStyle(boolean status) {
        mIsChange.set(status);
    }

    public void addCounter(int value) {
        mCurrentCounter = mCurrentCounter + value;
        if (mCurrentCounter > 0) {
            mValue.set(String.valueOf(mCurrentCounter));
        } else {
            mValue.set(null);
        }
    }
}
