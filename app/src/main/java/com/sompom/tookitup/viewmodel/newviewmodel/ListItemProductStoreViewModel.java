package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.text.TextUtils;
import android.view.View;

import com.sompom.tookitup.helper.AnimationHelper;
import com.sompom.tookitup.helper.IntentData;
import com.sompom.tookitup.helper.LoginActivityResultCallback;
import com.sompom.tookitup.listener.OnItemClickListener;
import com.sompom.tookitup.listener.OnProductItemClickListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.emun.Status;
import com.sompom.tookitup.model.result.ContentStat;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.utils.FormatNumber;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.AbsBaseViewModel;

/**
 * Created by nuonveyo on 6/5/18.
 */

public class ListItemProductStoreViewModel extends AbsBaseViewModel {
    public final ObservableBoolean mIsAddMarginBottom = new ObservableBoolean();
    public final ObservableInt mProductStatusId = new ObservableInt();
    public final ObservableBoolean mIsShowUserImageProfile = new ObservableBoolean();
    public final ObservableField<String> mLikeCount = new ObservableField<>();
    public final ObservableBoolean mIsEditable = new ObservableBoolean();
    public final ObservableBoolean mIsSetLikeBackground = new ObservableBoolean();
    public final ObservableBoolean mIsLike = new ObservableBoolean();
    private final OnProductItemClickListener mListener;
    private final String mUserId;
    private final ContentStat mContentStat;
    public Product mProduct;
    private OnItemClickListener<Product> mOnItemClickListener;
    private AbsBaseActivity mActivity;

    public ListItemProductStoreViewModel(AbsBaseActivity activity,
                                         Product product,
                                         String userId,
                                         boolean isMe,
                                         boolean isEditable,
                                         boolean isShowImageProfile,
                                         int productStatusId,
                                         OnProductItemClickListener onProductItemClickListener) {
        mActivity = activity;
        mProduct = product;
        mContentStat = mProduct.getContentStat();
        mUserId = userId;
        mIsShowUserImageProfile.set(isShowImageProfile);
        mProductStatusId.set(productStatusId);
        if (isEditable) {
            mIsEditable.set(isMe);
        } else {
            mIsEditable.set(false);
        }
        mListener = onProductItemClickListener;

        mIsLike.set(mProduct.isLike());
        mIsSetLikeBackground.set(mContentStat.getTotalLikes() > 0);
        if (mContentStat.getTotalLikes() > 1) {
            mLikeCount.set(FormatNumber.format(mContentStat.getTotalLikes()));
        }
    }

    public void setOnItemClickListener(OnItemClickListener<Product> onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    public void onProductItemClick() {
        mProduct.startActivityForResult(new IntentData() {
            @Override
            public AbsBaseActivity getActivity() {
                return mActivity;
            }

            @Override
            public void onDataResultCallBack(Adaptive adaptive, boolean isRemove) {
                if (adaptive instanceof Product) {
                    mProduct = (Product) adaptive;
                    mIsLike.set(((Product) adaptive).isLike());
                }
            }
        });
    }

    public void onLikeButtonClick(View view, Context context) {
        if (mProductStatusId.get() == Status.ON_SALE.getStatusProduct()) {
            AnimationHelper.animateBounce(view);
            if (context instanceof AbsBaseActivity) {
                ((AbsBaseActivity) context).startLoginWizardActivity(new LoginActivityResultCallback() {
                    @Override
                    public void onActivityResultSuccess(boolean isAlreadyLogin) {
                        if (!TextUtils.equals(mUserId, SharedPrefUtils.getUserId(context))) {
                            long likeCount = mContentStat.getTotalLikes();
                            if (mIsLike.get()) {
                                mIsLike.set(false);
                                likeCount = likeCount - 1;
                            } else {
                                mIsLike.set(true);
                                likeCount = likeCount + 1;
                            }
                            mProduct.setLike(mIsLike.get());
                            mContentStat.setTotalLikes(likeCount);
                            mProduct.setContentStat(mContentStat);

                            mIsSetLikeBackground.set(mContentStat.getTotalLikes() > 0);
                            if (mContentStat.getTotalLikes() > 1) {
                                mLikeCount.set(FormatNumber.format(mContentStat.getTotalLikes()));
                            } else {
                                mLikeCount.set(null);
                            }
                            if (mOnItemClickListener != null) {
                                mOnItemClickListener.onClick(mProduct);
                            }
                        }
                    }
                });
            }
        }
    }

    public void onProductItemEdit() {
        if (mListener != null) {
            mListener.onProductItemEdit(mProduct);
        }
    }
}
