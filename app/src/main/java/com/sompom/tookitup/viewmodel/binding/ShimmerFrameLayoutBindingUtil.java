package com.sompom.tookitup.viewmodel.binding;

import android.databinding.BindingAdapter;

import com.facebook.shimmer.ShimmerFrameLayout;

/**
 * Created by He Rotha on 3/21/18.
 */

public final class ShimmerFrameLayoutBindingUtil {
    private ShimmerFrameLayoutBindingUtil() {
    }

    @BindingAdapter("enableShimmerAnimation")
    public static void enableShimmerAnimation(ShimmerFrameLayout view, boolean isEnable) {
        if (isEnable) {
            view.startShimmerAnimation();
//            view.setAlpha(0.1f);
        } else {
            view.stopShimmerAnimation();
//            view.setAlpha(1f);
        }
    }
}
