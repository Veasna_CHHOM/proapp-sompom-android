package com.sompom.tookitup.viewmodel;

import android.view.View;

import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.OnArticleSelectListener;
import com.sompom.tookitup.model.result.Category;

/**
 * Created by He Rotha on 9/15/17.
 */

public class ArticleViewModel {
    private Category mCategory;
    private OnArticleSelectListener mOnArticleSelectListener;

    public ArticleViewModel(Category category, OnArticleSelectListener onArticleSelectListener) {
        mCategory = category;
        mOnArticleSelectListener = onArticleSelectListener;
    }

    public String getName() {
        return mCategory.getName();
    }

    public String getCount() {
        return String.valueOf(mCategory.getCount());
    }

    public void onItemClick(View view) {
        if (mOnArticleSelectListener != null) {
            if (mCategory.getName() != null && mCategory.getName()
                    .equalsIgnoreCase(view.getContext().getString(R.string.all))) {
                mOnArticleSelectListener.onAllArticleSelected(view, mCategory);
            } else {
                mOnArticleSelectListener.onNormalArticleSelected(view, mCategory);
            }
        }
    }


}
