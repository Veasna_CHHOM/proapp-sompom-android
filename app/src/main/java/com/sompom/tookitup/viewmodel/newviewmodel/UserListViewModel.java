package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;
import android.databinding.ObservableBoolean;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.NetworkBroadcastReceiver;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.listener.UserListAdaptive;
import com.sompom.tookitup.model.UserGroup;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.BaseObserverHelper;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.datamanager.UserListManager;
import com.sompom.tookitup.utils.FileUtils;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import timber.log.Timber;

public class UserListViewModel extends AbsLoadingViewModel {

    private UserListManager mDataManager;
    private UserListViewModelListener mListener;
    public final ObservableBoolean mIsRefresh = new ObservableBoolean();
    private boolean mIsLoadedData;
    private NetworkBroadcastReceiver.NetworkListener mNetworkStateListener;

    public UserListViewModel(Context context,
                             UserListManager dataManager,
                             UserListViewModelListener listener) {
        super(context);
        mDataManager = dataManager;
        mListener = listener;

        if (getContext() instanceof AbsBaseActivity) {
            mNetworkStateListener = networkState -> {
                if (!mIsLoadedData) {
                    if (networkState == NetworkBroadcastReceiver.NetworkState.Connected) {
                        requestUserGroupList();
                    } else if (networkState == NetworkBroadcastReceiver.NetworkState.Disconnected) {
                        showNetworkError();
                    }
                }
            };
            ((AbsBaseActivity) getContext()).addNetworkStateChangeListener(mNetworkStateListener);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).removeStateChangeListener(mNetworkStateListener);
        }
    }

    public OnClickListener onRetryButtonClick() {
        return this::requestUserGroupList;
    }

    public void onRefresh() {
        mIsRefresh.set(true);
        requestUserGroupList();
    }

    private void requestUserGroupList() {
        if (!mIsRefresh.get()) {
            showLoading();
        }
        Observable<List<UserListAdaptive>> suggestCall = mDataManager.getUserGroupListService();
        BaseObserverHelper<List<UserListAdaptive>> suggestHelper =
                new BaseObserverHelper<>(getContext(), suggestCall);
        addDisposable(suggestHelper.execute(new OnCallbackListener<List<UserListAdaptive>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                Timber.e("onFail: " + ex.getCode());

                if (mIsLoadedData) {
                    mIsRefresh.set(false);
                    return;
                }

                mIsLoadedData = false;
                mIsRefresh.set(false);
                if (ex.getCode() == 500) {
                    showError(getContext().getString(R.string.error_form));
                } else if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                    showNetworkError();
                } else {
                    showError(ex.toString());
                }
            }

            @Override
            public void onComplete(List<UserListAdaptive> result) {
                Timber.e("onComplete");
                onLoadSuccess(result);
            }
        }));
    }

    private void onLoadSuccess(List<UserListAdaptive> userGroups) {
        hideLoading();
        mIsLoadedData = true;
        mIsRefresh.set(false);
        if (userGroups != null) {
            if (mListener != null) {
                mListener.onLoadSuccess(userGroups);
            }
        }
    }

    private List<UserListAdaptive> getMockup() {
        String data = FileUtils.readRawTextFile(mDataManager.mContext, R.raw.user_groups);
//        Timber.i("data: " + data);
        List<UserGroup> userGroups = new Gson().fromJson(data, new TypeToken<List<UserGroup>>() {
        }.getType());

//        Timber.i("userGroups: " + new Gson().toJson(userGroups));

        List<UserListAdaptive> userListAdaptives = new ArrayList<>();

        if (userGroups != null) {
            for (UserGroup userGroup : userGroups) {
                if (userGroup != null) {
                    userListAdaptives.add((UserGroup) userGroup);
                    if (userGroup.getParticipants() != null && !userGroup.getParticipants().isEmpty()) {
                        for (User participant : userGroup.getParticipants()) {
                            userListAdaptives.add((User) participant);
                        }
                    }
                }
            }
        }

        Timber.i("Data: " + new Gson().toJson(userListAdaptives));

        return userListAdaptives;
    }

    public interface UserListViewModelListener {

        void onLoadSuccess(List<UserListAdaptive> userGroups);
    }
}
