package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.ObservableBoolean;

import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.NetworkBroadcastReceiver;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.listener.OnCompleteListListener;
import com.sompom.tookitup.model.notification.NotificationAdaptive;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.BaseObserverHelper;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.datamanager.NotificationDataManager;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

/**
 * Created by nuonveyo on 7/4/18.
 */

public class HomeNotificationFragmentViewModel extends AbsLoadingViewModel {
    private static final int SEPARATE_HOUR = 24;
    public final ObservableBoolean mIsRefresh = new ObservableBoolean();
    private final NotificationDataManager mDataManager;
    private final OnCompleteListListener<List<NotificationAdaptive>> mListener;
    private NetworkBroadcastReceiver.NetworkListener mNetworkStateListener;
    private boolean mIsLoadedData;

    public HomeNotificationFragmentViewModel(NotificationDataManager notificationDataManager,
                                             OnCompleteListListener<List<NotificationAdaptive>> listener) {
        super(notificationDataManager.getContext());
        mDataManager = notificationDataManager;
        mListener = listener;

        if (getContext() instanceof AbsBaseActivity) {
            mNetworkStateListener = networkState -> {
                if (!mIsLoadedData) {
                    if (networkState == NetworkBroadcastReceiver.NetworkState.Connected) {
                        getData();
                    } else if (networkState == NetworkBroadcastReceiver.NetworkState.Disconnected) {
                        showNetworkError();
                    }
                }
            };
            ((AbsBaseActivity) getContext()).addNetworkStateChangeListener(mNetworkStateListener);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).removeStateChangeListener(mNetworkStateListener);
        }
    }

    public void onRefresh() {
        mIsRefresh.set(true);
        getData();
    }

    public OnClickListener onRetryButtonClick() {
        return this::getData;
    }

    private void getData() {
        if (!mIsRefresh.get()) {
            showLoading();
        }
        Observable<List<NotificationAdaptive>> call = mDataManager.getNotificationList();
        BaseObserverHelper<List<NotificationAdaptive>> helper = new BaseObserverHelper<>(mDataManager.getContext(), call);
        Disposable disposable = helper.execute(new OnCallbackListener<List<NotificationAdaptive>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (mIsLoadedData) {
                    mIsRefresh.set(false);
                    return;
                }

                mIsLoadedData = false;
                mIsRefresh.set(false);
                if (ex.getCode() == 500) {
                    showError(getContext().getString(R.string.error_form));
                } else if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                    showNetworkError();
                } else {
                    showError(ex.toString());
                }
            }

            @Override
            public void onComplete(List<NotificationAdaptive> result) {
                Timber.e("onComplete");
                hideLoading();
                mIsLoadedData = true;
                mIsRefresh.set(false);
                if (mListener != null) {
                    mListener.onComplete(result, mDataManager.isCanLoadMore());
                }
            }
        });
        addDisposable(disposable);
    }

    public void loadMore(OnCallbackListListener<List<NotificationAdaptive>> listListener) {
        Observable<List<NotificationAdaptive>> call = mDataManager.loadMore();
        BaseObserverHelper<List<NotificationAdaptive>> helper = new BaseObserverHelper<>(mDataManager.getContext(), call);
        Disposable disposable = helper.execute(new OnCallbackListener<List<NotificationAdaptive>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                listListener.onFail(ex);
            }

            @Override
            public void onComplete(List<NotificationAdaptive> result) {
                listListener.onComplete(result, mDataManager.isCanLoadMore());
            }
        });
        addDisposable(disposable);
    }


}
