package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;
import android.databinding.ObservableField;
import android.text.TextUtils;

import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.model.request.ForgotPasswordBody;
import com.sompom.tookitup.model.result.ForgotPasswordResponse;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.ForgotPasswordDataManager;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

import io.reactivex.Observable;
import retrofit2.Response;

public class ForgotPasswordViewModel extends AbsLoadingViewModel {

    private ObservableField<String> mEmail = new ObservableField<>();
    private ObservableField<String> mEmailError = new ObservableField<>();
    private ForgotPasswordDataManager mDataManager;
    private ForgotPasswordViewModelListener mListener;

    public ForgotPasswordViewModel(Context context,
                                   ForgotPasswordDataManager dataManager,
                                   ForgotPasswordViewModelListener listener) {
        super(context);
        mDataManager = dataManager;
        mListener = listener;
    }

    public ObservableField<String> getEmail() {
        return mEmail;
    }

    public ObservableField<String> getEmailError() {
        return mEmailError;
    }

    public boolean isAllFieldValid() {
        boolean valid = true;

        if (TextUtils.isEmpty(mEmail.get())) {
            mEmailError.set(mDataManager.getContext().getString(R.string.edit_profile_error_field_required));
            valid = false;
        }

        if (!TextUtils.isEmpty(mEmail.get()) && !android.util.Patterns.EMAIL_ADDRESS.matcher(mEmail.get()).matches()) {
            mEmailError.set(mDataManager.getContext().getString(R.string.edit_profile_invalid_email_title));
            valid = false;
        }

        return valid;
    }

    public void requestForgotPassword() {
        showLoading();
        Observable<Response<ForgotPasswordResponse>> loginService = mDataManager.getForgotPasswordService(new ForgotPasswordBody(mEmail.get()));
        ResponseObserverHelper<Response<ForgotPasswordResponse>> helper = new ResponseObserverHelper<>(mDataManager.mContext,
                loginService);
        addDisposable(helper.execute(new OnCallbackListener<Response<ForgotPasswordResponse>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
//                hideLoading();
//                if (mListener != null) {
//                    mListener.onFailed(ex.getMessage());
//                }

                //TODO: Need to remove
                onForgotPasswordSuccess(null);
            }

            @Override
            public void onComplete(Response<ForgotPasswordResponse> result) {
                onForgotPasswordSuccess(result.body());
            }
        }));
    }

    private void onForgotPasswordSuccess(ForgotPasswordResponse response) {
        hideLoading();
        if (mListener != null) {
            mListener.onSuccess();
        }
    }

    public interface ForgotPasswordViewModelListener {

        void onFailed(String error);

        void onSuccess();
    }
}
