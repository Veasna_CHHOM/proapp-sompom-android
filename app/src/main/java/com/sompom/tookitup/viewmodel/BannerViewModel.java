package com.sompom.tookitup.viewmodel;

import com.google.android.gms.ads.AdView;

import java.util.List;

/**
 * Created by He Rotha on 9/4/17.
 */

public class BannerViewModel {
    private List<AdView> mAdViews;
    private int mPosition;

    public BannerViewModel(int position, List<AdView> adViews) {
        mAdViews = adViews;
        mPosition = position;
    }

    public AdView getAdView() {
        if (mAdViews == null) {
            return null;
        }
        return mAdViews.get(calculateIndex(mPosition));
    }

    private int calculateIndex(int position) {
        int value = sumDigit(position);
        if (value >= 5) {
            return value - 5;
        }
        return value;
    }

    private int sumDigit(int n) {
        return (n % 9 == 0 && n != 0) ? 9 : n % 9;
    }
}
