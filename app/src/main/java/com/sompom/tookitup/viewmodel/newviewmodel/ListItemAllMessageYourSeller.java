package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.ObservableField;

import com.sompom.tookitup.listener.OnMessageItemClick;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.viewmodel.AbsBaseViewModel;

/**
 * Created by nuonveyo on 6/26/18.
 */

public class ListItemAllMessageYourSeller extends AbsBaseViewModel {
    public ObservableField<User> mUser = new ObservableField<>();
    private OnMessageItemClick mOnMessageItemClick;


    public ListItemAllMessageYourSeller(User user, OnMessageItemClick messageItemClick) {
        mUser.set(user);
        mOnMessageItemClick = messageItemClick;
    }

    public void onItemClick() {
        if (mOnMessageItemClick != null) {
            mOnMessageItemClick.onConversationUserClick(mUser.get());
        }
    }

    public User getUser() {
        return mUser.get();
    }
}
