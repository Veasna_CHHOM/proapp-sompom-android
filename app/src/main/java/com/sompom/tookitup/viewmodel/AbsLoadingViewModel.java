package com.sompom.tookitup.viewmodel;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;

import com.sompom.tookitup.R;
import com.sompom.tookitup.helper.ObservableListenerBoolean;
import com.sompom.tookitup.model.emun.ErrorLoadingType;
import com.sompom.tookitup.newui.AbsBaseActivity;

/**
 * Created by He Rotha on 11/6/17.
 */

public class AbsLoadingViewModel extends AbsBaseViewModel {
    public final ObservableField<ErrorLoadingType> mErrorLoadingType = new ObservableField<>();
    public final ObservableBoolean mIsShowButtonRetry = new ObservableBoolean(true);
    public final ObservableInt mButtonRetryText = new ObservableInt(R.string.empty_button_retry);
    public final ObservableField<String> mErrorMessage = new ObservableField<>();
    public final ObservableListenerBoolean mIsLoading = new ObservableListenerBoolean(false);
    public final ObservableBoolean mIsError = new ObservableBoolean(false);

    private final Context mContext;

    public AbsLoadingViewModel(Context context) {
        mContext = context;
    }

    public Context getContext() {
        return mContext;
    }

    public void onRetryClick() {
        showLoading();
    }

    protected void setShowKeyboardWhileLoadingScreen() {
        mIsLoading.setShowKeyboard(false);
    }

    public void showNetworkError() {
        mErrorMessage.set(getContext().getString(R.string.error_internet_connection));
        mErrorLoadingType.set(ErrorLoadingType.GENERAL_ERROR);
        showError();
    }

    public void showError(String errorName) {
        mErrorMessage.set(errorName);
        mErrorLoadingType.set(ErrorLoadingType.GENERAL_ERROR);
        showError();
    }

    public void showNoItemError(String errorName) {
        mErrorMessage.set(errorName);
        mErrorLoadingType.set(ErrorLoadingType.NO_ITEM);
        showError();
    }


    public void showLoading() {
        mIsError.set(false);
        mIsLoading.set(true);
    }

    public void hideLoading() {
        mIsError.set(false);
        mIsLoading.set(false);
    }

    private void showError() {
        mIsError.set(true);
        mIsLoading.set(false);
        mIsShowButtonRetry.set(true);
    }


    public void onBackButtonClick() {
        if (mContext instanceof AbsBaseActivity) {
            ((AbsBaseActivity) mContext).onBackPressed();
        }
    }
}
