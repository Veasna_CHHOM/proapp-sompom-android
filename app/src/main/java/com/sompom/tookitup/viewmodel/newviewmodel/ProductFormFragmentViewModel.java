package com.sompom.tookitup.viewmodel.newviewmodel;

import android.app.Activity;
import android.content.Intent;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.text.TextUtils;

import com.sompom.baseactivity.ResultCallback;
import com.sompom.tookitup.R;
import com.sompom.tookitup.adapter.ProductPreviewAdapter;
import com.sompom.tookitup.broadcast.upload.ProductUploadService;
import com.sompom.tookitup.database.CurrencyDb;
import com.sompom.tookitup.helper.CheckRequestLocationCallbackHelper;
import com.sompom.tookitup.intent.SelectAddressIntent;
import com.sompom.tookitup.intent.SelectAddressIntentResult;
import com.sompom.tookitup.intent.newintent.FilterCategoryIntent;
import com.sompom.tookitup.intent.newintent.MyCameraIntent;
import com.sompom.tookitup.intent.newintent.MyCameraResultIntent;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnProductFormListener;
import com.sompom.tookitup.model.Locations;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.SearchAddressResult;
import com.sompom.tookitup.model.result.Category;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.ProductFormActivity;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.ProductManager;
import com.sompom.tookitup.utils.CurrencyUtils;
import com.sompom.tookitup.utils.NetworkStateUtil;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 6/11/18.
 */

public class ProductFormFragmentViewModel extends AbsLoadingViewModel
        implements ProductPreviewAdapter.OnProductItemClickListener {
    public final ObservableField<String> mTitle = new ObservableField<>();
    public final ObservableField<String> mPrice = new ObservableField<>();
    public final ObservableField<String> mErrorPriceMessage = new ObservableField<>();
    public final ObservableField<String> mAddress = new ObservableField<>();
    public final ObservableField<String> mDescription = new ObservableField<>();
    public final ObservableField<String> mCategory = new ObservableField<>();
    public final ObservableField<String> mCurrency = new ObservableField<>();
    public final ObservableField<String> mSelectCurrency = new ObservableField<>();

    public final ObservableBoolean mIsTitleError = new ObservableBoolean();
    public final ObservableBoolean mIsPriceError = new ObservableBoolean();
    private final ProductManager mProductManager;
    private final OnProductFormListener mListener;
    private List<Category> mAllCategories = new ArrayList<>();
    private List<Category> mSelectedCategories = new ArrayList<>();
    private List<Media> mProductMediaList = new ArrayList<>();
    private SearchAddressResult mSearchAddressResult;
    private Product mProduct;
    private String mCurrencyCode;

    public ProductFormFragmentViewModel(ProductManager productManager,
                                        List<Media> imagePaths,
                                        OnProductFormListener listener) {
        super(productManager.getContext());

        mProductManager = productManager;
        mProductMediaList = imagePaths;
        if (mProductMediaList == null) {
            mProductMediaList = new ArrayList<>();
        }
        mListener = listener;
        mListener.onSetImage(imagePaths);

        mCurrencyCode = CurrencyDb.getSelectedCurrency(getContext()).getName();
        setCurrencyCode();
        mCurrency.set(getContext().getString(R.string.product_form_price));
        getCategoryList();
        initCategory();
    }

    public ProductFormFragmentViewModel(ProductManager productManager,
                                        Product product,
                                        OnProductFormListener listener) {
        super(productManager.getContext());
        mProductManager = productManager;
        mListener = listener;
        mProduct = product;
        mCurrencyCode = CurrencyDb.getSelectedCurrency(getContext()).getName();
        setCurrencyCode();
        getData();
    }

    private void setCurrencyCode() {
        Currency currency = CurrencyUtils.getCurrencySymbol(mCurrencyCode);
        mSelectCurrency.set(currency.getSymbol());
    }

    private void getData() {
        showLoading();
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).requestLocation(new CheckRequestLocationCallbackHelper((Activity) getContext(),
                    location -> {
                        Observable<Response<Product>> callback = mProductManager.getProductById(mProduct.getId(), location);
                        ResponseObserverHelper<Response<Product>> helper = new ResponseObserverHelper<>(getContext(), callback);
                        addDisposable(helper.execute(new OnCallbackListener<Response<Product>>() {
                            @Override
                            public void onFail(ErrorThrowable ex) {
                                if (ex.getCode() == 404) {
                                    showError(getContext().getString(R.string.error_product_list_404));
                                } else if (ex.getCode() == 409) {
                                    showError(getContext().getString(R.string.error_item_is_delete_409));
                                } else if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                                    showNetworkError();
                                } else {
                                    showError(ex.toString());
                                }
                            }

                            @Override
                            public void onComplete(Response<Product> result) {
                                hideLoading();
                                mProduct = result.body();
                                mSearchAddressResult = new SearchAddressResult();
                                Locations locations = new Locations();
                                locations.setLatitude(mProduct.getLatitude());
                                locations.setLongtitude(mProduct.getLongitude());
                                mSearchAddressResult.setLocations(locations);
                                mSearchAddressResult.setFullAddress(mProduct.getAddress());
                                mListener.onSetImage(mProduct.getMedia());
                                mProductMediaList = mProduct.getMedia();
                                mTitle.set(mProduct.getName());
                                mPrice.set(CurrencyUtils.formatDouble(mProduct.getPrice()));
                                mAddress.set(mProduct.getAddress());
                                mDescription.set(mProduct.getDescription());
                                mSelectedCategories = mProduct.getCategories();
                                mCurrencyCode = mProduct.getCurrency();
                                setCurrencyCode();
                                mCurrency.set(getContext().getString(R.string.product_form_price));
                                getCategoryList();
                                initCategory();
                            }
                        }));
                    }));
        }
    }

    public void onCategoryClick() {
        boolean isEmpty = mAllCategories.isEmpty();
        ArrayList<Category> categories = new ArrayList<>();
        for (Category allCategory : mAllCategories) {
            categories.add(Category.clone(allCategory));
        }

        if (mSelectedCategories != null) {
            if (!isEmpty) {
                for (Category selectedCategory : mSelectedCategories) {
                    checkCategory(categories, selectedCategory);
                }
            } else {
                categories.addAll(mSelectedCategories);
            }
        }
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).startActivityForResult(new FilterCategoryIntent(getContext(),
                            categories,
                            isEmpty),
                    new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            mSelectedCategories.clear();
                            mSelectedCategories = data.getParcelableArrayListExtra(SharedPrefUtils.CATEGORY);
                            initCategory();
                        }
                    });
        }
    }

    private void checkCategory(List<Category> categories, Category selectedCategory) {
        for (Category category : categories) {
            if (selectedCategory.getId().equalsIgnoreCase(category.getId())) {
                category.setCheck(true);
                break;
            }
        }
    }

    public void onAddressClick() {
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).startActivityForResult(new SelectAddressIntent(getContext(), mSearchAddressResult),
                    new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            SelectAddressIntentResult result = new SelectAddressIntentResult(data);
                            mSearchAddressResult = result.getResult();
                            mAddress.set(mSearchAddressResult.getFullAddress());
                        }
                    });
        }
    }

    private boolean validatePrice(boolean isError) {
        String price = mPrice.get();
        if (TextUtils.isEmpty(price)) {
            mIsPriceError.set(true);
            isError = true;
        } else {
            double value = Double.parseDouble(mPrice.get());
            if (value <= 0) {
                mIsPriceError.set(true);
                mErrorPriceMessage.set(getContext().getString(R.string.product_form_price_cannot_be_zero));
                isError = true;
            } else {
                if (price.contains(".")) {
                    String[] divisions = price.split("\\.");
                    try {
                        int num = divisions[1].length();
                        if (num > 2) {
                            mIsPriceError.set(true);
                            mErrorPriceMessage.set(getContext().getString(R.string.product_form_price_allow_2_decimal_only));
                            isError = true;
                        }
                    } catch (Exception ex) {
                        mIsPriceError.set(true);
                        mErrorPriceMessage.set(getContext().getString(R.string.product_form_your_price_is_incorrect));
                        isError = true;
                    }
                }
            }
        }
        return isError;
    }

    private boolean isInvalidAddress(boolean isError) {
        if (!isError) {
            if (TextUtils.isEmpty(mAddress.get())) {
                onAddressClick();
                return true;
            } else if (mSelectedCategories != null && mSelectedCategories.isEmpty()) {
                //Display the category list.
                onCategoryClick();
                return true;
            }
        }
        return false;
    }

    public void onPushButtonClick() {
        boolean isError = false;
        if (TextUtils.isEmpty(mTitle.get())) {
            mIsTitleError.set(true);
            isError = true;
        }

        isError = validatePrice(isError);
        if (isInvalidAddress(isError)) {
            return;
        }

        if (!isError) {
            if (NetworkStateUtil.isNetworkAvailable(getContext())) {
                //uncomment code below, if client want to upload product in UI thread.
//                updateOrPostProduct();
                if (mProduct != null) {
                    String id = mProduct.getId();
                    mProduct = new Product();
                    mProduct.setId(id);
                } else {
                    mProduct = new Product();
                }
                mProduct.setCurrency(mCurrencyCode);
                mProduct.setLatitude(mSearchAddressResult.getLocations().getLatitude());
                mProduct.setLongitude(mSearchAddressResult.getLocations().getLongtitude());
                mProduct.setAddress(mSearchAddressResult.getFullAddress());
                mProduct.setCity(mSearchAddressResult.getCity());
                mProduct.setCountry(mSearchAddressResult.getCountry());
                mProduct.setName(mTitle.get());
                mProduct.setPrice(Double.parseDouble(mPrice.get()));
                mProduct.setStatus(1);
                mProduct.setCategories(mSelectedCategories);
                mProduct.setProductMedia(mProductMediaList);
                mProduct.setDescription(mDescription.get());

                Intent uploadService = ProductUploadService.getIntent(getContext(), mProduct);
                getContext().startService(uploadService);

                if (getContext() instanceof AbsBaseActivity) {
                    ((AbsBaseActivity) getContext()).finish();
                }
            } else {
                showSnackBar(R.string.error_no_internet_connection_message);
            }
        }
    }

    private void getCategoryList() {

        Observable<Response<List<Category>>> callBack = mProductManager.getCategories();
        ResponseObserverHelper<Response<List<Category>>> helper =
                new ResponseObserverHelper<>(getContext(), callBack);
        addDisposable(helper.execute(data -> mAllCategories = data.body()));
    }

    private void initCategory() {
        StringBuilder categories = new StringBuilder();
        for (int i = 0; i < mSelectedCategories.size(); i++) {
            if (i == mSelectedCategories.size() - 1) {
                categories.append(mSelectedCategories.get(i).getName());
            } else {
                categories.append(mSelectedCategories.get(i).getName()).append(", ");
            }
        }
        mCategory.set(categories.toString());
    }

    @Override
    public void onItemEditClick() {
        Intent intent = MyCameraIntent.newProductInstance(getContext(), mProductMediaList);
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).startActivityForResult(intent, new ResultCallback() {
                @Override
                public void onActivityResultSuccess(int resultCode, Intent data) {
                    MyCameraResultIntent resultIntent = new MyCameraResultIntent(data);
                    mProductMediaList = resultIntent.getMedia();
                    mListener.onSetImage(mProductMediaList);
                }
            });
        }
    }

    @Override
    public void onRetryClick() {
        getData();
    }

    public void onBackPress() {
        if (getContext() instanceof ProductFormActivity) {
            ((ProductFormActivity) getContext()).onBackPressed();
        }
    }
}
