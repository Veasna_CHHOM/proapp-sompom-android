package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.Bindable;
import android.databinding.ObservableField;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.NetworkBroadcastReceiver;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.OtherUserProfileDataManager;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

import java.util.Objects;

import io.reactivex.Observable;
import retrofit2.Response;

public class OtherProfileViewModel extends AbsLoadingViewModel {

    public final ObservableField<String> mCover = new ObservableField<>();
    private final OtherUserProfileDataManager mDataManager;
    private final String mUserId;
    private final OnCallback mOnCallback;
    @Bindable
    public User mUser = new User();
    private boolean mIsReadUserDataReady;
    private NetworkBroadcastReceiver.NetworkListener mNetworkListener;

    public OtherProfileViewModel(OtherUserProfileDataManager dataManager,
                                 String userId,
                                 OnCallback onCallback) {
        super(dataManager.getContext());
        mDataManager = dataManager;
        mUserId = userId;
        mOnCallback = onCallback;

        if (getContext() instanceof AbsBaseActivity) {
            mNetworkListener = networkState -> {
                if (!mIsReadUserDataReady) {
                    if (networkState == NetworkBroadcastReceiver.NetworkState.Connected) {
                        requestUserProfile();
                    } else if (networkState == NetworkBroadcastReceiver.NetworkState.Disconnected) {
                        showNetworkError();
                    }
                }
            };
            ((AbsBaseActivity) getContext()).addNetworkStateChangeListener(mNetworkListener);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).removeStateChangeListener(mNetworkListener);
        }
    }

    public void requestUserProfile() {
        showLoading();
        Observable<Response<User>> call = mDataManager.getUserProfile(mUserId);
        ResponseObserverHelper<Response<User>> observerHelper = new ResponseObserverHelper<>(getContext(), call);
        observerHelper.execute(new OnCallbackListener<Response<User>>() {
            @Override
            public void onComplete(Response<User> data) {
                mUser = data.body();
                updateData();
            }

            private void updateData() {
                hideLoading();
                mIsReadUserDataReady = true;

                notifyPropertyChanged(BR.user);
                mCover.set(mUser.getUserCoverProfile());
            }

            @Override
            public void onFail(ErrorThrowable ex) {
                if (ex.getCode() == 404) {
                    showError(getContext().getString(R.string.error_user_404));
                } else {
                    showError(ex.toString());
                }
            }
        });
    }

    @Override
    public void onRetryClick() {
        super.onRetryClick();
        requestUserProfile();
    }

    public final void onCallClick() {
        if (mOnCallback != null) {
            mOnCallback.onCallClicked(mUser);
        }
    }

    public final void onMessageClick() {
        if (mOnCallback != null) {
            mOnCallback.onMessageClicked(mUser);
        }
    }

    public final void onCloseClick() {
        if (mOnCallback != null) {
            mOnCallback.onCloseClicked();
        }
    }

    public final void onRootClick() {
        if (mOnCallback != null) {
            mOnCallback.onRootClicked();
        }
    }

    public interface OnCallback {
        void onCallClicked(User user);

        void onMessageClicked(User user);

        void onCloseClicked();

        void onRootClicked();
    }
}
