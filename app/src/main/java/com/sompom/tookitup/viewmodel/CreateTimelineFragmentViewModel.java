package com.sompom.tookitup.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.net.Uri;
import android.os.Bundle;
import android.support.v13.view.inputmethod.InputContentInfoCompat;
import android.text.Editable;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.util.Linkify;

import com.desmond.squarecamera.intent.CameraIntent;
import com.example.usermentionable.model.UserMentionable;
import com.example.usermentionable.utils.RenderTextAsMentionable;
import com.example.usermentionable.widget.KeyboardInputEditor;
import com.sompom.baseactivity.ResultCallback;
import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.upload.LifeStreamUploadService;
import com.sompom.tookitup.helper.UrlDetectionWatcher;
import com.sompom.tookitup.intent.CreateTimelineIntentResult;
import com.sompom.tookitup.intent.EditMediaFileIntent;
import com.sompom.tookitup.intent.SelectAddressIntent;
import com.sompom.tookitup.intent.SelectAddressIntentResult;
import com.sompom.tookitup.intent.newintent.MyCameraIntent;
import com.sompom.tookitup.intent.newintent.MyCameraResultIntent;
import com.sompom.tookitup.listener.AbsLinkPreviewCallback;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnItemClickListener;
import com.sompom.tookitup.model.LifeStream;
import com.sompom.tookitup.model.Locations;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.SearchAddressResult;
import com.sompom.tookitup.model.WallStreetAdaptive;
import com.sompom.tookitup.model.emun.CreateWallStreetScreenType;
import com.sompom.tookitup.model.emun.MediaType;
import com.sompom.tookitup.model.emun.PublishItem;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.BaseObserverHelper;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.CreateWallStreetDataManager;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.utils.SpannableUtil;
import com.sompom.tookitup.viewmodel.newviewmodel.ListItemTimelineHeaderViewModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 5/4/18.
 */

public class CreateTimelineFragmentViewModel extends ToolbarViewModel implements KeyboardInputEditor.KeyBoardInputCallbackListener {
    public final ObservableField<Spannable> mUserFullName = new ObservableField<>();
    public final ObservableBoolean mIsShowMediaLayout = new ObservableBoolean();
    public final ObservableBoolean mIsHideSelectPhotoVideo = new ObservableBoolean();
    public final ObservableBoolean mIsShareWallStreet = new ObservableBoolean();
    public final ObservableField<ListItemTimelineHeaderViewModel> mShareWallStreetItemViewModel = new ObservableField<>();
    public final ObservableField<WallStreetAdaptive> mWallStreetAdaptive = new ObservableField<>();
    public final ObservableField<CreateWallStreetScreenType> mScreenType = new ObservableField<>();
    public final ObservableField<String> mActionErrorMessage = new ObservableField<>();

    public final User mUser;
    private final List<Media> mMediaList = new ArrayList<>();
    private final AbsBaseActivity mContext;
    private final AbsLinkPreviewCallback mAbsLinkPreviewCallback;
    private final CreateWallStreetDataManager mStoreDataManager;

    private SearchAddressResult mSearchAddressResult = new SearchAddressResult();
    private Editable mEditable;

    private boolean mIsShowedLinkPreview;
    private boolean mIsMediaFromKeyboard;

    public CreateTimelineFragmentViewModel(AbsBaseActivity activity,
                                           CreateWallStreetDataManager dataManager,
                                           WallStreetAdaptive wallStreetAdaptive,
                                           CreateWallStreetScreenType screenType,
                                           AbsLinkPreviewCallback absLinkPreviewCallback) {
        super(dataManager.getContext());
        mStoreDataManager = dataManager;
        mContext = activity;
        mWallStreetAdaptive.set(wallStreetAdaptive);
        mScreenType.set(screenType);
        mAbsLinkPreviewCallback = absLinkPreviewCallback;
        mUser = SharedPrefUtils.getUser(mContext);

        String actionButton = mContext.getString(R.string.create_wall_street_post_button);
        if (screenType == CreateWallStreetScreenType.CREATE_POST) {
            if (wallStreetAdaptive != null
                    && wallStreetAdaptive.getMedia() != null
                    && !wallStreetAdaptive.getMedia().isEmpty()) {
                mIsShowMediaLayout.set(true);
                mIsEnableToolbarButton.set(true);
                mMediaList.addAll(wallStreetAdaptive.getMedia());
            }

        } else if (screenType == CreateWallStreetScreenType.EDIT_POST) {
            actionButton = mContext.getString(R.string.seller_store_button_edit);
            mIsHideSelectPhotoVideo.set(true);

            setSearchAddressResultData(wallStreetAdaptive.getLatitude(),
                    wallStreetAdaptive.getLongitude(),
                    wallStreetAdaptive.getAddress(),
                    wallStreetAdaptive.getCity());
            mAbsLinkPreviewCallback.onSetEditText(wallStreetAdaptive.getDescription());

        } else if (screenType == CreateWallStreetScreenType.SHARE_POST) {
            //this comment code below for auto render item of share, but we don't use it anymore.
            //to avoid re-write, so, we keep it for moment.
//            enableShareWallStreetControl();
//            ListItemTimelineHeaderViewModel viewModel = new ListItemTimelineHeaderViewModel(mContext,
//                    (Adaptive) wallStreetAdaptive,
//                    0,
//                    null);
//            viewModel.setDisableClickOnUser(true);
//            mShareWallStreetItemViewModel.set(viewModel);


            mIsEnableToolbarButton.set(true);
            if (!TextUtils.isEmpty(wallStreetAdaptive.getShareUrl())) {
                mAbsLinkPreviewCallback.onSetEditText(wallStreetAdaptive.getShareUrl());
                absLinkPreviewCallback.onStartDownloadLinkPreview(wallStreetAdaptive.getShareUrl());
            }
        } else if (screenType == CreateWallStreetScreenType.SHARE_LINK) {
            mIsEnableToolbarButton.set(true);
            if (!TextUtils.isEmpty(wallStreetAdaptive.getDescription())) {
                mAbsLinkPreviewCallback.onSetEditText(wallStreetAdaptive.getDescription());
                absLinkPreviewCallback.onStartDownloadLinkPreview(wallStreetAdaptive.getDescription());
            }
        }

        mIsShowAction.set(true);
        mActionText.set(actionButton);
        mUserFullName.set(getUserFullNameCheckIn());
    }

    private void setSearchAddressResultData(double latitude, double longitude, String address, String city) {
        Locations locations = new Locations();
        locations.setLatitude(latitude);
        locations.setLongtitude(longitude);
        mSearchAddressResult.setFullAddress(address);
        mSearchAddressResult.setCity(city);
        mSearchAddressResult.setLocations(locations);
    }

    @Override
    public void onToolbarButtonClicked(Context context) {
        LifeStream lifeStream = new LifeStream();
        String renderText = RenderTextAsMentionable.render(mContext, mEditable);
        lifeStream.setTitle(renderText);
        lifeStream.setAddress(mSearchAddressResult.getFullAddress());
        lifeStream.setCity(mSearchAddressResult.getCity());
        lifeStream.setCountry(mSearchAddressResult.getCountry());
        if (mSearchAddressResult.getLocations() != null) {
            lifeStream.setCountry(mSearchAddressResult.getLocations().getCountry());
            lifeStream.setLatitude(mSearchAddressResult.getLocations().getLatitude());
            lifeStream.setLongitude(mSearchAddressResult.getLocations().getLongtitude());
        }
        //TODO: change get default publish field
        lifeStream.setPublish(PublishItem.Follower.getId());

        if (mScreenType.get() == CreateWallStreetScreenType.EDIT_POST) {
            WallStreetAdaptive adaptive = mWallStreetAdaptive.get();
            if (adaptive != null) {
                String id = adaptive.getId();
                updateWallStreet(id, lifeStream);
            }
        } else {
            User user = new User();
            user.setId(SharedPrefUtils.getUserId(mContext));
            lifeStream.setStoreUser(user);
            lifeStream.setMedia(mMediaList);
            Intent uploadService = LifeStreamUploadService.getIntent(mContext, lifeStream);
            mContext.startService(uploadService);
            mContext.finish();
        }

    }

    private void updateWallStreet(String id, LifeStream lifeStream) {
        showLoading();
        Observable<Response<LifeStream>> call = mStoreDataManager.updateWallStreet(id, lifeStream);
        ResponseObserverHelper<Response<LifeStream>> helper = new ResponseObserverHelper<>(mContext, call);
        addDisposable(helper.execute(new OnCallbackListener<Response<LifeStream>>() {
            @Override
            public void onComplete(Response<LifeStream> result) {
                hideLoading();
                CreateTimelineIntentResult intent = new CreateTimelineIntentResult(result.body());
                mContext.setResult(Activity.RESULT_OK, intent);
                mContext.finish();
            }

            @Override
            public void onFail(ErrorThrowable ex) {
                mActionErrorMessage.set(null);
                if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                    mActionErrorMessage.set(ex.toString());
                } else {
                    mActionErrorMessage.set(getContext().getString(R.string.error_cannot_update_wallstreet_message));
                }
                hideLoading();
            }
        }));
    }

    private String getContent() {
        if (mEditable != null) {
            return mEditable.toString();
        }
        return "";
    }


    public TextWatcher onTextChanged() {
        return new UrlDetectionWatcher() {
            @Override
            public void onTyping(String text) {
                if (!mIsShareWallStreet.get()) {
                    if (!TextUtils.isEmpty(text.trim()) && mMediaList.isEmpty()) {
                        if (!mIsEnableToolbarButton.get()) {
                            mIsEnableToolbarButton.set(true);
                        }
                    } else {
                        if (mMediaList.isEmpty()) {
                            mIsEnableToolbarButton.set(false);
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                super.afterTextChanged(editable);
                mEditable = editable;
                Linkify.addLinks(editable, Linkify.WEB_URLS);
            }

            @Override
            public void onUrlInput(String url) {
                mAbsLinkPreviewCallback.onStartDownloadLinkPreview(url);
            }

            @Override
            public boolean shouldDetectUrl() {
                return !mIsShowMediaLayout.get() && !mIsShowedLinkPreview;
            }
        };
    }

    public void setShowedLinkPreview(boolean showedLinkPreview) {
        mIsShowedLinkPreview = showedLinkPreview;
    }

    private void removeLinkPreview() {
        if (mIsShowedLinkPreview) {
            mIsShowedLinkPreview = false;
            mAbsLinkPreviewCallback.onRemoveListener();
        }
    }

    public void onImageVideoClick() {
        CameraIntent intent;
        WallStreetAdaptive adaptive = mWallStreetAdaptive.get();
        if (mIsMediaFromKeyboard || adaptive == null) {
            intent = MyCameraIntent.newTimelineInstance(mContext, null);
        } else {
            intent = MyCameraIntent.newTimelineInstance(mContext, adaptive.getMedia());
        }

        mContext.startActivityForResult(intent, new ResultCallback() {
            @Override
            public void onActivityResultSuccess(int resultCode, Intent data) {
                if (resultCode == Activity.RESULT_OK) {
                    mIsMediaFromKeyboard = false;
                    MyCameraResultIntent cameraIntent1 = new MyCameraResultIntent(data);
                    mMediaList.clear();
                    mMediaList.addAll(cameraIntent1.getMedia());

                    final LifeStream lifeStream = new LifeStream();
                    lifeStream.setMedia(mMediaList);

                    mWallStreetAdaptive.set(lifeStream);
                    mIsShowMediaLayout.set(true);
                    removeLinkPreview();

                    if (!mIsEnableToolbarButton.get()) {
                        mIsEnableToolbarButton.set(true);
                    }
                }
            }
        });
    }

    public OnItemClickListener<Integer> onMediaItemClickListener() {
        return position -> {
            if (mScreenType.get() != CreateWallStreetScreenType.SHARE_POST) {
                EditMediaFileIntent intent = new EditMediaFileIntent(mContext,
                        (LifeStream) mWallStreetAdaptive.get(),
                        position,
                        mScreenType.get() == CreateWallStreetScreenType.EDIT_POST);
                mContext.startActivityForResult(intent, new ResultCallback() {
                    @Override
                    public void onActivityResultSuccess(int resultCode, Intent data) {
                        mMediaList.clear();
                        EditMediaFileIntent intent1 = new EditMediaFileIntent(data);
                        LifeStream lifeStream = intent1.getLifeStream();
                        if (lifeStream.getMedia() != null && !lifeStream.getMedia().isEmpty()) {
                            mMediaList.addAll(lifeStream.getMedia());
                            mWallStreetAdaptive.set(lifeStream);
                            mIsEnableToolbarButton.set(true);
                        } else {
                            mWallStreetAdaptive.set(null);
                            mIsShowMediaLayout.set(false);
                            mIsEnableToolbarButton.set(!TextUtils.isEmpty(getContent()));
                        }
                    }
                });

            }
        };
    }

    public void onCheckInClick() {
        SelectAddressIntent intent = new SelectAddressIntent(mContext, mSearchAddressResult);
        mContext.startActivityForResult(intent,
                new ResultCallback() {
                    @Override
                    public void onActivityResultSuccess(int resultCode, Intent data) {
                        SelectAddressIntentResult intentResult = new SelectAddressIntentResult(data);
                        mSearchAddressResult = intentResult.getResult();
                        mUserFullName.set(getUserFullNameCheckIn());
                        boolean isMediaNotEmpty = !mMediaList.isEmpty();
                        mIsEnableToolbarButton.set(!TextUtils.isEmpty(getContent()) || isMediaNotEmpty);
                    }
                });

    }

    private Spannable getUserFullNameCheckIn() {
        return SpannableUtil.getUserFullNameCheckIn(mContext,
                mUser,
                mSearchAddressResult,
                v -> onCheckInClick());
    }

    private void enableShareWallStreetControl() {
        mIsEnableToolbarButton.set(true);
        mIsHideSelectPhotoVideo.set(true);
        mIsShowMediaLayout.set(true);
        mIsShareWallStreet.set(true);
    }

    public int getMarginDimen() {
        if (mIsShareWallStreet.get()) {
            return mContext.getResources().getDimensionPixelSize(R.dimen.space_large);
        } else {
            return 0;
        }
    }

    @Override
    public void onCommitContent(InputContentInfoCompat inputContentInfo, int flags, Bundle opts) {
        mIsMediaFromKeyboard = true;
        //TODO: need to get width, height from image view
        Uri uri = inputContentInfo.getLinkUri();
        if (uri != null) {
            Media media = new Media();
            media.setUrl(String.valueOf(uri));
            media.setType(MediaType.IMAGE);

            mMediaList.clear();
            mMediaList.add(media);

            final LifeStream lifeStream = new LifeStream();
            lifeStream.setMedia(mMediaList);

            mWallStreetAdaptive.set(lifeStream);
            mIsShowMediaLayout.set(true);
            removeLinkPreview();

            if (!mIsEnableToolbarButton.get()) {
                mIsEnableToolbarButton.set(true);
            }
        }
    }

    public void getMentionUserList(String userName, OnCallbackListener<List<UserMentionable>> listOnCallbackListener) {
        Observable<List<UserMentionable>> observable = mStoreDataManager.getUserMentionList(userName);
        BaseObserverHelper<List<UserMentionable>> helper = new BaseObserverHelper<>(getContext(), observable);
        helper.execute(listOnCallbackListener);
    }
}
