package com.sompom.tookitup.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;

import com.sompom.tookitup.R;
import com.sompom.tookitup.chat.MessageState;
import com.sompom.tookitup.helper.ResizeAnimator;
import com.sompom.tookitup.listener.OnChatItemListener;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.listener.OnItemClickListener;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.SelectedChat;
import com.sompom.tookitup.model.emun.ChatBg;
import com.sompom.tookitup.model.emun.MediaType;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.utils.ChatUtility;
import com.sompom.tookitup.utils.DateTimeUtils;
import com.sompom.tookitup.viewmodel.newviewmodel.ItemChatSeenAvatarViewModel;
import com.sompom.tookitup.widget.chat.MultiImageChatView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by He Rotha on 9/14/17.
 */

public class ChatMediaViewModel extends ItemChatSeenAvatarViewModel {
    public final ObservableField<ChatBg> mChatBg = new ObservableField<>();
    public final ObservableBoolean mIsShowIconStatus = new ObservableBoolean();
    public final ObservableBoolean mIsShowSharingIcon = new ObservableBoolean();

    public User mSender;
    public final Chat mChat;

    private final OnChatItemListener mOnImageClickListener;
    private final int mPosition;
    private final ChatUtility.GroupMessage mGroupMessage;
    private final SelectedChat mCurrentSelectChat;
    private final Activity mContext;
    private boolean mIsLinkClickedAlready;
    private List<User> mGroupParticipants;
    private Conversation mConversation;

    public ChatMediaViewModel(Activity context,
                              Conversation conversation,
                              Chat chat,
                              ChatUtility.GroupMessage groupMessage,
                              int position,
                              SelectedChat selectChat,
                              OnChatItemListener onItemClickListener) {
        mContext = context;
        mConversation = conversation;
        mChat = chat;
        mCurrentSelectChat = selectChat;
        mGroupMessage = groupMessage;
        mChatBg.set(mGroupMessage.getChatBg());
        mPosition = position;
        mOnImageClickListener = onItemClickListener;
        mIsShowIconStatus.set(mChat.getStatus() != MessageState.SEEN);
        mIsShowSharingIcon.set(mChat.getStatus() != MessageState.SENDING &&
                mChat.getStatus() != MessageState.FAIL);
        checkToInitSender(conversation.getParticipants());
    }

    private void checkToInitSender(List<User> groupParticipants) {
        mGroupParticipants = groupParticipants;
        User senderIngroupChat = findSenderIngroupChat(mChat.getSenderId());
        if (senderIngroupChat != null) {
            mSender = senderIngroupChat;
        } else {
            if (mChat.getSender() == null) {
                mSender = mOnImageClickListener.getUser(mChat.getSenderId());
            } else {
                mSender = mChat.getSender();
            }
        }

        bindGroupSeenAvatar();
    }

    private boolean isGroupConversation() {
        return !TextUtils.isEmpty(mConversation.getGroupId());
    }

    private void bindGroupSeenAvatar() {
        if (mChat.getSeenParticipants() != null && !mChat.getSeenParticipants().isEmpty()) {
            setSeenAvatars(new ArrayList<>(mChat.getSeenParticipants()));
        }
    }

    private User findSenderIngroupChat(String senderId) {
        if (mGroupParticipants != null) {
            for (User groupParticipant : mGroupParticipants) {
                if (groupParticipant.getId().matches(senderId)) {
                    return groupParticipant;
                }
            }
        }

        return null;
    }

    public Activity getActivity() {
        return mContext;
    }

    public MultiImageChatView.OnLongClickListener onImageLongClick() {
        return productMedia -> {
            if (mOnImageClickListener != null) {
                mOnImageClickListener.onChatItemLongPressClick(mChat, productMedia, mPosition);
            }
        };
    }

    public OnClickListener onAudioLongClickListener() {
        return () -> {
            if (mOnImageClickListener != null) {
                mOnImageClickListener.onChatItemLongPressClick(mChat, null, mPosition);
            }
        };
    }

    public boolean onLongClick(View view) {
        if (mOnImageClickListener != null) {
//            ChatBg dr = (ChatBg) view.getTag(R.id.tagContainer);
//            view.setBackgroundResource(dr.getDrawable(true));

            mOnImageClickListener.onChatItemLongPressClick(mChat, null, mPosition);
        }
        return true;
    }

    public Drawable getStatusIcon(Context context) {
        return ContextCompat.getDrawable(context, mChat.getStatus().getIcon());
    }

    public String getTime(Context context) {
        return DateTimeUtils.parse(context, mChat.getDate().getTime());
    }

    public void onForwardClick() {
        mOnImageClickListener.onForwardClick(mChat);
    }

    public String getStatus(Context context) {
        return context.getString(mChat.getStatus().getTextStatus());
    }

    public int getVisibility() {
        if (mGroupMessage.isShowTime() || mChat.isExpandHeight()) {
            return View.VISIBLE;
        } else {
            return View.GONE;
        }
    }

    public int getStatusVisibility() {
        if (mChat.isExpandHeight()) {
            return View.VISIBLE;
        } else {
            return View.GONE;
        }
    }

    public void onRetryClick() {
        if (mChat.getStatus() == MessageState.FAIL) {
            mOnImageClickListener.onMessageRetryClick(mChat);
        }
    }

    public List<Media> photoUrl() {
        return mChat.getMediaList();
    }

    public OnItemClickListener<Boolean> onSelectChat(View view) {
        return (isLinkClicked) -> {
            if (!isLinkClicked && !mIsLinkClickedAlready) {
                if (mCurrentSelectChat.getSelectView() != null &&
                        mCurrentSelectChat.getSelectView() != view &&
                        mCurrentSelectChat.getSelectChat() != null) {
                    View textViewStatus = mCurrentSelectChat.getSelectView().findViewById(R.id.textViewStatus);
                    if (textViewStatus.getVisibility() == View.VISIBLE) {
                        animateLayout(mCurrentSelectChat.getSelectView(),
                                mCurrentSelectChat.getSelectChat(),
                                mCurrentSelectChat.getGroupMessage());
                    } else {
                        mCurrentSelectChat.getSelectChat().setExpandHeight(false);
                    }
                }
                animateLayout(view, mChat, mGroupMessage);
            }

            mIsLinkClickedAlready = isLinkClicked;
        };
    }

    private void animateLayout(View view, final Chat chat, final ChatUtility.GroupMessage groupMessage) {
        final View textViewStatus = view.findViewById(R.id.textViewStatus);
        final View textViewTime = view.findViewById(R.id.textViewTime);
        final View textViewMessage = view.findViewById(R.id.message);

        int statusExpandHeight = view.getContext().getResources().getDimensionPixelSize(R.dimen.chat_height);

        ResizeAnimator statusAnimator;
        ResizeAnimator timeAnimator = null;
        if (chat.isExpandHeight()) {
            statusAnimator = new ResizeAnimator(statusExpandHeight, 0, textViewStatus);
            if (!groupMessage.isShowTime()) {
                timeAnimator = new ResizeAnimator(statusExpandHeight, 0, textViewTime);
            }
            mCurrentSelectChat.setSelect(null, null, null);
        } else {
            statusAnimator = new ResizeAnimator(0, statusExpandHeight, textViewStatus);
            if (!groupMessage.isShowTime()) {
                timeAnimator = new ResizeAnimator(0, statusExpandHeight, textViewTime);
            }
            mCurrentSelectChat.setSelect(view, chat, groupMessage);
        }
        statusAnimator.setAnimatorListener(new ResizeAnimator.AnimatorListener() {
            @Override
            public void onStart() {
                if (!chat.isExpandHeight()) {
                    textViewStatus.setVisibility(View.VISIBLE);
                    textViewTime.setVisibility(View.VISIBLE);
                }
                chat.setExpandHeight(!chat.isExpandHeight());
                if (textViewMessage != null) {
                    ChatBg dr = (ChatBg) textViewMessage.getTag(R.id.tagContainer);
                    textViewMessage.setBackgroundResource(dr.getDrawable(chat.isExpandHeight()));
                }
            }

            @Override
            public void onEnd() {
                if (!chat.isExpandHeight()) {
                    textViewStatus.setVisibility(View.GONE);
                    if (!groupMessage.isShowTime()) {
                        textViewTime.setVisibility(View.GONE);
                    }
                }
            }
        });
        statusAnimator.startAnimation();
        if (timeAnimator != null) {
            timeAnimator.startAnimation();
        }
    }

    public OnItemClickListener<Integer> onImageClickListener() {
        return integer -> {
            if (mOnImageClickListener != null)
                mOnImageClickListener.onImageClick(photoUrl(), integer);
        };
    }

    public Media getGifMedia() {
        return mChat.getMediaList().get(0);
    }

    public String getViaGifText() {
        if (mChat.getMediaList().get(0).getType() == MediaType.TENOR_GIF) {
            return mContext.getString(R.string.chat_tenor_gif);
        } else {
            return "";
        }
    }
}
