package com.sompom.tookitup.viewmodel.binding;

import android.databinding.BindingAdapter;

import com.sompom.tookitup.listener.OnItemClickListener;
import com.sompom.tookitup.model.emun.ChooseDistanceItem;
import com.sompom.tookitup.widget.choosedistance.ChooseDistanceContainer;

/**
 * Created by nuonveyo on 7/5/18.
 */

public final class ChooseDistanceContainerBindingUtil {
    private ChooseDistanceContainerBindingUtil() {
    }

    @BindingAdapter({"addItem", "choosePosition", "onItemClick"})
    public static void addItem(ChooseDistanceContainer container,
                               ChooseDistanceItem[] items,
                               int choosePosition,
                               OnItemClickListener<ChooseDistanceItem> listener) {
        container.removeAllViews();
        for (ChooseDistanceItem item : items) {
            container.addView(item, choosePosition);
        }
        container.setOnItemClickListener(listener);
    }
}
