package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.DataBindingUtil;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ViewDataBinding;
import android.graphics.Point;
import android.view.View;

import com.sompom.tookitup.R;
import com.sompom.tookitup.databinding.LayoutProductMediaBinding;
import com.sompom.tookitup.databinding.ListItemImageBinding;
import com.sompom.tookitup.databinding.ListItemProductMediaBinding;
import com.sompom.tookitup.databinding.ListItemTimelineMediaBinding;
import com.sompom.tookitup.helper.WallStreetIntentData;
import com.sompom.tookitup.listener.OnMediaLayoutClickListener;
import com.sompom.tookitup.listener.OnTimelineItemButtonClickListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.ShareAds;
import com.sompom.tookitup.model.SharedProduct;
import com.sompom.tookitup.model.SharedTimeline;
import com.sompom.tookitup.model.WallStreetAdaptive;
import com.sompom.tookitup.model.emun.MediaType;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.services.datamanager.WallStreetDataManager;
import com.sompom.tookitup.utils.TimelinePostItemUtil;
import com.sompom.tookitup.utils.VolumePlaying;
import com.sompom.tookitup.viewholder.BindingViewHolder;
import com.sompom.tookitup.viewholder.TimelineViewHolder;
import com.sompom.tookitup.viewmodel.AbsBaseViewModel;
import com.sompom.tookitup.viewmodel.binding.ImageViewBindingUtil;
import com.sompom.tookitup.widget.lifestream.CollageView;
import com.sompom.tookitup.widget.lifestream.MediaLayout;
import com.sompom.tookitup.widget.lifestream.OnBindItemListener;

import java.util.List;

/**
 * Created by nuonveyo on 11/8/18.
 */

public class ListItemMainTimelineViewModel extends AbsBaseViewModel {
    public final ObservableField<ListItemTimelineHeaderViewModel> mTimelineHeaderShareViewModel = new ObservableField<>();
    public final ObservableField<ListItemTimelineHeaderViewModel> mTimelineHeaderViewModel = new ObservableField<>();
    public final ObservableField<ListItemTimelineViewModel> mTimelineFooterViewModel = new ObservableField<>();
    public final ObservableField<ListItemTimelineViewModel> mProductFooterViewModel = new ObservableField<>();
    public final ObservableBoolean mIsShowHeader = new ObservableBoolean();
    public final ObservableField<Product> mProduct = new ObservableField<>();
    protected final int mPosition;
    protected final AbsBaseActivity mContext;
    protected final OnTimelineItemButtonClickListener mOnItemClickListener;
    final BindingViewHolder mBindingViewHolder;
    final Adaptive mAdaptive;
    private final WallStreetIntentData mMyIntentData;
    private final WallStreetDataManager mWallStreetDataManager;

    public ListItemMainTimelineViewModel(AbsBaseActivity activity,
                                         BindingViewHolder bindingViewHolder,
                                         WallStreetDataManager dataManager,
                                         Adaptive adaptive,
                                         int position,
                                         OnTimelineItemButtonClickListener onItemClickListener) {
        mContext = activity;
        mBindingViewHolder = bindingViewHolder;
        mWallStreetDataManager = dataManager;
        mAdaptive = adaptive;
        mPosition = position;
        mOnItemClickListener = onItemClickListener;
        mMyIntentData = new WallStreetIntentData(activity, mOnItemClickListener);

        if (mAdaptive instanceof SharedTimeline
                || mAdaptive instanceof SharedProduct
                || mAdaptive instanceof ShareAds) {
            mIsShowHeader.set(true);

            String title;

            if (adaptive instanceof SharedProduct) {
                title = activity.getString(R.string.home_shared_product_title);
            } else {
                title = activity.getString(R.string.home_shared_post_title);
            }

            ListItemTimelineHeaderViewModel headerViewModel = new ListItemTimelineHeaderViewModel(mContext,
                    mAdaptive,
                    mPosition,
                    mOnItemClickListener);
            headerViewModel.setSharedTextTitle(title);
            mTimelineHeaderShareViewModel.set(headerViewModel);

            if (mAdaptive instanceof SharedProduct) {
                mProduct.set(((SharedProduct) mAdaptive).getProduct());
                initListProductFooter(mProduct.get());
            }
            initListItemTimelineFooter();
        } else if (mAdaptive instanceof Product) {
            mProduct.set((Product) mAdaptive);
            initListProductFooter(mProduct.get());
            //there is no footer for product
        } else {
            initListItemTimelineFooter();
        }
        initListItemTimelineHeader();
    }

    public void initListItemTimelineHeader() {
        Adaptive adaptive = TimelinePostItemUtil.getAdaptive(mAdaptive);
        ListItemTimelineHeaderViewModel viewModel = new ListItemTimelineHeaderViewModel(mContext,
                adaptive,
                mPosition,
                mOnItemClickListener);
        mTimelineHeaderViewModel.set(viewModel);

        CollageView collageView = getCollageView();
        if (collageView == null) {
            return;
        }

        final List<Media> medias = getMedia();
        if (medias == null) {
            return;
        }

        //Bind Timeline with media layout

        collageView.bindView(new CollageViewBinder(medias));
        collageView.setBadge(medias.size());
    }

    private List<Media> getMedia() {
        final List<Media> medias;
        if (mAdaptive instanceof WallStreetAdaptive) {
            medias = ((WallStreetAdaptive) mAdaptive).getMedia();
        } else {
            medias = null;
        }
        return medias;
    }

    private CollageView getCollageView() {
        CollageView collageView = null;
        if (mBindingViewHolder.getBinding() instanceof ListItemTimelineMediaBinding) {
            collageView = ((ListItemTimelineMediaBinding) mBindingViewHolder.getBinding()).mediaLayouts;
        } else if (mBindingViewHolder.getBinding() instanceof ListItemProductMediaBinding) {
            collageView = ((ListItemProductMediaBinding) mBindingViewHolder.getBinding()).mediaLayouts;
        }
        return collageView;
    }

    private void initListItemTimelineFooter() {
        ListItemTimelineViewModel viewModel = new ListItemTimelineViewModel(mContext,
                mAdaptive,
                mWallStreetDataManager,
                mPosition,
                mOnItemClickListener);
        mTimelineFooterViewModel.set(viewModel);
    }

    private void initListProductFooter(Product product) {
        ListItemTimelineViewModel viewModel = new ListItemTimelineViewModel(mContext,
                product,
                mWallStreetDataManager,
                mPosition,
                mOnItemClickListener);
        mProductFooterViewModel.set(viewModel);
    }

    private class CollageViewBinder implements OnBindItemListener {
        private final List<Media> mMedias;
        private boolean mIsMediaVideoAdded;

        private CollageViewBinder(List<Media> medias) {
            mMedias = medias;
        }

        @Override
        public void onBind(View view, int position) {
            ViewDataBinding bind = DataBindingUtil.bind(view);
            if (bind instanceof LayoutProductMediaBinding) {
                ((LayoutProductMediaBinding) bind).mediaLayout.setMedia(mMedias.get(position));
                ((LayoutProductMediaBinding) bind).mediaLayout.setOnMediaLayoutClickListener(new OnMediaLayoutClickListener() {
                    @Override
                    public void onMediaLayoutClick(MediaLayout mediaLayout) {
                        mOnItemClickListener.getRecyclerView().pause();
                        mOnItemClickListener.getRecyclerView().setAutoResume(false);
                        mMyIntentData.setMediaPosition(position);
                        getAdaptive().startActivityForResult(mMyIntentData);
                    }

                    private Adaptive getAdaptive() {
                        if (mAdaptive instanceof SharedTimeline) {
                            return ((SharedTimeline) mAdaptive).getLifeStream();
                        }
                        return mAdaptive;
                    }
                });


                if (mMedias.get(position).getType() == MediaType.VIDEO && !mIsMediaVideoAdded) {
                    mIsMediaVideoAdded = true;
                    ((LayoutProductMediaBinding) bind).mediaLayout.setMute(VolumePlaying.isMute(view.getContext()));
                    ((LayoutProductMediaBinding) bind).mediaLayout.setVisibleMuteButton(true);
                    if (mBindingViewHolder instanceof TimelineViewHolder) {
                        ((TimelineViewHolder) mBindingViewHolder).setMediaLayout(((LayoutProductMediaBinding) bind).mediaLayout);
                    }
                }
            } else if (bind instanceof ListItemImageBinding) {
                ImageViewBindingUtil.loadImageUrl(((ListItemImageBinding) bind).imageView,
                        ((WallStreetAdaptive) mAdaptive).getMedia().get(position).getUrl(),
                        0,
                        300,
                        null,
                        false);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mOnItemClickListener.getRecyclerView().pause();
                        mMyIntentData.setMediaPosition(position);
                        mOnItemClickListener.getRecyclerView().setAutoResume(false);
                        getAdaptive().startActivityForResult(mMyIntentData);
                    }

                    private Adaptive getAdaptive() {
                        if (mAdaptive instanceof SharedProduct) {
                            return ((SharedProduct) mAdaptive).getProduct();
                        }
                        return mAdaptive;
                    }
                });
            }
        }

        @Override
        public Point onRequiredWidthHeight() {
            Media media = mMedias.get(0);
            return new Point(media.getWidth(), media.getHeight());
        }
    }
}
