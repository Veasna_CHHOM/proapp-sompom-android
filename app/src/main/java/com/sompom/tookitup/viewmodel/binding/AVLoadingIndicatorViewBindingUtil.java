package com.sompom.tookitup.viewmodel.binding;

import android.databinding.BindingAdapter;
import android.view.View;

import com.wang.avi.AVLoadingIndicatorView;

/**
 * Created by He Rotha on 10/5/18.
 */
public class AVLoadingIndicatorViewBindingUtil {
    private AVLoadingIndicatorViewBindingUtil() {
    }

    @BindingAdapter("setVisibility")
    public static void setVisibility(AVLoadingIndicatorView view, boolean isShow) {
        if (isShow) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.INVISIBLE);

        }
    }
}
