package com.sompom.tookitup.viewmodel.binding;

import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.graphics.Point;
import android.view.View;

import com.sompom.tookitup.databinding.LayoutProductMediaBinding;
import com.sompom.tookitup.listener.OnItemClickListener;
import com.sompom.tookitup.model.LifeStream;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.SharedProduct;
import com.sompom.tookitup.model.SharedTimeline;
import com.sompom.tookitup.model.WallStreetAdaptive;
import com.sompom.tookitup.model.emun.CreateWallStreetScreenType;
import com.sompom.tookitup.model.emun.MediaType;
import com.sompom.tookitup.model.emun.ViewType;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.utils.VolumePlaying;
import com.sompom.tookitup.viewholder.BindingViewHolder;
import com.sompom.tookitup.viewholder.TimelineViewHolder;
import com.sompom.tookitup.widget.lifestream.CollageView;
import com.sompom.tookitup.widget.lifestream.OnBindItemListener;

/**
 * Created by nuonveyo on 8/9/18.
 */

public final class CollageViewBindingUtils {
    private CollageViewBindingUtils() {
    }

    @BindingAdapter(value = {"setCollageView", "setOnItemClickListener", "setScreenType", "setBindingViewHolder"}, requireAll = false)
    public static void setCollageView(CollageView collageView,
                                      WallStreetAdaptive adaptive,
                                      OnItemClickListener<Integer> onItemClickListener,
                                      CreateWallStreetScreenType streetScreenType,
                                      BindingViewHolder holder) {
        if (streetScreenType == CreateWallStreetScreenType.EDIT_POST || adaptive == null
                || adaptive.getMedia() == null
                || adaptive.getMedia().isEmpty()) {
            collageView.setVisibility(View.GONE);
            return;
        }

        int itemType = getItemType(adaptive);
        ViewType v = ViewType.fromValue(itemType);
        collageView.setOrientation(v.getOrientation());
        collageView.setNumberOfItem(v.getNumberOfItem());
        collageView.setFormat(v.getFormat());
        collageView.startGenerateView();

        collageView.bindView(new OnBindItemListener() {
            private boolean mIsMediaVideoAdded;

            @Override
            public void onBind(View view, int position) {
                LayoutProductMediaBinding bind = DataBindingUtil.bind(view);
                if (bind != null && position < adaptive.getMedia().size()) {
                    bind.mediaLayout.setMedia(adaptive.getMedia().get(position));
                    bind.mediaLayout.setOnMediaLayoutClickListener(mediaLayout -> {
                        if (onItemClickListener != null) {
                            onItemClickListener.onClick(position);
                        }
                    });
                    if (adaptive.getMedia().get(position).getType() == MediaType.VIDEO && !mIsMediaVideoAdded) {
                        mIsMediaVideoAdded = true;
                        bind.mediaLayout.setMute(VolumePlaying.isMute(bind.mediaLayout.getContext()));
                        bind.mediaLayout.setVisibleMuteButton(true);
                        if (holder instanceof TimelineViewHolder) {
                            ((TimelineViewHolder) holder).setMediaLayout(bind.mediaLayout);
                        }
                    }
                }
            }

            @Override
            public Point onRequiredWidthHeight() {
                Media media = adaptive.getMedia().get(0);
                return new Point(media.getWidth(), media.getHeight());
            }
        });
        collageView.setBadge(adaptive.getMedia().size());
        collageView.setVisibility(View.VISIBLE);
    }

    private static int getItemType(WallStreetAdaptive adaptive) {
        int itemType;
        if (adaptive instanceof Product || adaptive instanceof SharedProduct) {
            if (adaptive instanceof SharedProduct) {
                itemType = ((SharedProduct) adaptive).getTimelineViewType().getTimelineViewType();
            } else {
                itemType = ((Product) adaptive).getTimelineViewType().getTimelineViewType();
            }
        } else {
            if (adaptive instanceof SharedTimeline) {
                itemType = ((SharedTimeline) adaptive).getTimelineViewType().getTimelineViewType();
            } else {
                itemType = ((LifeStream) adaptive).getTimelineViewType().getTimelineViewType();
            }
            //For one item must be return ViewType.Timeline_FreeStyle_1
            if (itemType == ViewType.LiveVideoTimeline.getTimelineViewType()) {
                itemType = ViewType.FreeStyle.getTimelineViewType();
            }
        }
        return itemType;
    }
}
