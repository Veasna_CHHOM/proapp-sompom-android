package com.sompom.tookitup.viewmodel.newviewmodel;

import android.view.View;

import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.listener.OnTimelineItemButtonClickListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.services.datamanager.WallStreetDataManager;

/**
 * Created by He Rotha on 7/10/18.
 */
public class ListItemTimelineDetailViewModel extends ListItemTimelineViewModel {
    private OnClickListener onLikeButtonClickListener;


    public ListItemTimelineDetailViewModel(AbsBaseActivity activity,
                                           Adaptive adaptive,
                                           WallStreetDataManager dataManager,
                                           int position,
                                           OnTimelineItemButtonClickListener onItemClick,
                                           OnClickListener onClickListener) {
        super(activity, adaptive, dataManager, position, onItemClick);
        onLikeButtonClickListener = onClickListener;
    }

    @Override
    public void onLikeClick(View view) {
        super.onLikeClick(view);
        if (onLikeButtonClickListener != null) {
            onLikeButtonClickListener.onClick();
        }
    }
}
