package com.sompom.tookitup.viewmodel.binding;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.sompom.tookitup.R;
import com.sompom.tookitup.helper.GlideApp;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.emun.ChatBg;
import com.sompom.tookitup.utils.DrawChatImageRoundedUtil;
import com.sompom.tookitup.widget.RoundedImageView;

/**
 * Created by nuonveyo
 * on 1/7/19.
 */

public final class RoundedImageViewBindingUtil {
    private RoundedImageViewBindingUtil() {
    }

    @BindingAdapter({"setChatGifImage", "chatBg"})
    public static void setChatGifImage(RoundedImageView imageView, Media gifMedia, ChatBg chatBg) {
        imageView.setCornerType(DrawChatImageRoundedUtil.getCornerType(chatBg));
        Drawable drawable = ContextCompat.getDrawable(imageView.getContext(), R.drawable.ic_photo_placeholder_200dp);
        GlideApp
                .with(imageView.getContext())
                .asGif()
                .load(gifMedia.getUrl())
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .placeholder(drawable)
                .into(imageView);

        int imageWidth = imageView.getContext().getResources().getDimensionPixelOffset(R.dimen.chat_media_width);
        imageView.setMaxWidth(imageWidth);
        imageView.setMaxHeight(imageWidth * gifMedia.getHeight() / gifMedia.getWidth());
    }
}
