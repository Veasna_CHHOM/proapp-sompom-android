package com.sompom.tookitup.viewmodel.newviewmodel;

import com.sompom.tookitup.adapter.ProductPreviewAdapter;
import com.sompom.tookitup.model.Media;

/**
 * Created by He Rotha on 9/26/17.
 */

public class ProductPreviewViewModel {
    private String mUrl;
    private ProductPreviewAdapter.OnProductItemClickListener mListener;

    public ProductPreviewViewModel(Media url,
                                   ProductPreviewAdapter.OnProductItemClickListener listener) {
        mUrl = url.getUrl();
        mListener = listener;
    }

    public String getUrl() {
        return mUrl;
    }

    public void onItemEditClick() {
        mListener.onItemEditClick();
    }
}
