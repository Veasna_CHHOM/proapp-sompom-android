package com.sompom.tookitup.viewmodel.binding;

import android.databinding.BindingAdapter;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.widget.TextView;

import com.sompom.tookitup.adapter.MoreGameAdapter;
import com.sompom.tookitup.helper.AlphaAnimation;
import com.sompom.tookitup.helper.LoopAnimation;
import com.sompom.tookitup.model.result.MoreGame;
import com.sompom.tookitup.newui.fragment.MoreGameFragment;
import com.sompom.tookitup.viewmodel.ViewPagerViewModel;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nuonveyo on 4/24/18.
 */

public final class ViewPagerBindingUtil {
    private ViewPagerBindingUtil() {
    }

    @BindingAdapter("setAdapter")
    public static void setAdapter(ViewPager viewPager,
                                  ViewPagerViewModel viewPagerViewModel) {
        List<MoreGame> games = viewPagerViewModel.getMoreGames();
        FragmentManager manager = viewPagerViewModel.getFragmentManager();

        if (games == null || games.isEmpty()) {
            return;
        }

        List<MoreGameFragment> fragments = new ArrayList<>();

        for (int i = 0; i < viewPagerViewModel.getMoreGames().size(); i++) {
            MoreGameFragment fragment = MoreGameFragment.getInstance(games.get(i));
            fragments.add(fragment);
        }
        final MoreGameAdapter adapter = new MoreGameAdapter(manager, fragments);
        viewPager.setAdapter(adapter);
    }

    @BindingAdapter(value = {"setAdapter", "setTextViewLabel", "setCirclePageIndicator"})
    public static void setViewPagerAdapter(final ViewPager viewPager,
                                           final ViewPagerViewModel viewPagerViewModel,
                                           final TextView textViewLabel,
                                           CirclePageIndicator circlePageIndicator) {

        setAdapter(viewPager, viewPagerViewModel);
        circlePageIndicator.setViewPager(viewPager);


        final List<MoreGame> games = viewPagerViewModel.getMoreGames();
        if (games == null || games.isEmpty()) {
            return;
        }

        final AlphaAnimation in = new AlphaAnimation(0f, 1f);
        final AlphaAnimation out = new AlphaAnimation(1f, 0f);

        if (viewPager.getTag() instanceof LoopAnimation) {
            LoopAnimation loopAnimation = (LoopAnimation) viewPager.getTag();
            loopAnimation.cancel();
        }
        LoopAnimation loopAnimation = new LoopAnimation(games.size());
        loopAnimation.setAnimation(position -> {
            final MoreGame game = games.get(position);
            out.setOnAnimationListener(() -> {
                viewPager.setCurrentItem(position, true);
                TextViewBindingUtil.setTextViewLabel(textViewLabel, game);
                textViewLabel.startAnimation(in);
            });
            textViewLabel.startAnimation(out);
        });
        viewPager.setTag(loopAnimation);
    }
}
