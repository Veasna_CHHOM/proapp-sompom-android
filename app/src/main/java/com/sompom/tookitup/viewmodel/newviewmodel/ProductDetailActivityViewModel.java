package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;
import android.databinding.Bindable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.text.TextUtils;
import android.view.View;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.helper.NavigateSellerStoreHelper;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.model.request.ReportRequest;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.ProductDetailActivity;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.ProductManager;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.AbsBaseViewModel;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by He Rotha on 10/6/17.
 */

public class ProductDetailActivityViewModel extends AbsBaseViewModel {
    public final ObservableField<String> mBuyingText = new ObservableField<>();
    public final ObservableField<String> mCommentCount = new ObservableField<>();
    public final ObservableField<String> mLikeCount = new ObservableField<>();
    public final ObservableBoolean mIsVisibleButton = new ObservableBoolean();
    public final ObservableBoolean mChecktextviewFavourite = new ObservableBoolean();
    public final ObservableBoolean mTextviewComment = new ObservableBoolean();
    public final ObservableBoolean mIsBelongToOwner = new ObservableBoolean();
    public final ObservableBoolean mIsLike = new ObservableBoolean();
    public final ObservableBoolean mIsShow = new ObservableBoolean();

    @Bindable
    public User mUser = new User();

    private OnProductListener mProductListener;
    private ProductManager mProductManager;

    public ProductDetailActivityViewModel(ProductManager productManager,
                                          OnProductListener listener) {
        mProductManager = productManager;
        mProductListener = listener;
    }

    public void onSellerStoreClick() {
        if (mUser != null && !TextUtils.isEmpty(mUser.getId())) {
            new NavigateSellerStoreHelper(mProductManager.getContext(), mUser).openSellerStore();
        }
    }

    public void setShow(boolean isShow) {
        mIsShow.set(isShow);
    }

    public void onChatClick() {
        mProductListener.onChatClick();
    }

    public void onBuyClick(View view) {
        mProductListener.onBuyClick(view);
    }

    public int getPaddingRightSpace(Context context, boolean isBelongToOwner) {
        if (isBelongToOwner) {
            return 0;
        } else {
            return context.getResources().getDimensionPixelSize(R.dimen.space_xxlarge);
        }
    }

    public void setProduct(Product product) {
        mUser = product.getUser();
        notifyPropertyChanged(BR.user);

        String myUserId = SharedPrefUtils.getUserId(mProductManager.getContext());
        mCommentCount.set(String.valueOf(product.getContentStat().getTotalComments()));
        mLikeCount.set(String.valueOf(product.getContentStat().getTotalLikes()));
        mIsLike.set(product.isLike());

        mTextviewComment.set(product.getContentStat().getTotalComments() > 0);
        mChecktextviewFavourite.set(product.getContentStat().getTotalLikes() > 0);
        if (SharedPrefUtils.isLogin(mProductManager.getContext()) && myUserId.equals(product.getUser().getId())) {
            mIsBelongToOwner.set(true);
        }

        mIsVisibleButton.set(!mIsBelongToOwner.get());

        if (product.isRequestBuy()) {
            mBuyingText.set(mProductManager.getContext().getString(R.string.buying_pending));
        } else {
            mBuyingText.set(mProductManager.getContext().getString(R.string.buy_it_now));
        }

        mProductListener.onCheckComplete(mIsBelongToOwner.get());
    }

    public void reportProduct(String id, ReportRequest reportRequest) {
        Observable<Response<Object>> request = mProductManager.reportProduct(id, reportRequest);
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(mProductManager.getContext(), request);
        addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
            @Override
            public void onFail(ErrorThrowable errorThrowable) {
                showSnackBar(R.string.error_report_successfully);
            }

            @Override
            public void onComplete(Response<Object> data) {
                showSnackBar(R.string.error_report_fail);
            }
        }));
    }

    public void onCommentViewClick() {
        if (mProductListener != null) {
            mProductListener.onPopUpCommentLayout();
        }
    }

    public void onPopUpLikeViewerLayout() {
        if (mProductListener != null) {
            mProductListener.onPopUpLikeViewerLayout();
        }
    }

    public void onBackButtonClick() {
        if (mProductManager.getContext() instanceof ProductDetailActivity) {
            ((ProductDetailActivity) mProductManager.getContext()).onBackPressed();
        }
    }

    public void onCallClick(View v) {
        //Implement feature
        if (mProductListener != null) {
            mProductListener.onCallClick(mUser);
        }
    }

    public interface OnProductListener {
        void onChatClick();

        void onBuyClick(View view);

        void onCallClick(User user);

        void onCheckComplete(boolean isBelongToOwner);

        void onPopUpCommentLayout();

        void onPopUpLikeViewerLayout();
    }
}
