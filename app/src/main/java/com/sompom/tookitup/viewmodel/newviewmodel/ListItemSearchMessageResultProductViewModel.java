package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;

import com.sompom.tookitup.intent.newintent.ProductDetailIntent;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.viewmodel.AbsBaseViewModel;

/**
 * Created by nuonveyo on 7/2/18.
 */

public class ListItemSearchMessageResultProductViewModel extends AbsBaseViewModel {
    public Product mProduct;

    public ListItemSearchMessageResultProductViewModel(Product product) {
        mProduct = product;
    }

    public String getProductName() {
        return mProduct.getName();
    }

    public void onItemClick(Context context) {
        context.startActivity(new ProductDetailIntent(context, mProduct, false, false));
    }
}
