package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.ObservableBoolean;

import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.listener.OnCompleteListener;
import com.sompom.tookitup.model.result.Category;
import com.sompom.tookitup.newui.FilterCategoryActivity;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.FilterCategoryDataManager;
import com.sompom.tookitup.utils.NetworkStateUtil;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 6/19/18.
 */

public class FilterCategoryFragmentViewModel extends AbsLoadingViewModel {
    public ObservableBoolean mIsEnableSearchButton = new ObservableBoolean();
    private FilterCategoryDataManager mDataManager;
    private OnCompleteListener<List<Category>> mListener;

    public FilterCategoryFragmentViewModel(FilterCategoryDataManager dataManager,
                                           OnCompleteListener<List<Category>> listener) {
        super(dataManager.getContext());
        mDataManager = dataManager;
        mListener = listener;
    }

    public void getData() {
        showLoading();
        Observable<Response<List<Category>>> callBack = mDataManager.getCategoryList();
        ResponseObserverHelper<Response<List<Category>>> helper = new ResponseObserverHelper<>(getContext(), callBack);
        helper.execute(new OnCallbackListener<Response<List<Category>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mIsEnableSearchButton.set(true);
                hideLoading();
                if (ex.getCode() == 404) {
                    showError(getContext().getString(R.string.error_no_category_404));
                } else if (!NetworkStateUtil.isNetworkAvailable(getContext())) {
                    showError(mDataManager.getContext().getString(R.string.error_no_internet_connection_message));
                } else {
                    showError(ex.toString());
                }
            }

            @Override
            public void onComplete(Response<List<Category>> result) {
                mIsEnableSearchButton.set(true);
                hideLoading();
                mListener.onComplete(result.body());
            }
        });
    }

    public void onBackPress() {
        if (getContext() instanceof FilterCategoryActivity) {
            ((FilterCategoryActivity) getContext()).onBackPressed();
        }
    }

    public OnClickListener onRetryButtonClick() {
        return this::getData;
    }
}
