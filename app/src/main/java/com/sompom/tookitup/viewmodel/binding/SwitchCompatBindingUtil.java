package com.sompom.tookitup.viewmodel.binding;

import android.databinding.BindingAdapter;
import android.graphics.Typeface;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.SwitchCompat;

import com.sompom.tookitup.R;

/**
 * Created by nuonveyo on 7/4/18.
 */

public final class SwitchCompatBindingUtil {
    private SwitchCompatBindingUtil() {
    }

    @BindingAdapter("setFont")
    public static void setFont(SwitchCompat switchCompat, boolean isSetFont) {
        if (isSetFont) {
            Typeface regularFont = ResourcesCompat.getFont(switchCompat.getContext(), R.font.sf_pro_regular);
            switchCompat.setTypeface(regularFont);
        }
    }
}
