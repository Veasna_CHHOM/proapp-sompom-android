package com.sompom.tookitup.viewmodel.binding;

import android.databinding.BindingAdapter;
import android.text.TextUtils;

import com.sompom.tookitup.R;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.widget.ImageProfileLayout;
import com.sompom.tookitup.widget.RecipientProfileLayout;

import java.util.List;

/**
 * Created by nuonveyo on 8/22/18.
 */

public final class ImageProfileBindingUtil {
    private ImageProfileBindingUtil() {
    }

    @BindingAdapter(value = {"setUserStatusImageProfile", "simpleUrl"}, requireAll = false)
    public static void setUserStatusImageProfile(ImageProfileLayout layout,
                                                 User user,
                                                 String simpleUrl) {
        if (TextUtils.isEmpty(simpleUrl)) {
            if (user == null) {
                user = new User();
            }
            layout.setUser(user);
        } else {
            layout.loadSimpleUrl(simpleUrl);
        }
    }

    @BindingAdapter({"setUserStatusImageProfileComment", "isReplyComment"})
    public static void setUserStatusImageProfileComment(ImageProfileLayout layout, User user, boolean isReplyComment) {
        if (user == null) {
            user = new User();
        }
        int dimension = layout.getContext().getResources().getDimensionPixelSize(R.dimen.user_profile_chat);
        if (isReplyComment) {
            dimension = layout.getContext().getResources().getDimensionPixelSize(R.dimen.image_seller);
        }
        layout.setUser(user, dimension);
    }

    @BindingAdapter(value = {"participants"})
    public static void setParticipantProfile(RecipientProfileLayout layout, List<User> participants) {
        if (participants != null && !participants.isEmpty()) {
            layout.bindData(participants);
        }
    }
}
