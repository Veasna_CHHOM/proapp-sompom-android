package com.sompom.tookitup.viewmodel.binding;

import android.databinding.BindingAdapter;

import com.sompom.tookitup.listener.OnItemClickListener;
import com.sompom.tookitup.model.emun.FilterSortByItem;
import com.sompom.tookitup.widget.sortby.SortByFlowLayout;

/**
 * Created by nuonveyo on 7/6/18.
 */

public final class SortByFlowLayoutBindingUtil {
    @BindingAdapter({"addItem", "sortPosition", "onItemClickListener"})
    public static void addItem(SortByFlowLayout filterSortByGroup,
                               FilterSortByItem[] items,
                               int sortPosition,
                               OnItemClickListener<FilterSortByItem> listener) {
        filterSortByGroup.removeAllViews();
        for (FilterSortByItem item : items) {
            filterSortByGroup.addView(item, sortPosition);
        }
        filterSortByGroup.setOnItemClickListener(listener);
    }
}
