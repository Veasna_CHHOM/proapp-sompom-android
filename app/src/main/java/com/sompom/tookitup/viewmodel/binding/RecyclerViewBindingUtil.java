package com.sompom.tookitup.viewmodel.binding;

import android.databinding.BindingAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.sompom.tookitup.adapter.newadapter.ErrorAdapterAdapter;
import com.sompom.tookitup.adapter.newadapter.PlaceHolderAdapter;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.model.emun.ErrorLoadingType;

import timber.log.Timber;

/**
 * Created by nuonveyo on 6/28/18.
 */

public final class RecyclerViewBindingUtil {
    private RecyclerViewBindingUtil() {

    }

    @BindingAdapter({"showPlaceHolderAdapter", "placeholderType"})
    public static void showPlaceHolderAdapter(RecyclerView recyclerView, boolean isShow, int placeHolderType) {
        if (isShow) {
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
            PlaceHolderAdapter placeHolderAdapter = new PlaceHolderAdapter(placeHolderType);
            recyclerView.setAdapter(placeHolderAdapter);
        } else {
            if (recyclerView.getAdapter() instanceof PlaceHolderAdapter) {
                recyclerView.setAdapter(null);
            }
        }
    }

    @BindingAdapter(value = {"showErrorType",
            "isShowErrorAdapter",
            "errorMessage",
            "onRetryButtonClickListener",
            "isShowButtonRetry"},
            requireAll = false)
    public static void showErrorAdapter(RecyclerView recyclerView,
                                        ErrorLoadingType errorLoadingType,
                                        boolean isShow,
                                        String errorMessage,
                                        OnClickListener onClickListener,
                                        boolean isShowButtonRetry) {
        if (isShow) {
            if (recyclerView.getVisibility() == View.GONE || recyclerView.getVisibility() == View.INVISIBLE) {
                recyclerView.setVisibility(View.VISIBLE);
            }
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
            ErrorAdapterAdapter adapter = new ErrorAdapterAdapter(errorLoadingType, errorMessage, onClickListener);
            recyclerView.setAdapter(adapter);
            Timber.e("showErrorAdapter show");
        } else {
            if (recyclerView.getAdapter() instanceof ErrorAdapterAdapter) {
                recyclerView.setAdapter(null);
                Timber.e("showErrorAdapter null");

            }
        }
    }

}
