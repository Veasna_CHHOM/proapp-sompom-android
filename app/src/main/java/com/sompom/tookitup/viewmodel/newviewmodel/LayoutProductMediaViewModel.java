package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.ObservableField;

import com.sompom.tookitup.listener.OnItemClickListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.SharedProduct;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.viewmodel.AbsBaseViewModel;

/**
 * Created by nuonveyo on 7/13/18.
 */

public class LayoutProductMediaViewModel extends AbsBaseViewModel {
    public ObservableField<Product> mProduct = new ObservableField<>();
    private OnItemClickListener<Product> mOnItemClickListener;

    public LayoutProductMediaViewModel(Adaptive adaptive,
                                       OnItemClickListener<Product> buttonClickListener) {
        if (adaptive instanceof Product || adaptive instanceof SharedProduct) {
            if (adaptive instanceof Product) {
                mProduct.set((Product) adaptive);
            } else {
                mProduct.set(((SharedProduct) adaptive).getProduct());
            }
        }
        mOnItemClickListener = buttonClickListener;
    }

    public void onProductItemClick() {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onClick(mProduct.get());
        }
    }
}
