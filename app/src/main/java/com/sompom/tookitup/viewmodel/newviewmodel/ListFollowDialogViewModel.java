package com.sompom.tookitup.viewmodel.newviewmodel;

import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.NetworkBroadcastReceiver;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.listener.OnCompleteListListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.emun.FollowItemType;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.BaseObserverHelper;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.StoreDataManager;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 7/10/18.
 */

public class ListFollowDialogViewModel extends AbsLoadingViewModel {
    private final StoreDataManager mDataManager;
    private final User mUser;
    private final Adaptive mAdaptive;
    private final FollowItemType mType;
    private final OnCompleteListListener<List<User>> mListener;
    private boolean mIsLoadedData;
    private NetworkBroadcastReceiver.NetworkListener mNetworkListener;

    public ListFollowDialogViewModel(StoreDataManager dataManager,
                                     Adaptive adaptive,
                                     User user,
                                     FollowItemType type,
                                     OnCompleteListListener<List<User>> listener) {
        super(dataManager.getContext());
        mDataManager = dataManager;
        mUser = user;
        mType = type;
        mListener = listener;
        mAdaptive = adaptive;

        if (dataManager.getContext() instanceof AbsBaseActivity) {
            mNetworkListener = networkState -> {
                if (!mIsLoadedData) {
                    if (networkState == NetworkBroadcastReceiver.NetworkState.Connected) {
                        getListFollow();
                    } else if (networkState == NetworkBroadcastReceiver.NetworkState.Disconnected) {
                        showNetworkError();
                    }
                }

            };
            ((AbsBaseActivity) dataManager.getContext()).addNetworkStateChangeListener(mNetworkListener);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).removeStateChangeListener(mNetworkListener);
        }
    }

    public String getTitle() {
        if (mType == FollowItemType.FOLLOWER) {
            return getContext().getString(R.string.seller_store_followers_title);
        } else if (mType == FollowItemType.FOLLOWING) {
            return getContext().getString(R.string.seller_store_following_title);
        } else {
            return getContext().getString(R.string.seller_store_user_title);
        }
    }

    private void getListFollow() {
        showLoading();
        Observable<List<User>> call;
        if (mType == FollowItemType.FOLLOWER) {
            call = mDataManager.getFollower(mUser.getId());
        } else if (mType == FollowItemType.FOLLOWING) {
            call = mDataManager.getFollowing(mUser.getId());
        } else if (mType == FollowItemType.LIKE_VIEWER) {
            call = mDataManager.getUserLike(mAdaptive.getId());
        } else {
            call = mDataManager.getUserView(mAdaptive.getId());
        }
        BaseObserverHelper<List<User>> helper = new BaseObserverHelper<>(getContext(), call);
        addDisposable(helper.execute(new OnCallbackListener<List<User>>() {
            @Override
            public void onComplete(List<User> result) {
                if (result == null || result.isEmpty()) {
                    onFail(new ErrorThrowable(404, null));
                } else {
                    hideLoading();
                    mIsLoadedData = true;
                    if (mListener != null) {
                        mListener.onComplete(result, mDataManager.isCanLoadMore());
                    }
                }
            }

            @Override
            public void onFail(ErrorThrowable ex) {
                if (mIsLoadedData) {
                    return;
                }

                if (ex.getCode() == 404) {
                    showError(getContext().getString(R.string.error_no_store_found));
                } else {
                    showError(ex.toString());
                }
            }
        }));
    }

    public void loadMore(OnCallbackListListener<List<User>> listOnCallbackListener) {
        Observable<List<User>> call;
        if (mType == FollowItemType.FOLLOWER) {
            call = mDataManager.getFollower(mUser.getId());
        } else {
            call = mDataManager.getFollowing(mUser.getId());
        }

        BaseObserverHelper<List<User>> helper = new BaseObserverHelper<>(getContext(), call);
        addDisposable(helper.execute(new OnCallbackListener<List<User>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                listOnCallbackListener.onFail(ex);
            }

            @Override
            public void onComplete(List<User> result) {
                listOnCallbackListener.onComplete(result, mDataManager.isCanLoadMore());
            }
        }));
    }

    public void onFollowClick(User user, boolean isFollow) {
        Observable<Response<Object>> callback;
        if (isFollow) {
            callback = mDataManager.postFollow(user.getId());
        } else {
            callback = mDataManager.unFollow(user.getId());
        }
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute());
    }

    public OnClickListener onRetryButtonClick() {
        return this::getListFollow;
    }
}
