package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.text.TextUtils;
import android.view.View;

import com.sompom.tookitup.R;
import com.sompom.tookitup.helper.AnimationHelper;
import com.sompom.tookitup.helper.LoginActivityResultCallback;
import com.sompom.tookitup.helper.NavigateSellerStoreHelper;
import com.sompom.tookitup.listener.OnLikeItemListener;
import com.sompom.tookitup.model.emun.FollowItemType;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.utils.FormatNumber;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.AbsBaseViewModel;

/**
 * Created by nuonveyo on 6/8/18.
 */

public class ListItemLikeViewModel extends AbsBaseViewModel {
    public final ObservableField<String> mFollow = new ObservableField<>();
    public final ObservableBoolean mIsFollowing = new ObservableBoolean();
    public final ObservableBoolean mVisibleButtonFollow = new ObservableBoolean();
    public final User mUser;
    private final Context mContext;
    private final OnLikeItemListener mListener;
    private final FollowItemType mShowFollowType;

    public ListItemLikeViewModel(Context context,
                                 String myUserID,
                                 User likeViewerModel,
                                 FollowItemType showFollowType,
                                 OnLikeItemListener likeItemListener) {
        mContext = context;
        mUser = likeViewerModel;
        mShowFollowType = showFollowType;
        mListener = likeItemListener;
        mIsFollowing.set(mUser.isFollow());
        mVisibleButtonFollow.set(TextUtils.isEmpty(myUserID) || !TextUtils.equals(mUser.getId(), myUserID));
        getFollow();
    }

    private void getFollow() {
        long totalFollower = mUser.getContentStat().getTotalFollowers();
        StringBuilder followerCount = new StringBuilder();
        followerCount
                .append(FormatNumber.format(totalFollower))
                .append(" ");

        if (totalFollower > 1) {
            followerCount.append(mContext.getString(R.string.seller_store_followers_title));
        } else {
            followerCount.append(mContext.getString(R.string.suggestion_follower));
        }

        StringBuilder followingCount = new StringBuilder();
        long following = mUser.getContentStat().getTotalFollowings();
        followingCount
                .append(FormatNumber.format(following))
                .append(" ")
                .append(mContext.getString(R.string.seller_store_following_title));

        if (mShowFollowType == FollowItemType.FOLLOWER) {
            mFollow.set(followerCount.toString());
        } else if (mShowFollowType == FollowItemType.FOLLOWING) {
            mFollow.set(followingCount.toString());
        } else {
            // Show both follower and following
            followerCount
                    .append(" - ")
                    .append(followingCount);
            mFollow.set(followerCount.toString());
        }
    }

    public void onFollowButtonClick(View view) {
        AnimationHelper.animateBounceFollowButton(view, 600, 0.15f, 10.0f);
        if (mContext instanceof AbsBaseActivity) {
            ((AbsBaseActivity) mContext).startLoginWizardActivity(new LoginActivityResultCallback() {
                @Override
                public void onActivityResultSuccess(boolean isAlreadyLogin) {
                    if (!isAlreadyLogin) {
                        if (!SharedPrefUtils.getUserId(mContext).equals(mUser.getId())) {
                            mIsFollowing.set(!mIsFollowing.get());
                            mUser.setFollow(mIsFollowing.get());
                            mUser.getContentStat().setTotalFollowers(getFollowingCounter(mUser.getContentStat().getTotalFollowers()));
                            getFollow();
                            if (mListener != null) {
                                mListener.onFollowClick(mUser, mIsFollowing.get());
                                mListener.onNotifyItem();
                            }
                        } else {
                            mVisibleButtonFollow.set(false);
                            if (mListener != null) {
                                mListener.onNotifyItem();
                            }
                        }
                    } else {
                        mIsFollowing.set(!mIsFollowing.get());
                        mUser.setFollow(mIsFollowing.get());
                        mUser.getContentStat().setTotalFollowers(getFollowingCounter(mUser.getContentStat().getTotalFollowers()));
                        getFollow();
                        if (mListener != null) {
                            mListener.onFollowClick(mUser, mIsFollowing.get());
                        }
                    }
                }
            });
        }
    }

    private long getFollowingCounter(long followingCounter) {
        if (mIsFollowing.get()) {
            return followingCounter + 1;
        } else {
            long count = followingCounter - 1;
            if (count < 0) {
                count = 0;
            }
            return count;
        }
    }

    public void onUserProfileClick() {
        new NavigateSellerStoreHelper(mContext, mUser).openSellerStore();
    }
}
