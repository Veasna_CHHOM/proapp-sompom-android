package com.sompom.tookitup.viewmodel.binding;

import android.databinding.BindingAdapter;

import com.sompom.tookitup.listener.OnItemClickListener;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.emun.ChatBg;
import com.sompom.tookitup.widget.chat.MultiImageChatView;

import java.util.List;

/**
 * Created by nuonveyo on 8/30/18.
 */

public final class MultiImageChatViewBindingUtil {
    private MultiImageChatViewBindingUtil() {
    }

    @BindingAdapter(value = {"bindImageView", "onImageClickListener", "onLongClickListener", "backgroundType"})
    public static void bindImageView(MultiImageChatView view,
                                     List<Media> list,
                                     OnItemClickListener<Integer> onItemClickListener,
                                     MultiImageChatView.OnLongClickListener onLongClickListener,
                                     ChatBg chatBg) {
        view.generateLayout(list, onItemClickListener, onLongClickListener, chatBg);
    }
}
