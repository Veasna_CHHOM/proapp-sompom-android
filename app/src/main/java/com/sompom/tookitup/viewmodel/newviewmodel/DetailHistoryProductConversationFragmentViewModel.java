package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.ObservableBoolean;

import com.sompom.tookitup.broadcast.NetworkBroadcastReceiver;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.DetailHistoryProductConversationActivity;
import com.sompom.tookitup.observable.BaseObserverHelper;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.datamanager.ProductHistoryConversationDataManager;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by nuonveyo on 7/2/18.
 */

public class DetailHistoryProductConversationFragmentViewModel extends AbsLoadingViewModel {
    public final ObservableBoolean mIsRefresh = new ObservableBoolean();
    public final Product mProduct;
    private final ProductHistoryConversationDataManager mDataManager;
    private final OnCallbackListListener<List<Conversation>> mListener;
    private NetworkBroadcastReceiver.NetworkListener mNetworkStateListener;
    private boolean mIsLoadedData;

    public DetailHistoryProductConversationFragmentViewModel(ProductHistoryConversationDataManager dataManager,
                                                             Product product,
                                                             OnCallbackListListener<List<Conversation>> listListener) {
        super(dataManager.getContext());
        mDataManager = dataManager;
        mProduct = product;
        mListener = listListener;

        if (getContext() instanceof AbsBaseActivity) {
            mNetworkStateListener = networkState -> {
                if (!mIsLoadedData) {
                    if (networkState == NetworkBroadcastReceiver.NetworkState.Connected) {
                        getData();
                    } else if (networkState == NetworkBroadcastReceiver.NetworkState.Disconnected) {
                        showNetworkError();
                    }
                }
            };
            ((AbsBaseActivity) getContext()).addNetworkStateChangeListener(mNetworkStateListener);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).removeStateChangeListener(mNetworkStateListener);
        }
    }

    @Override
    public void onBackButtonClick() {
        if (getContext() instanceof DetailHistoryProductConversationActivity) {
            ((DetailHistoryProductConversationActivity) getContext()).onBackPressed();
        }
    }

    public String getName() {
        return mProduct.getName();
    }

    public void onRefresh() {
        mIsRefresh.set(true);
        getData();
    }

    public void loadMore(OnCallbackListListener<List<Conversation>> callbackListener) {
        Observable<List<Conversation>> callback = mDataManager.getUserWhoChatWithProduct(mProduct.getId());
        BaseObserverHelper<List<Conversation>> helper = new BaseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<List<Conversation>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                callbackListener.onFail(ex);
            }

            @Override
            public void onComplete(List<Conversation> result) {
                callbackListener.onComplete(result, mDataManager.isCanLoadMore());
            }
        }));
    }

    private void getData() {
        if (!mIsRefresh.get()) {
            showLoading();
        }
        Observable<List<Conversation>> callback = mDataManager.getUserWhoChatWithProduct(mProduct.getId());
        BaseObserverHelper<List<Conversation>> helper = new BaseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<List<Conversation>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (mIsLoadedData) {
                    mIsRefresh.set(false);
                    return;
                }

                mIsLoadedData = false;
                mIsRefresh.set(false);
                hideLoading();
                showError(ex.toString());
                mListener.onFail(ex);
            }

            @Override
            public void onComplete(List<Conversation> result) {
                hideLoading();
                mIsLoadedData = true;
                mIsRefresh.set(false);
                mListener.onComplete(result, mDataManager.isCanLoadMore());
            }
        }));
    }

    public OnClickListener onRetryButtonClick() {
        return this::getData;
    }
}
