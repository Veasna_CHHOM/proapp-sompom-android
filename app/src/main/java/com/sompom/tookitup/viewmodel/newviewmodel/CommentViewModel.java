package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;

import com.sompom.tookitup.R;
import com.sompom.tookitup.helper.AnimationHelper;
import com.sompom.tookitup.helper.NavigateSellerStoreHelper;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.listener.OnCommentItemClickListener;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.emun.MediaType;
import com.sompom.tookitup.model.result.Comment;
import com.sompom.tookitup.model.result.ContentStat;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.utils.DateTimeUtils;
import com.sompom.tookitup.utils.FormatNumber;
import com.sompom.tookitup.utils.RenderHashTagUtil;
import com.sompom.tookitup.viewmodel.AbsBaseViewModel;

/**
 * Created by He Rotha on 9/15/17.
 */

public class CommentViewModel extends AbsBaseViewModel {
    public final ObservableBoolean mIsShowReply = new ObservableBoolean(true);
    public final ObservableBoolean mIsLike = new ObservableBoolean();
    public final ObservableField<String> mLikeCount = new ObservableField<>();
    public final ObservableField<String> mHour = new ObservableField<>();
    public final ObservableBoolean mIsShowLikeCounter = new ObservableBoolean();
    public final ObservableBoolean mIsReplyCommentItem = new ObservableBoolean();
    public final ObservableField<User> mUser = new ObservableField<>();
    private final Comment mComment;
    private final OnCommentItemClickListener mOnCommentItemClickListener;
    private final int mPosition;
    private final Context mContext;
    private final ContentStat mContentStat;

    public CommentViewModel(Context context,
                            int position,
                            Comment comment,
                            OnCommentItemClickListener onCommentItemClickListener) {
        mContext = context;
        mComment = comment;
        mContentStat = mComment.getContentStat();
        mUser.set(comment.getUser());

        mIsLike.set(mComment.isLike());
        mPosition = position;
        mOnCommentItemClickListener = onCommentItemClickListener;

        if (mComment.isPosting()) {
            mHour.set(mContext.getString(R.string.product_form_notification_posting));
        } else {
            mHour.set(DateTimeUtils.parse(mContext, mComment.getDate().getTime()));
        }

        if (mContentStat.getTotalLikes() > 0) {
            mIsShowLikeCounter.set(true);
            if (mContentStat.getTotalLikes() > 1) {
                mLikeCount.set(FormatNumber.format(mContentStat.getTotalLikes()));
            }
        }
    }

    public CommentViewModel(Context context,
                            int position,
                            Comment comment,
                            OnCommentItemClickListener onCommentItemClickListener,
                            boolean isReplyComment) {
        this(context, position, comment, onCommentItemClickListener);
        mIsReplyCommentItem.set(isReplyComment);
        mIsShowReply.set(false);
    }

    public Spannable getMessage() {
        if (!TextUtils.isEmpty(mComment.getContent())) {
            return RenderHashTagUtil.renderMentionUser(mContext, mComment.getContent());
        }
        return new SpannableString("");
    }

    public void onReplyCommentClick() {
        if (!mComment.isPosting() && mOnCommentItemClickListener != null) {
            mOnCommentItemClickListener.onReplyButtonClick(mComment, mPosition);
        }
    }

    public void onLikeClick(View view) {
        if (!mComment.isPosting()) {
            AnimationHelper.animateBounce(view);
            long like;
            if (mIsLike.get()) {
                like = mContentStat.getTotalLikes() - 1;
                mIsLike.set(false);
            } else {
                like = mContentStat.getTotalLikes() + 1;
                mIsLike.set(true);
            }
            mContentStat.setTotalLikes(like);
            mComment.setContentStat(mContentStat);
            mComment.setLike(mIsLike.get());
            if (mContentStat.getTotalLikes() > 0) {
                mIsShowLikeCounter.set(true);
                if (mContentStat.getTotalLikes() > 1) {
                    mLikeCount.set(FormatNumber.format(mContentStat.getTotalLikes()));
                } else {
                    mLikeCount.set(null);
                }
            } else {
                mIsShowLikeCounter.set(false);
            }
            if (mOnCommentItemClickListener != null) {
                mOnCommentItemClickListener.onLikeButtonClick(mComment, mPosition);
            }
        }
    }

    public void onUserProfileClick() {
        new NavigateSellerStoreHelper(mContext, mUser.get()).openSellerStore();
    }

    public Media getGifMedia() {
        return mComment.getMedia().get(0);
    }

    public String getViaGifText() {
        if (mComment.getMedia().get(0).getType() == MediaType.TENOR_GIF) {
            return mContext.getString(R.string.chat_tenor_gif);
        } else {
            return "";
        }
    }

    public int getImageCornerRadius() {
        return mContext.getResources().getDimensionPixelSize(R.dimen.space_small);
    }

    public OnClickListener onLongPress() {
        return () -> {
            if (!mComment.isPosting() && mOnCommentItemClickListener != null) {
                mOnCommentItemClickListener.onLongPress(mComment, mPosition);
            }
        };
    }
}
