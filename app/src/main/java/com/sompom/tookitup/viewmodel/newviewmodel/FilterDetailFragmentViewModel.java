package com.sompom.tookitup.viewmodel.newviewmodel;

import android.app.Activity;
import android.content.Context;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;

import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnCompleteListener;
import com.sompom.tookitup.listener.OnItemClickListener;
import com.sompom.tookitup.model.Price;
import com.sompom.tookitup.model.emun.ChooseDistanceItem;
import com.sompom.tookitup.model.emun.FilterSortByItem;
import com.sompom.tookitup.model.emun.Range;
import com.sompom.tookitup.model.request.QueryProduct;
import com.sompom.tookitup.model.result.Category;
import com.sompom.tookitup.newui.FilterDetailActivity;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.PriceDataManager;
import com.sompom.tookitup.utils.CurrencyUtils;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;
import com.sompom.tookitup.widget.MyMultiSlider;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 7/5/18.
 */

public class FilterDetailFragmentViewModel extends AbsLoadingViewModel {
    public final ObservableField<String> mMinPriceSelected = new ObservableField<>();
    public final ObservableField<String> mMaxPriceSelected = new ObservableField<>();
    public final ObservableField<String> mMaxValuePrice = new ObservableField<>();
    public final ObservableInt mSlideBarMinValue = new ObservableInt();
    public final ObservableInt mSlideBarMaxValue = new ObservableInt();
    private final Context mContext;
    private int mSlideBarMinSaving;
    private int mSlideBarMaxSaving;
    private List<Category> mCategories;
    private String mChooseDistanceType;
    private int mSortId;
    private double mMinPrice;
    private double mMaxPrice;
    private String mCurrency;
    private Price mPrice;
    private PriceDataManager mPriceDataManager;
    private OnCompleteListener<Price> mPriceOnCompleteListener;
    private OnCompleteListener<QueryProduct> mQueryProductOnCompleteListener;


    public FilterDetailFragmentViewModel(Context context,
                                         PriceDataManager dataManager,
                                         OnCompleteListener<QueryProduct> listener,
                                         OnCompleteListener<Price> priceOnCompleteListener) {
        super(context);
        mContext = context;
        mPriceDataManager = dataManager;
        mPriceOnCompleteListener = priceOnCompleteListener;
        mQueryProductOnCompleteListener = listener;
        showLoading();
        getMinMaxPrice();
    }

    private void getMinMaxPrice() {
        if (mPrice == null) {
            Observable<Response<Price>> ob = mPriceDataManager.getPrice();
            ResponseObserverHelper<Response<Price>> call = new ResponseObserverHelper<>(mContext, ob);
            call.execute(new OnCallbackListener<Response<Price>>() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    showError(ex.getMessage());
                }

                @Override
                public void onComplete(Response<Price> result) {
                    mPrice = result.body();
                    mPriceOnCompleteListener.onComplete(mPrice);

                    if (mPrice.getMaxPrice() < Range.RANGE_10.getValue()) {
                        mMaxValuePrice.set(CurrencyUtils.formatPrice(Range.RANGE_10.getValue()));
                    } else {
                        mMaxValuePrice.set(CurrencyUtils.formatPrice(mPrice.getMaxPrice()));
                    }

                    QueryProduct query = SharedPrefUtils.getQueryProduct(mContext);
                    setData(query);
                    mQueryProductOnCompleteListener.onComplete(query);
                    hideLoading();
                }
            });
        }
    }

    @Override
    public void onRetryClick() {
        super.onRetryClick();
        getMinMaxPrice();
    }

    public void onBackPress() {
        if (mContext instanceof FilterDetailActivity) {
            ((FilterDetailActivity) mContext).onBackPressed();
        }
    }

    public ChooseDistanceItem[] getItems() {
        return ChooseDistanceItem.values();
    }

    public OnItemClickListener<ChooseDistanceItem> getOnChooseDistanceClick() {
        return item -> mChooseDistanceType = mContext.getString(ChooseDistanceItem.getValueFromId(item.getId()).getType());
    }

    public void setCategories(List<Category> categories) {
        mCategories = categories;
    }

    public int getChooseDistancePosition() {
        return ChooseDistanceItem.getChooseDistanceItemFromType(mContext, mChooseDistanceType).getId();
    }

    public FilterSortByItem[] getSortItems() {
        return FilterSortByItem.values();
    }

    public int getSelectedSortPosition() {
        return FilterSortByItem.getValueFromIndex(mSortId).getId();
    }

    public OnItemClickListener<FilterSortByItem> onSortItemClick() {
        return item -> mSortId = item.getId();
    }

    public void onResetButtonClick() {
        setData(new QueryProduct(mContext));
    }

    public void onApplyButtonClick() {
        //TODO: call api filter detail
        saveSelectionData();
        if (mContext instanceof FilterDetailActivity) {
            ((FilterDetailActivity) mContext).setResult(Activity.RESULT_OK);
            ((FilterDetailActivity) mContext).finish();
        }
    }

    public void setMinPrice(int scrollValue, double price) {
        mMinPrice = scrollValue;
        mSlideBarMinSaving = scrollValue;
        mMinPriceSelected.set(getPrice(price));
    }

    public void setMaxPrice(int scrollValue, double price) {
        mMaxPrice = price;
        mSlideBarMaxSaving = scrollValue;
        mMaxPriceSelected.set(getPrice(price));
    }

    private String getPrice(double value) {
        return CurrencyUtils.formatPrice(value, mCurrency);
    }

    private void setData(QueryProduct queryProduct) {
        if (queryProduct == null) {
            queryProduct = new QueryProduct(mContext);
        } else {
            mSortId = queryProduct.getSort();
            mChooseDistanceType = queryProduct.getDistanceOption();
        }
        mCurrency = queryProduct.getCurrency(mContext);

        if (queryProduct.getSlideMin() == 0 && queryProduct.getSlideMax() == 0) {
            queryProduct.setSlideMin(MyMultiSlider.MIN_SCROLL);
            queryProduct.setMinPrice(mPrice.getMinPrice());

            queryProduct.setSlideMax(MyMultiSlider.MAX_SCROLL);
            queryProduct.setMaxPrice(mPrice.getMaxPrice());
        }

        mMinPrice = queryProduct.getMinPrice();
        mMaxPrice = queryProduct.getMaxPrice();
        mSlideBarMinValue.set(queryProduct.getSlideMin());
        mSlideBarMaxValue.set(queryProduct.getSlideMax());
        mSlideBarMinSaving = queryProduct.getSlideMin();
        mSlideBarMaxSaving = queryProduct.getSlideMin();
        mCategories = queryProduct.getCategories();
        notifyChange();
    }

    private void saveSelectionData() {
        QueryProduct queryProduct = new QueryProduct(mContext);
        queryProduct.setSort(mSortId);
        queryProduct.setDistanceOption(mChooseDistanceType);
        queryProduct.setSlideMin(mSlideBarMinSaving);
        queryProduct.setSlideMax(mSlideBarMaxSaving);
        queryProduct.setMinPrice(mMinPrice);
        queryProduct.setMaxPrice(mMaxPrice);
        queryProduct.setCategories(mCategories);
        SharedPrefUtils.setQueryProduct(queryProduct, mContext);
    }
}
