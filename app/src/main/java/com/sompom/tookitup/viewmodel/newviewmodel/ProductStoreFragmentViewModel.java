package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.NetworkBroadcastReceiver;
import com.sompom.tookitup.intent.newintent.ProductFormIntent;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.listener.OnCompleteListener;
import com.sompom.tookitup.listener.OnEditDialogClickListener;
import com.sompom.tookitup.model.emun.AccountStatus;
import com.sompom.tookitup.model.emun.Status;
import com.sompom.tookitup.model.emun.StoreStatus;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.ProductStoreDataManager;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 6/5/18.
 */

public class ProductStoreFragmentViewModel extends AbsLoadingViewModel implements OnEditDialogClickListener {
    public final ObservableBoolean mIsRefresh = new ObservableBoolean();
    public final ObservableField<String> mActionErrorMessage = new ObservableField<>();
    private final ProductStoreDataManager mDataManager;
    private final Status mProductStatus;
    private final User mUser;
    private final OnCallback mCallback;
    private boolean mIsReadUserDataReady;
    private final OnCallbackListener<Response<List<Product>>> mProductCallback = new OnCallbackListener<Response<List<Product>>>() {
        @Override
        public void onFail(ErrorThrowable ex) {
            if (mIsReadUserDataReady) {
                mIsRefresh.set(false);
                hideLoading();
                return;
            }

            mIsReadUserDataReady = false;
            mIsRefresh.set(false);
            if (ex.getCode() == 404) {
                showNoItemError(getContext().getString(R.string.error_product_list_404));
            } else {
                showError(ex.toString());
            }
        }

        @Override
        public void onComplete(Response<List<Product>> result) {
            List<Product> productList = result.body();
            if (productList == null || productList.isEmpty()) {
                //if there is no product, result equals 404
                onFail(new ErrorThrowable(404, null));
                return;
            }
            mIsReadUserDataReady = true;
            mIsRefresh.set(false);
            hideLoading();
            if (mCallback != null) {
                mCallback.onGetProductSuccess(productList, mDataManager.isCanLoadMore());
            }

        }
    };
    private NetworkBroadcastReceiver.NetworkListener mNetworkStateListener;

    public ProductStoreFragmentViewModel(ProductStoreDataManager dataManager,
                                         Status status,
                                         User user,
                                         OnCallback onCallbackListener) {
        super(dataManager.getContext());
        showLoading();
        mDataManager = dataManager;
        mUser = user;
        mProductStatus = status;
        mCallback = onCallbackListener;

        if (getContext() instanceof AbsBaseActivity) {
            mNetworkStateListener = networkState -> {
                if (!mIsReadUserDataReady) {
                    if (networkState == NetworkBroadcastReceiver.NetworkState.Connected) {
                        getData();
                    } else if (networkState == NetworkBroadcastReceiver.NetworkState.Disconnected) {
                        showNetworkError();
                    }
                }
            };
            ((AbsBaseActivity) getContext()).addNetworkStateChangeListener(mNetworkStateListener);
        }
    }

    public void setReadUserDataReady() {
        mIsReadUserDataReady = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).removeStateChangeListener(mNetworkStateListener);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mUser.getId().equals(SharedPrefUtils.getUserId(getContext()))) {
            AccountStatus accountStatus = SharedPrefUtils.getAccountStatus(getContext());
            if (accountStatus == AccountStatus.BLOCK) {
                checkStoreBlock();
            }
        }
    }

    private void checkStoreBlock() {
        mButtonRetryText.set(R.string.seller_store_contact_us_title);
        showError(getContext().getString(R.string.seller_store_your_store_is_blocked_please_contact_our_email_message));
    }

    public void onRefresh() {
        clearDisposable();
        mIsRefresh.set(true);
        getData();
    }

    public void getData() {
        if (!mIsRefresh.get()) {
            showLoading();
        }
        String myUserId = SharedPrefUtils.getUserId(getContext());
        if (mUser.getId().equals(myUserId)) {
            StoreStatus status = SharedPrefUtils.getStatus(getContext());
            mUser.setStatus(status);

            if (mUser.getAccountStatus() == AccountStatus.BLOCK) {
                checkStoreBlock();
                return;
            }
        } else if (mUser.getStatus() != StoreStatus.ACTIVE || mUser.getAccountStatus() == AccountStatus.BLOCK) {
            showError(getContext().getString(R.string.seller_store_store_is_deactivated_message));
            mIsShowButtonRetry.set(false);
            return;
        }
        Observable<Response<List<Product>>> call = mDataManager.getProductByUserId(mUser, mProductStatus.getStatusProduct());
        ResponseObserverHelper<Response<List<Product>>> helper
                = new ResponseObserverHelper<>(mDataManager.getContext(), call);
        addDisposable(helper.execute(mProductCallback));
    }

    public void loadMore(OnCallbackListListener<List<Product>> onCallback) {
        Observable<Response<List<Product>>> call = mDataManager.loadMore();
        ResponseObserverHelper<Response<List<Product>>> helper
                = new ResponseObserverHelper<>(mDataManager.getContext(), call);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<Product>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                onCallback.onFail(ex);
            }

            @Override
            public void onComplete(Response<List<Product>> result) {
                onCallback.onComplete(result.body(), mDataManager.isCanLoadMore());
            }
        }));
    }

    @Override
    public void onEditClick(Product product) {
        if (getContext() instanceof AbsBaseActivity) {
            getContext().startActivity(new ProductFormIntent(getContext(), product));
        }
    }

    @Override
    public void onDeleteClick(Product product) {
        showLoading();
        Observable<Response<Object>> call = mDataManager.delete(product);
        ResponseObserverHelper<Response<Object>> helper
                = new ResponseObserverHelper<>(mDataManager.getContext(), call);
        addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                    mActionErrorMessage.set(ex.toString());
                } else {
                    mActionErrorMessage.set(getContext().getString(R.string.error_cannot_delete_item));
                }

            }

            @Override
            public void onComplete(Response<Object> result) {
                hideLoading();
                mCallback.onItemRemoved(product);
            }
        }));
    }

    public void cloneProduct(Product product, OnCompleteListener<Product> onProductCloneCompletedListener) {
        showLoading();
        Observable<Response<Product>> observable = mDataManager.closeProduct(product.getId());
        ResponseObserverHelper<Response<Product>> helper = new ResponseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(new OnCallbackListener<Response<Product>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
            }

            @Override
            public void onComplete(Response<Product> result) {
                getData();
                if (onProductCloneCompletedListener != null) {
                    onProductCloneCompletedListener.onComplete(result.body());
                }
            }
        }));
    }

    public void checkToPostLike(Product product) {
        Observable<Response<Object>> call = mDataManager.postLikeProduct(product.getId(), product.isLike());
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(mDataManager.getContext(), call);
        addDisposable(helper.execute());
    }

    public OnClickListener onRetryButtonClick() {
        return () -> {
            if (mUser.getAccountStatus() == AccountStatus.BLOCK &&
                    mUser.getId().equals(SharedPrefUtils.getUserId(getContext()))) {
                mCallback.onContactSupportEmailClick();
                return;
            }
            getData();
        };
    }

    public interface OnCallback {
        void onGetProductSuccess(List<Product> list, boolean isCanloadMore);

        void onItemRemoved(Product product);

        void onContactSupportEmailClick();
    }
}
