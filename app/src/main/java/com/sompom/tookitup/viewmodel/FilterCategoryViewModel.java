package com.sompom.tookitup.viewmodel;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.widget.Toast;

import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.OnCategoryClickListener;
import com.sompom.tookitup.model.result.Category;

import java.util.List;

/**
 * Created by He Rotha on 9/13/17.
 */

public class FilterCategoryViewModel {
    public ObservableBoolean mVisibleIcon = new ObservableBoolean();
    private boolean mIsDisableMultiClick;
    private Category mCategory;
    private List<Category> mAllCategories;
    private OnCategoryClickListener mOnCategoryClickListener;

    public FilterCategoryViewModel(Category category,
                                   List<Category> categories,
                                   boolean isDisableClick) {
        mCategory = category;
        mAllCategories = categories;
        mIsDisableMultiClick = isDisableClick;

        if (mCategory.isCheck()) {
            mVisibleIcon.set(true);
        } else {
            mVisibleIcon.set(false);
        }
    }

    public void setOnCategoryClickListener(OnCategoryClickListener onCategoryClickListener) {
        mOnCategoryClickListener = onCategoryClickListener;
    }

    public String getCategoryName() {
        return mCategory.getName();
    }

    public void onItemClick(Context context) {
        if (!mIsDisableMultiClick) {
            boolean isCheck = !mCategory.isCheck();
            if (!isCheck) {
                mCategory.setCheck(false);
                for (Category ca : mAllCategories) {
                    if (ca.getId().equalsIgnoreCase(mCategory.getId())) {
                        ca.setCheck(false);
                        break;
                    }
                }
            } else {
                int categoryAllCount = getCheckCount(mAllCategories);
                if (categoryAllCount >= 3) {
                    Toast.makeText(context, R.string.max_category, Toast.LENGTH_SHORT).show();
                } else {
                    mCategory.setCheck(true);
                    for (Category ca : mAllCategories) {
                        if (ca.getId().equalsIgnoreCase(mCategory.getId())) {
                            ca.setCheck(true);
                            break;
                        }
                    }
                }
            }
            if (mCategory.isCheck()) {
                mVisibleIcon.set(true);
            } else {
                mVisibleIcon.set(false);
            }
        }
        if (mOnCategoryClickListener != null) {
            mOnCategoryClickListener.onCategoryClick(mCategory);
        }
    }

    private int getCheckCount(List<Category> categories) {
        int checkCount = 0;
        for (Category category : categories) {
            if (category.isCheck()) {
                checkCount++;
            }
        }
        return checkCount;
    }
}
