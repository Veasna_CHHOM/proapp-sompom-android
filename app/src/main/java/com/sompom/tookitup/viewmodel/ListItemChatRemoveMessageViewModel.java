package com.sompom.tookitup.viewmodel;

import android.app.Activity;
import android.databinding.ObservableField;

import com.sompom.tookitup.listener.OnChatItemListener;
import com.sompom.tookitup.model.SelectedChat;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.utils.ChatUtility;

/**
 * Created by nuonveyo
 * on 3/6/19.
 */
public class ListItemChatRemoveMessageViewModel extends ChatMediaViewModel {
    public final ObservableField<String> mRemoveMessage = new ObservableField<>();

    public ListItemChatRemoveMessageViewModel(Activity context,
                                              Conversation conversation,
                                              Chat chat,
                                              ChatUtility.GroupMessage groupMessage,
                                              int position,
                                              SelectedChat selectChat,
                                              OnChatItemListener listener) {
        super(context, conversation, chat, groupMessage, position, selectChat, listener);
        mRemoveMessage.set(chat.getActualContent(context));
    }
}
