package com.sompom.tookitup.viewmodel;

import android.content.Context;
import android.databinding.ObservableBoolean;

/**
 * Created by nuonveyo on 6/13/18.
 */

public class LayoutToolbarEditProfileViewModel extends AbsLoadingViewModel {
    //Set true unless user has changed the info in edit profile screen
    public ObservableBoolean mIsShowButtonSave = new ObservableBoolean();

    public LayoutToolbarEditProfileViewModel(Context context) {
        super(context);
    }

    public void onSaveButtonClick() {

    }
}
