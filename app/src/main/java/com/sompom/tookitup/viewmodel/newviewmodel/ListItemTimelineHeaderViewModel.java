package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;
import android.databinding.Bindable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;

import com.sompom.tookitup.R;
import com.sompom.tookitup.helper.LoginActivityResultCallback;
import com.sompom.tookitup.helper.NavigateSellerStoreHelper;
import com.sompom.tookitup.helper.WallStreetIntentData;
import com.sompom.tookitup.listener.OnTimelineHeaderItemClickListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.LifeStream;
import com.sompom.tookitup.model.WallStreetAdaptive;
import com.sompom.tookitup.model.WallStreetAds;
import com.sompom.tookitup.model.emun.PublishItem;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.utils.DateTimeUtils;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.utils.SpannableUtil;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;
import com.sompom.tookitup.widget.ImageProfileLayout;
import com.sompom.tookitup.widget.WallStreetDescriptionTextView;

import java.util.Date;

/**
 * Created by He Rotha on 7/10/18.
 */
public class ListItemTimelineHeaderViewModel extends AbsLoadingViewModel {
    public static final int CLICK_ON_TEXT_POSITION = -1;
    public final ObservableBoolean mVisibleButtonFollow = new ObservableBoolean();
    public final ObservableField<User> mUser = new ObservableField<>();
    public final ObservableBoolean mIsFollowing = new ObservableBoolean();
    public final ObservableField<String> mTitle = new ObservableField<>();
    public final ObservableField<String> mShareTextTitle = new ObservableField<>();
    public final ObservableField<String> mAdsTitle = new ObservableField<>();
    public final ObservableField<String> mSimpleImageUrl = new ObservableField<>();
    public final ObservableBoolean mIsShowMoreButton = new ObservableBoolean(true);
    public final ObservableBoolean mIsSharedHeader = new ObservableBoolean();
    public final ObservableBoolean mIsProductItem = new ObservableBoolean();
    public final ObservableBoolean mIsSetMaxLine = new ObservableBoolean(true);
    public final ObservableField<ImageProfileLayout.RenderType> mRenderType = new ObservableField<>(ImageProfileLayout.RenderType.USER);

    protected final int mPosition;
    protected final AbsBaseActivity mContext;
    private final OnTimelineHeaderItemClickListener mOnItemClickListener;
    private Adaptive mAdaptive;
    private boolean mIsDisableClickOnUser;
    private boolean mIsLiveHeader = false;

    public ListItemTimelineHeaderViewModel(AbsBaseActivity context,
                                           Adaptive adaptive,
                                           int position,
                                           OnTimelineHeaderItemClickListener onItemClickListener) {
        super(context);
        mContext = context;
        mPosition = position;
        mOnItemClickListener = onItemClickListener;
        mAdaptive = adaptive;
        init();
    }

    public void init() {
        if (mAdaptive instanceof Product) {
            mRenderType.set(ImageProfileLayout.RenderType.SHOP);
            mIsProductItem.set(true);
        } else {
            mRenderType.set(ImageProfileLayout.RenderType.USER);
            mIsProductItem.set(false);
        }

        User user;
        String description;
        if (mAdaptive instanceof WallStreetAds) {
            mVisibleButtonFollow.set(true);
            mIsFollowing.set(true);
            WallStreetAds wall = ((WallStreetAds) mAdaptive);
            mAdsTitle.set(wall.getTitle());
            mSimpleImageUrl.set(wall.getCreativeIconUrl());
            if (wall.getNativeAd() != null) {
                wall.getNativeAd().recordImpression();
            }
        } else if (mAdaptive instanceof WallStreetAdaptive) {
            description = ((WallStreetAdaptive) mAdaptive).getDescription();
            mTitle.set(description);
            user = ((WallStreetAdaptive) mAdaptive).getUser();
            if (user != null) {
                //button follow visible for Product only
                if (mAdaptive instanceof Product) {
                    //button follow visible only if user not yet follow,
                    //or is not belong to user himself
                    boolean isFollow = true;
                    if (!SharedPrefUtils.checkIsMe(mContext, user.getId())) {
                        isFollow = user.isFollow();
                    }

                    mVisibleButtonFollow.set(!isFollow);
                    mIsFollowing.set(isFollow);
                } else {
                    mVisibleButtonFollow.set(false);
                }
                mUser.set(user);
            }
        }
    }

    void setAdaptive(Adaptive adaptive) {
        mAdaptive = adaptive;
        init();
    }

    void enableLiveHeader() {
        mIsLiveHeader = true;
    }

    void setSharedTextTitle(String title) {
        mShareTextTitle.set(title);
        mIsSharedHeader.set(true);
    }

    public String getFollowText(boolean isFollow) {
        if (mAdaptive instanceof WallStreetAds) {
            return mContext.getString(R.string.home_sponsored);
        } else if (isFollow) {
            return mContext.getString(R.string.following);
        } else {
            return mContext.getString(R.string.seller_store_follow_title);
        }
    }

    @Bindable
    public SpannableString getSubtitle() {
        if (mAdaptive instanceof WallStreetAds) {
            String sub = ((WallStreetAds) mAdaptive).getSubTitle();
            if (TextUtils.isEmpty(sub)) {
                return null;
            }
            return new SpannableString(sub);
        } else if (mIsLiveHeader && mAdaptive instanceof LifeStream) {
            return new SpannableString(((LifeStream) mAdaptive).getUser().getStoreName());
        } else if (mAdaptive instanceof WallStreetAdaptive) {
            String address = ((WallStreetAdaptive) mAdaptive).getDisplayAddress();
            PublishItem privacy = ((WallStreetAdaptive) mAdaptive).getPublish();
            if (mAdaptive instanceof Product) {
                return SpannableUtil.getProduct(mContext, ((Product) mAdaptive).getName(), address, privacy);
            } else {
                Date createDate = new Date();
                if (((WallStreetAdaptive) mAdaptive).getCreateDate() != null) {
                    createDate = ((WallStreetAdaptive) mAdaptive).getCreateDate();
                }
                String convertDate = DateTimeUtils.parse(getContext(), createDate.getTime());
                return SpannableUtil.getTimelineDate(mContext, convertDate, address, privacy);
            }
        } else {
            return null;
        }

    }

    public void onFollowButtonClick(View view, Context context) {
        if (mAdaptive instanceof WallStreetAds) {
            return;
        }
        if (mOnItemClickListener != null) {
            ((AbsBaseActivity) context).startLoginWizardActivity(new LoginActivityResultCallback() {
                @Override
                public void onActivityResultSuccess(boolean isAlreadyLogin) {
                    initFollow(isAlreadyLogin, context, view);
                }
            });
        }
    }

    private void initFollow(boolean isAlreadyLogin, Context context, View view) {
        User user = mUser.get();
        if (user == null) {
            return;
        }
        if (!isAlreadyLogin) {
            if (!SharedPrefUtils.getUserId(context).equals(user.getId())) {
                mIsFollowing.set(!mIsFollowing.get());
                mVisibleButtonFollow.set(!mIsFollowing.get());
                user.setFollow(mIsFollowing.get());
                mOnItemClickListener.onFollowButtonClick(user.getId(), mIsFollowing.get());
            } else {
                mVisibleButtonFollow.set(false);
            }
        } else {
            mIsFollowing.set(!mIsFollowing.get());
            mVisibleButtonFollow.set(!mIsFollowing.get());
            user.setFollow(mIsFollowing.get());
            mOnItemClickListener.onFollowButtonClick(user.getId(), mIsFollowing.get());
        }
    }

    public void onMoreButtonClick() {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onShowPostContextMenuClick(mAdaptive, mUser.get(), mPosition);
        }
    }

    public void setDisableClickOnUser(boolean disableClickOnUser) {
        mIsDisableClickOnUser = disableClickOnUser;
    }

    public void onUserProfileClick() {
        if (!mIsDisableClickOnUser) {
            new NavigateSellerStoreHelper(mContext, mUser.get()).openSellerStore();
        }
    }

    public void onHeaderItemClick(WallStreetDescriptionTextView textView) {
        if (mOnItemClickListener != null) {
            if (!textView.isSeeMore()) {
                mOnItemClickListener.onCommentClick(mAdaptive);
            } else {
                mOnItemClickListener.getRecyclerView().pause();
                mOnItemClickListener.getRecyclerView().setAutoResume(false);
                mAdaptive.startActivityForResult(new WallStreetIntentData(mContext, mOnItemClickListener));
            }
        }
    }

    public void onFooterCommentItemClick() {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onCommentClick(mAdaptive);
        }
    }
}
