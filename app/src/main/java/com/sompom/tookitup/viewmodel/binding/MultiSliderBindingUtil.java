package com.sompom.tookitup.viewmodel.binding;

import android.databinding.BindingAdapter;

import com.sompom.tookitup.widget.MyMultiSlider;

/**
 * Created by nuonveyo on 7/6/18.
 */

public final class MultiSliderBindingUtil {
    private MultiSliderBindingUtil() {
    }

    @BindingAdapter({"minMultiSlider", "maxMultiSlider"})
    public static void setValue(MyMultiSlider slider, int minMultiSlider, int maxMultiSlider) {
        //need to set value thumb(1) before thumb (0) to avoid incorrect set scroll position
//        slider.getThumb(1).setValue(maxMultiSlider);
//        slider.getThumb(0).setValue(minMultiSlider);
        if (minMultiSlider == 0 && maxMultiSlider == 0) {
            return;
        }
        slider.setValue(minMultiSlider, maxMultiSlider);
    }
}
