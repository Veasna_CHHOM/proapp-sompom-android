package com.sompom.tookitup.viewmodel.newviewmodel;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.sompom.tookitup.R;
import com.sompom.tookitup.helper.CounterClusterRenderer;
import com.sompom.tookitup.listener.OnCompleteListener;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.NearByDataManager;
import com.sompom.tookitup.utils.BitmapConverter;
import com.sompom.tookitup.viewmodel.AbsBaseViewModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 7/3/18.
 */

public class NearbyMapFragmentViewModel extends AbsBaseViewModel {

    private Context mContext;
    private GoogleMap mGoogleMap;
    private ClusterManager<User> mClusterManager;
    private Marker mCurrentMarker;
    private OnNearByMapListener mOnUserPictureClickListener;

    public NearbyMapFragmentViewModel(Context context,
                                      GoogleMap googleMap,
                                      NearByDataManager manager,
                                      OnNearByMapListener listener) {
        mContext = context;
        mGoogleMap = googleMap;
        mOnUserPictureClickListener = listener;
        startLocationUpdates();
        enableCurrentLocation();

        if (context instanceof AbsBaseActivity) {
            ((AbsBaseActivity) context).requestLocation((location, isNeverAskAgain) -> {
                onLocationChanged(location.getLatitude(), location.getLongtitude());

                Observable<Response<List<User>>> call = manager.getUser(location);
                ResponseObserverHelper<Response<List<User>>> helper = new ResponseObserverHelper<>(mContext, call);
                addDisposable(helper.execute(result -> setClusterData(result.body())));
            });
        }
    }

    private void setClusterData(List<User> result) {
        if (mClusterManager == null) {
            mClusterManager = new ClusterManager<>(mContext, mGoogleMap);
            mClusterManager.setRenderer(new CounterClusterRenderer(mContext, mGoogleMap, mClusterManager));
            mClusterManager.setAnimation(true);

            mClusterManager.setOnClusterClickListener(cluster -> {
                if (mOnUserPictureClickListener != null) {
                    mOnUserPictureClickListener.onHideBottomSheet();
                }
                float value = (float) Math.floor(mGoogleMap.getCameraPosition().zoom + 1);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(cluster.getPosition(), value);
                mGoogleMap.animateCamera(cameraUpdate, 300, null);
                return true;
            });

            mGoogleMap.setOnCameraIdleListener(mClusterManager);
            mGoogleMap.setOnMarkerClickListener(mClusterManager);

            // Customize info window to cluster manager
            mClusterManager.setOnClusterItemClickListener(user -> {
                if (mOnUserPictureClickListener != null) {
                    mOnUserPictureClickListener.onComplete(user);
                }
                return false;
            });

            mGoogleMap.setInfoWindowAdapter(mClusterManager.getMarkerManager());
            mGoogleMap.setOnInfoWindowClickListener(mClusterManager);

            mGoogleMap.setOnMapClickListener(latLng -> {
                if (mOnUserPictureClickListener != null) {
                    mOnUserPictureClickListener.onHideBottomSheet();
                }
            });

        }

        mClusterManager.clearItems();
        mClusterManager.addItems(result);
        mClusterManager.cluster();

        if (result != null && !result.isEmpty()) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (User user : result) {
                builder.include(user.getPosition());
            }
            LatLngBounds bounds = builder.build();

            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, mContext.getResources().getDimensionPixelOffset(R.dimen.space_12dp));
            mGoogleMap.moveCamera(cu);
        }

    }

    private void addMarker(LatLng latLng) {
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
        mCurrentMarker = mGoogleMap.addMarker(new MarkerOptions().position(latLng)
                .zIndex(1)
                .icon(BitmapConverter.bitmapDescriptorFromVector(mContext, R.drawable.ic_pin_location_star)));
    }

    private void enableCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mGoogleMap.setMyLocationEnabled(true);
        }
    }

    /**
     * Trigger new location updates at interval
     */
    private void startLocationUpdates() {
        // Create the location request to start receiving updates
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(1000);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(locationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(mContext);
        settingsClient.checkLocationSettings(locationSettingsRequest);

        if (ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            // new Google API SDK v11 uses getFusedLocationProviderClient(this)
            LocationServices.getFusedLocationProviderClient(mContext).requestLocationUpdates(locationRequest, new LocationCallback() {
                        @Override
                        public void onLocationResult(LocationResult locationResult) {
                            onLocationChanged(locationResult.getLastLocation().getLatitude(),
                                    locationResult.getLastLocation().getLongitude());
                        }
                    },
                    Looper.myLooper());
        }

    }

    private void onLocationChanged(double lat, double lng) {
        LatLng latLng = new LatLng(lat, lng);
        if (mCurrentMarker != null) {
            mCurrentMarker.setPosition(latLng);
        } else {
            addMarker(latLng);
        }
    }

    public void onMoveToCurrentLocationClick() {
        if (mCurrentMarker == null) {
            return;
        }
        if (mOnUserPictureClickListener != null) {
            mOnUserPictureClickListener.onHideBottomSheet();
        }
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(mCurrentMarker.getPosition()));
    }

    public interface OnNearByMapListener extends OnCompleteListener<User> {
        void onHideBottomSheet();
    }
}
