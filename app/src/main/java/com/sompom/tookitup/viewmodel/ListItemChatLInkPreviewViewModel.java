package com.sompom.tookitup.viewmodel;

import android.app.Activity;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;

import com.leocardz.link.preview.library.LinkPreviewCallback;
import com.leocardz.link.preview.library.SourceContent;
import com.leocardz.link.preview.library.TextCrawler;
import com.sompom.tookitup.listener.OnChatItemListener;
import com.sompom.tookitup.model.SelectedChat;
import com.sompom.tookitup.model.emun.ChatLinkBg;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.LinkPreviewModel;
import com.sompom.tookitup.utils.ChatUtility;

import timber.log.Timber;

/**
 * Created by He Rotha on 9/14/17.
 */

public class ListItemChatLInkPreviewViewModel extends ChatViewModel {
    public final ObservableField<String> mLinkLogoUrl = new ObservableField<>();
    public final ObservableField<String> mLinkTitle = new ObservableField<>();
    public final ObservableField<String> mLinkDescription = new ObservableField<>();
    public final ObservableBoolean mIsShowLogo = new ObservableBoolean(true);
    private TextCrawler mTextCrawler;
    private String mCurrentLinkPreview;
    private ChatLinkBg mChatLinkBg;

    public ListItemChatLInkPreviewViewModel(Activity context,
                                            Conversation conversation,
                                            View rootView,
                                            int position,
                                            Chat chat,
                                            SelectedChat selectedChat,
                                            ChatUtility.GroupMessage drawable,
                                            OnChatItemListener onItemClickListener) {
        super(context, conversation, rootView, position, chat, selectedChat, drawable, onItemClickListener);
        mChatLinkBg = ChatLinkBg.getChatLinkBg(drawable.getChatBg());

        if (mChat.getLinkPreviewModel() != null
                && mChat.getLinkPreviewModel().isLinkDownloaded()) {
            bindData(mChat.getLinkPreviewModel());
        } else {
            mCurrentLinkPreview = getUrlFirstIndex(chat.getContent());
            if (!TextUtils.isEmpty(mCurrentLinkPreview)) {
                LinkPreviewModel linkPreviewModel = new LinkPreviewModel();
                linkPreviewModel.setLink(mCurrentLinkPreview);
                mChat.setLinkPreviewModel(linkPreviewModel);

                mTextCrawler = new TextCrawler();
                mTextCrawler.makePreview(new LinkPreviewCallback() {
                    @Override
                    public void onPre() {
                        Timber.e("onPre");
                    }

                    @Override
                    public void onPos(SourceContent data, boolean b) {
                        String title = data.getTitle();
                        if (TextUtils.isEmpty(title)) {
                            title = data.getCannonicalUrl();
                        }
                        linkPreviewModel.setTitle(title);
                        linkPreviewModel.setDescription(data.getDescription());
                        linkPreviewModel.setLink(data.getUrl());
                        linkPreviewModel.setLinkDownloaded(true);

                        if (data.getImages() != null && !data.getImages().isEmpty()) {
                            for (String s : data.getImages()) {
                                if (!TextUtils.isEmpty(s)) {
                                    linkPreviewModel.setLogo(s);
                                    break;
                                }
                            }
                        }
                        bindData(linkPreviewModel);
                    }
                }, mCurrentLinkPreview);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTextCrawler != null) mTextCrawler.cancel();
    }

    public void onContainerLinkClick() {
        if (!TextUtils.isEmpty(mCurrentLinkPreview)) {
            if (!mCurrentLinkPreview.startsWith("http")) {
                mCurrentLinkPreview = "http://" + mCurrentLinkPreview;
            }
            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
            CustomTabsIntent customTabsIntent = builder.build();
            customTabsIntent.launchUrl(getActivity(), Uri.parse(mCurrentLinkPreview));
        }
    }

    public Drawable getLinkTitleBackground() {
        return ContextCompat.getDrawable(getActivity(), mChatLinkBg.getDrawableTitle());
    }

    public Drawable getLinkLayoutBackground() {
        return ContextCompat.getDrawable(getActivity(), mChatLinkBg.getDrawableBody());
    }

    private void bindData(LinkPreviewModel linkPreviewModel) {
        mIsShowLogo.set(!TextUtils.isEmpty(linkPreviewModel.getLogo()));
        mCurrentLinkPreview = linkPreviewModel.getLink();
        mLinkTitle.set(linkPreviewModel.getTitle());
        mLinkDescription.set(linkPreviewModel.getDescription());
        mLinkLogoUrl.set(linkPreviewModel.getLogo());
    }

    private String getUrlFirstIndex(String content) {
        String replaceText = content.replace("\n", " ");
        String[] splitText = replaceText.split(" ");
        for (String url : splitText) {
            if (Patterns.WEB_URL.matcher(url).matches()) {
                if (!url.startsWith("http")) {
                    url = "http://" + url;
                }
                return url;
            }
        }
        return null;
    }
}
