package com.sompom.tookitup.viewmodel;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.sompom.tookitup.R;
import com.sompom.tookitup.helper.CheckRequestLocationCallbackHelper;
import com.sompom.tookitup.intent.SelectAddressIntentResult;
import com.sompom.tookitup.model.Locations;
import com.sompom.tookitup.model.SearchAddressResult;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.SelectAddressActivity;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by nuonveyo on 5/10/18.
 */

public class SelectAddressFragmentViewModel extends AbsBaseViewModel {
    private static final int MAX_RESULT = 1;
    public final ObservableField<String> mAddress = new ObservableField<>();
    public final ObservableBoolean mIsEnableButton = new ObservableBoolean();
    private final GoogleMap mGoogleMap;
    private final Context mContext;
    private final OnCallback mOnCallback;
    private SearchAddressResult mSearchAddressResult;
    private Locations mLocations;

    public SelectAddressFragmentViewModel(Context context,
                                          GoogleMap googleMap,
                                          SearchAddressResult addressResult,
                                          OnCallback onCallback) {
        mContext = context;
        mGoogleMap = googleMap;
        mSearchAddressResult = addressResult;
        mOnCallback = onCallback;
        if (mSearchAddressResult != null) {
            mLocations = mSearchAddressResult.getLocations();
        }

        mGoogleMap.setOnCameraIdleListener(() -> {
            Timber.e("mGoogleMap.getCameraPosition().target: %s", mGoogleMap.getCameraPosition().target);
            mIsEnableButton.set(false);
            LatLng center = mGoogleMap.getCameraPosition().target;
            mAddress.set(mContext.getString(R.string.search_address_looking_for_address_title));
            Observable<SearchAddressResult> observable = Observable.just(center).concatMap(latLng -> {
                Geocoder geocoder = new Geocoder(mContext);
                List<Address> addressList = geocoder.getFromLocation(latLng.latitude, latLng.longitude, MAX_RESULT);
                if (addressList != null && !addressList.isEmpty()) {
                    SearchAddressResult searchAddressResult = new SearchAddressResult();
                    searchAddressResult.setFullAddress(addressList.get(0).getAddressLine(0));
                    searchAddressResult.setCity(addressList.get(0).getLocality());
                    searchAddressResult.setCountry(addressList.get(0).getCountryName());
                    Locations locations = new Locations();
                    locations.setLatitude(center.latitude);
                    locations.setLongtitude(center.longitude);
                    locations.setCountry(addressList.get(0).getCountryCode());
                    searchAddressResult.setLocations(locations);
                    return Observable.just(searchAddressResult);
                }
                return Observable.error(new Throwable("Cannot find the address!"));
            });
            observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(data -> {
                        mSearchAddressResult = data;
                        mAddress.set(mSearchAddressResult.getFullAddress());

                        mIsEnableButton.set(true);
                    }, throwable -> mAddress.set(throwable.getMessage()));
            if (mOnCallback != null) {
                mOnCallback.slideUpButton();
            }
        });

        enableCurrentLocation();
        initGoogleMap();
    }

    public void onPickUpAddressClicked() {
        if (mContext instanceof AbsBaseActivity) {
            SelectAddressIntentResult intent = new SelectAddressIntentResult(mSearchAddressResult);
            ((AbsBaseActivity) mContext).setResult(Activity.RESULT_OK, intent);
            ((AbsBaseActivity) mContext).finish();
        }
    }

    public void onBackPress() {
        if (mContext instanceof SelectAddressActivity) {
            ((SelectAddressActivity) mContext).onBackPressed();
        }
    }

    private void initGoogleMap() {
        if (mGoogleMap != null) {
            if (mLocations == null) {
                if (mContext instanceof AbsBaseActivity) {
                    ((AbsBaseActivity) mContext).requestLocation(new CheckRequestLocationCallbackHelper((Activity) mContext,
                            location -> {
                                mLocations = location;
                                zoomGoogleMapCamera();
                            }));
                }
            } else {
                zoomGoogleMapCamera();
            }
        }
    }

    private void enableCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mGoogleMap.setMyLocationEnabled(true);
        }
    }

    private void zoomGoogleMapCamera() {
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLocations.getLatitude(), mLocations.getLongtitude()),
                18));
    }

    public void onMoveToCurrentLocation() {
        if (mContext instanceof AbsBaseActivity) {
            ((AbsBaseActivity) mContext).requestLocation(new CheckRequestLocationCallbackHelper((Activity) mContext,
                    location -> {
                        mLocations = location;
                        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLocations.getLatitude(),
                                mLocations.getLongtitude()), 18));
                    }));
        }
    }

    public interface OnCallback {
        void slideUpButton();
    }
}
