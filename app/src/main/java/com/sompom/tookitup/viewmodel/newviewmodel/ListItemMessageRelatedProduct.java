package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.ObservableBoolean;

import com.sompom.tookitup.model.emun.SegmentedControlItem;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.viewmodel.AbsBaseViewModel;

/**
 * Created by nuonveyo on 6/26/18.
 */

public class ListItemMessageRelatedProduct extends AbsBaseViewModel {
    public final ObservableBoolean mIsShowProduct = new ObservableBoolean();
    public final Product mProduct;

    ListItemMessageRelatedProduct(Conversation conversation, SegmentedControlItem item) {
        Product product = null;
        if (item != SegmentedControlItem.Message) {
            product = conversation.getProduct();
            if (item == SegmentedControlItem.Buying || item == SegmentedControlItem.Selling) {
                if (product == null && conversation.getLastMessage() != null) {
                    product = conversation.getLastMessage().getProduct();
                }
            }
        }
        mIsShowProduct.set(product != null);
        mProduct = product;
    }

    public String getName() {
        if (mIsShowProduct.get()) {
            return mProduct.getName();
        }
        return null;
    }
}
