package com.sompom.tookitup.viewmodel.binding;

import android.databinding.BindingAdapter;

import com.sompom.tookitup.listener.OnSegmentedClickListener;
import com.sompom.tookitup.model.emun.SegmentedControlItem;
import com.sompom.tookitup.widget.segmentedcontrol.SegmentedButtonGroup;

/**
 * Created by nuonveyo on 6/26/18.
 */

public final class SegmentedButtonGroupBindingUtil {
    private SegmentedButtonGroupBindingUtil() {
    }

    @BindingAdapter({"addItem", "onItemClick"})
    public static void addItem(SegmentedButtonGroup segmentedButtonGroup, SegmentedControlItem[] item, OnSegmentedClickListener listener) {
        if (item == null) {
            return;
        }
        segmentedButtonGroup.removeAllViews();
        for (int i = 0; i < item.length; i++) {
            SegmentedControlItem segmentedControlItem = item[i];
            segmentedButtonGroup.addChildView(segmentedControlItem.getId(), segmentedControlItem.getTitle(), i == item.length - 1);
        }
        segmentedButtonGroup.setOnSegmentedItemClickListener(listener);
    }
}
