package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.ObservableField;

import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.NetworkBroadcastReceiver;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.listener.OnEditTextCommentListener;
import com.sompom.tookitup.model.result.Comment;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.PopUpCommentDataManager;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 6/8/18.
 */

public class PopUpCommentLayoutViewModel extends LayoutEditTextCommentViewModel {
    public final ObservableField<String> mEmptyMessage = new ObservableField<>();
    private final OnCallback mOnCallback;
    private NetworkBroadcastReceiver.NetworkListener mNetworkStateListener;
    private boolean mIsLoadedData;
    private String mLastSubCommentId = "0";

    public PopUpCommentLayoutViewModel(PopUpCommentDataManager dataManager,
                                       String itemId,
                                       OnCallback onCallback) {
        super(dataManager, itemId, onCallback);
        mOnCallback = onCallback;

        mEmptyMessage.set(getContext().getString(R.string.comment_no_comment_yet_message));
        if (getContext() instanceof AbsBaseActivity) {
            mNetworkStateListener = networkState -> {
                if (!mIsLoadedData) {
                    if (networkState == NetworkBroadcastReceiver.NetworkState.Connected) {
                        showLoading();
                        getData();
                    } else if (networkState == NetworkBroadcastReceiver.NetworkState.Disconnected) {
                        mIsShowEmpty.set(true);
                        mEmptyMessage.set(getContext().getString(R.string.error_internet_connection));
                    }
                }
            };
            ((AbsBaseActivity) getContext()).addNetworkStateChangeListener(mNetworkStateListener);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).removeStateChangeListener(mNetworkStateListener);
        }
    }

    @Override
    public void onRefresh() {
        super.onRefresh();
        mLastSubCommentId = "0";
        getData();
    }

    private void getData() {
        Observable<Response<List<Comment>>> callback = mDataManager.getCommentList(mContentId, mLastSubCommentId);
        ResponseObserverHelper<Response<List<Comment>>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<Comment>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mIsRefresh.set(false);
                hideLoading();
                if (mIsLoadedData) {
                    return;
                }
                mIsLoadedData = false;
                mIsShowEmpty.set(true);
                if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                    mEmptyMessage.set(getContext().getString(R.string.error_internet_connection));
                } else {
                    mEmptyMessage.set(getContext().getString(R.string.comment_no_comment_yet_message));
                }
                mOnCallback.onLoadCommentFail();
            }

            @Override
            public void onComplete(Response<List<Comment>> value) {
                List<Comment> commentList = value.body();
                if (commentList == null || commentList.isEmpty()) {
                    onFail(new ErrorThrowable(404, null));
                    return;
                }
                hideLoading();
                mIsRefresh.set(false);
                mIsLoadedData = true;
                mIsShowEmpty.set(false);
                mLastSubCommentId = getLastSubCommentId(commentList);
                mOnCallback.onLoadCommentSuccess(commentList, mDataManager.isCanLoadMore());
            }
        }));
    }

    public void loadMore(OnCallbackListListener<Response<List<Comment>>> listListener) {
        Observable<Response<List<Comment>>> callback = mDataManager.getCommentList(mContentId, mLastSubCommentId);
        ResponseObserverHelper<Response<List<Comment>>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<Comment>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                listListener.onFail(ex);
            }

            @Override
            public void onComplete(Response<List<Comment>> value) {
                mLastSubCommentId = getLastSubCommentId(value.body());
                listListener.onComplete(value, mDataManager.isCanLoadMore());
            }
        }));
    }

    public void onDeleteComment(Comment comment, OnClickListener onClickListener) {
        showLoading();
        Observable<Response<Object>> callback = mDataManager.deleteComment(comment.getId());
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mErrorMessage.set(null);
                if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                    mErrorMessage.set(ex.toString());
                } else {
                    mErrorMessage.set(getContext().getString(R.string.error_cannot_delete_comment));
                }
                hideLoading();
            }

            @Override
            public void onComplete(Response<Object> result) {
                hideLoading();
                onClickListener.onClick();
                mNumberOfCommentAdd--;
            }
        }));
    }

    public void checkToLikeComment(Comment comment) {
        Observable<Response<Object>> callback = mDataManager.postLikeComment(comment.getId(), comment.isLike());
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute());
    }

    public interface OnCallback extends OnEditTextCommentListener {
        void onLoadCommentSuccess(List<Comment> listComment, boolean isCanLoadMore);

        void onLoadCommentFail();
    }
}
