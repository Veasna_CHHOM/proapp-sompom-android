package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.ObservableField;

import com.sompom.tookitup.listener.OnTimelineItemButtonClickListener;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.services.datamanager.WallStreetDataManager;

/**
 * Created by nuonveyo on 7/12/18.
 */

public class ListItemTimelineMediaDetailViewModel {
    public final ObservableField<ListItemTimelineViewModel> mFooterViewModel = new ObservableField<>();

    public ListItemTimelineMediaDetailViewModel(AbsBaseActivity activity,
                                                Media media,
                                                WallStreetDataManager dataManager,
                                                int position,
                                                OnTimelineItemButtonClickListener onItemClick) {
        ListItemTimelineViewModel viewModel = new ListItemTimelineViewModel(activity,
                media,
                dataManager,
                position,
                onItemClick);
        mFooterViewModel.set(viewModel);
    }
}
