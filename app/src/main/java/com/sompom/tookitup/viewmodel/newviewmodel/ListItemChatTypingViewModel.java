package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;

import com.sompom.tookitup.R;
import com.sompom.tookitup.model.result.User;

/**
 * Created by He Rotha on 7/16/18.
 */
public class ListItemChatTypingViewModel {
    private User mUser;

    public ListItemChatTypingViewModel(User user) {
        mUser = user;
    }

    public String getText(Context context) {
        return mUser.getFullName() + " " + context.getString(R.string.chat_typing_title);
    }
}
