package com.sompom.tookitup.viewmodel;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.sompom.tookitup.helper.NavigateSellerStoreHelper;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.utils.StoreStatusUtil;

/**
 * Created by He Rotha on 9/13/17.
 */

public class StoreListingViewModel extends AbsBaseViewModel {
    public User mUser;
    private Context mContext;

    public StoreListingViewModel(Context context, User user) {
        mContext = context;
        mUser = user;
    }

    public int getCountryNameVisibility() {
        if (TextUtils.isEmpty(mUser.getCountry())) {
            return View.GONE;
        } else {
            return View.VISIBLE;
        }
    }

    public String getCountryName() {
        return mUser.getCountryName();
    }

    public String getLikeCount() {
        return String.valueOf(mUser.getLike());
    }

    public String getShareCount() {
        return String.valueOf(mUser.getShare());
    }

    public String getCommentCount() {
        return String.valueOf(mUser.getComment());
    }

    public String getSellBuyCount() {
        return String.valueOf(mUser.getSellCount() + mUser.getBuyCount());
    }

    public void onUserClick(Context context) {
    }

    public String getStatus(Context context) {
        return StoreStatusUtil.getUserStatus(context, mUser);
    }

    public void onUserProfileClick() {
        new NavigateSellerStoreHelper(mContext, mUser).openSellerStore();
    }
}
