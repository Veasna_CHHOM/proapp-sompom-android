package com.sompom.tookitup.viewmodel;

import android.app.Activity;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.media.AudioManager;
import android.os.Handler;
import android.view.View;

import com.desmond.squarecamera.utils.FormatTime;
import com.sinch.android.rtc.calling.Call;
import com.sompom.tookitup.chat.call.CallingService;
import com.sompom.tookitup.chat.call.NotificationCallVo;
import com.sompom.tookitup.helper.AudioPlayer;
import com.sompom.tookitup.intent.ChatIntent;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;

import java.util.Map;

import javax.annotation.Nullable;

public class CallingViewModel extends AbsBaseViewModel implements CallingService.CallingListener {
    public final ObservableInt mIsAnswerVisible = new ObservableInt();
    public final ObservableInt mVisibleDuration = new ObservableInt(View.INVISIBLE);
    public final ObservableField<User> mUser = new ObservableField<>();
    public final ObservableField<String> mDuration = new ObservableField();
    public final ObservableField<String> mImageUserUrl = new ObservableField<>();
    public final ObservableField<String> mUserName = new ObservableField<>();
    public final ObservableField<String> mProductUrl = new ObservableField<>();
    public final ObservableField<String> mProductName = new ObservableField<>();
    public final ObservableBoolean mIsLoudSpeaker = new ObservableBoolean();
    public final ObservableField<Product> mProduct = new ObservableField<>();

    private final AudioPlayer mAudioPlayer;
    private int mCurrVideoRecTimer = 0;
    private CallingService.SinchBinder mBinder;
    private CallingService.CallingListener mListener;
    private Activity mActivity;
    private Handler mTimerHandler = new Handler();
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            mDuration.set(FormatTime.getFormattedDuration(mCurrVideoRecTimer++));
            mTimerHandler.postDelayed(this, 1000);
        }
    };
    private boolean mIsOutGoingCalling;

    public CallingViewModel(Activity activity,
                            NotificationCallVo callVo,
                            User user,
                            @Nullable Product product,
                            CallingService.SinchBinder binder,
                            CallingService.CallingListener listener) {
        mAudioPlayer = new AudioPlayer(activity);
        mActivity = activity;
        this.mBinder = binder;
        mListener = listener;

        binder.addCallListener(this);
        if (callVo == null) {
            mIsOutGoingCalling = true;
            mIsAnswerVisible.set(View.GONE);
            binder.call(user.getId(), product);
            mUser.set(user);
            mProduct.set(product);
        } else {
            mIsOutGoingCalling = false;
            mUser.set(user);
            Map<String, String> map = callVo.getData();
            mUser.set(binder.getCurrentCallingUser(map));
            mProduct.set(binder.getCurrentProduct(map));
            mAudioPlayer.playRingtone();
        }

        User currentCallingUser = mUser.get();
        if (currentCallingUser != null) {
            Product prd = mProduct.get();
            if (prd == null) {
                mUserName.set(currentCallingUser.getFullName());
            } else {
                mUserName.set(currentCallingUser.getStoreName());
                mProductName.set(prd.getName());
                if (prd.getMedia() != null && !prd.getMedia().isEmpty()) {
                    mProductUrl.set(prd.getMedia().get(0).getUrl());
                }
            }
            mImageUserUrl.set(currentCallingUser.getUserProfile());
        } else {
            onCallEnd();
        }
    }

    @Override
    public void onCallEnd() {
        mListener.onCallEnd();
        if (mIsOutGoingCalling) {
            mAudioPlayer.stopProgressTone();
            mActivity.setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
        } else {
            mAudioPlayer.stopRingtone();
        }
    }

    @Override
    public void onIncomingCall(Call call) {
        mListener.onIncomingCall(call);
    }

    @Override
    public void onCallEstablished(Call call) {
        mActivity.runOnUiThread(mRunnable);
        mVisibleDuration.set(View.VISIBLE);
        if (mIsOutGoingCalling) {
            mAudioPlayer.stopProgressTone();
            mActivity.setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
        }
    }

    @Override
    public void onCallProgressing(Call call) {
        if (mIsOutGoingCalling) {
            mAudioPlayer.playProgressTone();
        }
    }

    public void onAnswerClick() {
        if (mBinder == null) {
            return;
        }
        mBinder.answer();
        mIsAnswerVisible.set(View.GONE);
        mAudioPlayer.stopRingtone();

    }

    public void onHangupClick() {
        mAudioPlayer.stopRingtone();
        if (mBinder == null) {
            onCallEnd();
            return;
        }
        boolean isError = mBinder.hangup();
        if (isError) {
            onCallEnd();
        }
    }

    @Override
    public void onDestroy() {
        if (mBinder != null) {
            mBinder.removeCallListener(this);
            mBinder.hangup();
        }
        mTimerHandler.removeCallbacks(mRunnable);
        super.onDestroy();
    }

    public void onMessageClick() {
        User user = mUser.get();
        if (user != null) {
            Product prd = mProduct.get();
            if (prd == null) {
                mActivity.startActivity(new ChatIntent(mActivity, user));
            } else {
                mActivity.startActivity(new ChatIntent(mActivity, prd, user));
            }
            onHangupClick();
        }
    }

    public void onSpeakerClick() {
        if (mBinder != null) {
            mIsLoudSpeaker.set(!mIsLoudSpeaker.get());
            mBinder.setVolume(mIsLoudSpeaker.get());
        }
    }

}
