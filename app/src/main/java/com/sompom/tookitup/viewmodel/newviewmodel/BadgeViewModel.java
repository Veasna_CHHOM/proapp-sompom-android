package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.ObservableField;

import com.sompom.tookitup.viewmodel.AbsBaseViewModel;

public class BadgeViewModel extends AbsBaseViewModel {

    private static final int LIMIT_DISPLAY_VALUE = 999;

    public final ObservableField<String> mBadgeValue = new ObservableField<>();
    private long mSaveBadge;

    public void setBadgeValue(long badgeValue) {
        mSaveBadge = badgeValue;
        if (mSaveBadge > 0) {
            if (mSaveBadge > LIMIT_DISPLAY_VALUE) {
                mBadgeValue.set(String.valueOf(LIMIT_DISPLAY_VALUE + "+"));
            } else {
                mBadgeValue.set(String.valueOf(mSaveBadge));
            }
        } else {
            mBadgeValue.set(null);
        }
    }

    public long getSaveBadge() {
        return mSaveBadge;
    }

    public void updateBadgeValue() {
        mSaveBadge -= 1;
        setBadgeValue(mSaveBadge);
    }
}
