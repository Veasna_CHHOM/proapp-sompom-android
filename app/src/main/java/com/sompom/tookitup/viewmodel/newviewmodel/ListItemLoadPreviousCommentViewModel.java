package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.ObservableBoolean;

import com.sompom.tookitup.listener.OnLoadPreviousCommentClickListener;
import com.sompom.tookitup.model.LoadPreviousComment;
import com.sompom.tookitup.viewmodel.AbsBaseViewModel;

/**
 * Created by nuonveyo on 12/17/18.
 */
public class ListItemLoadPreviousCommentViewModel extends AbsBaseViewModel {
    public final ObservableBoolean mIsAddMarginLeft = new ObservableBoolean();
    public final ObservableBoolean mIsItemClick = new ObservableBoolean();
    private final OnLoadPreviousCommentClickListener mOnClickListener;
    private final LoadPreviousComment mLoadPreviousComment;
    private final int mPosition;

    public ListItemLoadPreviousCommentViewModel(boolean isAddMarginLeft,
                                                int position,
                                                OnLoadPreviousCommentClickListener onClickListener,
                                                LoadPreviousComment loadPreviousComment) {
        mIsAddMarginLeft.set(isAddMarginLeft);
        mPosition = position;
        mOnClickListener = onClickListener;
        mLoadPreviousComment = loadPreviousComment;
    }

    public void onItemClick() {
        mIsItemClick.set(true);
        if (mOnClickListener != null) {
            mOnClickListener.onClick(mLoadPreviousComment, mPosition);
        }
    }
}
