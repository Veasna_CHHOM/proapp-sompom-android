package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableLong;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;

import com.sompom.tookitup.helper.AnimationHelper;
import com.sompom.tookitup.helper.LoginActivityResultCallback;
import com.sompom.tookitup.helper.WallStreetIntentData;
import com.sompom.tookitup.intent.CallingIntent;
import com.sompom.tookitup.intent.ChatIntent;
import com.sompom.tookitup.listener.OnTimelineItemButtonClickListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.WallStreetAdaptive;
import com.sompom.tookitup.model.emun.ContentType;
import com.sompom.tookitup.model.emun.PublishItem;
import com.sompom.tookitup.model.result.ContentStat;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.WallStreetDataManager;
import com.sompom.tookitup.utils.DateTimeUtils;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.utils.SpannableUtil;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by He Rotha on 7/10/18.
 */
public class ListItemTimelineViewModel extends ListItemTimelineHeaderViewModel {
    public final ObservableLong mLikeCount = new ObservableLong();
    public final ObservableLong mShareCount = new ObservableLong();
    public final ObservableLong mCommentCount = new ObservableLong();
    public final ObservableBoolean mIsLike = new ObservableBoolean();
    public final ObservableBoolean mIsShowShare = new ObservableBoolean();
    public final ObservableBoolean mIsHideCommentSharedButton = new ObservableBoolean();

    private Adaptive mAdaptive;
    private WallStreetDataManager mWallStreetDataManager;
    private OnTimelineItemButtonClickListener mOnItemClickListener;
    private ContentStat mContentStat;

    public ListItemTimelineViewModel(AbsBaseActivity activity,
                                     Adaptive adaptive,
                                     WallStreetDataManager dataManager,
                                     int position,
                                     OnTimelineItemButtonClickListener onItemClick) {
        super(activity, adaptive, position, onItemClick);
        mOnItemClickListener = onItemClick;
        mWallStreetDataManager = dataManager;
        mAdaptive = adaptive;
        if (mAdaptive instanceof WallStreetAdaptive) {
            mContentStat = ((WallStreetAdaptive) mAdaptive).getContentStat();
            mIsShowShare.set(!TextUtils.isEmpty(((WallStreetAdaptive) mAdaptive).getShareUrl()));
        }
        setData();
    }

    public OnTimelineItemButtonClickListener getOnItemClickListener() {
        return mOnItemClickListener;
    }

    public SpannableString getTime(Context context) {
        if (mAdaptive instanceof WallStreetAdaptive) {
            Date createDate = ((WallStreetAdaptive) mAdaptive).getCreateDate();
            String address = ((WallStreetAdaptive) mAdaptive).getUser().getCity();
            PublishItem publish = ((WallStreetAdaptive) mAdaptive).getPublish();
            String date = DateTimeUtils.parse(context, createDate.getTime());
            return SpannableUtil.getTimelineDate(context, date, address, publish);
        } else {
            return null;
        }
    }

    public boolean isHideProperty(List<User> user, long comment, long like, long share) {
        return comment == 0 && like == 0 && share == 0 && (user == null || user.isEmpty());
    }

    public void onCommentClick() {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onCommentButtonClick(mAdaptive, mPosition);
        }
    }

    public void onShareClick() {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onShareButtonClick(mAdaptive, mPosition);
        }
    }

    public void onLikeClick(View view) {
        if (mAdaptive instanceof WallStreetAdaptive && mContentStat != null) {
            AnimationHelper.animateBounce(view);
            long likeCount = mContentStat.getTotalLikes();
            if (mIsLike.get()) {
                likeCount -= 1;
                mIsLike.set(false);
            } else {
                likeCount += 1;
                mIsLike.set(true);
            }
            mContentStat.setTotalLikes(likeCount);
            checkLikeItem();


            mContext.startLoginWizardActivity(new LoginActivityResultCallback() {
                @Override
                public void onActivityResultSuccess(boolean isAlreadyLogin) {
                    ContentType contentType;
                    if (mAdaptive instanceof Media) {
                        contentType = ContentType.MEDIA;
                    } else {
                        contentType = ContentType.POST;
                    }
                    Observable<Response<Object>> call = mWallStreetDataManager.likePost(mAdaptive.getId(), contentType, mIsLike.get());
                    ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(mContext, call);
                    addDisposable(helper.execute());
                }
            });
        }


    }

    private void checkLikeItem() {
        if (mAdaptive instanceof WallStreetAdaptive && mContentStat != null) {
            ((WallStreetAdaptive) mAdaptive).setLike(mIsLike.get());
            ((WallStreetAdaptive) mAdaptive).setContentStat(mContentStat);
            mLikeCount.set(mContentStat.getTotalLikes());
            mIsLike.set(((WallStreetAdaptive) mAdaptive).isLike());
        }
    }

    private void setData() {
        if (mAdaptive instanceof WallStreetAdaptive && mContentStat != null) {
            mLikeCount.set(mContentStat.getTotalLikes());
            mShareCount.set(mContentStat.getTotalShares());
            mCommentCount.set(mContentStat.getTotalComments());
            mIsLike.set(((WallStreetAdaptive) mAdaptive).isLike());
        }
    }

    public List<User> getUserViewItem() {
        if (mAdaptive instanceof WallStreetAdaptive) {
            return ((WallStreetAdaptive) mAdaptive).getUserView();
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public Context getContext() {
        return mContext;
    }

    public WallStreetDataManager getWallStreetDataManager() {
        return mWallStreetDataManager;
    }

    public void onUserViewItemClick() {
        if (getUserViewItem() != null
                && !getUserViewItem().isEmpty()
                && mOnItemClickListener != null)
            mOnItemClickListener.onUserViewMediaClick(mAdaptive);
    }

    public void onSeeMoreClick() {
        mOnItemClickListener.getRecyclerView().pause();
        mOnItemClickListener.getRecyclerView().setAutoResume(false);
        mAdaptive.startActivityForResult(new WallStreetIntentData(mContext, mOnItemClickListener));
    }

    public void onMessageClick() {
        if (!(mAdaptive instanceof Product) || ((Product) mAdaptive).getUser() == null) {
            return;
        }
        mContext.startLoginWizardActivity(new LoginActivityResultCallback() {
            @Override
            public void onActivityResultSuccess(boolean isAlreadyLogin) {
                if (!isAlreadyLogin && TextUtils.equals(SharedPrefUtils.getUserId(getContext()), mAdaptive.getId())) {
                    return;
                }
                mContext.startActivity(new ChatIntent(mContext, ((Product) mAdaptive).getUser()));
            }
        });
    }

    public void onCallClick() {
        if (!(mAdaptive instanceof Product) || ((Product) mAdaptive).getUser() == null) {
            return;
        }
        mContext.startLoginWizardActivity(new LoginActivityResultCallback() {
            @Override
            public void onActivityResultSuccess(boolean isAlreadyLogin) {
                if (!isAlreadyLogin && TextUtils.equals(SharedPrefUtils.getUserId(getContext()), mAdaptive.getId())) {
                    return;
                }
                mContext.startActivity(new CallingIntent(mContext, ((Product) mAdaptive).getUser(), (Product) mAdaptive));
            }
        });
    }

    public void onLikeClick() {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onLikeViewerClick(mAdaptive);
        }
    }
}
