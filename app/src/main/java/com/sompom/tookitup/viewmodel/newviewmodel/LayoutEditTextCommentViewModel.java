package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

import com.example.usermentionable.model.UserMentionable;
import com.example.usermentionable.tokenization.QueryToken;
import com.example.usermentionable.tokenization.interfaces.QueryTokenReceiver;
import com.example.usermentionable.utils.RenderTextAsMentionable;
import com.example.usermentionable.widget.KeyboardInputEditor;
import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.SoundEffectService;
import com.sompom.tookitup.helper.LoginActivityResultCallback;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnEditTextCommentListener;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.emun.MediaType;
import com.sompom.tookitup.model.request.CommentBody;
import com.sompom.tookitup.model.result.Comment;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.BaseObserverHelper;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.PopUpCommentDataManager;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.utils.ToastUtil;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 11/7/18.
 */

public class LayoutEditTextCommentViewModel extends AbsLoadingViewModel {
    public final ObservableField<String> mHashTagToken = new ObservableField<>();
    public final ObservableField<String> mMessage = new ObservableField<>();
    public final ObservableBoolean mIsAddIconAnimation = new ObservableBoolean();
    public final ObservableBoolean mIsShowEmpty = new ObservableBoolean();
    public final ObservableBoolean mIsUpdate = new ObservableBoolean();
    public final ObservableField<List<UserMentionable>> mUserMentionableList = new ObservableField<>();
    public final ObservableBoolean mIsShowListPopupWindow = new ObservableBoolean();
    public final ObservableBoolean mIsRefresh = new ObservableBoolean();
    final String mContentId;
    final PopUpCommentDataManager mDataManager;
    final OnEditTextCommentListener mEditTextCommentListener;
    public int mNumberOfCommentAdd = 0;
    int mUpdateCommentPosition;

    private Comment mUpdateComment;
    private Editable mEditable;

    LayoutEditTextCommentViewModel(PopUpCommentDataManager dataManager,
                                   String id,
                                   OnEditTextCommentListener editTextCommentListener) {
        super(dataManager.getContext());
        mDataManager = dataManager;
        mContentId = id;
        mEditTextCommentListener = editTextCommentListener;
    }

    public void setUpdateComment(Comment updateComment, int position) {
        mUpdateComment = updateComment;
        mUpdateCommentPosition = position;
        mIsUpdate.set(mUpdateComment != null);
    }

    public void onSendButtonClick() {
        if (mEditable != null && !TextUtils.isEmpty(mEditable.toString())) {
            mMessage.set(mEditable.toString());
            if (getContext() instanceof AbsBaseActivity) {
                ((AbsBaseActivity) getContext()).startLoginWizardActivity(new LoginActivityResultCallback() {
                    @Override
                    public void onActivityResultSuccess(boolean isAlreadyLogin) {
                        String renderText = RenderTextAsMentionable.render(getContext(), mEditable);
                        if (mIsUpdate.get()) {
                            mUpdateComment.setContent(renderText);
                            updateComment(mUpdateComment);
                            if (mEditTextCommentListener != null) {
                                mEditTextCommentListener.onUpdateCommentSuccess(mUpdateComment, mUpdateCommentPosition);
                            }
                            mMessage.set("");
                            setUpdateComment(null, mUpdateCommentPosition);
                        } else {
                            Comment comment = new Comment();
                            comment.setContent(renderText);
                            comment.setUser(SharedPrefUtils.getUser(getContext()));
                            comment.setDate(new Date());
                            postComment(comment);
                            if (mEditTextCommentListener != null) {
                                mEditTextCommentListener.onSendText(comment);
                            }
                        }
                    }
                });
            }
        }
    }

    public void updateComment(Comment comment) {
        CommentBody commentBody = new CommentBody();
        commentBody.setMessage(comment.getContent());
        Observable<Response<Comment>> callback = mDataManager.updateComment(comment.getId(), commentBody);
        ResponseObserverHelper<Response<Comment>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute());
    }

    public void postComment(Comment comment) {
        comment.setPosting(true);
        Observable<Response<Comment>> callback = mDataManager.postComment(mContentId, comment);
        ResponseObserverHelper<Response<Comment>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<Comment>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mErrorMessage.set(null);
                if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                    mErrorMessage.set(ex.toString());
                } else {
                    mErrorMessage.set(getContext().getString(R.string.error_cannot_post_comment));
                }
                mEditTextCommentListener.onPostCommentFail();
            }

            @Override
            public void onComplete(Response<Comment> result) {
                SoundEffectService.playSound(getContext(), SoundEffectService.SoundFile.SEND_MESSAGE);
                if (mEditTextCommentListener != null) {
                    Comment comment1 = result.body();
                    if (comment1 != null) {
                        comment1.setUser(comment.getUser());
                    }
                    mEditTextCommentListener.onPostCommentSuccess(comment1);
                }
                mNumberOfCommentAdd++;
            }
        }));

        mMessage.set("");
        mEditable = null;
        mIsShowEmpty.set(false);
    }

    public void onCloseUpdateComment() {
        setUpdateComment(null, mUpdateCommentPosition);
        mMessage.set("");
        ToastUtil.showToast(getContext(), R.string.change_language_button_cancel);
    }

    public TextWatcher onTextChange() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //Stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                mEditable = s;
                if (!TextUtils.isEmpty(s)) {
                    if (!mIsAddIconAnimation.get()) {
                        mIsAddIconAnimation.set(true);
                    }
                } else {
                    if (mIsAddIconAnimation.get()) {
                        mIsAddIconAnimation.set(false);
                    }
                }
            }
        };
    }

    public KeyboardInputEditor.KeyBoardInputCallbackListener onKeyBoardInputCallbackListener() {
        return (inputContentInfo, flags, opts) -> {
            if (getContext() instanceof AbsBaseActivity) {
                ((AbsBaseActivity) getContext()).startLoginWizardActivity(new LoginActivityResultCallback() {
                    @Override
                    public void onActivityResultSuccess(boolean isAlreadyLogin) {
                        Uri uri = inputContentInfo.getLinkUri();
                        if (uri != null) {
                            Media media = new Media();
                            media.setUrl(String.valueOf(uri));
                            media.setType(MediaType.IMAGE);
                            Comment comment = new Comment();
                            comment.setMedia(media);
                            comment.setUser(SharedPrefUtils.getUser(getContext()));
                            postComment(comment);
                            mEditTextCommentListener.onSendText(comment);
                        }
                    }
                });
            }
        };
    }

    public QueryTokenReceiver queryTokenReceiverListener() {
        return new QueryTokenReceiver() {
            @Override
            public void onQueryReceived(@NonNull QueryToken queryToken) {
                if (queryToken.getExplicitChar() == '@') {
                    mIsShowListPopupWindow.set(true);
                    getMentionUserList(queryToken.getKeywords());
                } else if (queryToken.getExplicitChar() == '#') {
                    mHashTagToken.set(queryToken.getTokenString());
                } else {
                    mIsShowListPopupWindow.set(false);
                }
            }

            @Override
            public void invalidToken() {
                mIsShowListPopupWindow.set(false);
            }
        };
    }

    String getLastSubCommentId(List<Comment> commentList) {
        String commentId = "";
        if (commentList != null && !commentList.isEmpty()) {
            commentId = commentList.get(0).getId();
        }
        if (TextUtils.isEmpty(commentId)) {
            commentId = "0";
        }
        return commentId;
    }

    String getNewestSubCommentId(List<Comment> commentList) {
        if (commentList != null && !commentList.isEmpty()) {
            int lastIndex = commentList.size() - 1;
            return commentList.get(lastIndex).getId();
        }
        return "";
    }

    private void getMentionUserList(String keyboard) {
        Observable<List<UserMentionable>> observable = mDataManager.getUserMentionList(keyboard);
        BaseObserverHelper<List<UserMentionable>> helper = new BaseObserverHelper<>(getContext(), observable);
        helper.execute(new OnCallbackListener<List<UserMentionable>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mUserMentionableList.set(null);
            }

            @Override
            public void onComplete(List<UserMentionable> result) {
                if (result != null && !result.isEmpty()) {
                    mUserMentionableList.set(result);
                } else {
                    mUserMentionableList.set(null);
                }
            }
        });
    }

    public void onRefresh() {
        mIsRefresh.set(true);
    }
}
