package com.sompom.tookitup.viewmodel.newviewmodel;

import com.sompom.tookitup.listener.OnItemClickListener;
import com.sompom.tookitup.model.emun.ActionButtonItem;
import com.sompom.tookitup.viewmodel.AbsBaseViewModel;

import timber.log.Timber;

/**
 * Created by nuonveyo on 8/1/18.
 */

public class ProductDetailActionButtonDialogViewModel extends AbsBaseViewModel {
    public ActionButtonItem[] getItems() {
        return ActionButtonItem.values();
    }

    public OnItemClickListener<ActionButtonItem> getListener() {
        return actionButtonItem -> Timber.e("actionButtonItem: %s", actionButtonItem.getId());
    }
}
