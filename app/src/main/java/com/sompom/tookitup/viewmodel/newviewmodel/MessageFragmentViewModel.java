package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;

import com.sompom.tookitup.listener.OnSegmentedClickListener;
import com.sompom.tookitup.model.emun.SegmentedControlItem;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

/**
 * Created by nuonveyo on 6/25/18.
 */

public class MessageFragmentViewModel extends AbsLoadingViewModel {
    public OnSegmentedClickListener mOnSegmentedClickListener;

    public MessageFragmentViewModel(Context context, OnSegmentedClickListener onSegmentedClickListener) {
        super(context);
        mOnSegmentedClickListener = onSegmentedClickListener;
    }

    public SegmentedControlItem[] getItem() {
        return SegmentedControlItem.getVisibleItems();
    }

}
