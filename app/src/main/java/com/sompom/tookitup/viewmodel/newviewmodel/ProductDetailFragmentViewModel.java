package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.text.TextUtils;
import android.view.View;

import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.NetworkBroadcastReceiver;
import com.sompom.tookitup.helper.AnimationHelper;
import com.sompom.tookitup.helper.CheckRequestLocationCallbackHelper;
import com.sompom.tookitup.helper.LoginActivityResultCallback;
import com.sompom.tookitup.helper.NavigateSellerStoreHelper;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.model.emun.Status;
import com.sompom.tookitup.model.result.ContentStat;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.newui.ProductDetailActivity;
import com.sompom.tookitup.newui.dialog.PostContextMenuDialog;
import com.sompom.tookitup.newui.dialog.ShareLinkMenuDialog;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.ProductManager;
import com.sompom.tookitup.services.datamanager.StoreDataManager;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by He Rotha on 10/6/17.
 */

public class ProductDetailFragmentViewModel extends AbsLoadingViewModel {
    public final ObservableBoolean mIsBelongToOwner = new ObservableBoolean(false);
    public final ObservableBoolean mIsFollow = new ObservableBoolean(false);
    public final ObservableBoolean mIsLike = new ObservableBoolean(false);
    public final ObservableBoolean mIsShowSoldIcon = new ObservableBoolean();
    public final ObservableBoolean mIsShowDescription = new ObservableBoolean();
    public final ObservableField<String> mRelatedProduct = new ObservableField<>();
    public final ObservableField<String> mProductDescription = new ObservableField<>();
    public final ObservableField<String> mProductName = new ObservableField<>();
    private final String mMyUserId;
    private final ProductDetailActivity mActivity;
    private final ProductManager mProductManager;
    private final StoreDataManager mStoreDataManager;
    private final OnCallback mOnCallback;
    private final NetworkBroadcastReceiver.NetworkListener mNetworkStateListener;
    public Product mProduct;
    private boolean mIsDataReady;

    public ProductDetailFragmentViewModel(ProductDetailActivity activity,
                                          ProductManager productManager,
                                          StoreDataManager storeDataManager,
                                          Product product,
                                          OnCallback onCallback) {
        super(activity);
        mProductManager = productManager;
        mStoreDataManager = storeDataManager;
        mActivity = activity;
        mProduct = product;
        mMyUserId = SharedPrefUtils.getUserId(mActivity);
        mOnCallback = onCallback;


        mNetworkStateListener = networkState -> {
            if (!mIsDataReady) {
                if (networkState == NetworkBroadcastReceiver.NetworkState.Connected) {
                    showLoading();
                    getData();
                } else if (networkState == NetworkBroadcastReceiver.NetworkState.Disconnected) {
                    showNetworkError();
                }
            }
        };
        mActivity.addNetworkStateChangeListener(mNetworkStateListener);
    }

    public NetworkBroadcastReceiver.NetworkListener getNetworkStateListener() {
        return mNetworkStateListener;
    }

    public String getFollowText(Context context, boolean isFollow) {
        if (isFollow) {
            return context.getString(R.string.following);
        } else {
            return context.getString(R.string.seller_store_follow_title);
        }
    }

    public void onSellerStoreClick() {
        if (mProduct.getUser() != null && !TextUtils.isEmpty(mProduct.getUser().getId())) {
            new NavigateSellerStoreHelper(mActivity, mProduct.getUser()).openSellerStore();
        }
    }

    @Override
    public void showLoading() {
        super.showLoading();
        mActivity.setShown(false);
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        mActivity.setShown(true);
    }

    @Override
    public void showError(String errorName) {
        super.showError(errorName);
        mActivity.setShown(false);
    }

    public void onFollowClick(View view) {
        AnimationHelper.animateBounceFollowButton(view, 600, 0.15f, 10.0f);
        if (mActivity != null) {
            mActivity.startLoginWizardActivity(new LoginActivityResultCallback() {
                @Override
                public void onActivityResultSuccess(boolean isAlreadyLogin) {
                    mIsFollow.set(!mIsFollow.get());
                    mProduct.getUser().setFollow(mIsFollow.get());
                    String mUserId = mProduct.getUser().getId();
                    Observable<Response<Object>> call;
                    if (mIsFollow.get()) {
                        call = mStoreDataManager.postFollow(mUserId);
                    } else {
                        call = mStoreDataManager.unFollow(mUserId);
                    }
                    ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(mActivity, call);
                    addDisposable(helper.execute(data -> {
                        if (mOnCallback != null) {
                            mOnCallback.onUpdateProduct(mProduct);
                        }
                    }));
                }
            });
        }
    }

    private void getRelatedProduct() {
        Observable<Response<List<Product>>> related = mProductManager.getRelatedProduct(mProduct.getId());
        ResponseObserverHelper<Response<List<Product>>> helper =
                new ResponseObserverHelper<>(mActivity, related);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<Product>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mRelatedProduct.set(mActivity.getString(R.string.product_detail_no_related_title));
            }

            @Override
            public void onComplete(Response<List<Product>> result) {
                if (result.body() == null || result.body().isEmpty()) {
                    mRelatedProduct.set(mActivity.getString(R.string.product_detail_no_related_title));
                } else {
                    mRelatedProduct.set(mActivity.getString(R.string.product_detail_related_title));
                    if (mOnCallback != null) {
                        mOnCallback.onGetRelatedProductSuccess(result.body());
                    }
                }
            }
        }));
    }

    private void getData() {
        mActivity.requestLocation(new CheckRequestLocationCallbackHelper(mActivity,
                locations -> {
                    Observable<Response<Product>> request = mProductManager.getProductById(mProduct.getId(), locations);
                    ResponseObserverHelper<Response<Product>> helper = new ResponseObserverHelper<>(mActivity, request);
                    addDisposable(helper.execute(new OnCallbackListener<Response<Product>>() {
                        @Override
                        public void onFail(ErrorThrowable ex) {
                            Timber.e("onServiceFail: %s", ex.toString());
                            if (ex.getCode() == 404) {
                                showNoItemError(mActivity.getString(R.string.error_product_list_404));
                            } else if (ex.getCode() == 409) {
                                showError(mActivity.getString(R.string.error_item_is_delete_409));
                            } else {
                                showError(ex.toString());
                            }
                        }

                        @Override
                        public void onComplete(Response<Product> result) {
                            mIsDataReady = true;
                            mProduct = result.body();

                            initView();
                            if (SharedPrefUtils.isLogin(mActivity) && mMyUserId.equals(mProduct.getUser().getId())) {
                                mIsBelongToOwner.set(true);
                            }

                            getRelatedProduct();
                            if (mOnCallback != null) {
                                mOnCallback.onGetProductDetailSuccess(mProduct);
                            }

                        }
                    }));
                }));

    }

    @Override
    public void onRetryClick() {
        super.onRetryClick();
        getData();
    }

    private void initView() {
        mIsFollow.set(mProduct.getUser().isFollow());
        mProductDescription.set(mProduct.getDescription());
        mIsShowDescription.set(!TextUtils.isEmpty(mProduct.getDescription()));
        mIsShowSoldIcon.set(mProduct.getStatus() == Status.SOLD);
        mProductName.set(mProduct.getName());
        mIsLike.set(mProduct.isLike());

        if (SharedPrefUtils.isLogin(mActivity) && mMyUserId.equals(mProduct.getUser().getId())) {
            mIsBelongToOwner.set(true);
        }
    }

    public void onShareClick() {
        ShareLinkMenuDialog dialog = ShareLinkMenuDialog.newInstance(mProduct);
        dialog.show(mActivity.getSupportFragmentManager(), PostContextMenuDialog.TAG);
    }

    public void onCommentClick() {
        mActivity.showCommentLayout();

    }

    public void onLikeClick(View view) {
        AnimationHelper.animateBounce(view);
        mActivity.startLoginWizardActivity(new LoginActivityResultCallback() {
            @Override
            public void onActivityResultSuccess(boolean isAlreadyLogin) {
                ContentStat contentStat = mProduct.getContentStat();
                long likeCount = contentStat.getTotalLikes();
                if (mIsLike.get()) {
                    likeCount -= 1;
                    mIsLike.set(false);
                } else {
                    likeCount += 1;
                    mIsLike.set(true);
                }
                contentStat.setTotalLikes(likeCount);
                mProduct.setLike(mIsLike.get());
                mProduct.setContentStat(contentStat);
                mActivity.updateLikeCount(contentStat, mIsLike.get());

                checkToPostLike(mProduct);
                if (mOnCallback != null) {
                    mOnCallback.onUpdateProduct(mProduct);
                }
            }
        });
    }

    public void checkToPostLike(Product product) {
        Observable<Response<Object>> call = mProductManager.postLikeProduct(product.getId(), product.isLike());
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(mActivity, call);
        addDisposable(helper.execute());
    }

    public interface OnCallback {
        void onGetRelatedProductSuccess(List<Product> products);

        void onGetProductDetailSuccess(Product product);

        void onUpdateProduct(Product product);
    }
}
