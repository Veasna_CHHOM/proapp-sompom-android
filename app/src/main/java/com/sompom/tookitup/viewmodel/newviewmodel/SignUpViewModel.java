package com.sompom.tookitup.viewmodel.newviewmodel;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ClickableSpan;
import android.view.View;

import com.desmond.squarecamera.model.MediaFile;
import com.sompom.baseactivity.ResultCallback;
import com.sompom.tookitup.R;
import com.sompom.tookitup.chat.service.SocketService;
import com.sompom.tookitup.helper.CheckPermissionCallbackHelper;
import com.sompom.tookitup.helper.CheckRequestLocationCallbackHelper;
import com.sompom.tookitup.helper.DelayWatcher;
import com.sompom.tookitup.helper.FacebookLoginHelper;
import com.sompom.tookitup.intent.newintent.MyCameraIntent;
import com.sompom.tookitup.intent.newintent.MyCameraResultIntent;
import com.sompom.tookitup.intent.newintent.SuggestionIntent;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.model.Locations;
import com.sompom.tookitup.model.request.LoginBody;
import com.sompom.tookitup.model.request.PhoneLoginRequest;
import com.sompom.tookitup.model.request.SocialRequest;
import com.sompom.tookitup.model.result.AuthResponse;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.LoginDataManager;
import com.sompom.tookitup.utils.CurrencyUtils;
import com.sompom.tookitup.utils.HTTPResponse;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import retrofit2.Response;

/**
 * Created by He Rotha on 6/4/18.
 */
public class SignUpViewModel extends AbsLoadingViewModel {
    public final ObservableField<String> mFirstName = new ObservableField<>();
    public final ObservableField<String> mFirstNameError = new ObservableField<>();
    public final ObservableField<String> mLastName = new ObservableField<>();
    public final ObservableField<String> mLastNameError = new ObservableField<>();
    public final ObservableField<String> mEmail = new ObservableField<>();
    public final ObservableField<String> mEmailError = new ObservableField<>();
    public final ObservableField<String> mStoreName = new ObservableField<>();
    public final ObservableField<String> mStoreNamePlaceHolder = new ObservableField<>();
    public final ObservableField<String> mStoreNameError = new ObservableField<>();
    public final ObservableField<String> mImageUri = new ObservableField<>();
    public final ObservableBoolean mIsEnable = new ObservableBoolean();
    public final ObservableField<String> mPassword = new ObservableField<>();
    public final ObservableField<String> mPasswordError = new ObservableField<>();


    public final ObservableBoolean mShowButtonFacebook = new ObservableBoolean(false);
    private final LoginDataManager mLoginDataManager;
    private final Context mContext;
    private final PhoneLoginRequest mPhoneLoginRequest;
    private User mUser = new User();
    private FacebookLoginHelper mFacebookLoginHelper;
    private Locations mLocations;
    private SignUpViewModelListener mListener;

    public SignUpViewModel(Activity activity,
                           PhoneLoginRequest request,
                           LoginDataManager loginDataManager) {
        super(loginDataManager.getContext());
        // mUser.setPhone(request.getPhone()); //No need anymore
        mLoginDataManager = loginDataManager;
        mPhoneLoginRequest = request;
        mContext = mLoginDataManager.getContext();
        mStoreNamePlaceHolder.set(mContext.getString(R.string.sign_up_store_name_title));

        if (activity instanceof AbsBaseActivity) {
            ((AbsBaseActivity) activity).requestLocation(new CheckRequestLocationCallbackHelper((Activity) mContext,
                    location -> mLocations = location));
        }

        //No need for now.
//        showLoading();
    }

    public final ClickableSpan getForgotPasswordClickableSpan() {
        return new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                if (mListener != null) {
                    mListener.onForgotPasswordClicked();
                }
            }
        };
    }

    public void setListener(SignUpViewModelListener listener) {
        mListener = listener;
    }

    public void setStoreNamePlaceHolder() {
        if (TextUtils.isEmpty(mStoreName.get()) && TextUtils.isEmpty(mFirstName.get())) {
            mStoreNamePlaceHolder.set(mContext.getString(R.string.sign_up_store_name_title));
        } else {
            if (TextUtils.isEmpty(mStoreName.get()) && !TextUtils.isEmpty(mFirstName.get())) {
                mStoreNamePlaceHolder.set(mFirstName.get());
            }
        }
        checkToEnableContinueButton(mFirstName.get(),
                mLastName.get(),
                mEmail.get());
    }

    public TextWatcher onFirstNameTextChange() {
        return new DelayWatcher(false) {
            @Override
            public void onTextChanged(String text) {
                checkToEnableContinueButton(text,
                        mLastName.get(),
                        mEmail.get());
            }
        };
    }


    public TextWatcher onLastNameTextChange() {
        return new DelayWatcher(false) {
            @Override
            public void onTextChanged(String text) {
                checkToEnableContinueButton(mFirstName.get(),
                        text, mEmail.get());
            }
        };
    }


    public TextWatcher onEmailTextChange() {
        return new DelayWatcher(false) {
            @Override
            public void onTextChanged(String text) {
                checkToEnableContinueButton1(text, mPassword.get());
            }
        };
    }

    public TextWatcher onPasswordTextChange() {
        return new DelayWatcher(false) {
            @Override
            public void onTextChanged(String text) {
                checkToEnableContinueButton1(mEmail.get(), text);
            }
        };
    }

    public TextWatcher onStoreNameTextChange() {
        return new DelayWatcher(false) {
            @Override
            public void onTextChanged(String text) {
                checkToEnableContinueButton(mFirstName.get(),
                        mLastName.get(),
                        mEmail.get());

                if (!TextUtils.isEmpty(text)) {
                    mStoreNamePlaceHolder.set(text);
                } else {
                    if (!TextUtils.isEmpty(mFirstName.get())) {
                        mStoreNamePlaceHolder.set(mFirstName.get());
                    } else {
                        mStoreNamePlaceHolder.set(mContext.getString(R.string.sign_up_store_name_title));
                    }
                }
            }
        };
    }

    private void checkToEnableContinueButton(String firstName,
                                             String lastName,
                                             String email) {
        String imageProfile = mImageUri.get();
        if (!TextUtils.isEmpty(firstName)
                && !TextUtils.isEmpty(lastName)
                && !TextUtils.isEmpty(email)
                && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
                && !TextUtils.isEmpty(imageProfile)) {
            if (!mIsEnable.get())
                mIsEnable.set(true);
        } else {
            if (mIsEnable.get())
                mIsEnable.set(false);
        }
    }

    private void checkToEnableContinueButton1(String email,
                                              String passoword) {
        if (!TextUtils.isEmpty(email)
                && !TextUtils.isEmpty(passoword)
                && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            if (!mIsEnable.get())
                mIsEnable.set(true);
        } else {
            if (mIsEnable.get())
                mIsEnable.set(false);
        }
    }

    private LoginBody getLoginBody() {
        return new LoginBody(mEmail.get(), mPassword.get());
    }

    private void requestLogin() {
        showLoading();
        Observable<Response<AuthResponse>> loginService = mLoginDataManager.getLoginService(getLoginBody());
        ResponseObserverHelper<Response<AuthResponse>> helper = new ResponseObserverHelper<>(mLoginDataManager.mContext,
                loginService);
        addDisposable(helper.execute(new OnCallbackListener<Response<AuthResponse>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                String error = ex.getMessage();
                if (ex.getCode() == HTTPResponse.BAD_REQUEST) {
                    error = mContext.getString(R.string.invalid_identifier);
                } else if (ex.getCode() == HTTPResponse.NOT_FOUND) {
                    error = mContext.getString(R.string.account_not_found);
                }

                if (mListener != null) {
                    mListener.onLoginError(error);
                }
            }

            @Override
            public void onComplete(Response<AuthResponse> result) {
                onLoginSuccess(result.body());
            }
        }));

    }

    private void onLoginSuccess(AuthResponse authResponse) {
//        Timber.i("onLoginSuccess: " + new Gson().toJson(authResponse));
        //TODO: Need to remove after sever fix
        if (SharedPrefUtils.isEmailExist(mContext, mEmail.get())) {
            saveUserLoggedIn(authResponse);
        } else {
            hideLoading();
            if (mListener != null) {
                mListener.onChangePasswordRequire(authResponse);
            }
        }
    }

    public void saveUserLoggedIn(AuthResponse authResponse) {
        //Set accessToken to user
        if (authResponse.getUser() != null) {
            authResponse.getUser().setAccessToken(authResponse.getAccessToken());
            SharedPrefUtils.setUserValue(authResponse.getUser(), mContext);
        }

        hideLoading();
        if (mListener != null) {
            mListener.onLoginSuccess();
        }
    }

    private void getData() {
        Observable<Response<User>> callback = mLoginDataManager.loginWithPhoneNumber(mPhoneLoginRequest);
        callback = callback.concatMap(userResult -> {
            User user = userResult.body();
            if (user == null) {
                if (mLocations != null && !mLocations.isEmpty() && TextUtils.isEmpty(mUser.getCurrency())) {
                    mUser.setCurrency(CurrencyUtils.getLocaleCurrency(mLocations.getCountry()));
                }
            } else if (TextUtils.isEmpty(user.getCurrency())) {
                if (mLocations != null && !mLocations.isEmpty()) {
                    user.setCurrency(CurrencyUtils.getLocaleCurrency(mLocations.getCountry()));
                }
            }
            return Observable.just(userResult);
        });
        ResponseObserverHelper<Response<User>> helper = new ResponseObserverHelper<>(mLoginDataManager.getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<User>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (ex.getCode() == 404) {
                    mShowButtonFacebook.set(true);
                    hideLoading();
                } else {
                    showError(ex.toString());
                }
            }

            @Override
            public void onComplete(Response<User> userResponse) {
                mUser = userResponse.body();
                if (mUser != null) {
                    if (!TextUtils.isEmpty(mUser.getAccessToken())) {
                        SharedPrefUtils.setAccessToken(mUser.getAccessToken(), mContext);
                    }

                    if (TextUtils.isEmpty(mFirstName.get())) {
                        mFirstName.set(mUser.getFirstName());
                    }
                    if (TextUtils.isEmpty(mLastName.get())) {
                        mLastName.set(mUser.getLastName());
                    }
                    if (TextUtils.isEmpty(mImageUri.get())) {
                        mImageUri.set(mUser.getUserProfile());
                    }
                    if (TextUtils.isEmpty(mEmail.get())) {
                        mEmail.set(mUser.getEmail());
                    }
                    if (TextUtils.isEmpty(mStoreName.get())) {
                        mStoreName.set(mUser.getStoreName());

                        if (!TextUtils.isEmpty(mStoreName.get())) {
                            mStoreNamePlaceHolder.set(mUser.getStoreName());
                        } else {
                            if (!TextUtils.isEmpty(mFirstName.get())) {
                                mStoreNamePlaceHolder.set(mUser.getFirstName());
                            }
                        }
                    }
                    mShowButtonFacebook.set(TextUtils.isEmpty(mUser.getSocialId()));
                }

                checkToEnableContinueButton(mFirstName.get(), mLastName.get(), mEmail.get());

                hideLoading();
            }
        }));
    }

    public void onBackClick() {
        if (mContext instanceof Activity) {
            ((Activity) mContext).finish();
        }
    }

    @Override
    public void onRetryClick() {
        super.onRetryClick();
        onContinueClick();
    }

    public void onContinueClick() {
        requestLogin();

//        mUser.setEmail(mEmail.get());
//        mUser.setFirstName(mFirstName.get());
//        mUser.setLastName(mLastName.get());
//        mUser.setUserProfile(mImageUri.get());
//
//        if (TextUtils.isEmpty(mUser.getCurrency())) {
//            mUser.setCurrency(CurrencyDb.getSelectedCurrency(mContext).getName());
//        }
//        if (mLocations != null && !mLocations.isEmpty()) {
//            mUser.setLatitude(mLocations.getLatitude());
//            mUser.setLongitude(mLocations.getLongtitude());
//            mUser.setCountry(mLocations.getCountry());
//        }
//
//        String storeName = mStoreNamePlaceHolder.get();
//        if (TextUtils.isEmpty(mStoreName.get())
//                && !TextUtils.equals(storeName, mFirstName.get())) {
//            storeName = mFirstName.get();
//        }
//        mUser.setStoreName(storeName);
//
//        boolean isNewUser = TextUtils.isEmpty(mUser.getId());
//
//        Observable<Response<User>> callback = mLoginDataManager.loginOrRegister(mUser, mImageUri::set);
//        ResponseObserverHelper<Response<User>> helper = new ResponseObserverHelper<>(mContext, callback, true);
//        addDisposable(helper.execute(new OnCallbackListener<Response<User>>() {
//            @Override
//            public void onFail(ErrorThrowable ex) {
//                String title;
//                String description;
//                String button;
//                if (ex.getCode() == ErrorThrowable.AdditionalError.UPDATE_EXIST_EMAIL.getCode()) {
//                    title = mContext.getString(R.string.error_login_title);
//                    description = mContext.getString(R.string.error_email_is_already_used);
//                } else if (ex.getCode() == ErrorThrowable.AdditionalError.UPDATE_EXIST_PHONE.getCode()) {
//                    title = mContext.getString(R.string.error_login_title);
//                    description = mContext.getString(R.string.error_phone_social_already_used);
//                } else if (ex.getCode() == ErrorThrowable.AdditionalError.UPDATE_EXIST_STORE_NAME.getCode()) {
//                    title = mContext.getString(R.string.error_login_title);
//                    description = mContext.getString(R.string.error_store_already_used);
//                } else if (ex.getCode() == ProfileUploader.sUploadError) {
//                    title = mContext.getString(R.string.error_login_title);
//                    description = mContext.getString(R.string.error_upload_profile_picture);
//                } else {
//                    title = mContext.getString(R.string.error_login_title);
//                    description = mContext.getString(R.string.error_general);
//                }
//                button = mContext.getString(R.string.change_language_button_ok);
//                showDialogMessage(mContext, title, description, button);
//                hideLoading();
//            }
//
//            @Override
//            public void onComplete(Response<User> value) {
//                onLoginOrRegisterSuccess(value.body(), isNewUser);
//            }
//        }));
    }

    private void onLoginOrRegisterSuccess(User user, boolean isNewUser) {
        if (user != null && !TextUtils.isEmpty(user.getId())) {
            SharedPrefUtils.setUserValue(user, mContext);
            if (mContext instanceof AbsBaseActivity) {
                if (((AbsBaseActivity) mContext).getSinchBinder() != null) {
                    mContext.startService(new Intent(mContext, SocketService.class));

                    ((AbsBaseActivity) mContext).getSinchBinder().startService(null);
                    ((AbsBaseActivity) mContext).bindCall(null);
                }
                if (isNewUser) {
                    mContext.startActivity(new SuggestionIntent(getContext()));
                }
                ((AbsBaseActivity) mContext).setResult(Activity.RESULT_OK);
                ((AbsBaseActivity) mContext).finish();
            }
        } else {
            showDialogMessage(mContext,
                    mContext.getString(R.string.error_login_title),
                    mContext.getString(R.string.error_general),
                    mContext.getString(R.string.change_language_button_ok));
            hideLoading();
        }
    }

    public void onFacebookClick() {
        if (mContext instanceof Activity) {
            if (mFacebookLoginHelper == null) {
                mFacebookLoginHelper = new FacebookLoginHelper((AppCompatActivity) mContext, new FacebookLoginHelper.LoginCallBack() {
                    @Override
                    public void onSuccess(FacebookLoginHelper.Authentication authentication) {
                        showLoading();
                        Observable<Response<Object>> callback
                                = mLoginDataManager.checkFbExist(new SocialRequest(authentication.getUserId()));
                        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(mContext, callback);
                        Disposable disposable = helper.execute(new OnCallbackListener<Response<Object>>() {
                            @Override
                            public void onFail(ErrorThrowable ex) {
                                checkOnRequestFacebook(ex, authentication);
                                hideLoading();
                            }

                            @Override
                            public void onComplete(Response<Object> result) {
                                showDialogMessage(mContext,
                                        mContext.getString(R.string.error_login_title),
                                        mContext.getString(R.string.error_exist_social),
                                        mContext.getString(R.string.change_language_button_ok));

                                hideLoading();
                            }
                        });
                        addDisposable(disposable);
                    }

                    @Override
                    public void onFail() {
                        showDialogMessage(mContext,
                                mContext.getString(R.string.error_login_title),
                                mContext.getString(R.string.error_change_social),
                                mContext.getString(R.string.change_language_button_ok));
                    }
                });
            }
            mFacebookLoginHelper.execute();
        }
    }

    private void checkOnRequestFacebook(ErrorThrowable ex, FacebookLoginHelper.Authentication authentication) {
        if (ex.getCode() == 404) {
            if (TextUtils.isEmpty(mImageUri.get())) {
                mImageUri.set(authentication.getPictureUrl());
            }
            if (TextUtils.isEmpty(mUser.getFirstName())) {
                mUser.setFirstName(authentication.getFirstName());
            }
            if (TextUtils.isEmpty(mUser.getLastName())) {
                mUser.setLastName(authentication.getLastName());
            }
            if (TextUtils.isEmpty(mFirstName.get())) {
                mFirstName.set(mUser.getFirstName());
                mLastName.set(mUser.getLastName());
                mStoreNamePlaceHolder.set(mUser.getFirstName());
            }
            if (TextUtils.isEmpty(mEmail.get())) {
                mEmail.set(authentication.getEmail());
            }

            if (TextUtils.isEmpty(mUser.getSocialId())) {
                mUser.setSocialId(authentication.getUserId());
            }

            mShowButtonFacebook.set(false);
        } else {
            showDialogMessage(mContext,
                    mContext.getString(R.string.error_login_title),
                    mContext.getString(R.string.error_change_social),
                    mContext.getString(R.string.change_language_button_ok));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mFacebookLoginHelper != null) {
            mFacebookLoginHelper.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void onProfileClick() {
        if (mContext instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).checkPermission(
                    new CheckPermissionCallbackHelper((AbsBaseActivity) getContext(),
                            CheckPermissionCallbackHelper.Type.CAMERA) {
                        @Override
                        public void onPermissionGranted() {
                            Intent intent = MyCameraIntent.newProfileInstance(getContext());
                            ((AbsBaseActivity) mContext).startActivityForResult(intent, new ResultCallback() {
                                @Override
                                public void onActivityResultSuccess(int resultCode, Intent data) {
                                    MyCameraResultIntent cameraIntent = new MyCameraResultIntent(data);
                                    List<MediaFile> file = cameraIntent.getMediaFiles();
                                    mImageUri.set(file.get(0).getPath());
                                    checkToEnableContinueButton(mFirstName.get(),
                                            mLastName.get(),
                                            mEmail.get());
                                }
                            });
                        }

                    }, Manifest.permission.CAMERA);
        }
    }


    public interface SignUpViewModelListener {
        void onLoginError(String message);

        void onLoginSuccess();

        void onChangePasswordRequire(AuthResponse authResponse);

        void onForgotPasswordClicked();
    }

}
