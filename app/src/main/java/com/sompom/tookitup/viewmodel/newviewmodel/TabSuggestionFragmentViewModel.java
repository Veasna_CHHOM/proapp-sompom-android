package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.ObservableBoolean;

import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.BaseObserverHelper;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.SuggestionDataManager;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 7/20/18.
 */

public class TabSuggestionFragmentViewModel extends AbsLoadingViewModel {
    public ObservableBoolean mIsRefresh = new ObservableBoolean();
    private SuggestionDataManager mSuggestionDataManager;
    private OnCallbackListListener<List<User>> mListListener;
    private boolean mIsLoadedData;

    public TabSuggestionFragmentViewModel(SuggestionDataManager dataManager,
                                          OnCallbackListListener<List<User>> listListener) {
        super(dataManager.getContext());
        mSuggestionDataManager = dataManager;
        mListListener = listListener;
        getData();
    }


    public void onRefresh() {
        mIsRefresh.set(true);
        getData();
    }

    private void getData() {
        if (!mIsRefresh.get()) {
            showLoading();
        }
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).requestLocation((location, isNeverAskAgain) -> {
                Observable<List<User>> observable = mSuggestionDataManager.getListSuggestion(location);
                BaseObserverHelper<List<User>> helper = new BaseObserverHelper<>(getContext(), observable);
                addDisposable(helper.execute(new OnCallbackListener<List<User>>() {
                    @Override
                    public void onFail(ErrorThrowable ex) {
                        if (mIsLoadedData) {
                            mIsRefresh.set(false);
                            return;
                        }
                        showError(ex.toString());
                        mIsLoadedData = false;
                        mIsRefresh.set(false);
                        if (mListListener != null) {
                            mListListener.onFail(ex);
                        }
                    }

                    @Override
                    public void onComplete(List<User> result) {
                        hideLoading();
                        mIsLoadedData = true;
                        mIsRefresh.set(false);
                        if (mListListener != null) {
                            mListListener.onComplete(result, mSuggestionDataManager.isCanLoadMore());
                        }
                    }
                }));
            });
        }
    }

    public OnClickListener onRetryButtonClick() {
        return this::getData;
    }

    public void follow(User user) {
        Observable<Response<Object>> call = mSuggestionDataManager.follow(user);
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), call);
        addDisposable(helper.execute());
    }
}
