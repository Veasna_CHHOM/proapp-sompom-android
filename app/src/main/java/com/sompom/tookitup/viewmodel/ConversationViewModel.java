package com.sompom.tookitup.viewmodel;

import android.content.Context;
import android.text.TextUtils;

import com.sompom.tookitup.listener.OnChatItemClickListener;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.utils.DateTimeUtils;

/**
 * Created by He Rotha on 9/14/17.
 */

public class ConversationViewModel implements OnChatItemClickListener {
    public Conversation mConversation;
    public User mRecipient;
    private OnChatItemClickListener mOnChatItemClickListener;

    public ConversationViewModel(Context context,
                                 Conversation conversation,
                                 OnChatItemClickListener onChatItemClickListener) {
        mConversation = conversation;
        mOnChatItemClickListener = onChatItemClickListener;
        mRecipient = conversation.getRecipient(context);
    }

    public String getMessage(Context context) {
        if (!TextUtils.isEmpty(mConversation.getSenderID())) {
            return mConversation.getLastMessage().getActualContent(context);
        } else {
            return "";
        }
    }

    public String getDate(Context context) {
        if (mConversation.getLastMessage().getDate().getTime() != 0) {
            return DateTimeUtils.parse(context, mConversation.getLastMessage().getDate().getTime());
        } else {
            return "";
        }
    }


    @Override
    public void onChatItemClicked(Conversation chatItemModel) {
        mOnChatItemClickListener.onChatItemClicked(chatItemModel);
    }
}
