package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.text.TextUtils;
import android.view.View;

import com.sompom.tookitup.R;
import com.sompom.tookitup.newui.dialog.ConfirmationDialog;

/**
 * Created by He Rotha on 9/10/18.
 */
public class ConfirmationDialogViewModel {
    public final ObservableField<String> mTitle = new ObservableField<>();
    public final ObservableField<String> mDescription = new ObservableField<>();
    public final ObservableField<String> mPositionButtonText = new ObservableField<>();
    public final ObservableInt mIcon = new ObservableInt();
    private DialogModel mDialogModel;
    private ConfirmationDialog mConfirmationDialog;

    public ConfirmationDialogViewModel(ConfirmationDialog dialog,
                                       DialogModel dialogModel) {
        mDialogModel = dialogModel;
        mConfirmationDialog = dialog;
        mTitle.set(mDialogModel.mTitle);
        mDescription.set(mDialogModel.mDescription);
        mIcon.set(mDialogModel.mIcon);
        if (TextUtils.isEmpty(dialogModel.mPositionButtonText)) {
            mPositionButtonText.set(dialog.getString(R.string.change_language_button_ok));
        } else {
            mPositionButtonText.set(mDialogModel.mPositionButtonText);
        }
    }

    public void onPositionButtonClick(View view) {
        mConfirmationDialog.dismiss();
        if (mDialogModel.mPositionButtonClick != null) {
            mDialogModel.mPositionButtonClick.onClick(view);
        }
    }

    public static class DialogModel {
        private String mTitle;
        private String mDescription;
        private String mPositionButtonText;
        private int mIcon = R.drawable.ic_error_small;
        private View.OnClickListener mPositionButtonClick;

        public void setTitle(String title) {
            mTitle = title;
        }

        public void setDescription(String description) {
            mDescription = description;
        }

        public void setPositionButtonText(String positionButtonText) {
            mPositionButtonText = positionButtonText;
        }

        public void setIcon(int icon) {
            mIcon = icon;
        }

        public void setPositionButtonClick(View.OnClickListener positionButtonClick) {
            mPositionButtonClick = positionButtonClick;
        }

    }
}
