package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.ObservableField;

public class ListItemUserGroupTitleViewModel {

    private ObservableField<String> mTitle = new ObservableField<>();

    public ListItemUserGroupTitleViewModel(String title) {
        mTitle.set(title);
    }

    public ObservableField<String> getTitle() {
        return mTitle;
    }
}
