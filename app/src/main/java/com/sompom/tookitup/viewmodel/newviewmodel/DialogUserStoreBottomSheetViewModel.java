package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.Bindable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.view.View;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.NetworkBroadcastReceiver;
import com.sompom.tookitup.helper.AnimationHelper;
import com.sompom.tookitup.helper.LoginActivityResultCallback;
import com.sompom.tookitup.helper.NavigateSellerStoreHelper;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.listener.OnCompleteListListener;
import com.sompom.tookitup.model.emun.AccountStatus;
import com.sompom.tookitup.model.emun.Status;
import com.sompom.tookitup.model.emun.StoreStatus;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.ProductStoreDataManager;
import com.sompom.tookitup.services.datamanager.StoreDataManager;
import com.sompom.tookitup.utils.FormatNumber;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 10/24/18.
 */

public class DialogUserStoreBottomSheetViewModel extends AbsLoadingViewModel {
    public final ObservableField<String> mCountryName = new ObservableField<>();
    public final ObservableField<String> mUserFollower = new ObservableField<>();
    public final ObservableField<String> mUserFollowing = new ObservableField<>();
    public final ObservableBoolean mIsFollow = new ObservableBoolean(false);
    public final ObservableBoolean mIsMe = new ObservableBoolean(false);
    private final ProductStoreDataManager mProductStoreDataManager;
    private final StoreDataManager mDataManager;
    private final OnCompleteListListener<List<Product>> mListOnCompleteListener;
    @Bindable
    public User mUser;
    private NetworkBroadcastReceiver.NetworkListener mNetworkListener;
    private boolean mIsReadUserDataReady;

    public DialogUserStoreBottomSheetViewModel(StoreDataManager dataManager,
                                               ProductStoreDataManager productStoreDataManager,
                                               User user,
                                               OnCompleteListListener<List<Product>> listOnCompleteListener) {
        super(dataManager.getContext());
        mDataManager = dataManager;
        mProductStoreDataManager = productStoreDataManager;
        mUser = user;
        mListOnCompleteListener = listOnCompleteListener;

        if (getContext() instanceof AbsBaseActivity) {
            mNetworkListener = networkState -> {
                if (!mIsReadUserDataReady) {
                    if (networkState == NetworkBroadcastReceiver.NetworkState.Connected) {
                        getData();
                    } else if (networkState == NetworkBroadcastReceiver.NetworkState.Disconnected) {
                        showNetworkError();
                    }
                }
            };
            ((AbsBaseActivity) getContext()).addNetworkStateChangeListener(mNetworkListener);
        }
    }

    public void onUserProfileClick() {
        new NavigateSellerStoreHelper(getContext(),
                mUser)
                .openSellerStore();
    }

    public OnClickListener onRetryButtonClick() {
        return this::getData;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).removeStateChangeListener(mNetworkListener);
        }
    }

    public String getFollowButtonText(boolean isFollow) {
        if (isFollow) {
            return getContext().getString(R.string.following);
        } else {
            return getContext().getString(R.string.seller_store_follow_title);
        }
    }

    private void setUserFollowers(boolean isFollow) {
        if (isFollow) {
            mUser.getContentStat().setTotalFollowers(mUser.getContentStat().getTotalFollowers() + 1);
        } else {
            mUser.getContentStat().setTotalFollowers(mUser.getContentStat().getTotalFollowers() - 1);
        }
        getFollowers();
    }

    public void onFollowClick(View view) {
        AnimationHelper.animateBounceFollowButton(view, 600, 0.15f, 10.0f);
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).startLoginWizardActivity(new LoginActivityResultCallback() {
                @Override
                public void onActivityResultSuccess(boolean isAlreadyLogin) {
                    if (!isAlreadyLogin) {
                        checkIsMe();
                    }
                    checkToPostFollow();
                }
            });
        }
    }

    private void checkToPostFollow() {
        if (!mIsMe.get()) {
            mIsFollow.set(!mIsFollow.get());
            boolean isFollow = mIsFollow.get();
            mUser.setFollow(isFollow);
            setUserFollowers(isFollow);
            Observable<Response<Object>> observable;
            if (isFollow) {
                observable = mDataManager.postFollow(mUser.getId());
            } else {
                observable = mDataManager.unFollow(mUser.getId());
            }

            ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), observable);
            addDisposable(helper.execute(value -> {
                mUser.setFollow(isFollow);
                notifyPropertyChanged(BR.user);
                getFollowers();
            }));
        }
    }

    public void loadMore(OnCallbackListListener<List<Product>> onCallback) {
        Observable<Response<List<Product>>> call = mProductStoreDataManager.loadMore();
        ResponseObserverHelper<Response<List<Product>>> helper
                = new ResponseObserverHelper<>(mDataManager.getContext(), call);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<Product>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                onCallback.onFail(ex);
            }

            @Override
            public void onComplete(Response<List<Product>> result) {
                onCallback.onComplete(result.body(), mDataManager.isCanLoadMore());
            }
        }));
    }

    public void getData() {
        showLoading();
        Observable<Response<User>> call = mDataManager.getUserById(mUser.getId());
        ResponseObserverHelper<Response<User>> observerHelper = new ResponseObserverHelper<>(getContext(), call);
        observerHelper.execute(new OnCallbackListener<Response<User>>() {
            @Override
            public void onComplete(Response<User> data) {
                mUser = data.body();

                if (mUser != null) {
                    mCountryName.set(mUser.getCountryName());
                    mIsFollow.set(mUser.isFollow());
                }

                mIsMe.set(SharedPrefUtils.checkIsMe(getContext(), mUser.getId()));
                notifyPropertyChanged(BR.user);
                checkIsMe();
                getFollowers();
                getFollowing();
                getListProductStore();
            }

            @Override
            public void onFail(ErrorThrowable ex) {
                if (ex.getCode() == 404) {
                    showError(getContext().getString(R.string.error_user_404));
                } else {
                    showError(ex.toString());
                }
            }
        });
    }

    private void checkIsMe() {
        if (SharedPrefUtils.isLogin(getContext())) {
            if (mUser.getId().equals(SharedPrefUtils.getUserId(getContext()))) {
                SharedPrefUtils.setUserValue(mUser, getContext());
                mIsMe.set(true);
            } else {
                mIsMe.set(false);
            }
        } else {
            mIsMe.set(false);
        }
    }

    private void getListProductStore() {
        if (initStatus()) {
            return;
        }

        Observable<Response<List<Product>>> call = mProductStoreDataManager.getProductByUserId(mUser, Status.ON_SALE.getStatusProduct());
        ResponseObserverHelper<Response<List<Product>>> helper
                = new ResponseObserverHelper<>(mDataManager.getContext(), call);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<Product>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (mIsReadUserDataReady) {
                    return;
                }

                mIsReadUserDataReady = false;
                if (ex.getCode() == 404) {
                    showNoItemError(getContext().getString(R.string.error_product_list_404));
                } else {
                    showError(ex.toString());
                }
            }

            @Override
            public void onComplete(Response<List<Product>> result) {
                hideLoading();
                mIsReadUserDataReady = true;
                if (mListOnCompleteListener != null) {
                    mListOnCompleteListener.onComplete(result.body(), mProductStoreDataManager.isCanLoadMore());
                }
            }
        }));
    }

    private boolean initStatus() {
        String myUserId = SharedPrefUtils.getUserId(getContext());
        if (mUser.getId().equals(myUserId)) {
            AccountStatus accountStatus = SharedPrefUtils.getAccountStatus(getContext());
            if (accountStatus == AccountStatus.BLOCK) {
                mButtonRetryText.set(R.string.seller_store_contact_us_title);
                showError(getContext().getString(R.string.seller_store_your_store_is_blocked_please_contact_our_email_message));
                return true;
            }
        } else {
            if (mUser.getStatus() != StoreStatus.ACTIVE || mUser.getAccountStatus() == AccountStatus.BLOCK) {
                showError(getContext().getString(R.string.seller_store_store_is_deactivated_message));
                mIsShowButtonRetry.set(false);
                return true;
            }
        }
        return false;
    }

    private void getFollowers() {
        long followers = mUser.getContentStat().getTotalFollowers();
        String followersText = getContext().getString(R.string.seller_store_followers_title);
        if (followers <= 1) {
            followersText = getContext().getString(R.string.suggestion_follower);
        }
        mUserFollower.set(FormatNumber.format(followers) + " " + followersText);
    }

    private void getFollowing() {
        long following = mUser.getContentStat().getTotalFollowings();
        String followingText = getContext().getString(R.string.seller_store_following_title);
        mUserFollowing.set(FormatNumber.format(following) + " " + followingText);
    }
}
