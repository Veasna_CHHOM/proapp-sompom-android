package com.sompom.tookitup.viewmodel.newviewmodel;

import android.app.Activity;
import android.content.Intent;
import android.databinding.ObservableField;
import android.os.Handler;

import com.sinch.android.rtc.calling.Call;
import com.sompom.baseactivity.ResultCallback;
import com.sompom.tookitup.broadcast.NetworkBroadcastReceiver;
import com.sompom.tookitup.chat.call.CallingService;
import com.sompom.tookitup.chat.call.NotificationCallVo;
import com.sompom.tookitup.database.CurrencyDb;
import com.sompom.tookitup.helper.CheckRequestLocationCallbackHelper;
import com.sompom.tookitup.helper.LoginActivityResultCallback;
import com.sompom.tookitup.helper.NavigateSellerStoreHelper;
import com.sompom.tookitup.intent.CallingIntent;
import com.sompom.tookitup.intent.newintent.MyCameraIntent;
import com.sompom.tookitup.intent.newintent.MyCameraResultIntent;
import com.sompom.tookitup.intent.newintent.NotificationIntent;
import com.sompom.tookitup.intent.newintent.ProductFormIntent;
import com.sompom.tookitup.intent.newintent.SearchMessageIntent;
import com.sompom.tookitup.intent.newintent.SearchOptionIntent;
import com.sompom.tookitup.model.result.Currency;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.model.result.UserBadge;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.BaseObserverHelper;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.ProductListDataManager;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 6/22/18.
 */

public class HomeFragmentViewModel extends AbsLoadingViewModel {
    public final ObservableField<User> mUser = new ObservableField<>();
    public final ObservableField<UserBadge> mUserBadge = new ObservableField<>();
    private final ProductListDataManager mDataManager;
    private NetworkBroadcastReceiver.NetworkListener mNetworkStateListener;
    private CallingService.SinchBinder mBinder;
    private OnQueryProductListener mOnQueryProductListener;
    public final BadgeViewModel mNotificationBadge = new BadgeViewModel();

    public HomeFragmentViewModel(ProductListDataManager dataManager, OnQueryProductListener listener) {
        super(dataManager.getContext());
        mOnQueryProductListener = listener;
        mDataManager = dataManager;
        if (SharedPrefUtils.isLogin(getContext())) {
            mUser.set(SharedPrefUtils.getUser(getContext()));

            if (getContext() instanceof AbsBaseActivity) {
                mNetworkStateListener = networkState -> {
                    if (networkState == NetworkBroadcastReceiver.NetworkState.Connected) {
                        ((AbsBaseActivity) getContext()).requestLocation(new CheckRequestLocationCallbackHelper((Activity) getContext(),
                                location -> {
                                    Observable<Response<User>> userCall = mDataManager.updateUserLocation(location);
                                    ResponseObserverHelper<Response<User>> userHelper = new ResponseObserverHelper<>(getContext(), userCall);
                                    addDisposable(userHelper.execute(result -> SharedPrefUtils.setUserValue(result.body(), getContext())));

                                    //get currency by country code
                                    Observable<List<Currency>> currencyCall = mDataManager.insertOrUpdateCurrency(location);
                                    BaseObserverHelper<List<Currency>> currencyHelper = new BaseObserverHelper<>(getContext(), currencyCall);
                                    addDisposable(currencyHelper.execute(result -> CurrencyDb.insertOrUpdateCurrencies(getContext(), result)));
                                }));

                        getUserBadge();
                    }
                };
                ((AbsBaseActivity) getContext()).addNetworkStateChangeListener(mNetworkStateListener);

                ((AbsBaseActivity) getContext()).bindCall(result -> {
                    mBinder = result;
                    result.addCallListener(new CallingService.CallingListener() {
                        @Override
                        public void onCallEnd() {

                        }

                        @Override
                        public void onIncomingCall(Call call) {
                            NotificationCallVo callVo = new NotificationCallVo();
                            callVo.setData((HashMap<String, String>) call.getHeaders());
                            getContext().startActivity(new CallingIntent(getContext(), callVo));
                        }

                        @Override
                        public void onCallEstablished(Call call) {

                        }

                        @Override
                        public void onCallProgressing(Call call) {

                        }
                    });
                });
            }


        }
    }

    public void setNotificationBadgeValue(long badgeValue) {
        mNotificationBadge.setBadgeValue(badgeValue);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mBinder != null) {
            mBinder.stop();
        }
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).removeStateChangeListener(mNetworkStateListener);
        }
    }

    public void onImageProfileClick() {
        new NavigateSellerStoreHelper(getContext(),
                SharedPrefUtils.getUser(getContext()))
                .openMyStore(true, mUser::set);
    }

    public void onSearchButtonClick() {
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).startActivityForResult(new SearchOptionIntent(getContext()), new ResultCallback() {
                @Override
                public void onActivityResultSuccess(int resultCode, Intent data) {
                    mOnQueryProductListener.onQueryProductOption(true);
                }
            });
        }
    }

    public void onNotificationButtonClick() {
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).startActivityForResult(new NotificationIntent(getContext()),
                    new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {

                        }
                    });
        }
        new Handler().postDelayed(mNotificationBadge::updateBadgeValue, 100);
    }

    public void onTitleClick() {
        getContext().startActivity(new SearchMessageIntent(getContext()));
    }

    public void onFloatingButtonClick() {
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).startLoginWizardActivity(new LoginActivityResultCallback() {
                @Override
                public void onActivityResultSuccess(boolean isAlreadyLogin) {
                    if (isAlreadyLogin) {
                        Intent intent = MyCameraIntent.newProductInstance(getContext(), null);
                        ((AbsBaseActivity) getContext()).startActivityForResult(intent, new ResultCallback() {
                            @Override
                            public void onActivityResultSuccess(int resultCode, Intent data) {
                                MyCameraResultIntent resultIntent = new MyCameraResultIntent(data);
                                getContext().startActivity(new ProductFormIntent(getContext(), resultIntent.getMedia()));
                            }
                        });
                    }
                }
            });
        }
    }

    private void getUserBadge() {
        Observable<Response<UserBadge>> observable = mDataManager.getUerBadge();
        ResponseObserverHelper<Response<UserBadge>> helper = new ResponseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(result -> {
            mUserBadge.set(result.body());

            //Update Notification badge
            if (result.body() != null) {
                mNotificationBadge.setBadgeValue(result.body().getUnreadNotification());
            }
        }));
    }

    public interface OnQueryProductListener {
        void onQueryProductOption(boolean isEnable);
    }
}
