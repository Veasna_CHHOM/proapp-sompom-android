package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;
import android.databinding.Bindable;
import android.support.v7.app.AppCompatActivity;

import com.sompom.tookitup.BR;
import com.sompom.tookitup.helper.NavigateSellerStoreHelper;
import com.sompom.tookitup.intent.CallingIntent;
import com.sompom.tookitup.intent.ChatIntent;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.viewmodel.AbsBaseViewModel;

public class ListItemUserViewModel extends AbsBaseViewModel {

    @Bindable
    public User mUser = new User();
    private final Context mContext;

    public ListItemUserViewModel(Context context, User user) {
        mContext = context;
        mUser = user;
        bindData();
    }

    private void bindData() {
        if (mUser != null) {
            notifyPropertyChanged(BR.user);
        }
    }

    public final void onCallClick() {
        if (mUser == null) {
            return;
        }

        mContext.startActivity(new CallingIntent(mContext, mUser, null));
    }

    public final void onMessageClick() {
        if (mUser == null) {
            return;
        }

        mContext.startActivity(new ChatIntent(mContext, mUser));
    }

    public final void onProfileClick() {
        if (mUser == null) {
            return;
        }

        NavigateSellerStoreHelper.openSellerStore((AppCompatActivity) mContext, mUser.getId());
    }
}
