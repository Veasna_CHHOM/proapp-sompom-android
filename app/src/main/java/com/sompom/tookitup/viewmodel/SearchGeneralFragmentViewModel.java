package com.sompom.tookitup.viewmodel;

import android.databinding.ObservableBoolean;

import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.NetworkBroadcastReceiver;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.listener.OnCompleteListener;
import com.sompom.tookitup.model.ActiveUser;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.BaseObserverHelper;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.datamanager.SearchGeneralDataManager;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by nuonveyo on 8/2/18.
 */

public class SearchGeneralFragmentViewModel extends AbsLoadingViewModel {
    public final ObservableBoolean mIsRefresh = new ObservableBoolean();
    private final SearchGeneralDataManager mSearchGeneralDataManager;
    private final OnCompleteListener<List<ActiveUser>> mCompleteListener;
    private NetworkBroadcastReceiver.NetworkListener mNetworkStateListener;
    private boolean mIsLoadedData;

    public SearchGeneralFragmentViewModel(SearchGeneralDataManager dataManager, OnCompleteListener<List<ActiveUser>> completeListener) {
        super(dataManager.getContext());
        mSearchGeneralDataManager = dataManager;
        mCompleteListener = completeListener;

        if (getContext() instanceof AbsBaseActivity) {
            mNetworkStateListener = networkState -> {
                if (!mIsLoadedData) {
                    if (networkState == NetworkBroadcastReceiver.NetworkState.Connected) {
                        getSearchGeneralUser();
                    } else if (networkState == NetworkBroadcastReceiver.NetworkState.Disconnected) {
                        showNetworkError();
                    }
                }
            };
            ((AbsBaseActivity) getContext()).addNetworkStateChangeListener(mNetworkStateListener);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).removeStateChangeListener(mNetworkStateListener);
        }
    }

    public void onRefresh() {
        mIsRefresh.set(true);
        getSearchGeneralUser();
    }

    public OnClickListener onButtonRetryClick() {
        return this::getSearchGeneralUser;
    }


    private void getSearchGeneralUser() {
        if (!mIsRefresh.get()) {
            showLoading();
        }
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).requestLocation((location, isNeverAskAgain) -> {
                Observable<List<ActiveUser>> observable = mSearchGeneralDataManager.getSearchGeneralUser(location);
                BaseObserverHelper<List<ActiveUser>> helper = new BaseObserverHelper<>(getContext(), observable);
                addDisposable(helper.execute(new OnCallbackListener<List<ActiveUser>>() {
                    @Override
                    public void onFail(ErrorThrowable ex) {
                        mIsRefresh.set(false);
                        if (mIsLoadedData) {
                            return;
                        }

                        mIsLoadedData = false;
                        if (ex.getCode() == 500) {
                            showError(getContext().getString(R.string.error_form));
                        } else if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                            showNetworkError();
                        } else {
                            showError(ex.toString());
                        }
                    }

                    @Override
                    public void onComplete(List<ActiveUser> result) {
                        mIsLoadedData = true;
                        mIsRefresh.set(false);
                        hideLoading();
                        if (mCompleteListener != null) {
                            mCompleteListener.onComplete(result);
                        }
                    }
                }));
            });
        }
    }
}
