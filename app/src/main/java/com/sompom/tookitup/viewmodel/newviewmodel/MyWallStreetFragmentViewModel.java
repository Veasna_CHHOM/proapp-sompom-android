package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.ObservableBoolean;

import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.NetworkBroadcastReceiver;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.listener.OnCompleteListListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.WallStreetAdaptive;
import com.sompom.tookitup.model.request.ReportRequest;
import com.sompom.tookitup.model.result.LoadMoreWrapper;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.BaseObserverHelper;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.WallStreetDataManager;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 8/1/18.
 */

public class MyWallStreetFragmentViewModel extends AbsLoadingViewModel {
    public final ObservableBoolean mIsRefresh = new ObservableBoolean();
    private final WallStreetDataManager mWallStreetDataManager;
    private final OnCompleteListListener<List<Adaptive>> mListOnCallbackListener;
    private boolean mIsLoadedData;
    private NetworkBroadcastReceiver.NetworkListener mNetworkStateListener;
    private String mUserId;

    public MyWallStreetFragmentViewModel(String userId,
                                         WallStreetDataManager dataManager,
                                         OnCompleteListListener<List<Adaptive>> listOnCallbackListener) {
        super(dataManager.getContext());
        mUserId = userId;
        mWallStreetDataManager = dataManager;
        mListOnCallbackListener = listOnCallbackListener;

        if (getContext() instanceof AbsBaseActivity) {
            mNetworkStateListener = networkState -> {
                if (!mIsLoadedData) {
                    if (networkState == NetworkBroadcastReceiver.NetworkState.Connected) {
                        getTimeline();
                    } else if (networkState == NetworkBroadcastReceiver.NetworkState.Disconnected) {
                        showNetworkError();
                    }
                }
            };
            ((AbsBaseActivity) getContext()).addNetworkStateChangeListener(mNetworkStateListener);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).removeStateChangeListener(mNetworkStateListener);
        }
    }

    public void onRefresh() {
        mIsRefresh.set(true);
        getTimeline();
    }

    private void getTimeline() {
        if (!mIsRefresh.get()) {
            showLoading();
        }
        mWallStreetDataManager.resetPagination();
        Observable<LoadMoreWrapper<Adaptive>> callback = mWallStreetDataManager.getMyWallStreet(mUserId);
        BaseObserverHelper<LoadMoreWrapper<Adaptive>> helper = new BaseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<LoadMoreWrapper<Adaptive>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (mIsLoadedData) {
                    mIsRefresh.set(false);
                    return;
                }
                mIsLoadedData = false;
                mIsRefresh.set(false);
                if (ex.getCode() == 500) {
                    showError(getContext().getString(R.string.error_form));
                } else if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                    showNetworkError();
                } else {
                    showError(ex.toString());
                }
            }

            @Override
            public void onComplete(LoadMoreWrapper<Adaptive> result) {
                mIsLoadedData = true;
                mIsRefresh.set(false);
                mIsError.set(false);
                mListOnCallbackListener.onComplete(result.getData(),
                        mWallStreetDataManager.isCanLoadMore());

                hideLoading();
            }
        }));
    }

    public void loadMoreData(OnCallbackListListener<LoadMoreWrapper<Adaptive>> listener) {
        Observable<LoadMoreWrapper<Adaptive>> callback = mWallStreetDataManager.loadMoreMyWallStreet(mUserId);
        BaseObserverHelper<LoadMoreWrapper<Adaptive>> helper = new BaseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<LoadMoreWrapper<Adaptive>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                listener.onFail(ex);
            }

            @Override
            public void onComplete(LoadMoreWrapper<Adaptive> result) {
                listener.onComplete(result, mWallStreetDataManager.isCanLoadMore());
            }
        }));
    }

    public void checkToPostFollow(String id, boolean isFollow) {
        Observable<Response<Object>> callback;
        if (isFollow) {
            callback = mWallStreetDataManager.postFollow(id);
        } else {
            callback = mWallStreetDataManager.unFollow(id);
        }
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute());
    }

    public void deletePost(WallStreetAdaptive lifeStream) {
        Observable<Response<Object>> call = mWallStreetDataManager.deletePost(lifeStream.getId());
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), call);
        addDisposable(helper.execute());
    }

    public void reportPost(String id, ReportRequest reportRequest) {
        Observable<Response<Object>> request = mWallStreetDataManager.reportPost(id, reportRequest);
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), request);
        addDisposable(helper.execute());
    }

    public void unFollowShop(String id) {
        Observable<Response<Object>> observable = mWallStreetDataManager.unFollowShop(id);
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute());
    }

    public OnClickListener onRetryButtonClick() {
        return this::getTimeline;
    }
}
