package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.text.TextUtils;

import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.NetworkBroadcastReceiver;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.listener.OnEditTextCommentListener;
import com.sompom.tookitup.listener.OnTimelineHeaderItemClickListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.LifeStream;
import com.sompom.tookitup.model.SharedProduct;
import com.sompom.tookitup.model.SharedTimeline;
import com.sompom.tookitup.model.WallStreetAdaptive;
import com.sompom.tookitup.model.request.ReportRequest;
import com.sompom.tookitup.model.result.Comment;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.TimelineDetailDataManager;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 7/12/18.
 */

public class TimelineDetailViewModel extends ListItemTimelineHeaderViewModel {
    public final ObservableBoolean mIsShowComment = new ObservableBoolean();
    public final ObservableField<LayoutEditTextCommentViewModel> mEditTextViewModel = new ObservableField<>();

    private final OnEditTextCommentListener mOnEditTextCommentListener;
    private final Callback onTimelineHeaderItemClickListener;
    private final TimelineDetailDataManager mDetailDataManager;
    private final String mWallStreetId;
    private final NetworkBroadcastReceiver.NetworkListener mNetworkStateListener;
    private WallStreetAdaptive mWallStreetAdaptive;
    private LayoutEditTextCommentViewModel mLayoutEditTextCommentViewModel;
    private boolean mIsLoadedData;
    private String mLastCommentId = "0";

    public TimelineDetailViewModel(AbsBaseActivity activity,
                                   String wallStreetId,
                                   TimelineDetailDataManager dataManager,
                                   WallStreetAdaptive adaptive,
                                   Callback listener,
                                   OnEditTextCommentListener onEditTextCommentListener) {

        super(activity, (Adaptive) adaptive, 0, listener);
        mWallStreetId = wallStreetId;
        mDetailDataManager = dataManager;
        mOnEditTextCommentListener = onEditTextCommentListener;
        onTimelineHeaderItemClickListener = listener;

        if (!isRequestWallStreetDetail()) {
            checkToShowComment(adaptive);
        }

        mNetworkStateListener = networkState -> {
            if (!mIsLoadedData) {
                if (networkState == NetworkBroadcastReceiver.NetworkState.Connected) {
                    loadData();
                } else if (networkState == NetworkBroadcastReceiver.NetworkState.Disconnected) {
                    if (isRequestWallStreetDetail()) {
                        showNetworkError();
                    }
                }
            }
        };
        mContext.addNetworkStateChangeListener(mNetworkStateListener);
    }

    private boolean isRequestWallStreetDetail() {
        return !TextUtils.isEmpty(mWallStreetId);
    }

    private void loadData() {
        if (isRequestWallStreetDetail()) {
            requestWallStreetDetail();
        } else {
            if (mIsShowComment.get()) {
                getCommentList();
            }
        }
    }

    private void requestWallStreetDetail() {
        showLoading();
        Observable<Response<LifeStream>> callback = mDetailDataManager.getWallStreetDetail(mWallStreetId);
        ResponseObserverHelper<Response<LifeStream>> helper = new ResponseObserverHelper<>(mDetailDataManager.getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<LifeStream>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (mIsLoadedData) {
                    return;
                }
                showError(ex.getMessage());
            }

            @Override
            public void onComplete(Response<LifeStream> result) {
                mIsLoadedData = true;
                hideLoading();
                setAdaptive(result.body());
                checkToShowComment(result.body());
                if (mIsShowComment.get()) {
                    getCommentList();
                }
                onTimelineHeaderItemClickListener.onGetCommentDetailSuccess(result.body());
                TimelineDetailViewModel.this.notifyChange();

            }
        }));
    }

    @Override
    public void onRetryClick() {
        super.onRetryClick();
        if (isRequestWallStreetDetail()) {
            requestWallStreetDetail();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mContext.removeStateChangeListener(mNetworkStateListener);

    }

    public void checkToPostFollow(String userId, boolean isFollow) {
        Observable<Response<Object>> callback;
        if (isFollow) {
            callback = mDetailDataManager.postFollow(userId);
        } else {
            callback = mDetailDataManager.unFollow(userId);
        }
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(mDetailDataManager.getContext(), callback);
        addDisposable(helper.execute());
    }

    public void reportPost(String id, ReportRequest reportRequest) {
        Observable<Response<Object>> request = mDetailDataManager.reportPost(id, reportRequest);
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(mDetailDataManager.getContext(), request);
        addDisposable(helper.execute());
    }

    public void unFollowShop(String id) {
        Observable<Response<Object>> observable = mDetailDataManager.unFollowShop(id);
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute());
    }

    public void onBackPress() {
        mContext.onBackPressed();
    }

    public void postComment(Comment comment) {
        if (mLayoutEditTextCommentViewModel != null) {
            mLayoutEditTextCommentViewModel.postComment(comment);
        }
    }

    private void getCommentList() {
        Observable<Response<List<Comment>>> callback = mDetailDataManager.getCommentList(mWallStreetId, mLastCommentId);
        ResponseObserverHelper<Response<List<Comment>>> helper = new ResponseObserverHelper<>(mContext, callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<Comment>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mIsLoadedData = false;
            }

            @Override
            public void onComplete(Response<List<Comment>> value) {
                mIsLoadedData = true;
                mLastCommentId = mLayoutEditTextCommentViewModel.getLastSubCommentId(value.body());
                onTimelineHeaderItemClickListener.onLoadCommentSuccess(value.body(),
                        mDetailDataManager.isCanLoadMore());
            }
        }));
    }

    public void loadMoreCommentList(OnCallbackListListener<Response<List<Comment>>> listListener) {
        Observable<Response<List<Comment>>> callback = mDetailDataManager.getCommentList(mWallStreetId, mLastCommentId);
        ResponseObserverHelper<Response<List<Comment>>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<List<Comment>>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                listListener.onFail(ex);
            }

            @Override
            public void onComplete(Response<List<Comment>> value) {
                mLastCommentId = mLayoutEditTextCommentViewModel.getLastSubCommentId(value.body());
                listListener.onComplete(value, mDetailDataManager.isCanLoadMore());
            }
        }));
    }

    private void checkToShowComment(WallStreetAdaptive adaptive) {
        List<Adaptive> adaptiveList = new ArrayList<>();
        if (adaptive instanceof SharedTimeline || adaptive instanceof SharedProduct) {
            adaptiveList.add((Adaptive) adaptive);

            if (adaptive instanceof SharedProduct) {
                mShareTextTitle.set(mContext.getString(R.string.home_shared_product_title));
                mWallStreetAdaptive = ((SharedProduct) adaptive).getProduct();
            } else {
                mShareTextTitle.set(mContext.getString(R.string.home_shared_post_title));
                mWallStreetAdaptive = ((SharedTimeline) adaptive).getLifeStream();
            }
            showCommentLayout(adaptive.getId());
        } else {
            mWallStreetAdaptive = adaptive;
            if (!TextUtils.isEmpty(mWallStreetAdaptive.getDescription())
                    || (mWallStreetAdaptive.getMedia() != null && mWallStreetAdaptive.getMedia().size() == 1)) {
                adaptiveList.add((Adaptive) mWallStreetAdaptive);
            }

            if (mWallStreetAdaptive.getMedia() != null && mWallStreetAdaptive.getMedia().size() > 1) {
                adaptiveList.addAll(mWallStreetAdaptive.getMedia());
            }

            if (adaptiveList.size() == 1 && adaptiveList.get(0) instanceof LifeStream) {
                showCommentLayout(adaptive.getId());
            }
        }

        onTimelineHeaderItemClickListener.onShowAdapter(adaptiveList, mIsShowComment.get());
    }

    private void showCommentLayout(String wallStreetId) {
        mIsShowComment.set(true);
        mLayoutEditTextCommentViewModel = new LayoutEditTextCommentViewModel(mDetailDataManager,
                wallStreetId,
                mOnEditTextCommentListener);
        mEditTextViewModel.set(mLayoutEditTextCommentViewModel);
    }

    public void setUpdateComment(Comment comment, int updateCommentPosition) {
        if (mLayoutEditTextCommentViewModel != null) {
            mLayoutEditTextCommentViewModel.mMessage.set(comment.getContent());
            mLayoutEditTextCommentViewModel.setUpdateComment(comment, updateCommentPosition);
        }
    }

    public void onDeleteComment(Comment comment, OnClickListener onClickListener) {
        showLoading();
        Observable<Response<Object>> callback = mDetailDataManager.deleteComment(comment.getId());
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mErrorMessage.set(null);
                if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                    mErrorMessage.set(ex.toString());
                } else {
                    mErrorMessage.set(getContext().getString(R.string.error_cannot_delete_comment));
                }
                hideLoading();
            }

            @Override
            public void onComplete(Response<Object> result) {
                hideLoading();
                onClickListener.onClick();
            }
        }));
    }

    public void deletePost(WallStreetAdaptive adaptive) {
        Observable<Response<Object>> call = mDetailDataManager.deletePost(adaptive.getId());
        ResponseObserverHelper<Response<Object>> helper
                = new ResponseObserverHelper<>(getContext(), call);
        addDisposable(helper.execute());
    }

    public interface Callback extends OnTimelineHeaderItemClickListener {
        void onShowAdapter(List<Adaptive> adaptives, boolean isShowEditText);

        void onLoadCommentSuccess(List<Comment> commentList, boolean isCanLoadMore);

        void onGetCommentDetailSuccess(WallStreetAdaptive adaptive);
    }
}
