package com.sompom.tookitup.viewmodel.newviewmodel;

import android.app.Activity;
import android.content.Context;
import android.databinding.ObservableField;
import android.location.Address;
import android.location.Geocoder;
import android.text.TextWatcher;

import com.google.gson.Gson;
import com.sompom.tookitup.helper.CheckRequestLocationCallbackHelper;
import com.sompom.tookitup.helper.DelayWatcher;
import com.sompom.tookitup.model.Locations;
import com.sompom.tookitup.model.SearchAddressResult;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.SelectAddressActivity;
import com.sompom.tookitup.services.datamanager.SelectAddressDataManager;
import com.sompom.tookitup.viewmodel.AbsBaseViewModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by nuonveyo on 6/11/18.
 */

public class SearchAddressFragmentViewModel extends AbsBaseViewModel {
    private static final int MAX_ADDRESS_RESULT = 30;
    public final ObservableField<String> mContent = new ObservableField<>();
    private final Context mContext;
    private final OnCallback mOnCallback;
    private final SelectAddressDataManager mSelectAddressDataManager;
    private SearchAddressResult mSearchAddressResult;

    public SearchAddressFragmentViewModel(SelectAddressDataManager selectAddressDataManager,
                                          SearchAddressResult searchAddressResult,
                                          OnCallback onCallback) {
        mSelectAddressDataManager = selectAddressDataManager;
        mSearchAddressResult = searchAddressResult;
        mContext = selectAddressDataManager.getContext();
        mOnCallback = onCallback;

        if (mSearchAddressResult != null) {
            if (mSearchAddressResult.getLocations() == null) {
                requestLocation();
            } else {
                mContent.set(mSearchAddressResult.getFullAddress());
                onSearchAddress(SearchType.GOOGLE_PLAY_API);
            }
        } else {
            requestLocation();
        }
    }

    public TextWatcher onSearchAddressTextChanged() {
        return new DelayWatcher(true) {
            @Override
            public void onTextChanged(String text) {
                //clear previous api
                clearDisposable();

                mContent.set(text);
                onSearchAddress(SearchType.GOOGLE_PLAY_API);
            }
        };
    }

    private void onSearchAddress(SearchType searchType) {
        if (searchType == SearchType.GEO_CODER) {
            Observable<List<SearchAddressResult>> observable = Observable.just(mContent.get()).concatMap(content -> {
                Geocoder geocoder = new Geocoder(mContext);
                List<Address> addressList = geocoder.getFromLocationName(content, MAX_ADDRESS_RESULT);
                Timber.e("addressList: %s", new Gson().toJson(addressList));
                if (addressList != null && !addressList.isEmpty()) {
                    List<SearchAddressResult> searchAddressResults = new ArrayList<>();
                    for (Address address : addressList) {
                        SearchAddressResult searchAddressResult = new SearchAddressResult();
                        searchAddressResult.setFullAddress(address.getAddressLine(0));
                        searchAddressResult.setCity(address.getLocality());
                        searchAddressResult.setCountry(address.getCountryName());

                        Locations locations = new Locations();
                        locations.setLatitude(address.getLatitude());
                        locations.setLongtitude(address.getLongitude());
                        locations.setCountry(address.getCountryCode());
                        searchAddressResult.setLocations(locations);
                        searchAddressResults.add(searchAddressResult);
                    }
                    return Observable.just(searchAddressResults);
                } else {
                    return mSelectAddressDataManager.searchAddress(content);
                }
            });
            addDisposable(observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(addressResults -> {
                        if (mOnCallback != null) {
                            mOnCallback.onGetAddressSuccess(addressResults);
                        }
                    }, throwable -> {
                        Timber.e(throwable.toString());
                        if (mOnCallback != null) {
                            mOnCallback.onGetAddressSuccess(new ArrayList<>());
                        }
                    }));
        } else {
            searchAddressFromGooglePlace();
        }
    }

    private void searchAddressFromGooglePlace() {
        Observable<List<SearchAddressResult>> observable = mSelectAddressDataManager.searchAddress(mContent.get());
        addDisposable(observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(addressResults -> {
                    if (mOnCallback != null) {
                        mOnCallback.onGetAddressSuccess(addressResults);
                    }
                }, throwable -> {
                    if (mOnCallback != null) {
                        mOnCallback.onGetAddressSuccess(new ArrayList<>());
                    }
                }));
    }

    public void searchDetailAddress(SearchAddressResult searchAddressResult, OnSearchAddressDetailListener onSearchAddressDetailListener) {
        Observable<SearchAddressResult> observable = mSelectAddressDataManager.searchAddressDetail(searchAddressResult);
        addDisposable(observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onSearchAddressDetailListener::searchAddressSuccess, throwable -> Timber.e("search address detail fail!")));
    }

    private void requestLocation() {
        if (mContext instanceof AbsBaseActivity) {
            ((AbsBaseActivity) mContext).requestLocation(new CheckRequestLocationCallbackHelper((Activity) mContext,
                    location -> {
                        mSearchAddressResult = new SearchAddressResult();
                        mSearchAddressResult.setLocations(location);
                    }));
        }
    }

    public void onSetLocationOnMapClick() {
        if (mContext instanceof SelectAddressActivity) {
            ((SelectAddressActivity) mContext).navigateToSetLocationOnMap();
        }
    }

    public void onBackPress() {
        if (mContext instanceof SelectAddressActivity) {
            ((SelectAddressActivity) mContext).onBackPressed();
        }
    }

    public enum SearchType {
        GEO_CODER, GOOGLE_PLAY_API
    }

    public interface OnCallback {
        void onGetAddressSuccess(List<SearchAddressResult> list);
    }

    public interface OnSearchAddressDetailListener {
        void searchAddressSuccess(SearchAddressResult addressResult);
    }
}
