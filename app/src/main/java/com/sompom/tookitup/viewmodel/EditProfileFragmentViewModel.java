package com.sompom.tookitup.viewmodel;

import android.Manifest;
import android.content.Intent;
import android.databinding.Bindable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.support.annotation.StringRes;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;

import com.desmond.squarecamera.model.AppTheme;
import com.desmond.squarecamera.utils.ThemeManager;
import com.sompom.baseactivity.ResultCallback;
import com.sompom.tookitup.BR;
import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.NetworkBroadcastReceiver;
import com.sompom.tookitup.database.CurrencyDb;
import com.sompom.tookitup.helper.CheckPermissionCallbackHelper;
import com.sompom.tookitup.helper.FacebookSMSValidation;
import com.sompom.tookitup.helper.LocaleManager;
import com.sompom.tookitup.helper.upload.ProfileUploader;
import com.sompom.tookitup.intent.SelectAddressIntent;
import com.sompom.tookitup.intent.SelectAddressIntentResult;
import com.sompom.tookitup.intent.newintent.MyCameraIntent;
import com.sompom.tookitup.intent.newintent.MyCameraResultIntent;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnCompleteListener;
import com.sompom.tookitup.listener.OnEditProfileListener;
import com.sompom.tookitup.listener.OnSpannableClickListener;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.SearchAddressResult;
import com.sompom.tookitup.model.emun.StoreStatus;
import com.sompom.tookitup.model.result.NotificationSettingModel;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.newui.EditProfileActivity;
import com.sompom.tookitup.newui.dialog.ChangeLanguageDialog;
import com.sompom.tookitup.newui.dialog.ChangePasswordDialog;
import com.sompom.tookitup.newui.dialog.CurrencyDialog;
import com.sompom.tookitup.newui.dialog.InputMessageDialog;
import com.sompom.tookitup.newui.dialog.ThemeDialog;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.StoreDataManager;
import com.sompom.tookitup.utils.SharedPrefUtils;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * Created by nuonveyo on 6/13/18.
 */

public class EditProfileFragmentViewModel extends LayoutToolbarEditProfileViewModel {

    private static final int ERROR_GET_DATA = 0X001;
    public final ObservableField<String> mStoreName = new ObservableField<>("");
    public final ObservableField<String> mFirstName = new ObservableField<>("");
    public final ObservableField<String> mLastName = new ObservableField<>("");
    public final ObservableField<String> mEmail = new ObservableField<>("");
    public final ObservableBoolean mIsEnablePhoneClick = new ObservableBoolean();
    public final ObservableField<String> mPhone = new ObservableField<>("");
    public final ObservableField<String> mCover = new ObservableField<>();
    public final ObservableField<String> mAddress = new ObservableField<>();
    public final ObservableField<String> mCurrency = new ObservableField<>();
    public final ObservableField<String> mLanguage = new ObservableField<>();
    public final ObservableField<AppTheme> mTheme = new ObservableField<>();
    public final OnSpannableClickListener mSpannableClickListener;
    private final StoreDataManager mStoreDataManager;
    private final OnEditProfileListener mOnCallbackListener;
    private final FragmentManager mFragmentManager;
    @Bindable
    public User mUser = new User();
    private int mErrorType = ERROR_GET_DATA;
    private SearchAddressResult mSearchAddressResult = new SearchAddressResult();
    private FacebookSMSValidation mSMSValidationUtil;
    private int mLanguageSelectedPosition;
    private boolean mIsLoadedData;
    private NetworkBroadcastReceiver.NetworkListener mNetworkStateListener;
    private OnCallbackListener mUpdateCallback = new OnCallbackListener<Response<User>>() {
        @Override
        public void onFail(ErrorThrowable ex) {
            hideLoading();
            if (ex.getCode() == ErrorThrowable.AdditionalError.UPDATE_EXIST_EMAIL.getCode()) {
                showSnackBar(R.string.error_email_is_already_used);
            } else if (ex.getCode() == ErrorThrowable.AdditionalError.UPDATE_EXIST_PHONE.getCode()) {
                showSnackBar(R.string.error_phone_social_already_used);
            } else if (ex.getCode() == ErrorThrowable.AdditionalError.UPDATE_EXIST_STORE_NAME.getCode()) {
                showSnackBar(R.string.error_store_already_used);
            } else if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                showSnackBar(R.string.error_no_internet_connection_message);
            } else {
                showSnackBar(R.string.error_cannot_update_user);
            }
        }

        @Override
        public void onComplete(Response<User> result) {
            showSnackBar(R.string.profile_updated);
            CurrencyDb.updateSelectCurrency(getContext(), mCurrency.get());
            setUser(result.body());
            mIsShowButtonSave.set(false);
            hideLoading();
        }
    };

    public EditProfileFragmentViewModel(StoreDataManager storeDataManager,
                                        FragmentManager fragmentManager,
                                        OnEditProfileListener onCallbackListener,
                                        OnSpannableClickListener onSpannableClickListener) {
        super(storeDataManager.getContext());
        mStoreDataManager = storeDataManager;
        mOnCallbackListener = onCallbackListener;
        mSpannableClickListener = onSpannableClickListener;
        mTheme.set(ThemeManager.getAppTheme(getContext()));
        mFragmentManager = fragmentManager;
        if (getContext() instanceof AbsBaseActivity) {
            mNetworkStateListener = networkState -> {
                if (!mIsLoadedData) {
                    if (networkState == NetworkBroadcastReceiver.NetworkState.Connected) {
                        getMyUserProfile();
                    } else if (networkState == NetworkBroadcastReceiver.NetworkState.Disconnected) {
                        showNetworkError();
                    }
                }
            };
            ((AbsBaseActivity) getContext()).addNetworkStateChangeListener(mNetworkStateListener);
        }

        mLanguageSelectedPosition = LocaleManager.selectLanguagePosition(getContext());
        setLanguage();

        mIsEnablePhoneClick.set(CurrencyDb.getCurrencies(getContext()).size() > 1);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).removeStateChangeListener(mNetworkStateListener);
        }
    }

    @Override
    public void onSaveButtonClick() {
        showLoading();
        mUser.setStoreName(mStoreName.get());
        mUser.setFirstName(mFirstName.get());
        mUser.setLastName(mLastName.get());
        mUser.setEmail(mEmail.get());
        mUser.setPhone(mPhone.get());
        mUser.setCity(mAddress.get());
        mUser.setCurrency(mCurrency.get());
        if (mSearchAddressResult != null) {
            if (mSearchAddressResult.getLocations() != null && !mSearchAddressResult.getLocations().isEmpty()) {
                mUser.setLatitude(mSearchAddressResult.getLocations().getLatitude());
                mUser.setLongitude(mSearchAddressResult.getLocations().getLongtitude());
                if (!TextUtils.isEmpty(mSearchAddressResult.getLocations().getCountry())) {
                    mUser.setCountry(mSearchAddressResult.getLocations().getCountry());
                }
            }
            mUser.setAddress(mSearchAddressResult.getFullAddress());
        }

        Observable<ProfileUploader.UploadResult> observable = null;
        if (!TextUtils.isEmpty(mUser.getUserProfile()) && !mUser.getUserProfile().startsWith("http")) {
            observable = ProfileUploader.uploadProfileImage(getContext(), mUser.getUserProfile())
                    .concatMap(uploadResult -> {
                        if (uploadResult == null) {
                            uploadResult = new ProfileUploader.UploadResult();
                        }
                        mUser.setUserProfile(uploadResult.getUrl());
                        return Observable.just(uploadResult);
                    });
        }
        if (!TextUtils.isEmpty(mUser.getUserCoverProfile()) && !mUser.getUserCoverProfile().startsWith("http")) {
            if (observable == null) {
                observable = ProfileUploader.uploadCoverImage(getContext(), mUser.getUserCoverProfile())
                        .concatMap(uploadResult -> {
                            if (uploadResult == null) {
                                uploadResult = new ProfileUploader.UploadResult();
                            }

                            mUser.setUserCoverProfileThumbnail(uploadResult.getThumbnail());
                            mUser.setUserCoverProfile(uploadResult.getUrl());
                            return Observable.just(uploadResult);
                        });
            } else {
                observable = observable.concatMap(uploadResult -> ProfileUploader.uploadCoverImage(getContext(), mUser.getUserCoverProfile())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()));
            }
        }
        Observable<Response<User>> call;
        if (observable != null) {
            call = observable.concatMap(uploadResult -> mStoreDataManager.updateMyProfile(mUser)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()));
        } else {
            call = mStoreDataManager.updateMyProfile(mUser);
        }
        ResponseObserverHelper<Response<User>> helper = new ResponseObserverHelper<>(getContext(), call, true);
        Disposable disposable = helper.execute(mUpdateCallback);
        addDisposable(disposable);
    }


    public void onCountryClick() {
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).startActivityForResult(new SelectAddressIntent(getContext(), mSearchAddressResult),
                    new ResultCallback() {
                        @Override
                        public void onActivityResultSuccess(int resultCode, Intent data) {
                            SelectAddressIntentResult result = new SelectAddressIntentResult(data);
                            SearchAddressResult value = result.getResult();
                            mIsShowButtonSave.set(!TextUtils.equals(value.getFullAddress(), mSearchAddressResult.getFullAddress()));
                            mSearchAddressResult = value;
                            String city = mSearchAddressResult.getCountry();
                            if (TextUtils.isEmpty(city)) {
                                city = mSearchAddressResult.getFullAddress();
                            }
                            mAddress.set(city);
                            mIsShowButtonSave.set(true);
                        }
                    });
        }
    }

    @Override
    public void onRetryClick() {
        if (mErrorType == ERROR_GET_DATA) {
            getMyUserProfile();
        }
    }

    public void onChangeCoverClick() {
        checkCameraPermission(PhotoType.COVER);
    }

    public void onChangeProfileClick() {
        checkCameraPermission(PhotoType.PROFILE);
    }

    private void checkCameraPermission(PhotoType photoType) {
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).checkPermission(
                    new CheckPermissionCallbackHelper((AbsBaseActivity) getContext(),
                            CheckPermissionCallbackHelper.Type.CAMERA) {
                        @Override
                        public void onPermissionGranted() {
                            launchCamera(photoType);
                        }

                    }, Manifest.permission.CAMERA);
        }
    }

    private void launchCamera(PhotoType photoType) {
        Intent intent = MyCameraIntent.newProfileInstance(getContext());
        ((AbsBaseActivity) getContext()).startActivityForResult(intent, new ResultCallback() {
            @Override
            public void onActivityResultSuccess(int resultCode, Intent data) {
                MyCameraResultIntent resultIntent = new MyCameraResultIntent(data);
                for (Media productMedia : resultIntent.getMedia()) {
                    if (photoType == PhotoType.PROFILE) {
                        mUser.setUserProfile(productMedia.getUrl());
                        notifyPropertyChanged(BR.user);
                    } else {
                        mCover.set(productMedia.getUrl());
                        mUser.setUserCoverProfile(productMedia.getUrl());
                    }
                }
                mIsShowButtonSave.set(true);
            }
        });
    }

    private void getMyUserProfile() {
        showLoading();
        Observable<Response<User>> call = mStoreDataManager.getMyUserProfile();
        ResponseObserverHelper<Response<User>> helper = new ResponseObserverHelper<>(getContext(), call);
        addDisposable(helper.execute(new OnCallbackListener<Response<User>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                if (SharedPrefUtils.isLogin(getContext())) {
                    User user = SharedPrefUtils.getUser(getContext());
                    updateData(user);
                    return;
                }
                if (mIsLoadedData) {
                    return;
                }
                mIsLoadedData = false;
                mErrorType = ERROR_GET_DATA;
                if (ex.getCode() == 404) {
                    showError(getContext().getString(R.string.error_user_404));
                } else {
                    showError(ex.toString());
                }
            }

            @Override
            public void onComplete(Response<User> result) {
                updateData(result.body());
            }

            private void updateData(User user) {
                mIsLoadedData = true;
                setUser(user);
                hideLoading();
                if (mOnCallbackListener != null && user.getNotificationSettingModel() != null) {
                    mOnCallbackListener.onComplete(user.getNotificationSettingModel());
                }
            }
        }));
    }

    private void setUser(User user) {
        SharedPrefUtils.setUserValue(user, getContext());
        mUser = user;
        notifyPropertyChanged(BR.user);
        mCover.set(user.getUserCoverProfile());
        mStoreName.set(user.getStoreName());
        mFirstName.set(user.getFirstName());
        mLastName.set(user.getLastName());
        mEmail.set(user.getEmail());
        mPhone.set(user.getPhone());
        mAddress.set(user.getCountryName());
        mCurrency.set(user.getCurrency());
        if (TextUtils.isEmpty(mCurrency.get())) {
            mCurrency.set(CurrencyDb.getSelectedCurrency(getContext()).getName());
        }
    }

    public void onPhoneClick() {
        if (mSMSValidationUtil == null) {
            mSMSValidationUtil = new FacebookSMSValidation(getContext());
        }
        mSMSValidationUtil.doValidation(new FacebookSMSValidation.OnSMSValidationResult() {
            @Override
            public void onSuccess(final String phoneNumber, String token) {
                showLoading();
                Observable<Response<Object>> callback = mStoreDataManager.loginWithPhoneNumber(phoneNumber, token);
                ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(mStoreDataManager.getContext(), callback);
                addDisposable(helper.execute(new OnCallbackListener<Response<Object>>() {
                    @Override
                    public void onFail(ErrorThrowable ex) {
                        hideLoading();
                        if (ex.getCode() == 404) {
                            mPhone.set(phoneNumber);
                        } else {
                            showSnackBar(ex.toString());
                        }
                    }

                    @Override
                    public void onComplete(Response<Object> userResponse) {
                        hideLoading();
                        showSnackBar(R.string.error_change_phone_with_exist_number);
                    }
                }));
            }

            @Override
            public void onFail() {
                //Stub
            }

            @Override
            public void onCancel() {
                //Stub
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mSMSValidationUtil != null) {
            mSMSValidationUtil.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void onCurrencyClick() {
        if (!mIsEnablePhoneClick.get()) {
            return;
        }
        CurrencyDialog dialog = new CurrencyDialog();
        dialog.setCurrency(mCurrency.get());
        dialog.setOnCurrencyChooseListener(currency -> {
            if (!TextUtils.equals(currency, mCurrency.get())) {
                mCurrency.set(currency);
                mIsShowButtonSave.set(true);
            }
        });
        dialog.show(mFragmentManager);
    }

    public void onThemeClick() {
        ThemeDialog dialog = new ThemeDialog();
        dialog.setOnThemeChooseListener(appTheme -> {
            if (mTheme.get() != appTheme) {
                ThemeManager.setAppTheme(getContext(), appTheme);

                mTheme.set(appTheme);
                mIsShowButtonSave.set(true);

                if (getContext() instanceof EditProfileActivity) {
                    ((EditProfileActivity) getContext()).recreate();
                }
            }
        });
        dialog.show(mFragmentManager);
    }

    public void onChangeLanguageClick() {
        ChangeLanguageDialog dialog = new ChangeLanguageDialog();
        dialog.setLanguages(getLanguage());
        dialog.setPosition(mLanguageSelectedPosition);
        dialog.setOnLanguageChooseListener(position -> {
            mLanguageSelectedPosition = position;
            setLanguage();

            String[] iso3SupportLanguage = getContext().getResources().getStringArray(R.array.support_lang_iso3);
            String localString = iso3SupportLanguage[mLanguageSelectedPosition];

            if (!TextUtils.equals(localString, SharedPrefUtils.getLanguage(getContext())) && getContext() instanceof EditProfileActivity) {
                ((EditProfileActivity) getContext()).recreateFragment(localString);
            }
        });
        dialog.show(mFragmentManager);
    }

    private String[] getLanguage() {
        return getContext().getResources().getStringArray(R.array.change_language_support_language);
    }

    private void setLanguage() {
        mLanguage.set(getLanguage()[mLanguageSelectedPosition]);
    }

    public void pushNotificationSetting(boolean disableAllNotification) {
        NotificationSettingModel notificationSettingModel = SharedPrefUtils.getNotificationSetting(getContext());
        notificationSettingModel.setDisableAllNotification(disableAllNotification);

        Observable<Response<User>> call = mStoreDataManager.updateNotification(notificationSettingModel);
        ResponseObserverHelper<Response<User>> helper = new ResponseObserverHelper<>(getContext(), call);
        addDisposable(helper.execute(new OnCallbackListener<Response<User>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                showSnackBar(R.string.error_cannot_change_this_setting);
            }

            @Override
            public void onComplete(Response<User> result) {
                if (result.body() != null) {
                    SharedPrefUtils.setNotificationSetting(getContext(), result.body().getNotificationSettingModel());
                }
            }
        }));
    }

    public void changeMyStoreStatus(StoreStatus status, OnCallbackListener<Response<User>> listener) {
        Observable<Response<User>> call = mStoreDataManager.changeMyStoreStatus(status);
        ResponseObserverHelper<Response<User>> helper = new ResponseObserverHelper<>(getContext(), call);
        addDisposable(helper.execute(listener));
    }

    public void onStoreNameClick() {
        showInputDialog(R.string.edit_profile_store_name,
                R.string.edit_profile_enter_store_name_hint,
                mStoreName.get(), InputMessageDialog.Validation.Store, mStoreName::set);
    }

    public void onFirstNameClick() {
        showInputDialog(R.string.edit_profile_first_name,
                R.string.edit_profile_enter_first_name_hint,
                mFirstName.get(), InputMessageDialog.Validation.Required, mFirstName::set);
    }

    public void onLastNameClick() {
        showInputDialog(R.string.edit_profile_last_name,
                R.string.edit_profile_enter_last_name_hint,
                mLastName.get(),
                InputMessageDialog.Validation.Required,
                mLastName::set);
    }

    public void onEmailClick() {
        showInputDialog(R.string.edit_profile_email,
                R.string.edit_profile_enter_your_email_hint,
                mEmail.get(),
                InputMessageDialog.Validation.Email,
                mEmail::set);
    }

    public void onChangePasswordClick() {
        ChangePasswordDialog changePasswordDialog = new ChangePasswordDialog();
        changePasswordDialog.setListener(new ChangePasswordDialog.ChangePasswordDialogListener() {
            @Override
            public void onChangePasswordSuccess() {
                showSnackBar(R.string.password_change_success);
            }
        });
        changePasswordDialog.show(mFragmentManager);
    }

    public void onLogoutClick() {
        mOnCallbackListener.onLogoutClick();
    }

    private void showInputDialog(@StringRes int title,
                                 @StringRes int message,
                                 String hint,
                                 InputMessageDialog.Validation validation,
                                 OnCompleteListener<String> listener) {
        InputMessageDialog dialog = new InputMessageDialog();
        dialog.setTitle(getContext().getString(title));
        dialog.setMessage(getContext().getString(message));
        dialog.setLeftText(getContext().getString(R.string.search_address_button_confirm), view -> {
            mIsShowButtonSave.set(true);
            listener.onComplete(dialog.getCurrentInputText());
        });
        dialog.setRightText(getContext().getString(R.string.change_language_button_cancel), null);
        dialog.setInputLayout(hint, validation);
        dialog.show(mFragmentManager);
    }

    public enum PhotoType {
        PROFILE, COVER
    }


}
