package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;
import android.databinding.ObservableField;
import android.text.TextUtils;

import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.model.request.ChangeOrResetPasswordBody;
import com.sompom.tookitup.model.result.AuthResponse;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.ChangePasswordDataManager;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

import java.util.Objects;

import io.reactivex.Observable;
import retrofit2.Response;

public class ChangePasswordViewModel extends AbsLoadingViewModel {

    private ObservableField<String> mPassword = new ObservableField<>();
    private ObservableField<String> mPasswordError = new ObservableField<>();
    private ObservableField<String> mConfirmPassword = new ObservableField<>();
    private ObservableField<String> mConfirmPasswordError = new ObservableField<>();
    private ChangePasswordDataManager mDataManager;
    private ChangePasswordViewModelListener mListener;

    public ChangePasswordViewModel(Context context,
                                   ChangePasswordDataManager dataManager,
                                   ChangePasswordViewModelListener listener) {
        super(context);
        mDataManager = dataManager;
        mListener = listener;
    }

    public ObservableField<String> getConfirmPassword() {
        return mConfirmPassword;
    }

    public ObservableField<String> getPassword() {
        return mPassword;
    }

    public ObservableField<String> getPasswordError() {
        return mPasswordError;
    }

    public ObservableField<String> getConfirmPasswordError() {
        return mConfirmPasswordError;
    }

    private ChangeOrResetPasswordBody getBody() {
        return new ChangeOrResetPasswordBody(mPassword.get(), mPassword.get());
    }

    public boolean isReadyToChangePassword() {
        return isAllFieldValid() && isPasswordMatch();
    }

    private boolean isAllFieldValid() {
        boolean valid = true;

        if (TextUtils.isEmpty(mPassword.get())) {
            mPasswordError.set(mDataManager.getContext().getString(R.string.edit_profile_error_field_required));
            valid = false;
        } else {
            mPasswordError.set(null);
        }

        if (TextUtils.isEmpty(mConfirmPassword.get())) {
            mConfirmPasswordError.set(mDataManager.getContext().getString(R.string.edit_profile_error_field_required));
            valid = false;
        } else {
            mConfirmPasswordError.set(null);
        }

        return valid;
    }

    private boolean isPasswordMatch() {
        boolean match = true;

        if (!Objects.requireNonNull(mPassword.get()).matches(Objects.requireNonNull(mConfirmPassword.get()))) {
            match = false;
            mConfirmPasswordError.set(mDataManager.mContext.getString(R.string.confirm_password_not_match));
        } else {
            mConfirmPasswordError.set(null);
        }

        return match;
    }

    public void requestChangePassword() {
        showLoading();
        Observable<Response<AuthResponse>> loginService = mDataManager.getChangePassswordService(getBody());
        ResponseObserverHelper<Response<AuthResponse>> helper = new ResponseObserverHelper<>(mDataManager.mContext,
                loginService);
        addDisposable(helper.execute(new OnCallbackListener<Response<AuthResponse>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
//                hideLoading();
//                if (mListener != null) {
//                    mListener.onChangePasswordFailed(ex.getMessage());
//                }

                //TODO: Need to remove
                onChangePasswordSuccess(null);
            }

            @Override
            public void onComplete(Response<AuthResponse> result) {
                onChangePasswordSuccess(result.body());
            }
        }));
    }

    private void onChangePasswordSuccess(AuthResponse response) {
        hideLoading();
        if (mListener != null) {
            mListener.onChangePasswordSuccess();
        }
    }

    public interface ChangePasswordViewModelListener {

        void onChangePasswordFailed(String error);

        void onChangePasswordSuccess();
    }
}
