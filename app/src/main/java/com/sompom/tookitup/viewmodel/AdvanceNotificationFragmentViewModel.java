package com.sompom.tookitup.viewmodel;

import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.model.result.NotificationSettingModel;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.StoreDataManager;
import com.sompom.tookitup.utils.SharedPrefUtils;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 5/3/18.
 */

public class AdvanceNotificationFragmentViewModel extends AbsLoadingViewModel {
    public NotificationSettingModel mNotificationSettingModel;
    private StoreDataManager mDataManager;

    public AdvanceNotificationFragmentViewModel(StoreDataManager dataManager) {
        super(dataManager.getContext());
        mDataManager = dataManager;
        mNotificationSettingModel = SharedPrefUtils.getNotificationSetting(mDataManager.getContext());
        if (mNotificationSettingModel == null) {
            mNotificationSettingModel = new NotificationSettingModel();
        }
    }

    public void onButtonSaveClicked() {
        pushNotificationSetting();
    }

    private void pushNotificationSetting() {
        showLoading();
        Observable<Response<User>> call = mDataManager.updateNotification(mNotificationSettingModel);
        ResponseObserverHelper<Response<User>> helper =
                new ResponseObserverHelper<>(mDataManager.getContext(), call);

        addDisposable(helper.execute(new OnCallbackListener<Response<User>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                hideLoading();
                showSnackBar(R.string.error_cannot_change_this_setting);
            }

            @Override
            public void onComplete(Response<User> value) {
                if (value.body() != null) {
                    mNotificationSettingModel = value.body().getNotificationSettingModel();
                    SharedPrefUtils.setNotificationSetting(mDataManager.getContext(), mNotificationSettingModel);
                }

                hideLoading();
            }
        }));
    }
}
