package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.text.TextUtils;
import android.view.View;

import com.sompom.tookitup.intent.newintent.DetailHistoryProductConversationIntent;
import com.sompom.tookitup.listener.OnMessageItemClick;
import com.sompom.tookitup.model.emun.SegmentedControlItem;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.utils.DateTimeUtils;
import com.sompom.tookitup.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by nuonveyo on 6/26/18.
 */

public class ListItemAllMessageViewModel extends ListItemMessageRelatedProduct {
    public final ObservableBoolean mIsRead = new ObservableBoolean();
    //    public final User mUser; //Old code
    private final Conversation mChatMessage;
    private final Context mContext;
    private final OnMessageItemClick mOnMessageItemClick;
    public List<User> mParticipants;
    public String mName;

    public ListItemAllMessageViewModel(Context context,
                                       Conversation chatMessage,
                                       OnMessageItemClick messageItemClick,
                                       SegmentedControlItem item) {
        super(chatMessage, item);
        mContext = context;
        mChatMessage = chatMessage;
        //Old code
//        mUser = mChatMessage.getRecipient(context);
        mOnMessageItemClick = messageItemClick;
        mIsRead.set(mChatMessage.isRead(context));
        bindConversationName(chatMessage);
//
//        if (!TextUtils.isEmpty(chatMessage.getGroupId())) {
//            Timber.i("Group: " + chatMessage.getGroupName() +
//                    ", statusSeenConversation: " + new Gson().toJson(chatMessage.getStatusSeenConversation()));
//        }
    }

    private void bindConversationName(Conversation conversation) {
        if (conversation != null) {
            if (!isGroupConversation(conversation)) {
                mParticipants = Collections.singletonList(conversation.getRecipient(mContext));
                mName = mParticipants.get(0).getFullName();
            } else {
                mName = conversation.getGroupName();
                if (conversation.getParticipants() != null) {
                    mParticipants = new ArrayList<>(conversation.getParticipants());
                }
            }
        }
    }

    private boolean isGroupConversation(Conversation conversation) {
        return conversation != null && !TextUtils.isEmpty(conversation.getGroupId());
    }

    private List<User> getParticipants(User user, int pos) {
        List<User> users = new ArrayList<>();

        if (pos == 0) {
            users.add(user);
        } else if (pos == 1) {
            users.add(user);
            users.add(user);
        } else if (pos == 2) {
            users.add(user);
            users.add(user);
            users.add(user);
        } else if (pos == 3) {
            users.add(user);
            users.add(user);
            users.add(user);
            users.add(user);
        } else if (pos == 4) {
            users.add(user);
            users.add(user);
            users.add(user);
            users.add(user);
            users.add(user);
        } else if (pos == 5) {
            users.add(user);
            users.add(user);
            users.add(user);
            users.add(user);
            users.add(user);
            users.add(user);
        }

        return users;
    }

    public String getDescription() {
        if (TextUtils.isEmpty(mChatMessage.getContent(mContext))) {
            if (mChatMessage.getLastMessage() != null) {
                return mChatMessage.getLastMessage().getActualContent(mContext);
            } else {
                //TODO: Need to remove after server fix response of last message
                return "";
            }
        }
        return mChatMessage.getContent(mContext);
    }

    public String getFormatDate() {
        if (mChatMessage.getDate() != null) {
            return DateTimeUtils.parse(mContext, mChatMessage.getDate().getTime());
        } else {
            return "";
        }
    }


    public void onItemClick() {
        if (!mIsRead.get()) {
            mIsRead.set(true);
        }
        mOnMessageItemClick.onConversationClick(mChatMessage);
    }

    public View.OnLongClickListener onItemLongClick() {
        return view -> {
            mOnMessageItemClick.onConversationLongClick(mChatMessage);
            return false;
        };
    }

    public void onProductItemClick() {
        if (mContext instanceof AbsBaseActivity && TextUtils.equals(mChatMessage.getSeller(), SharedPrefUtils.getUserId(mContext))) {
            Product product = mChatMessage.getProduct();
            if (product == null) {
                product = mChatMessage.getLastMessage().getProduct();
            }
            mContext.startActivity(new DetailHistoryProductConversationIntent(mContext, product));
        }
    }
}
