package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;
import android.databinding.ObservableField;

import com.sompom.tookitup.R;
import com.sompom.tookitup.helper.NavigateSellerStoreHelper;
import com.sompom.tookitup.listener.OnCompleteListener;
import com.sompom.tookitup.model.result.ContentStat;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.utils.FormatNumber;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.AbsBaseViewModel;

/**
 * Created by nuonveyo on 6/8/18.
 */

public class ListItemProductHistoryConversation extends AbsBaseViewModel {
    public final ObservableField<User> mUser = new ObservableField<>();
    private final Context mContext;
    private final OnCompleteListener<Conversation> mOnChatItemClickListener;
    private final Conversation mConversation;

    public ListItemProductHistoryConversation(Context context,
                                              Conversation conversation,
                                              OnCompleteListener<Conversation> listener) {
        mContext = context;
        mOnChatItemClickListener = listener;
        mConversation = conversation;
        mUser.set(getUser());

    }

    public String getFollow() {
        StringBuilder follow = new StringBuilder();
        User user = mUser.get();
        if (user != null) {
            ContentStat contentStat = user.getContentStat();
            follow
                    .append(FormatNumber.format(contentStat.getTotalFollowers()))
                    .append(" ");
            if (contentStat.getTotalFollowers() > 1) {
                follow.append(mContext.getString(R.string.seller_store_followers_title));
            } else {
                follow.append(mContext.getString(R.string.suggestion_follower));
            }

            follow
                    .append(" ")
                    .append(" ")
                    .append(" ")
                    .append(" ")
                    .append(" ")
                    .append(FormatNumber.format(contentStat.getTotalFollowings()))
                    .append(" ")
                    .append(mContext.getString(R.string.seller_store_following_title));
        }
        return follow.toString();
    }

    public void onItemClick() {
        if (mOnChatItemClickListener != null) {
            mOnChatItemClickListener.onComplete(mConversation);
        }
    }

    public void onUserProfileClick() {
        new NavigateSellerStoreHelper(mContext, mUser.get()).openSellerStore();
    }

    private User getUser() {
        for (User user : mConversation.getParticipants()) {
            if (!SharedPrefUtils.checkIsMe(mContext, user.getId())) {
                return user;
            }
        }
        return new User();
    }
}
