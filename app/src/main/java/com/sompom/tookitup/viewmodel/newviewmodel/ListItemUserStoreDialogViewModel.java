package com.sompom.tookitup.viewmodel.newviewmodel;

import com.sompom.tookitup.listener.OnCompleteListener;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.viewmodel.AbsBaseViewModel;

/**
 * Created by nuonveyo on 10/25/18.
 */

public class ListItemUserStoreDialogViewModel extends AbsBaseViewModel {
    public Product mProduct;
    private OnCompleteListener<Product> mOnItemClickListener;

    public ListItemUserStoreDialogViewModel(Product product, OnCompleteListener<Product> listener) {
        mProduct = product;
        mOnItemClickListener = listener;
    }

    public void onItemClick() {
        mOnItemClickListener.onComplete(mProduct);
    }
}
