package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.ObservableField;

/**
 * Created by nuonveyo on 8/30/18.
 */

public class LayoutChatImageFullScreenViewModel {
    public ObservableField<String> mImageUrl = new ObservableField<>();

    public LayoutChatImageFullScreenViewModel(String imageUrl) {
        mImageUrl.set(imageUrl);
    }
}
