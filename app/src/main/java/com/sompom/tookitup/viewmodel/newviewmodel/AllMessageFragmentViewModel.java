package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.ObservableBoolean;

import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.NetworkBroadcastReceiver;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.model.ActiveUserWrapper;
import com.sompom.tookitup.model.emun.SegmentedControlItem;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.BaseObserverHelper;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.services.datamanager.AllMessageDataManager;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by nuonveyo on 6/26/18.
 */

public class AllMessageFragmentViewModel extends AbsLoadingViewModel {
    public final ObservableBoolean mIsRefresh = new ObservableBoolean();
    private final OnCallback mOnCallback;
    private final AllMessageDataManager mDataManager;
    private final SegmentedControlItem mItemType;
    private NetworkBroadcastReceiver.NetworkListener mNetworkStateListener;

    public AllMessageFragmentViewModel(AllMessageDataManager dataManager,
                                       OnCallback onCallback,
                                       SegmentedControlItem itemType) {
        super(dataManager.getContext());
        mDataManager = dataManager;
        mOnCallback = onCallback;
        mItemType = itemType;

        if (getContext() instanceof AbsBaseActivity) {
            mNetworkStateListener = networkState -> {
                if (networkState == NetworkBroadcastReceiver.NetworkState.Connected) {
                    getRecentlyMessage();
                } else if (networkState == NetworkBroadcastReceiver.NetworkState.Disconnected) {
                    if (mItemType == SegmentedControlItem.All) {
                        getRecentlyMessage();
                    } else {
                        showNetworkError();
                    }
                }
            };
            ((AbsBaseActivity) getContext()).addNetworkStateChangeListener(mNetworkStateListener);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).removeStateChangeListener(mNetworkStateListener);
        }
    }

    public OnClickListener onButtonRetryClick() {
        return this::getRecentlyMessage;
    }

    public void onRefresh() {
        mIsRefresh.set(true);
        mDataManager.resetNextPage();
        getRecentlyMessage();

    }

    private void getRecentlyMessage() {
        if (!mIsRefresh.get()) {
            showLoading();
        }

        if (mItemType == SegmentedControlItem.All) {
            Observable<List<Conversation>> ob = mDataManager.getAllMessageList();
            BaseObserverHelper<List<Conversation>> helper = new BaseObserverHelper<>(getContext(), ob);
            addDisposable(helper.execute(new Callback() {
                @Override
                public void onFail(ErrorThrowable ex) {
                    getAll(ex, false);
                }

                @Override
                public void onComplete(List<Conversation> result) {
                    super.onComplete(result);
                    if (!result.isEmpty()) {
                        getAll(null, true);
                    }
                }
            }));
        } else {
            Observable<List<Conversation>> ob = mDataManager.getAllMessageList();
            BaseObserverHelper<List<Conversation>> helper = new BaseObserverHelper<>(getContext(), ob);
            addDisposable(helper.execute(new Callback()));
        }
    }

    private void getAll(ErrorThrowable throwable, boolean isShowMoreTitle) {
        BaseObserverHelper<ActiveUserWrapper> helper = new BaseObserverHelper<>(getContext(), mDataManager.getSeller(isShowMoreTitle));
        addDisposable(helper.execute(new OnCallbackListener<ActiveUserWrapper>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mIsRefresh.set(false);
                if (mOnCallback.isHasData()) {
                    return;
                }
                if (throwable != null) {
                    ex = throwable;
                }
                if (ex.getCode() == 404) {
                    showError(getContext().getString(R.string.error_no_data));
                } else {
                    showError(ex.getMessage());
                }
            }

            @Override
            public void onComplete(ActiveUserWrapper result) {
                if (result.getYourSeller().isEmpty() && result.getActiveSeller().isEmpty()) {
                    onFail(throwable);
                    return;
                }

                hideLoading();
                mIsRefresh.set(false);

                if (mOnCallback != null) {
                    int activateSellerIndex = mDataManager.getUnreadMessageSize();
                    mOnCallback.onGetActivateSellerStoreSuccess(result, activateSellerIndex);
                }
            }
        }));
    }

    public void loadMore(OnCallbackListListener<List<Conversation>> listListener) {
        Observable<List<Conversation>> observable = mDataManager.loadMore();
        BaseObserverHelper<List<Conversation>> helper = new BaseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute(new OnCallbackListener<List<Conversation>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                listListener.onFail(ex);
            }

            @Override
            public void onComplete(List<Conversation> result) {
                listListener.onComplete(result, mDataManager.isCanLoadMore());
            }
        }));
    }

    public interface OnCallback {
        void getGetRecentMessageSuccess(List<Conversation> conversations, boolean isCanLoadMore);

        void onGetActivateSellerStoreSuccess(ActiveUserWrapper seller, int index);

        boolean isHasData();
    }

    private class Callback implements OnCallbackListener<List<Conversation>> {

        @Override
        public void onFail(ErrorThrowable ex) {
            mIsRefresh.set(false);
            if (mOnCallback.isHasData()) {
                return;
            }
            mIsError.set(true);
            if (ex.getCode() == 404) {
                showError(getContext().getString(R.string.error_no_data));
            } else {
                showError(ex.toString());
            }
        }

        @Override
        public void onComplete(List<Conversation> result) {
            if (result.isEmpty()) {
                onFail(new ErrorThrowable(404, ""));
                return;
            }
            mIsRefresh.set(false);
            hideLoading();
            mOnCallback.getGetRecentMessageSuccess(result, mDataManager.isCanLoadMore());

            if (!mDataManager.isFromDb() && getContext() instanceof AbsBaseActivity) {
                ((AbsBaseActivity) getContext()).removeStateChangeListener(mNetworkStateListener);
            }
        }
    }
}
