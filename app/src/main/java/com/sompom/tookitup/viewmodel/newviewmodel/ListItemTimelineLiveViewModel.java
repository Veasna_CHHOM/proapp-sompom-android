package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.ObservableField;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.ViewGroup;

import com.sompom.tookitup.databinding.ListItemTimelineSharedLiveVideoBinding;
import com.sompom.tookitup.listener.OnTimelineItemButtonClickListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.WallStreetAdaptive;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.services.datamanager.WallStreetDataManager;
import com.sompom.tookitup.utils.TimelinePostItemUtil;
import com.sompom.tookitup.viewholder.BindingViewHolder;
import com.sompom.tookitup.viewholder.TimelineViewHolder;
import com.sompom.tookitup.widget.lifestream.MediaLayout;

/**
 * Created by nuonveyo on 8/6/18.
 */

public class ListItemTimelineLiveViewModel extends ListItemMainTimelineViewModel {
    public final ObservableField<String> mCountViewer = new ObservableField<>();

    public ListItemTimelineLiveViewModel(AbsBaseActivity activity,
                                         BindingViewHolder bindingViewHolder,
                                         Adaptive adaptive,
                                         WallStreetDataManager dataManager,
                                         int position,
                                         OnTimelineItemButtonClickListener onItemClick) {
        super(activity, bindingViewHolder, dataManager, adaptive, position, onItemClick);

        //TODO: get viewer counter from server after done
        mCountViewer.set("100");
    }

    @Override
    public void initListItemTimelineHeader() {
        Adaptive adaptive = TimelinePostItemUtil.getAdaptive(mAdaptive);
        ListItemTimelineHeaderViewModel viewModel = new ListItemTimelineHeaderViewModel(mContext,
                adaptive,
                mPosition,
                mOnItemClickListener);
        viewModel.enableLiveHeader();

        Media media = ((WallStreetAdaptive) adaptive).getMedia().get(0);
        MediaLayout mediaLayout = ((ListItemTimelineSharedLiveVideoBinding) mBindingViewHolder.getBinding()).mediaLayout;
        DisplayMetrics metrics = new DisplayMetrics();
        ((AppCompatActivity) mediaLayout.getContext()).getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int screenwidth = metrics.widthPixels;
        ViewGroup.LayoutParams param = mediaLayout.getLayoutParams();
        int height;
        if (media.getWidth() == 0 || media.getHeight() == 0) {
            height = screenwidth;
        } else {
            height = media.getHeight() * screenwidth / media.getWidth();
        }
        param.width = screenwidth;
        param.height = height;

        mediaLayout.setLayoutParams(param);
        mediaLayout.setMedia(media);
        mediaLayout.setVisibleMuteButton(false);
        if (mBindingViewHolder instanceof TimelineViewHolder) {
            ((TimelineViewHolder) mBindingViewHolder).setMediaLayout(mediaLayout);
        }
        mTimelineHeaderViewModel.set(viewModel);
    }
}
