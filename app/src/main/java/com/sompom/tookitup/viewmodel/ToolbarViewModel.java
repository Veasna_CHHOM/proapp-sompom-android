package com.sompom.tookitup.viewmodel;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

/**
 * Created by nuonveyo on 2/7/18.
 */

public class ToolbarViewModel extends AbsLoadingViewModel {
    public ObservableField<String> mActionText = new ObservableField<>();
    public ObservableBoolean mIsShowAction = new ObservableBoolean(false);
    public ObservableBoolean mIsEnableToolbarButton = new ObservableBoolean(false);

    public ToolbarViewModel(Context context) {
        super(context);
    }

    public void onToolbarButtonClicked(Context context) {
    }
}
