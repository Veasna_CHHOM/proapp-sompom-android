package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.Nullable;
import android.view.View;

import com.sompom.tookitup.adapter.newadapter.ForwardAdapter;
import com.sompom.tookitup.chat.MessageState;
import com.sompom.tookitup.helper.AnimationHelper;
import com.sompom.tookitup.listener.ConversationDataAdaptive;
import com.sompom.tookitup.model.ForwardTitle;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.User;

/**
 * Created by He Rotha on 9/13/18.
 */
public class ListItemForwardViewModel {
    public final ObservableField<User> mUser = new ObservableField<>();
    public final ObservableBoolean mIsShowIndicator = new ObservableBoolean();
    public final ObservableBoolean mIsSent = new ObservableBoolean();
    public final ObservableBoolean mIsChangeBg = new ObservableBoolean();
    public final ObservableInt mTitle = new ObservableInt();
    private ForwardAdapter.OnForwardItemClickListener mListener;
    private ConversationDataAdaptive mAdaptive;

    public ListItemForwardViewModel(Context context,
                                    ConversationDataAdaptive adaptive,
                                    @Nullable MessageState state,
                                    ForwardAdapter.OnForwardItemClickListener listener) {
        mListener = listener;
        mAdaptive = adaptive;
        User user;
        if (adaptive instanceof User) {
            user = (User) adaptive;
        } else if (adaptive instanceof Conversation) {
            user = ((Conversation) adaptive).getRecipient(context);
        } else {
            user = new User();
        }
        mUser.set(user);

        if (state == MessageState.SENDING) {
            mIsSent.set(false);
            mIsShowIndicator.set(true);
            mIsChangeBg.set(true);
        } else if (state == MessageState.SENT) {
            mIsSent.set(true);
            mIsShowIndicator.set(false);
            mIsChangeBg.set(true);
        } else {
            mIsSent.set(false);
            mIsShowIndicator.set(false);
            mIsChangeBg.set(false);

        }
    }

    public ListItemForwardViewModel(ForwardTitle title) {
        mTitle.set(title.getTitle());
    }

    public void onSendClick(View view) {
        AnimationHelper.animateBounce(view);
        mListener.onSendClick(mAdaptive);
        mIsSent.set(false);
        mIsChangeBg.set(true);
        mIsShowIndicator.set(true);
    }
}
