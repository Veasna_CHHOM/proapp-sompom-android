package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.sompom.tookitup.R;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.viewmodel.AbsBaseViewModel;
import com.sompom.tookitup.viewmodel.binding.ImageViewBindingUtil;

import java.util.List;

/**
 * Created by Chhom Veasna on 7/10/2019.
 */
public class ItemChatSeenAvatarViewModel extends AbsBaseViewModel {

    private ObservableField<List<User>> mSeenAvatars = new ObservableField<>();

    public ItemChatSeenAvatarViewModel() {
    }

    public void setSeenAvatars(List<User> seenAvatars) {
        mSeenAvatars.set(seenAvatars);
    }

    public ObservableField<List<User>> getSeenAvatars() {
        return mSeenAvatars;
    }

    @BindingAdapter("seenAvatar")
    public static void setSeenAvatar(LinearLayout linearLayout, List<User> users) {
        if (linearLayout.getChildCount() > 0) {
            linearLayout.removeAllViews();
        }

        if (users != null && !users.isEmpty()) {
            int seenAvatarSize = getSeenAvatarSize(linearLayout.getContext());
            for (int i = 0; i < users.size(); i++) {
                if (!users.get(i).isSeenMessage()) {
                    continue;
                }

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(seenAvatarSize, seenAvatarSize);
                if (i > 0) {
                    params.leftMargin = linearLayout.getResources().getDimensionPixelSize(R.dimen.space_tiny);
                }

                ImageView imageView = new ImageView(linearLayout.getContext());
                linearLayout.addView(imageView, params);
                ImageViewBindingUtil.setUserUrl(imageView, users.get(i));
            }
        }
    }

    private static int getSeenAvatarSize(Context context) {
        int originSize = context.getResources().getDimensionPixelSize(R.dimen.chat_height);
        return (int) (originSize * 0.7);
    }
}
