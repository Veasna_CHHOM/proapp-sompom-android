package com.sompom.tookitup.viewmodel.binding;

import android.databinding.BindingAdapter;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.text.Spannable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdView;
import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.model.emun.ChatBg;
import com.sompom.tookitup.utils.SnackBarUtil;

import timber.log.Timber;

/**
 * Created by He Rotha on 8/28/17.
 */

public final class BindingUtils {
    private BindingUtils() {
    }

    @BindingAdapter("setVisibility")
    public static void setVisibility(View view, boolean isVisible) {
        if (isVisible) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.GONE);
        }
    }

    @BindingAdapter("android:visibility")
    public static void visibility(View view, boolean isVisible) {
        if (isVisible) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.GONE);
        }
    }

    @BindingAdapter("paddingRight")
    public static void paddingRight(View textView, int dimen) {
        textView.setPadding(0, 0, dimen, 0);
    }

    @BindingAdapter("changeLayoutWeight")
    public static void changeLayoutWeight(View view, boolean isChanged) {
        if (isChanged) {
            ((LinearLayout.LayoutParams) view.getLayoutParams()).weight = 95;
        } else {
            ((LinearLayout.LayoutParams) view.getLayoutParams()).weight
                    = view.getContext().getResources().getInteger(R.integer.layout_weight_of_store);
        }
    }

    @BindingAdapter("addAdmob")
    public static void addAdMob(ViewGroup viewGroup, AdView adView) {
        if (adView == null) {
            viewGroup.setVisibility(View.GONE);
            return;
        }
        viewGroup.removeAllViews();
        if (adView.getParent() != null) {
            ((ViewGroup) adView.getParent()).removeView(adView);
        }
        viewGroup.addView(adView);
    }

    @BindingAdapter("snackBarErrorMessage")
    public static void snackBarErrorMessage(final View view, String message) {
        Timber.e("snackBarErrorMessage: %s", message);
        if (TextUtils.isEmpty(message)) {
            return;
        }
        SnackBarUtil.showSnackBar(view, message);
    }

    @BindingAdapter("layout_marginTop")
    public static void addMarginTop(View view, int dimen) {
        ViewGroup.MarginLayoutParams param = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        param.setMargins(0, dimen, 0, param.bottomMargin);
    }

    @BindingAdapter("layout_marginBottom")
    public static void addMarginBottom(View view, int dimen) {
        ViewGroup.MarginLayoutParams param = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        param.setMargins(param.leftMargin, param.topMargin, param.rightMargin, dimen);
    }

    @BindingAdapter("isAddCommentMarginStart")
    public static void addMarginStart(View view, boolean isAddMargin) {
        if (isAddMargin) {
            ViewGroup.MarginLayoutParams param = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            int marginStart = view.getContext().getResources().getDimensionPixelSize(R.dimen.reply_comment_margin_left);
            param.setMargins(marginStart, param.topMargin, param.rightMargin, param.bottomMargin);
        }
    }

    @BindingAdapter("addProductItemMarginBottom")
    public static void addProductItemMarginBottom(View view, boolean isAddMarginBottom) {
        ViewGroup.MarginLayoutParams param = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        int sizeTiny = view.getContext().getResources().getDimensionPixelSize(R.dimen.space_tiny);
        if (isAddMarginBottom) {
            int sizeMedium = view.getContext().getResources().getDimensionPixelSize(R.dimen.space_medium);
            param.setMargins(sizeTiny, sizeTiny, sizeTiny, sizeMedium);
        } else {
            param.setMargins(sizeTiny, sizeTiny, sizeTiny, sizeTiny);
        }
    }

    @BindingAdapter("invisibleView")
    public static void invisibleView(View view, boolean isInvisible) {
        if (isInvisible) {
            view.setVisibility(View.INVISIBLE);
        } else {
            view.setVisibility(View.VISIBLE);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @BindingAdapter("setElevation")
    public static void setElevation(View view, int value) {
        view.setElevation(value);
    }

    @BindingAdapter("animationTranslationTrigger")
    public static void animation(View view, boolean isAnimate) {
        if (isAnimate) {
            Animation rotation = AnimationUtils.loadAnimation(view.getContext(), R.anim.slide_trigger);
            rotation.setFillAfter(true);
            view.startAnimation(rotation);
        } else {
            view.clearAnimation();
        }
    }

    @BindingAdapter("chatGroupSeparator")
    public static void chatGroupSeparator(View view, ChatBg chatBg) {
        if (chatBg == null) {
            return;
        }
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        if (chatBg == ChatBg.ChatMeTop || chatBg == ChatBg.ChatYouTop || chatBg == ChatBg.ChatMeSingle || chatBg == ChatBg.ChatYouSingle) {
            params.topMargin = view.getContext().getResources().getDimensionPixelSize(R.dimen.space_medium);
        } else {
            params.topMargin = 0;
        }
    }

    @BindingAdapter("addMargin")
    public static void addMargin(View view, int dimen) {
        ViewGroup.MarginLayoutParams param = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        param.setMargins(dimen, dimen, dimen, dimen);
    }

    @BindingAdapter("setLongClickListener")
    public static void setLongClickListener(View view, OnClickListener onClickListener) {
        view.setOnLongClickListener(v -> {
            onClickListener.onClick();
            return true;
        });
    }

    @BindingAdapter("editTextError")
    public static void setEditTextError(EditText editText, String error) {
        editText.setError(error);
    }

    @BindingAdapter("relativeViewHeight")
    public static void setRelativeViewHeight(View adaptiveView, ViewGroup ruleView) {
        ruleView.post(() -> {
            Timber.i("ruleView.getHeight(): " + ruleView.getHeight());
            adaptiveView.getLayoutParams().height = ruleView.getHeight();
        });
    }

    @BindingAdapter({"fullText", "clickableText", "clickableTextColor", "clickableSpan"})
    public static void setSpannableClick(TextView textView,
                                         String fullText,
                                         String clickableText,
                                         int clickableTextColor,
                                         ClickableSpan clickableSpan) {

        Spannable span = Spannable.Factory.getInstance().newSpannable(fullText);
        int startIndex = fullText.indexOf(clickableText);

        span.setSpan(clickableSpan,
                startIndex,
                startIndex + clickableText.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        span.setSpan(new ForegroundColorSpan(clickableTextColor),
                startIndex,
                startIndex + clickableText.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        span.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),
                startIndex,
                startIndex + clickableText.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(span);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
