package com.sompom.tookitup.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;

import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.OnChatItemListener;
import com.sompom.tookitup.model.SelectedChat;
import com.sompom.tookitup.model.emun.ChatBg;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.utils.ChatUtility;

/**
 * Created by He Rotha on 9/14/17.
 */

public class ChatViewModel extends ChatMediaViewModel {
    public final ObservableField<String> mContent = new ObservableField<>();
    public final ObservableBoolean mIsShowShareIcon = new ObservableBoolean();
    private final ChatBg mDrawable;

    public ChatViewModel(Activity context,
                         Conversation conversation,
                         View rootView,
                         int position,
                         Chat chat,
                         SelectedChat selectedChat,
                         ChatUtility.GroupMessage drawable,
                         OnChatItemListener onItemClickListener) {
        super(context, conversation, chat, drawable, position, selectedChat, onItemClickListener);
        mDrawable = drawable.getChatBg();
        if (chat.isExpandHeight()) {
            selectedChat.setSelect(rootView, chat, drawable);
        }

        String content = mChat.getContent();
        mContent.set(content);
        if (!TextUtils.isEmpty(content)) {
            mIsShowShareIcon.set((Patterns.WEB_URL.matcher(content).find()
                    || Patterns.EMAIL_ADDRESS.matcher(content).find()) && mIsShowSharingIcon.get());
        }
    }

    public Drawable getBackground(View view, Context context) {
        view.setTag(R.id.tagContainer, mDrawable);
        return ContextCompat.getDrawable(context, mDrawable.getDrawable(mChat.isExpandHeight()));
    }

}
