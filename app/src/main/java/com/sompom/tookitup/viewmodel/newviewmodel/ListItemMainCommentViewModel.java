package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.sompom.tookitup.R;
import com.sompom.tookitup.listener.OnCommentItemClickListener;
import com.sompom.tookitup.model.result.Comment;
import com.sompom.tookitup.model.result.User;

/**
 * Created by nuonveyo on 7/20/18.
 */

public class ListItemMainCommentViewModel extends CommentViewModel {
    public final ObservableBoolean mIsShowReplayLayout = new ObservableBoolean();
    public final ObservableField<String> mReplyText = new ObservableField<>();
    public final ObservableField<User> mUserSubComment = new ObservableField<>();

    public ListItemMainCommentViewModel(Context context,
                                        int position,
                                        Comment comment,
                                        OnCommentItemClickListener onCommentItemClickListener) {
        super(context, position, comment, onCommentItemClickListener);
        if (comment.getReplyComment() != null && !comment.getReplyComment().isEmpty()) {
            int lastCommentIndex = comment.getReplyComment().size() - 1;
            mUserSubComment.set(comment.getReplyComment().get(lastCommentIndex).getUser());
            mIsShowReplayLayout.set(true);
            String content;
            int replyCommentCount = comment.getTotalSubComment();
            if (replyCommentCount == 1) {
                content = replyCommentCount + " " + context.getString(R.string.comment_reply_title);
            } else {
                content = replyCommentCount + " " + context.getString(R.string.comment_replies_title);
            }
            mReplyText.set(content);
        }
    }
}
