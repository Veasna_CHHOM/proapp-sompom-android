package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.text.TextUtils;
import android.text.TextWatcher;

import com.sompom.tookitup.R;
import com.sompom.tookitup.helper.DelayWatcher;
import com.sompom.tookitup.newui.SearchMessageActivity;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

/**
 * Created by nuonveyo on 7/2/18.
 */

public class SearchGeneralContainerFragmentViewModel extends AbsLoadingViewModel {
    public final ObservableBoolean mIsShowClearIcon = new ObservableBoolean();
    public final ObservableField<String> mSearchText = new ObservableField<>();
    public final ObservableInt mElevation = new ObservableInt();
    private final Callback mCallback;

    public SearchGeneralContainerFragmentViewModel(Context context, Callback callback) {
        super(context);
        mCallback = callback;
        mElevation.set(getContext().getResources().getDimensionPixelSize(R.dimen.space_small));
    }

    public TextWatcher onSearchTextChange() {
        return new DelayWatcher(true) {
            @Override
            public void onTextChanged(String text) {
                mCallback.onSearch(text);
            }

            @Override
            public void onTyping(String text) {
                if (!TextUtils.isEmpty(text)) {
                    if (!mIsShowClearIcon.get()) {
                        mIsShowClearIcon.set(true);
                        mCallback.onReplaceFragment();
                        mElevation.set(getContext().getResources().getDimensionPixelSize(R.dimen.space_tiny));
                    } else {
                        mCallback.showLoading();
                    }

                } else {
                    if (mIsShowClearIcon.get()) {
                        mIsShowClearIcon.set(false);
                        mCallback.onRemoveFragment();
                        mElevation.set(getContext().getResources().getDimensionPixelSize(R.dimen.space_small));
                    }
                }
            }
        };
    }

    public void onClearText() {
        if (!TextUtils.isEmpty(mSearchText.get())) {
            mSearchText.set("");
        }
    }

    public void onBackPress() {
        if (getContext() instanceof SearchMessageActivity) {
            ((SearchMessageActivity) getContext()).onBackPressed();
        }
    }

    public interface Callback {
        void onReplaceFragment();

        void onRemoveFragment();

        void onSearch(String text);

        void showLoading();
    }
}
