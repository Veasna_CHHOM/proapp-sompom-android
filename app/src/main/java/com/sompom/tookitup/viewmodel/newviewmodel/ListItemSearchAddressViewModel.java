package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;
import android.text.TextUtils;

import com.sompom.tookitup.listener.OnItemClickListener;
import com.sompom.tookitup.model.SearchAddressResult;
import com.sompom.tookitup.viewmodel.AbsBaseViewModel;

/**
 * Created by nuonveyo on 6/11/18.
 */

public class ListItemSearchAddressViewModel extends AbsBaseViewModel {
    private SearchAddressResult mSearchAddressResult;
    private OnItemClickListener<SearchAddressResult> mListener;

    public ListItemSearchAddressViewModel(SearchAddressResult searchAddressResult,
                                          OnItemClickListener<SearchAddressResult> listener) {
        mSearchAddressResult = searchAddressResult;
        mListener = listener;
    }

    public String getStreet() {
        return mSearchAddressResult.getFullAddress();
    }


    public String getCity() {
        boolean isCityEmpty = TextUtils.isEmpty(mSearchAddressResult.getCity());
        boolean isCountry = TextUtils.isEmpty(mSearchAddressResult.getCountry());
        if (!isCityEmpty && !isCountry) {
            return mSearchAddressResult.getCity() + ", " + mSearchAddressResult.getCountry();
        }
        return null;
    }

    public void onItemClick(Context context) {
        if (mListener != null) {
            mListener.onClick(mSearchAddressResult);
        }
    }
}
