package com.sompom.tookitup.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.support.annotation.StringRes;
import android.support.v4.app.FragmentActivity;

import com.sompom.tookitup.newui.dialog.AbsBaseFragmentDialog;
import com.sompom.tookitup.newui.dialog.ConfirmationDialog;
import com.sompom.tookitup.newui.fragment.AbsBaseFragment;
import com.sompom.tookitup.viewmodel.newviewmodel.ConfirmationDialogViewModel;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by He Rotha on 10/24/17.
 */

public abstract class AbsBaseViewModel extends BaseObservable {
    private CompositeDisposable mCompositeDisposable;
    private AbsBaseFragment mAbsBaseFragment;
    private AbsBaseFragmentDialog mAbsBaseFragmentDialog;

    public void setAbsBaseFragment(AbsBaseFragment absBaseFragment) {
        mAbsBaseFragment = absBaseFragment;
    }

    public void setAbsBaseFragmentDialog(AbsBaseFragmentDialog absBaseFragmentDialog) {
        mAbsBaseFragmentDialog = absBaseFragmentDialog;
    }


    public void onCreate() {

    }

    public void onDestroy() {
        clearDisposable();
    }

    public void onResume() {

    }

    public void onPause() {

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    public void clearDisposable() {
        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
        }
    }

    public void addDisposable(Disposable disposable) {
        if (mCompositeDisposable == null) {
            mCompositeDisposable = new CompositeDisposable();
        }
        mCompositeDisposable.add(disposable);
    }


    public void showDialogMessage(Context context, String title, String description, String button) {
        if (context instanceof FragmentActivity) {
            ConfirmationDialogViewModel.DialogModel model = new ConfirmationDialogViewModel.DialogModel();
            model.setTitle(title);
            model.setDescription(description);
            model.setPositionButtonText(button);
            ConfirmationDialog dialog = new ConfirmationDialog.Builder().build(model);
            dialog.show(((FragmentActivity) context).getSupportFragmentManager(), ConfirmationDialog.TAG);
        }
    }

    public void showSnackBar(String message) {
        if (mAbsBaseFragment != null) {
            mAbsBaseFragment.showSnackBar(message);
        } else {
            if (mAbsBaseFragmentDialog != null) {
                mAbsBaseFragmentDialog.showSnackBar(message);
            }
        }
    }

    public void showSnackBar(@StringRes int message) {
        if (mAbsBaseFragment != null) {
            mAbsBaseFragment.showSnackBar(message);
        } else {
            if (mAbsBaseFragmentDialog != null) {
                mAbsBaseFragmentDialog.showSnackBar(message);
            }
        }
    }

}
