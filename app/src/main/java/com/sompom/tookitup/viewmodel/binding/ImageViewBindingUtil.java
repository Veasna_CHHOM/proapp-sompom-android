package com.sompom.tookitup.viewmodel.binding;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.widget.ImageView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestOptions;
import com.sompom.tookitup.R;
import com.sompom.tookitup.helper.GlideApp;
import com.sompom.tookitup.helper.GlideRequest;
import com.sompom.tookitup.helper.GlideRequests;
import com.sompom.tookitup.model.AdsItem;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.emun.ErrorLoadingType;
import com.sompom.tookitup.model.notification.UserTemp;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;

import java.io.File;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

/**
 * Created by He Rotha on 3/20/18.
 */

public final class ImageViewBindingUtil {
    private ImageViewBindingUtil() {
    }

    public static void loadImageUrl(ImageView imageView,
                                    String imageUrl,
                                    int resizeWidth,
                                    int resizeHeight,
                                    Drawable drawable,
                                    boolean isRound) {
        loadImageUrl(imageView, imageUrl, resizeWidth, resizeHeight, drawable, isRound, 0);
    }


    @BindingAdapter(value =
            {"loadImageUrl", "resizeWidth", "resizeHeight", "drawable", "isRound", "cornerRadius"},
            requireAll = false)
    public static void loadImageUrl(ImageView imageView,
                                    String imageUrl,
                                    int resizeWidth,
                                    int resizeHeight,
                                    Drawable drawable,
                                    boolean isRound,
                                    float cornerRadius) {
        if (drawable == null) {
            drawable = ContextCompat.getDrawable(imageView.getContext(), R.drawable.ic_photo_placeholder_400dp);
        }
        if (TextUtils.isEmpty(imageUrl)) {
            imageView.setImageDrawable(drawable);
            return;
        }
        GlideRequests glide = GlideApp.with(imageView);
        GlideRequest<?> glideRequest;

        if (imageUrl.startsWith("http")) {
            glideRequest = glide.load(imageUrl);
        } else {
            glideRequest = glide.load(new File(imageUrl));
        }

        if (resizeHeight != 0 || resizeWidth != 0) {
            if (isRound) {
                glideRequest = glideRequest.apply(new RequestOptions()
                        .centerCrop()
                        .dontAnimate()
                        .circleCrop()
                        .override(resizeWidth, resizeHeight)
                        .placeholder(drawable));
            } else {
                if (cornerRadius != 0) {
                    MultiTransformation<android.graphics.Bitmap> multi = new MultiTransformation<>(
                            new CenterCrop(),
                            new RoundedCornersTransformation((int) cornerRadius, 0));
                    glideRequest = glideRequest.apply(RequestOptions.bitmapTransform(multi));
                } else {
                    glideRequest = glideRequest
                            .override(resizeWidth, resizeHeight);
                }
            }
        }
        glideRequest.placeholder(drawable).into(imageView);
    }

    @BindingAdapter("setUserImage")
    public static void setUserUrl(ImageView imageView, User user) {
        String name;
        String url;
        if (user == null) {
            name = null;
            url = null;
        } else {
            name = user.getLastName();
            url = user.getUserProfile();

            if (TextUtils.isEmpty(url)) {
                url = user.getUserProfile();
            }
        }

        TextDrawable drawable = getTextDrawable(imageView.getContext(), name);
        loadImageUrl(imageView, url, 150, 150, drawable, true);
    }

    @BindingAdapter("setUserImage")
    public static void setUserUrl(ImageView imageView, UserTemp user) {
        String name;
        String url;
        if (user == null) {
            name = null;
            url = null;
        } else {
            name = user.getLastName();
            url = user.getUserProfile();

            if (TextUtils.isEmpty(url)) {
                url = user.getUserProfile();
            }
        }

        TextDrawable drawable = getTextDrawable(imageView.getContext(), name);
        loadImageUrl(imageView, url, 150, 150, drawable, true);
    }

    public static TextDrawable getTextDrawable(Context context, String name) {
        if (TextUtils.isEmpty(name)) {
            name = context.getResources().getString(R.string.app_name);
        }
        char letter = name.charAt(0);
        ColorGenerator generator = ColorGenerator.MATERIAL;
        int color = generator.getColor(name);
        return TextDrawable.builder()
                .buildRound(String.valueOf(letter).toUpperCase(), color);
    }

    @BindingAdapter("setProductImage")
    public static void setProductImage(ImageView imageView, Product product) {
        //TODO: need to get thumbnail from ProductMedia Object after server done
        if (product == null) {
            return;
        }
        String productPhotoUrl = null;
        if (product.getMedia() != null && !product.getMedia().isEmpty()) {
            productPhotoUrl = product.getMedia().get(0).getUrl();
        }
        if (product.getMedia() != null && !product.getMedia().isEmpty()) {
            Drawable drawable = ContextCompat.getDrawable(imageView.getContext(), R.drawable.ic_photo_placeholder_200dp);
            loadImageUrl(imageView, productPhotoUrl, 160, 160, drawable, false);
        } else {
            imageView.setImageResource(R.drawable.ic_photo_placeholder_200dp);
        }
    }

    @BindingAdapter("setAdsImage")
    public static void setAdsImage(ImageView imageView, AdsItem product) {
        boolean isPhone = imageView.getResources().getBoolean(R.bool.is_phone);
        String productPhotoUrl;
        if (isPhone) {
            productPhotoUrl = product.getCreativeMobile();
        } else {
            productPhotoUrl = product.getCreativeTablet();
        }
        GlideApp.with(imageView)
                .load(productPhotoUrl)
                .placeholder(R.drawable.ic_photo_placeholder_200dp)
                .into(imageView);
    }

    @BindingAdapter("setMoreGameImageView")
    public static void setMoreGameImageView(ImageView imageView, String url) {
        GlideApp.with(imageView)
                .load(url)
                .error(R.drawable.image_place_holder)
                .override(0, 300)
                .placeholder(R.drawable.image_place_holder)
                .into(imageView);
    }

    @BindingAdapter("setErrorImageLoading")
    public static void setErrorImageLoading(ImageView imageView, ErrorLoadingType type) {
        if (type == null) {
            return;
        }
        imageView.setImageResource(type.getIcon());
    }

    @BindingAdapter(value = {"loadImageUrlWithCorner", "resizeWidth", "resizeHeight", "radius"})
    public static void loadImageUrlWithCorner(ImageView imageView, String imageUrl, int resizeWidth, int resizeHeight, int radius) {
        Drawable drawable = ContextCompat.getDrawable(imageView.getContext(), R.drawable.ic_photo_placeholder_400dp);
        MultiTransformation<Bitmap> multi = new MultiTransformation<>(
                new CenterCrop(),
                new RoundedCornersTransformation(radius, 0));
        GlideApp.with(imageView)
                .load(imageUrl)
                .apply(RequestOptions.bitmapTransform(multi))
                .placeholder(drawable)
                .override(resizeWidth, resizeHeight)
                .into(imageView);
    }


    @BindingAdapter(value = {"setGifImage", "setIsComment"}, requireAll = false)
    public static void setGifImage(ImageView imageView, Media gifMedia, boolean isComment) {
        Drawable drawable = ContextCompat.getDrawable(imageView.getContext(), R.drawable.ic_photo_placeholder_400dp);
        int rounded = imageView.getContext().getResources().getDimensionPixelSize(R.dimen.space_small);
        GlideApp
                .with(imageView.getContext())
                .asGif()
                .load(gifMedia.getUrl())
                .transform(new RoundedCornersTransformation(rounded, 0))
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .placeholder(drawable)
                .into(imageView);

        int imageWidth;
        if (isComment) {
            imageWidth = imageView.getContext().getResources().getDimensionPixelOffset(R.dimen.comment_media_width);
        } else {
            imageWidth = imageView.getContext().getResources().getDimensionPixelOffset(R.dimen.chat_media_width);
        }

        imageView.getLayoutParams().width = imageWidth;
        imageView.getLayoutParams().height = imageWidth * gifMedia.getHeight() / gifMedia.getWidth();
    }

    @BindingAdapter("setLogoLinkPreview")
    public static void setLogoLinkPreview(ImageView imageView, String url) {
        Drawable drawable = ContextCompat.getDrawable(imageView.getContext(), R.drawable.ic_photo_placeholder_400dp);
        GlideApp
                .with(imageView.getContext())
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .override(0, 300)
                .placeholder(drawable)
                .into(imageView);
    }
}
