package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.sompom.tookitup.model.ActiveUser;

/**
 * Created by He Rotha on 7/6/18.
 */
public class ListItemChatSellerViewModel {
    public final ObservableField<String> mTitle = new ObservableField<>();
    public final ObservableBoolean mVisibleLoadMore = new ObservableBoolean();

    public ListItemChatSellerViewModel(ActiveUser seller) {
        mTitle.set(seller.getTitle());
        mVisibleLoadMore.set(seller.isShowMore());
    }
}
