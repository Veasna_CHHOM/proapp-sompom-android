package com.sompom.tookitup.viewmodel;


import android.content.Context;

import com.sompom.tookitup.R;

/**
 * Created by nuonveyo on 5/7/18.
 */

public class EditMediaFileViewModel extends ToolbarViewModel {
    private OnCallback mOnCallback;

    public EditMediaFileViewModel(Context context, OnCallback onCallback) {
        super(context);
        mOnCallback = onCallback;
        mIsShowAction.set(true);
        mIsEnableToolbarButton.set(true);
        mActionText.set(context.getString(R.string.chat_done_button));
    }

    @Override
    public void onToolbarButtonClicked(Context context) {
        if (mOnCallback != null) {
            mOnCallback.onDone();
        }
    }

    public interface OnCallback {
        void onDone();
    }
}
