package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.text.TextUtils;
import android.text.TextWatcher;

import com.sompom.tookitup.chat.AbsChatBinder;
import com.sompom.tookitup.chat.MessageState;
import com.sompom.tookitup.chat.listener.GlobalChatListener;
import com.sompom.tookitup.helper.DelayWatcher;
import com.sompom.tookitup.listener.ConversationDataAdaptive;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.model.ForwardResult;
import com.sompom.tookitup.model.Media;
import com.sompom.tookitup.model.emun.MediaType;
import com.sompom.tookitup.model.result.Chat;
import com.sompom.tookitup.model.result.Conversation;
import com.sompom.tookitup.model.result.LinkPreviewModel;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.ForwardDataManager;
import com.sompom.tookitup.utils.ConversationUtil;
import com.sompom.tookitup.utils.SharedPrefUtils;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by He Rotha on 9/13/18.
 */
public class ForwardViewModel extends AbsLoadingViewModel implements GlobalChatListener {
    public final ObservableField<String> mForwardChat = new ObservableField<>();
    public final ObservableField<String> mImageCount = new ObservableField<>();
    public final ObservableField<String> mImageUrl = new ObservableField<>();
    public final ObservableBoolean mIsFromOutsideApp = new ObservableBoolean();
    public final ObservableBoolean mIsVisibleCounter = new ObservableBoolean(false);
    public final ObservableField<String> mLinkTitle = new ObservableField<>();
    public final ObservableField<String> mLink = new ObservableField<>();
    public final ObservableBoolean mIsDownloadingLinkPreview = new ObservableBoolean();
    public final ObservableBoolean mIsAudioMedia = new ObservableBoolean();

    private final List<Media> mForwardProductMedia;
    private final ForwardDataManager mDataManager;
    private final OnViewModelCallback mCallback;
    private final List<ConversationDataAdaptive> mCachePeople = new ArrayList<>();
    private final List<ConversationDataAdaptive> mCacheSuggestion = new ArrayList<>();
    private AbsChatBinder mAbsChatBinder;
    private String mUrl;

    public ForwardViewModel(AbsBaseActivity context,
                            String forwardChat,
                            List<Media> forwardProductMedia,
                            boolean isFromOutSideApp,
                            ForwardDataManager manager,
                            OnViewModelCallback callback) {
        super(context);
        setShowKeyboardWhileLoadingScreen();
        mIsFromOutsideApp.set(isFromOutSideApp);
        mForwardChat.set(forwardChat);
        mForwardProductMedia = forwardProductMedia;

        if (mForwardProductMedia != null && !mForwardProductMedia.isEmpty()) {
            MediaType mediaType = mForwardProductMedia.get(0).getType();
            if (mediaType == MediaType.IMAGE) {
                mImageUrl.set(mForwardProductMedia.get(0).getUrl());
                mImageCount.set(String.valueOf(mForwardProductMedia.size()));
                mIsVisibleCounter.set(mForwardProductMedia.size() > 1);

            } else if (mediaType == MediaType.AUDIO) {
                mIsAudioMedia.set(true);

            } else if (mediaType == MediaType.TENOR_GIF) {
                mImageUrl.set(mForwardProductMedia.get(0).getUrl());
            }
        }
        mDataManager = manager;
        mCallback = callback;
        onRetryClick();
        context.addOnServiceListener(binder -> {
            mAbsChatBinder = binder;
            mAbsChatBinder.addGlobalChatListener(this);
        });
    }

    public void setDisplayLink(String url, LinkPreviewModel linkPreviewModel) {
        mIsFromOutsideApp.set(true);
        mIsDownloadingLinkPreview.set(false);
        mForwardChat.set("");
        mUrl = url;
        mImageUrl.set(linkPreviewModel.getLogo());
        mLinkTitle.set(linkPreviewModel.getTitle());
        mLink.set(linkPreviewModel.getLink());
    }


    public TextWatcher getTextWatcher() {
        return new DelayWatcher(true) {
            @Override
            public void onTextChanged(String text) {
                if (TextUtils.isEmpty(text)) {
                    mCallback.onPeopleSearch(null);
                    mCallback.onSuggestPeopleComplete(mCacheSuggestion);
                    mCallback.onPeopleComplete(mCachePeople, mDataManager.isCanLoadMore());
                    hideLoading();
                } else {
                    clearDisposable();
                    Observable<Response<ForwardResult>> call = mDataManager.searchPeople(text);
                    ResponseObserverHelper<Response<ForwardResult>> helper = new ResponseObserverHelper<>(getContext(), call);
                    addDisposable(helper.execute(new OnCallbackListener<Response<ForwardResult>>() {
                        @Override
                        public void onFail(ErrorThrowable ex) {
                            mCallback.onPeopleSearch(null);
                            hideLoading();
                        }

                        @Override
                        public void onComplete(Response<ForwardResult> result) {
                            mCallback.onPeopleSearch(new ArrayList<>(result.body().getResult()));
                            hideLoading();
                        }
                    }));
                }
            }

            @Override
            public void onTyping(String text) {
                showLoading();
            }
        };
    }

    public void onBackPress() {
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).finish();
        }
    }

    @Override
    public void onRetryClick() {
        super.onRetryClick();

        Observable<Response<ForwardResult>> suggestCall = mDataManager.getSuggestion();
        ResponseObserverHelper<Response<ForwardResult>> suggestHelper =
                new ResponseObserverHelper<>(getContext(), suggestCall);
        addDisposable(suggestHelper.execute(new OnCallbackListener<Response<ForwardResult>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                showError(ex.toString());
            }

            @Override
            public void onComplete(Response<ForwardResult> result) {
                mCacheSuggestion.clear();
                mCacheSuggestion.addAll(result.body().getSuggested());
                mCallback.onSuggestPeopleComplete(mCacheSuggestion);

                mCachePeople.clear();
                mCachePeople.addAll(result.body().getPeople());
                mCallback.onPeopleComplete(mCachePeople, mDataManager.isCanLoadMore());
                hideLoading();
            }
        }));
    }

    public String onForwardClick(ConversationDataAdaptive data) {
        final User user;
        Product product = null;

        if (data instanceof Conversation) {
            user = ((Conversation) data).getRecipient(getContext());
            product = ((Conversation) data).getProduct();
        } else if (data instanceof User) {
            user = (User) data;
        } else {
            user = null;
        }


        if (user != null) {
            String channelId;
            if (product != null) {
                channelId = ConversationUtil
                        .getProductConversationId(product.getId(), user.getId(), SharedPrefUtils.getUserId(getContext()));
            } else {
                channelId = ConversationUtil
                        .getConversationId(user.getId(), SharedPrefUtils.getUserId(getContext()));
            }
            return getMessageId(user, channelId, product);

        }
        return null;
    }

    private String getMessageId(User user, String channelId, Product product) {
        String messageId = null;
        if (mForwardProductMedia != null && !mForwardProductMedia.isEmpty()) {
            Chat chat = Chat.getNewChat(getContext(), user, product);
            chat.setMediaList(mForwardProductMedia);
            chat.setChannelId(channelId);
            mAbsChatBinder.sendMessage(user, chat);
            messageId = chat.getId();
        }

        if (!TextUtils.isEmpty(mForwardChat.get())) {
            Chat chat = Chat.getNewChat(getContext(), user, product);
            chat.setContent(mForwardChat.get());
            chat.setChannelId(channelId);
            mAbsChatBinder.sendMessage(user, chat);

            if (TextUtils.isEmpty(messageId)) {
                messageId = chat.getId();
            }
        }
        if (!TextUtils.isEmpty(mUrl)) {
            Chat chat = Chat.getNewChat(getContext(), user, product);
            chat.setContent(mUrl);
            chat.setChannelId(channelId);
            mAbsChatBinder.sendMessage(user, chat);

            if (TextUtils.isEmpty(messageId)) {
                messageId = chat.getId();
            }
        }
        return messageId;
    }

    @Override
    public void onMessageUpdated(Chat message, MessageState state) {
        mCallback.onForwardComplete(message.getId(), MessageState.SENT);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAbsChatBinder != null) {
            mAbsChatBinder.removeGlobalChatListener(this);
        }
    }

    public interface OnViewModelCallback {
        void onSuggestPeopleComplete(List<ConversationDataAdaptive> suggest);

        void onPeopleComplete(List<ConversationDataAdaptive> people, boolean isLoadMore);

        void onPeopleSearch(List<ConversationDataAdaptive> people);

        void onForwardComplete(String messageId,
                               MessageState messageState);
    }
}
