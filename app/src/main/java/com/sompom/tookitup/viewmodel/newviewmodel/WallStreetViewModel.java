package com.sompom.tookitup.viewmodel.newviewmodel;

import android.content.Context;
import android.databinding.ObservableBoolean;

import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.NetworkBroadcastReceiver;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.model.ActiveUser;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.Locations;
import com.sompom.tookitup.model.MoreGameItem;
import com.sompom.tookitup.model.WallStreetAdaptive;
import com.sompom.tookitup.model.request.ReportRequest;
import com.sompom.tookitup.model.result.LoadMoreWrapper;
import com.sompom.tookitup.model.result.Product;
import com.sompom.tookitup.model.result.WallLoadMoreWrapper;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.BaseObserverHelper;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.WallStreetDataManager;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by He Rotha on 7/10/18.
 */
public class WallStreetViewModel extends AbsLoadingViewModel {
    public final ObservableBoolean mIsShowRecyclerView = new ObservableBoolean();
    public final ObservableBoolean mIsRefresh = new ObservableBoolean();
    private final WallStreetDataManager mWallStreetDataManager;
    private final OnCallback mCompleteListener;
    private final MoreGameItem mMoreGameList = new MoreGameItem();
    private boolean mIsLoadedData = false;
    private NetworkBroadcastReceiver.NetworkListener mNetworkStateListener;

    public WallStreetViewModel(Context context,
                               WallStreetDataManager manager,
                               OnCallback onCallback) {
        super(context);
        mWallStreetDataManager = manager;
        mCompleteListener = onCallback;

        if (getContext() instanceof AbsBaseActivity) {
            mNetworkStateListener = networkState -> {
                if (!mIsLoadedData) {
                    if (networkState == NetworkBroadcastReceiver.NetworkState.Connected) {
                        getTimeline();
                    } else if (networkState == NetworkBroadcastReceiver.NetworkState.Disconnected) {
                        showNetworkError();
                    }
                }
            };
            ((AbsBaseActivity) getContext()).addNetworkStateChangeListener(mNetworkStateListener);
        }
    }

    public OnClickListener onButtonRetryClick() {
        return this::getTimeline;
    }

    public void onRefresh() {
        mIsRefresh.set(true);
        getTimeline();
    }

    private void getTimeline() {
        if (!mIsRefresh.get()) {
            showLoading();
            mIsShowRecyclerView.set(false);
        }
        mWallStreetDataManager.resetPagination();
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).requestLocation((location, isNeverAskAgain) -> startGetData(location));
        }
    }

    private void startGetData(Locations locations) {
        Observable<WallLoadMoreWrapper<Adaptive>> callback;

        /*
            Hide more game.
         */
//        if (mMoreGameList.isEmpty()) {
//            callback = mWallStreetDataManager.getMoreGame().concatMap(moreGames -> {
//                mMoreGameList.addAll(moreGames);
//                mCompleteListener.onGetMoreGameSuccess(mMoreGameList);
//                return mWallStreetDataManager.getData(locations.getLatitude(), locations.getLongtitude());
//            });
//        } else {
//            callback = mWallStreetDataManager.getData(locations.getLatitude(), locations.getLongtitude());
//        }

        callback = mWallStreetDataManager.getData(locations.getLatitude(), locations.getLongtitude());

        BaseObserverHelper<WallLoadMoreWrapper<Adaptive>> helper = new BaseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<WallLoadMoreWrapper<Adaptive>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mIsRefresh.set(false);
                if (mIsLoadedData) {
                    return;
                }
                mIsLoadedData = false;
                mIsShowRecyclerView.set(false);
                if (ex.getCode() == 500) {
                    showError(getContext().getString(R.string.error_form));
                } else if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                    showNetworkError();
                } else {
                    showError(ex.toString());
                }
            }

            @Override
            public void onComplete(WallLoadMoreWrapper<Adaptive> result) {
                hideLoading();
                mIsShowRecyclerView.set(true);
                mIsLoadedData = true;
                mIsRefresh.set(false);
                mIsError.set(false);
                mCompleteListener.onGetTimelineSuccess(result.getData(),
                        mWallStreetDataManager.isCanLoadMore());

            }
        }));
    }

    public void loadMoreData(OnCallbackListListener<LoadMoreWrapper<Adaptive>> listener) {
        Observable<WallLoadMoreWrapper<Adaptive>> callback = mWallStreetDataManager.loadMoreData();
        BaseObserverHelper<WallLoadMoreWrapper<Adaptive>> helper = new BaseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<WallLoadMoreWrapper<Adaptive>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                listener.onFail(ex);
            }

            @Override
            public void onComplete(WallLoadMoreWrapper<Adaptive> result) {
                listener.onComplete(result, mWallStreetDataManager.isCanLoadMore());
            }
        }));
    }

    public void checkTopPostFollow(String id, boolean isFollow) {
        Observable<Response<Object>> callback;
        if (isFollow) {
            callback = mWallStreetDataManager.postFollow(id);
        } else {
            callback = mWallStreetDataManager.unFollow(id);
        }
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute());
    }

    public void deletePost(WallStreetAdaptive lifeStream) {
        Observable<Response<Object>> call = mWallStreetDataManager.deletePost(lifeStream.getId());
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), call);
        addDisposable(helper.execute());
    }

    public void deleteProduct(Product product) {
        Observable<Response<Object>> call = mWallStreetDataManager.deleteProduct(product);
        ResponseObserverHelper<Response<Object>> helper
                = new ResponseObserverHelper<>(getContext(), call);
        addDisposable(helper.execute());
    }

    public void reportProduct(String id, ReportRequest reportRequest) {
        Observable<Response<Object>> request = mWallStreetDataManager.reportProduct(id, reportRequest);
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), request);
        addDisposable(helper.execute());
    }

    public void unFollowShop(String id) {
        Observable<Response<Object>> observable = mWallStreetDataManager.unFollowShop(id);
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), observable);
        addDisposable(helper.execute());
    }

    public void reportPost(String id, ReportRequest reportRequest) {
        Observable<Response<Object>> request = mWallStreetDataManager.reportPost(id, reportRequest);
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), request);
        addDisposable(helper.execute());
    }

    public NetworkBroadcastReceiver.NetworkListener getNetworkStateListener() {
        return mNetworkStateListener;
    }

    public interface OnCallback {
        void onGetMoreGameSuccess(MoreGameItem moreGames);

        void onGetActiveUserSuccess(ActiveUser activeUser);

        void onGetTimelineSuccess(List<Adaptive> list, boolean isCanLoadMore);
    }
}
