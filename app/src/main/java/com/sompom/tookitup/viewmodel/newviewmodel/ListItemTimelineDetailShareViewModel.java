package com.sompom.tookitup.viewmodel.newviewmodel;

import android.databinding.ObservableField;
import android.view.View;

import com.sompom.tookitup.listener.OnDetailShareItemCallback;
import com.sompom.tookitup.listener.OnItemClickListener;
import com.sompom.tookitup.listener.OnTimelineItemButtonClickListener;
import com.sompom.tookitup.model.Adaptive;
import com.sompom.tookitup.model.SharedProduct;
import com.sompom.tookitup.model.SharedTimeline;
import com.sompom.tookitup.model.WallStreetAdaptive;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.services.datamanager.WallStreetDataManager;
import com.sompom.tookitup.viewholder.BindingViewHolder;

/**
 * Created by nuonveyo on 7/12/18.
 */

public class ListItemTimelineDetailShareViewModel extends ListItemTimelineViewModel {
    public final ObservableField<ListItemTimelineHeaderViewModel> mListItemTimelineHeaderViewModel = new ObservableField<>();
    public final ObservableField<WallStreetAdaptive> mAdaptive = new ObservableField<>();
    public final ObservableField<BindingViewHolder> mBindingViewHolder = new ObservableField<>();

    private final WallStreetAdaptive mLifeStream;
    private final OnDetailShareItemCallback mOnLikeButtonClickListener;

    public ListItemTimelineDetailShareViewModel(AbsBaseActivity activity,
                                                BindingViewHolder viewHolder,
                                                WallStreetDataManager wallStreetDataManager,
                                                WallStreetAdaptive adaptive,
                                                OnTimelineItemButtonClickListener listener,
                                                OnDetailShareItemCallback onLikeButtonClickListener) {
        super(activity, (Adaptive) adaptive, wallStreetDataManager, 0, listener);
        mAdaptive.set(adaptive);
        mOnLikeButtonClickListener = onLikeButtonClickListener;
        mBindingViewHolder.set(viewHolder);

        if (adaptive instanceof SharedProduct) {
            mLifeStream = ((SharedProduct) adaptive).getProduct();
        } else {
            mLifeStream = ((SharedTimeline) adaptive).getLifeStream();
        }
        ListItemTimelineHeaderViewModel headerViewModel = new ListItemTimelineHeaderViewModel((AbsBaseActivity) getContext(),
                (Adaptive) mLifeStream,
                0,
                getOnItemClickListener());
        headerViewModel.mIsSetMaxLine.set(false);
        mListItemTimelineHeaderViewModel.set(headerViewModel);
    }

    @Override
    public void onLikeClick(View view) {
        super.onLikeClick(view);
        if (mOnLikeButtonClickListener != null) mOnLikeButtonClickListener.onClick();
    }

    public OnItemClickListener<Integer> onItemClickListener() {
        return integer -> mOnLikeButtonClickListener.onMediaClick(mLifeStream, integer);
    }
}
