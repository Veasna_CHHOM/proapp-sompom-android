package com.sompom.tookitup.viewmodel.newviewmodel;

import com.sompom.tookitup.R;
import com.sompom.tookitup.broadcast.NetworkBroadcastReceiver;
import com.sompom.tookitup.listener.OnCallbackListListener;
import com.sompom.tookitup.listener.OnCallbackListener;
import com.sompom.tookitup.listener.OnClickListener;
import com.sompom.tookitup.listener.OnLayoutLikeListener;
import com.sompom.tookitup.listener.OnLikeItemListener;
import com.sompom.tookitup.model.result.User;
import com.sompom.tookitup.newui.AbsBaseActivity;
import com.sompom.tookitup.observable.BaseObserverHelper;
import com.sompom.tookitup.observable.ErrorThrowable;
import com.sompom.tookitup.observable.ResponseObserverHelper;
import com.sompom.tookitup.services.datamanager.LikeViewerDataManager;
import com.sompom.tookitup.viewmodel.AbsLoadingViewModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by nuonveyo on 6/8/18.
 */

public class LayoutLikeViewModel extends AbsLoadingViewModel implements OnLikeItemListener {
    private final LikeViewerDataManager mDataManager;
    private final String mProductId;
    private final OnLayoutLikeListener mOnCallback;
    private NetworkBroadcastReceiver.NetworkListener mNetworkStateListener;
    private boolean mIsLoadedData;

    public LayoutLikeViewModel(String productID,
                               LikeViewerDataManager dataManager,
                               OnLayoutLikeListener onCallback) {
        super(dataManager.getContext());
        mProductId = productID;
        mDataManager = dataManager;
        mOnCallback = onCallback;

        if (getContext() instanceof AbsBaseActivity) {
            mNetworkStateListener = networkState -> {
                if (!mIsLoadedData) {
                    if (networkState == NetworkBroadcastReceiver.NetworkState.Connected) {
                        getData();
                    } else if (networkState == NetworkBroadcastReceiver.NetworkState.Disconnected) {
                        showNetworkError();
                    }
                }
            };
            ((AbsBaseActivity) getContext()).addNetworkStateChangeListener(mNetworkStateListener);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getContext() instanceof AbsBaseActivity) {
            ((AbsBaseActivity) getContext()).removeStateChangeListener(mNetworkStateListener);
        }
    }

    private void getData() {
        showLoading();
        Observable<List<User>> callback = mDataManager.getUserWhoLikeProduct(mProductId);
        BaseObserverHelper<List<User>> helper = new BaseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<List<User>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                mIsLoadedData = false;
                if (ex.getCode() == 500) {
                    showError(getContext().getString(R.string.error_form));
                } else if (ex.getCode() == ErrorThrowable.NETWORK_ERROR) {
                    showNetworkError();
                } else {
                    showError(ex.toString());
                }
            }

            @Override
            public void onComplete(List<User> result) {
                hideLoading();
                mIsLoadedData = true;
                mOnCallback.onComplete(result, mDataManager.isCanLoadMore());
            }
        }));
    }

    public void loadMore(OnCallbackListListener<List<User>> callbackListener) {
        Observable<List<User>> callback = mDataManager.getUserWhoLikeProduct(mProductId);
        BaseObserverHelper<List<User>> helper = new BaseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute(new OnCallbackListener<List<User>>() {
            @Override
            public void onFail(ErrorThrowable ex) {
                callbackListener.onFail(ex);
            }

            @Override
            public void onComplete(List<User> result) {
                callbackListener.onComplete(result, mDataManager.isCanLoadMore());
            }
        }));
    }

    @Override
    public void onFollowClick(User user, boolean isFollow) {
        Observable<Response<Object>> callback;
        if (isFollow) {
            callback = mDataManager.postFollow(user.getId());
        } else {
            callback = mDataManager.unFollow(user.getId());
        }
        ResponseObserverHelper<Response<Object>> helper = new ResponseObserverHelper<>(getContext(), callback);
        addDisposable(helper.execute());
    }

    @Override
    public void onNotifyItem() {
        mOnCallback.onNotifyItem();
    }

    public OnClickListener onRetryButtonClick() {
        return this::getData;
    }
}
