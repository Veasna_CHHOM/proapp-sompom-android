package com.example.usermentionable.utils;

import android.content.Context;
import android.text.TextUtils;
import android.widget.ImageView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.usermentionable.R;
import com.example.usermentionable.model.UserMentionable;

/**
 * Created by nuonveyo on 12/6/18.
 */

public final class GlideUtil {
    private GlideUtil() {
    }

    public static void loadImageProfile(ImageView imageView, UserMentionable userMentionable) {
        TextDrawable drawable = getTextDrawable(imageView.getContext(), userMentionable.getLastName());
        Glide.with(imageView.getContext())
                .load(userMentionable.getImageUrl())
                .apply(RequestOptions
                        .circleCropTransform()
                        .centerCrop()
                        .override(0, 400)
                        .placeholder(drawable))
                .into(imageView);
    }

    private static TextDrawable getTextDrawable(Context context, String name) {
        if (TextUtils.isEmpty(name)) {
            name = context.getResources().getString(R.string.app_name);
        }
        char letter = name.charAt(0);
        ColorGenerator generator = ColorGenerator.MATERIAL;
        int color = generator.getColor(name);
        return TextDrawable.builder()
                .buildRound(String.valueOf(letter).toUpperCase(), color);
    }

}
