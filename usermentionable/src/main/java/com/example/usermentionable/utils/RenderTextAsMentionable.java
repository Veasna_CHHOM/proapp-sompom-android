package com.example.usermentionable.utils;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.widget.EditText;

import com.example.usermentionable.R;
import com.example.usermentionable.mentions.MentionSpan;
import com.example.usermentionable.model.UserMentionable;
import com.example.usermentionable.widget.MentionsEditText;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nuonveyo on 12/6/18.
 */

public final class RenderTextAsMentionable {
    public static String render(Context context, Editable editable) {
        EditText editText = new EditText(context);
        editText.setText(editable);
        MentionSpan[] spans = editText.getText().getSpans(0, editText.getText().length(), MentionSpan.class);
        if (spans != null && spans.length > 0) {
            for (MentionSpan span : spans) {
                int start = editText.getText().getSpanStart(span);
                int end = editText.getText().getSpanEnd(span);
                UserMentionable userMentionable = (UserMentionable) span.getMention();
                if (!userMentionable.getUserName().startsWith("#")) {
                    editText.getText().replace(start, end, getMentionUserFormat(context, userMentionable));
                }
            }
        }
        return editText.getText().toString();
    }

    public static List<UserMentionable> getMentionableList(List<UserMentionable> list, MentionsEditText mentionsEditText) {
        List<UserMentionable> userMentionableList = new ArrayList<>();
        Editable text = mentionsEditText.getText();
        if (text != null) {
            for (UserMentionable userMentionable : list) {
                if (!isUserAdded(userMentionable, mentionsEditText.getMentionsText().getMentionSpans())) {
                    userMentionableList.add(userMentionable);
                }
            }
        } else {
            userMentionableList = list;
        }
        return userMentionableList;
    }

    private static boolean isUserAdded(UserMentionable user, List<MentionSpan> spanList) {
        for (MentionSpan span : spanList) {
            UserMentionable userMentionable = (UserMentionable) span.getMention();
            if (TextUtils.equals(user.getUserId(), userMentionable.getUserId())) {
                return true;
            }
        }
        return false;
    }

    private static String getMentionUserFormat(Context context, UserMentionable user) {
        return context.getString(R.string.mention_format,
                user.getUserId(),
                user.getUserName());
    }
}
