package com.example.usermentionable.widget;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v13.view.inputmethod.EditorInfoCompat;
import android.support.v13.view.inputmethod.InputConnectionCompat;
import android.support.v13.view.inputmethod.InputContentInfoCompat;
import android.util.AttributeSet;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;

/**
 * Created by nuonveyo on 11/1/18.
 */

public class KeyboardInputEditor extends android.support.v7.widget.AppCompatEditText {
    private String[] mImgTypeString;
    private KeyBoardInputCallbackListener keyBoardInputCallbackListener;
    private final InputConnectionCompat.OnCommitContentListener callback = new InputConnectionCompat.OnCommitContentListener() {
        @Override
        public boolean onCommitContent(InputContentInfoCompat inputContentInfo,
                                       int flags, Bundle opts) {

            // read and display inputContentInfo asynchronously
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1 && (flags &
                    InputConnectionCompat.INPUT_CONTENT_GRANT_READ_URI_PERMISSION) != 0) {
                try {
                    inputContentInfo.requestPermission();
                } catch (Exception e) {
                    return false; // return false if failed
                }
            }
            boolean supported = false;
            for (final String mimeType : mImgTypeString) {
                if (inputContentInfo.getDescription().hasMimeType(mimeType)) {
                    supported = true;
                    break;
                }
            }
            if (!supported) {
                return false;
            }

            if (keyBoardInputCallbackListener != null) {
                keyBoardInputCallbackListener.onCommitContent(inputContentInfo, flags, opts);
            }
            return true;  // return true if succeeded
        }
    };

    public KeyboardInputEditor(Context context) {
        super(context);
        initView(null);
    }

    public KeyboardInputEditor(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(attrs);
    }

    public KeyboardInputEditor(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(attrs);
    }

    private void initView(AttributeSet attrs) {
        mImgTypeString = new String[]{"image/png",
                "image/gif",
                "image/jpeg",
                "image/webp"};
    }

    @Override
    public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
        final InputConnection ic = super.onCreateInputConnection(outAttrs);
        EditorInfoCompat.setContentMimeTypes(outAttrs,
                mImgTypeString);
        return InputConnectionCompat.createWrapper(ic, outAttrs, callback);
    }

    public void setKeyBoardInputCallbackListener(KeyBoardInputCallbackListener keyBoardInputCallbackListener) {
        this.keyBoardInputCallbackListener = keyBoardInputCallbackListener;
    }

    public interface KeyBoardInputCallbackListener {
        void onCommitContent(InputContentInfoCompat inputContentInfo,
                             int flags, Bundle opts);
    }
}
