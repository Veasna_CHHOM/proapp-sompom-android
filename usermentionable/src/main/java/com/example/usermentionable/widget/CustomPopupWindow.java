package com.example.usermentionable.widget;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.PopupWindowCompat;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;

/**
 * Created by nuonveyo
 * on 1/28/19.
 */

class CustomPopupWindow {

    private final Context mContext;
    private final PopupWindow mPopup;
    private final Rect mTempRect;
    private View mDropDownAnchorView;

    private ListView mDropDownList;
    private ListAdapter mAdapter;

    private final int mDropDownHorizontalOffset;
    private int mDropDownHeight;
    private int mDropDownWidth;
    private int mDropDownVerticalOffset;

    private boolean mDropDownVerticalOffsetSet;

    private DataSetObserver mObserver;
    private Callback mCallback;

    CustomPopupWindow(Context context) {
        mContext = context;
        mPopup = new PopupWindow(mContext);
        mTempRect = new Rect();
        mDropDownHorizontalOffset = 0;
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    public void setHeight(int dropDownHeight) {
        mDropDownHeight = dropDownHeight;
    }

    public void setWidth(int dropDownWidth) {
        mDropDownWidth = dropDownWidth;
    }

    void setAnchorView(View view) {
        mDropDownAnchorView = view;
    }

    void setVerticalOffset(int offset) {
        this.mDropDownVerticalOffset = offset;
        this.mDropDownVerticalOffsetSet = true;
    }

    void setBackgroundDrawable(@Nullable Drawable d) {
        this.mPopup.setBackgroundDrawable(d);
    }

    public void setAdapter(@Nullable ListAdapter adapter) {
        if (this.mObserver == null) {
            this.mObserver = new PopupDataSetObserver();
        } else if (this.mAdapter != null) {
            this.mAdapter.unregisterDataSetObserver(this.mObserver);
        }

        this.mAdapter = adapter;
        if (adapter != null) {
            adapter.registerDataSetObserver(this.mObserver);
        }

        if (this.mDropDownList != null) {
            this.mDropDownList.setAdapter(this.mAdapter);
        }

    }

    public void dismiss() {
        this.mPopup.dismiss();
        this.mPopup.setContentView(null);
        this.mDropDownList = null;
    }

    public void show() {
        if (getAnchorView() == null) {
            return;
        }

        int height = this.buildDropDown();
        boolean noInputMethod = this.isInputMethodNotNeeded();
        PopupWindowCompat.setWindowLayoutType(this.mPopup, 1002);
        int widthSpec;
        int heightSpec;
        if (this.mPopup.isShowing()) {
            if (!ViewCompat.isAttachedToWindow(getAnchorView())) {
                return;
            }

            if (this.mDropDownWidth == -1) {
                widthSpec = -1;
            } else if (this.mDropDownWidth == -2) {
                widthSpec = this.getAnchorView().getWidth();
            } else {
                widthSpec = this.mDropDownWidth;
            }

            if (this.mDropDownHeight == -1) {
                heightSpec = noInputMethod ? height : -1;
                if (noInputMethod) {
                    this.mPopup.setWidth(this.mDropDownWidth == -1 ? -1 : 0);
                    this.mPopup.setHeight(0);
                } else {
                    this.mPopup.setWidth(this.mDropDownWidth == -1 ? -1 : 0);
                    this.mPopup.setHeight(-1);
                }
            } else if (this.mDropDownHeight == -2) {
                heightSpec = height;
            } else {
                heightSpec = this.mDropDownHeight;
            }

            this.mPopup.setOutsideTouchable(true);
            this.mPopup.update(this.getAnchorView(), this.mDropDownHorizontalOffset, this.mDropDownVerticalOffset, widthSpec < 0 ? -1 : widthSpec, heightSpec < 0 ? -1 : heightSpec);
            if (mPopup.isAboveAnchor() && mCallback != null) {
                mCallback.onUpdateHeight(widthSpec, heightSpec);
            }
        } else {
            if (this.mDropDownWidth == -1) {
                widthSpec = -1;
            } else if (this.mDropDownWidth == -2) {
                widthSpec = this.getAnchorView().getWidth();
            } else {
                widthSpec = this.mDropDownWidth;
            }

            if (this.mDropDownHeight == -1) {
                heightSpec = -1;
            } else if (this.mDropDownHeight == -2) {
                heightSpec = height;
            } else {
                heightSpec = this.mDropDownHeight;
            }

            this.mPopup.setWidth(widthSpec);
            this.mPopup.setHeight(heightSpec);
            this.mPopup.setClippingEnabled(true);
            this.mPopup.setOutsideTouchable(true);

            PopupWindowCompat.showAsDropDown(this.mPopup,
                    getAnchorView(),
                    this.mDropDownHorizontalOffset,
                    this.mDropDownVerticalOffset,
                    Gravity.NO_GRAVITY);
            if (mPopup.isAboveAnchor() && mCallback != null) {
                mCallback.onUpdateHeight(widthSpec, heightSpec);
            }
            this.mDropDownList.setSelection(-1);
        }
    }

    void update(int widthSpec, int heightSpec, int offset) {
        this.mPopup.update(this.getAnchorView(), this.mDropDownHorizontalOffset, offset, widthSpec < 0 ? -1 : widthSpec, heightSpec < 0 ? -1 : heightSpec);
    }

    private int buildDropDown() {
        if (getAnchorView() == null) {
            return 0;
        }
        int otherHeights = 0;
        int widthSize;
        int widthMode;
        int widthSpec;
        if (this.mDropDownList == null) {
            this.mDropDownList = new ListView(mContext);
            this.mDropDownList.setDivider(null);
            this.mDropDownList.setAdapter(this.mAdapter);
            this.mDropDownList.setFocusable(true);
            this.mDropDownList.setFocusableInTouchMode(true);
        }

        ViewGroup dropDownView = this.mDropDownList;
        this.mPopup.setContentView(dropDownView);
        Drawable background = this.mPopup.getBackground();
        int padding;
        if (background != null) {
            background.getPadding(this.mTempRect);
            padding = this.mTempRect.top + this.mTempRect.bottom;
            if (!this.mDropDownVerticalOffsetSet) {
                this.mDropDownVerticalOffset = -this.mTempRect.top;
            }
        } else {
            this.mTempRect.setEmpty();
            padding = 0;
        }

        int maxHeight = this.mPopup.getMaxAvailableHeight(getAnchorView(), mDropDownVerticalOffset);
        if (this.mDropDownHeight != -1) {
            int var10001;
            switch (this.mDropDownWidth) {
                case -2:
                    var10001 = this.mTempRect.left + this.mTempRect.right;
                    widthSize = View.MeasureSpec.makeMeasureSpec(this.mContext.getResources().getDisplayMetrics().widthPixels - var10001, View.MeasureSpec.AT_MOST);
                    break;
                case -1:
                    var10001 = this.mTempRect.left + this.mTempRect.right;
                    widthSize = View.MeasureSpec.makeMeasureSpec(this.mContext.getResources().getDisplayMetrics().widthPixels - var10001, View.MeasureSpec.EXACTLY);
                    break;
                default:
                    widthSize = View.MeasureSpec.makeMeasureSpec(this.mDropDownWidth, View.MeasureSpec.EXACTLY);
            }

            widthMode = measureHeightOfChildrenCompat(widthSize, maxHeight - otherHeights, -1);
            if (widthMode > 0) {
                widthSpec = this.mDropDownList.getPaddingTop() + this.mDropDownList.getPaddingBottom();
                otherHeights += padding + widthSpec;
            }
            return widthMode + otherHeights;
        } else {
            return maxHeight + padding;
        }
    }

    private int measureHeightOfChildrenCompat(int widthMeasureSpec, int maxHeight, int disallowPartialChildPosition) {
        int paddingTop = mDropDownList.getListPaddingTop();
        int paddingBottom = mDropDownList.getListPaddingBottom();
        int reportedDividerHeight = mDropDownList.getDividerHeight();
        Drawable divider = mDropDownList.getDivider();
        ListAdapter adapter = mDropDownList.getAdapter();
        if (adapter == null) {
            return paddingTop + paddingBottom;
        } else {
            int returnedHeight = paddingTop + paddingBottom;
            int dividerHeight = reportedDividerHeight > 0 && divider != null ? reportedDividerHeight : 0;
            int prevHeightWithoutPartialChild = 0;
            View child = null;
            int viewType = 0;
            int count = adapter.getCount();

            for (int i = 0; i < count; ++i) {
                int newType = adapter.getItemViewType(i);
                if (newType != viewType) {
                    child = null;
                    viewType = newType;
                }

                child = adapter.getView(i, child, mDropDownList);
                AbsListView.LayoutParams childLp = (AbsListView.LayoutParams) child.getLayoutParams();

                int heightMeasureSpec;
                if (childLp.height > 0) {
                    heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(childLp.height, View.MeasureSpec.EXACTLY);
                } else {
                    heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                }

                child.measure(widthMeasureSpec, heightMeasureSpec);
                child.forceLayout();
                if (i > 0) {
                    returnedHeight += dividerHeight;
                }

                returnedHeight += child.getMeasuredHeight();
                if (returnedHeight >= maxHeight) {
                    return disallowPartialChildPosition >= 0 && i > disallowPartialChildPosition && prevHeightWithoutPartialChild > 0 && returnedHeight != maxHeight ? prevHeightWithoutPartialChild : maxHeight;
                }

                if (disallowPartialChildPosition >= 0 && i >= disallowPartialChildPosition) {
                    prevHeightWithoutPartialChild = returnedHeight;
                }
            }

            return returnedHeight;
        }
    }

    private boolean isInputMethodNotNeeded() {
        return this.mPopup.getInputMethodMode() == 2;
    }

    @Nullable
    private View getAnchorView() {
        return this.mDropDownAnchorView;
    }

    boolean isShowing() {
        return this.mPopup.isShowing();
    }

    private class PopupDataSetObserver extends DataSetObserver {
        PopupDataSetObserver() {
        }

        public void onChanged() {
            if (isShowing()) {
                show();
            }
        }

        public void onInvalidated() {
            dismiss();
        }
    }

    public interface Callback {
        void onUpdateHeight(int width, int height);
    }
}
