/*
 * Copyright 2015 LinkedIn Corp. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package com.example.usermentionable.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.ListPopupWindow;
import android.text.InputType;
import android.text.Layout;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.example.usermentionable.R;
import com.example.usermentionable.adapter.MentionUserAdapter;
import com.example.usermentionable.mentions.MentionSpanConfig;
import com.example.usermentionable.model.UserMentionable;
import com.example.usermentionable.tokenization.QueryToken;
import com.example.usermentionable.tokenization.impl.WordTokenizer;
import com.example.usermentionable.tokenization.impl.WordTokenizerConfig;
import com.example.usermentionable.tokenization.interfaces.QueryTokenReceiver;
import com.example.usermentionable.tokenization.interfaces.SuggestionsVisibilityManager;
import com.example.usermentionable.utils.RenderTextAsMentionable;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Custom view for the RichEditor. Manages three subviews:
 * <p/>
 * 1. EditText - contains text typed by user <br/>
 * 2. TextView - displays count of the number of characters in the EditText <br/>
 * 3. ListView - displays mention suggestions when relevant
 * <p/>
 * <b>XML attributes</b>
 * <p/>
 */
public class RichEditorView extends RelativeLayout implements QueryTokenReceiver, SuggestionsVisibilityManager {
    private static final String PATTER_HAST_TAG = "[#]+[\\p{Alnum}\\p{M}*+_]+\\b";
    private static final String PATTER_USER_NAME = "+[@\\p{Alnum}\\p{M}*+_\\s]+[]]";
    private static final String PATTER_USER_ID = "[\\[]+[\\-A-Za-z0-9]+[:]";
    private static final String PATTER_USER_MENTION = "[@]+" + PATTER_USER_ID + PATTER_USER_NAME; // mention format @[user-id:user-name]

    private MentionsEditText mMentionsEditText;
    private int mOriginalInputType = InputType.TYPE_CLASS_TEXT; // Default to plain text

    private QueryTokenReceiver mHostQueryTokenReceiver;

    private ViewGroup.LayoutParams mPrevEditTextParams;
    private CustomPopupWindow mListPopupWindow;
    private boolean mIsAlwaysShowAboveAnchor;
    private MentionUserAdapter mMentionUserAdapter;

    public RichEditorView(@NonNull Context context) {
        super(context);
        init(context, null, 0);
    }

    public RichEditorView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public RichEditorView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater != null) {
            inflater.inflate(R.layout.editor_view, this, true);
        }
        mMentionsEditText = findViewById(R.id.text_editor);

        // Get the MentionSpanConfig from custom XML attributes and set it
        MentionSpanConfig mentionSpanConfig = parseMentionSpanConfigFromAttributes(attrs, defStyleAttr);
        mMentionsEditText.setMentionSpanConfig(mentionSpanConfig);

        // Create the tokenizer to use for the editor
        WordTokenizerConfig tokenizerConfig = new WordTokenizerConfig.Builder()
                .setThreshold(3)
                .setMaxNumKeywords(2)
                .setExplicitChars("@#")
                .build();
        WordTokenizer tokenizer = new WordTokenizer(tokenizerConfig);
        mMentionsEditText.setTokenizer(tokenizer);

        // Set various delegates on the MentionEditText to the RichEditorView
        mMentionsEditText.setQueryTokenReceiver(this);
        mMentionsEditText.setAvoidPrefixOnTap(true);
        mMentionsEditText.setSuggestionsVisibilityManager(this);
    }

    private MentionSpanConfig parseMentionSpanConfigFromAttributes(@Nullable AttributeSet attrs, int defStyleAttr) {
        final Context context = getContext();
        MentionSpanConfig.Builder builder = new MentionSpanConfig.Builder();
        if (attrs == null) {
            return builder.build();
        }

        TypedArray attributes = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.RichEditorView,
                defStyleAttr,
                0);
        @ColorInt int normalTextColor = attributes.getColor(R.styleable.RichEditorView_mentionTextColor, -1);
        builder.setMentionTextColor(normalTextColor);
        @ColorInt int normalBgColor = attributes.getColor(R.styleable.RichEditorView_mentionTextBackgroundColor, -1);
        builder.setMentionTextBackgroundColor(normalBgColor);
        @ColorInt int selectedTextColor = attributes.getColor(R.styleable.RichEditorView_selectedMentionTextColor, -1);
        builder.setSelectedMentionTextColor(selectedTextColor);
        @ColorInt int selectedBgColor = attributes.getColor(R.styleable.RichEditorView_selectedMentionTextBackgroundColor, -1);
        builder.setSelectedMentionTextBackgroundColor(selectedBgColor);

        String hint = attributes.getString(R.styleable.RichEditorView_textHint);
        if (!TextUtils.isEmpty(hint)) {
            mMentionsEditText.setHint(hint);
        }

        @ColorInt int textHintColor = attributes.getColor(R.styleable.RichEditorView_textHintColor, -1);
        if (textHintColor > -1) {
            mMentionsEditText.setHintTextColor(textHintColor);
        }

        @ColorInt int attrColor = attributes.getColor(R.styleable.RichEditorView_textColor, Color.BLACK);
        mMentionsEditText.setTextColor(attrColor);

        int textSize = attributes.getDimensionPixelSize(R.styleable.RichEditorView_textSize, 0);
        if (textSize > 0) {
            mMentionsEditText.setTextSize(textSize);
        }

        Drawable drawable = attributes.getDrawable(R.styleable.RichEditorView_background);
        mMentionsEditText.setBackground(drawable);

        int maxLine = attributes.getInteger(R.styleable.RichEditorView_maxLines, 0);
        if (maxLine > 0) {
            mMentionsEditText.setMaxLines(maxLine);
        }

        mIsAlwaysShowAboveAnchor = attributes.getBoolean(R.styleable.RichEditorView_alwaysDisplayAbove, false);

        attributes.recycle();

        return builder.build();
    }

    public void addOnTextChangeListener(TextWatcher textWatcher) {
        if (mMentionsEditText != null) mMentionsEditText.addTextChangedListener(textWatcher);
    }

    public void setOnKeyboardInputCallback(KeyboardInputEditor.KeyBoardInputCallbackListener listener) {
        if (mMentionsEditText != null) mMentionsEditText.setKeyBoardInputCallbackListener(listener);
    }

    public MentionsEditText getMentionsEditText() {
        return mMentionsEditText;
    }

    /**
     * @return current line number of the cursor, or -1 if no cursor
     */
    private int getCurrentCursorLine() {
        int selectionStart = mMentionsEditText.getSelectionStart();
        Layout layout = mMentionsEditText.getLayout();
        if (layout != null && selectionStart != -1) {
            return layout.getLineForOffset(selectionStart);
        }
        return -1;
    }

    // --------------------------------------------------
    // QueryTokenReceiver Implementation
    // --------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public void onQueryReceived(@NonNull QueryToken queryToken) {
        // Pass the query token to a host receiver
        if (mHostQueryTokenReceiver != null) {
            mHostQueryTokenReceiver.onQueryReceived(queryToken);
        }
    }

    @Override
    public void invalidToken() {
        if (mHostQueryTokenReceiver != null) {
            mHostQueryTokenReceiver.invalidToken();
        }
    }

    // --------------------------------------------------
    // SuggestionsManager Implementation
    // --------------------------------------------------

    /**
     * {@inheritDoc}
     */
    public void displaySuggestions(boolean display) {
        // Change view depending on whether suggestions are being shown or not
        if (display) {
            disableSpellingSuggestions(true);
            mPrevEditTextParams = mMentionsEditText.getLayoutParams();
            mMentionsEditText.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
            mMentionsEditText.setVerticalScrollBarEnabled(false);
            int cursorLine = getCurrentCursorLine();
            Layout layout = mMentionsEditText.getLayout();
            if (layout != null) {
                int lineTop = layout.getLineTop(cursorLine);
                mMentionsEditText.scrollTo(0, lineTop);
            }
        } else {
            disableSpellingSuggestions(false);
            if (mPrevEditTextParams == null) {
                mPrevEditTextParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            }
            mMentionsEditText.setLayoutParams(mPrevEditTextParams);
            mMentionsEditText.setVerticalScrollBarEnabled(true);
        }

        requestLayout();
        invalidate();
    }

    @Override
    public boolean isDisplayingSuggestions() {
        return false;
    }

    /**
     * Disables spelling suggestions from the user's keyboard.
     * This is necessary because some keyboards will replace the input text with
     * spelling suggestions automatically, which changes the suggestion results.
     * This results in a confusing user experience.
     *
     * @param disable {@code true} if spelling suggestions should be disabled, otherwise {@code false}
     */
    private void disableSpellingSuggestions(boolean disable) {
        // toggling suggestions often resets the cursor location, but we don't want that to happen
        int start = mMentionsEditText.getSelectionStart();
        int end = mMentionsEditText.getSelectionEnd();
        // -1 means there is no selection or cursor.
        if (start == -1 || end == -1) {
            return;
        }
        if (disable) {
            // store the previous input type
            mOriginalInputType = mMentionsEditText.getInputType();
        }
        mMentionsEditText.setRawInputType(disable ? InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS : mOriginalInputType);
        mMentionsEditText.setSelection(start, end);
    }

    // --------------------------------------------------
    // Pass-Through Methods to the MentionsEditText
    // --------------------------------------------------

    /**
     * Sets the text being displayed within the {@link RichEditorView}. Note that this removes the
     * {@link TextWatcher} temporarily to avoid changing the text while listening to text changes
     * (which could result in an infinite loop).
     *
     * @param text the text to display
     */
    public void setText(final @NonNull CharSequence text) {
        if (mMentionsEditText != null) {
            mMentionsEditText.setText(text);
            if (!TextUtils.isEmpty(text)) {
                renderUserMention(text);
                if (!TextUtils.isEmpty(mMentionsEditText.getText())) {
                    int lastIndex = mMentionsEditText.getText().length();
                    mMentionsEditText.setSelection(lastIndex);
                }
            }
        }
    }

    /**
     * This method call when user edit post
     *
     * @param text from edit post description for render
     */
    private void renderUserMention(CharSequence text) {
        Pattern pattern = Pattern.compile(PATTER_USER_MENTION);
        Matcher matcher = pattern.matcher(text);

        while (matcher.find()) {
            final String substring = matcher.group();
            Matcher findUserName = Pattern.compile("[:]" + PATTER_USER_NAME).matcher(substring);
            Matcher findUserId = Pattern.compile(PATTER_USER_ID).matcher(substring);

            if (findUserName.find() && findUserId.find()) {
                final String userName = substring.substring(findUserName.start() + 1, findUserName.end() - 1);
                final String userId = substring.substring(findUserId.start() + 1, findUserId.end() - 1);

                UserMentionable user = new UserMentionable();
                user.setUserName(userName);
                user.setUserId(userId);
                mMentionsEditText.insertMentionInternal(user, mMentionsEditText.getEditableText(), matcher.start(), matcher.end());
                matcher.reset(mMentionsEditText.getText());
            }
        }

        if (!TextUtils.isEmpty(mMentionsEditText.getText())) {
            Pattern tagPatter = Pattern.compile(PATTER_HAST_TAG);
            Matcher tagMatcher = tagPatter.matcher(mMentionsEditText.getText());
            while (tagMatcher.find()) {
                UserMentionable userMentionable = new UserMentionable();
                userMentionable.setUserName(tagMatcher.group());
                mMentionsEditText.replaceHashTag(userMentionable,
                        mMentionsEditText.getEditableText(),
                        tagMatcher.start(),
                        tagMatcher.end());
            }
        }
    }

    /**
     * Sets the receiver of any tokens generated by the embedded {@link MentionsEditText}. The
     * receive should act on the queries as they are received and call
     *
     * @param client the object that can receive {@link QueryToken} objects and generate suggestions from them
     */
    public void setQueryTokenReceiver(final @Nullable QueryTokenReceiver client) {
        mHostQueryTokenReceiver = client;
    }

    public void setListPopupWindow(boolean isShowListPopupWindow, List<UserMentionable> userMentionableList) {
        if (!isShowListPopupWindow) {
            removeListPopupWindow();
        } else {
            if (userMentionableList == null || userMentionableList.isEmpty()) {
                hideListPopupWindow();
            } else {
                showListPopupWindow(userMentionableList);
            }
        }
    }

    public void showListPopupWindow(List<UserMentionable> userMentionableList) {
        List<UserMentionable> list = RenderTextAsMentionable.getMentionableList(userMentionableList, mMentionsEditText);
        if (list != null && !list.isEmpty()) {
            if (mListPopupWindow == null) {
                mListPopupWindow = new CustomPopupWindow(getContext());
                mListPopupWindow.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.shadow_background));
                mListPopupWindow.setAnchorView(getMentionsEditText());
                mListPopupWindow.setWidth(ListPopupWindow.MATCH_PARENT);
                mListPopupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);
                mListPopupWindow.setCallback((width, height1) -> {
                    if (!mIsAlwaysShowAboveAnchor) {
                        int selectionStart = getSelectionStart();
                        Layout layout = getLayout();

                        int lineHeight = getMentionsEditText().getLineHeight();
                        int lineOffset = layout.getLineForOffset(selectionStart);
                        int textSize = (int) (getMentionsEditText().getTextSize() / 2);
                        int popupOffset = (lineHeight * lineOffset) + textSize;
                        int heightOffset = popupOffset + lineHeight;
                        mListPopupWindow.update(width, heightOffset, popupOffset);
                    }
                });

                mMentionUserAdapter = new MentionUserAdapter(list, user -> {
                    removeListPopupWindow();
                    if (mMentionsEditText != null) {
                        mMentionsEditText.insertMention(user);
                    }
                });
                mListPopupWindow.setAdapter(mMentionUserAdapter);
                checkToSetVerticalOffset();
                mListPopupWindow.show();

            } else {
                mMentionUserAdapter.notifyData(list);
                if (!mListPopupWindow.isShowing()) {
                    checkToSetVerticalOffset();
                    mListPopupWindow.show();
                }
            }
        } else {
            hideListPopupWindow();
        }
    }

    private void checkToSetVerticalOffset() {
        if (!mIsAlwaysShowAboveAnchor) {
            mListPopupWindow.setVerticalOffset(calculateOffsetFromBottom());
        }
    }

    public void hideListPopupWindow() {
        if (mListPopupWindow != null) {
            mListPopupWindow.dismiss();
        }
    }

    public void removeListPopupWindow() {
        if (mListPopupWindow != null) {
            mListPopupWindow.dismiss();
            mMentionUserAdapter = null;
            mListPopupWindow = null;
        }
    }

    public void checkToRenderHashTag(String tokenString) {
        mMentionsEditText.checkToRenderHashTag(tokenString);
    }

    private int calculateOffsetFromBottom() {
        int selectionStart = getSelectionStart();
        Layout layout = getLayout();

        int lineCount = getMentionsEditText().getLineCount();
        int lineHeight = getMentionsEditText().getLineHeight();
        int textSize = (int) (getMentionsEditText().getTextSize() / 2);
        int popupOffset;
        if (selectionStart != -1) {
            int lineOffset = layout.getLineForOffset(selectionStart);
            popupOffset = ((lineHeight * (lineCount - lineOffset)) - textSize);
        } else {
            popupOffset = ((lineHeight * lineCount) - textSize);
        }
        return -popupOffset;
    }

    private int getSelectionStart() {
        return Selection.getSelectionStart(getMentionsEditText().getText());
    }

    private Layout getLayout() {
        return getMentionsEditText().getLayout();
    }
}
