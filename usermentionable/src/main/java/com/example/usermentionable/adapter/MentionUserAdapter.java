package com.example.usermentionable.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.usermentionable.R;
import com.example.usermentionable.databinding.ListItemMentionBinding;
import com.example.usermentionable.listener.OnClickListener;
import com.example.usermentionable.model.UserMentionable;
import com.example.usermentionable.utils.GlideUtil;

import java.util.List;

/**
 * Created by nuonveyo on 11/16/18.
 */
public class MentionUserAdapter extends BaseAdapter {
    private List<UserMentionable> mUserMentionableList;
    private OnClickListener<UserMentionable> mCallback;

    public MentionUserAdapter(List<UserMentionable> userMentionableList, OnClickListener<UserMentionable> listener) {
        mUserMentionableList = userMentionableList;
        mCallback = listener;
    }

    @Override
    public int getCount() {
        return mUserMentionableList.size();
    }

    @Override
    public UserMentionable getItem(int position) {
        return mUserMentionableList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        @SuppressLint("ViewHolder") ListItemMentionBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.list_item_mention,
                parent,
                false);

        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = binding.getRoot();
            viewHolder = new ViewHolder(binding);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.setVariable(mUserMentionableList.get(position));
        convertView.setTag(viewHolder);

        return convertView;
    }

    public void notifyData(List<UserMentionable> list) {
        mUserMentionableList = list;
        notifyDataSetChanged();
    }

    private final class ViewHolder {
        private Context mContext;
        private ListItemMentionBinding mViewDataBinding;

        private ViewHolder(ListItemMentionBinding viewDataBinding) {
            mViewDataBinding = viewDataBinding;
            mContext = mViewDataBinding.getRoot().getContext();
        }

        public void setVariable(UserMentionable userMentionable) {
            String userName = mContext.getString(R.string.seller_store_user_fullName, userMentionable.getFirstName(), userMentionable.getLastName());
            mViewDataBinding.textView.setText(userName);
            GlideUtil.loadImageProfile(mViewDataBinding.imageView, userMentionable);
            mViewDataBinding.getRoot().setOnClickListener(view -> mCallback.onClick(userMentionable));
        }
    }
}
