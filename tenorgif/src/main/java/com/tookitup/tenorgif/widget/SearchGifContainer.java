package com.tookitup.tenorgif.widget;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;

import com.tookitup.tenorgif.R;
import com.tookitup.tenorgif.adapter.TenorGifAdapter;
import com.tookitup.tenorgif.databinding.LayoutSearchGifContainerBinding;
import com.tookitup.tenorgif.helper.DelayWatcher;
import com.tookitup.tenorgif.listener.OnClickListener;
import com.tookitup.tenorgif.model.BaseGifModel;
import com.tookitup.tenorgif.model.Result;
import com.tookitup.tenorgif.model.TenorResult;
import com.tookitup.tenorgif.service.ApiService;
import com.tookitup.tenorgif.service.ServiceGenerator;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by nuonveyo on 10/16/18.
 */

public class SearchGifContainer extends LinearLayout implements OnClickListener<Result> {
    private static final String MEDIA_FILTER = "minimal";
    private static final String CONTENT_FILTER = "medium";
    private ApiService mApiService = ServiceGenerator.createService();
    private LayoutSearchGifContainerBinding mBinding;
    private TenorGifAdapter mTenorGifAdapter;
    private CompositeDisposable mCompositeDisposable;
    private com.tookitup.tenorgif.listener.OnClickListener<BaseGifModel> mBaseGifModelOnClickListener;

    public SearchGifContainer(Context context) {
        super(context);
        init();
    }

    public SearchGifContainer(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SearchGifContainer(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public SearchGifContainer(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.layout_search_gif_container,
                this,
                true);

        mBinding.editText.addTextChangedListener(new DelayWatcher(true) {
            @Override
            public void onTextChanged(String text) {
                if (TextUtils.isEmpty(text)) {
                    requestTrendingGif();
                } else {
                    searchGif(text);
                }
            }
        });
    }

    public void loadTrendingGifList(com.tookitup.tenorgif.listener.OnClickListener<BaseGifModel> listener) {
        mBaseGifModelOnClickListener = listener;
        setVisibility(VISIBLE);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

        showKeyboard();
        requestTrendingGif();
    }

    public void onBackPressClickListener(final OnClickListener onClickListener) {
        mBinding.backPressTextView.setOnClickListener(v -> {
            clearData();
            onClickListener.onClick(v);
        });
    }

    public void clearDisposable() {
        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
        }
    }

    public void addDisposable(Disposable disposable) {
        if (mCompositeDisposable == null) {
            mCompositeDisposable = new CompositeDisposable();
        }
        mCompositeDisposable.add(disposable);
    }

    public void clearData() {
        setVisibility(GONE);
        mBinding.editText.setText("");
        mTenorGifAdapter = null;
        mBinding.progressBar.setVisibility(GONE);
        mBinding.recyclerView.setVisibility(GONE);
        mBinding.recyclerView.setAdapter(null);
        clearDisposable();
    }

    private void requestTrendingGif() {
        mBinding.progressBar.setVisibility(VISIBLE);
        Observable<TenorResult> observable = mApiService.getTrendingList(getContext().getString(R.string.tenor_key),
                CONTENT_FILTER,
                MEDIA_FILTER,
                50);
        addDisposable(observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(tenorResult -> {
                    mBinding.progressBar.setVisibility(GONE);
                    mBinding.recyclerView.setVisibility(VISIBLE);
                    if (tenorResult.getResults() != null && !tenorResult.getResults().isEmpty()) {
                        checkToHideErrorText();
                        mTenorGifAdapter = new TenorGifAdapter(new ArrayList<>(tenorResult.getResults()), this);
                        mBinding.recyclerView.setAdapter(mTenorGifAdapter);
                    } else {
                        showErrorTextView();
                    }
                }, throwable -> {
                    mBinding.progressBar.setVisibility(GONE);
                    showErrorTextView();
                }));
    }

    private void searchGif(String query) {
        Observable<TenorResult> observable = mApiService.searchGif(getContext().getString(R.string.tenor_key),
                query,
                CONTENT_FILTER,
                MEDIA_FILTER,
                50);
        addDisposable(observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(tenorResult -> {
                    if (tenorResult.getResults() != null && !tenorResult.getResults().isEmpty()) {
                        checkToHideErrorText();
                        mTenorGifAdapter = new TenorGifAdapter(tenorResult.getResults(), this);
                        mBinding.recyclerView.setAdapter(mTenorGifAdapter);
                    } else {
                        showErrorTextView();
                    }
                }, throwable -> showErrorTextView()));
    }

    private void showKeyboard() {
        mBinding.editText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null)
            imm.showSoftInput(mBinding.editText, InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    public void onClick(Result result) {
        clearData();
        if (mBaseGifModelOnClickListener != null) {
            mBaseGifModelOnClickListener.onClick(result);
        }
    }

    private void checkToHideErrorText() {
        if (mBinding.errorTextView.getVisibility() == VISIBLE) {
            mBinding.errorTextView.setVisibility(GONE);
            mBinding.recyclerView.setVisibility(VISIBLE);
        }
    }

    private void showErrorTextView() {
        mBinding.recyclerView.setVisibility(GONE);
        mBinding.errorTextView.setVisibility(VISIBLE);
    }
}
