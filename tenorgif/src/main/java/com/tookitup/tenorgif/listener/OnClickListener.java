package com.tookitup.tenorgif.listener;

/**
 * Created by nuonveyo on 10/16/18.
 */

public interface OnClickListener<T> {
    void onClick(T t);
}
