package com.tookitup.tenorgif.viewmodel;

import android.view.View;

import com.tookitup.tenorgif.listener.OnClickListener;
import com.tookitup.tenorgif.model.Result;

/**
 * Created by nuonveyo on 10/16/18.
 */

public class ListItemTenorGifViewModel {
    private Result mResult;
    private OnClickListener<Result> mBaseGifModelOnClickListener;

    public ListItemTenorGifViewModel(Result result, OnClickListener<Result> baseGifModelOnClickListener) {
        mResult = result;
        mBaseGifModelOnClickListener = baseGifModelOnClickListener;
    }

    public String getGifUrl() {
        return mResult.getMedia().get(0).getTinyGif().getUrl();
    }

    public int getGifWidth() {
        long width = mResult.getMedia().get(0).getTinyGif().getDims().get(0);
        return (int) width;
    }

    public int getGifHeight() {
        long height = mResult.getMedia().get(0).getTinyGif().getDims().get(1);
        return (int) height;
    }

    public void onItemClick() {
        mBaseGifModelOnClickListener.onClick(mResult);
    }

    public View getProgressBar(View view) {
        return view;
    }
}
