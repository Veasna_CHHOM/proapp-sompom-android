package com.tookitup.tenorgif.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tookitup.tenorgif.BR;
import com.tookitup.tenorgif.R;
import com.tookitup.tenorgif.adapter.viewholder.BindingViewHolder;
import com.tookitup.tenorgif.listener.OnClickListener;
import com.tookitup.tenorgif.model.BaseGifModel;
import com.tookitup.tenorgif.model.Medium;
import com.tookitup.tenorgif.model.Result;
import com.tookitup.tenorgif.viewmodel.ListItemTenorGifViewModel;

import java.util.List;

/**
 * Created by nuonveyo on 10/16/18.
 */

public class TenorGifAdapter extends RecyclerView.Adapter<BindingViewHolder> {
    private List<Result> mResults;
    private OnClickListener<Result> mBaseGifModelOnClickListener;

    public TenorGifAdapter(List<Result> results, OnClickListener<Result> listener) {
        mResults = results;
        mBaseGifModelOnClickListener = listener;
    }

    @NonNull
    @Override
    public BindingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BindingViewHolder.Builder(parent, R.layout.list_item_tenor_gif).build();
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolder holder, int position) {
        ListItemTenorGifViewModel viewModel = new ListItemTenorGifViewModel(mResults.get(position), mBaseGifModelOnClickListener);
        holder.setVariable(BR.viewModel, viewModel);
    }

    @Override
    public int getItemCount() {
        return mResults.size();
    }

    public void setData(List<Result> list) {
        mResults.clear();
        mResults.addAll(list);
        notifyDataSetChanged();
    }
}
