package com.tookitup.tenorgif.adapter.viewholder;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by nuonveyo on 10/16/18.
 */

public class BindingViewHolder extends RecyclerView.ViewHolder {
    private ViewDataBinding mBinding;

    public BindingViewHolder(ViewDataBinding itemView) {
        super(itemView.getRoot());
        mBinding = itemView;
    }

    public ViewDataBinding getBinding() {
        return mBinding;
    }

    public void setVariable(int variableId, Object value) {
        getBinding().setVariable(variableId, value);
        getBinding().executePendingBindings();
    }

    public Context getContext() {
        return getBinding().getRoot().getContext();
    }

    public static class Builder {
        private ViewGroup mParent;
        @LayoutRes
        private int mLayoutRes;

        public Builder(ViewGroup parent, @LayoutRes int layoutRes) {
            mParent = parent;
            mLayoutRes = layoutRes;
        }

        public BindingViewHolder build() {
            final LayoutInflater inflater = LayoutInflater.from(mParent.getContext());
            final ViewDataBinding binding = DataBindingUtil.inflate(inflater, mLayoutRes, mParent, false);
            return new BindingViewHolder(binding);
        }
    }
}
