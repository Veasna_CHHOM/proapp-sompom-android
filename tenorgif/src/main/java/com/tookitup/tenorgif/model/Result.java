
package com.tookitup.tenorgif.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Result extends BaseGifModel {

    @SerializedName("composite")
    private Object mComposite;
    @SerializedName("created")
    private Double mCreated;
    @SerializedName("hasaudio")
    private Boolean mHasaudio;
    @SerializedName("id")
    private String mId;
    @SerializedName("itemurl")
    private String mItemurl;
    @SerializedName("media")
    private List<Medium> mMedia;
    @SerializedName("shares")
    private Long mShares;
    @SerializedName("tags")
    private List<Object> mTags;
    @SerializedName("title")
    private String mTitle;
    @SerializedName("url")
    private String mUrl;

    public Object getComposite() {
        return mComposite;
    }

    public void setComposite(Object composite) {
        mComposite = composite;
    }

    public Double getCreated() {
        return mCreated;
    }

    public void setCreated(Double created) {
        mCreated = created;
    }

    public Boolean getHasaudio() {
        return mHasaudio;
    }

    public void setHasaudio(Boolean hasaudio) {
        mHasaudio = hasaudio;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getItemurl() {
        return mItemurl;
    }

    public void setItemurl(String itemurl) {
        mItemurl = itemurl;
    }

    public List<Medium> getMedia() {
        return mMedia;
    }

    public void setMedia(List<Medium> media) {
        mMedia = media;
    }

    public Long getShares() {
        return mShares;
    }

    public void setShares(Long shares) {
        mShares = shares;
    }

    public List<Object> getTags() {
        return mTags;
    }

    public void setTags(List<Object> tags) {
        mTags = tags;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    @Override
    public String getGifUrl() {
        return getMedia().get(0).getGif().getUrl();
    }

    @Override
    public GifType getGifType() {
        return GifType.Tenor;
    }

    @Override
    public int getGifWidth() {
        long width = getMedia().get(0).getGif().getDims().get(0);
        return (int) width;
    }

    @Override
    public int getGifHeight() {
        long width = getMedia().get(0).getGif().getDims().get(1);
        return (int) width;
    }
}
