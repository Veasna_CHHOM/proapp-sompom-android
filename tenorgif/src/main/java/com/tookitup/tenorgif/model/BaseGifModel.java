package com.tookitup.tenorgif.model;

/**
 * Created by nuonveyo on 10/16/18.
 */

public abstract class BaseGifModel {
    public abstract String getGifUrl();

    public abstract int getGifWidth();

    public abstract int getGifHeight();

    public abstract GifType getGifType();
}
