package com.tookitup.tenorgif.service;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by nuonveyo on 10/5/18.
 */

public final class ServiceGenerator {
    private static final String BASE_URL = "https://api.tenor.com/v1/";
    private static final Retrofit.Builder mBuilder = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create());

    private static Retrofit mRetrofit = mBuilder.build();

    public static ApiService createService() {
        return mRetrofit.create(ApiService.class);
    }
}
