package com.tookitup.tenorgif.service;

import com.tookitup.tenorgif.model.TenorResult;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by nuonveyo on 10/5/18.
 */

public interface ApiService {
    @GET("search")
    Observable<TenorResult> searchGif(@Query("key") String key,
                                      @Query("q") String query,
                                      @Query("contentfilter") String contentFilter,
                                      @Query("media_filter") String mediaFilter,
                                      @Query("limit") int limit);

    @GET("trending")
    Observable<TenorResult> getTrendingList(@Query("key") String key,
                                            @Query("contentfilter") String contentFilter,
                                            @Query("media_filter") String mediaFilter,
                                            @Query("limit") int limit);
}
