package com.tookitup.tenorgif.utils;

import android.databinding.BindingAdapter;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.tookitup.tenorgif.R;

/**
 * Created by nuonveyo on 10/16/18.
 */

public final class BindingUtil {
    private BindingUtil() {
    }

    @BindingAdapter({"setImageView", "setGifWidth", "setGifHeight", "setViewLoading"})
    public static void setImageView(ImageView imageView, String gifUrl, int width, int height, View viewLoading) {
        viewLoading.setVisibility(View.VISIBLE);
        Glide.with(imageView.getContext())
                .asGif()
                .load(gifUrl)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.DATA))
                .listener(new RequestListener<GifDrawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<GifDrawable> target, boolean isFirstResource) {
                        viewLoading.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GifDrawable resource, Object model, Target<GifDrawable> target, DataSource dataSource, boolean isFirstResource) {
                        viewLoading.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(imageView);
        int imageHeight = imageView.getContext().getResources().getDimensionPixelSize(R.dimen.image_view_height);
        imageView.getLayoutParams().width = imageHeight * width / height;
        imageView.getLayoutParams().height = imageHeight;
    }

    @BindingAdapter("android:visibility")
    public static void setVisibility(View view, boolean isShow) {
        if (isShow) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.GONE);

        }
    }
}
