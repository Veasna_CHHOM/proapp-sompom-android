package com.sompom.baseactivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import java.util.Random;

/**
 * Created by He Rotha on 10/17/17.
 */

public class ResultPermissionActivity extends AppCompatActivity {
    private Random mRandom;
    private ResultCallback mActivityCallback;
    private PermissionCheckHelper mPermissionCheckHelper;

    public void startActivityForResult(Intent intent,
                                       ResultCallback callback) {
        callback.setRequestCode(getMinMaxRandom());
        mActivityCallback = callback;
        startActivityForResult(intent, callback.getRequestCode());
    }

    @SuppressLint("RestrictedApi")
    public void startActivityForResult(Intent intent,
                                       ResultCallback callback,
                                       Bundle bundle) {
        callback.setRequestCode(getMinMaxRandom());
        mActivityCallback = callback;
        startActivityForResult(intent, callback.getRequestCode(), bundle);
    }

    public void checkPermission(@Nullable PermissionCheckHelper.OnPermissionCallback callback, String... permission) {
        mPermissionCheckHelper = new PermissionCheckHelper(this, getMinMaxRandom());
        mPermissionCheckHelper.setPermissions(permission);
        mPermissionCheckHelper.setCallback(callback);
        mPermissionCheckHelper.execute();
    }

    private int getMinMaxRandom() {
        int min = 99;
        int max = 9999;
        if (mRandom == null) {
            mRandom = new Random();
        }
        return mRandom.nextInt(max + 1 - min) + min;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                finishAfterTransition();
            } else {
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mActivityCallback != null && mActivityCallback.getRequestCode() == requestCode) {
            if (resultCode == RESULT_OK) {
                mActivityCallback.onActivityResultSuccess(resultCode, data);
            } else {
                mActivityCallback.onActivityResultFail();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (mPermissionCheckHelper != null) {
            mPermissionCheckHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
