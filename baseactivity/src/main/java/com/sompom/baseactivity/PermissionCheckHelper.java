package com.sompom.baseactivity;

import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by he.rotha on 5/31/16.
 */
public class PermissionCheckHelper {

    private OnPermissionCallback mListener;
    private final AppCompatActivity mAppCompatActivity;
    private final List<String> mPermission = new ArrayList<>();
    private final int mRequestCode;

    public PermissionCheckHelper(AppCompatActivity appCompatActivity,
                                 String permission,
                                 int requestCode) {
        mAppCompatActivity = appCompatActivity;
        mPermission.add(permission);
        mRequestCode = requestCode;
    }

    PermissionCheckHelper(AppCompatActivity appCompatActivity,
                                 int requestCode) {
        mAppCompatActivity = appCompatActivity;
        mRequestCode = requestCode;
    }

    public void setPermissions(String... value) {
        mPermission.addAll(Arrays.asList(value));
    }

    public void setCallback(OnPermissionCallback listener) {
        mListener = listener;
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == mRequestCode) {
            if (grantResults.length > 0) {
                boolean isUserPressNeverAskAgain = false;
                List<String> nonGrantedPermission = new ArrayList<>();
                List<String> grantPermission = new ArrayList<>();

                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        nonGrantedPermission.add(mPermission.get(i));
                        if (!isUserPressNeverAskAgain) {
                            isUserPressNeverAskAgain = !ActivityCompat.shouldShowRequestPermissionRationale(mAppCompatActivity, mPermission.get(i));
                        }
                    } else {
                        grantPermission.add(mPermission.get(i));
                    }
                }
                if (nonGrantedPermission.isEmpty()) {
                    if (mListener != null) {
                        mListener.onPermissionGranted();
                    }
                } else {
                    String[] granted = new String[grantPermission.size()];
                    grantPermission.toArray(granted);

                    String[] denied = new String[nonGrantedPermission.size()];
                    grantPermission.toArray(denied);
                    if (mListener != null) {
                        mListener.onPermissionDenied(granted, denied, isUserPressNeverAskAgain);
                    }
                }
            } else {
                String[] denied = new String[mPermission.size()];
                mPermission.toArray(denied);
                if (mListener != null) {
                    mListener.onPermissionDenied(new String[]{}, denied, true);
                }
            }
        }
    }

    public void execute() {
        for (int i = mPermission.size() - 1; i >= 0; i--) {
            if (isPermissionGranted(mPermission.get(i))) {
                mPermission.remove(i);
            }
        }

        if (mPermission.isEmpty()) {
            if (mListener != null) {
                mListener.onPermissionGranted();
            }
        } else {
            String[] permissions = new String[mPermission.size()];
            permissions = mPermission.toArray(permissions);

            requestPermission(permissions, mRequestCode);
        }
    }

    private boolean isPermissionGranted(String permission) {
        return ActivityCompat.checkSelfPermission(mAppCompatActivity, permission) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission(String[] permissions, int requestCode) {
        ActivityCompat.requestPermissions(mAppCompatActivity, permissions, requestCode);
    }

    public interface OnPermissionCallback {
        void onPermissionGranted();

        void onPermissionDenied(String[] grantedPermission,
                                String[] deniedPermission,
                                boolean isUserPressNeverAskAgain);
    }
}
