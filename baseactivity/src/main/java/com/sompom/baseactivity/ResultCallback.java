package com.sompom.baseactivity;

import android.content.Intent;

/**
 * Created by imac on 7/31/17.
 */

public abstract class ResultCallback {
    private int mRequestCode;

    public int getRequestCode() {
        return mRequestCode;
    }

    public void setRequestCode(int requestCode) {
        mRequestCode = requestCode;
    }

    public abstract void onActivityResultSuccess(int resultCode, Intent data);

    public void onActivityResultFail() {

    }
}
