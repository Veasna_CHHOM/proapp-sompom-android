package com.desmond.squarecamera.listeners;

import com.desmond.squarecamera.model.FlashMode;
import com.desmond.squarecamera.widget.CameraTabLayout;

/**
 * Created by He Rotha on 8/9/18.
 */
public interface OnCameraViewModelListener {
    void onCaptureClick(CameraTabLayout.TabIndex index);

    void onGalleryClick();

    void onFlashClick(FlashMode flashMode);
}
