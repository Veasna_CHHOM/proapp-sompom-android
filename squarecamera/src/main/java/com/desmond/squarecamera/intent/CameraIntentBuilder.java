package com.desmond.squarecamera.intent;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by He Rotha on 10/19/18.
 */
public class CameraIntentBuilder implements Parcelable {

    private FirstOpen mOpenType;

    private Context mContext;

    public CameraIntentBuilder(Context context) {
        mContext = context;
    }

    public CameraIntentBuilder openType(FirstOpen openType) {
        mOpenType = openType;
        return this;
    }


    public FirstOpen getOpenType() {
        return mOpenType;
    }



    public Context getContext() {
        return mContext;
    }

    public CameraIntent build() {
        return new CameraIntent(mContext, this);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mOpenType, flags);
    }

    protected CameraIntentBuilder(Parcel in) {
        this.mOpenType = in.readParcelable(FirstOpen.class.getClassLoader());
    }

    public static final Creator<CameraIntentBuilder> CREATOR = new Creator<CameraIntentBuilder>() {
        @Override
        public CameraIntentBuilder createFromParcel(Parcel source) {
            return new CameraIntentBuilder(source);
        }

        @Override
        public CameraIntentBuilder[] newArray(int size) {
            return new CameraIntentBuilder[size];
        }
    };
}
