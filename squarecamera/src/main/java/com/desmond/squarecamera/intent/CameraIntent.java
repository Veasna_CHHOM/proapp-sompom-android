package com.desmond.squarecamera.intent;

import android.content.Context;
import android.content.Intent;

import com.desmond.squarecamera.model.MediaFile;
import com.desmond.squarecamera.ui.CameraActivity;
import com.desmond.squarecamera.utils.Keys;

import java.util.List;

/**
 * Created by he.rotha on 6/23/16.
 */
public class CameraIntent extends Intent {

    public CameraIntent(Intent o) {
        super(o);
    }

    CameraIntent(Context context, CameraIntentBuilder o) {
        super(context, CameraActivity.class);
        putExtra(Keys.DATA, o);
    }

    public CameraIntentBuilder getBuilder() {
        return getParcelableExtra(Keys.DATA);
    }

    public List<MediaFile> getMediaFiles() {
        return getParcelableArrayListExtra(Keys.PARAM);
    }

}