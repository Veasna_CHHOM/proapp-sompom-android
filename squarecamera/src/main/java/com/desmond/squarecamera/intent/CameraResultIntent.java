package com.desmond.squarecamera.intent;

import android.content.Intent;
import android.os.Parcelable;

import com.desmond.squarecamera.model.MediaFile;
import com.desmond.squarecamera.utils.Keys;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by he.rotha on 6/23/16.
 */
public class CameraResultIntent extends Intent {

    public CameraResultIntent(List<MediaFile> mediaFile) {
        putParcelableArrayListExtra(Keys.PARAM, new ArrayList<Parcelable>(mediaFile));
    }

    public CameraResultIntent(Intent o) {
        super(o);
    }


    public List<MediaFile> getMediaFiles() {
        return getParcelableArrayListExtra(Keys.PARAM);
    }

}