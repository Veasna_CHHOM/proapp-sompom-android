package com.desmond.squarecamera.viewmodel;

import android.content.Context;
import android.databinding.ObservableInt;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Toast;

import com.desmond.squarecamera.R;
import com.desmond.squarecamera.helper.GalleryLoader;
import com.desmond.squarecamera.intent.CameraIntentBuilder;
import com.desmond.squarecamera.intent.CameraType;
import com.desmond.squarecamera.intent.GalleryType;
import com.desmond.squarecamera.listeners.GalleryLoaderListener;
import com.desmond.squarecamera.listeners.OnGalleryItemClickListener;
import com.desmond.squarecamera.model.MediaFile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by He Rotha on 6/7/18.
 */
public class GalleryFrViewModel implements GalleryLoaderListener, OnGalleryItemClickListener {

    public ObservableInt mVisibleButton = new ObservableInt(View.GONE);
    public ObservableInt mVisibleDesc = new ObservableInt(View.VISIBLE);
    private ArrayList<MediaFile> mMediaFile = new ArrayList<>();
    private ViewModelListener mListener;
    private GalleryType mGalleryType;
    private Context mContext;

    public GalleryFrViewModel(FragmentActivity activity,
                              CameraIntentBuilder cameraIntentBuilder,
                              ViewModelListener listener) {

        mListener = listener;
        mContext = activity;

        if (cameraIntentBuilder.getOpenType() instanceof CameraType) {
            mGalleryType = ((CameraType) cameraIntentBuilder.getOpenType()).getGalleryType();
        } else if (cameraIntentBuilder.getOpenType() instanceof GalleryType) {
            mGalleryType = (GalleryType) cameraIntentBuilder.getOpenType();
        }

        if (mGalleryType == null) {
            throw new RuntimeException("GalleryType is null");
        }

        List<MediaFile> medias = mGalleryType.getMediaFiles();
        if (medias != null && medias.size() > 0) {
            mMediaFile.addAll(medias);
        }

        GalleryLoader galleryLoader = new GalleryLoader(activity, medias, mGalleryType.getLoaderType(), this);
        galleryLoader.execute();

        if (mGalleryType.isShowDescription()) {
            mVisibleDesc.set(View.VISIBLE);
        } else {
            mVisibleDesc.set(View.GONE);
        }
    }

    @Override
    public void onLoadComplete(List<MediaFile> MediaFile) {
        mListener.onLoadComplete(MediaFile);
    }

    public void onBackClick() {
        mListener.onBackClick();
    }

    public void onButtonClick() {
        if (mMediaFile.size() > 0) {
            mListener.onPhotoChoose(mMediaFile);
        }
    }

    @Override
    public void onGalleryItemClick(int position, MediaFile data) {
        mListener.onGalleryItemClick(position);

        boolean isSelect = !data.isSelected();
        if (mGalleryType.getSelectionNumbers() > 1) {
            if (isSelect) {

                if (mMediaFile.size() < mGalleryType.getSelectionNumbers()) {
                    mMediaFile.add(data);
                    for (int i = 0; i < mMediaFile.size(); i++) {
                        MediaFile galleryDatum = mMediaFile.get(i);
                        galleryDatum.setPosition(i);
                        mListener.onItemUpdate(galleryDatum, true);
                    }
                } else {
                    final String text = mContext.getString(R.string.gallery_select_max, mGalleryType.getSelectionNumbers());
                    Toast.makeText(mContext, text, Toast.LENGTH_SHORT).show();
                }
            } else {
                mMediaFile.remove(data);
                mListener.onItemUpdate(data, false);
                for (int i = 0; i < mMediaFile.size(); i++) {
                    MediaFile galleryDatum = mMediaFile.get(i);
                    galleryDatum.setPosition(i);
                    mListener.onItemUpdate(galleryDatum, true);
                }
            }

        } else {
            data.setSelected(isSelect);

            for (MediaFile galleryDatum : mMediaFile) {
                mListener.onItemUpdate(galleryDatum, false);
            }
            mMediaFile.clear();

            if (data.isSelected()) {
                mMediaFile.add(data);
                mListener.onItemUpdate(data, true);
            }
        }
        if (mMediaFile.size() > 0) {
            mVisibleButton.set(View.VISIBLE);
        } else {
            mVisibleButton.set(View.INVISIBLE);
        }
    }

    @Override
    public void onGalleryAddClick() {
        mListener.onCameraClick();
    }

    public interface ViewModelListener extends GalleryLoaderListener {
        void onItemUpdate(MediaFile MediaFile, boolean isSelect);

        void onPhotoChoose(List<MediaFile> MediaFile);

        void onCameraClick();

        void onBackClick();

        void onGalleryItemClick(int position);
    }
}
