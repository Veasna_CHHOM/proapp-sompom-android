package com.desmond.squarecamera.viewmodel.binding;

import android.databinding.BindingAdapter;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.desmond.squarecamera.R;
import com.desmond.squarecamera.model.FlashMode;
import com.desmond.squarecamera.widget.ButtonCapture;
import com.desmond.squarecamera.widget.CameraTabLayout;

import java.io.File;

/**
 * Created by He Rotha on 8/9/18.
 */
public final class BindingUtil {
    private BindingUtil() {
    }

    @BindingAdapter("visible")
    public static void visibleGone(View view, boolean isVisible) {
        if (isVisible) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.GONE);
        }
    }

    @BindingAdapter("hideControl")
    public static void hideControl(View view, boolean isVisible) {
        if (isVisible) {
            view.setVisibility(View.INVISIBLE);
        }
    }

    @BindingAdapter("animate")
    public static void animate(final View view, boolean isVisible) {
        if (isVisible) {
            view.setAlpha(0f);
            view.setVisibility(View.VISIBLE);
            view.animate().alpha(1f).setDuration(500).start();
        } else {
            view.setAlpha(1f);
            view.setVisibility(View.INVISIBLE);
            view.animate().alpha(0f).setDuration(500).start();
        }
    }

    @BindingAdapter("visibility")
    public static void visibility(View view, int isVisible) {
        view.setVisibility(isVisible);
    }

    @BindingAdapter("setButtonMode")
    public static void setButtonMode(ButtonCapture view, CameraTabLayout.TabIndex tabIndex) {
        view.setTabIndex(tabIndex);
    }

    @BindingAdapter("android:text")
    public static void text(TextView view, FlashMode flashMode) {
        if (flashMode == null) {
            return;
        }
        view.setText(flashMode.getText());
    }

    @BindingAdapter("loadGallery")
    public static void loadGallery(ImageView imageView, String mediaFile) {
        if (TextUtils.isEmpty(mediaFile)) {
            return;
        }

        if (!mediaFile.startsWith("http")) {
            if (mediaFile.endsWith("gif") || mediaFile.endsWith("GIF")) {
                Glide.with(imageView.getContext())
                        .asGif()
                        .load(new File(mediaFile))
                        .into(imageView);
            } else {
                Glide.with(imageView.getContext())
                        .load(new File(mediaFile))
                        .apply(new RequestOptions()
                                .centerCrop()
                                .dontAnimate()
                                .override(300, 300)
                                .placeholder(R.drawable.ic_photo_placeholder_200dp))
                        .into(imageView);
            }
        } else {
            Glide.with(imageView.getContext())
                    .load(mediaFile)
                    .apply(new RequestOptions()
                            .centerCrop()
                            .dontAnimate()
                            .override(300, 300)
                            .placeholder(R.drawable.ic_photo_placeholder_200dp))
                    .into(imageView);

        }
    }

    @BindingAdapter("loadThumbnail")
    public static void loadThumbnail(final ImageView imageView, final String mediaFile) {
        if (TextUtils.isEmpty(mediaFile)) {
            return;
        }
        Glide.with(imageView.getContext())
                .load(new File(mediaFile))
                .apply(new RequestOptions()
                        .centerCrop()
                        .dontAnimate()
                        .override(300, 300)
                        .placeholder(R.drawable.ic_photo_placeholder_200dp))
                .into(imageView);
    }
}
