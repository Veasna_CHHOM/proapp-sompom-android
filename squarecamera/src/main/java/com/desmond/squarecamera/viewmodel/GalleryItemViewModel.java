package com.desmond.squarecamera.viewmodel;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.desmond.squarecamera.R;
import com.desmond.squarecamera.intent.CameraIntentBuilder;
import com.desmond.squarecamera.intent.GalleryType;
import com.desmond.squarecamera.listeners.OnGalleryItemClickListener;
import com.desmond.squarecamera.model.MediaFile;

/**
 * Created by He Rotha on 6/7/18.
 */
public class GalleryItemViewModel {
    public ObservableField<String> mPath = new ObservableField<>();
    public ObservableBoolean mIsSelect = new ObservableBoolean();
    public ObservableBoolean mIsShowText = new ObservableBoolean();
    public ObservableField<String> mIndex = new ObservableField<>();
    private MediaFile mMediaFile;
    private OnGalleryItemClickListener mListener;
    private int mPosition;

    public GalleryItemViewModel(Context context,
                                GalleryType sourceType,
                                MediaFile mediaFile,
                                int position,
                                OnGalleryItemClickListener listener) {
        mMediaFile = mediaFile;
        mListener = listener;
        mPath.set(mediaFile.getPath());
        mPosition = position;
        mIsSelect.set(mediaFile.isSelected());

        if (sourceType.getSelectionNumbers() > 1) {
            mIsShowText.set(mediaFile.isSelected());
            if (mediaFile.isSelected()) {
                if (mediaFile.getPosition() == 0 && sourceType.isFirstIndexIsCover()) {
                    mIndex.set(context.getString(R.string.gallery_cover));
                } else {
                    mIndex.set(String.valueOf(mediaFile.getPosition() + 1));
                }
            }
        } else {
            mIsShowText.set(false);
            mIndex.set(null);
        }
    }

    public void onItemClick() {
        mListener.onGalleryItemClick(mPosition, mMediaFile);
    }
}
