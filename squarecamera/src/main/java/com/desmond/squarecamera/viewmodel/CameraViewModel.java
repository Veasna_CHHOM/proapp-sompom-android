package com.desmond.squarecamera.viewmodel;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.hardware.Camera;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.desmond.squarecamera.helper.GalleryLoader;
import com.desmond.squarecamera.intent.CameraIntentBuilder;
import com.desmond.squarecamera.intent.CameraType;
import com.desmond.squarecamera.listeners.GalleryLoaderListener;
import com.desmond.squarecamera.listeners.OnCameraViewModelListener;
import com.desmond.squarecamera.model.FlashMode;
import com.desmond.squarecamera.model.MediaFile;
import com.desmond.squarecamera.widget.CameraTabLayout;

import java.util.List;

/**
 * Created by He Rotha on 8/9/18.
 */
public class CameraViewModel implements CameraTabLayout.OnTabChangeListener {
    public ObservableInt mIsShowDescription = new ObservableInt(View.GONE);
    public ObservableInt mIsShowLiveControl = new ObservableInt(View.INVISIBLE);
    public ObservableInt mIsShowButtonGallery = new ObservableInt(View.GONE);
    public ObservableBoolean mIsShowLiveStatus = new ObservableBoolean();
    public ObservableBoolean mHideControlIfVideoRecording = new ObservableBoolean();
    public ObservableField<FlashMode> mFlashMode = new ObservableField<>();
    public ObservableField<CameraTabLayout.TabIndex> mTabIndex = new ObservableField<>();
    public ObservableField<String> mLatestVideoOrImage = new ObservableField<>();

    private OnCameraViewModelListener mOnCameraViewModelListener;
    private FlashMode mTempFlash;
    private boolean mIsFlashClickable = true;

    public CameraViewModel(FragmentActivity activity,
                           CameraIntentBuilder builder,
                           FlashMode flashMode,
                           OnCameraViewModelListener modelListener) {
        mFlashMode.set(flashMode);
        mTempFlash = flashMode;
        mOnCameraViewModelListener = modelListener;

        boolean isShowButtonGallery = false;
        boolean isShowDescription = false;
        boolean isShowLive = false;

        if (builder.getOpenType() instanceof CameraType) {
            isShowButtonGallery = ((CameraType) builder.getOpenType()).getGalleryType() != null;
            isShowDescription = ((CameraType) builder.getOpenType()).isShowDescription();
            isShowLive = ((CameraType) builder.getOpenType()).getTabIndex().contains(CameraTabLayout.TabIndex.LIVE);
        }
        if (isShowButtonGallery) {
            mIsShowButtonGallery.set(View.VISIBLE);
        } else {
            mIsShowButtonGallery.set(View.GONE);
        }

        if (isShowDescription) {
            mIsShowDescription.set(View.VISIBLE);
        } else {
            mIsShowDescription.set(View.GONE);
        }

        if (isShowLive) {
            mIsShowLiveControl.set(View.GONE);
        } else {
            mIsShowLiveControl.set(View.INVISIBLE);
        }
        GalleryLoader galleryLoader = new GalleryLoader(activity, new GalleryLoaderListener() {
            @Override
            public void onLoadComplete(List<MediaFile> galleryData) {
                if (galleryData.size() > 0) {
                    mLatestVideoOrImage.set(galleryData.get(0).getPath());
                }
            }
        });
        galleryLoader.execute();
    }

    public void enableFlash(boolean isEnable) {
        mIsFlashClickable = isEnable;
        if (isEnable) {
            mFlashMode.set(mTempFlash);
        } else {
            mTempFlash = mFlashMode.get();
            mFlashMode.set(FlashMode.FlashOff);
        }
    }

    @Override
    public void onTabSelect(CameraTabLayout.TabIndex tabIndex) {
        mTabIndex.set(tabIndex);

        if (tabIndex == CameraTabLayout.TabIndex.LIVE) {
            mIsShowLiveControl.set(View.VISIBLE);
        } else {
            mIsShowLiveControl.set(View.INVISIBLE);
        }
    }

    public void onCaptureClick() {
        mIsShowLiveStatus.set(mTabIndex.get() == CameraTabLayout.TabIndex.LIVE);
        mHideControlIfVideoRecording.set(mTabIndex.get() == CameraTabLayout.TabIndex.VIDEO);
        mOnCameraViewModelListener.onCaptureClick(mTabIndex.get());
    }

    public void onGalleryClick() {
        mOnCameraViewModelListener.onGalleryClick();
    }

    public void onFlashClick() {
        if (mIsFlashClickable) {
            if (Camera.Parameters.FLASH_MODE_AUTO.equalsIgnoreCase(mFlashMode.get().getValue())) {
                mFlashMode.set(FlashMode.FlashOn);
            } else if (Camera.Parameters.FLASH_MODE_ON.equalsIgnoreCase(mFlashMode.get().getValue())) {
                mFlashMode.set(FlashMode.FlashOff);
            } else if (Camera.Parameters.FLASH_MODE_OFF.equalsIgnoreCase(mFlashMode.get().getValue())) {
                mFlashMode.set(FlashMode.FlashAuto);
            }
            mTempFlash = mFlashMode.get();
            mOnCameraViewModelListener.onFlashClick(mFlashMode.get());
        }
    }
}
