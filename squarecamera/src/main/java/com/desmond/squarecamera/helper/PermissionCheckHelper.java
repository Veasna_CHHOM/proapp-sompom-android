package com.desmond.squarecamera.helper;

import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by he.rotha on 5/31/16.
 */
public class PermissionCheckHelper {

    private OnPermissionCallback mListener;
    private final AppCompatActivity mAppCompatActivity;
    private final List<String> mPermission;
    private final int mRequestCode;

    public PermissionCheckHelper(AppCompatActivity appCompatActivity,
                                 String permission,
                                 int requestCode) {
        mAppCompatActivity = appCompatActivity;
        mPermission = new ArrayList<>();
        mPermission.add(permission);
        mRequestCode = requestCode;
    }

    public PermissionCheckHelper(AppCompatActivity appCompatActivity,
                                 int requestCode) {
        mAppCompatActivity = appCompatActivity;
        mPermission = new ArrayList<>();
        mRequestCode = requestCode;
    }

    public void setPermissions(String... value) {
        mPermission.addAll(Arrays.asList(value));
    }

    public void setCallback(OnPermissionCallback listener) {
        mListener = listener;
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == mRequestCode) {
            if (grantResults.length > 0) {
                boolean isUserPressNeverAskAgain = false;
                List<String> nonGrantedPermission = new ArrayList<>();

                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        nonGrantedPermission.add(mPermission.get(i));
                        if (!isUserPressNeverAskAgain) {
                            isUserPressNeverAskAgain = !ActivityCompat.shouldShowRequestPermissionRationale(mAppCompatActivity, mPermission.get(i));
                        }
                    }
                }
                if (nonGrantedPermission.isEmpty()) {
                    mListener.onPermissionGranted();
                } else {
                    mListener.onPermissionDeny(isUserPressNeverAskAgain);
                }
            } else {
                mListener.onPermissionDeny(true);
            }
        }
    }

    public void execute() {

        for (int i = mPermission.size() - 1; i >= 0; i--) {
            if (isPermissionGranted(mPermission.get(i))) {
                mPermission.remove(i);
            }
        }

        if (mPermission.isEmpty()) {
            mListener.onPermissionGranted();
        } else {
            String[] permissions = new String[mPermission.size()];
            permissions = mPermission.toArray(permissions);

            requestPermission(permissions, mRequestCode);
        }
    }

    private boolean isPermissionGranted(String permission) {
        return ActivityCompat.checkSelfPermission(mAppCompatActivity, permission) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission(String[] permissions, int requestCode) {
        ActivityCompat.requestPermissions(mAppCompatActivity, permissions, requestCode);
    }

    public interface OnPermissionCallback {
        void onPermissionGranted();

        void onPermissionDeny(boolean isUserPressNeverAskAgain);
    }
}
