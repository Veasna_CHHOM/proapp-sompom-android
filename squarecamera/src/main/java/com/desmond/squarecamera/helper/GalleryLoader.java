package com.desmond.squarecamera.helper;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;

import com.desmond.squarecamera.listeners.GalleryLoaderListener;
import com.desmond.squarecamera.model.MediaFile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by He Rotha on 6/7/18.
 */
public class GalleryLoader implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final int ORIENTATION_ROTATE_270 = 270;
    private static final int ORIENTATION_ROTATE_90 = 90;
    private static final int sLoaderId = 1;
    private FragmentActivity mContext;
    private LoaderType mLoaderType;
    private GalleryLoaderListener mGalleryLoaderListener;
    private ArrayList<MediaFile> mSelectMedia;
    private boolean mIsQueryOnlyLatestOne;

    /**
     * query all photo/video from phone
     */
    public GalleryLoader(FragmentActivity context,
                         List<MediaFile> selectMedia,
                         LoaderType loaderType,
                         GalleryLoaderListener galleryLoaderListener) {
        mContext = context;
        if (selectMedia != null) {
            mSelectMedia = new ArrayList<>(selectMedia);
        }
        mLoaderType = loaderType;
        mGalleryLoaderListener = galleryLoaderListener;
        mIsQueryOnlyLatestOne = false;
    }

    /**
     * query latest one photo/video from phone
     */
    public GalleryLoader(FragmentActivity context,
                         GalleryLoaderListener galleryLoaderListener) {
        mContext = context;
        mGalleryLoaderListener = galleryLoaderListener;
        mLoaderType = LoaderType.PhotoVideo;
        mIsQueryOnlyLatestOne = true;
    }

    public void execute() {
        mContext.getSupportLoaderManager()
                .initLoader(sLoaderId, null, this);
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        String[] projection = {
                MediaStore.Files.FileColumns._ID,
                MediaStore.Files.FileColumns.DATA,
                MediaStore.Files.FileColumns.MEDIA_TYPE,
                MediaStore.Files.FileColumns.MIME_TYPE,
                MediaStore.Files.FileColumns.TITLE,
                MediaStore.Files.FileColumns.WIDTH,
                MediaStore.Files.FileColumns.HEIGHT,
                MediaStore.Images.Media.ORIENTATION
        };

        String selection;
        if (mLoaderType == LoaderType.PhotoVideo) {
            selection = MediaStore.Files.FileColumns.MEDIA_TYPE + "="
                    + MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE
                    + " OR "
                    + MediaStore.Files.FileColumns.MEDIA_TYPE + "="
                    + MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;
        } else {
            selection = MediaStore.Files.FileColumns.MEDIA_TYPE + "="
                    + MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
        }

        Uri queryUri = MediaStore.Files.getContentUri("external");

        if (mIsQueryOnlyLatestOne) {
            return new CursorLoader(
                    mContext,
                    queryUri,
                    projection,
                    selection,
                    null, // Selection args (none).
                    MediaStore.Files.FileColumns.DATE_MODIFIED + " DESC LIMIT 1"  // Sort order.
            );
        } else {
            return new CursorLoader(
                    mContext,
                    queryUri,
                    projection,
                    selection,
                    null, // Selection args (none).
                    MediaStore.Files.FileColumns.DATE_MODIFIED + " DESC"  // Sort order.
            );
        }
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor) {
        int dataIndex = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.DATA);
        int mimeTypeIndex = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.MIME_TYPE);
        int widthIndex = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.WIDTH);
        int heightIndex = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.HEIGHT);
        int orientationIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.ORIENTATION);

        List<MediaFile> galleryData = new ArrayList<>();
        while (cursor.moveToNext()) {
            String absolutePathOfImage = cursor.getString(dataIndex);
            String mineType = cursor.getString(mimeTypeIndex);
            int width = cursor.getInt(widthIndex);
            int height = cursor.getInt(heightIndex);
            int orientation = cursor.getInt(orientationIndex);

            MediaFile mediaFile = new MediaFile(absolutePathOfImage, mineType);
            if (orientation == ORIENTATION_ROTATE_90 || orientation == ORIENTATION_ROTATE_270) {
                if (width > height) {
                    mediaFile.setWidth(height);
                    mediaFile.setHeight(width);
                } else {
                    mediaFile.setWidth(width);
                    mediaFile.setHeight(height);
                }
            } else {
                mediaFile.setWidth(width);
                mediaFile.setHeight(height);
            }


            if (mSelectMedia != null) {
                for (MediaFile file : mSelectMedia) {
                    if (mediaFile.getPath().equals(file.getPath())) {
                        mediaFile.setSelected(true);
                        mediaFile.setPosition(file.getPosition());
                        mSelectMedia.remove(file);
                        break;
                    }
                }
            }
//            Log.e("rotha", "mineType: " + mineType + " => " + absolutePathOfImage);
            galleryData.add(mediaFile);
        }
        if (mSelectMedia != null) {
            for (MediaFile mediaFile : mSelectMedia) {
                mediaFile.setSelected(true);
            }
            galleryData.addAll(0, mSelectMedia);
        }
        mContext.getSupportLoaderManager().destroyLoader(sLoaderId);
        mGalleryLoaderListener.onLoadComplete(galleryData);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {

    }

    public enum LoaderType {
        PhotoOnly, PhotoVideo
    }
}
