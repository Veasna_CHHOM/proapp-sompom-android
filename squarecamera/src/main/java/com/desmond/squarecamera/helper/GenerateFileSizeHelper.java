package com.desmond.squarecamera.helper;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.util.Log;

import java.io.File;

/**
 * Created by nuonveyo on 1/16/18.
 */

public class GenerateFileSizeHelper {
    private static final String TAG = GenerateFileSizeHelper.class.getName();

    public void getImageSize(Uri uri, GenerateFileSizeHelperListener listener) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        File file = new File(uri.getPath());
        BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;
        Log.e(TAG, "file size: " + file.length());
        if (listener != null) {
            listener.onSuccess(uri, imageWidth, imageHeight);
        }
    }

    public void getVideoSize(Uri uri, GenerateFileSizeHelperListener listener) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        Bitmap bmp = null;
        File file = new File(uri.getPath());
        try {
            retriever.setDataSource(uri.getPath());
            bmp = retriever.getFrameAtTime();
            int videoHeight = bmp.getHeight();
            int videoWidth = bmp.getWidth();

            // Get duration
            String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            long timeInMilliSec = Long.parseLong(time);
            Log.e(TAG, "timeInMilliSec: " + timeInMilliSec);
            Log.e(TAG, "file size: " + file.length());
            if (listener != null) {
                listener.onSuccess(uri, videoWidth, videoHeight);
            }
        } catch (Exception e) {
            Log.e(TAG, "e: " + e.getMessage());
        }
    }

    public interface GenerateFileSizeHelperListener {
        void onSuccess(Uri uri, int width, int height);
    }
}
