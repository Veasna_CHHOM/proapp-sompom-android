package com.desmond.squarecamera.helper;

import android.app.Activity;
import android.support.annotation.StringRes;
import android.view.View;

import com.desmond.squarecamera.R;
import com.desmond.squarecamera.intent.IntentUtil;
import com.desmond.squarecamera.ui.CameraActivity;
import com.desmond.squarecamera.ui.dialog.MessageDialog;

/**
 * Created by nuonveyo
 * on 1/31/19.
 */

public abstract class CheckCameraPermissionCallbackHelper implements PermissionCheckHelper.OnPermissionCallback {
    private final Activity mActivity;

    protected CheckCameraPermissionCallbackHelper(Activity activity) {
        mActivity = activity;
    }

    @Override
    public void onPermissionDeny(boolean isUserPressNeverAskAgain) {
        if (isUserPressNeverAskAgain) {
            MessageDialog dialog = new MessageDialog();
            dialog.setTitle(getString(R.string.camera_access_permission_title));
            dialog.setMessage(getString(R.string.camera_access_camera_description));
            dialog.setLeftText(getString(R.string.camera_access_app_setting_button), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    IntentUtil.openAppSetting(mActivity);
                }
            });
            dialog.setRightText(getString(R.string.camera_no_button), null);
            dialog.setCallback(new MessageDialog.Callback() {
                @Override
                public void onDismiss() {
                    onDismissDialog();
                }
            });
            dialog.show(((CameraActivity) mActivity).getSupportFragmentManager(), MessageDialog.TAG);
        }
    }

    public void onDismissDialog() {

    }

    private String getString(@StringRes int id) {
        return mActivity.getString(id);
    }
}
