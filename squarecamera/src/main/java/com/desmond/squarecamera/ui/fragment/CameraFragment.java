package com.desmond.squarecamera.ui.fragment;


import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Size;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;

import com.desmond.squarecamera.BR;
import com.desmond.squarecamera.R;
import com.desmond.squarecamera.databinding.SquarecameraFragmentCameraBinding;
import com.desmond.squarecamera.helper.CameraOrientationListener;
import com.desmond.squarecamera.helper.SurfaceListener;
import com.desmond.squarecamera.intent.CameraIntentBuilder;
import com.desmond.squarecamera.listeners.OnCameraViewModelListener;
import com.desmond.squarecamera.model.FlashMode;
import com.desmond.squarecamera.model.ImageParameters;
import com.desmond.squarecamera.model.MediaFile;
import com.desmond.squarecamera.model.MediaType;
import com.desmond.squarecamera.ui.CameraActivity;
import com.desmond.squarecamera.ui.dialog.EditPostPrivacyDialog;
import com.desmond.squarecamera.utils.CameraSettingPreferences;
import com.desmond.squarecamera.utils.FormatTime;
import com.desmond.squarecamera.utils.Keys;
import com.desmond.squarecamera.utils.ScreenSize;
import com.desmond.squarecamera.utils.media.FileNameUtil;
import com.desmond.squarecamera.viewmodel.CameraViewModel;
import com.desmond.squarecamera.widget.CameraTabLayout;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CameraFragment extends Fragment implements Camera.PictureCallback {

    public static final String TAG = CameraFragment.class.getSimpleName();
    public static final int MAX_VIDEO_DURATION = 20; //20 seconds
    private int mCameraID;
    private FlashMode mFlashMode;
    private Camera mCamera;
    private SurfaceTexture mSurfaceHolder;
    private boolean mIsSafeToTakePhoto = false;
    private boolean mIsRecording;
    private ImageParameters mImageParameters = new ImageParameters();

    private CameraOrientationListener mOrientationListener;
    private int mScreenWidth;
    private int mScreenHeight;
    private SquarecameraFragmentCameraBinding mBinding;
    private CameraIntentBuilder mCameraIntentBuilder;
    private int mCurrVideoRecTimer = MAX_VIDEO_DURATION;
    private Handler mTimerHandler = new Handler();
    private MediaRecorder mMediaRecorder;
    private String mVideoPath;
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (mCurrVideoRecTimer < 0) {
                takePicture(CameraTabLayout.TabIndex.VIDEO);
            } else {
                mBinding.textViewTime.setText(FormatTime.getFormattedDuration(mCurrVideoRecTimer--));
                mTimerHandler.postDelayed(this, 1000);
            }
        }
    };
    private CameraViewModel mViewModel;
    private int mHeight;
    private int mWidth;

    public static CameraFragment newInstance(CameraIntentBuilder sourceType) {
        Bundle args = new Bundle();
        args.putParcelable(Keys.DATA, sourceType);
        CameraFragment fragment = new CameraFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mOrientationListener = new CameraOrientationListener(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            Point sizeScreen = ScreenSize.getScreenSize(getActivity());
            mScreenWidth = sizeScreen.x;
            mScreenHeight = sizeScreen.y;

            if (savedInstanceState == null) {
                mCameraID = getBackCameraID();
                mFlashMode = FlashMode.fromValue(CameraSettingPreferences.getCameraFlashMode(getActivity()));
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = SquarecameraFragmentCameraBinding.inflate(inflater, container, false);

        if (getActivity() != null) {
            int navigationBarHeight = ScreenSize.getNavigationBarSize(getActivity());

            if (navigationBarHeight > 0) {
                int margin = getActivity().getResources().getDimensionPixelOffset(R.dimen.navigation_bar_margin);
                Log.e(TAG, "navigationBarHeight: " + navigationBarHeight + ", marginSmall: " + margin);

                navigationBarHeight += margin;
                ViewGroup.MarginLayoutParams param = (ViewGroup.MarginLayoutParams) mBinding.tabs.getLayoutParams();
                param.setMargins(0, 0, 0, navigationBarHeight);
                ViewGroup.MarginLayoutParams param1 = (ViewGroup.MarginLayoutParams) mBinding.containerRecordButton.getLayoutParams();
                param1.setMargins(0, 0, 0, navigationBarHeight);
            }
        }
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            mCameraIntentBuilder = getArguments().getParcelable(Keys.DATA);
        }

        mBinding.containerPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditPostPrivacyDialog dialog = EditPostPrivacyDialog.newInstance();
                dialog.setOnCallback(new EditPostPrivacyDialog.OnCallback() {
                    @Override
                    public void onFollowersClick() {
                        if (getActivity() == null) {
                            return;
                        }
                        dialog.dismiss();
                        mBinding.followerIcon.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_followers));
                        mBinding.followerTextView.setText(R.string.post_menu_followers_title);
                    }

                    @Override
                    public void onEveryoneClick() {
                        if (getActivity() == null) {
                            return;
                        }
                        dialog.dismiss();
                        mBinding.followerIcon.setImageDrawable(ContextCompat.getDrawable(getActivity(),
                                R.drawable.ic_earth_grid));
                        mBinding.followerTextView.setText(R.string.post_menu_everyone_title);
                    }
                });
                dialog.show(getChildFragmentManager(), EditPostPrivacyDialog.TAG);
            }
        });


        mViewModel = new CameraViewModel(getActivity(), mCameraIntentBuilder, mFlashMode, new OnCameraViewModelListener() {
            @Override
            public void onCaptureClick(CameraTabLayout.TabIndex index) {
                takePicture(index);
            }

            @Override
            public void onGalleryClick() {
                FragmentManager fragmentManager = getFragmentManager();
                if (fragmentManager != null) {
                    fragmentManager
                            .beginTransaction()
                            .replace(R.id.fragment_container,
                                    GalleryFragment.newInstance(mCameraIntentBuilder),
                                    GalleryFragment.TAG)
                            .addToBackStack(null)
                            .commit();
                }
            }

            @Override
            public void onFlashClick(FlashMode flashMode) {
                mFlashMode = flashMode;
                setupCamera();
            }
        });
        mBinding.setVariable(BR.viewModel, mViewModel);
        mBinding.tabs.setOnTabChangeListener(mViewModel);
        mBinding.tabs.setCameraIntentBuilder(mCameraIntentBuilder);
        mBinding.cameraPreviewView.setSwipeListener(mBinding.tabs.getOnSwipeListener());

        mOrientationListener.enable();

        mBinding.cameraPreviewView.setSurfaceTextureListener(new SurfaceListener() {
            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture) {
                mSurfaceHolder = surfaceTexture;

                getCamera(mCameraID);
                startCameraPreview();
            }
        });

        mImageParameters.mIsPortrait =
                getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;

        mBinding.changeCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCameraID == CameraInfo.CAMERA_FACING_FRONT) {
                    mCameraID = getBackCameraID();
                } else {
                    mCameraID = getFrontCameraID();
                }
                restartPreview();
            }
        });


        mBinding.close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() == null) {
                    return;
                }
                getActivity().finish();
            }
        });
    }

    private void getCamera(int cameraID) {
        try {
            mCamera = Camera.open(cameraID);
            mBinding.cameraPreviewView.setCamera(mCamera);
        } catch (Exception e) {
            Log.e(TAG, "Can't open camera with id " + cameraID + ", Error: " + e.toString());
        }
    }

    /**
     * Restart the camera preview
     */
    private void restartPreview() {
        if (mCamera != null) {
            stopCameraPreview();
            mCamera.release();
            mCamera = null;
        }

        getCamera(mCameraID);
        startCameraPreview();
    }

    /**
     * Start the camera preview
     */
    private void startCameraPreview() {
        if (mCamera == null) {
            return;
        }
        determineDisplayOrientation();
        setupCamera();

        try {
            mCamera.setPreviewTexture(mSurfaceHolder);
            mCamera.startPreview();

            setSafeToTakePhoto(true);
            setCameraFocusReady(true);
        } catch (IOException e) {
            Log.d(TAG, "Can't start camera preview due to IOException " + e.toString());
        }
    }

    /**
     * Stop the camera preview
     */
    private void stopCameraPreview() {
        setSafeToTakePhoto(false);
        setCameraFocusReady(false);

        // Nulls out callbacks, stops face detection
        mCamera.stopPreview();
        mBinding.cameraPreviewView.setCamera(null);
    }

    private void setSafeToTakePhoto(final boolean isSafeToTakePhoto) {
        mIsSafeToTakePhoto = isSafeToTakePhoto;
    }

    private void setCameraFocusReady(final boolean isFocusReady) {
        if (this.mBinding.cameraPreviewView != null) {
            mBinding.cameraPreviewView.setIsFocusReady(isFocusReady);
        }
    }

    /**
     * Determine the current display orientation and rotate the camera preview
     * accordingly
     */
    private void determineDisplayOrientation() {
        CameraInfo cameraInfo = new CameraInfo();
        Camera.getCameraInfo(mCameraID, cameraInfo);

        // Clockwise rotation needed to align the window display to the natural position
        if (getActivity() == null) {
            return;
        }
        int rotation = getActivity().getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;

        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
            default:
                break;
        }

        int displayOrientation;

        // CameraInfo.Orientation is the angle relative to the natural position of the device
        // in clockwise rotation (angle that is rotated clockwise from the natural position)
        if (cameraInfo.facing == CameraInfo.CAMERA_FACING_FRONT) {
            // Orientation is angle of rotation when facing the camera for
            // the camera image to match the natural orientation of the device
            displayOrientation = (cameraInfo.orientation + degrees) % 360;
            displayOrientation = (360 - displayOrientation) % 360;
        } else {
            displayOrientation = (cameraInfo.orientation - degrees + 360) % 360;
        }

        mImageParameters.mDisplayOrientation = displayOrientation;
        mImageParameters.mLayoutOrientation = degrees;
        if (mCamera == null) {
            return;
        }
        mCamera.setDisplayOrientation(mImageParameters.mDisplayOrientation);
    }

    /**
     * Setup the camera parameters
     */
    private void setupCamera() {
        // Never keep a global parameters
        Camera.Parameters parameters = mCamera.getParameters();

        Size bestPreviewSize = determineBestSize(parameters.getSupportedPreviewSizes());
        Size bestPicture = determineBestSize(parameters.getSupportedPictureSizes());
        parameters.setPreviewSize(bestPreviewSize.width, bestPreviewSize.height);
        parameters.setPictureSize(bestPicture.width, bestPicture.height);

        // Set continuous picture focus, if it's supported
        if (parameters.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        }

        List<String> flashModes = parameters.getSupportedFlashModes();
        if (flashModes != null && flashModes.contains(mFlashMode.getValue())) {
            parameters.setFlashMode(mFlashMode.getValue());
            mViewModel.enableFlash(true);
        } else {
            mViewModel.enableFlash(false);
        }

        // Lock in the changes
        mCamera.setParameters(parameters);


        if (bestPreviewSize.width > bestPreviewSize.height) {
            mHeight = bestPreviewSize.width;
            mWidth = bestPreviewSize.height;
        } else {
            mHeight = bestPreviewSize.height;
            mWidth = bestPreviewSize.width;
        }
        int resizeWidth = mScreenWidth;
        int resizeHeight = resizeWidth * mHeight / mWidth;

        if (resizeHeight < mScreenHeight) {
            resizeHeight = mScreenHeight;
            resizeWidth = resizeHeight * mWidth / mHeight;

            int transitionX = Math.abs(resizeWidth - mScreenWidth) / -2;
            mBinding.cameraPreviewView.setTranslationX(transitionX);
        } else {
            int transitionY = Math.abs(resizeHeight - mScreenHeight) / -2;
            mBinding.cameraPreviewView.setTranslationY(transitionY);
        }

        mBinding.cameraPreviewView.getLayoutParams().width = resizeWidth;
        mBinding.cameraPreviewView.getLayoutParams().height = resizeHeight;
        mBinding.cameraPreviewView.requestLayout();

        mBinding.layoutTool.getLayoutParams().width = mScreenWidth;
        mBinding.layoutTool.getLayoutParams().height = mScreenHeight;
        mBinding.layoutTool.requestLayout();

    }

    private Size determineBestSize(List<Size> sizes) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) mScreenHeight / mScreenWidth;

        if (sizes == null) return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = mScreenHeight;

        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if ((Math.abs(size.height - targetHeight) < minDiff)) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        if (optimalSize == null) {
            int preWidthSize = 0;
            for (Camera.Size size : sizes) {
                double ratio = (double) size.width / size.height;

                if (ratio > 1.6f && (size.width > preWidthSize && size.width <= 1920)) {
                    optimalSize = size;
                    preWidthSize = optimalSize.width;
                }
            }
        }

        return optimalSize;
    }

    private int getFrontCameraID() {
        if (getActivity() == null) {
            return getBackCameraID();
        }
        PackageManager pm = getActivity().getPackageManager();
        if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT)) {
            return CameraInfo.CAMERA_FACING_FRONT;
        }

        return getBackCameraID();
    }

    private int getBackCameraID() {
        return CameraInfo.CAMERA_FACING_BACK;
    }

    /**
     * Take a picture
     */
    private void takePicture(CameraTabLayout.TabIndex index) {
        if (index == CameraTabLayout.TabIndex.PHOTO) {
            if (mIsSafeToTakePhoto) {
                setSafeToTakePhoto(false);
                mOrientationListener.rememberOrientation();
                mCamera.takePicture(null, null, null, this);
            }
        } else if (index == CameraTabLayout.TabIndex.VIDEO) {
            mBinding.cameraPreviewView.setEnableSwipe(false);

            if (mIsRecording) {
                mMediaRecorder.stop();
                releaseMediaRecorder();
                mCamera.lock();
                mIsRecording = false;

                mBinding.textViewTime.setVisibility(View.GONE);
                mBinding.textViewTime.setText(FormatTime.getFormattedDuration(MAX_VIDEO_DURATION));
                mCurrVideoRecTimer = MAX_VIDEO_DURATION;
                mTimerHandler.removeCallbacks(mRunnable);
                if (getActivity() instanceof CameraActivity) {
                    //add video file into media store, to make sure MediaStore can query new video
                    MediaScannerConnection.scanFile(getActivity(), new String[]{mVideoPath}, null, null);

                    MediaFile mediaFile = new MediaFile(mVideoPath, MediaType.VIDEO, 720, 1280);
                    List<MediaFile> mediaFiles = new ArrayList<>();
                    mediaFiles.add(mediaFile);
                    ((CameraActivity) getActivity()).returnUri(mediaFiles, true);
                }
            } else {
                if (prepareVideoRecorder()) {
                    mMediaRecorder.start();
                    mIsRecording = true;
                    getActivity().runOnUiThread(mRunnable);
                    mBinding.textViewTime.setVisibility(View.VISIBLE);
                } else {
                    releaseMediaRecorder();
                }
            }
        }
    }

    private boolean prepareVideoRecorder() {
        mMediaRecorder = new MediaRecorder();
        mCamera.unlock();
        mMediaRecorder.setCamera(mCamera);
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mMediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_720P));
        mVideoPath = FileNameUtil.getVideoFile(getActivity()).toString();
        mMediaRecorder.setOutputFile(mVideoPath);
        mMediaRecorder.setOrientationHint(getPhotoRotation());
        try {
            mMediaRecorder.prepare();
        } catch (IllegalStateException e) {
            Log.d(TAG, "IllegalStateException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            Log.d(TAG, "IOException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        }
        return true;
    }

    private void releaseMediaRecorder() {
        if (mMediaRecorder != null) {
            mMediaRecorder.reset();   // clear recorder configuration
            mMediaRecorder.release(); // release the recorder object
            mMediaRecorder = null;
            mCamera.lock();           // lock camera for later use
        }
        mTimerHandler.removeCallbacks(mRunnable);
    }


    @Override
    public void onResume() {
        super.onResume();

        if (mCamera == null) {
            restartPreview();
        }
    }

    @Override
    public void onStop() {
        releaseMediaRecorder();
        releaseCamera();

        mOrientationListener.disable();

        if (mIsRecording) {
            File file = new File(mVideoPath);
            file.delete();
            mIsRecording = false;
        }


        // stop the preview
        if (mCamera != null) {
            stopCameraPreview();
            mCamera.release();
            mCamera = null;
        }

        CameraSettingPreferences.saveCameraFlashMode(getActivity(), mFlashMode.getValue());

        super.onStop();
    }


    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        mImageParameters.mRotation = getPhotoRotation();
        mImageParameters.mWidth = mWidth;
        mImageParameters.mHeight = mHeight;
        if (getFragmentManager() == null) {
            return;
        }
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container,
                        EditSavePhotoFragment.newInstance(mCameraIntentBuilder, data, mImageParameters.createCopy()),
                        EditSavePhotoFragment.TAG)
                .addToBackStack(null)
                .commit();

        setSafeToTakePhoto(true);
    }

    private int getPhotoRotation() {
        int rotation;
        int orientation = mOrientationListener.getRememberedNormalOrientation();
        CameraInfo info = new CameraInfo();
        Camera.getCameraInfo(mCameraID, info);

        if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
            rotation = (info.orientation - orientation + 360) % 360;
        } else {
            rotation = (info.orientation + orientation) % 360;
        }
        return rotation;
    }


    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.release();        // release the camera for other applications
            mCamera = null;
        }
    }


}
