package com.desmond.squarecamera.ui.dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.desmond.squarecamera.R;

/**
 * Created by he.rotha on 4/28/16.
 */
public class MessageDialog extends DialogFragment {
    public static final String TAG = MessageDialog.class.getName();

    private String mTitle;
    private String mMessage;
    private String mLeftText;
    private String mRightText;
    private View.OnClickListener mLeftClickListener;
    private View.OnClickListener mRightClickListener;
    private Callback mCallback;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_fragment_message, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        TextView textViewTitle = view.findViewById(R.id.textview_title);
        TextView textViewMessage = view.findViewById(R.id.textview_message);
        TextView buttonLeft = view.findViewById(R.id.buttonLeft);
        TextView buttonRight = view.findViewById(R.id.buttonRight);

        if (TextUtils.isEmpty(mTitle)) {
            textViewTitle.setVisibility(View.GONE);
        } else {
            textViewTitle.setText(mTitle);
        }

        if (TextUtils.isEmpty(mMessage)) {
            textViewMessage.setVisibility(View.GONE);
        } else {
            textViewMessage.setText(mMessage);
        }

        setButtonLeftText(buttonLeft);
        setButtonRightText(buttonRight);
    }

    private void setButtonLeftText(TextView buttonLeft) {
        if (TextUtils.isEmpty(mLeftText)) {
            buttonLeft.setVisibility(View.GONE);
        } else {
            buttonLeft.setText(mLeftText);
            buttonLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mLeftClickListener != null) {
                        mLeftClickListener.onClick(view);
                    }
                    dismiss();
                }
            });
        }
    }

    private void setButtonRightText(TextView buttonRight) {
        if (TextUtils.isEmpty(mRightText)) {
            buttonRight.setVisibility(View.GONE);
        } else {
            buttonRight.setText(mRightText);
            buttonRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mRightClickListener != null) {
                        mRightClickListener.onClick(view);
                    }
                    dismiss();
                }
            });
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mCallback != null) {
            mCallback.onDismiss();
        }
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public void setLeftText(String leftText, View.OnClickListener listener) {
        mLeftText = leftText;
        mLeftClickListener = listener;
    }

    public void setRightText(String rightText, View.OnClickListener listener) {
        mRightText = rightText;
        mRightClickListener = listener;
    }

    public void show(FragmentManager manager) {
        super.show(manager, this.getClass().getName());
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    public interface Callback {
        void onDismiss();
    }

}
