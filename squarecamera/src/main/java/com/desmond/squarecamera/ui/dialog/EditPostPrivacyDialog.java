package com.desmond.squarecamera.ui.dialog;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.desmond.squarecamera.R;
import com.desmond.squarecamera.databinding.DialogEditPrivacyBinding;

/**
 * Created by He Rotha on 6/19/18.
 */
public class EditPostPrivacyDialog extends BottomSheetDialogFragment {
    public static final String TAG = EditPostPrivacyDialog.class.getName();
    private OnCallback mOnCallback;
    private DialogEditPrivacyBinding mBinding;

    public static EditPostPrivacyDialog newInstance() {
        return new EditPostPrivacyDialog();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_edit_privacy, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding.followerItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnCallback.onFollowersClick();
            }
        });
        mBinding.everyoneItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnCallback.onEveryoneClick();
            }
        });
    }

    public void setOnCallback(OnCallback callback) {
        mOnCallback = callback;
    }

    public interface OnCallback {
        void onFollowersClick();

        void onEveryoneClick();
    }
}
