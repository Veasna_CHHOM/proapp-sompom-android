package com.desmond.squarecamera.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.desmond.squarecamera.BR;
import com.desmond.squarecamera.R;
import com.desmond.squarecamera.adapter.GalleryAdapter;
import com.desmond.squarecamera.databinding.FragmentGalleryBinding;
import com.desmond.squarecamera.intent.CameraIntentBuilder;
import com.desmond.squarecamera.intent.GalleryType;
import com.desmond.squarecamera.model.MediaFile;
import com.desmond.squarecamera.ui.CameraActivity;
import com.desmond.squarecamera.utils.Keys;
import com.desmond.squarecamera.viewmodel.GalleryFrViewModel;

import java.util.List;

/**
 * Created by He Rotha on 6/7/18.
 */
public class GalleryFragment extends Fragment
        implements GalleryFrViewModel.ViewModelListener {
    public static final String TAG = GalleryFragment.class.getName();

    public GalleryAdapter mAdapter;
    private FragmentGalleryBinding mBinding;
    private GalleryFrViewModel mViewModel;
    private CameraIntentBuilder mSourceType;

    public static GalleryFragment newInstance(CameraIntentBuilder sourceType) {
        Bundle args = new Bundle();
        args.putParcelable(Keys.DATA, sourceType);
        GalleryFragment fragment = new GalleryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mBinding = FragmentGalleryBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSourceType = getArguments().getParcelable(Keys.DATA);

        mViewModel = new GalleryFrViewModel(getActivity(), mSourceType, this);
        mBinding.setVariable(BR.viewModel, mViewModel);

    }

    @Override
    public void onLoadComplete(List<MediaFile> mediaFiles) {
        mAdapter = new GalleryAdapter(mediaFiles, mSourceType, mViewModel);
        ((SimpleItemAnimator) mBinding.recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        mBinding.recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3), false);
        mBinding.recyclerView.setHasFixedSize(true);
        mBinding.recyclerView.setAdapter(mAdapter);
    }


    @Override
    public void onItemUpdate(MediaFile MediaFile, boolean isselect) {
        mAdapter.notifyItem(MediaFile, isselect);
    }

    @Override
    public void onPhotoChoose(List<MediaFile> mediaFiles) {
        if (getActivity() instanceof CameraActivity) {
            ((CameraActivity) getActivity()).returnUri(mediaFiles, false);
        }
    }

    @Override
    public void onCameraClick() {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container,
                        CameraFragment.newInstance(mSourceType),
                        CameraFragment.TAG)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackClick() {
        if (mSourceType.getOpenType() instanceof GalleryType) {
            getActivity().finish();
        } else {
            if (getActivity() instanceof CameraActivity) {
                ((CameraActivity) getActivity()).onCancel(null);
            }
        }
    }

    @Override
    public void onGalleryItemClick(int position) {
        mBinding.recyclerView.setCurrentPlayPosition(position);
    }

}
