package com.desmond.squarecamera.ui;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import com.desmond.squarecamera.R;
import com.desmond.squarecamera.helper.CheckCameraPermissionCallbackHelper;
import com.desmond.squarecamera.helper.PermissionCheckHelper;
import com.desmond.squarecamera.intent.CameraIntent;
import com.desmond.squarecamera.intent.CameraIntentBuilder;
import com.desmond.squarecamera.intent.CameraResultIntent;
import com.desmond.squarecamera.intent.CameraType;
import com.desmond.squarecamera.model.AppTheme;
import com.desmond.squarecamera.model.MediaFile;
import com.desmond.squarecamera.ui.fragment.CameraFragment;
import com.desmond.squarecamera.ui.fragment.GalleryFragment;
import com.desmond.squarecamera.utils.ThemeManager;

import java.util.ArrayList;
import java.util.List;


public class CameraActivity extends AppCompatActivity {
    public static final String TAG = CameraActivity.class.getSimpleName();
    private static final int GALLERY_REQUEST_CODE = 0X0002;
    private PermissionCheckHelper mPermissionChecker;
    private List<MediaFile> mOriginalData;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppTheme theme = ThemeManager.getAppTheme(this);
        if (theme == AppTheme.Black) {
            setTheme(R.style.squarecamera__CameraFullScreenThemeFullScreen_Black);
        } else {
            setTheme(R.style.squarecamera__CameraFullScreenThemeFullScreen_White);
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        setContentView(R.layout.squarecamera__activity_camera);

        CameraIntent intent = new CameraIntent(getIntent());
        final CameraIntentBuilder mSourceType = intent.getBuilder();
        mOriginalData = intent.getMediaFiles();
        if (mOriginalData == null) {
            mOriginalData = new ArrayList<>();
        }
        checkPermission(new CheckCameraPermissionCallbackHelper(this) {
            @Override
            public void onPermissionGranted() {
                if (savedInstanceState == null) {
                    Fragment fragment;
                    if (mSourceType.getOpenType() instanceof CameraType) {
                        fragment = CameraFragment.newInstance(mSourceType);
                    } else {
                        fragment = GalleryFragment.newInstance(mSourceType);

                    }
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.fragment_container, fragment, CameraFragment.TAG)
                            .commit();
                }
            }

            @Override
            public void onDismissDialog() {
                finish();
            }
        });

    }

    public void returnUri(List<MediaFile> mediaFiles, boolean isFromCamera) {
        if (isFromCamera) {
            mediaFiles.get(0).setPosition(mOriginalData.size());
            mOriginalData.addAll(mediaFiles);
        } else {
            mOriginalData.clear();
            mOriginalData.addAll(mediaFiles);
        }
        CameraResultIntent data = new CameraResultIntent(mOriginalData);


        if (getParent() == null) {
            setResult(RESULT_OK, data);
        } else {
            getParent().setResult(RESULT_OK, data);
        }

        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void onCancel(View view) {
        getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (mPermissionChecker != null) {
            mPermissionChecker.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void checkPermission(PermissionCheckHelper.OnPermissionCallback callback) {
        if (mPermissionChecker == null) {
            mPermissionChecker = new PermissionCheckHelper(this, GALLERY_REQUEST_CODE);
            mPermissionChecker.setPermissions(Manifest.permission.CAMERA,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        mPermissionChecker.setCallback(callback);
        mPermissionChecker.execute();
    }
}
