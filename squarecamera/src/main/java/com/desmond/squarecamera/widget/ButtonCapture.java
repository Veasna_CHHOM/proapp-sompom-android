package com.desmond.squarecamera.widget;

import android.animation.ValueAnimator;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;

import com.desmond.squarecamera.R;
import com.desmond.squarecamera.databinding.LayoutButtonCaptureBinding;

/**
 * Created by He Rotha on 8/14/18.
 */
public class ButtonCapture extends LinearLayout {
    private static final float SCALE_MIN_PERCENT = .4f; //20percent
    private static final float SCALE_MAX_PERCENT = .9f; //90percent

    private CameraTabLayout.TabIndex mPreviousTabIndex;
    private LayoutButtonCaptureBinding mBinding;

    public ButtonCapture(Context context) {
        super(context);
        init(context);
    }

    public ButtonCapture(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ButtonCapture(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.layout_button_capture, this, true);
        mBinding.buttonCenter.setScaleX(SCALE_MAX_PERCENT);
        mBinding.buttonCenter.setScaleY(SCALE_MAX_PERCENT);
    }

    public void setTabIndex(CameraTabLayout.TabIndex tabIndex) {
        if (mPreviousTabIndex == null) {
            mPreviousTabIndex = tabIndex;
        }

        Log.e("rotha", mPreviousTabIndex + " " + tabIndex);

        if (tabIndex == CameraTabLayout.TabIndex.PHOTO) {
            if (mPreviousTabIndex == CameraTabLayout.TabIndex.VIDEO) {
                ValueAnimator valueAnimator = ValueAnimator.ofFloat(SCALE_MIN_PERCENT, SCALE_MAX_PERCENT);
                valueAnimator.setDuration(300);
                valueAnimator.setInterpolator(new LinearInterpolator());
                valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        mBinding.buttonCenter.setScaleX((Float) valueAnimator.getAnimatedValue());
                        mBinding.buttonCenter.setScaleY((Float) valueAnimator.getAnimatedValue());
                    }
                });
                valueAnimator.start();
            } else {
                mBinding.buttonCenter.setScaleX(SCALE_MAX_PERCENT);
                mBinding.buttonCenter.setScaleY(SCALE_MAX_PERCENT);
            }
        } else if (tabIndex == CameraTabLayout.TabIndex.VIDEO) {
            if (mPreviousTabIndex == CameraTabLayout.TabIndex.PHOTO) {
                ValueAnimator valueAnimator = ValueAnimator.ofFloat(SCALE_MAX_PERCENT, SCALE_MIN_PERCENT);
                valueAnimator.setDuration(300);
                valueAnimator.setInterpolator(new LinearInterpolator());
                valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        mBinding.buttonCenter.setScaleX((Float) valueAnimator.getAnimatedValue());
                        mBinding.buttonCenter.setScaleY((Float) valueAnimator.getAnimatedValue());
                    }
                });

                valueAnimator.start();
            } else {
                mBinding.buttonCenter.setScaleX(SCALE_MIN_PERCENT);
                mBinding.buttonCenter.setScaleY(SCALE_MIN_PERCENT);
            }
        } else {
            mBinding.buttonCenter.setScaleX(SCALE_MAX_PERCENT);
            mBinding.buttonCenter.setScaleY(SCALE_MAX_PERCENT);
        }
        mPreviousTabIndex = tabIndex;
    }

    public void showVideoRecordingIcon(boolean isEnable) {
        if (isEnable) {
            mBinding.captureImageButton.setBackgroundResource(R.drawable.squarecamera__red_bg);
            mBinding.buttonCenter.setBackgroundResource(R.drawable.squarecamera__white_round_corner);
        } else {
            mBinding.captureImageButton.setBackgroundResource(R.drawable.squarecamera__white_bg);
            mBinding.buttonCenter.setBackgroundResource(R.drawable.squarecamera__red_bg);
        }
    }

    @Override
    public void setOnClickListener(@Nullable final OnClickListener l) {
        super.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPreviousTabIndex == CameraTabLayout.TabIndex.VIDEO) {
                    showVideoRecordingIcon(true);
                } else {
                    showVideoRecordingIcon(false);
                }
                if (l != null) {
                    l.onClick(view);
                }
            }
        });
    }
}
