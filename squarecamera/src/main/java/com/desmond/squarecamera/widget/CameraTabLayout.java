package com.desmond.squarecamera.widget;

import android.content.Context;
import android.support.annotation.StringRes;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.desmond.squarecamera.R;
import com.desmond.squarecamera.helper.OnSwipeListener;
import com.desmond.squarecamera.intent.CameraIntentBuilder;
import com.desmond.squarecamera.intent.CameraType;
import com.desmond.squarecamera.intent.GalleryType;

/**
 * Created by He Rotha on 8/9/18.
 */
public class CameraTabLayout extends TabLayout {
    private CameraType mCameraType;
    private int mCurrentPosition;
    private final OnSwipeListener mOnSwipeListener = new OnSwipeListener() {
        @Override
        public boolean onSwipe(Direction direction) {
            if (direction == Direction.left && mCurrentPosition + 1 <= getTabCount() - 1) {
                Tab tab = getTabAt(mCurrentPosition + 1);
                if (tab != null) {
                    tab.select();
                }
            } else if (direction == Direction.right && mCurrentPosition - 1 >= 0) {
                Tab tab =  getTabAt(mCurrentPosition - 1);
                if (tab != null) {
                    tab.select();
                }
            }
            return super.onSwipe(direction);
        }
    };
    private OnTabChangeListener mOnTabChangeListener;

    public CameraTabLayout(Context context) {
        super(context);
    }

    public CameraTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CameraTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public OnSwipeListener getOnSwipeListener() {
        return mOnSwipeListener;
    }

    public void setCameraIntentBuilder(CameraIntentBuilder cameraIntentBuilder) {
        if (cameraIntentBuilder.getOpenType() instanceof CameraType) {
            mCameraType = (CameraType) cameraIntentBuilder.getOpenType();
        } else if (cameraIntentBuilder.getOpenType() instanceof GalleryType) {
            mCameraType = ((GalleryType) cameraIntentBuilder.getOpenType()).getCameraType();
        } else {
            mCameraType = null;
        }

        if (mCameraType != null) {
            for (TabIndex tabIndex : mCameraType.getTabIndex()) {
                addTab(newTab().setText(tabIndex.getValue()));
            }
            Tab tab = getTabAt(mCameraType.getSelectIndex());
            if (tab != null) {
                tab.select();
            }
            for (int i = 0; i < getTabCount(); i++) {
                TextView tv1 = (TextView) (((LinearLayout) ((LinearLayout) getChildAt(0)).getChildAt(i)).getChildAt(1));
                tv1.setScaleY(-1);
            }
        }

    }

    public void setOnTabChangeListener(final OnTabChangeListener listener) {
        mOnTabChangeListener = listener;
        addOnTabSelectedListener(new OnTabSelectedListener() {
            @Override
            public void onTabSelected(Tab tab) {
                if (mCameraType == null) {
                    return;
                }
                TabIndex currentTab = mCameraType.getTabIndex().get(tab.getPosition());
                mOnTabChangeListener.onTabSelect(currentTab);
                mCurrentPosition = tab.getPosition();
            }

            @Override
            public void onTabUnselected(Tab tab) {

            }

            @Override
            public void onTabReselected(Tab tab) {

            }
        });
    }

    public enum TabIndex {
        LIVE(R.string.camera_live_button),
        PHOTO(R.string.camera_photo),
        VIDEO(R.string.camera_video);

        @StringRes
        private final int mValue;

        TabIndex(int value) {
            mValue = value;
        }

        private int getValue() {
            return mValue;
        }
    }

    public interface OnTabChangeListener {
        void onTabSelect(TabIndex tabIndex);
    }
}
