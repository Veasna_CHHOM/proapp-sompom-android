package com.desmond.squarecamera.adapter.viewholder;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;

import com.desmond.squarecamera.BR;

/**
 * Created by He Rotha on 8/8/18.
 */
public class PhotoViewHolder extends RecyclerView.ViewHolder {
    private ViewDataBinding mBinding;

    public PhotoViewHolder(ViewDataBinding itemView) {
        super(itemView.getRoot());
        mBinding = itemView;
    }

    public void bind(Object object) {
        mBinding.setVariable(BR.viewModel, object);
    }
}
