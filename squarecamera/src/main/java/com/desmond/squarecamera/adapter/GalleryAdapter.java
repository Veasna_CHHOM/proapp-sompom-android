package com.desmond.squarecamera.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.desmond.squarecamera.R;
import com.desmond.squarecamera.adapter.viewholder.PhotoViewHolder;
import com.desmond.squarecamera.adapter.viewholder.VideoViewHolder;
import com.desmond.squarecamera.intent.CameraIntentBuilder;
import com.desmond.squarecamera.intent.CameraType;
import com.desmond.squarecamera.intent.GalleryType;
import com.desmond.squarecamera.listeners.OnGalleryItemClickListener;
import com.desmond.squarecamera.model.MediaFile;
import com.desmond.squarecamera.model.MediaType;
import com.desmond.squarecamera.viewmodel.GalleryItemViewModel;

import java.util.List;

/**
 * Created by He Rotha on 6/7/18.
 */
public class GalleryAdapter extends RecyclerView.Adapter<PhotoViewHolder> {
    private static final int VIEW_TYPE_ADD = 1;
    private static final int VIEW_TYPE_PHOTO = 2;
    private static final int VIEW_TYPE_VIDEO = 3;

    private List<MediaFile> mMediaFile;
    private OnGalleryItemClickListener mListener;
    private GalleryType mBuilder;
    private boolean mIsShowButtonAdd = false;

    public GalleryAdapter(List<MediaFile> mediaFiles,
                          CameraIntentBuilder builder,
                          OnGalleryItemClickListener listener) {

        mMediaFile = mediaFiles;
        mListener = listener;
        if (builder.getOpenType() instanceof GalleryType) {
            mBuilder = (GalleryType) builder.getOpenType();
            mIsShowButtonAdd = mBuilder.getCameraType() != null;
        } else if (builder.getOpenType() instanceof CameraType) {
            mBuilder = ((CameraType) builder.getOpenType()).getGalleryType();
        }
    }

    @Override
    public void onViewRecycled(@NonNull PhotoViewHolder holder) {
        super.onViewRecycled(holder);
        if (holder instanceof VideoViewHolder) {
            ((VideoViewHolder) holder).release();
        }
    }

    @NonNull
    @Override
    public PhotoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final ViewDataBinding binding;
        if (viewType == VIEW_TYPE_ADD) {
            binding = DataBindingUtil.inflate(inflater, R.layout.squarecamera__list_item_add, parent, false);
            return new PhotoViewHolder(binding);
        } else if (viewType == VIEW_TYPE_PHOTO) {
            binding = DataBindingUtil.inflate(inflater, R.layout.squarecamera__list_item_photo, parent, false);
            return new PhotoViewHolder(binding);
        } else {
            binding = DataBindingUtil.inflate(inflater, R.layout.squarecamera__list_item_video, parent, false);
            return new VideoViewHolder(binding);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final PhotoViewHolder holder, int position) {
        if (position == 0 && mIsShowButtonAdd) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onGalleryAddClick();
                }
            });
        } else {
            final MediaFile mediaFile;
            if (mIsShowButtonAdd) {
                mediaFile = mMediaFile.get(position - 1);
            } else {
                mediaFile = mMediaFile.get(position);
            }
            holder.bind(new GalleryItemViewModel(holder.itemView.getContext(),
                    mBuilder,
                    mediaFile,
                    position,
                    mListener));

            if (holder instanceof VideoViewHolder) {
                ((VideoViewHolder) holder).setPath(mediaFile.getPath());
            }
        }
    }

    public void notifyItem(MediaFile mediaFile, boolean isSelect) {
        int i = mMediaFile.indexOf(mediaFile);
        if (i < 0) {
            return;
        }
        mMediaFile.get(i).setSelected(isSelect);
        mMediaFile.get(i).setPosition(mediaFile.getPosition());
        if (mIsShowButtonAdd) {
            notifyItemChanged(i + 1);
        } else {
            notifyItemChanged(i);
        }
    }

    @Override
    public int getItemCount() {
        if (mIsShowButtonAdd) {
            return mMediaFile.size() + 1;
        } else {
            return mMediaFile.size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mIsShowButtonAdd) {
            position = position - 1;
        }
        if (position < 0) {
            return VIEW_TYPE_ADD;
        } else {
            if (mMediaFile.get(position).getType() == MediaType.IMAGE) {
                return VIEW_TYPE_PHOTO;
            } else {
                return VIEW_TYPE_VIDEO;
            }
        }
    }

}
