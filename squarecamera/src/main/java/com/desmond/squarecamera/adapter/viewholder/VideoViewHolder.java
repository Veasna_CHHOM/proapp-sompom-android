package com.desmond.squarecamera.adapter.viewholder;

import android.annotation.SuppressLint;
import android.databinding.ViewDataBinding;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.desmond.squarecamera.R;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.PlayerView;
import com.sompom.videomanager.helper.ExoPlayerBuilder;
import com.sompom.videomanager.model.MediaListItem;

/**
 * Created by He Rotha on 8/8/18.
 */
public class VideoViewHolder extends PhotoViewHolder implements MediaListItem {
    private final PlayerView mVideoView;
    private final ImageView mImageView;
    private SimpleExoPlayer mExoPlayer;
    private String mMediaFile;

    public VideoViewHolder(ViewDataBinding itemView) {
        super(itemView);
        mVideoView = itemView.getRoot().findViewById(R.id.player_view);
        mVideoView.setUseController(false);
        mImageView = itemView.getRoot().findViewById(R.id.imageView);
    }

    public void setPath(String mediaFile) {
        this.mMediaFile = mediaFile;
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void onMediaPlay(boolean isStartPlay) {
        if (mExoPlayer != null) {
            mExoPlayer.release();
        }
        mExoPlayer = ExoPlayerBuilder.build(mVideoView.getContext(), mMediaFile, new ExoPlayerBuilder.OnPlayerListener() {
            @Override
            public void onPlayStatus(boolean isPlaying) {
                if (isPlaying) {
                    mImageView.setVisibility(View.GONE);
                } else {
                    mImageView.setVisibility(View.VISIBLE);
                }
            }
        });
        mExoPlayer.setVolume(0f);
        mVideoView.setPlayer(mExoPlayer);
        mExoPlayer.setPlayWhenReady(true);

    }

    @Override
    public void onMediaPause() {
        if (mExoPlayer != null) {
            mExoPlayer.stop();
        }
    }

    @Nullable
    @Override
    public PlayerView getPlayerView() {
        return mVideoView;
    }

    public void release() {
        if (mExoPlayer != null) {
            mExoPlayer.release();
            mExoPlayer = null;
        }
    }
}
