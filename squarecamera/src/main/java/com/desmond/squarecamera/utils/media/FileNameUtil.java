package com.desmond.squarecamera.utils.media;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.Nullable;

import com.desmond.squarecamera.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by He Rotha on 10/11/18.
 */
public final class FileNameUtil {
    private static final String sFolderMessenger = "messenger";

    private FileNameUtil() {
    }

    public static File getVideoFile(Context context) {
        return getFile(context, "MV_", ".mp4", false);
    }

    public static File getPictureFile(Context context) {
        return getFile(context, "IMG_", ".jpg", false);
    }

    public static File getPictureCacheFile(Context context) {
        return getFile(context, "IMG_", ".jpg", true);
    }

    public static File getSoundFile(Context context) {
        return getFile(context, "VOIC_", ".mp3", true);
    }

    public static File getMessengerFile(String url, Context context) {
        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                context.getString(R.string.app_name) + File.separator + sFolderMessenger
        );

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        return new File(mediaStorageDir.getPath() + File.separator + convertNameFromUrl(url));

    }

    public static String convertNameFromUrl(String url) {
        Uri uri = Uri.parse(url);
        return uri.getLastPathSegment();
    }


    @Nullable
    public static File getFile(Context context, String prex, String extension, boolean iscache) {
        File mediaStorageDir = getDirectory(context, iscache);
        if (mediaStorageDir == null) return null;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + prex + timeStamp + extension);
        return mediaFile;
    }

    @Nullable
    public static File getDirectory(Context context, boolean iscache) {
        File mediaStorageDir;
        if (iscache) {
            mediaStorageDir = new File(context.getCacheDir(),
                    context.getString(R.string.app_name));
        } else {
            mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    context.getString(R.string.app_name));
        }

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        return mediaStorageDir;
    }
}
