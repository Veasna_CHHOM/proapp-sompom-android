package com.desmond.squarecamera.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

/**
 * Created by He Rotha on 6/6/18.
 */
public final class ScreenSize {
    private ScreenSize() {
    }

    public static Point getScreenSize(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        Point sizeScreen = new Point();
        activity.getWindowManager().getDefaultDisplay().getRealSize(sizeScreen);
        return sizeScreen;
    }

    public static int getNavigationBarSize(Context context) {
        Resources resources = context.getResources();
        if (hasNavBar(resources) || hasNavBar(context)) {
            return getNavigationBarHeight(resources);
        } else {
            return 0;
        }
    }

    private static int getNavigationBarHeight(Resources resources) {
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    private static boolean hasNavBar(Resources resources) {
        int id = resources.getIdentifier("config_showNavigationBar", "bool", "android");
        return id > 0 && resources.getBoolean(id);
    }

    private static boolean hasNavBar(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        if (windowManager == null) {
            return false;
        }
        Display display = windowManager.getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        display.getRealMetrics(displayMetrics);

        int realHeight = displayMetrics.heightPixels;
        int realWidth = displayMetrics.widthPixels;

        display.getMetrics(displayMetrics);

        int displayHeight = displayMetrics.heightPixels;
        int displayWidth = displayMetrics.widthPixels;

        return (realWidth - displayWidth) > 0 || (realHeight - displayHeight) > 0;
    }
}
