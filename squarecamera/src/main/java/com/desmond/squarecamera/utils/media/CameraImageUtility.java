package com.desmond.squarecamera.utils.media;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.support.v4.graphics.BitmapCompat;
import android.view.Display;
import android.view.WindowManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by desmond on 24/10/14.
 */
public class CameraImageUtility {

    public static Uri savePicture(Context context, Bitmap bitmap, boolean isSquareImage) {
        try {
            if (isSquareImage) {
                int cropHeight;
                if (bitmap.getHeight() > bitmap.getWidth()) cropHeight = bitmap.getWidth();
                else cropHeight = bitmap.getHeight();

                bitmap = ThumbnailUtils.extractThumbnail(bitmap, cropHeight, cropHeight, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
            }

            File mediaFile = FileNameUtil.getPictureFile(context);
            if (mediaFile == null) {
                return null;
            }


            // Saving the bitmap
            try {
                ByteArrayOutputStream out = new ByteArrayOutputStream();

                double sizeInKb = BitmapCompat.getAllocationByteCount(bitmap) * 0.01 / 100;
                if (sizeInKb < 150) {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                } else if (sizeInKb >= 150 && sizeInKb < 250) {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                } else if (sizeInKb >= 250 && sizeInKb < 350) {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
                } else {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 75, out);
                }

                FileOutputStream stream = new FileOutputStream(mediaFile);
                stream.write(out.toByteArray());
                stream.close();
                bitmap.recycle();
            } catch (IOException exception) {
                exception.printStackTrace();
            }

            // Mediascanner need to scan for the image saved
            Intent mediaScannerIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri fileContentUri = Uri.fromFile(mediaFile);
            mediaScannerIntent.setData(fileContentUri);
            context.sendBroadcast(mediaScannerIntent);
            return fileContentUri;
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Decode and sample down a bitmap from a byte stream
     */
    public static Bitmap decodeSampledBitmapFromByte(Context context, byte[] bitmapBytes) {
        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

        int reqWidth, reqHeight;
        Point point = new Point();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            display.getSize(point);
            reqWidth = point.x;
            reqHeight = point.y;
        } else {
            reqWidth = display.getWidth();
            reqHeight = display.getHeight();
        }


        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inMutable = true;
        options.inBitmap = BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Load & resize the image to be 1/inSampleSize dimensions
        // Use when you do not want to scale the image with a inSampleSize that is a power of 2
        options.inScaled = true;
        options.inDensity = options.outWidth;
        options.inTargetDensity = reqWidth * options.inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false; // If set to true, the decoder will return null (no bitmap), but the out... fields will still be set, allowing the caller to query the bitmap without having to allocate the memory for its pixels.
        options.inPurgeable = true;         // Tell to gc that whether it needs free memory, the Bitmap can be cleared
        options.inInputShareable = true;    // Which kind of reference will be used to recover the Bitmap data after being clear, when it will be used in the future

        return BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        int initialInSampleSize = computeInitialSampleSize(options, reqWidth, reqHeight);

        int roundedInSampleSize;
        if (initialInSampleSize <= 8) {
            roundedInSampleSize = 1;
            while (roundedInSampleSize < initialInSampleSize) {
                // Shift one bit to left
                roundedInSampleSize <<= 1;
            }
        } else {
            roundedInSampleSize = (initialInSampleSize + 7) / 8 * 8;
        }

        return roundedInSampleSize;
    }

    private static int computeInitialSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final double height = options.outHeight;
        final double width = options.outWidth;

        final long maxNumOfPixels = reqWidth * reqHeight;
        final int minSideLength = Math.min(reqHeight, reqWidth);

        int lowerBound = (maxNumOfPixels < 0) ? 1 :
                (int) Math.ceil(Math.sqrt(width * height / maxNumOfPixels));
        int upperBound = (minSideLength < 0) ? 128 :
                (int) Math.min(Math.floor(width / minSideLength),
                        Math.floor(height / minSideLength));

        if (upperBound < lowerBound) {
            // return the larger one when there is no overlapping zone.
            return lowerBound;
        }

        if (maxNumOfPixels < 0 && minSideLength < 0) {
            return 1;
        } else if (minSideLength < 0) {
            return lowerBound;
        } else {
            return upperBound;
        }
    }

    public static String getImageUrlWithAuthority(Context context, Uri uri) {
        if (uri == null) {
            return null;
        }
        InputStream is = null;
        if (uri.getAuthority() != null) {
            try {
                is = context.getContentResolver().openInputStream(uri);
                Bitmap bmp = BitmapFactory.decodeStream(is);
                Uri uriSave = savePicture(context, bmp, false);
                return uriSave != null ? uriSave.toString() : null;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (is != null) {
                        is.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public enum FileType {
        IMAGE, VIDEO
    }
}
