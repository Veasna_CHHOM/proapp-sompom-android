package com.desmond.squarecamera.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.desmond.squarecamera.model.AppTheme;

/**
 * Created by he.rotha on 2/15/16.
 */
public final class SharedPrefUtils {
    public static final String SHARE_PREF = "SHARE_PREF";
    private static final String LANGUAGE = "LANGUAGE";

    private SharedPrefUtils() {
    }

    public static String getLanguage(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(
                SHARE_PREF, Context.MODE_PRIVATE);
        return prefs.getString(LANGUAGE, "");
    }


}
