package com.desmond.squarecamera.utils;

/**
 * Created by He Rotha on 6/11/18.
 */
public final class Keys {
    public static final String SOURCE_TYPE = "SOURCE_TYPE";
    public static final String DATA = "DATA";
    public static final String PATH = "PATH";
    public static final String PARAM = "PARAM";

    public static final String VERB_SELL = "sell";
    public static final String VERB_POST = "post";
    public static final String VERB_SHARE = "share";
    public static final String VERB_LIKE = "like";
    public static final String VERB_COMMENT = "comment";
    public static final String VERB_DISCOUNT = "discount";
    public static final String VERB_FOLLOW = "follow";

    public static final String OBJECT = "object";
    public static final String OBJECT_TYPE = "objectType";
    public static final String OBJECT_PRODUCT = "product";
    public static final String OBJECT_POST = "post";
    public static final String OBJECT_USER = "user";
    public static final String OBJECT_COMMENT = "comment";


    public static final String ID = "_id";
    public static final String ACTOR = "actor";
    public static final String VERB = "verb";
    public static final String PUBLISH = "createdAt";
    public static final String IS_READ = "isRead";
}
