package com.desmond.squarecamera.utils.media;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

/**
 * Created by desmond on 24/10/14.
 */
public final class ImageUtil {
    private static final String TAG = ImageUtil.class.getName();

    private ImageUtil() {
    }

    public static Bitmap rotateImageWithoutCompress(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public static Bitmap resizeBitmap(final Bitmap temp) {
        int maxWidth = 1080;
        int maxHeight = 720;

        int width = temp.getWidth();
        int height = temp.getHeight();
        Log.e(TAG, "Width and height are " + width + "--"
                + height + ", Size: "
                + temp.getByteCount());

        if (width > height) {
            // landscape
            float ratio = (float) width / maxWidth;
            width = maxWidth;
            height = (int) (height / ratio);
        } else if (height > width) {
            // portrait
            float ratio = (float) height / maxHeight;
            height = maxHeight;
            width = (int) (width / ratio);
        } else {
            // square
            height = maxWidth;
            width = maxWidth;
        }

        Bitmap bitmap = Bitmap.createScaledBitmap(temp, width, height, true);
        Log.e(TAG, "after scaling Width and height are "
                + width + "--"
                + height + ", New Size: "
                + bitmap.getByteCount());

        return bitmap;
    }

    public static String getRealPathFromURI(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(contentUri, proj,
                null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
}
