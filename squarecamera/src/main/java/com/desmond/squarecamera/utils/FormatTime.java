package com.desmond.squarecamera.utils;

import java.util.Locale;

/**
 * Created by imac on 8/15/17.
 */

public class FormatTime {
    public static String getFormattedDuration(int duration) {
        StringBuilder sb = new StringBuilder(8);
        int hours = duration / (60 * 60);
        int minutes = duration % (60 * 60) / 60;
        int seconds = duration % (60 * 60) % 60;

        if (duration > 3600) {
            sb.append(String.format(Locale.getDefault(), "%02d", hours)).append(":");
        }

        sb.append(String.format(Locale.getDefault(), "%02d", minutes));
        sb.append(":").append(String.format(Locale.getDefault(), "%02d", seconds));
        return sb.toString();
    }
}
