package com.desmond.squarecamera.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.desmond.squarecamera.model.AppTheme;

/**
 * Created by He Rotha on 11/13/18.
 */
public final class ThemeManager {
    private static final String APP_THEME = "APP_THEME";

    private ThemeManager() {
    }

    public static AppTheme getAppTheme(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(
                SharedPrefUtils.SHARE_PREF, Context.MODE_PRIVATE);
        final int id = prefs.getInt(APP_THEME, AppTheme.White.getId());
        return AppTheme.fromValue(id);
    }

    public static void setAppTheme(Context context, AppTheme appTheme) {
        SharedPreferences prefs = context.getSharedPreferences(
                SharedPrefUtils.SHARE_PREF, Context.MODE_PRIVATE);
        prefs.edit().putInt(APP_THEME, appTheme.getId()).apply();
    }
}
