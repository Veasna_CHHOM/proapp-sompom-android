package com.desmond.squarecamera.model;

import android.hardware.Camera;
import android.support.annotation.StringRes;

import com.desmond.squarecamera.R;

/**
 * Created by He Rotha on 8/10/18.
 */
public enum FlashMode {
    FlashOn(R.string.icon_flash_on, Camera.Parameters.FLASH_MODE_ON),
    FlashOff(R.string.icon_flash_off, Camera.Parameters.FLASH_MODE_OFF),
    FlashAuto(R.string.icon_flash_auto, Camera.Parameters.FLASH_MODE_AUTO);
    @StringRes
    private int mText;
    private String mValue;

    FlashMode(int text, String value) {
        mText = text;
        mValue = value;
    }

    public static FlashMode fromValue(String value) {
        for (FlashMode flashMode : FlashMode.values()) {
            if (flashMode.getValue().equals(value)) {
                return flashMode;
            }
        }
        return FlashOff;
    }

    public int getText() {
        return mText;
    }

    public void setText(int text) {
        mText = text;
    }

    public String getValue() {
        return mValue;
    }

    public void setValue(String value) {
        mValue = value;
    }
}
