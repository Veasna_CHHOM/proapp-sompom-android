package com.desmond.squarecamera.model;

/**
 * Created by imac on 8/15/17.
 */

public enum MediaType {
    VIDEO(0), IMAGE(1), BOTH(2);

    private int mValue;

    MediaType(int value) {
        mValue = value;
    }

    public static MediaType fromValue(int value) {
        for (MediaType mediaType : MediaType.values()) {
            if (mediaType.mValue == value) {
                return mediaType;
            }
        }
        return IMAGE;
    }

    public int getValue() {
        return mValue;
    }
}
