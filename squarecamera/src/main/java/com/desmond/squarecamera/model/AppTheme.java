package com.desmond.squarecamera.model;


import com.desmond.squarecamera.R;

/**
 * Created by He Rotha on 10/18/18.
 */
public enum AppTheme {
    White(0, R.style.AppTheme_NoActionBar_White, R.string.edit_profile_theme_light),
    Black(1, R.style.AppTheme_NoActionBar_Black, R.string.edit_profile_theme_dark);

    private int mThemeRes;
    private int mId;
    private int mStringRes;

    AppTheme(int id, int themeRes, int stringRes) {
        mId = id;
        mThemeRes = themeRes;
        mStringRes = stringRes;
    }

    public int getThemeRes() {
        return mThemeRes;
    }

    public int getId() {
        return mId;
    }

    public int getStringRes() {
        return mStringRes;
    }

    public static AppTheme fromValue(int id) {
        for (AppTheme appTheme : AppTheme.values()) {
            if (appTheme.getId() == id) {
                return appTheme;
            }
        }
        return White;
    }
}
