package com.desmond.squarecamera.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

/**
 * Created by nuonveyo on 4/24/18.
 */

public class MediaFile implements Parcelable {
    public static final Creator<MediaFile> CREATOR = new Creator<MediaFile>() {
        @Override
        public MediaFile createFromParcel(Parcel source) {
            return new MediaFile(source);
        }

        @Override
        public MediaFile[] newArray(int size) {
            return new MediaFile[size];
        }
    };
    private String mPath;
    private int mType;
    private int mWidth;
    private int mHeight;
    private boolean mIsSelected;
    private int mPosition;

    public MediaFile() {
    }

    public MediaFile(String path, String mineType) {
        mPath = path;
        if (TextUtils.isEmpty(mineType)) {
            mType = MediaType.IMAGE.getValue();
        } else {
            if (mineType.startsWith("image")) {
                mType = MediaType.IMAGE.getValue();
            } else {
                mType = MediaType.VIDEO.getValue();
            }
        }
    }

    public MediaFile(String path, MediaType type, int width, int height) {
        mPath = path;
        mType = type.getValue();
        mWidth = width;
        mHeight = height;
    }

    protected MediaFile(Parcel in) {
        this.mPath = in.readString();
        this.mType = in.readInt();
        this.mWidth = in.readInt();
        this.mHeight = in.readInt();
        this.mIsSelected = in.readByte() != 0;
        this.mPosition = in.readInt();
    }

    public String getPath() {
        return mPath;
    }

    public void setPath(String path) {
        mPath = path;
    }

    public MediaType getType() {
        return MediaType.fromValue(mType);
    }

    public void setType(MediaType type) {
        mType = type.getValue();
    }

    public int getWidth() {
        return mWidth;
    }

    public void setWidth(int width) {
        mWidth = width;
    }

    public int getHeight() {
        return mHeight;
    }

    public void setHeight(int height) {
        mHeight = height;
    }

    public boolean isSelected() {
        return mIsSelected;
    }

    public void setSelected(boolean selected) {
        mIsSelected = selected;
    }

    public int getPosition() {
        return mPosition;
    }

    public void setPosition(int position) {
        mPosition = position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MediaFile)) return false;
        return getPath().equals(((MediaFile) o).getPath());
    }

    @Override
    public int hashCode() {
        return getPath().hashCode();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mPath);
        dest.writeInt(this.mType);
        dest.writeInt(this.mWidth);
        dest.writeInt(this.mHeight);
        dest.writeByte(this.mIsSelected ? (byte) 1 : (byte) 0);
        dest.writeInt(this.mPosition);
    }
}
